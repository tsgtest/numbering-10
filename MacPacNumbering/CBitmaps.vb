Option Explicit On

Imports System.IO
Imports System.Drawing

Namespace LMP.Numbering
    Friend Class CBitmaps
        Public Function CaptureWindow(ByVal hWndSrc As Long, _
                                      ByVal LeftSrc As Long, _
                                      ByVal TopSrc As Long, _
                                      ByVal WidthSrc As Integer, _
                                      ByVal HeightSrc As Integer, _
                                      ByVal xFile As String) As Long

            Dim memoryImage As Bitmap = New Bitmap(WidthSrc, HeightSrc)
            Dim s As Size = New Size(memoryImage.Width, memoryImage.Height)

            'Create graphics 
            Dim memoryGraphics As Graphics = Graphics.FromImage(memoryImage)
            memoryGraphics.CopyFromScreen(LeftSrc, TopSrc, LeftSrc, TopSrc, s)
            memoryImage.Save(xFile)
        End Function
    End Class
End Namespace


