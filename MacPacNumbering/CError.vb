Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.cNumTOC
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables

Namespace LMP.Numbering
    Friend Class CError
        Enum mpnErrors
            mpError_NoTOCStyFile = 523
            mpError_InvalidSchemePropValue = 524
            mpError_CouldNotUpgradeStyFile = 525
            mpError_NoSharedSchemeDeletePermission = 526
            mpError_NoSharedSchemeCategories = 527
            mpError_CouldNotOpenFAQ = 528
        End Enum

        Sub Raise(CurError As ErrObject)
            Dim xDesc As String
            Dim iSeverity As Integer
            Dim xError As String
            Dim lError As Long
            Dim xErrSource As String
            Dim xTitle As String

            With CurError
                xError = .Description
                lError = .Number
                xErrSource = .Source
            End With

            On Error GoTo ProcError
            EchoOn()
            CurWordApp.ScreenUpdating = True

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                xTitle = "Num�rotation TSG"
                Select Case lError
                    Case mpnErrors.mpError_CouldNotOpenFAQ
                        xDesc = "Ne peut ouvrir FAQ.doc. Le fichier est peut �tre corrompu ou n'est pas en format MS Word."
                        iSeverity = vbExclamation
                    Case mpnErrors.mpError_NoSharedSchemeCategories
                        xDesc = "Aucune cat�gorie de th�me partag� n'a �t� trouv�.  Votre administrateur doit ajouter au moins une cat�gorie � vos th�mes partag�s."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_NoTOCStyFile
                        xDesc = "tsgTOC.sty est manquant du r�pertoire de groupe de travail.  Veuillez contacter votre administrateur."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_CouldNotUpgradeStyFile
                        xDesc = "Ne peut mettre � jour Th�mes de Num�rotation publics/priv�s. Th�mes utilis�s dans Office 2000.  Veuillez contacter votre administrateur."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_NoSharedSchemeDeletePermission
                        xDesc = "Vous n'avez pas les droits pour supprimer ce th�me. Seulement la personne qui a affich� ce th�me et votre administrateur peuvent supprimer ce th�me."
                        iSeverity = vbExclamation
                    Case Else
                        xDesc = "Une erreur inattendue s'est produite dans la proc�dure " & _
                            xErrSource & "." & vbCr & "Erreur #" & lError & ": " & _
                            xError
                        iSeverity = vbCritical
                End Select
            Else
                xTitle = "TSG Numbering"
                Select Case lError
                    Case mpnErrors.mpError_CouldNotOpenFAQ
                        xDesc = "Could not open FAQ.doc.  The file may " & _
                                "be corrupt or not in MS Word format."
                        iSeverity = vbExclamation
                    Case mpnErrors.mpError_NoSharedSchemeCategories
                        xDesc = "No shared scheme categories were found.  " & _
                                "Please have your administrator " & _
                                "add at least one category to your " & _
                                "set of shared schemes."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_NoTOCStyFile
                        xDesc = "tsgTOC.sty is missing from your Workgroup directory.  " & _
                            "Please contact your administrator."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_CouldNotUpgradeStyFile
                        xDesc = "Could not upgrade your public/private Numbering " & _
                                       "Schemes for use with office 2000.  " & _
                                       "Please contact your administrator."
                        iSeverity = vbCritical
                    Case mpnErrors.mpError_NoSharedSchemeDeletePermission
                        xDesc = "You do not have permission to delete this scheme.  " & vbCr & _
                            "Only the person who posted the scheme and your " & _
                            "administrator can delete this scheme."
                        iSeverity = vbExclamation
                    Case Else
                        xDesc = "An unexpected error occurred in procedure " & _
                            xErrSource & "." & vbCr & "Error #" & lError & ": " & _
                            xError
                        iSeverity = vbCritical
                End Select
            End If
            MsgBox(xDesc, iSeverity, xTitle)
            Exit Sub

ProcError:
            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                xMsg = "Une erreur inattendue s'est produite dans ""CError.RaiseError"". " & _
                       vbCr & Err.Number & ":" & Err.Description & "."
            Else
                xMsg = "An unexpected error occurred in ""CError.RaiseError"". " & _
                       vbCr & Err.Number & ":" & Err.Description & "."
            End If
            MsgBox(xMsg, vbCritical, g_xAppName)
            Exit Sub
        End Sub
    End Class
End Namespace

