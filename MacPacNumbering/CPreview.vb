Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cNumbers
Imports LMP.Numbering.Base.cStrings
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports System.Runtime.InteropServices
Imports System.Drawing

Namespace LMP.Numbering
    Friend Class cPreview
        '**********************************************************
        '   CPreview Collection Class
        '   created 3/01/98 by Daniel Fisherman-
        '   momshead@earthlink.net

        '   Contains properties and methods that define the
        '   CPreview object - the numbering Preview - the preview
        '   is the visual representation of the numbering scheme
        '   that is displayed when the edit scheme dialog is active.
        '   numbering bitmaps are created by using bit block transfers
        '   of the screen representation of the preview.
        '**********************************************************
        Private Const GW_CHILD = 5
        Private Const GW_HWNDFIRST = 0
        Private Const GW_HWNDLAST = 1
        Private Const GW_HWNDNEXT = 2
        Private Const GW_HWNDPREV = 3
        Private Const GW_MAX = 5

        <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)> _
        Private Shared Function GetWindow(ByVal hWnd As IntPtr, ByVal uCmd As UInt32) As IntPtr
        End Function

        <DllImport("user32.dll", EntryPoint:="GetWindowText")>
        Private Shared Function GetWindowText(ByVal hwnd As Integer, ByVal lpString As System.Text.StringBuilder, ByVal cch As Integer) As Integer
        End Function

        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function IsWindowVisible(ByVal hWnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        <DllImport("user32.dll")> _
        Private Shared Function GetWindowPlacement(ByVal hWnd As IntPtr, ByRef lpwndpl As WINDOWPLACEMENT) As Boolean
        End Function

        Private Structure udtWindowProps
            Shared iZoom As Integer
            Shared bShowAll As Boolean
            Shared bShowTabs As Boolean
            Shared bShowSpaces As Boolean
            Shared bShowParas As Boolean
            Shared bShowTableGrids As Boolean
            Shared iView As Integer
        End Structure

        Private m_lRefreshes As Long

        Private Structure POINTAPI
            Shared X As Long
            Shared Y As Long
        End Structure

        Private Structure RECT
            Shared Left As Long
            Shared Top As Long
            Shared Right As Long
            Shared Bottom As Long
        End Structure

        Private Structure WINDOWPLACEMENT
            Shared Length As Long
            Shared flags As Long
            Shared showCmd As Long
            Shared ptMinPosition As POINTAPI
            Shared ptMaxPosition As POINTAPI
            Shared rcNormalPosition As RECT
        End Structure

        Public Function ShowPreview(ByVal xScheme As String, ByVal iSchemeType As mpSchemeTypes) As Long
            Dim rngLocation As Word.Range
            Dim xAutoText As String
            Dim oAutoText As Word.AutoTextEntry
            Dim WinProps As udtWindowProps
            Dim rngP As Word.Range
            Dim xSource As String
            Dim iZoom As Integer

            EchoOff()
            CurWordApp.ScreenUpdating = False

            '   get current zoom
            On Error Resume Next
            iZoom = GetUserSetting("Numbering", "Zoom")
            On Error GoTo 0

            With CurWordApp.ActiveDocument
                '       set margins/pageheight only if necessary - there's a Word quirk
                '       such that setting page setup programmatically the first time
                '       in a session takes an inordinate amount of time - this solution
                '       is the best we can do.
                With .Content.Sections(1).PageSetup
                    If .Orientation = Word.WdOrientation.wdOrientLandscape Then
                        .Orientation = Word.WdOrientation.wdOrientPortrait
                    End If
                    If (.LeftMargin <> 90) Then
                        .LeftMargin = 90
                    End If
                    If (.RightMargin <> 70) Then
                        .RightMargin = 70
                    End If
                    If .PageHeight < (22 * 72) Then
                        .PageHeight = (22 * 72)
                    End If
                End With

                rngLocation = .Content
                rngLocation.StartOf()

                '       9.7.4 - decided to always create the preview on the fly, after three
                '       different XP clients experienced the same problem with corrupt autotext;
                '       specifically, some of the numbered paragraphs in the preview table are
                '       winding up with simple numbering, causing errors in AdjustLines

                '        If iSchemeType = mpSchemeTypes.mpSchemeType_Private Then
                ''           insert preview autotext
                '            xAutoText = xScheme & "Preview"
                '            On Error Resume Next
                '            Set oAutoText = Templates(g_xPNumSty) _
                '                        .AutoTextEntries(xAutoText)
                '            On Error GoTo 0
                '
                '            If Not (oAutoText Is Nothing) Then
                '                On Error Resume Next
                '                oAutoText.Insert rngLocation, True
                '                Zoom CBool(iZoom), False
                '                If Err.Number Then
                '                    On Error GoTo 0
                '                    InsertTable rngLocation, xScheme
                '                Else
                ''                   relink list template to level 1 style this
                ''                   guarantees that list levels display correctly
                '                    On Error GoTo 0
                '                    Set rngP = CurWordApp.ActiveDocument.Tables(1).Cell(1, 1).Range
                '                    With rngP.ListFormat
                '                        If Not g_bIsXP Then
                ''                           not even sure whether this relinking is necessary anymore,
                ''                           but in XP, it causes all style indents to go to 0
                '                            CurWordApp.ActiveDocument.Styles(rngP.Style) _
                '                                .LinkToListTemplate .ListTemplate, .ListLevelNumber
                '                        End If
                '                    End With
                '                End If
                '            Else
                '                InsertTable rngLocation, xScheme
                '            End If
                '        Else
                InsertTable(rngLocation, xScheme)
                '        End If

                '       add trailing returns to move
                '       end of file line below screen
                rngLocation = CurWordApp.ActiveDocument.Content
                With rngLocation
                    .EndOf()
                    .InsertAfter(New String(vbCr, 160))
                    .Style = WdBuiltinStyle.wdStyleNormal
                    .EndOf()
                    .Select()
                End With
            End With

            On Error Resume Next
            With CurWordApp.ActiveWindow
                .VerticalPercentScrolled = 0
                With .View
                    .TableGridlines = False
                    .ShowAll = False
                    .ShowTabs = False
                    .ShowSpaces = False
                    .ShowParagraphs = False
                    .Type = WdViewType.wdNormalView
                End With
                .StyleAreaWidth = 0
            End With
            EchoOn()
        End Function

        Private Sub InsertTable(rngLocation As Word.Range, xScheme As String)
            Dim i As Integer
            Dim iLevels As Integer
            Dim xStyle As String
            Dim rngParaStart As Word.Range
            Dim tblPreview As Word.Table
            Dim rngLevel As Word.Range
            Dim rngHeading As Word.Range
            Dim iZoom As Integer

            '    Application.StatusBar = "Updating Preview..."

            On Error Resume Next
            iZoom = GetUserSetting("Numbering", "Zoom")
            On Error GoTo 0
            Zoom(iZoom, False)

            With CurWordApp.ActiveDocument
                On Error Resume Next
                '       add a table at the beginning of document
                tblPreview = rngLocation.Tables.Add(rngLocation, 9, 1)

                tblPreview.Borders.Enable = False
                tblPreview.Rows.HeightRule = WdRowHeightRule.wdRowHeightAuto

                iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)

                '       insert paras for all levels
                For i = 1 To iLevels
                    On Error Resume Next
                    If bIsHeadingScheme(xScheme) Then 'GLOG 15853 (dm)
                        xStyle = xTranslateHeadingStyle(i)
                    Else
                        xStyle = xGetStyleRoot(xScheme) & "_L" & i
                    End If
                    If .Styles(xStyle) Is Nothing Then
                        Exit For
                    End If
                    On Error GoTo 0
                    rngLevel = InsertLevel(tblPreview, xScheme, i)
                Next i
            End With

            CurWordApp.ActiveDocument.UndoClear()
            '    Application.StatusBar = ""
        End Sub

        Sub RefreshPreviewLevel(xScheme As String, iLevel As Integer)
            Dim rngP As Word.Range
            Dim i As Integer
            Dim iLineSpaceRule As WdLineSpacing
            Dim iLineSpacing As WdLineSpacing
            Dim iPreLineSpacing As Integer
            Dim iPostLineSpacing As Integer
            Dim xPath As String

            On Error Resume Next
            With CurWordApp.ActiveDocument
                rngP = .Tables(1).Cell(iLevel, 1).Range
                If rngP Is Nothing Then
                    Exit Sub
                End If

                '       get spacing before refresh of level
                iPreLineSpacing = rngP.ParagraphFormat.LineSpacing

                'GLOG 5463 - removing this screen refresh prevents Word
                'from crashing when editing with multiple Bingham documents
                'of a certain type open in print layout view - what reason
                'could there be for a screen refresh here?
                '        CurWordApp.ScreenRefresh

                '       remove number
                rngP.Delete()
                rngP.Style = wdBuiltInStyle.wdStyleNormal
                InsertLevel(.Tables(1), xScheme, iLevel)

                'GLOG 15971 (dm) - adjust lines
                AdjustLines(iLevel)

                '       increment the number of refreshes executed-
                '       when this number reaches a threshold, save
                '       preview - this prevents "Formatting is too
                '       complex" message from Word
                m_lRefreshes = m_lRefreshes + 1
                If m_lRefreshes Mod 28 = 0 Then
                    xPath = g_xUserPath & "\Refresh.mpf"
                    .SaveAs(xPath, , , , False)
                    'there were circumstances in which there was
                    'no active doc after resaving (see GLOG 4756)
                    CurWordApp.Documents(xPath).Activate()
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        .CurWordApp.ActiveWindow.Caption = "Aper�u Th�me"
                    Else
                        .CurWordApp.ActiveWindow.Caption = "Scheme Preview"
                    End If
                End If

                '       show change now
                EchoOn()
            End With
        End Sub

        Private Function InsertLevel(tblPreview As Word.Table, _
                                     ByVal xScheme As String, _
                                     ByVal iLevel As Integer) As Word.Range
            Dim xStyle As String
            Dim rngParaStart As Word.Range
            Dim i As Integer
            Dim bHeadingIsPara As Boolean
            Dim bLinesInserted As Boolean
            Dim llCur As Word.ListLevel
            Dim iHeadingFormat As Integer
            Dim lDelStart As Long
            Dim lDelEnd As Long
            Dim iLineSpaceRule As WdLineSpacing
            Dim iLineSpacing As Single
            Dim iAlignment As WdParagraphAlignment
            Dim iSpaceAfter As Integer
            Dim iMove As Integer
            Dim iTrailUnderline As Integer

            '   get style name
            xStyle = xGetStyleName(xScheme, iLevel)

            '   set range as appropriate row of table
            rngParaStart = tblPreview.Range.Cells(iLevel) _
                               .Range.Paragraphs(1).Range

            '   insert number
            rngParaStart = rngInsertNumWordNum(rngParaStart, _
                                                   xScheme, _
                                                   iLevel)
            llCur = rngParaStart.ListFormat _
                        .ListTemplate.ListLevels(iLevel)

            With rngParaStart
                With .ParagraphFormat
                    '            .LeftIndent = llCur.TextPosition
                    '            .FirstLineIndent = llCur.NumberPosition - _
                    '                               llCur.TextPosition
                    On Error Resume Next
                    .TabStops(1).Position = llCur.TabPosition

                    '           delete extra tab stops (12/21/00)
                    For i = 2 To .TabStops.Count
                        With .TabStops(i)
                            If .Position <> llCur.TabPosition Then
                                .Clear()
                            End If
                        End With
                    Next i
                End With
                .StartOf()
                .InsertAfter(g_xPreviewLevelText & iLevel)

                '       format info is held as bits
                '       in an integer - get integer
                iHeadingFormat = xGetLevelProp(xScheme, _
                                          iLevel, _
                                          mpNumLevelProps.mpNumLevelProp_HeadingFormat, _
                                          mpSchemeTypes.mpSchemeType_Document)
                With rngParaStart.Font
                    '           test bits for format value
                    .Bold = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Bold)
                    .Italic = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Italic)
                    .AllCaps = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_AllCaps)
                    .Underline = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Underline)
                    .SmallCaps = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_SmallCaps)
                    .ColorIndex = WdColorIndex.wdBlue
                End With

                .EndOf()

                With .ParagraphFormat
                    '           guarantee that left indent and first line indent
                    '           show the indents of the list template by setting
                    '           the para formats to the values of the corresponding
                    '           list templates - there are instances where the
                    '           autotext previews lose there 'number indent' and
                    '           'text indent' values - i suspect that the para values
                    '           are overriding them
                    '            .LeftIndent = llCur.TextPosition
                    '            .FirstLineIndent = llCur.NumberPosition - .LeftIndent

                    '           if user has chosen to base para on host doc
                    '           "normal" style, preview as left aligned;
                    '           right alignment vs. base on normal is now held in
                    '           trailing underline level prop
                    iTrailUnderline = xGetLevelProp(xScheme, _
                                                    iLevel, _
                                                    mpNumLevelProps.mpNumLevelProp_TrailUnderline, _
                                                    mpSchemeTypes.mpSchemeType_Document)
                    If ((.Alignment = WdParagraphAlignment.wdAlignParagraphRight) And (iTrailUnderline < 2)) Or _
                            bBitwisePropIsTrue(iTrailUnderline, _
                            mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal) Then
                        .Alignment = WdParagraphAlignment.wdAlignParagraphLeft
                    End If
                    iAlignment = .Alignment
                    .PageBreakBefore = False
                End With

                '       insert lines if not centered
                If Not iAlignment = WdParagraphAlignment.wdAlignParagraphCenter Then
                    .InsertAfter(New String(vbTab, 24))
                    .Underline = WdUnderline.wdUnderlineSingle

                    bHeadingIsPara = (iHeadingFormat And _
                                      mpTCFormatFields.mpTCFormatField_Type)

                    If bHeadingIsPara Then
                        .Font.ColorIndex = WdColorIndex.wdBlue
                    Else
                        .Font.ColorIndex = WdColorIndex.wdAuto
                    End If
                    bLinesInserted = True
                End If

                With rngParaStart
                    .Expand(wdUnits.wdParagraph)

                    '           set font size
                    If bIsZoomedOut() Then
                        .Font.Size = 20
                    Else
                        .Font.Size = 12
                    End If

                    '           get eo para
                    lDelEnd = .End

                    .Select()
                End With

                '       delete all text after first two lines
                CurWordApp.Selection.StartOf()
                If InStr(rngParaStart, New String(Chr(11), 2)) Then
                    iMove = 3
                Else
                    iMove = 2
                End If

                lRet = CurWordApp.Selection.Move(WdUnits.wdLine, iMove)
                lDelStart = CurWordApp.Selection.Start

                If iAlignment <> WdParagraphAlignment.wdAlignParagraphJustify Then
                    '           delete one extra tab at start of range-
                    '           this will mimic left or center alignment
                    lDelStart = lDelStart - 1
                End If
                CurWordApp.ActiveDocument.Range(lDelStart, lDelEnd).Text = ""
                If iAlignment = WdParagraphAlignment.wdAlignParagraphLeft Then
                    rngParaStart.InsertAfter("__")
                End If
                With rngParaStart.ParagraphFormat
                    '           set line spacing
                    If .LineSpacing < 16 Then
                        iLineSpacing = 17.5
                    ElseIf .LineSpacing < 22 Then
                        iLineSpacing = 24
                    ElseIf .LineSpacing < 28 Then
                        iLineSpacing = 30
                    Else
                        iLineSpacing = 36
                    End If
                    .LineSpacingRule = wdLineSpacing.wdLineSpaceExactly
                    .LineSpacing = iLineSpacing

                    '            iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, .LineSpacing)
                    '            .LineSpacingRule = wdLineSpaceExactly
                    '            If iLineSpaceRule = wdLineSpace1pt5 Then
                    '                iLineSpacing = 24
                    '            ElseIf iLineSpaceRule = wdLineSpaceDouble Then
                    '                iLineSpacing = 30
                    '            Else
                    '                iLineSpacing = 17.5
                    '            End If
                    '            .LineSpacing = iLineSpacing
                End With
                .EndOf()
                InsertLevel = rngParaStart
            End With

            '   move insertion point out of the way
            CurWordApp.ActiveDocument.Paragraphs.Last.Range.Select()
            CurWordApp.ActiveWindow.VerticalPercentScrolled = 100
            CurWordApp.ActiveWindow.VerticalPercentScrolled = 0
        End Function

        Private Function CapturePreviewWindow(ByVal xFile As String) As Long
            'captures the scheme preview and saves to specified file
            Dim oBitmap As CBitmaps
            Dim iCharsRet As Integer
            Dim oRect As RECT
            Dim sb As New System.Text.StringBuilder(" ", 100)

            Dim hWndTop As IntPtr = FindWindow(vbNullString, _
                CurWordApp.ActiveWindow.Caption & " - " & CurWordApp.Caption)

            If hWndTop = 0 Then
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    MsgBox("Ne peut trouver la fen�tre de l'aper�u.  L'image ne peut pas �tre cr�e/mise � jour.",
                           vbCritical, g_xAppName)
                Else
                    MsgBox("Unable to locate preview window.  The bitmap cannot not be " & _
                        "created/updated.", vbCritical, g_xAppName)
                End If
                Exit Function
            End If

            'get first Word child window
            Dim oChildWnd As IntPtr = GetWindow(hWndTop, GW_CHILD)

            'get preview doc window, which is the first
            'visible child window whose text is "MsoDockLeft"
            iCharsRet = GetWindowText(oChildWnd, sb, sb.Capacity + 1)

            Dim xText As String = sb.ToString()
            While InStr(xText, "MsoDockLeft") = 0
                oChildWnd = GetWindow(oChildWnd, GW_HWNDNEXT)
                iCharsRet = GetWindowText(oChildWnd, sb, sb.Capacity + 1)
            End While

            'specify bounds of screen shot
            Dim iFactor = LMP.Numbering.PublicFunctions.GetScreenScalingFactor()
            Dim iTop As Integer = mpnControls.PreviewCapture.GetPreviewWindowTop(oChildWnd) + 26 * iFactor
            Dim iLeft As Integer = 20 * iFactor
            Dim iWidth As Integer = 300 * iFactor
            Dim iHeight As Integer = mpMin(400 * iFactor, (CurWordApp.ActiveWindow.Height + 40 / iFactor) * iFactor)

            Dim memoryImage As Bitmap = New Bitmap(iWidth, iHeight)
            Dim memoryGraphics As Graphics = Graphics.FromImage(memoryImage)

            'copy screen
            memoryGraphics.CopyFromScreen(iLeft, iTop, 0, 0, New Size(iWidth, iHeight))
            If (CScaling.GetScalingFactor > 1) Then
                memoryImage = CScaling.ResizeImage(memoryImage, 300, 400)
            End If
            memoryImage.Save(xFile)
        End Function

        Sub CreateBitmap(ByVal xScheme As String, ByVal xAlias As String)
            Dim lPicture As Long
            Dim rngP As Word.Range
            Dim i As Long
            Dim xBitmap As String

            ''   remove highlight - on some machines, it shows up in snapshot
            ''3/22/13 - entire preview was turning black on a VM machine with Word 2013
            'If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then _
            RemoveHighlight()

            If Not bIsZoomedOut() Then
                '       ensure zoom out
                EchoOff()
                ZoomOut()

                '9.9.6006 (8/27/15)
                If g_iWordVersion = mpWordVersions.mpWordVersion_2013 Then
                    Dim l As Long
                    For l = 1 To 100
                        System.Windows.Forms.Application.DoEvents()
                    Next l
                End If

                EchoOn()
            End If

            With CurWordApp
                rngP = .ActiveDocument.Tables(1).Range

                .ScreenUpdating = True
                .ScreenRefresh()
                .ScreenUpdating = False
                '.Activate()

                xBitmap = xSubstitute(xAlias, "/", "=")
                lPicture = CapturePreviewWindow( _
                            g_xMPBPath & "\" & xBitmap & ".mpb")

                '9.8.1007 - removed code below because we're now keeping sty files loaded
                'and we stopped using autotext entries in 9.7.4
                ''       attempt to add autotext entry - ignore
                ''       if not successful - scheme preview will
                ''       then be created on the fly
                '        On Error Resume Next
                ''       store preview table as autotext
                '        .Templates(g_xPNumSty).AutoTextEntries.Add _
                '                    xScheme & "Preview", rngP
                '        If Err.Number Then
                '            Err.Clear
                '            .Templates(g_xPNumSty) _
                '                .AutoTextEntries(xScheme & "Preview").Delete
                '        End If
                '
                '        #If compHandleErrors Then
                '            On Error GoTo ProcError
                '        #End If
            End With
        End Sub

        Function GetFile(ByVal xAlias As String, ByVal iSchemeType As mpSchemeTypes) As String
            Dim xPreviewFilePath As String
            Dim xBitmap As String
            Dim xAppPath As String

            '   get appropriate preview file
            xAppPath = GetAppPath()
            xBitmap = xSubstitute(xAlias, "/", "=")
            Select Case iSchemeType
                Case mpSchemeTypes.mpSchemeType_Document
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xPreviewFilePath = xAppPath & "\DocSchemeFrench" & ".mpb"
                    Else
                        xPreviewFilePath = xAppPath & "\DocScheme" & ".mpb"
                    End If
                Case mpSchemeTypes.mpSchemeType_Private
                    xPreviewFilePath = g_xMPBPath & "\" & xBitmap & ".mpb"

                    If Dir(xPreviewFilePath) = "" Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xPreviewFilePath = xAppPath & "\NoPreviewFrench.mpb"
                        Else
                            xPreviewFilePath = xAppPath & "\NoPreview.mpb"
                        End If
                    End If
                Case mpSchemeTypes.mpSchemeType_Public
                    xPreviewFilePath = xAppPath & "\" & xBitmap & ".mpb"

                    If Dir(xPreviewFilePath) = "" Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xPreviewFilePath = xAppPath & "\NoPreviewFrench.mpb"
                        Else
                            xPreviewFilePath = xAppPath & "\NoPreview.mpb"
                        End If
                    End If
                Case Else
                    '           scheme type is "category"
                    Select Case xAlias
                        Case mpPersonalSchemes
                            xPreviewFilePath = xAppPath & "\PersonalSchemes.mpb"
                        Case mpPublicSchemes
                            If g_bIsAdmin Then
                                xPreviewFilePath = xAppPath & "\AdminSchemes.mpb"
                            Else
                                xPreviewFilePath = xAppPath & "\FirmSchemes.mpb"
                            End If
                        Case mpDocumentSchemes
                            xPreviewFilePath = xAppPath & "\DocumentSchemes.mpb"
                        Case mpAdminSchemes
                            xPreviewFilePath = xAppPath & "\AdminSchemes.mpb"
                        Case mpFavoriteSchemes
                            xPreviewFilePath = xAppPath & "\FavoriteSchemes.mpb"
                        Case mpPersonalSchemesFrench
                            xPreviewFilePath = xAppPath & "\PersonalSchemesFrench.mpb"
                        Case mpPublicSchemesFrench
                            If g_bIsAdmin Then
                                xPreviewFilePath = xAppPath & "\AdminSchemesFrench.mpb"
                            Else
                                xPreviewFilePath = xAppPath & "\FirmSchemesFrench.mpb"
                            End If
                        Case mpDocumentSchemesFrench
                            xPreviewFilePath = xAppPath & "\DocumentSchemesFrench.mpb"
                        Case mpAdminSchemesFrench
                            xPreviewFilePath = xAppPath & "\AdminSchemesFrench.mpb"
                        Case mpFavoriteSchemesFrench
                            xPreviewFilePath = xAppPath & "\FavoriteSchemesFrench.mpb"
                        Case Else
                            xPreviewFilePath = xAppPath & "\Blank.mpb"
                    End Select
            End Select
            GetFile = xPreviewFilePath
        End Function

        Sub HighlightLevel(iLevel As Integer)
            'Highlights the cell containing the specified level -
            'removes highlight from all other cells
            Dim rngP As Word.Range
            Dim rngQ As Word.Range
            Dim sVScroll As Single

            CurWordApp.ActiveDocument.UndoClear()
            On Error Resume Next
            rngP = CurWordApp.ActiveDocument _
                .Tables(1).Range.Cells(iLevel).Range
            On Error GoTo 0
            If rngP Is Nothing Then
                Exit Sub
            End If
            On Error Resume Next

            '   clear current highlight
            RemoveHighlight()

            '   highlight current level
            rngP.Shading.Texture = WdTextureIndex.wdTexture7Pt5Percent

            rngP.StartOf()
            rngP.Select()
            rngQ = CurWordApp.ActiveDocument.Content
            rngQ.EndOf()

            With CurWordApp.ActiveWindow
                sVScroll = .VerticalPercentScrolled
                rngQ.Select()
                .VerticalPercentScrolled = 100
                '       ensure that highlighted level is visible
                '       on screen if not currently visible
                If iLevel = 1 Then
                    .VerticalPercentScrolled = 0
                Else
                    rngP.Select()
                    If CurWordApp.Selection.Information( _
                        WdInformation.wdHorizontalPositionRelativeToPage) = -1 Then
                        .VerticalPercentScrolled = sVScroll + 10
                        sVScroll = .VerticalPercentScrolled
                    Else
                        .VerticalPercentScrolled = 0
                        sVScroll = .VerticalPercentScrolled
                    End If
                    rngQ.Select()
                    .VerticalPercentScrolled = sVScroll
                End If
            End With
        End Sub

        Public Function BitmapExists(xSchemeDisplayName As String, _
                                     iSchemeType As mpSchemeTypes) As Boolean
            'returns TRUE if the bitmap for the specified scheme name exists
            Select Case iSchemeType
                Case mpSchemeTypes.mpSchemeType_Private
                    BitmapExists = (Dir(g_xUserPath & "\" & _
                                xSchemeDisplayName) <> "")
                Case mpSchemeTypes.mpSchemeType_Public
                    BitmapExists = (Dir(GetAppPath() & "\" & _
                                xSchemeDisplayName) <> "")
                Case mpSchemeTypes.mpSchemeType_Document
                    BitmapExists = False
            End Select
        End Function

        Private Function GetCurWindowProps() As udtWindowProps
            Dim WinProps As udtWindowProps
            With CurWordApp.ActiveWindow.View
                WinProps.bShowAll = .ShowAll
                WinProps.bShowTabs = .ShowTabs
                WinProps.bShowSpaces = .ShowSpaces
                WinProps.bShowParas = .ShowParagraphs
                WinProps.bShowTableGrids = .TableGridlines
                WinProps.iView = .Type
                WinProps.iZoom = .Zoom.Percentage
            End With
        End Function

        Private Sub SetCurWindowProps(WinProps As udtWindowProps)
            With CurWordApp.ActiveWindow.View
                .ShowAll = WinProps.bShowAll
                .ShowTabs = WinProps.bShowTabs
                .ShowSpaces = WinProps.bShowSpaces
                .ShowParagraphs = WinProps.bShowParas
                .TableGridlines = WinProps.bShowTableGrids
                .Type = WinProps.iView
                .Zoom.Percentage = WinProps.iZoom
            End With
        End Sub

        Public Sub Zoom(ByVal bIn As Boolean,
                        Optional ByVal bAdjustLines As Boolean = True,
                        Optional ByVal bRemoveHighlight As Boolean = False)
            If bIn Then
                ZoomIn(bAdjustLines)
            Else
                ZoomOut(bRemoveHighlight, bAdjustLines)
            End If

            '   refresh ruler to reflect zoom  - tweaked for Word 10
            With CurWordApp.ActiveWindow
                .DisplayRulers = False
                .DisplayRulers = True
            End With
        End Sub

        Public Sub ZoomIn(Optional ByVal bAdjustLines As Boolean = True)
            'makes preview 100% and changes font to 12 pt
            Dim llCur As Word.ListLevel
            Dim xLT As String

            System.Windows.Forms.Application.DoEvents()
            xLT = xGetFullLTName(g_oCurScheme.Name)
            CurWordApp.ActiveDocument.ActiveWindow.View.Zoom.Percentage = 70 ' / CScaling.GetScalingFactor()
            CurWordApp.ActiveDocument.Content.Font.Size = 12
            For Each llCur In CurWordApp.ActiveDocument.ListTemplates(xLT).ListLevels
                If llCur.Font.Size <> WdConstants.wdUndefined Then
                    llCur.Font.Size = 12
                End If
            Next llCur

            If bAdjustLines Then
                AdjustLines()
            End If
        End Sub

        Private Shared ReadOnly Property ZoomPercentage As Single
            Get
                Return 41 ' / LMP.Numbering.PublicFunctions.GetScreenScalingFactor()
            End Get
        End Property

        Public Sub ZoomOut(Optional ByVal bRemoveHighlight As Boolean = False, _
                           Optional ByVal bAdjustLines As Boolean = True)
            Dim llCur As Word.ListLevel
            Dim xLT As String

            If bRemoveHighlight Then
                RemoveHighlight()
            End If
            System.Windows.Forms.Application.DoEvents()
            xLT = xGetFullLTName(g_oCurScheme.Name)
            CurWordApp.ActiveDocument.ActiveWindow.View.Zoom.Percentage = ZoomPercentage
            'GLOG 8611: Add 1pt for each 25% increase in scaling
            CurWordApp.ActiveDocument.Content.Font.Size = 20 + ((CScaling.GetScalingFactor() - 1) * 4)
            For Each llCur In CurWordApp.ActiveDocument.ListTemplates(xLT).ListLevels
                If llCur.Font.Size <> WdConstants.wdUndefined Then
                    'GLOG 8611: Add 1pt for each 25% increase in scaling
                    llCur.Font.Size = 20 + ((CScaling.GetScalingFactor() - 1) * 4)
                End If
            Next llCur
            If bAdjustLines Then
                AdjustLines()
            End If
        End Sub

        Public Function bIsZoomedOut() As Boolean
            bIsZoomedOut = (CurWordApp.ActiveWindow.View.Zoom.Percentage = ZoomPercentage)
        End Function

        Public Sub AdjustLines(Optional ByVal iLevel As Integer = 0)
            Dim rngTable As Word.Range
            Dim rngP As Word.Range
            Dim rngQ As Word.Range
            Dim paraP As Word.Paragraph
            Dim iAlignment As Integer
            Dim bMakeBlue As Boolean
            Dim bTabExists As Boolean
            Dim lDelEnd As Long
            Dim lDelStart As Long
            Dim iMove As Integer
            Dim iFont As Integer
            Dim llp As Word.ListLevel

            'GLOG 15971 (dm) - added optional parameter to specify a single level
            If iLevel > 0 Then
                rngTable = CurWordApp.ActiveDocument.Tables(1).Rows(iLevel).Range
            Else
                rngTable = CurWordApp.ActiveDocument.Tables(1).Range
            End If

            '   set font size
            If bIsZoomedOut() Then
                iFont = 20
            Else
                iFont = 12
            End If

            For Each paraP In rngTable.Paragraphs
                rngP = paraP.Range

                With rngP
                    bTabExists = (InStr(rngP.Text, vbTab))

                    If Not bTabExists Then
                        GoTo NextPara
                    End If

                    '           find first tab
                    .MoveStartUntil(vbTab)

                    '           exclude trailing para mark
                    If .Characters.Last.Text = vbCr Then
                        .MoveEnd(WdUnits.wdCharacter, -1)
                    End If

                    '           get whether to make blue
                    bMakeBlue = (.Font.ColorIndex = WdColorIndex.wdBlue)

                    '           delete all underlines
                    .Text = New String(vbTab, 24)

                    iAlignment = paraP.Alignment

                    '           if list level underlining is undefined, number will
                    '           be inadvertently underlined when we insert lines below;
                    '           since underlining will be defined anyway as soon as ANY change
                    '           is made to list level, we may as well do it now
                    With .ListFormat
                        llp = .ListTemplate.ListLevels(.ListLevelNumber)
                    End With
                    If llp.Font.Underline = WdConstants.wdUndefined Then _
                        llp.Font.Underline = wdunderline.wdUnderlineNone

                    '           insert lines
                    .Underline = WdUnderline.wdUnderlineSingle

                    '           set color on underlines
                    rngQ = rngP.Duplicate
                    rngQ.MoveEnd(WdUnits.wdCharacter, -1)

                    If bMakeBlue Then
                        rngQ.Font.ColorIndex = WdColorIndex.wdBlue
                    Else
                        rngQ.Font.ColorIndex = WdColorIndex.wdAuto
                    End If

                    .Expand(WdUnits.wdParagraph)

                    .Font.Size = iFont

                    '           get eo para
                    lDelEnd = .End

                    .Select()

                    '           delete all text after first two lines
                    CurWordApp.Selection.StartOf()
                    If InStr(rngP.Text, New String(Chr(11), 2)) Then
                        iMove = 3
                    Else
                        iMove = 2
                    End If

                    lRet = CurWordApp.Selection.Move(WdUnits.wdLine, iMove)
                    If CurWordApp.Selection.Information(wdinformation.wdAtEndOfRowMarker) Then _
                        CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)
                    lDelStart = CurWordApp.Selection.Start

                    If iAlignment <> WdParagraphAlignment.wdAlignParagraphJustify Then
                        '               delete one extra tab at start of range-
                        '               this will mimic left or center alignment
                        lDelStart = lDelStart - 1
                    End If

                    '           added in 9.8.1004 to prevent crashing in
                    '           ocr scanned documents in Word 2003
                    System.Windows.Forms.Application.DoEvents()

                    If lDelEnd > lDelStart Then
                        CurWordApp.ActiveDocument.Range(lDelStart, lDelEnd).Text = ""
                        If iAlignment = WdParagraphAlignment.wdAlignParagraphLeft Then
                            rngP.InsertAfter("__")
                        End If
                    End If
                End With
NextPara:
            Next paraP
            '   move insertion point out of the way
            CurWordApp.ActiveDocument.Paragraphs.Last.Range.Select()
            CurWordApp.ActiveWindow.VerticalPercentScrolled = 0
        End Sub

        Public Sub ArrangeWindows(docPreview As Word.Document, _
                                  docStart As Word.Document)
            'arranges Word windows to display preview
            'in the same location as the original document
            Dim sWindowHeight As Single
            Dim sWindowWidth As Single
            Dim iWindowState As Integer

            With docStart.CurWordApp.ActiveWindow
                sWindowHeight = .Height
                sWindowWidth = .Width
                iWindowState = .WindowState
            End With

            With docPreview.CurWordApp.ActiveWindow
                .WindowState = iWindowState
                If .WindowState = WdWindowState.wdWindowStateNormal Then
                    .Height = sWindowHeight
                    .Width = sWindowWidth
                    .Left = docStart.CurWordApp.ActiveWindow.Left
                    .Top = docStart.CurWordApp.ActiveWindow.Top
                End If
            End With

            With docStart.CurWordApp.ActiveWindow
                .WindowState = WdWindowState.wdWindowStateMinimize
            End With
        End Sub

        Public Sub RemoveHighlight()
            'removes shading from entire table
            On Error Resume Next
            With CurWordApp.ActiveDocument.Tables(1).Range.Shading
                .Texture = WdTextureIndex.wdTextureNone
                .ForegroundPatternColorIndex = WdColorIndex.wdAuto
                .BackgroundPatternColorIndex = WdColorIndex.wdAuto
            End With
        End Sub
    End Class
End Namespace

