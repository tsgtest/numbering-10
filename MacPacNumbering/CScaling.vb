﻿Imports System.Drawing
Public Class CScaling
    'returns the screen scaling factor
    Public Shared Function GetScalingFactor() As Single
        Dim g As System.Drawing.Graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero)
        Return g.DpiX / 96
    End Function
    ''' <summary>
    ''' Returns resized image based on current scaling factor
    ''' </summary>
    ''' <param name="oImage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ScaleImageForScreen(oImage As Image) As Bitmap
        If CScaling.GetScalingFactor() > 1 Then
            Return ResizeImage(oImage, oImage.Width * CScaling.GetScalingFactor(), oImage.Height * CScaling.GetScalingFactor())
        Else
            Return New Bitmap(oImage)
        End If
    End Function
    '''<summary>
    '''Resize the image to the specified width and height.
    '''</summary>
    '''<param name="image">The image to resize.</param>
    '''<param name="width">The width to resize to.</param>
    '''<param name="height">The height to resize to.</param>
    '''<returns>The resized image.</returns>
    Public Shared Function ResizeImage(oImage As System.Drawing.Image, iWidth As Integer, iHeight As Integer) As System.Drawing.Bitmap

        Dim oRect As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, iWidth, iHeight)
        Dim oNewImage As System.Drawing.Bitmap = New System.Drawing.Bitmap(iWidth, iHeight)

        oNewImage.SetResolution(oImage.HorizontalResolution, oImage.VerticalResolution)

        Using oGraphics As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(oNewImage)
            oGraphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy
            oGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality
            oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
            oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality
            oGraphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality
            Using owrapMode As New System.Drawing.Imaging.ImageAttributes()
                owrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY)
                oGraphics.DrawImage(oImage, oRect, 0, 0, oImage.Width, oImage.Height, System.Drawing.GraphicsUnit.Pixel, owrapMode)
            End Using
        End Using

        Return oNewImage
    End Function
End Class
