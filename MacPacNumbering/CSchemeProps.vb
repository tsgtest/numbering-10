Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlConversions
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords

Namespace LMP.Numbering
    Friend Class cSchemeProps
        '**********************************************************
        '   CSchemeProps Collection Class
        '   created 1/25/98 by Daniel Fisherman-
        '   momshead@earthlink.net

        '   Contains properties and methods that define the
        '   CSchemeProps object - SchemeProps are the scheme-
        '   wide properties that the user can set on use of a scheme

        '   Properties with "NP" prefix relate to scheme-specific
        '   next paragraph styles
        '**********************************************************

        Public Enum mnSchemeAlignments
            mnSchemeAlignments_UseScheme = -1
            mnSchemeAlignments_Left = 0
            mnSchemeAlignments_Justified = 3
            mnSchemeAlignments_Adjust = 4
        End Enum

        Public Enum mnSchemeLineSpacing
            mnSchemeLineSpacing_UseScheme = -1
            mnSchemeLineSpacing_1 = 0
            mnSchemeLineSpacing_15 = 1
            mnSchemeLineSpacing_2 = 2
        End Enum

        Public Enum mnSchemeIndents
            mnSchemeIndent_UseScheme = -1
            mnSchemeIndent_StepAndWrap = 0
            mnSchemeIndent_StepAndHang = 1
            mnSchemeIndent_Wrap = 2
            mnSchemeIndent_Hang = 3
        End Enum

        Private m_iAlignment As Integer
        Private m_iLineSpacing As Integer
        Private m_iIndents As Integer
        Private m_xParaFontName As String
        Private m_xNumFontName As String
        Private m_sParaFontSize As Single
        Private m_sNumFontSize As Single
        'Private m_iBeforeAfter As Integer
        Private m_iNPAlignment As Integer
        Private m_iNPLineSpacing As Integer
        Private m_iNPIndents As Integer
        Private m_xNPParaFontName As String
        Private m_sNPParaFontSize As Single
        Private m_xScheme As String
        Private m_sSpaceBefore As Single
        Private m_sSpaceAfter As Single
        Private m_sSpacingAt As Single

        'Private m_iNPBeforeAfter As Integer

        'Public Property Let BeforeAfter(iNew As mnSchemeBeforeAfter)
        '    m_iBeforeAfter = iNew
        'End Property
        '
        'Public Property Get BeforeAfter() As mnSchemeBeforeAfter
        '    BeforeAfter = m_iBeforeAfter
        'End Property

        Public Property Scheme As String
            Get
                Scheme = m_xScheme
            End Get
            Set(value As String)
                m_xScheme = value
            End Set
        End Property

        Public Property Alignment As mnSchemeAlignments
            Get
                Alignment = m_iAlignment
            End Get
            Set(value As mnSchemeAlignments)
                m_iAlignment = value
            End Set
        End Property

        Public Property LineSpacing As mnSchemeLineSpacing
            Get
                LineSpacing = m_iLineSpacing
            End Get
            Set(value As mnSchemeLineSpacing)
                m_iLineSpacing = value
            End Set
        End Property

        Public Property SpaceBefore As Single
            Get
                SpaceBefore = m_sSpaceBefore
            End Get
            Set(value As Single)
                m_sSpaceBefore = value
            End Set
        End Property

        Public Property SpaceAfter As Single
            Get
                SpaceAfter = m_sSpaceAfter
            End Get
            Set(value As Single)
                m_sSpaceAfter = value
            End Set
        End Property

        Public Property SpacingAt As Single
            Get
                SpacingAt = m_sSpacingAt
            End Get
            Set(value As Single)
                m_sSpacingAt = value
            End Set
        End Property

        Public Property Indent As mnSchemeIndents
            Get
                Indent = m_iIndents
            End Get
            Set(value As mnSchemeIndents)
                m_iIndents = value
            End Set
        End Property

        Public Property ParagraphFontName As String
            Get
                ParagraphFontName = m_xParaFontName
            End Get
            Set(value As String)
                m_xParaFontName = value
            End Set
        End Property

        Public Property ParagraphFontSize
            Get
                ParagraphFontSize = m_sParaFontSize
            End Get
            Set(value)
                m_sParaFontSize = value
            End Set
        End Property

        Public Property NumberFontName As String
            Get
                NumberFontName = m_xNumFontName
            End Get
            Set(value As String)
                m_xNumFontName = value
            End Set
        End Property

        Public Property NumberFontSize As Single
            Get
                NumberFontSize = m_sNumFontSize
            End Get
            Set(value As Single)
                m_sNumFontSize = value
            End Set
        End Property

        Public Property NPAlignment As mnSchemeAlignments
            Get
                NPAlignment = m_iNPAlignment
            End Get
            Set(value As mnSchemeAlignments)
                m_iNPAlignment = value
            End Set
        End Property

        Public Property NPLineSpacing As mnSchemeLineSpacing
            Get
                NPLineSpacing = m_iNPLineSpacing
            End Get
            Set(value As mnSchemeLineSpacing)
                m_iNPLineSpacing = value
            End Set
        End Property

        Public Property NPIndent As mnSchemeIndents
            Get
                NPIndent = m_iNPIndents
            End Get
            Set(value As mnSchemeIndents)
                m_iNPIndents = value
            End Set
        End Property

        Public Property NPParagraphFontName As String
            Get
                NPParagraphFontName = m_xNPParaFontName
            End Get
            Set(value As String)
                m_xNPParaFontName = value
            End Set
        End Property

        Public Property NPParagraphFontSize As Single
            Get
                NPParagraphFontSize = m_sNPParaFontSize
            End Get
            Set(value As Single)
                m_sNPParaFontSize = value
            End Set
        End Property

        Public Sub New()
            'GLOG 15789 (dm) - this was Class_initialize(), which doesn't get called in VB.Net
            '   without doc change code, macros could run before conversion
            '   of doc or or even initialization of app
            If g_xPNumSty = "" Then _
                bAppInitialize()
            If Not bIsConverted() Then _
                DoConversions()

            '   set default values for use without quick edit form display
            m_iAlignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme
            m_iLineSpacing = mnSchemeLineSpacing.mnSchemeLineSpacing_UseScheme
            m_iIndents = mnSchemeIndents.mnSchemeIndent_UseScheme
            m_iNPAlignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme
            m_iNPLineSpacing = mnSchemeLineSpacing.mnSchemeLineSpacing_UseScheme
            m_iNPIndents = mnSchemeIndents.mnSchemeIndent_UseScheme
        End Sub

        Public Function Execute() As Boolean
            'sets the properties of me.scheme
            'in CurWordApp.ActiveDocument to reflect the property settings in Me
            Dim i As Integer
            Dim iNumLevels As Integer
            Dim styP As Word.Style
            Dim styPrev As Word.Style
            Dim xStyle As String
            Dim iSpacing As Integer
            Dim bUseSchemeSpacing As Boolean
            Dim xDocBT As String
            Dim styBT As Word.Style
            Dim xStyleRoot As String
            Dim sTab As Single
            Dim xLT As String
            Dim bIsHScheme As Boolean
            Dim xPrev As String
            Dim sTabDef As Single
            Dim iStart As Integer
            Dim iEnd As Integer
            Dim sNumberPos As Single
            Dim xScheme As String
            Dim iTrailUnderline As Integer
            Dim iAlignment As Integer
            Dim iNormalAlign As WdParagraphAlignment
            Dim oLT As Word.ListTemplate
            Dim styC As Word.Style
            Dim iSchemes As Integer
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try

                '10/4/12 - user is no longer required to select scheme
                If Me.Scheme = "" Then
                    iSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                    If iSchemes = 0 Then
                        If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                            MsgBox("Aucun th�me de num�rotation TSG dans ce document.", vbInformation, g_xAppName)
                        Else
                            MsgBox("There are no TSG numbering schemes " & _
                                "in this document.", vbInformation, g_xAppName)
                        End If
                        Exit Function
                    ElseIf iSchemes = 1 Then
                        Me.Scheme = g_xDSchemes(0, 0)
                    Else
                        'force accelerator cues in Word 2013
                        If g_iWordVersion > mpWordVersions.mpWordVersion_2010 Then
                            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                            If lKeyboardCues = 0 Then
                                SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                                bRestoreNoCues = True
                            End If
                        End If

                        If g_lUILanguage = wdlanguageid.wdFrenchCanadian Then
                            Dim oForm As frmSchemeSelectorFrench
                            oForm = New frmSchemeSelectorFrench
                            oForm.ShowDialog()
                            If oForm.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                Me.Scheme = g_xDSchemes(oForm.lstSchemes.SelectedIndex, 0)
                            End If
                            oForm = Nothing
                        Else
                            Dim oForm As frmSchemeSelector
                            oForm = New frmSchemeSelector
                            oForm.ShowDialog()
                            If oForm.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                Me.Scheme = g_xDSchemes(oForm.lstSchemes.SelectedIndex, 0)
                            End If
                            oForm = Nothing
                        End If
                    End If

                    If Me.Scheme = "" Then
                        'turn off accelerator cues if necessary
                        If bRestoreNoCues Then _
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
                        Exit Function
                    End If
                End If

                CurWordApp.ScreenUpdating = False

                iNumLevels = iGetLevels(Me.Scheme, mpSchemeTypes.mpSchemeType_Document)
                xStyleRoot = xGetStyleRoot(Me.Scheme)
                xLT = xGetFullLTName(Me.Scheme)
                oLT = CurWordApp.ActiveDocument.ListTemplates(xLT)
                xScheme = xGetLTRoot(Me.Scheme)
                bIsHScheme = bIsHeadingScheme(xScheme)
                iNormalAlign = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal).ParagraphFormat.Alignment
                '    If FullParagraphSelected() Then
                '    End If

                For i = 1 To iNumLevels
                    With Me
                        '           adjust numbered paragraph style
                        If bIsHScheme Then
                            xStyle = xTranslateHeadingStyle(i)
                            xPrev = xTranslateHeadingStyle(i - 1)
                        Else
                            xStyle = xStyleRoot & "_L" & i
                            xPrev = xStyleRoot & "_L" & i - 1
                        End If
                        styP = CurWordApp.ActiveDocument.Styles(xStyle)

                        '           adjust alignment
                        If Not (.Alignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme And _
                                .NPAlignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme) Then
                            '               get level prop
                            iTrailUnderline = xGetLevelProp(xScheme, i, _
                                mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document)
                            If bBitwisePropIsTrue(iTrailUnderline, _
                                    mpTrailUnderlineFields.mpTrailUnderlineField_Underline) Then
                                iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_Underline
                            End If
                        End If
                        If .Alignment <> mnSchemeAlignments.mnSchemeAlignments_UseScheme Then
                            If .Alignment = mnSchemeAlignments.mnSchemeAlignments_Adjust Then
                                '                   implement specified alignment
                                '                   except on centered levels
                                If styP.ParagraphFormat.Alignment <> _
                                        WdParagraphAlignment.wdAlignParagraphCenter Then
                                    styP.ParagraphFormat.Alignment = iNormalAlign
                                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal
                                Else
                                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_HonorAlignment
                                End If
                            Else
                                '                   implement specified alignment
                                '                   except on centered levels
                                If styP.ParagraphFormat.Alignment <> _
                                        WdParagraphAlignment.wdAlignParagraphCenter Then
                                    styP.ParagraphFormat.Alignment = .Alignment
                                End If
                                iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_HonorAlignment
                            End If
                        End If

                        '           do fontname and size if specified
                        If .ParagraphFontName <> "" Then _
                            styP.Font.Name = .ParagraphFontName

                        If .ParagraphFontSize > 0 Then _
                            styP.Font.Size = .ParagraphFontSize

                        '           do fontname and size for number if specified
                        With oLT.ListLevels(i)
                            If Me.NumberFontName <> "" Then _
                                .Font.Name = Me.NumberFontName

                            If Me.NumberFontSize > 0 Then _
                                .Font.Size = Me.NumberFontSize
                        End With

                        '           do indent if specified
                        sTabDef = CurWordApp.ActiveDocument.DefaultTabStop
                        If sTabDef = 0 Then _
                            sTabDef = 36
                        Select Case .Indent
                            Case mnSchemeIndents.mnSchemeIndent_StepAndWrap
                                '                   set indents so that para is indented
                                '                   .5in more than previous level- then
                                '                   force text to wrap back to margin
                                With styP.ParagraphFormat
                                    If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or _
                                        .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                        If i = 1 Then
                                            .FirstLineIndent = 0
                                        Else
                                            styPrev = CurWordApp.ActiveDocument _
                                                .Styles(xPrev)
                                            If styPrev.ParagraphFormat _
                                                    .Alignment = WdParagraphAlignment.wdAlignParagraphCenter Then
                                                .FirstLineIndent = 0
                                            Else
                                                .FirstLineIndent = styPrev _
                                                    .ParagraphFormat.FirstLineIndent + sTabDef
                                            End If
                                        End If
                                        .LeftIndent = 0
                                        If .TabStops.Count Then
                                            .TabStops(1).Position = .FirstLineIndent + sTabDef
                                        Else
                                            .TabStops.Add(.FirstLineIndent + sTabDef)
                                        End If
                                        sNumberPos = .LeftIndent + .FirstLineIndent
                                        With oLT.ListLevels(i)
                                            .NumberPosition = sNumberPos
                                            .TextPosition = styP.ParagraphFormat.LeftIndent
                                            If styP.ParagraphFormat.TabStops.Count Then
                                                .TabPosition = styP.ParagraphFormat.TabStops(1).Position
                                            Else
                                                .TabPosition = sNumberPos + sTabDef
                                            End If
                                        End With
                                    End If
                                End With
                            Case mnSchemeIndents.mnSchemeIndent_StepAndHang
                                '                   set indents so that para is indented
                                '                   .5in more than previous level- then
                                '                   force a hanging indent
                                With styP.ParagraphFormat
                                    If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or _
                                        .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                        If i = 1 Then
                                            .LeftIndent = sTabDef
                                        Else
                                            styPrev = CurWordApp.ActiveDocument _
                                                .Styles(xPrev)
                                            .LeftIndent = styPrev _
                                                .ParagraphFormat.LeftIndent + sTabDef
                                        End If
                                        .FirstLineIndent = -sTabDef
                                    End If
                                    .TabStops.ClearAll()
                                    With oLT.ListLevels(i)
                                        .NumberPosition = styP.ParagraphFormat.LeftIndent +
                                            styP.ParagraphFormat.FirstLineIndent
                                        .TextPosition = styP.ParagraphFormat.LeftIndent
                                        .TabPosition = styP.ParagraphFormat.LeftIndent
                                    End With
                                End With
                            Case mnSchemeIndents.mnSchemeIndent_Wrap
                                '                   force text to wrap back to margin
                                With styP.ParagraphFormat
                                    If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or _
                                        .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                        sNumberPos = .LeftIndent + .FirstLineIndent
                                        .FirstLineIndent = sNumberPos
                                        .LeftIndent = 0
                                        With oLT.ListLevels(i)
                                            .NumberPosition = sNumberPos
                                            .TextPosition = 0
                                            If styP.ParagraphFormat.TabStops.Count Then
                                                .TabPosition = styP.ParagraphFormat.TabStops(1).Position
                                            Else
                                                .TabPosition = sNumberPos + sTabDef
                                            End If
                                        End With
                                    End If
                                End With
                            Case mnSchemeIndents.mnSchemeIndent_Hang
                                '                   force a hanging indent
                                With styP.ParagraphFormat
                                    'GLOG 5319 (9.9.5005) - avoid error if list level
                                    'tab position is undefined
                                    If oLT.ListLevels(i).TabPosition <> 9999999 Then
                                        sNumberPos = .LeftIndent + .FirstLineIndent
                                        If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or _
                                            .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                            With oLT.ListLevels(i)
                                                sTab = .TabPosition - sNumberPos
                                                If sTab <= 0 Then _
                                                    sTab = sTabDef
                                            End With
                                            .LeftIndent = sNumberPos + sTab
                                            .FirstLineIndent = -sTab
                                        End If
                                    End If
                                    '                        .TabStops.ClearAll
                                    With oLT.ListLevels(i)
                                        .NumberPosition = styP.ParagraphFormat.LeftIndent +
                                            styP.ParagraphFormat.FirstLineIndent
                                        .TextPosition = styP.ParagraphFormat.LeftIndent
                                        .TabPosition = styP.ParagraphFormat.LeftIndent
                                    End With
                                End With
                            Case Else
                                '                   do nothing - use scheme definition for each level
                        End Select

                        'ensure that left indent of para styles matches new left indent
                        'of numbered styles
                        If .Indent <> mnSchemeIndents.mnSchemeIndent_UseScheme Then _
                            UpdateParaStyles(xScheme)

                        '           adjust next paragraph style if possible
                        styC = Nothing
                        xStyle = xStyleRoot & " Cont " & i
                        Try
                            styC = CurWordApp.ActiveDocument.Styles(xStyle)
                        Catch
                        End Try
                        If styC Is Nothing Then _
                            GoTo labSetProp

                        '           do alignment for level if specified
                        If .NPAlignment <> mnSchemeAlignments.mnSchemeAlignments_UseScheme Then
                            If .NPAlignment = mnSchemeAlignments.mnSchemeAlignments_Adjust Then
                                '                   implement specified alignment
                                '                   except on centered levels
                                If styC.ParagraphFormat.Alignment <> _
                                        WdParagraphAlignment.wdAlignParagraphCenter Then
                                    styC.ParagraphFormat.Alignment = iNormalAlign
                                    iAlignment = iAlignment Or _
                                        mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal
                                End If
                            Else
                                '                   implement specified alignment
                                '                   except on centered levels
                                If styC.ParagraphFormat.Alignment <> _
                                        WdParagraphAlignment.wdAlignParagraphCenter Then
                                    styC.ParagraphFormat.Alignment = .NPAlignment
                                End If
                            End If
                        End If

                        '           do fontname and size if specified
                        If .NPParagraphFontName <> "" Then _
                            styC.Font.Name = .NPParagraphFontName

                        If .NPParagraphFontSize > 0 Then _
                            styC.Font.Size = .NPParagraphFontSize

                        '           do indent if specified
                        If .Indent <> mnSchemeIndents.mnSchemeIndent_UseScheme Then
                            With styP.ParagraphFormat
                                If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or _
                                        .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                    '                       align with text, not number
                                    styC.ParagraphFormat.LeftIndent = .LeftIndent
                                    styC.ParagraphFormat.RightIndent = .RightIndent
                                    If xGetLevelProp(xScheme, i, mpNumLevelProps.mpNumLevelProp_TrailChr, _
                                            mpSchemeTypes.mpSchemeType_Document) = mpTrailingChars.mpTrailingChar_Tab Then
                                        If .TabStops.Count = 0 Then
                                            '                               converted schemes may not have tab stops
                                            styC.ParagraphFormat.FirstLineIndent = oLT.ListLevels(i) _
                                                .TabPosition - .LeftIndent
                                        Else
                                            '                               use tab stop
                                            styC.ParagraphFormat.FirstLineIndent = .TabStops(1).Position - _
                                                .LeftIndent
                                        End If
                                    Else
                                        styC.ParagraphFormat.FirstLineIndent = .FirstLineIndent
                                    End If
                                End If
                            End With
                        End If

labSetProp:
                        '           set level prop
                        If Not (.Alignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme And _
                                .NPAlignment = mnSchemeAlignments.mnSchemeAlignments_UseScheme) Then
                            lSetLevelProp(xScheme, i, mpNumLevelProps.mpNumLevelProp_TrailUnderline, _
                                CStr(iAlignment), mpSchemeTypes.mpSchemeType_Document)
                        End If
                    End With
                Next i

                'clear undo list
                CurWordApp.ActiveDocument.UndoClear()

            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)

                CurWordApp.ScreenUpdating = True
            End Try
        End Function

        Public Function ChangeSchemeFonts()
            'GLOG 3688 (11/4/11): modified font size properties and code to
            'support franctional sizes
            Dim styNormal As Word.Style
            Dim oForm As System.Windows.Forms.Form
            Dim oFontDlg As frmSchemeFonts
            Dim oFontDlgFrench As frmSchemeFontsFrench
            Dim iSchemes As Integer
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean
            Dim bCancelled As Boolean

            Try
                'force accelerator cues in Word 2013
                If g_iWordVersion > mpWordVersions.mpWordVersion_2010 Then
                    lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                    If lKeyboardCues = 0 Then
                        SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                        bRestoreNoCues = True
                    End If
                End If

                '10/4/12 - user is no longer required to select scheme
                If Me.Scheme = "" Then
                    iSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                    If iSchemes = 0 Then
                        If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                            MsgBox("Aucun th�me de num�rotation TSG dans ce document.", vbInformation, g_xAppName)
                        Else
                            MsgBox("There are no TSG numbering schemes " & _
                                "in this document.", vbInformation, g_xAppName)
                        End If
                        'turn off accelerator cues if necessary
                        If bRestoreNoCues Then _
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
                        Exit Function
                    ElseIf iSchemes = 1 Then
                        Me.Scheme = g_xDSchemes(0, 0)
                    Else
                        If g_lUILanguage = wdlanguageid.wdFrenchCanadian Then
                            Dim oFormS As frmSchemeSelectorFrench
                            oFormS = New frmSchemeSelectorFrench
                            oFormS.ShowDialog()
                            If oFormS.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                Me.Scheme = g_xDSchemes(oFormS.lstSchemes.SelectedIndex, 0)
                            End If
                            oFormS = Nothing
                        Else
                            Dim oFormS As frmSchemeSelector
                            oFormS = New frmSchemeSelector
                            oFormS.ShowDialog()
                            If oFormS.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                Me.Scheme = g_xDSchemes(oFormS.lstSchemes.SelectedIndex, 0)
                            End If
                            oFormS = Nothing
                        End If
                    End If

                    If Me.Scheme = "" Then
                        'turn off accelerator cues if necessary
                        If bRestoreNoCues Then _
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
                        Exit Function
                    End If
                End If

                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    oForm = New frmSchemeFontsFrench
                    oForm.Text = " Changer propri�t�s de police pour th�me " & _
                        xGetStyleRoot(xGetLTRoot(Me.Scheme))
                    oFontDlgFrench = CType(oForm, frmSchemeFontsFrench)
                    oForm.ShowDialog()
                    bCancelled = oFontDlgFrench.Cancelled
                Else
                    oForm = New frmSchemeFonts
                    oForm.Text = " Change Font Properties for " & _
                        xGetStyleRoot(xGetLTRoot(Me.Scheme)) & " Scheme"
                    oFontDlg = CType(oForm, frmSchemeFonts)
                    oForm.ShowDialog()
                    bCancelled = oFontDlg.Cancelled
                End If

                If Not bCancelled Then
                    styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)

                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        With oFontDlgFrench
                            If .cmbNumFontName.SelectedIndex > 1 Then
                                Me.NumberFontName = .cmbNumFontName.Text
                            ElseIf .cmbNumFontName.SelectedIndex = 1 Then
                                Me.NumberFontName = styNormal.Font.Name
                            End If

                            If (.cmbNumFontSize.SelectedIndex > 1) Or _
                                    (.cmbNumFontSize.SelectedIndex < 0) Then
                                Me.NumberFontSize = CSng(.cmbNumFontSize.Text)
                            ElseIf .cmbNumFontSize.SelectedIndex = 1 Then
                                Me.NumberFontSize = styNormal.Font.Size
                            End If

                            If .cmbFontName.SelectedIndex > 1 Then
                                Me.ParagraphFontName = .cmbFontName.Text
                                Me.NPParagraphFontName = .cmbFontName.Text
                            ElseIf .cmbFontName.SelectedIndex = 1 Then
                                Me.ParagraphFontName = styNormal.Font.Name
                                Me.NPParagraphFontName = styNormal.Font.Name
                            End If

                            If .cmbFontSize.SelectedIndex > 1 Or _
                                    (.cmbFontSize.SelectedIndex < 0) Then
                                Me.ParagraphFontSize = CSng(.cmbFontSize.Text)
                                Me.NPParagraphFontSize = CSng(.cmbFontSize.Text)
                            ElseIf .cmbFontSize.SelectedIndex = 1 Then
                                Me.ParagraphFontSize = styNormal.Font.Size
                                Me.NPParagraphFontSize = styNormal.Font.Size
                            End If
                        End With
                    Else
                        With oFontDlg
                            If .cmbNumFontName.SelectedIndex > 1 Then
                                Me.NumberFontName = .cmbNumFontName.Text
                            ElseIf .cmbNumFontName.SelectedIndex = 1 Then
                                Me.NumberFontName = styNormal.Font.Name
                            End If

                            If (.cmbNumFontSize.SelectedIndex > 1) Or _
                                    (.cmbNumFontSize.SelectedIndex < 0) Then
                                Me.NumberFontSize = CSng(.cmbNumFontSize.Text)
                            ElseIf .cmbNumFontSize.SelectedIndex = 1 Then
                                Me.NumberFontSize = styNormal.Font.Size
                            End If

                            If .cmbFontName.SelectedIndex > 1 Then
                                Me.ParagraphFontName = .cmbFontName.Text
                                Me.NPParagraphFontName = .cmbFontName.Text
                            ElseIf .cmbFontName.SelectedIndex = 1 Then
                                Me.ParagraphFontName = styNormal.Font.Name
                                Me.NPParagraphFontName = styNormal.Font.Name
                            End If

                            If .cmbFontSize.SelectedIndex > 1 Or _
                                    (.cmbFontSize.SelectedIndex < 0) Then
                                Me.ParagraphFontSize = CSng(.cmbFontSize.Text)
                                Me.NPParagraphFontSize = CSng(.cmbFontSize.Text)
                            ElseIf .cmbFontSize.SelectedIndex = 1 Then
                                Me.ParagraphFontSize = styNormal.Font.Size
                                Me.NPParagraphFontSize = styNormal.Font.Size
                            End If
                        End With
                    End If
                    Me.Execute()
                End If
                oForm.Close()

                CurWordApp.ActiveWindow.SetFocus()
            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Function ChangeSchemeLineSpacing(ByVal xScheme As String) As Boolean
            'GLOG 5599 (9.9.6009) - removed unused parameters to eliminate
            'compile error on some machines in Word 2013
            Dim i As Integer
            Dim iLineSpacing As Integer
            Dim xStyle As String
            Dim xCont As String
            Dim xStyleRoot As String
            Dim stySchemeL As Word.Style
            Dim styCont As Word.Style
            Dim styNormal As Word.Style
            Dim pfNormal As Word.ParagraphFormat
            Dim bIsHScheme As Boolean
            Dim sBefore As Single
            Dim sAfter As Single
            Dim sNormalSpacing As Single
            Dim sAt As Single
            Dim frmLS As System.Windows.Forms.Form
            Dim xUseCurrent As String
            Dim iSchemes As Integer
            Dim oForm As System.Windows.Forms.Form
            Dim oLSDlg As frmLineSpacing
            Dim oLSDlgFrench As frmLineSpacingFrench
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try

                'force accelerator cues in Word 2013
                If g_iWordVersion > mpWordVersions.mpWordVersion_2010 Then
                    lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                    If lKeyboardCues = 0 Then
                        SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                        bRestoreNoCues = True
                    End If
                End If

                '10/4/12 - user is no longer required to select scheme
                If xScheme = "" Then
                    iSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                    If iSchemes = 0 Then
                        If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                            MsgBox("Aucun th�me de num�rotation TSG dans ce document.", vbInformation, g_xAppName)
                        Else
                            MsgBox("There are no TSG numbering schemes " & _
                                "in this document.", vbInformation, g_xAppName)
                        End If
                    ElseIf iSchemes = 1 Then
                        xScheme = g_xDSchemes(0, 0)
                    Else
                        If g_lUILanguage = wdlanguageid.wdFrenchCanadian Then
                            Dim oFormS As frmSchemeSelectorFrench
                            oFormS = New frmSchemeSelectorFrench
                            oFormS.ShowDialog()
                            If oFormS.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                xScheme = g_xDSchemes(oFormS.lstSchemes.SelectedIndex, 0)
                            End If
                            oFormS = Nothing
                        Else
                            Dim oFormS As frmSchemeSelector
                            oFormS = New frmSchemeSelector
                            oFormS.ShowDialog()
                            If oFormS.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                xScheme = g_xDSchemes(oFormS.lstSchemes.SelectedIndex, 0)
                            End If
                            oFormS = Nothing
                        End If
                    End If

                    If xScheme <> "" Then
                        Me.Scheme = xScheme
                    Else
                        'turn off accelerator cues if necessary
                        If bRestoreNoCues Then _
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
                        Exit Function
                    End If
                End If

                xStyleRoot = xGetStyleRoot(xScheme)
                bIsHScheme = bIsHeadingScheme(xGetLTRoot(xScheme))

                '   show dialog
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    frmLS = New frmLineSpacingFrench
                    oLSDlgFrench = CType(frmLS, frmLineSpacingFrench)
                    With oLSDlgFrench
                        .Text = " Changer l'espacement du th�me " & _
                                        xGetStyleRoot(xGetLTRoot(Me.Scheme))
                        xUseCurrent = "-Utiliser courant-"
                        .ShowDialog()
                        If .Cancelled Then
                            .Close()
                            CurWordApp.ActiveWindow.SetFocus()
                            Exit Function
                        End If
                        iLineSpacing = .cmbLineSpacing.SelectedIndex - 1
                        If .txtSpaceBefore.Text = xUseCurrent Then
                            sBefore = -1
                        Else
                            sBefore = .txtSpaceBefore.Text
                        End If
                        If .txtSpaceAfter.Text = xUseCurrent Then
                            sAfter = -1
                        Else
                            sAfter = .txtSpaceAfter.Text
                        End If
                        If .txtAt.Text <> "" Then
                            sAt = .txtAt.Text
                        Else
                            sAt = 0
                        End If
                    End With
                Else
                    frmLS = New frmLineSpacing
                    oLSDlg = CType(frmLS, frmLineSpacing)
                    With oLSDlg
                        .Text = " Change Line Spacing for " & _
                                        xGetStyleRoot(xGetLTRoot(Me.Scheme)) & _
                                        " Scheme"
                        xUseCurrent = "-Use Current-"
                        .ShowDialog()
                        If .Cancelled Then
                            .Close()
                            CurWordApp.ActiveWindow.SetFocus()
                            Exit Function
                        End If
                        iLineSpacing = .cmbLineSpacing.SelectedIndex - 1
                        If .txtSpaceBefore.Text = xUseCurrent Then
                            sBefore = -1
                        Else
                            sBefore = .txtSpaceBefore.Text
                        End If
                        If .txtSpaceAfter.Text = xUseCurrent Then
                            sAfter = -1
                        Else
                            sAfter = .txtSpaceAfter.Text
                        End If
                        If .txtAt.Text <> "" Then
                            sAt = .txtAt.Text
                        Else
                            sAt = 0
                        End If
                    End With
                End If

                'set properties
                Me.LineSpacing = iLineSpacing
                Me.SpaceAfter = sAfter
                Me.SpaceBefore = sBefore
                Me.SpacingAt = sAt

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                System.Windows.Forms.Application.DoEvents()
                EchoOff()

                styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
                pfNormal = styNormal.ParagraphFormat

                For i = 1 To 9
                    '       get styles to edit
                    If bIsHScheme Then
                        xStyle = xTranslateHeadingStyle(i)
                    Else
                        xStyle = xStyleRoot & "_L" & i
                    End If
                    xCont = xStyleRoot & " Cont" & " " & i

                    With CurWordApp.ActiveDocument.Styles
                        Try
                            stySchemeL = .Item(xStyle)
                        Catch
                        End Try
                        Try
                            styCont = .Item(xCont)
                        Catch
                        End Try
                    End With

                    If Not (stySchemeL Is Nothing) Then
                        Dim pfP As Word.ParagraphFormat
                        pfP = stySchemeL.ParagraphFormat

                        With pfP
                            '               line spacing
                            If iLineSpacing <> -1 Then
                                .LineSpacingRule = iLineSpacing
                                If iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast Or _
                                        iLineSpacing = WdLineSpacing.wdLineSpaceExactly Then
                                    .LineSpacing = sAt
                                ElseIf iLineSpacing = WdLineSpacing.wdLineSpaceMultiple Then
                                    .LineSpacing = CurWordApp.LinesToPoints(sAt)
                                End If
                            End If

                            '               space before/after
                            If sBefore <> -1 Then _
                                .SpaceBefore = sBefore
                            If sAfter <> -1 Then _
                                .SpaceAfter = sAfter

                            If Not styCont Is Nothing Then
                                '                   do for continued style -
                                '                   make same as level style
                                With styCont.ParagraphFormat
                                    If iLineSpacing <> -1 Then
                                        .LineSpacingRule = pfP.LineSpacingRule
                                        .LineSpacing = pfP.LineSpacing
                                    End If
                                    If sBefore <> -1 Then _
                                        .SpaceBefore = sBefore
                                    If sAfter <> -1 Then _
                                        .SpaceAfter = sAfter
                                End With
                            End If
                        End With
                    End If

                    stySchemeL = Nothing
                    styCont = Nothing
                Next i

                'clear undo list
                CurWordApp.ActiveDocument.UndoClear()

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                frmLS.Close()
                CurWordApp.ActiveWindow.SetFocus()
            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
                EchoOn()
            End Try
        End Function
    End Class
End Namespace



