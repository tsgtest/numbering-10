Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cConstants
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.CError

Namespace LMP.Numbering
    Friend Class cShare
        '**********************************************************
        '   CShare Collection Class
        '   created 1/25/98 by Daniel Fisherman-
        '   momshead@earthlink.net

        '   Contains properties and methods that define the
        '   CShare object
        '**********************************************************
        Private Structure SchemeProfile
            Shared PostedBy As String
            Shared PostedOn As Date
            Shared Description As String
            Shared Category As String
            Shared Owner As String
        End Structure

        Private Const mpStyleDoesNotExist As Integer = 5608

        Private m_xDir As String
        Private m_xScheme As String
        Private xMsg As String
        Private iUserChoice As Integer

        '**********************************************************
        'PROPERTIES
        '**********************************************************

        Public Property PublicDir As String
            Get
                If Right(m_xDir, 1) <> "\" Then
                    m_xDir = m_xDir & "\"
                End If
                PublicDir = m_xDir
            End Get
            Set(value As String)
                m_xDir = value
            End Set
        End Property


        Public Property Scheme As String
            Get
                Scheme = m_xScheme
            End Get
            Set(value As String)
                m_xScheme = value
            End Set
        End Property


        '**********************************************************
        'METHODS
        '**********************************************************

        Public Function ExportPublicScheme(xScheme As String, xDisplayName As String)
            'exports a scheme to a file in the shared schemes directory
            Dim dlgSave As Word.Dialog
            Dim lUserChoice As Long
            Dim xFile As String
            Dim xFileNameRoot As String
            Dim xFileExport As String
            Dim pflP As SchemeProfile
            Dim bRet As Boolean
            Dim xDir As String
            Dim xDestPath As String
            Dim xBitmap As String
            Dim oScheme As CNumScheme

            Try
                CurWordApp.WordBasic.DisableAutoMacros(1)

                With CurWordApp
                    '       validate accessibility of public dir
                    If Not PublicDirIsValid() Then
                        Exit Function
                    End If

                    '9.9.5001 - get existing description
                    oScheme = GetRecord(xScheme, mpSchemeTypes.mpSchemeType_Private)

                    '       prompt user for scheme profile
                    bRet = GetProfile(xDisplayName, oScheme.Description, pflP)

                    If bRet = 0 Then
                        Exit Function
                    End If

                    'Screen.ActiveForm.MousePointer = vbHourglass 'remmed for GLOG 5350

                    '       create blank doc
                    EchoOff()
                    .ScreenUpdating = False
                    If g_xBufferTemplate = "" Then
                        CurWordApp.Documents.Add()
                    Else
                        '9.9.3 - a template other than Normal has been specified in the ini
                        Try
                            CurWordApp.Documents.Add(g_xBufferTemplate)
                        Catch
                            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                                MsgBox("Le mod�le precise dans " & cNumTOC.SettingsFileName & " (" & g_xBufferTemplate & _
                                    ") est invalide.  Veuillez contacter votre administrateur syst�me.", vbCritical, g_xAppName)
                            Else
                                MsgBox("The template specified in " & cNumTOC.SettingsFileName & " (" & g_xBufferTemplate & _
                                    ") is invalid.  Please contact your system administrator.", vbCritical, g_xAppName)
                            End If
                            Exit Function
                        End Try
                    End If

                    '9.9.2010 - save file before copying styles to avoid native
                    'OrganizerCopy issue

                    '       we use slash placeholder in bitmap names
                    xBitmap = xSubstitute(xDisplayName, "/", "=")

                    '       save file - add a random number as prefix
                    '       to guarantee the uniqueness of the file name
                    Randomize()
                    xFileNameRoot = xBitmap & _
                            mpFilePrefixSep & (Mid(Rnd(), 3))
                    xDestPath = Me.PublicDir & pflP.Category & "\"

                    '       freeze screen to prevent user
                    '       from seeing the actual filename
                    '       in status bar during file save
                    EchoOff()
                    With CurWordApp.ActiveDocument
                        CurWordApp.ActiveWindow.View.ShowAll = False
                        .SaveAs(xDestPath & xFileNameRoot & ".mpn", _
                                WdSaveFormat.wdFormatTemplate, , , False)
                        ''            .Close wdDoNotSaveChanges
                        ''           iManage COM workaraound
                        '            .Saved = True
                        '            .Close
                    End With

                    '       save profile
                    SaveProfile(pflP, CurWordApp.ActiveDocument)

                    '       copy scheme into file - bitmap must also be copied
                    iCopySchemeFromSty(xScheme, xDisplayName)

                    '       mark the list template as the one to import
                    CurWordApp.ActiveDocument.CustomDocumentProperties.Add( _
                                            "ExportScheme", _
                                            False, _
                                            MsoDocProperties.msoPropertyTypeString, _
                                            xScheme)

                    '9.9.2010 - resave and close file
                    With CurWordApp.ActiveDocument
                        .Save()
                        .Close()
                    End With

                    '9.9.2010 - remove from recent files list
                    With CurWordApp.RecentFiles
                        If .Count Then
                            With .Item(1)
                                If .Name = xFileNameRoot & ".mpn" Then _
                                    .Delete()
                            End With
                        End If
                    End With

                    '       refresh screen
                    EchoOn()
                    CurWordApp.ScreenUpdating = True
                    CurWordApp.ScreenRefresh()

                    '       copy bitmap file to shared schemes directory
                    FileCopy(g_xMPBPath & "\" & xBitmap & ".mpb", _
                             xDestPath & xFileNameRoot & ".mpb")

                    '       cleanup
                    '        Screen.ActiveForm.MousePointer = vbDefault 'remmed for GLOG 5350
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        MsgBox("Exportation r�ussie avec succ�s.", vbInformation, g_xAppName)
                    Else
                        MsgBox("Export was successful.", vbInformation, g_xAppName)
                    End If
                End With
            Finally
                CurWordApp.ScreenUpdating = True
                EchoOn()
                CurWordApp.WordBasic.DisableAutoMacros(0)
            End Try
        End Function
        Public Function ImportPublicScheme(ByVal arrSelSchemes As String()) As String
            'prompt for scheme(s) - present dlg with list of shared schemes
            'copy scheme/bitmap to .sty/numbering dir -
            'RETURNS imported scheme name
            Dim i As Integer
            Dim iPos As Integer
            Dim iSchemes As Integer
            Dim xDetail As String
            Dim xFile As String
            Dim oScheme As cNumScheme
            Dim oSource As Object
            Dim xScheme As String
            Dim ltP As Word.ListTemplate
            Dim xLevel1 As String

            Try
                EchoOff()

                '   validate accessibility of public dir
                If Not PublicDirIsValid() Then
                    Exit Function
                End If

                CurWordApp.WordBasic.DisableAutoMacros(1)

                Try
                    iSchemes = UBound(arrSelSchemes) + 1
                Catch
                End Try
                If iSchemes Then
                    For i = LBound(arrSelSchemes) To UBound(arrSelSchemes)
                        '           load import file
                        xFile = Me.PublicDir & arrSelSchemes(i)
                        CurWordApp.AddIns.Add(xFile, True)
                        oSource = GetTemplate(xFile)

                        With oSource.CustomDocumentProperties
                            '               get scheme to import
                            Try
                                xScheme = .Item("ExportScheme").value
                            Catch
                            End Try
                            Try
                                xLevel1 = .Item(xScheme & "1").Value
                            Catch
                            End Try

                            If xLevel1 <> "" Then
                                Dim xIsSource As String
                                Try
                                    xIsSource = .Item("MPN90SourceFile").value
                                Catch
                                End Try
                                If xIsSource = "" Then
                                    '                   mark file as TSG numbering source file-
                                    '                   this tells the code to look in doc props
                                    '                   for level props of the scheme
                                    .Add("MPN90SourceFile", _
                                         False, _
                                         MsoDocProperties.msoPropertyTypeBoolean, _
                                         True)
                                End If
                            Else
                                .Item("MPN90SourceFile").Delete()
                            End If
                        End With

                        '           alert and exit if no valid
                        '           TSG listtemplate was found
                        If Len(xScheme) = 0 Then
                            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                                xMsg = "Le th�me ne peut pas �tre import�.  Le fichier " & xFile & " semble corrompu. Veuillez contacter votre administrateur."
                            Else
                                xMsg = "The scheme could not be imported.  " & _
                                       "The file " & xFile & " appears to be corrupted. " & _
                                       "Please contact your administrator."
                            End If
                            MsgBox(xMsg, vbExclamation, g_xAppName)
                            CurWordApp.AddIns(xFile).Delete()
                            Exit Function
                        End If

                        '           retrieve scheme details from this scheme
                        oScheme = GetRecord(xScheme, , oSource)

                        '           import
                        ImportPublicScheme = ImportScheme(oScheme, xFile, True)

                        '           remove source file
                        Try
                            CurWordApp.AddIns(xFile).Delete()
                        Catch
                        End Try
                    Next i
                End If
            Finally
                CurWordApp.WordBasic.DisableAutoMacros(0)
                EchoOn()
            End Try
        End Function
        '
        '
        'Public Function ImportPublicScheme_OLD() As String
        ''prompt for scheme(s) - present dlg with list of shared schemes
        ''copy scheme/bitmap to .sty/numbering dir -
        ''RETURNS imported scheme name
        '    Dim arrSelSchemes() As String
        '    Dim i As Integer
        '    Dim iPos As Integer
        '    Dim iSchemes As Integer
        '    Dim xDetail As String
        '    Dim xFile As String
        '    Dim oScheme As CNumScheme
        '    Dim oSource As Object
        '    Dim xScheme As String
        '    Dim ltP As Word.ListTemplate
        '
        '    #If compHandleErrors Then
        '        On Error GoTo ProcError
        '    #End If
        '
        '    EchoOff
        '
        ''   validate accessibility of public dir
        '    If Not PublicDirIsValid() Then
        '        Exit Function
        '    End If
        '
        '    CurWordApp.WordBasic.DisableAutoMacros 1
        '
        ''   prompt for scheme(s) to import
        '    GetPublicSchemes arrSelSchemes
        '
        '    On Error Resume Next
        '    iSchemes = UBound(arrSelSchemes) + 1
        '    #If compHandleErrors Then
        '        On Error GoTo ProcError
        '    #Else
        '        On Error GoTo 0
        '    #End If
        '
        '    If iSchemes Then
        '        For i = LBound(arrSelSchemes) To UBound(arrSelSchemes)
        ''           load import file
        '            xFile = Me.PublicDir & arrSelSchemes(i)
        '            Word.AddIns.Add xFile, True
        '            Set oSource = Word.Templates(xFile)
        '
        ''           get scheme to import
        '            xScheme = oSource.CustomDocumentProperties("ExportScheme")
        '
        ''           alert and exit if no valid
        ''           macpac listtemplate was found
        '            If Len(xScheme) = 0 Then
        '                xMsg = "The scheme could not be imported.  " & _
        '                       "The file " & xFile & " appears to be corrupted. " & _
        '                       "Please contact your administrator."
        '                MsgBox xMsg, vbExclamation, g_xAppName
        '                Word.AddIns(xFile).Delete
        '                Exit Function
        '            End If
        '
        ''           retrieve scheme details from this scheme
        '            Set oScheme = GetRecord(xScheme, , oSource)
        '
        ''           import
        '            ImportPublicScheme = ImportScheme(oScheme, _
        '                                              xFile, _
        '                                              True)
        '            DoEvents
        '        Next i
        '    End If
        '    CurWordApp.WordBasic.DisableAutoMacros 0
        '    EchoOn
        '    Exit Function
        'ProcError:
        '    EchoOn
        '    RaiseError "CShare.ImportPublicScheme"
        '    Exit Function
        'End Function

        Public Function ImportFileScheme() As String
            ''prompt for file - file open
            ''copy scheme to .sty - copy bitmap
            ''RETURNS imported scheme name
            '    Dim xScheme As String
            '    Dim xDisplay As String
            '    Dim xSource As String
            '    Dim iRet As Integer
            '    Dim bShowAll As Boolean
            '    Dim bShowGridlines As Boolean
            '    Dim wndP As Word.Window
            '    Dim xDetail As String
            '    Dim xPreviewCaption As String
            '    Dim iPos As Integer
            '    Dim oScheme As CNumScheme
            '    Dim bIsExport As Boolean
            '
            '    #If compHandleErrors Then
            '        On Error GoTo ProcError
            '    #End If
            '
            '    CurWordApp.WordBasic.DisableAutoMacros 1
            '
            '    Set wndP = Word.CurWordApp.ActiveWindow
            '
            ''   get current show all state
            '    bShowAll = wndP.View.ShowAll
            '    bShowGridlines = wndP.View.TableGridlines
            '
            '    Word.CurWordApp.ScreenUpdating = False
            '
            ''   turn off show all, so the import doc
            ''   preview doesn't show all
            '    wndP.View.ShowAll = False
            '    wndP.View.TableGridlines = False
            '
            ''   open import doc
            '    On Error Resume Next
            '    EchoOn
            '    g_dlgScheme.Visible = False
            '    Word.CurWordApp.ScreenUpdating = True
            '    CurWordApp.Run "FileOpen"
            '    g_dlgScheme.Visible = True
            '    Word.CurWordApp.ScreenUpdating = False
            '
            '    #If compHandleErrors Then
            '        On Error GoTo ProcError
            '    #End If
            '
            '    If Not (wndP Is Word.CurWordApp.ActiveWindow) Then
            '        EchoOn
            '        With CurWordApp
            '            .ScreenUpdating = True
            '            EchoOff
            '            .ScreenUpdating = False
            '        End With
            '
            ''       return original window show all state-
            ''       window will longer exist if it was
            ''       the first window in the Word session,
            ''       so ignore error
            '        On Error Resume Next
            '        wndP.View.ShowAll = bShowAll
            '        wndP.View.TableGridlines = bShowGridlines
            '
            '        #If compHandleErrors Then
            '            On Error GoTo ProcError
            '        #End If
            '
            '        With CurWordApp.ActiveDocument
            '            On Error Resume Next
            ''           test for valid file type - ie Export File
            '            bIsExport = .Variables("zzmpExportFile")
            '
            '            If Not bIsExport Then
            ''               alert user that this is not a valid file
            '                xMsg = "The selected document is not a " & _
            '                       "MacPac Numbering Export File.  " & vbCr & _
            '                       "If the document contains a MacPac numbering " & _
            '                       "scheme that you wish to use, " & vbCr & _
            '                       "open the document, and create a new scheme " & _
            '                       "based on the scheme."
            '                MsgBox xMsg, vbExclamation, g_xAppName
            '                .Close wdDoNotSaveChanges
            '            Else
            ''               test for valid scheme name
            '                xScheme = .Variables(mpActiveSchemeDocVar)
            '
            ''               attempt to get display name from 'Scheme1' doc prop - this
            ''               doc prop is inserted into an export file by code.
            '                Set oScheme = GetRecord(xScheme, , CurWordApp.ActiveDocument)
            '
            '                #If compHandleErrors Then
            '                    On Error GoTo ProcError
            '                #End If
            '
            '                If Len(oScheme.Name) Then
            '                    xPreviewCaption = "'" & oScheme.Alias & "' Scheme"
            '                    Word.CurWordApp.ActiveWindow.Caption = xPreviewCaption
            '
            ''                   import scheme from active document
            '                    ImportFileScheme = ImportScheme( _
            '                           oScheme, .FullName)
            '                Else
            ''                   alert user to no valid scheme to import
            '                    xMsg = "Could not import a scheme.   " & _
            '                           "The selected document " & _
            '                           "contains no MacPac numbering schemes."
            '                    MsgBox xMsg, vbExclamation, g_xAppName
            '                End If
            '
            '                If Word.Windows.Count Then
            '                    If Word.CurWordApp.ActiveWindow.Caption = xPreviewCaption Then
            ''                       close import doc
            '                        .Close wdDoNotSaveChanges
            '                    End If
            '                Else
            '                    Word.Documents.Add
            '                End If
            '            End If
            '        End With 'CurWordApp.ActiveDocument
            '    End If
            '
            '    CurWordApp.WordBasic.DisableAutoMacros 0
            '    EchoOn
            '    Word.CurWordApp.ScreenUpdating = True
            '    Exit Function
            'ProcError:
            '    EchoOn
            '    Word.CurWordApp.ScreenUpdating = True
            '    RaiseError "CShare.ImportFileScheme"
            '    Exit Function
        End Function

        '**********************************************************
        'INTERNAL PROCEDURES
        '**********************************************************
        Private Function ImportScheme(ByVal oScheme As cNumScheme, _
                                      xSourceFile As String, _
                                      Optional bIsPublicScheme As Boolean = False) As String
            'imports a scheme from a source file -
            'RETURNS imported scheme name
            Dim xBitmap As String
            Dim bStyleNameValid As Boolean
            Dim bSchemeNameValid As Boolean
            Dim bRenameScheme As Boolean
            Dim xMsg As String
            Dim iUserChoice As Integer
            Dim docSource As Word.Document
            Dim iPos As Integer
            Dim oSchemeNew As CNumScheme
            Dim oSource As Object
            Dim oPreview As New cPreview
            Dim sDlgTop As Single
            Dim oForm As System.Windows.Forms.Form
            Dim oDlg As frmNameScheme
            Dim oDlgFrench As frmNameSchemeFrench

            oSchemeNew = New cNumScheme
            oSource = GetTemplate(xSourceFile)

            '   save as template if source
            '   file is not a template
            If oSource Is Nothing Then
                With CurWordApp.ActiveDocument
                    .SaveAs(.FullName, WdSaveFormat.wdFormatTemplate, , , False)
                End With
                oSource = GetTemplate(xSourceFile)
            End If

            '   use values from source scheme for
            '   default values of imported scheme
            With oSchemeNew
                .Name = oScheme.Name
                .DisplayName = oScheme.DisplayName
                .Origin = oScheme.Origin
                .SchemeType = oScheme.SchemeType
                .TOCScheme = oScheme.TOCScheme
                'truncate description to 120 characters
                .Description = Left$(oScheme.Description, 120)
            End With

            '   test validity of name and.DisplayName
            bStyleNameValid = bStyleNameIsValid(oSchemeNew.Name, True, False)
            bSchemeNameValid = bSchemeNameIsValid(oSchemeNew.DisplayName, True, False)

            xMsg = ""
            iUserChoice = vbOK

            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                If Not (bStyleNameValid Or bSchemeNameValid) Then
                    xMsg = "Veuillez entrer un nouveau nom et nom de style pour ce th�me.  D'autres th�mes priv�s utilisent actuellement ce nom dans un th�me."
                ElseIf Not bStyleNameValid Then
                    xMsg = "Veuillez entrer un nouveau nom de style pour ce th�me.  Un autre th�me priv� existe d�j� avec ce nom."
                ElseIf Not bSchemeNameValid Then
                    If Len(oScheme.DisplayName) Then
                        xMsg = "Veuillez entrer un nouveau nom pour ce th�me.  Un autre th�me priv� existe d�j� avec ce nom."
                    Else
                        xMsg = "Veuillez entrer un nom pour ce th�me. Aucun th�me de ce nom n'a �t� trouv�."
                    End If
                End If
            Else
                If Not (bStyleNameValid Or bSchemeNameValid) Then
                    xMsg = "Please enter a new name and " & _
                           "style name for this scheme. " & _
                           "Other private schemes currently " & _
                           "use the names proposed for this scheme."
                ElseIf Not bStyleNameValid Then
                    xMsg = "Please enter a new style name for this " & _
                           "scheme.  Another private scheme " & _
                           "with this name already exists."
                ElseIf Not bSchemeNameValid Then
                    If Len(oScheme.DisplayName) Then
                        xMsg = "Please enter a new name for " & _
                               "this scheme. Another private scheme " & _
                               "with this name already exists."
                    Else
                        xMsg = "Please enter a name for this scheme.  " & _
                               "No valid name could be found."
                    End If
                End If
            End If

            bRenameScheme = Len(xMsg)

            If bRenameScheme Then
                '       display rename
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    oForm = New frmNameSchemeFrench
                    oForm.Text = " Renommer le Th�me import�"
                    oDlgFrench = CType(oForm, frmNameSchemeFrench)
                    With oDlgFrench
                        '           setup dlg before showing
                        .lblMessage.Text = xMsg
                        .txtName.Text = xGetStyleRoot(oSchemeNew.Name)
                        .txtAlias.Text = oSchemeNew.DisplayName
                        .txtName.Enabled = Not bStyleNameValid
                        If Not bStyleNameValid Then
                            .txtName.SelectionStart = 0
                            .txtName.SelectionLength = Len(.txtName.Text)
                        Else
                            .txtAlias.SelectionStart = 0
                            .txtAlias.SelectionLength = Len(.txtAlias.Text)
                        End If

                        '           show rename dlg
                        .ShowDialog()

                        '           get new values
                        oSchemeNew.Name = mpPrefix & Trim(.txtName.Text)
                        oSchemeNew.DisplayName = Trim(.txtAlias.Text)
                    End With
                Else
                    oForm = New frmNameScheme
                    oForm.Text = " Rename the Imported Scheme"
                    oDlg = CType(oForm, frmNameScheme)
                    With oDlg
                        '           setup dlg before showing
                        .lblMessage.Text = xMsg
                        .txtName.Text = xGetStyleRoot(oSchemeNew.Name)
                        .txtAlias.Text = oSchemeNew.DisplayName
                        .txtName.Enabled = Not bStyleNameValid
                        If Not bStyleNameValid Then
                            .txtName.SelectionStart = 0
                            .txtName.SelectionLength = Len(.txtName.Text)
                        Else
                            .txtAlias.SelectionStart = 0
                            .txtAlias.SelectionLength = Len(.txtAlias.Text)
                        End If

                        '           show rename dlg
                        .ShowDialog()

                        '           get new values
                        oSchemeNew.Name = mpPrefix & Trim(.txtName.Text)
                        oSchemeNew.DisplayName = Trim(.txtAlias.Text)
                    End With
                End If

                '           exit if cancelled
                If oForm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                    oForm.Close()
                    Exit Function
                End If

                oForm.Close()

                '           rename scheme if name was not
                '           valid - rename dlg guarantees that
                '           new name is valid
                If Not bStyleNameValid Then
                    RenameScheme(oScheme.Name, _
                                 xSourceFile, _
                                 oSchemeNew.Name, _
                                 oSchemeNew.DisplayName)
                End If
            End If

            '    Screen.MousePointer = vbHourglass 'remmed for GLOG 5350

            EchoOn()
            CurWordApp.ScreenRefresh()
            CurWordApp.ScreenUpdating = False
            EchoOff()

            '   copy scheme in sty file
            iCopySchemeToSty(oSchemeNew, xSourceFile, True)

            If bIsPublicScheme Then
                '       copy bitmap from Public Dir
                xBitmap = Left(xSourceFile, Len(xSourceFile) - 3) & "mpb"
                xBitmap = xSubstitute(xBitmap, "/", "=")
                FileCopy(xBitmap, g_xMPBPath & "\" & _
                         xSubstitute(oSchemeNew.DisplayName, "/", "=") & ".mpb")

                On Error Resume Next
                CurWordApp.AddIns(Me.PublicDir & xSourceFile).Delete()
                On Error GoTo 0
            Else
                '       create a bitmap
                If docSource Is Nothing Then
                    '           open source filen read only
                    docSource = CurWordApp.Documents.Open(xSourceFile, , True, False)
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        docSource.CurWordApp.ActiveWindow.Caption = _
                            "Importation du Th�me " & oSchemeNew.DisplayName
                    Else
                        docSource.CurWordApp.ActiveWindow.Caption = _
                            "Importing " & oSchemeNew.DisplayName & " Scheme"
                    End If
                End If

                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    sDlgTop = frmSchemesFrench.ActiveForm.Top
                    frmSchemesFrench.ActiveForm.Top = 20000
                Else
                    sDlgTop = frmSchemes.ActiveForm.Top
                    frmSchemes.ActiveForm.Top = 20000
                End If

                System.Windows.Forms.Application.DoEvents()
                oPreview.CreateBitmap(oSchemeNew.Name, _
                                      oSchemeNew.DisplayName)

                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    frmSchemesFrench.ActiveForm.Top = sDlgTop
                Else
                    frmSchemes.ActiveForm.Top = sDlgTop
                End If

                System.Windows.Forms.Application.DoEvents()
                oPreview = Nothing
            End If

            '   close rename doc if renamed
            If bRenameScheme Or Not bIsPublicScheme Then
                '        CurWordApp.ActiveDocument.Close wdDoNotSaveChanges
                '       iManage COM workaround
                With CurWordApp.ActiveDocument
                    .Saved = True
                    .Close()
                End With
            End If

            'save personal sty
            'workaround for Word 2010 to avoid error 5986 -
            '"this command is not available in an unsaved document"
            g_oPNumSty.OpenAsDocument.Save()
            CurWordApp.ActiveDocument.Close(False)

            ImportScheme = oSchemeNew.Name
            iGetSchemes(g_xPSchemes, mpSchemeTypes.mpSchemeType_Private)
        End Function

        Public Sub New()
            Dim xDir As String

            '   get shared schemes directory
            xDir = GetAppSetting("Numbering", "SharedDirectory")
            If xDir <> "" Then
                xDir = GetEnvironVarPath(xDir)
                If Right(xDir, 1) <> "\" Then _
                    xDir = xDir & "\"
            End If

            Me.PublicDir = xDir
        End Sub

        Private Function iCopySchemeFromSty(xScheme As String, xDisplayName As String) As Integer
            'copies the specified scheme from
            'personal tsgNumbers.sty to active document
            Dim xSource As String
            Dim xDest As String
            Dim rngLocation As Word.Range
            Dim oAutoText As Word.AutoTextEntry
            Dim xMsg As String

            On Error GoTo ProcError

            '   prompt for status
            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                xMsg = "Cr�ation di fichier Exportation. Veuillez patienter..."
            Else
                xMsg = "Creating Export File.  Please wait..."
            End If
            If Not g_dlgScheme Is Nothing Then
                '        g_dlgScheme.sbStatus.SimpleText = xMsg
            Else
                CurWordApp.StatusBar = xMsg
            End If

            With CurWordApp
                .ScreenUpdating = False
                xSource = g_xPNumSty

                '       ensure both source and dest are addins
                .AddIns(g_xPNumSty).Installed = True

                '       use CurWordApp.WordBasic function to bypass ODMA based return
                xDest = CurWordApp.WordBasic.FileName$()
            End With

            '   copy scheme elements - ie styles, listtemplate
            iCopySchemeElements(xScheme, g_xPNumSty, xDest, True)

            With CurWordApp
                '***********************************************
                'Inserting autotext can unname list templates in Word XP; since it's not
                'clear why we're doing this at all, I removed the code in 9.7.3
                '***********************************************
                ''       test for existence of
                ''       preview autotext entry
                '        On Error Resume Next
                '        Set oAutoText = .Templates(g_xPNumSty) _
                '            .AutoTextEntries(xScheme & "Preview")
                '        On Error GoTo ProcError
                '
                ''       copy preview autotext
                '        If Not (oAutoText Is Nothing) Then
                '            oAutoText.Insert _
                '                CurWordApp.ActiveDocument.Range(0, 0), True
                '            Set rngLocation = CurWordApp.ActiveDocument.Content
                '            With rngLocation
                '                .EndOf
                '                .InsertAfter String(80, vbCr)
                '                .Style = wdStyleNormal
                '                .EndOf
                '            End With
                '
                '            With CurWordApp.ActiveDocument.Windows(1).View
                '                .Zoom.Percentage = 41
                '                .ShowAll = False
                '            End With
                '        End If

                If Not g_dlgScheme Is Nothing Then
                    '            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    '                g_dlgScheme.sbStatus.SimpleText = "Pr�te"
                    '            Else
                    '                g_dlgScheme.sbStatus.SimpleText = "Ready"
                    '            End If
                Else
                    .StatusBar = ""
                    SendShiftKey()
                End If
            End With
            Exit Function

ProcError:
            Select Case Err.Number
                Case Else
                    MsgBox(Err.Number & ":" & Err.Description)
            End Select
            Exit Function
        End Function

        Private Function iCopySchemeToSty(ByVal oScheme As cNumScheme, _
                                          ByVal xSourceFile As String, _
                                          Optional ByVal bCopyLT As Boolean = False) As Integer
            'copies specified scheme from source to personal numbers.sty
            '9.9.5001 - xScheme parameter changed to oScheme, since description is now required
            Dim xMsg As String

            On Error GoTo ProcError
            With CurWordApp
                '       prompt for status
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    xMsg = "Importation du Th�me.  Veuillez patienter..."
                Else
                    xMsg = "Importing Scheme.  Please wait..."
                End If
                If Not g_dlgScheme Is Nothing Then
                    '            g_dlgScheme.sbStatus.SimpleText = xMsg
                Else
                    .StatusBar = xMsg
                End If

                '       ensure both source and dest are loaded addins
                .AddIns.Add(g_xPNumSty, True)
                .AddIns.Add(xSourceFile, True)
            End With

            '   copy scheme elements
            iCopySchemeElements(oScheme.Name, _
                                xSourceFile, _
                                g_xPNumSty, _
                                bCopyLT)

            '9.9.5001 - add doc prop for description
            SetField(oScheme.Name, mpRecordFields.mpRecField_Description, _
                oScheme.Description, mpSchemeTypes.mpSchemeType_Private)

            '   unload source and dest from add-ins if they
            '   are not tsgNumbers.sty file
            GetTemplate(xSourceFile).Saved = True
            CurWordApp.AddIns(xSourceFile).Delete()
            If Not g_dlgScheme Is Nothing Then
                '        g_dlgScheme.sbStatus.SimpleText = ""
            Else
                CurWordApp.StatusBar = ""
                SendShiftKey()
            End If
            Exit Function

ProcError:
            Select Case Err.Number
                Case Else
                    MsgBox(Err.Number & ":" & Err.Description)
            End Select
            Exit Function
        End Function

        Public Function iCopySchemeElements(ByVal xScheme As String, _
                                             ByVal xSourceFile As String, _
                                             ByVal xDestFile As String, _
                                             Optional ByVal bCopyLT As Boolean = False) As Integer
            'copies all necessary styles/props from source to dest

            Dim xNumStyle As String
            Dim xContStyle As String
            Dim i As Integer
            Dim xStyleRoot As String
            Dim ltScheme As Word.ListTemplate
            Dim j As Integer

            On Error GoTo ProcError

            xStyleRoot = xGetStyleRoot(xScheme)

            '   cycle through cont styles, copying twice to ensure presence
            '   of next paragraph styles
            '   9.9.4008 (GLOG 4962) - added error handling to avoid a silent exit before
            '   copying the other scheme elements when there are less than nine cont levels
            For j = 1 To 2
                On Error Resume Next
                For i = 1 To 9
                    xContStyle = xStyleRoot & " Cont " & i
                    '           do num continued styles
                    CurWordApp.OrganizerCopy(xSourceFile, _
                                                   xDestFile, _
                                                   xContStyle, _
                                                   WdOrganizerObject.wdOrganizerObjectStyles)
                    If Err.Number = mpErrors.mpStyleDoesNotExist Then
                        Exit For
                    ElseIf Err.Number > 0 Then
                        GoTo ProcError
                    End If
                Next i
                On Error GoTo ProcError
            Next j

            '   cycle through num styles, copying twice to ensure presence
            '   of next paragraph styles
            For j = 1 To 2
                On Error Resume Next
                For i = 1 To 9
                    xNumStyle = xStyleRoot & "_L" & i
                    '           copy styles into active document
                    CurWordApp.OrganizerCopy(xSourceFile, _
                                                   xDestFile, _
                                                   xNumStyle, _
                                                   WdOrganizerObject.wdOrganizerObjectStyles)
                Next i
                On Error GoTo CopyLT
            Next j

CopyLT:
            '   Word 10 patch - ensure named LT
            If xDestFile <> g_xPNumSty Then
                On Error Resume Next
                ltScheme = CurWordApp.ActiveDocument.ListTemplates(xScheme)
                On Error GoTo ProcError
                If ltScheme Is Nothing Then
                    ltScheme = CurWordApp.ActiveDocument.ListTemplates.Add(True)
                    ltScheme.Name = xScheme
                End If
            End If

            '   copy level props
            CopyLevelProps(xScheme, xSourceFile, xDestFile)

            '   Word 10 patch - relink scheme
            If xDestFile <> g_xPNumSty Then
                '       temporarily mark file as MacPac numbering source file-
                '       this will ensure that bRelinkScheme gets the level count
                '       from the custom props, not the LT name
                CurWordApp.ActiveDocument.CustomDocumentProperties.Add("MPN90SourceFile",
                     False, MsoDocProperties.msoPropertyTypeBoolean, True)
                bRelinkScheme(xScheme, mpSchemeTypes.mpSchemeType_Document, False, False, False)
                CurWordApp.ActiveDocument.CustomDocumentProperties("MPN90SourceFile").Delete()
            End If

            If bCopyLT Then
                '       copy list template
                iCopyListTemplate(xScheme, _
                                  xSourceFile, _
                                  xDestFile)
            End If

            Exit Function

ProcError:
            Select Case Err.Number
                Case mpStyleDoesNotExist
                    '           return last level loaded
                    CurWordApp.StatusBar = ""
                    SendShiftKey()
                    iCopySchemeElements = i
                Case Else
                    MsgBox(Err.Number & ":" & Err.Description)
            End Select
            Exit Function
        End Function

        Private Function GetProfile(ByVal xScheme As String, ByVal xDescription As String, _
            pflP As SchemeProfile) As Boolean
            'creates a profile for a scheme and saves it using document properties -
            'returns false if user cancelled profile
            Dim oForm As System.Windows.Forms.Form
            Dim oDlg As frmSchemeProfile
            Dim oDlgFrench As frmSchemeProfileFrench

            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                oForm = New frmSchemeProfileFrench
                oDlgFrench = CType(oForm, frmSchemeProfileFrench)

                'fill out profile for shared scheme
                With oDlgFrench
                    .lblSchemeText.Text = xScheme
                    .txtDescription.Text = xDescription
                    .ShowDialog()
                    CurWordApp.ScreenUpdating = True
                    If .DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                        pflP.Description = .txtDescription.Text
                        pflP.PostedBy = .txtPostedBy.Text
                        pflP.PostedOn = Date.Now()
                        pflP.Category = .cbxCategories.Text
                        GetProfile = True
                    Else
                        GetProfile = False
                    End If
                    .Close()
                End With
            Else
                oForm = New frmSchemeProfile
                oDlg = CType(oForm, frmSchemeProfile)

                'fill out profile for shared scheme
                With oDlg
                    .lblSchemeText.Text = xScheme
                    .txtDescription.Text = xDescription
                    .ShowDialog()
                    CurWordApp.ScreenUpdating = True
                    If .DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                        pflP.Description = .txtDescription.Text
                        pflP.PostedBy = .txtPostedBy.Text
                        pflP.PostedOn = Date.Now()
                        pflP.Category = .cbxCategories.Text
                        GetProfile = True
                    Else
                        GetProfile = False
                    End If
                    .Close()
                End With
            End If
        End Function

        Private Function SaveProfile(proP As SchemeProfile, docP As Word.Document)
            Dim dpsP As Object
            Dim xUserID As String

            '   get domain user name - used for security-
            '   only owner of scheme or Admin can delete the scheme
            xUserID = Environment.UserName()

            dpsP = docP.CustomDocumentProperties
            dpsP.Add("Description", False, msoDocProperties.msoPropertyTypeString, proP.Description)
            dpsP.Add("PostedBy", False, msoDocProperties.msoPropertyTypeString, proP.PostedBy)
            dpsP.Add("PostedOn", False, MsoDocProperties.msoPropertyTypeDate, proP.PostedOn)
            dpsP.Add("Owner", False, msoDocProperties.msoPropertyTypeString, xUserID)
        End Function

        Function iCopyListTemplate(xScheme As String, _
                                   xSource As String, _
                                   xDest As String) As Integer
            '   copies all list levels of xScheme from xSource to xDest
            Dim ltSource As ListTemplate
            Dim llSource As ListLevel
            Dim ltDest As ListTemplate
            Dim llDest As ListLevel
            Dim i As Integer
            Dim oTemplate As Word.Template
            Dim xStyleRoot As String

            Try
                oTemplate = GetTemplate(xDest)
            Catch
            End Try

            Try
                ltSource = GetTemplate(xSource).ListTemplates(xScheme)
                Try
                    If (oTemplate Is Nothing) Then
                        ltDest = CurWordApp.Documents(xDest).ListTemplates(xScheme)
                    Else
                        ltDest = GetTemplate(xDest).ListTemplates(xScheme)
                    End If
                Catch
                End Try

                If ltDest Is Nothing Then
                    '       list template does not exist - create
                    If (oTemplate Is Nothing) Then
                        ltDest = CurWordApp.Documents(xDest).ListTemplates.Add(True)
                    Else
                        ltDest = GetTemplate(xDest).ListTemplates.Add(True)
                    End If
                    ltDest.Name = xScheme
                End If

                xStyleRoot = xGetStyleRoot(xScheme)

                '   copy all levels
                For i = 1 To 9
                    llSource = ltSource.ListLevels(i)
                    llDest = ltDest.ListLevels(i)
                    '       exit if we've exceeded the number
                    '       of levels in the scheme
                    If Len(llSource.LinkedStyle) = 0 Then
                        Exit For
                    End If
                    iCopyListLevel(xScheme, llSource, llDest)
                    Try
                        llDest.LinkedStyle = xStyleRoot & "_L" & i
                    Catch
                    End Try
                Next i
            Catch e As System.Exception
                Select Case Err.Number
                    Case mpStyleDoesNotExist
                        '           return last level copied
                        iCopyListTemplate = i
                    Case Else
                        Throw e
                End Select
            End Try
        End Function

        Private Function RenameScheme(ByVal xScheme As String, _
                                      ByVal xSourceFile As String, _
                                      ByVal xName As String, _
                                      ByVal xDisplay As String)
            '   renames xScheme in file xSourceFile
            '   to xName and xDisplay
            Dim docSource As Word.Document
            Dim iLevels As Integer

            CurWordApp.ScreenUpdating = False
            '   freeze screen - set environment options
            EchoOff()
            With CurWordApp.ActiveWindow
                .View.Type = WdViewType.wdNormalView
                .View.ShowAll = False
                .View.TableGridlines = False
            End With

            '   open source file read only
            On Error Resume Next
            CurWordApp.WordBasic.DisableAutoMacros(1)
            docSource = CurWordApp.Documents.Open(xSourceFile, , True)
            On Error GoTo 0

            If docSource Is Nothing Then
                '       alert - there was a problem opening the file
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    xMsg = "Ne peut ouvrir le fichier � importer. Le fichier peut �tre en utilisation, le fichier est manquant ou l'acc�s r�seau n'est pas disponible."
                Else
                    xMsg = "Could not open the file to import. Someone may " & _
                        "be using this file already, the file is missing, " & _
                        "or there is a network access problem."
                End If
                MsgBox(xMsg, vbExclamation, g_xAppName)
                Exit Function
            End If

            '    If Not g_dlgScheme Is Nothing Then
            '        If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
            '            g_dlgScheme.sbStatus.SimpleText = "Importation du Th�me.  Veuillez patienter..."
            '        Else
            '            g_dlgScheme.sbStatus.SimpleText = "Importing Scheme.  Please wait..."
            '        End If
            '    End If

            '   setup window
            With CurWordApp.ActiveWindow
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    .Caption = "Th�me " & xDisplay
                Else
                    .Caption = xDisplay & " Scheme"
                End If
                .View.Type = WdViewType.wdNormalView
                .View.ShowAll = False
            End With

            '   show window
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()

            iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)
            bRenameExistingScheme(xName, xScheme, iLevels)

            '    On Error Resume Next
            SetField(xName, mpRecordFields.mpRecField_Alias, xDisplay, mpSchemeTypes.mpSchemeType_Document)

            '   clean dirty file - the file will be
            '   closed by a caller function
            docSource.Saved = True
        End Function

        Function bMarkForNoTrailer()
            'prevents a macpac trailer from running -
            'what to do about other packages, whose
            'trailers might run on open
            CurWordApp.ActiveDocument.Sections(1) _
                .Footers(1).Range.Text = _
                    "***No Trailer - DO NOT delete***"
        End Function

        Public Function DeletePublicScheme(xPath As String) As Long
            Dim oAddIn As Word.AddIn
            Dim oTemplate As Word.Template
            Dim xUserID As String
            Dim xOwner As String
            Dim xAdminID As String

#If compHandleErrors Then
        On Error GoTo ProcError
#Else
            On Error GoTo 0
#End If

            '   validate accessibility of public dir
            If Not PublicDirIsValid() Then
                Exit Function
            End If

            '   ensure that shared scheme is an add-in
            '   add as template
            CurWordApp.AddIns.Add(xPath, True)
            oTemplate = GetTemplate(xPath)

            '   test for ownership or admin acct -
            xUserID = Environment.UserName()

            '   get doc properties
            xOwner = oTemplate.CustomDocumentProperties.Item("Owner").Value

            If xUserID <> xOwner Then
                '       user is not owner of scheme - check if user is admin
                xAdminID = GetUserSetting("Numbering", "AdminID")
                If xUserID <> xAdminID Then
                    '           user is not admin either - prevent deletion
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        xMsg = "Vous n'avez pas les droits pour supprimer ce th�me. Seulement la personne qui a affich� ce th�me et votre administrateur peuvent supprimer ce th�me."
                    Else
                        xMsg = "You do not have permission to delete this scheme.  " & vbCr & _
                            "Only the person who posted the scheme and your " & _
                            "administrator can delete this scheme."
                    End If
                    MsgBox(xMsg, vbCritical, g_xAppName)
                    DeletePublicScheme = mpnErrors.mpError_NoSharedSchemeDeletePermission
                    Exit Function
                End If
            End If

            '   remove as add-in
            CurWordApp.AddIns(xPath).Delete()

            '   delete mpn
            Kill(xPath)

            '   delete mpb
            Kill(Left(xPath, Len(xPath) - 1) & "b")

            Exit Function

ProcError:
            DeletePublicScheme = Err.Number
            Exit Function
        End Function

        Private Function PublicDirIsValid() As Boolean
            'returns TRUE only if public directory is valid
            Dim xDir As String
            Dim xMsg As String

            If Len(Me.PublicDir) = 0 Then
                '       alert and exit if no shared schemes
                '       folder has been specified.
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    xMsg = "Aucun dossier de th�mes partag�s pr�cise. Veuillez contacter votre administrateur."
                Else
                    xMsg = "No Shared Schemes folder has been specified.  " & _
                           "Please contact your adminstrator."
                End If
                MsgBox(xMsg, vbCritical, g_xAppName)
                Exit Function
            End If

            '   validate shared schemes directory
            xDir = Dir(Me.PublicDir, vbDirectory)
            If Len(xDir) = 0 Then
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    xMsg = "Le dossier Th�me partag� " & _
                           "'" & Me.PublicDir & "' n'est pas disponible. Veuillez contacter votre administrateur."
                Else
                    xMsg = "The Shared Schemes folder " & _
                           "'" & Me.PublicDir & "' is not accessible.  " & _
                           "Please contact your administrator."
                End If
                MsgBox(xMsg, vbCritical, g_xAppName)
                Exit Function
            End If

            PublicDirIsValid = True
        End Function

        Private Sub CopyLevelProps(ByVal xScheme As String, _
                                   xSource As String, _
                                   xDest As String)
            'copies all level props of scheme xScheme
            'from oSource to oDest
            '9.9.2009 - moved from mpBase to take advantage of cShare.GetTemplate(),
            'which works around native bug in Word 2007 SP 2
            Dim i As Integer
            Dim xProp As String
            Dim xValue As String
            Dim oDest As Object
            Dim oSource As Object
            Dim xLT As String

            '   get source file
            On Error Resume Next
            oSource = GetTemplate(xSource)
            On Error GoTo 0
            If oSource Is Nothing Then
                oSource = CurWordApp.Documents(xSource)
            End If

            '   get destination file
            On Error Resume Next
            oDest = GetTemplate(xDest)
            On Error GoTo 0
            If oDest Is Nothing Then
                oDest = CurWordApp.Documents(xDest)
            End If

            On Error Resume Next
            '   do levels 1 to 9
            For i = 1 To 9
                xValue = ""
                '       get source prop name and value
                xProp = xScheme & i
                xValue = oSource.CustomDocumentProperties(xProp)
                If xValue = "" Then
                    xLT = xGetFullLTName(xScheme)
                    xValue = xGetPropFld(oSource.ListTemplates(xLT), i)
                End If
                With oDest
                    If (.CustomDocumentProperties(xProp) Is Nothing) Then
                        '               prop does not exist in dest - create
                        .CustomDocumentProperties.Add( _
                            xProp, False, msoDocProperties.msoPropertyTypeString, xValue)
                    Else
                        '               prop exists in dest - edit value
                        .CustomDocumentProperties(xProp).Value = xValue
                    End If
                End With
            Next i
            On Error GoTo 0
        End Sub

        Private Function xGetPropFld(ltScheme As Word.ListTemplate, _
                                     iLevel As Integer) As String
            '9.9.2009 - moved from mpBase to take advantage of cShare.GetTemplate(),
            'which works around native bug in Word 2007 SP 2
            Dim oSource As Object
            Dim bIsSourceFile As Boolean
            Dim iPos As Integer
            Dim iNext As Integer
            Dim xName As String
            Dim i As Integer

            oSource = ltScheme.Parent.Parent

            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            xName = ltScheme.Name

            If bIsSourceFile Then
                iPos = InStr(xName, "|")
                If iPos > 0 Then _
                    xName = Left(xName, iPos - 1)
                xGetPropFld = oSource.CustomDocumentProperties(xName & iLevel).Value
            Else
                '       find starting point of level prop
                For i = 1 To iLevel
                    iPos = InStr(iPos + 1, xName, "||")
                Next i

                '       find starting point of next level prop
                iNext = InStr(iPos + 1, xName, "||")

                '       parse between start and end points -
                '       include starting and end pipe
                xGetPropFld = Mid(xName, iPos + 1, _
                             iNext - iPos)
            End If
        End Function
    End Class
End Namespace


