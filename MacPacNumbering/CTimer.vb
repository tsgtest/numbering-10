Option Explicit On

Namespace LMP.Numbering
    Friend Class cTimer
        '**********************************************************
        '   CTimer Class
        '   created 9/28/99 by Daniel Fisherman-
        '   momshead@earthlink.net

        '   Contains properties and methods that define a timer.
        '   This object is similar to the VB Timer Object
        '   except that it does not need to be hosted by a form.
        '**********************************************************
        Private Declare Function SetTimer Lib "user32" ( _
            ByVal hwnd As Long, ByVal nIDEvent As Long, ByVal uElapse As Long, _
            ByVal lpTimerFunc As Long) As Long
        Private Declare Function KillTimer Lib "user32" ( _
            ByVal hwnd As Long, ByVal nIDEvent As Long) As Long

        Private g_lTimerID As Long

        Public Function Start(lInterval As Long, lFunctionAddress As Long)
            g_lTimerID = SetTimer(0, 0, lInterval, lFunctionAddress)
        End Function

        Public Function Halt()
            If g_lTimerID Then
                KillTimer(0, g_lTimerID)
            End If
        End Function
        Private Sub Class_Terminate()
            Me.Halt()
        End Sub
    End Class
End Namespace

