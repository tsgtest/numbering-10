﻿Imports System.Windows.Forms

Namespace LMP.Forte.MSWord
    Friend Class Clipboard
        Private Structure ClipboardContents
            Dim Data As Object
            Dim Format As String
        End Structure
        Shared m_aContents() As ClipboardContents
        Public Sub New()
        End Sub
        Public Shared Sub SaveClipboard()
            Dim i As Integer
            Dim aFormats As System.Collections.ArrayList
            Dim aAllFormats As String()
            Dim oClipObj As System.Windows.Forms.DataObject
            Dim iRetries As Integer
            ClearArray()
            On Error GoTo ProcError
            With My.Computer.Clipboard
                oClipObj = Nothing
                iRetries = 0
                While iRetries < 20
                    On Error Resume Next
                    oClipObj = .GetDataObject()
                    If Err.Number <> 0 Then
                        iRetries = iRetries + 1
                        System.Threading.Thread.Sleep(250)
                    Else
                        Exit While
                    End If
                End While
                If Not oClipObj Is Nothing Then
                    On Error GoTo ProcError
                    aFormats = New System.Collections.ArrayList()
                    aAllFormats = oClipObj.GetFormats(False)
                    For i = 0 To aAllFormats.GetUpperBound(0)
                        Select Case aAllFormats(i)
                            'Limit handled formats to subset of standard Windows Forms types
                            'In particular, attempting GetData with EnhancedMetafile format will
                            'result in an untrappable error
                            Case DataFormats.Bitmap, DataFormats.CommaSeparatedValue, DataFormats.Dib, DataFormats.Dif,
                                DataFormats.FileDrop, DataFormats.Html, DataFormats.Locale, DataFormats.MetafilePict,
                                DataFormats.OemText, DataFormats.Riff, DataFormats.Rtf, DataFormats.StringFormat,
                                DataFormats.SymbolicLink, DataFormats.Text, DataFormats.Tiff, DataFormats.UnicodeText,
                                DataFormats.WaveAudio
                                aFormats.Add(aAllFormats(i))
                        End Select
                    Next
                    ReDim m_aContents(aFormats.Count - 1)
                    For i = 0 To aFormats.Count - 1
                        m_aContents(i).Format = aFormats.Item(i)
                        m_aContents(i).Data = .GetData(aFormats.Item(i))
                    Next

                End If
            End With
ProcError:
            Err.Clear()
        End Sub

        Public Shared Sub RestoreClipboard()
            On Error GoTo ProcError
            Dim oDataObj As System.Windows.Forms.DataObject
            Dim iRetries As Integer
            oDataObj = Nothing
            If (IsArray(m_aContents)) Then
                oDataObj = New System.Windows.Forms.DataObject()
                For i = 0 To m_aContents.GetUpperBound(0)
                    'The EnhancedMetafile format should have been skipped by SaveClipboard,
                    'but exclude here just in case, since attempting to set
                    'can result in an untrappable error
                    If UCase(m_aContents(i).Format) <> "ENHANCEDMETAFILE" Then
                        oDataObj.SetData(m_aContents(i).Format, m_aContents(i).Data)
                    End If
                Next
            End If
            With My.Computer.Clipboard
                Dim i As Integer
                iRetries = 0
                While iRetries < 20
                    On Error Resume Next
                    .Clear()
                    If Err.Number <> 0 Then
                        iRetries = iRetries + 1
                        System.Threading.Thread.Sleep(250)
                    Else
                        Exit While
                    End If
                End While
                If Not oDataObj Is Nothing Then
                    iRetries = 0
                    While iRetries < 20
                        On Error Resume Next
                        .SetDataObject(oDataObj)
                        If Err.Number <> 0 Then
                            iRetries = iRetries + 1
                            System.Threading.Thread.Sleep(100)
                        Else
                            Exit While
                        End If
                    End While
                End If
                ClearArray()
            End With
            Exit Sub
ProcError:
            Err.Clear()
        End Sub
        Private Shared Sub ClearArray()
            If IsArray(m_aContents) Then
                For i = 0 To m_aContents.GetUpperBound(0)
                    m_aContents(i).Data = Nothing
                    m_aContents(i).Format = ""
                Next
            End If
            ReDim m_aContents(-1)
        End Sub
    End Class
End Namespace
