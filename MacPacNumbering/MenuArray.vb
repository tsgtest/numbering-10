﻿Imports System.IO
Imports System.Windows.Forms

Public Class MenuArray
    Inherits System.Collections.CollectionBase
    Public Event Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Private ReadOnly m_oMenu As System.Windows.Forms.ToolStripMenuItem

    Public Sub New(ByVal oMenu As System.Windows.Forms.ToolStripMenuItem)
        m_oMenu = oMenu
    End Sub

    Default Public ReadOnly Property Item(ByVal Index As Integer) As System.Windows.Forms.ToolStripMenuItem
        Get
            Return CType(Me.List.Item(Index), System.Windows.Forms.ToolStripMenuItem)
        End Get
    End Property

    Public Function AddNewItem(ByVal xText As String) As System.Windows.Forms.ToolStripMenuItem
        Dim oItem As New System.Windows.Forms.ToolStripMenuItem
        oItem.Text = xText
        oItem.Tag = Me.List.Count
        AddHandler (oItem.Click), AddressOf ClickHandler
        Me.List.Add(oItem)
        m_oMenu.DropDownItems.Insert(Me.List.Count - 1, oItem)
        Return oItem
    End Function

    Public Sub ClickHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent Click(sender, e)
    End Sub
End Class

Public Class HelpMenuArray
    Inherits System.Collections.CollectionBase
    Public Event Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Private ReadOnly m_oMenu As System.Windows.Forms.ToolStripMenuItem

    Public Sub New(ByVal oMenu As System.Windows.Forms.ToolStripMenuItem, ByVal xRootFolder As String)
        Dim oFolder As DirectoryInfo

        m_oMenu = oMenu
        oFolder = New DirectoryInfo(xRootFolder)
        LoadItems(oMenu, oFolder)
    End Sub

    Default Public ReadOnly Property Item(ByVal Index As Integer) As System.Windows.Forms.ToolStripMenuItem
        Get
            Return CType(Me.List.Item(Index), System.Windows.Forms.ToolStripMenuItem)
        End Get
    End Property

    Public Function AddNewItem(ByVal oParent As ToolStripMenuItem, ByVal xPath As String, ByVal xText As String) As ToolStripMenuItem
        Dim oItem As New ToolStripMenuItem
        Dim iIndex As Integer

        oItem.Text = xText
        oItem.Tag = xPath

        If Path.HasExtension(xPath) Then 'GLOG 8716
            AddHandler (oItem.Click), AddressOf ClickHandler
        End If

        Me.List.Add(oItem)
        If (oParent.Text = "&Help") Or (oParent.Text = "&Aide") Then
            'add above Help|About
            iIndex = oParent.DropDownItems.Count - 2
        Else
            'add to bottom
            iIndex = oParent.DropDownItems.Count
        End If
        oParent.DropDownItems.Insert(iIndex, oItem)
        Return oItem
    End Function

    Public Sub ClickHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent Click(sender, e)
    End Sub

    Private Sub LoadItems(ByVal oParent As ToolStripMenuItem, ByVal oDir As DirectoryInfo)
        Dim xDirPath As String
        Dim xDirName As String
        Dim xFiles As String()
        Dim xMenu As String
        Dim xName As String
        Dim iPos As Integer
        Dim i As Integer
        Dim oSubDir As DirectoryInfo
        Dim xPath As String
        Dim xExt As String
        Dim oMenuItem As ToolStripMenuItem

        xDirPath = oDir.FullName
        xDirName = oDir.Name
        xFiles = Directory.GetFiles(xDirPath, "*.*?", SearchOption.TopDirectoryOnly)

        'GLOG : 8716 : ceh
        oParent.Enabled = xFiles.Count

        Array.Sort(xFiles)

        For Each oSubDir In oDir.GetDirectories()
            'make sure ids need unique names, use folder name plus an incrementer, 
            'in case subfolders have duplicate names
            'remove illegal chars from id.
            oMenuItem = AddNewItem(oParent, oSubDir.FullName, oSubDir.Name)
            LoadItems(oMenuItem, oSubDir)
        Next oSubDir

        For Each xPath In xFiles
            If (xPath.EndsWith(".css")) Then Continue For

            xName = Path.GetFileNameWithoutExtension(xPath)
            xExt = Path.GetExtension(xPath).ToLower()

            'GLOG : 8349 : ceh - find last occurrence
            'don't truncate filename if the only underscore is the first character
            iPos = xName.LastIndexOf("_")
            If iPos > 0 Then
                xName = xName.Substring(0, iPos)
            End If

            'Ignore temp files in directory
            'GLOG 4445 (dm) - get only html files -
            'GLOG item #3818 - expanded to doc/pdf files as well
            If (Not xName.StartsWith("~")) And ((xExt = ".html") Or (xExt = ".doc") Or (xExt = ".docx") Or (xExt = ".pdf")) Then
                AddNewItem(oParent, xPath, xName)
            End If
        Next xPath
    End Sub
End Class