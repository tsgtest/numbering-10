﻿Imports MacPacNumbering.LMP.Numbering.UserFunctions_Numbering
Imports LMP.Numbering.TOC
Imports System.Runtime.InteropServices
Imports System.Drawing
Imports LMP.Numbering.Base.cNumTOC
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports LMP

Namespace LMP.Numbering
    <ComVisible(True)>
    <Guid("9316BFBF-0D4B-4C1F-BC31-28570E9FF210")>
    Public Interface iPublicFunctions
        Sub ChangeToDocumentScheme(xScheme As String, bSuppressMessages As Boolean)
        Sub DeleteScheme(xScheme As String, bSuppressMessages As Boolean)
        Sub Reinitialize()
        Sub SetScheme()
        Sub UseScheme(xScheme As String, bReset As Boolean)
        Sub InsertTableOfContents()
        Sub UnderlineHeadingToggle()
        Sub MarkHeading()
        Sub PromoteToNumberedStyle()
        Sub DemoteToContStyle()
        Sub PromoteLevel()
        Sub DemoteLevel()
        Sub ActivateScheme()
        Sub MarkAll()
        Sub ContinueFromPrevious()
        Sub EditScheme()
        Sub MarkAndFormatHeading()
        Sub RestartAt()
        Sub RemoveNumbers()
        Sub UnmarkForTOC()
        Sub SetSchemeInternal()
        Sub Level1Insert()
        Sub Level2Insert()
        Sub Level3Insert()
        Sub Level4Insert()
        Sub Level5Insert()
        Sub Level6Insert()
        Sub Level7Insert()
        Sub Level8Insert()
        Sub Level9Insert()
        Sub DecrementTOCTabs()
        Sub IncremenetTOCTabs()
        Sub SelectScheme()
    End Interface

    <ComVisible(True)>
    <Guid("54994054-0205-40D7-AFE0-A7FE08593F68"), ClassInterface(ClassInterfaceType.None)>
    Public Class PublicFunctions
        Implements iPublicFunctions
        Private Shared m_oRibbon As Microsoft.Office.Core.IRibbonUI

        Public Shared Function GetScreenScalingFactor() As Single
            'returns the current screen scaling factor
            Dim g As Graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero)
            Return g.DpiX / 96
        End Function

        Public Shared Property TSGNumberingRibbon As Microsoft.Office.Core.IRibbonUI
            Get
                TSGNumberingRibbon = m_oRibbon
            End Get
            Set(value As Microsoft.Office.Core.IRibbonUI)
                m_oRibbon = value
            End Set
        End Property

        Public Sub ChangeToDocumentScheme(xScheme As String, bSuppressMessages As Boolean) Implements iPublicFunctions.ChangeToDocumentScheme
            Try
                zzmpChangeToDocumentScheme(xScheme, bSuppressMessages)
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub DeleteScheme(xScheme As String, bSuppressMessages As Boolean) Implements iPublicFunctions.DeleteScheme
            Try
                zzmpDeleteScheme(xScheme, bSuppressMessages)
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Reinitialize() Implements iPublicFunctions.Reinitialize
            Try
                zzmpReinitialize()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub SetScheme() Implements iPublicFunctions.SetScheme
            Try
                zzmpSetSchemeWithoutInsertion()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub UseScheme(xScheme As String, bReset As Boolean) Implements iPublicFunctions.UseScheme
            Try
                zzmpUseScheme(xScheme, bReset)
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub InsertTableOfContents() Implements iPublicFunctions.InsertTableOfContents
            Try
                UserFunctions_TOC.zzmpTableOfContentsInsert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub UnderlineHeadingToggle() Implements iPublicFunctions.UnderlineHeadingToggle
            Try
                zzmpUnderLineHeadingToggle()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub MarkHeading() Implements iPublicFunctions.MarkHeading
            Try
                UserFunctions_TOC.zzmpMarkHeading()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub PromoteToNumberedStyle() Implements iPublicFunctions.PromoteToNumberedStyle
            Try
                zzmpPromoteToNumberedStyle()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub DemoteToContStyle() Implements iPublicFunctions.DemoteToContStyle
            Try
                zzmpDemoteToContStyle()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub PromoteLevel() Implements iPublicFunctions.PromoteLevel
            Try
                zzmpPromoteLevel()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub DemoteLevel() Implements iPublicFunctions.DemoteLevel
            Try
                zzmpDemoteLevel()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub ActivateScheme() Implements iPublicFunctions.ActivateScheme
            Dim xScheme As String
            Dim xMsg As String

            Try
                If Not Ribbon.bUnprotectedDocIsOpen() Then _
                    Exit Sub

                xScheme = Ribbon.xGetScheme()

                If Len(xScheme) Then
                    zzmpActivateScheme(CurWordApp.ActiveDocument, xScheme)
                    Try
                        m_oRibbon.Invalidate()
                    Catch
                    End Try
                End If
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub MarkAll() Implements iPublicFunctions.MarkAll
            Try
                UserFunctions_TOC.zzmpMarkAll()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub ContinueFromPrevious() Implements iPublicFunctions.ContinueFromPrevious
            Try
                zzmpContinueFromPrevious()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub EditScheme() Implements iPublicFunctions.EditScheme
            Dim xScheme As String
            Dim xMsg As String

            Try
                If Not Ribbon.bUnprotectedDocIsOpen() Then _
                    Exit Sub

                xScheme = Ribbon.xGetScheme(False)
                zzmpEditScheme(xScheme)
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub MarkAndFormatHeading() Implements iPublicFunctions.MarkAndFormatHeading
            Try
                UserFunctions_TOC.zzmpMarkAndFormatHeading()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub RestartAt() Implements iPublicFunctions.RestartAt
            Try
                zzmpRestartAt()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub RemoveNumbers() Implements iPublicFunctions.RemoveNumbers
            Try
                zzmpRemoveNumbers()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub UnmarkForTOC() Implements iPublicFunctions.UnmarkForTOC
            Try
                UserFunctions_TOC.zzmpUnmarkForTOC()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub SetSchemeInternal() Implements iPublicFunctions.SetSchemeInternal
            Dim xName As String

            Try
                'get current active scheme
                xName = xActiveScheme(CurWordApp.ActiveDocument)

                zzmpSetScheme()

                If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                    Try
                        m_oRibbon.Invalidate()
                    Catch
                    End Try
                End If
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level1Insert() Implements iPublicFunctions.Level1Insert
            Try
                zzmpLevel1Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level2Insert() Implements iPublicFunctions.Level2Insert
            Try
                zzmpLevel2Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level3Insert() Implements iPublicFunctions.Level3Insert
            Try
                zzmpLevel3Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level4Insert() Implements iPublicFunctions.Level4Insert
            Try
                zzmpLevel4Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level5Insert() Implements iPublicFunctions.Level5Insert
            Try
                zzmpLevel5Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level6Insert() Implements iPublicFunctions.Level6Insert
            Try
                zzmpLevel6Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level7Insert() Implements iPublicFunctions.Level7Insert
            Try
                zzmpLevel7Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level8Insert() Implements iPublicFunctions.Level8Insert
            Try
                zzmpLevel8Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub Level9Insert() Implements iPublicFunctions.Level9Insert
            Try
                zzmpLevel9Insert()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub DecrementTOCTabs() Implements iPublicFunctions.DecrementTOCTabs
            Try
                UserFunctions_TOC.zzmpDecrTOCTabs()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub IncremenetTOCTabs() Implements iPublicFunctions.IncremenetTOCTabs
            Try
                UserFunctions_TOC.zzmpIncrTOCTabs()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub SelectScheme() Implements iPublicFunctions.SelectScheme
            Try
                zzmpSelectScheme()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub
    End Class

    <ComVisible(True)>
    <InterfaceType(System.Runtime.InteropServices.ComInterfaceType.InterfaceIsDual)>
    Public Interface iAddInObject
        ReadOnly Property mpnPublicFunctions As MacPacNumbering.LMP.Numbering.PublicFunctions
    End Interface

    <ComVisible(True)>
    <ClassInterface(ClassInterfaceType.None)>
    Public Class AddInObject
        Inherits StandardOleMarshalObject
        Implements iAddInObject
        Public ReadOnly Property mpnPublicFunctions As MacPacNumbering.LMP.Numbering.PublicFunctions Implements iAddInObject.mpnPublicFunctions
            Get
                Return New MacPacNumbering.LMP.Numbering.PublicFunctions
            End Get
        End Property
    End Class
End Namespace
