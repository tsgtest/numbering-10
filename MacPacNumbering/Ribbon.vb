﻿Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports System.IO
Imports System.IO.Path
Imports System.Reflection.Assembly
Imports LMP.Numbering.TOC.UserFunctions_TOC
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base
Imports MacPacNumbering.LMP.Numbering.UserFunctions_Numbering
Imports MacPacNumbering.LMP.Numbering.cSchemeProps
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports System.Drawing
Imports LMP

Namespace LMP.Numbering
    <ComVisible(True)>
    Public Class Ribbon
        Implements IRibbonExtensibility
        Friend Ribbon As IRibbonUI
        Friend m_bRibbonLoaded As Boolean

        Public Function GetCustomUI(RibbonID As String) As String Implements Microsoft.Office.Core.IRibbonExtensibility.GetCustomUI
            Dim xRibbon As String
            Dim xXML As String
            Dim iStartPos As Integer
            Dim iEndPos As Integer
            Dim xLabel As String
            Dim bIsForte As Boolean
            Dim xValue As String
            Dim xInsert As String
            Dim iPos As Integer
            Dim xScreentip As String

            Try
                'determine whether Forte is loaded
                bIsForte = COMAddInIsLoaded("ForteAddIn")

                'get language
                g_lUILanguage = GetLanguageFromIni()
                If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    g_xAppName = "Numérotation TSG"
                Else
                    g_xAppName = "TSG Numbering"
                End If

                'get ribbon to load
                xRibbon = CodeBasePath & "\tsgNumberingRibbon"
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xRibbon += "French"
                End If
                If InStr(CurWordApp.Version, "14.") <> 0 Then
                    xRibbon += "_2010"
                End If
                xRibbon += ".xml"

                xXML = System.IO.File.ReadAllText(xRibbon, System.Text.Encoding.Default)

                'get tab label
                iStartPos = xXML.IndexOf("x:mpTab2")
                If iStartPos > -1 Then
                    iStartPos = xXML.IndexOf("label=", iStartPos) + 7
                    iEndPos = xXML.IndexOf("""", iStartPos)
                    xLabel = xXML.Substring(iStartPos, (iEndPos - iStartPos))
                End If

                If xLabel <> "" Then
                    'if part of Forte, shorten to "Numbering"
                    If (xLabel = "TSG Numbering") And bIsForte Then
                        xLabel = "Numbering"
                    End If

                    'upper case in Word 2013
                    If InStr(CurWordApp.Version, "15.") <> 0 Then
                        xLabel = xLabel.ToUpper()
                    End If

                    xXML = xXML.Substring(0, iStartPos) + xLabel + xXML.Substring(iEndPos)
                End If

                'GLOG 15287 (dm) - remove TOC button in Forte environment in order
                'to avoid add-in user interface error
                If bIsForte Then
                    iStartPos = xXML.IndexOf("<button id=" & """" & "g49" & """")
                    If iStartPos <> -1 Then
                        iEndPos = xXML.IndexOf("/>", iStartPos)
                        xXML = xXML.Substring(0, iStartPos) & xXML.Substring(iEndPos + 2)
                    End If
                End If

                'add admin utility if specified
                xValue = ""
                Try
                    xValue = GetAppSetting("Numbering", "ShowAdminUtility")
                Catch
                End Try
                If xValue.ToUpper() = "TRUE" Then
                    xInsert = "<tab idQ=" & """" & "x:mpTab3" & """" & " label=" & """"
                    If bIsForte Then
                        Dim oReg As cRegistry
                        oReg = New cRegistry()
                        xValue = oReg.GetLocalMachineValue("Software\The Sackett Group\Deca", "RibbonOnly")
                        If xValue = "1" Then
                            xLabel = "Forte Tools Utilities"
                        Else
                            xLabel = "Forte Utilities"
                        End If
                    ElseIf g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xLabel = "Administration TSG"
                    Else
                        xLabel = "TSG Administration"
                    End If
                    If InStr(CurWordApp.Version, "15.") <> 0 Then
                        xLabel = xLabel.ToUpper()
                    End If
                    xInsert += xLabel & """" & " keytip=" & """" & "D" & """" & " insertAfterQ=" & """" & _
                        "x:mpTab2" & """" & "><group idQ=" & """" & "x:mpNumberingAdmin" & """"
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xLabel = "Numérotation"
                    Else
                        xLabel = "Numbering"
                    End If
                    xInsert += " label=" & _
                        """" & xLabel & """" & " insertAfterQ=" & """" & "x:mpAdmin4" & """" & "><button id=" & _
                        """" & "mpAdmin1" & """" & " imageMso=" & """" & "TableIndexes" & """" & " size=" & """" & _
                        "large" & """"
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xLabel = "Thèmes Admin"
                    Else
                        xLabel = "Admin Schemes"
                    End If
                    xInsert += " label=" & """" & _
                        xLabel & """" & " keytip=" & """" & "A" & """" & " onAction=" & """" & "zzmpCallback" & _
                        """" & " tag=" & """" & "zzmpWorkAsAdmin" & """" & "/></group></tab>"
                    iPos = xXML.IndexOf("</tabs>")
                    xXML = xXML.Insert(iPos, xInsert)
                End If

                'add conversion utility to bottom of Schemes menu if specified
                xValue = ""
                Try
                    xValue = GetAppSetting("Numbering", "ShowConversionUtility")
                Catch
                End Try
                If xValue.ToUpper() = "TRUE" Then
                    iPos = xXML.IndexOf("<menu idQ=" & """" & "x:mnuschl" & """")
                    If iPos > 0 Then
                        iPos = xXML.IndexOf("</menu", iPos)
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xLabel = "Convertir les modèles en numérotation &amp;TSG"
                            xScreentip = "Convertir les modèles en numérotation TSG"
                        Else
                            xLabel = "Convert Mapped Schemes to &amp;TSG"
                            xScreentip = "Convert mapped schemes to TSG schemes"
                        End If
                        xInsert = "<button id=" & """" & "mnuSch6" & """" & " label=" & """" & xLabel & """" & " onAction=" & """" & _
                            "zzmpCallback" & """" & " tag=" & """" & "zzmpConvertToMacPac" & """" & " screentip=" & """" & _
                            xScreentip & """" & "/>"
                        xXML = xXML.Insert(iPos, xInsert)
                    End If
                End If
                Return xXML
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Sub zzmpRibbonLoaded(ribbonUI As IRibbonUI)
            Me.Ribbon = ribbonUI
            PublicFunctions.TSGNumberingRibbon = Me.Ribbon
            m_bRibbonLoaded = True
            Me.Ribbon.Invalidate()
        End Sub
        'Callback for all MacPac menu buttons
        Public Sub zzmpCallback(ByVal control As IRibbonControl)
            Dim xTag As String

            Try
                xTag = control.Tag
                Select Case xTag
                    Case "zzmpTableOfContentsInsert"
                        zzmpTableOfContentsInsertExecute()
                    Case "zzmpGoToMPTOC"
                        zzmpGoToMPTOC()
                    Case "EditCustomScheme"
                        zzmpEditCustomTOCScheme()
                    Case "zzmpIncrementTOCTabs"
                        zzmpIncrTOCTabs()
                    Case "zzmpDecrementTOCTabs"
                        zzmpDecrTOCTabs()
                    Case "zzmpChangeTOCTabs"
                        zzmpChangeTOCTabs()
                    Case "zzmpDeleteMPTOC"
                        zzmpDeleteMPTOC()
                    Case "zzmpSwitchTOCMode"
                        zzmpSwitchTOCModeExecute()
                    Case "zzmpShowHelpContents"
                        zzmpShowHelpContents()
                    Case "zzmpShowHelpFAQ"
                        zzmpShowHelpFAQ()
                    Case "zzmpShowHelpQuickSteps"
                        zzmpShowHelpQuickSteps()
                    Case "zzmpShowHelpAbout"
                        zzmpShowHelpAbout()
                    Case "zzmpMarkAll"
                        zzmpMarkAll()
                    Case "zzmpMarkHeading"
                        zzmpMarkHeading()
                    Case "zzmpUnmarkForTOC"
                        zzmpUnmarkForTOC()
                    Case "SchemeAlignLeft"
                        SchemeAlignment(mnSchemeAlignments.mnSchemeAlignments_Left)
                    Case "SchemeAlignJustified"
                        SchemeAlignment(mnSchemeAlignments.mnSchemeAlignments_Justified)
                    Case "SchemeAlignAdjust"
                        SchemeAlignment(mnSchemeAlignments.mnSchemeAlignments_Adjust)
                    Case "SchemeHanging"
                        SchemeIndents(mnSchemeIndents.mnSchemeIndent_Hang)
                    Case "SchemeStepHanging"
                        SchemeIndents(mnSchemeIndents.mnSchemeIndent_StepAndHang)
                    Case "SchemeWrapped"
                        SchemeIndents(mnSchemeIndents.mnSchemeIndent_Wrap)
                    Case "SchemeStepWrapped"
                        SchemeIndents(mnSchemeIndents.mnSchemeIndent_StepAndWrap)
                    Case "SchemeLineSpace"
                        SchemeLineSpace()
                    Case "SetSchemeFont"
                        SetSchemeFont()
                    Case "SyncContStyles"
                        SyncContStyles()
                    Case "zzmpEditScheme"
                        EditScheme()
                    Case "zzmpRestartAt"
                        zzmpRestartAt()
                    Case "zzmpRemoveNumbers"
                        zzmpRemoveNumbers()
                    Case "zzmpContinueFromPrevious"
                        zzmpContinueFromPrevious()
                    Case "zzmpUnderlineHeadingToggle"
                        zzmpUnderLineHeadingToggle()
                    Case "zzmpPromoteLevel"
                        zzmpPromoteLevel()
                    Case "zzmpDemoteLevel"
                        zzmpDemoteLevel()
                    Case "zzmpPromoteToNumberedStyle"
                        zzmpPromoteToNumberedStyle()
                    Case "zzmpDemoteToContStyle"
                        zzmpDemoteToContStyle()
                    Case "zzmpSetSchemeInternal"
                        zzmpSetSchemeInternal()
                    Case "zzmpNewScheme"
                        NewScheme()
                    Case "zzmpRelinkDocumentSchemes"
                        zzmpRelinkDocumentSchemes()
                    Case "zzmpConvertToFromHeadingStyles"
                        ConvertToFromHeadingStyles()
                    Case "zzmpLevel1Insert"
                        zzmpLevel1Insert()
                    Case "zzmpLevel2Insert"
                        zzmpLevel2Insert()
                    Case "zzmpLevel3Insert"
                        zzmpLevel3Insert()
                    Case "zzmpLevel4Insert"
                        zzmpLevel4Insert()
                    Case "zzmpLevel5Insert"
                        zzmpLevel5Insert()
                    Case "zzmpLevel6Insert"
                        zzmpLevel6Insert()
                    Case "zzmpLevel7Insert"
                        zzmpLevel7Insert()
                    Case "zzmpLevel8Insert"
                        zzmpLevel8Insert()
                    Case "zzmpLevel9Insert"
                        zzmpLevel9Insert()
                    Case "zzmpActivateSchemeFromRibbon1"
                        zzmpActivateSchemeFromRibbon1()
                    Case "zzmpActivateSchemeFromRibbon2"
                        zzmpActivateSchemeFromRibbon2()
                    Case "zzmpActivateSchemeFromRibbon3"
                        zzmpActivateSchemeFromRibbon3()
                    Case "zzmpActivateSchemeFromRibbon4"
                        zzmpActivateSchemeFromRibbon4()
                    Case "zzmpActivateSchemeFromRibbon5"
                        zzmpActivateSchemeFromRibbon5()
                    Case "zzmpActivateScheme"
                        zzmpActivateScheme()
                    Case "zzmpLoadDefaultScheme"
                        zzmpLoadDefaultScheme()
                    Case "zzmpMarkandFormatHeading"
                        zzmpMarkAndFormatHeading()
                    Case "zzmpTCCodesToStyleSeparators"
                        zzmpTCCodesToStyleSeparators()
                    Case "zzmpStyleSeparatorsToTCCodes"
                        zzmpStyleSeparatorsToTCCodes()
                    Case "zzmpFillTCCodes"
                        zzmpFillTCCodes()
                    Case "zzmpFillTCCodesFormatted"
                        zzmpFillTCCodesFormatted()
                    Case "zzmpRestoreTCCodes"
                        zzmpRestoreTCCodes()
                    Case "zzmpSwitchMarkingMode"
                        zzmpSwitchMarkingMode()
                    Case "zzmpWorkAsAdmin"
                        zzmpWorkAsAdmin()
                    Case "zzmpConvertToMacPac"
                        zzmpConvertToMacPac()
                    Case Else
                        If xTag.StartsWith("zzmpUseFavoriteSchemeFromRibbon") Then
                            zzmpUseFavoriteSchemeFromRibbon(CInt(Right(xTag, 1)))
                            Ribbon.Invalidate()
                        ElseIf xTag.StartsWith("zzmpApplyContStyle") Then
                            zzmpApplyContStyle(CInt(Right(xTag, 1)))
                        End If
                End Select

            Catch e As Exception
                [Error].Show(e)
            End Try
        End Sub

        Public Sub zzmpMPHelpCallback(ByVal control As IRibbonControl)
            Try
                ShowHelp(control.Tag)
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Sub

        'Callback for Dynamic menu
        Public Function zzmpGetSchemeActivationMenu(control As IRibbonControl) As String
            Try
                Return GetSchemeActivationXML()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        'Callback for Dynamic menu
        Public Function zzmpGetContStyleMenu(control As IRibbonControl) As String
            Try
                Return GetContStyleMenuXML()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        'Callback for Dynamic Help Menu
        Public Function zzmpHelpMenu(control As IRibbonControl) As String
            Try
                Return ReturnDynamicHELPXML()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        'Callback for Dynamic menu
        Public Function zzmpGetMarkMenu(control As IRibbonControl) As String
            Try
                Return GetMarkMenuXML()
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetVisible(control As IRibbonControl) As Boolean
            Dim oMP10 As COMAddIn
            Dim xID As String
            Dim bVisible As Boolean

            Try
                xID = control.Id
                If xID = "g52" Then
                    bVisible = True
                    Try
                        oMP10 = CurWordApp.COMAddIns.Item("MacPac102007")
                    Catch
                    End Try
                    If Not oMP10 Is Nothing Then
                        bVisible = Not oMP10.Connect
                    End If

                    If bVisible Then
                        oMP10 = Nothing
                        Try
                            oMP10 = CurWordApp.COMAddIns.Item("ForteAddIn")
                        Catch
                        End Try
                        If Not oMP10 Is Nothing Then
                            bVisible = Not oMP10.Connect
                        End If
                    End If
                ElseIf xID = "mpnSwitchTOCModeMenu" Then
                    bVisible = AllowTOCAsField()
                End If

                Return bVisible
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetEnabled(control As IRibbonControl) As Boolean
            Dim xID As String
            Dim xLevel As String
            Dim oSchemes As DocumentSchemesInfo
            Dim bEnabled As Boolean

            Try
                If m_bRibbonLoaded Then
                    'GLOG 8782 (dm)
                    If (Not ThisAddIn.IsValid) Then
                        Return False
                    End If

                    bEnabled = True
                    xID = control.Id
                    oSchemes = zzmpGetDocumentSchemesInfo()
                    If oSchemes.ActiveSchemeName <> "" Then
                        If (Len(xID) = 4) And (Left$(xID, 3) = "btn") Then
                            xLevel = Right$(xID, 1)
                            If IsNumeric(xLevel) Then
                                bEnabled = (oSchemes.ActiveSchemeLevels >= CInt(xLevel))
                            End If
                        End If
                    End If

                    If xID = "mnuCont" Then
                        bEnabled = (oSchemes.Count > 0)
                    ElseIf xID = "mpnInsertAsField" Then
                        bEnabled = Not InsertTOCsAsField()
                    ElseIf xID = "mpnInsertAsText" Then
                        bEnabled = InsertTOCsAsField()
                    End If

                    Return bEnabled
                Else
                    Return False
                End If
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetScreentip(control As IRibbonControl) As String
            Dim xID As String
            Try
                xID = control.Id
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If xID = "mpnInsertAsField" Then
                        If InsertTOCsAsField() Then
                            Return "TM est insérée comme champ"
                        Else
                            Return "Insérer TM comme champ"
                        End If
                    ElseIf xID = "mpnInsertAsText" Then
                        If InsertTOCsAsField() Then
                            Return "Insérer TM comme texte"
                        Else
                            Return "TM est insérée comme texte"
                        End If
                    End If
                Else
                    If xID = "mpnInsertAsField" Then
                        If InsertTOCsAsField() Then
                            Return "TOC is currently inserted as a field code"
                        Else
                            Return "Insert TOC as a field code"
                        End If
                    ElseIf xID = "mpnInsertAsText" Then
                        If InsertTOCsAsField() Then
                            Return "Insert TOC as text"
                        Else
                            Return "TOC is currently inserted as text"
                        End If
                    End If
                End If
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetImage(imagename As String) As stdole.IPictureDisp
            Try
                Select Case imagename
                    Case "Help5"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Help5)
                    Case "Num1"
                        Return PictureConverter.IconToPictureDisp(My.Resources.Num1)
                    Case "Num2"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Num2)
                    Case "Num3"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Num3)
                    Case "Num4"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Num4)
                    Case "Num6"
                        Return PictureConverter.IconToPictureDisp(My.Resources.Num6)
                    Case "Num7"
                        Return PictureConverter.IconToPictureDisp(My.Resources.Num7)
                    Case "Num8"
                        Return PictureConverter.IconToPictureDisp(My.Resources.Num8)
                    Case "Mark"
                        Return PictureConverter.IconToPictureDisp(My.Resources.Mark)
                    Case "Unmark"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Unmark)
                    Case "Schemes"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.Schemes)
                    Case "brushNewT"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.brushNewT)
                    Case "LeftArrow"
                        Return PictureConverter.IconToPictureDisp(My.Resources.LeftArrow2)
                    Case "RightArrow"
                        Return PictureConverter.IconToPictureDisp(My.Resources.RightArrow)
                    Case "UpArrow"
                        Return PictureConverter.IconToPictureDisp(My.Resources.UpArrow)
                    Case "DownArrow"
                        Return PictureConverter.IconToPictureDisp(My.Resources.DownArrow)
                    Case "B1T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B1T)
                    Case "B2T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B2T)
                    Case "B3T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B3T)
                    Case "B4T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B4T)
                    Case "B5T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B5T)
                    Case "B6T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B6T)
                    Case "B7T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B7T)
                    Case "B8T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B8T)
                    Case "B9T"
                        Return PictureConverter.ImageToPictureDisp(My.Resources.B9T)
                End Select
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetDynamicImage(control As IRibbonControl) As stdole.IPictureDisp
            Dim xID As String
            Dim bInsertAsField As Boolean

            Try
                xID = control.Id
                bInsertAsField = InsertTOCsAsField()
                If ((xID = "mpnInsertAsField") And InsertTOCsAsField()) Or _
                        ((xID = "mpnInsertAsText") And Not InsertTOCsAsField()) Then
                    Return CurWordApp.CommandBars.GetImageMso("TagMarkComplete", 16, 16)
                Else
                    Return Nothing
                End If
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function GetActivateSchemeLabel(control As IRibbonControl) As String
            Dim xLabel As String
            Dim oSchemes As DocumentSchemesInfo

            Try
                oSchemes = zzmpGetDocumentSchemesInfo()
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If oSchemes.ActiveSchemeName = "" Then
                        xLabel = "Thème actif:  " & "aucun"
                    Else
                        xLabel = "Thème actif:  " & oSchemes.ActiveSchemeDisplayName
                    End If
                Else
                    If oSchemes.ActiveSchemeName = "" Then
                        xLabel = "Active Scheme:  " & "None"
                    Else
                        xLabel = "Active Scheme:  " & oSchemes.ActiveSchemeDisplayName
                    End If
                End If
                Return xLabel
            Catch e As System.Exception
                [Error].Show(e)
            End Try
        End Function

        Public Function ReturnDynamicHELPXML() As String
            '---creates the XML for MacPac HELP dynamic menu
            Dim xHelpFilesPath As String
            Dim Folder As DirectoryInfo
            Dim File As FileInfo
            Dim xUserPath As String
            Dim xTemp As String
            Dim xfilename As String
            Dim xXML As String
            Dim i As Integer
            Dim oFSO() As FileInfo
            Dim lPos As Long
            Dim iFolderIncrementer As Integer
            Dim xLabel As String

            Try
                '---create XML
                xXML = "<menu xmlns=""http://schemas.microsoft.com/office/2006/01/customui"">" & vbCrLf

                '*  get Help files path
                xTemp = CodeBasePath

                i = InStrRev(UCase(xTemp), "\") '* 9.5.0
                If i > 0 Then _
                    xTemp = Left(xTemp, i - 1)

                xHelpFilesPath = xTemp & "\Tools\User Documentation\pdf"
                Folder = New DirectoryInfo(xHelpFilesPath)
                xXML = xXML & GetHelpDirectories(Folder, iFolderIncrementer)

                If InStr(xXML, "<button id=") > 0 Then   'add separator
                    xXML = xXML & " <menuSeparator id=" & """" & "mnuSepHelp" & """" & "/>" & vbCrLf
                End If

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xLabel = "À propos de la numérotation TSG"
                Else
                    xLabel = "About TSG Numbering"
                End If

                'add About button
                xXML = xXML & " <button id=" & """" & "btnAbout" & """"
                xXML = xXML & " label=" & """" & "&amp;" & xLabel & """"
                xXML = xXML & " imageMso=" & """" & "Info" & """"
                xXML = xXML & " onAction=" & """" & "zzmpCallback" & """"
                xXML = xXML & " tag=" & """" & "zzmpShowHelpAbout" & """"
                xXML = xXML & " screentip=" & """" & xLabel & """"
                xXML = xXML & "/>" & vbCrLf

                'next we add the root closing tag
                xXML = xXML & "</" & "menu" & ">"

                ReturnDynamicHELPXML = xXML

                Try
                    Ribbon.Invalidate()
                Catch
                End Try
            Catch e As System.Exception
                [Error].Show(e)
            End Try

        End Function

        Private Function GetHelpDirectories(ByVal oDir As DirectoryInfo, ByRef iSubDir As Integer) As String
            Dim xDirPath As String
            Dim xDirName As String
            Dim xFiles As String()
            Dim xMenu As String
            Dim xName As String
            Dim iPos As Integer
            Dim i As Integer
            Dim oSubDir As DirectoryInfo
            Dim xPath As String
            Dim xExt As String

            xDirPath = oDir.FullName
            xDirName = oDir.Name
            xFiles = Directory.GetFiles(xDirPath, "*.*?", SearchOption.TopDirectoryOnly)
            Array.Sort(xFiles)

            For Each oSubDir In oDir.GetDirectories()
                'make sure ids need unique names, use folder name plus an incrementer, 
                'in case subfolders have duplicate names
                'remove illegal chars from id.  
                xMenu += "<menu id=" & """" & "HelpMenu" & RemoveIllegalIDChars(oSubDir.Name) + iSubDir.ToString() + """" & _
                    " label=" & """" & oSubDir.Name & """" & ">"
                iSubDir = iSubDir + 1
                xMenu += GetHelpDirectories(oSubDir, iSubDir)
                xMenu += "</menu>"
            Next oSubDir

            For Each xPath In xFiles
                If (xPath.EndsWith(".css")) Then Continue For

                xName = Path.GetFileNameWithoutExtension(xPath)
                xExt = Path.GetExtension(xPath).ToLower()

                'GLOG : 8349 : ceh - find last occurrence
                'don't truncate filename if the only underscore is the first character
                iPos = xName.LastIndexOf("_")
                If iPos > 0 Then
                    xName = xName.Substring(0, iPos)
                End If

                'Ignore temp files in directory
                'GLOG 4445 (dm) - get only html files -
                'GLOG item #3818 - expanded to doc/pdf files as well
                If (Not xName.StartsWith("~")) And ((xExt = ".html") Or (xExt = ".doc") Or (xExt = ".docx") Or (xExt = ".pdf")) Then
                    i = i + 1
                    xMenu = xMenu & " <" & "button"

                    'insert dynamic id field
                    xMenu = xMenu & " id=" & """" & "Help" & RemoveIllegalIDChars(oDir.Name) & iSubDir.ToString() & "_" & i.ToString() & """"

                    'insert label
                    xMenu = xMenu & " label=" & """" & xName & """"

                    'insert onAction
                    xMenu = xMenu & " onAction=" & """" & "zzmpMPHELPCallback" & """"

                    'insert tag (contains macro and ico names)
                    xMenu = xMenu & " tag=" & """" & xPath & """"

                    xMenu = xMenu & "/>" & vbCrLf
                End If
            Next xPath

            Return xMenu
        End Function

        Private Sub SetSchemeFont()
            'sets the font name & size for the selected scheme
            'paragraphs/next paragraphs and numbers
            Dim oProps As New cSchemeProps

            If Not bUnprotectedDocIsOpen() Then _
                Exit Sub

            oProps.Scheme = xGetScheme(False)

            oProps.ChangeSchemeFonts()
        End Sub

        Public Shared Function xGetScheme(Optional ByVal bRequireSelection As Boolean = True) As String
            'returns MacPac numbering scheme name of
            'first paragraph in selection
            '10/4/12 - added bRequireSelection parameter
            Dim xScheme As String
            Dim LTScheme As ListTemplate
            Dim xStyle As String
            Dim iPos As Integer
            Dim xMsg As String

            '   convert if necessary
            zzmpDoConversions()

            '   get scheme of 1st selected paragraph
            '   GLOG 2847 - trap for unstyled paragraphs
            On Error Resume Next
            xStyle = CurWordApp.Selection.Paragraphs(1).Style.NameLocal.ToString()
            On Error GoTo 0

            iPos = InStr(xStyle, " Cont ")
            If iPos <> 0 Then
                '       cursor is in a cont style
                xScheme = Left(xStyle, iPos - 1)
                If xScheme = "Heading" Then
                    xScheme = "HeadingStyles"
                Else
                    xScheme = "zzmp" & xScheme
                End If
            End If

            If xScheme = "" Then
                iPos = InStr(xStyle, " Para ")
                If iPos <> 0 Then
                    '           cursor is in a para style
                    xScheme = Left(xStyle, iPos - 1)
                    If xScheme = "Heading" Then
                        xScheme = "HeadingStyles"
                    Else
                        xScheme = "zzmp" & xScheme
                    End If
                Else
                    '       attempt to get list template
                    With CurWordApp.Selection.Paragraphs(1).Range.Characters(1).ListFormat
                        On Error Resume Next
                        LTScheme = .ListTemplate
                        On Error GoTo 0
                        '           if outline numbered, get list template name
                        If Not LTScheme Is Nothing Then
                            If LTScheme.ListLevels.Count = 9 Then
                                xScheme = LTScheme.Name
                            End If
                        End If
                    End With
                End If
            End If

            '9.9.5011 - always validate MacPac scheme - we were previously
            'only doing so in conjunction with bRequireSelection = True
            If xScheme <> "" Then
                If Not MacPacNumbering.LMP.Numbering.UserFunctions_Numbering.IsMacPacScheme(xScheme, , _
                        CurWordApp.ActiveDocument) Then
                    xScheme = ""
                End If
            End If

            '   prompt if macro requires a MacPac scheme to be selected
            If (xScheme = "") And bRequireSelection Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xMsg = "Veuillez placer le curseur dans le paragraphe numéroté avec un thème numéroté TSG."
                Else
                    xMsg = "Please place the insertion point in a paragraph " & _
                           "that is numbered with a TSG numbering scheme."
                End If
                MsgBox(xMsg, vbExclamation, g_xAppName)
                Exit Function
            Else
                xGetScheme = xScheme
            End If
        End Function

        Private Sub SchemeLineSpace()
            'sets the line spacing for the selected scheme
            Dim oProps As New cSchemeProps
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Sub

            oProps.Scheme = xGetScheme(False)

            'GLOG 5590 (9.9.6009) - signature changed
            oProps.ChangeSchemeLineSpacing(oProps.Scheme)

            oProps = Nothing
        End Sub

        Private Sub SchemeAlignment(iValue As Integer)
            'sets the alignment for the selected scheme
            Dim oProps As New cSchemeProps
            Dim xMsg As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Sub

            With oProps
                .Scheme = xGetScheme(False)
                '        If Len(.Scheme) Then
                '           set alignment
                .Alignment = iValue
                .NPAlignment = iValue

                '           do in document
                .Execute()
                '        End If
            End With

            oProps = Nothing
        End Sub

        Private Sub SchemeIndents(iValue As Integer)
            'sets the alignment for the selected scheme
            Dim oProps As New cSchemeProps

            If Not bUnprotectedDocIsOpen() Then _
                Exit Sub

            oProps.Scheme = xGetScheme(False)
            oProps.Indent = iValue

            'do in document
            oProps.Execute()
            oProps = Nothing
        End Sub

        Public Shared Function bUnprotectedDocIsOpen() As Boolean
            If CurWordApp.Documents.Count = 0 Then
                MsgBox("Please open a document before running this function.", _
                    vbInformation, g_xAppName)
                Exit Function
            ElseIf CurWordApp.ActiveDocument.ProtectionType <> WdProtectionType.wdNoProtection Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("La fonction sélectionnée n'est pas disponible parce que le document est en protection pour le modifications. Pour utiliser cette fonction, veuillez ôter la protection du document.", _
                           vbInformation, g_xAppName)
                Else
                    MsgBox("The function you have selected is not " & _
                           "available because the current document " & _
                           "is protected from changes." & vbCr & _
                           "To use this function, you must first " & _
                           "unprotect the document.", vbInformation, g_xAppName)
                End If
                Exit Function
            End If

            bUnprotectedDocIsOpen = True
        End Function

        Function ActivateScheme() As Long
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Function

            xScheme = xGetScheme()

            If Len(xScheme) Then
                ActivateScheme = MacPacNumbering.LMP.Numbering.UserFunctions_Numbering _
                    .zzmpActivateScheme(CurWordApp.ActiveDocument, xScheme)
            End If
        End Function

        Private Function SyncContStyles() As Long
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Function

            xScheme = xGetScheme(False)

            SyncContStyles = zzmpSyncContStyles(xScheme)
        End Function

        Private Function ConvertToFromHeadingStyles() As Long
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Function

            xScheme = xGetScheme(False)

            ConvertToFromHeadingStyles = zzmpConvertStyles(xScheme)
        End Function

        Private Function EditScheme() As Long
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Function

            xScheme = xGetScheme(False)

            EditScheme = zzmpEditScheme(xScheme)
        End Function

        Private Function NewScheme() As Long
            Dim xScheme As String

            If Not bUnprotectedDocIsOpen() Then _
                Exit Function

            xScheme = xGetScheme(False)

            NewScheme = zzmpNewScheme(xScheme)
        End Function

        Private Sub zzmpTableOfContentsInsertExecute() 'Renamed to avoid recursion
            Dim oMP9 As Word.AddIn

            If CurWordApp.Documents.Count = 0 Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("Veuillez ouvrir un document avant d'utiliser cette fonction.", _
                        vbInformation, "Numérotation TSG")
                Else
                    MsgBox("Please open a document before running this function.", _
                        vbInformation, "TSG Numbering")
                End If
                Exit Sub
            ElseIf CurWordApp.ActiveDocument.ActiveWindow.View.Type = WdViewType.wdPrintPreview Then
                If CurWordApp.ActiveDocument.ProtectionType = WdProtectionType.wdNoProtection Then
                    CurWordApp.Run("FilePrintPreview")
                ElseIf g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("Ce raccourci clavier n'est pas disponible en  protection de document. Veuillez plutôt utiliser Alt+F+V.", _
                           vbInformation, "Numérotation TSG")
                Else
                    MsgBox("This keyboard shortcut is not available in protected " & _
                        "documents.  Please use Alt+F+V instead.", vbInformation, _
                        "TSG Numbering")
                End If
            ElseIf CurWordApp.ActiveWindow.View.SplitSpecial <> WdSpecialPane.wdPaneNone Then
                CurWordApp.ActiveWindow.View.SplitSpecial = WdSpecialPane.wdPaneNone
            ElseIf CurWordApp.Selection.StoryType <> WdStoryType.wdMainTextStory Then
                CurWordApp.ActiveWindow.ActivePane.View.SeekView = WdSeekView.wdSeekMainDocument
            Else
                On Error Resume Next
                oMP9 = CurWordApp.AddIns("@@MP90.dotm")
                If Not oMP9 Is Nothing Then _
                    CurWordApp.Run("InitializeMacPacIfNecessary")
                On Error GoTo 0

                zzmpTableOfContentsInsert()
            End If
        End Sub

        Private Sub zzmpSetSchemeInternal()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = MacPacNumbering.LMP.Numbering.UserFunctions_Numbering.zzmpSetScheme()

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpLoadDefaultScheme()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = MacPacNumbering.LMP.Numbering.UserFunctions_Numbering.zzmpLoadDefaultScheme()

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateSchemeFromRibbon1()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpActivateSchemeFromRibbon(0)

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateSchemeFromRibbon2()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpActivateSchemeFromRibbon(1)

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateSchemeFromRibbon3()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpActivateSchemeFromRibbon(2)

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateSchemeFromRibbon4()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpActivateSchemeFromRibbon(3)

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateSchemeFromRibbon5()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpActivateSchemeFromRibbon(4)

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpSetScheme()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = zzmpSetSchemeWithoutInsertion()

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpActivateScheme()
            Dim xName As String
            Dim lRet As Long

            'get current active scheme
            xName = xActiveScheme(CurWordApp.ActiveDocument)

            lRet = ActivateScheme()

            'if active scheme has changed, update label and buttons in Insert Level group
            If xActiveScheme(CurWordApp.ActiveDocument) <> xName Then
                On Error Resume Next
                Ribbon.Invalidate()
            End If
        End Sub

        Private Sub zzmpSwitchTOCModeExecute()
            Dim lRet As Long
            lRet = zzmpSwitchTOCMode()
            Ribbon.Invalidate()
        End Sub

        Sub ShowDLLErrMsg(lErr As Long, xSource As String)
            If lErr = -1 Then Exit Sub
            If lErr Then
                RaiseError(xSource)
            End If
        End Sub
    End Class

    Friend Class PictureConverter
        Inherits AxHost

        Sub New()
            MyBase.New("")
        End Sub

        Public Shared Function ImageToPictureDisp(ByVal oImage As Image) As stdole.IPictureDisp
            Return CType(AxHost.GetIPictureDispFromPicture(oImage), stdole.IPictureDisp)
        End Function

        Public Shared Function IconToPictureDisp(ByVal oIcon As Icon) As stdole.IPictureDisp
            Return ImageToPictureDisp(oIcon.ToBitmap())
        End Function

        Public Shared Function PictureDispToImage(ByVal oPicture As stdole.IPictureDisp) As Image
            Return GetPictureFromIPicture(oPicture)
        End Function
    End Class
End Namespace
