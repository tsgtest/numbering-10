﻿Option Explicit On

Imports LMP.Numbering.Base
Imports LMP.Numbering.ContentControls
Imports LMP.Numbering.WordXML
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports Microsoft.Office.Interop.Word

Public Class ThisAddIn
    Private m_oRibbon As LMP.Numbering.Ribbon
    Private Shared m_oAddInObject As MacPacNumbering.LMP.Numbering.AddInObject
    Private Shared m_bIsValid As Boolean

    Private Sub ThisAddIn_Startup() Handles Me.Startup
        'GLOG 8782 (dm) - license check moved from bAppInitialize()
        If MacPacIsLegal() Then
            m_bIsValid = True
        Else
            m_bIsValid = False
            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                xMsg = "Votre evaluation de TSG Numbering est expirée.  Veuillez contacter votre adminstrateur."
            Else
                xMsg = "Your evaluation copy of TSG Numbering has expired." & _
                       "  Please contact your system administrator."
            End If
            MsgBox(xMsg, vbExclamation, g_xAppName)
        End If
    End Sub

    Private Sub ThisAddIn_Shutdown() Handles Me.Shutdown
        m_oRibbon = Nothing
    End Sub

    Protected Overrides Function RequestService(serviceGuid As Guid) As Object
        'pass current CurWordApp to other projects
        cNumTOC.CurWordApp = Me.Application

        If serviceGuid = GetType(Office.IRibbonExtensibility).GUID Then
            If m_oRibbon Is Nothing Then _
                m_oRibbon = New LMP.Numbering.Ribbon()
            Return m_oRibbon
        End If
        Return MyBase.RequestService(serviceGuid)
    End Function

    Protected Overrides Function RequestComAddInAutomationService() As Object
        If m_oAddInObject Is Nothing Then
            m_oAddInObject = New LMP.Numbering.AddInObject()
        End If
        Return m_oAddInObject
    End Function

    Private Sub Application_DocumentChange() Handles Application.DocumentChange
        On Error Resume Next
        m_oRibbon.Ribbon.Invalidate()
    End Sub

    Public Shared ReadOnly Property IsValid As Boolean
        'returns true iff the add-in has a valid license key
        Get
            IsValid = m_bIsValid
        End Get
    End Property
End Class
