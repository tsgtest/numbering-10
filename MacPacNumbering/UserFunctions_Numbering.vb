Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mdlNumLock
Imports MacPacNumbering.LMP.Numbering.mpRange
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlConversions
Imports MacPacNumbering.LMP.Numbering.CError
Imports System.IO.Path
Imports System.Reflection.Assembly
Imports LMP

Namespace LMP.Numbering
    Friend Class UserFunctions_Numbering
        Public Structure DocumentSchemesInfo
            Public ActiveSchemeName As String
            Public ActiveSchemeDisplayName As String
            Public ActiveSchemeLevels As Integer
            Public Count As Integer
            Public DefaultIsLoaded As Boolean
        End Structure

        Public Shared Function zzmpShowModify() As Long

        End Function
        Public Shared Function zzmpEditPaste() As Long
            '    If bReadyToGo() Then
            '        zzmpEditPaste = EditPaste()
            '    End If
        End Function

        Public Shared Function zzmpConvertNumDoc() As Long
            If bReadyToGo(False, False, True, True, False) Then
                zzmpConvertNumDoc = LMP.Numbering.mdlConversions.ConvMPN80To90()
            End If
        End Function

        Public Shared Function IsMacPacScheme(ByVal xScheme As String, _
                                Optional iSchemeType As mpSchemeTypes = mpSchemeTypes.mpSchemeType_Private, _
                                Optional oSource As Object = Nothing) As Boolean
            'returns true if xScheme is the name of a MacPac
            'numbering scheme in oSource
            If bReadyToGo(True, False, False, True, True) Then
                IsMacPacScheme = cNumTOC.IsMacPacScheme(xScheme, _
                                                       iSchemeType, _
                                                       oSource)
            End If
        End Function

        Public Shared Function InitializeNumbering() As Long
            InitializeNumbering = lInitializeNumbering()
        End Function

        Public Shared Function zzmpContinueFromPrevious() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                iChangeStartAt(mpContinueFromPrevious)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpDemoteLevel() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                iChangeLevel(1)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpPromoteLevel() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                iChangeLevel(-1)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpRemoveNumbers() As Long
            If bReadyToGo(True, False, True, False, True) Then
                iRemove(CurWordApp.Selection.Range)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpRestartAt() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                iChangeStartAt(0)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpSetScheme() As Long
            If bReadyToGo(True, False, True, True, True) Then
                '       disable mp10 event handling
                If g_bIsMP10 Then _
                    CurWordApp.Run("zzmpSuppressXMLEventHandling")

                bRet = bPrepareForSchemesDialog()
                If Not bRet Then _
                    Exit Function
                g_iSchemeEditMode = mpModeSchemeMain
                ShowSchemesDialogs()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpLevel1Insert() As Long
            Dim bState As Boolean

            On Error GoTo ProcError

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel1Insert = bInsertNumberFromToolbar(1)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
            Exit Function
ProcError:
            [Error].Show(Err.GetException())
        End Function

        Public Shared Function zzmpLevel2Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel2Insert = bInsertNumberFromToolbar(2)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel3Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel3Insert = bInsertNumberFromToolbar(3)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel4Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel4Insert = bInsertNumberFromToolbar(4)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel5Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel5Insert = bInsertNumberFromToolbar(5)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel6Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel6Insert = bInsertNumberFromToolbar(6)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel7Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel7Insert = bInsertNumberFromToolbar(7)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel8Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel8Insert = bInsertNumberFromToolbar(8)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpLevel9Insert() As Long
            Dim bState As Boolean

            If bReadyToGo(True, True, True, False, True) Then
                CurWordApp.ScreenUpdating = False

                '       if NUM LOCK is ON, turn OFF
                bState = bGetNumLockState()
                If bState = True Then _
                    lSetNumLockState(True, False)

                zzmpLevel9Insert = bInsertNumberFromToolbar(9)

                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       if NUM LOCK was ON, turn back ON
                If bState = True Then _
                    lSetNumLockState(False, True)

                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpUnderLineHeadingToggle() As Long
            If bReadyToGo(True, False, True, False, True) Then
                '       disable mp10 event handling
                If g_bIsMP10 Then _
                    CurWordApp.Run("zzmpSuppressXMLEventHandling")

                bUnderLineHeadingToggle()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       restore mp10 event handling
                If g_bIsMP10 Then _
                    CurWordApp.Run("zzmpResumeXMLEventHandling")
            End If
        End Function

        Public Shared Function zzmpFixCorruption()
            With CurWordApp
                .ScreenUpdating = False
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    .StatusBar = "R�tabli styles aux niveaux de liste.  Veuillez patienter..."
                Else
                    .StatusBar = "Relinking styles to list levels.  Please wait..."
                End If

                lRet = iRelinkAllListTemplateStyles()

                .ScreenUpdating = True
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    .StatusBar = "Termin�.  Nombre de th�mes v�rifi�: " & lRet
                Else
                    .StatusBar = "Finished.  Number of schemes checked: " & lRet
                End If
                SendShiftKey()
            End With
        End Function

        Private Shared Function bReadyToGo(ByVal bOnlyIfActiveDocument As Boolean,
                                    ByVal bOnlyIfActiveSchemeExists As Boolean,
                                    ByVal bOnlyIfDocUnprotected As Boolean,
                                    ByVal bLoadStyFiles As Boolean,
                                    ByVal bDoConversions As Boolean) As Boolean
            'returns TRUE iff Word environment
            'is ready to fun Numbering functions
            Dim xValue As String
            Dim bDelete As Boolean
            Dim oAddIn As Word.AddIn

            '9.9.3 - attempt to get ui language immediately -
            'this will allow us to show pre-initialization messages in the correct language
            If g_lUILanguage = 0 Then
                g_lUILanguage = GetLanguageFromIni()
            End If

            '9.9.5003
            If g_xAppName = "" Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    g_xAppName = "Num�rotation TSG"
                Else
                    g_xAppName = "TSG Numbering"
                End If
            End If

            If bOnlyIfActiveDocument Then
                ''       false if schemes btn is disabled
                '        If Not CommandBars(mpNumberingToolbar) _
                '            .Controls(mpCtlSchemes).Enabled Then _
                '                Exit Function
                If CurWordApp.Documents.Count = 0 Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Vous devez ouvrir un document avant d'utiliser cette fonction.",
                            vbInformation, g_xAppName)
                    Else
                        MsgBox("Please open a document before running this function.",
                            vbInformation, g_xAppName)
                    End If
                    Exit Function
                End If
            End If

            If bOnlyIfDocUnprotected Then
                '       false if doc is protected
                If bDocProtected(CurWordApp.ActiveDocument, True) Then _
                    Exit Function
            End If

            '   reinitialize if necessary, if not necessary,
            If g_xPNumSty = "" Then
                If Not bAppInitialize() Then
                    '           false if init failed
                    Exit Function
                End If
            End If

            '   ensure that all files are loaded
            If bLoadStyFiles Then
                LoadStyFiles()
                '        If Not bLoadFiles(g_bAllowTOCLink) Then
                ''           false if load failed
                '            Exit Function
                '        End If
            End If

            '   6/30/09 - check for doc var corruption caused by KB969604
            DeleteCorruptedVariables()

            '   run conversions
            If bDoConversions Then
                RenameActiveLTFromVar()
                xActivateFirstScheme()
                If Not bIsConverted() Then
                    DoConversions()
                End If
            End If

            '   ensure active scheme
            If bOnlyIfActiveSchemeExists Then
                ''       false if insert level 1 is disabled
                '        If Not CommandBars(mpNumberingToolbar) _
                '            .Controls(mpCtlLevel1).Enabled Then _
                '                Exit Function
                If xActiveScheme(CurWordApp.ActiveDocument) = "" Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucun th�me actif dans ce document.  Cliquez sur le bouton Th�mes pour s�lectionner un th�me.", vbInformation, g_xAppName)
                    Else
                        MsgBox("There is no active scheme in this document.  " &
                            "Click the Schemes button to select a " &
                            "scheme to use.", vbInformation, g_xAppName)
                    End If
                    Exit Function
                End If
            End If

            bReadyToGo = True
        End Function

        Public Shared Function zzmpUnregisterVersionSpecificDLLs() As Long
            Dim xAppPath As String

            On Error Resume Next

            If mpDeveloperEnvironment Then _
                Exit Function

            xAppPath = CodeBasePath

            Shell("regsvr32 /u /s " & """" &
                xAppPath & "\mpWD80.dll" & """")
            Shell("regsvr32 /u /s " & """" &
                xAppPath & "\mpWD90.dll" & """")
            Shell("regsvr32 /s " & """" &
                xAppPath & "\mpWDXX.dll" & """")

            '    lRet = SetVersionFiles()
        End Function

        Public Shared Function zzmpWorkAsAdmin() As Long
            If bReadyToGo(True, False, True, True, True) Then
                bRet = bPrepareForSchemesDialog()
                If Not bRet Then _
                    Exit Function
                WorkAsAdmin()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpActivateScheme(ByVal docP As Word.Document,
                                    ByVal xKey As String) As Long
            If bReadyToGo(True, False, True, False, True) Then
                zzmpActivateScheme = ActivateScheme(xKey)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpDoConversions()
            'runs conversion code if necessary
            bReadyToGo(True, False, True, False, True)
            If Not g_bPreserveUndoList Then _
                CurWordApp.ActiveDocument.UndoClear()
        End Function

        Public Shared Function zzmpUseScheme(ByVal xScheme As String,
                               ByVal bReset As Boolean) As Long
            If bReadyToGo(True, False, True, True, True) Then
                zzmpUseScheme = UseSchemeExternal(xScheme, bReset, False)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpPromoteToNumberedStyle() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                zzmpPromoteToNumberedStyle = NumberContSwitch(True)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpDemoteToContStyle() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                zzmpDemoteToContStyle = NumberContSwitch(False)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpHideParagraph() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                zzmpHideParagraph = HideParagraph()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpConvertToMacPac() As Long
            If bReadyToGo(True, False, True, False, True) Then
                CurWordApp.ScreenUpdating = False
                zzmpConvertToMacPac = ConvertToMacPac()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpSyncContStyles(xScheme As String) As Long
            If bReadyToGo(True, False, True, False, True) Then
                CreateContStyles(xScheme)
                '9.9.5001
                UpdateParaStyles(xScheme)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpConvertStyles(xScheme As String,
                                   Optional bShowMessage As Boolean = True) As Long
            Dim xRoot As String
            Dim iSchemes As Integer

            If bReadyToGo(True, False, True, False, True) Then
                '10/4/12 - user is no longer required to select scheme
                If xScheme = "" Then
                    iSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                    If iSchemes = 0 Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            MsgBox("Aucun th�me de num�rotation TSG dans ce document.", vbInformation, g_xAppName)
                        Else
                            MsgBox("There are no TSG numbering schemes " &
                                "in this document.", vbInformation, g_xAppName)
                        End If
                    ElseIf iSchemes = 1 Then
                        xScheme = g_xDSchemes(0, 0)
                    Else
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            Dim oForm As frmSchemeSelectorFrench
                            oForm = New frmSchemeSelectorFrench
                            oForm.ShowDialog()
                            If oForm.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                xScheme = g_xDSchemes(oForm.lstSchemes.SelectedIndex, 0)
                            End If
                            oForm = Nothing
                        Else
                            Dim oForm As frmSchemeSelector
                            oForm = New frmSchemeSelector
                            oForm.ShowDialog()
                            If oForm.DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                                xScheme = g_xDSchemes(oForm.lstSchemes.SelectedIndex, 0)
                            End If
                            oForm = Nothing
                        End If
                    End If
                End If

                If xScheme <> "" Then
                    CurWordApp.ScreenUpdating = False
                    xRoot = xGetLTRoot(xScheme)
                    If bIsHeadingScheme(xRoot) Then
                        bConvertToMacPacStyles(xRoot)
                    Else
                        bConvertToHeadingStyles(xRoot)
                    End If
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                    CurWordApp.ScreenUpdating = True
                End If
            End If
        End Function

        Public Shared Function zzmpShowHelpAbout() As Long
            Dim oForm As System.Windows.Forms.Form

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                oForm = New frmAboutNewFrench
            Else
                oForm = New frmAboutNew
            End If
            oForm.ShowDialog()
        End Function

        Public Shared Function zzmpShowHelpContents() As Long
            HtmlHelp(0, CodeBasePath &
                "\Fullhelp.chm", HH_DISPLAY_TOC, 0)
        End Function

        Public Shared Function zzmpShowHelpFAQ() As Long
            HtmlHelp(0, CodeBasePath &
                "\FAQ.chm", HH_DISPLAY_TOC, 0)
        End Function

        Public Shared Function zzmpShowHelpQuickSteps() As Long
            HtmlHelp(0, CodeBasePath &
                "\QuickSteps.chm", HH_DISPLAY_TOC, 0)
        End Function

        Public Shared Function zzmpEditScheme(ByVal xScheme As String) As Long
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, False, True, True, True) Then
                    'force accelerator cues in Word 2013
                    If g_iWordVersion > mpWordVersions.mpWordVersion_2010 Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    If g_bAllowSchemeEdit Then
                        EditScheme(xScheme)
                        If Not g_bPreserveUndoList Then _
                            CurWordApp.ActiveDocument.UndoClear()
                    Else
                        'GLOG 4723
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            MsgBox("Cette fonctionnalit� n'est pas disponible. Veuillez contacter votre administrateur.",
                               vbInformation, g_xAppName)
                        Else
                            MsgBox("This feature is not available.  Please contact your system administrator.",
                                vbInformation, g_xAppName)
                        End If
                    End If
                End If
            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpNewScheme(ByVal xScheme As String) As Long
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, False, True, True, True) Then
                    'force accelerator cues in Word 2013
                    If g_iWordVersion > mpWordVersions.mpWordVersion_2010 Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    If g_bAllowSchemeNew Then
                        NewScheme(xScheme)
                        If Not g_bPreserveUndoList Then _
                            CurWordApp.ActiveDocument.UndoClear()
                    Else
                        'GLOG 4723
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            MsgBox("Cette fonctionnalit� n'est pas disponible. Veuillez contacter votre administrateur.",
                               vbInformation, g_xAppName)
                        Else
                            MsgBox("This feature is not available.  Please contact your system administrator.",
                                vbInformation, g_xAppName)
                        End If
                    End If
                End If
            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpRelinkDocumentSchemes() As Long
            If bReadyToGo(True, False, True, True, True) Then
                RelinkDocumentSchemes()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpLoadDefaultScheme() As Long
            If bReadyToGo(True, False, True, True, True) Then
                'prompt to save document if necessary
                If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow$() = "") Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox(mpOrganizerSavePromptFrench, vbInformation, g_xAppName)
                    Else
                        MsgBox(mpOrganizerSavePrompt, vbInformation, g_xAppName)
                    End If
                    Exit Function
                End If

                zzmpLoadDefaultScheme = UseSchemeExternal("", True, True)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpActivateSchemeFromRibbon(ByVal iIndex As Integer) As Long
            If bReadyToGo(True, False, True, False, True) Then
                ActivateSchemeFromRibbon(iIndex)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function GetSchemeActivationXML() As String
            If bReadyToGo(True, False, True, False, True) Then
                GetSchemeActivationXML = mdlN90.GetSchemeActivationXML
            End If
        End Function

        Public Shared Function GetContStyleMenuXML() As String
            If bReadyToGo(True, False, True, False, True) Then
                GetContStyleMenuXML = mdlN90.GetContStyleMenuXML
            End If
        End Function

        Public Shared Function zzmpApplyContStyle(ByVal iLevel As Integer) As Long
            If bReadyToGo(True, True, True, True, True) Then
                ApplyContStyle(iLevel)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpSetSchemeWithoutInsertion() As Long
            Dim xValue As String

            'turn off "Insert Level 1 Automatically"
            xValue = GetUserSetting("Numbering", "InsertLevel1Automatically")
            If xValue = "1" Then _
                SetUserSetting("Numbering", "InsertLevel1Automatically", "0")

            'display dialog
            zzmpSetScheme()

            'restore original setting
            If xValue = "1" Then _
                SetUserSetting("Numbering", "InsertLevel1Automatically", "1")
        End Function

        Public Shared Function zzmpSelectScheme() As Long
            'turn on selection-only mode
            g_bSchemeSelectionOnly = True

            'display dialog with automatic insertion disabled
            zzmpSetSchemeWithoutInsertion()

            'turn off selection-only mode
            g_bSchemeSelectionOnly = False
        End Function

        Public Shared Sub ShowHelp(xFileName As String)
            LaunchDocumentByExtension(xFileName)
        End Sub

        Public Shared Function zzmpGetDocumentSchemesInfo() As DocumentSchemesInfo
            Dim oSchemes As New DocumentSchemesInfo
            Dim oRecord As cNumScheme
            Dim xScheme As String
            Dim iCount As Integer
            Dim i As Integer
            Dim xDefaultScheme As String

            On Error GoTo ProcError

            If CurWordApp.Documents.Count > 0 Then
                'GLOG 5247 - changed all parameters to False to prevent autorelinking
                'from occurring as soon as you click the Numbering tab on the ribbon
                If bReadyToGo(False, False, False, False, False) Then
                    xScheme = xActiveScheme(CurWordApp.ActiveDocument)
                    If xScheme <> "" Then
                        On Error Resume Next
                        oRecord = GetRecord(xScheme, mpSchemeTypes.mpSchemeType_Document)
                        If Err.Number = 0 Then
                            On Error GoTo ProcError
                            oSchemes.ActiveSchemeName = xScheme
                            oSchemes.ActiveSchemeDisplayName = oRecord.DisplayName
                            oSchemes.ActiveSchemeLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)
                            iCount = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                            oSchemes.Count = iCount

                            'determine whether default scheme is loaded
                            If bDefaultSchemeExists() Then
                                xDefaultScheme = GetUserSetting("Numbering", "DefaultScheme")
                            Else
                                xDefaultScheme = g_xFSchemes(0, 0)
                            End If
                            For i = 0 To iCount - 1
                                If g_xDSchemes(i, 0) = xDefaultScheme Then
                                    oSchemes.DefaultIsLoaded = True
                                    Exit For
                                End If
                            Next i
                        End If
                    End If
                End If
            End If

            zzmpGetDocumentSchemesInfo = oSchemes
            Exit Function
ProcError:
            'GLOG 5476 - suppress error if the only document closes during execution
            If Err.Number <> 4248 Then
                Throw New System.Exception("MPN90.UserFunctions_Numbering.zzmpGetDocumentSchemesInfo")
            End If
            Exit Function
        End Function

        Public Shared Function zzmpUseFavoriteSchemeFromRibbon(ByVal iIndex As Integer) As Long
            If bReadyToGo(True, False, True, True, True) Then
                'prompt to save document if necessary
                If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow$() = "") Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox(mpOrganizerSavePromptFrench, vbInformation, g_xAppName)
                    Else
                        MsgBox(mpOrganizerSavePrompt, vbInformation, g_xAppName)
                    End If
                    Exit Function
                End If

                zzmpUseFavoriteSchemeFromRibbon = UseSchemeExternal(
                    g_xFavoriteSchemes(iIndex - 1, 0), True, True)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpReinitialize() As Long
            'added in 9.9.6001 for GLOG 3940
            zzmpReinitialize = CLng(bAppInitialize())
        End Function

        Public Shared Function zzmpDeleteScheme(ByVal xScheme As String,
            ByVal bSuppressMessages As Boolean) As Long
            'added in 9.9.6013
            Dim xAlias As String
            Dim xMsg As String
            If bReadyToGo(True, False, True, True, True) Then
                On Error Resume Next
                xAlias = GetField(xScheme, mpRecordFields.mpRecField_Alias, mpSchemeTypes.mpSchemeType_Document)
                On Error GoTo 0
                If xAlias <> "" Then
                    bDeleteScheme(xScheme, xAlias, mpSchemeTypes.mpSchemeType_Document)
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                ElseIf Not bSuppressMessages Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Le th�me " & Chr(34) & xScheme & Chr(34) &
                            " n'existe pas dans ce document."
                    Else
                        xMsg = "The scheme " & Chr(34) & xScheme & Chr(34) &
                            " does not exist in this document."
                    End If
                    MsgBox(xMsg, vbExclamation, g_xAppName)
                End If
            End If
        End Function

        Public Shared Function zzmpChangeToDocumentScheme(ByVal xScheme As String,
            ByVal bSuppressMessages As Boolean) As Long
            'added in 9.9.6013
            Dim xAlias As String
            Dim xMsg As String
            If bReadyToGo(True, False, True, True, True) Then
                'native Heading scheme may not yet be a MacPac scheme
                If xScheme = "HeadingStyles" Then
                    ConvertWordHeadingStyles()
                End If

                On Error Resume Next
                xAlias = GetField(xScheme, mpRecordFields.mpRecField_Alias, mpSchemeTypes.mpSchemeType_Document)
                On Error GoTo 0

                If xAlias <> "" Then
                    iChangeScheme(xScheme, mpSchemeTypes.mpSchemeType_Document, "",
                        False, , False, , bIsHeadingScheme(xScheme))

                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                ElseIf Not bSuppressMessages Then
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        xMsg = "Le th�me " & Chr(34) & xScheme & Chr(34) & _
                            " n'existe pas dans ce document."
                    Else
                        xMsg = "The scheme " & Chr(34) & xScheme & Chr(34) & _
                            " does not exist in this document."
                    End If
                    MsgBox(xMsg, vbExclamation, g_xAppName)
                End If
            End If
        End Function
    End Class
End Namespace



