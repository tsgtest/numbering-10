<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAboutNewFrench
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents pctClose As System.Windows.Forms.PictureBox
    Public WithEvents pctMPLogo As System.Windows.Forms.PictureBox
    Public WithEvents lblAppName2 As System.Windows.Forms.Label
    Public WithEvents lblCompatibility1 As System.Windows.Forms.Label
    Public WithEvents lblCopyright As System.Windows.Forms.Label
    Public WithEvents lblVersion As System.Windows.Forms.Label
    Public WithEvents lblAppName As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAboutNewFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pctClose = New System.Windows.Forms.PictureBox()
        Me.pctMPLogo = New System.Windows.Forms.PictureBox()
        Me.lblAppName2 = New System.Windows.Forms.Label()
        Me.lblCompatibility1 = New System.Windows.Forms.Label()
        Me.lblCopyright = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.lblAppName = New System.Windows.Forms.Label()
        Me.pictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pictureBox3 = New System.Windows.Forms.PictureBox()
        CType(Me.pctClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctMPLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pctClose
        '
        Me.pctClose.BackColor = System.Drawing.Color.White
        Me.pctClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.pctClose.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pctClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pctClose.Image = CType(resources.GetObject("pctClose.Image"), System.Drawing.Image)
        Me.pctClose.Location = New System.Drawing.Point(534, 11)
        Me.pctClose.Name = "pctClose"
        Me.pctClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pctClose.Size = New System.Drawing.Size(20, 18)
        Me.pctClose.TabIndex = 9
        Me.pctClose.TabStop = False
        '
        'pctMPLogo
        '
        Me.pctMPLogo.BackColor = System.Drawing.Color.White
        Me.pctMPLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.pctMPLogo.Font = New System.Drawing.Font("Arial", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pctMPLogo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pctMPLogo.Image = CType(resources.GetObject("pctMPLogo.Image"), System.Drawing.Image)
        Me.pctMPLogo.Location = New System.Drawing.Point(8, 13)
        Me.pctMPLogo.Name = "pctMPLogo"
        Me.pctMPLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pctMPLogo.Size = New System.Drawing.Size(240, 240)
        Me.pctMPLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pctMPLogo.TabIndex = 0
        Me.pctMPLogo.TabStop = False
        '
        'lblAppName2
        '
        Me.lblAppName2.BackColor = System.Drawing.Color.Transparent
        Me.lblAppName2.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAppName2.Font = New System.Drawing.Font("Gill Sans MT", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppName2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAppName2.Location = New System.Drawing.Point(249, 64)
        Me.lblAppName2.Name = "lblAppName2"
        Me.lblAppName2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAppName2.Size = New System.Drawing.Size(292, 70)
        Me.lblAppName2.TabIndex = 10
        Me.lblAppName2.Text = "TSG"
        Me.lblAppName2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCompatibility1
        '
        Me.lblCompatibility1.BackColor = System.Drawing.Color.White
        Me.lblCompatibility1.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompatibility1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompatibility1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompatibility1.Location = New System.Drawing.Point(247, 194)
        Me.lblCompatibility1.Name = "lblCompatibility1"
        Me.lblCompatibility1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompatibility1.Size = New System.Drawing.Size(297, 72)
        Me.lblCompatibility1.TabIndex = 7
        Me.lblCompatibility1.Text = "Compatible avec " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Office 2016/365 ProPlus (32-bit et 64-bit) et " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Office 2013/365" & _
    " ProPlus et 2010 (32-bit)"
        Me.lblCompatibility1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCopyright
        '
        Me.lblCopyright.BackColor = System.Drawing.Color.White
        Me.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCopyright.Location = New System.Drawing.Point(272, 167)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCopyright.Size = New System.Drawing.Size(247, 18)
        Me.lblCopyright.TabIndex = 6
        Me.lblCopyright.Text = "©1990 - 2017"
        Me.lblCopyright.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.White
        Me.lblVersion.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVersion.Location = New System.Drawing.Point(272, 134)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblVersion.Size = New System.Drawing.Size(247, 28)
        Me.lblVersion.TabIndex = 5
        Me.lblVersion.Text = "Version automatic"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblAppName
        '
        Me.lblAppName.BackColor = System.Drawing.Color.Transparent
        Me.lblAppName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAppName.Font = New System.Drawing.Font("Gill Sans MT", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAppName.Location = New System.Drawing.Point(264, 19)
        Me.lblAppName.Name = "lblAppName"
        Me.lblAppName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAppName.Size = New System.Drawing.Size(262, 56)
        Me.lblAppName.TabIndex = 4
        Me.lblAppName.Text = "Numérotation"
        Me.lblAppName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pictureBox2
        '
        Me.pictureBox2.Image = Global.MacPacNumbering.My.Resources.Resources.TSGLogo
        Me.pictureBox2.Location = New System.Drawing.Point(246, 250)
        Me.pictureBox2.Name = "pictureBox2"
        Me.pictureBox2.Size = New System.Drawing.Size(38, 28)
        Me.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureBox2.TabIndex = 58
        Me.pictureBox2.TabStop = False
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = Global.MacPacNumbering.My.Resources.Resources.TSGTagline
        Me.pictureBox1.Location = New System.Drawing.Point(8, 289)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(274, 26)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureBox1.TabIndex = 57
        Me.pictureBox1.TabStop = False
        '
        'pictureBox3
        '
        Me.pictureBox3.Image = Global.MacPacNumbering.My.Resources.Resources.TSGInc
        Me.pictureBox3.Location = New System.Drawing.Point(8, 258)
        Me.pictureBox3.Name = "pictureBox3"
        Me.pictureBox3.Size = New System.Drawing.Size(241, 43)
        Me.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureBox3.TabIndex = 56
        Me.pictureBox3.TabStop = False
        '
        'frmAboutNewFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(555, 325)
        Me.ControlBox = False
        Me.Controls.Add(Me.pictureBox2)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.pictureBox3)
        Me.Controls.Add(Me.lblAppName)
        Me.Controls.Add(Me.pctClose)
        Me.Controls.Add(Me.pctMPLogo)
        Me.Controls.Add(Me.lblAppName2)
        Me.Controls.Add(Me.lblCompatibility1)
        Me.Controls.Add(Me.lblCopyright)
        Me.Controls.Add(Me.lblVersion)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Gill Sans MT", 8.4!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(1, 1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAboutNewFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.pctClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctMPLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents pictureBox2 As System.Windows.Forms.PictureBox
    Private WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Private WithEvents pictureBox3 As System.Windows.Forms.PictureBox
#End Region
End Class