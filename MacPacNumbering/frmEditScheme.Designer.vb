﻿Imports LMP.Controls

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditScheme
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditScheme))
        Me.tsToolbar = New System.Windows.Forms.ToolStrip()
        Me.cmdCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdSave = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdZoom = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdUpdate = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.pnlLevels = New System.Windows.Forms.Panel()
        Me.tsLevel = New System.Windows.Forms.ToolStrip()
        Me.tbtnLevel1 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel2 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel3 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel4 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel5 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel6 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel7 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel8 = New System.Windows.Forms.ToolStripButton()
        Me.tbtnLevel9 = New System.Windows.Forms.ToolStripButton()
        Me.cmdAddLevel = New System.Windows.Forms.ToolStripButton()
        Me.cmdDeleteLevel = New System.Windows.Forms.ToolStripButton()
        Me.splitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tabControl1 = New System.Windows.Forms.TabControl()
        Me.tabPage1 = New System.Windows.Forms.TabPage()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.pnlNumberFont = New System.Windows.Forms.GroupBox()
        Me.cmbNumFontSize = New mpnControls.ComboBox()
        Me.cmbNumFontName = New mpnControls.ComboBox()
        Me.cmbUnderlineStyle = New mpnControls.ComboBox()
        Me.lblNumFontSize = New System.Windows.Forms.Label()
        Me.lblNumFontName = New System.Windows.Forms.Label()
        Me.chkNumberCaps = New System.Windows.Forms.CheckBox()
        Me.chkNumberItalic = New System.Windows.Forms.CheckBox()
        Me.chkNumberBold = New System.Windows.Forms.CheckBox()
        Me.lblNumFontUnderline = New System.Windows.Forms.Label()
        Me.pnlNumber = New System.Windows.Forms.GroupBox()
        Me.txtStartAt = New System.Windows.Forms.TextBox()
        Me.lstPrevLevels = New System.Windows.Forms.ListView()
        Me.LevelName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.LevelValue = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.spnTabPosition = New SpinTextInternational()
        Me.cmbTrailingChar = New mpnControls.ComboBox()
        Me.cmbNumberStyle = New mpnControls.ComboBox()
        Me.rtfNumberFormat = New System.Windows.Forms.RichTextBox()
        Me.chkNonbreakingSpaces = New System.Windows.Forms.CheckBox()
        Me.chkRightAlign = New System.Windows.Forms.CheckBox()
        Me.chkLegalStyle = New System.Windows.Forms.CheckBox()
        Me.chkReset = New System.Windows.Forms.CheckBox()
        Me.btnAddPrevLevel = New System.Windows.Forms.Button()
        Me.lblTabPosition = New System.Windows.Forms.Label()
        Me.lblStartAt = New System.Windows.Forms.Label()
        Me.lblTrailingChar = New System.Windows.Forms.Label()
        Me.lbNumberStyle = New System.Windows.Forms.Label()
        Me.lblNumberFormat = New System.Windows.Forms.Label()
        Me.lblPreviousLevels = New System.Windows.Forms.Label()
        Me.udStartAt = New System.Windows.Forms.NumericUpDown()
        Me.tabPage2 = New System.Windows.Forms.TabPage()
        Me.pnlParagraph = New System.Windows.Forms.Panel()
        Me.pnlFont = New System.Windows.Forms.GroupBox()
        Me.optStyleFormats = New System.Windows.Forms.RadioButton()
        Me.optDirectFormats = New System.Windows.Forms.RadioButton()
        Me.chkTCCaps = New System.Windows.Forms.CheckBox()
        Me.chkTCSmallCaps = New System.Windows.Forms.CheckBox()
        Me.chkTCUnderline = New System.Windows.Forms.CheckBox()
        Me.chkTCItalic = New System.Windows.Forms.CheckBox()
        Me.chkTCBold = New System.Windows.Forms.CheckBox()
        Me.pnlPosition = New System.Windows.Forms.GroupBox()
        Me.spnRightIndent = New SpinTextInternational()
        Me.spnTextPosition = New SpinTextInternational()
        Me.spnNumberPosition = New SpinTextInternational()
        Me.spnSpaceAfter = New SpinTextInternational()
        Me.spnSpaceBefore = New SpinTextInternational()
        Me.spnAt = New SpinTextInternational()
        Me.cmbTextAlign = New mpnControls.ComboBox()
        Me.cmbLineSpacing = New mpnControls.ComboBox()
        Me.cmbFontSize = New mpnControls.ComboBox()
        Me.cmbFontName = New mpnControls.ComboBox()
        Me.cmdSynchronize = New System.Windows.Forms.Button()
        Me.lblSynchronize = New System.Windows.Forms.Label()
        Me.lblFontSize = New System.Windows.Forms.Label()
        Me.lblLineSpacing = New System.Windows.Forms.Label()
        Me.lblFontName = New System.Windows.Forms.Label()
        Me.lblTextAlign = New System.Windows.Forms.Label()
        Me.lblAt = New System.Windows.Forms.Label()
        Me.lblSpaceAfter = New System.Windows.Forms.Label()
        Me.lblSpaceBefore = New System.Windows.Forms.Label()
        Me.lblRightIndent = New System.Windows.Forms.Label()
        Me.lblTextPosition = New System.Windows.Forms.Label()
        Me.lblNumberPosition = New System.Windows.Forms.Label()
        Me.pnlOther = New System.Windows.Forms.GroupBox()
        Me.cmbNextParagraph = New mpnControls.ComboBox()
        Me.chkPageBreak = New System.Windows.Forms.CheckBox()
        Me.chkKeepWithNext = New System.Windows.Forms.CheckBox()
        Me.chkKeepLinesTogether = New System.Windows.Forms.CheckBox()
        Me.chkWidowOrphan = New System.Windows.Forms.CheckBox()
        Me.lblNextParagraph = New System.Windows.Forms.Label()
        Me.tabPage3 = New System.Windows.Forms.TabPage()
        Me.pnlContStyle = New System.Windows.Forms.Panel()
        Me.pnlContPagination = New System.Windows.Forms.GroupBox()
        Me.chkContKeepLinesTogether = New System.Windows.Forms.CheckBox()
        Me.chkContKeepWithNext = New System.Windows.Forms.CheckBox()
        Me.chkContWidowOrphan = New System.Windows.Forms.CheckBox()
        Me.pnlContSpacing = New System.Windows.Forms.GroupBox()
        Me.spnContAt = New SpinTextInternational()
        Me.spnContSpaceAfter = New SpinTextInternational()
        Me.spnContSpaceBefore = New SpinTextInternational()
        Me.cmbContLineSpacing = New mpnControls.ComboBox()
        Me.lblContAt = New System.Windows.Forms.Label()
        Me.lblContLineSpacing = New System.Windows.Forms.Label()
        Me.lblContSpaceAfter = New System.Windows.Forms.Label()
        Me.lblContSpaceBefore = New System.Windows.Forms.Label()
        Me.pnlContIndentation = New System.Windows.Forms.GroupBox()
        Me.spnBy = New SpinTextInternational()
        Me.spnContRightIndent = New SpinTextInternational()
        Me.spnContLeftIndent = New SpinTextInternational()
        Me.cmbSpecial = New mpnControls.ComboBox()
        Me.lblBy = New System.Windows.Forms.Label()
        Me.lblSpecial = New System.Windows.Forms.Label()
        Me.lblContRightIndent = New System.Windows.Forms.Label()
        Me.lblContLeftIndent = New System.Windows.Forms.Label()
        Me.pnlContGeneral = New System.Windows.Forms.GroupBox()
        Me.cmbContFontSize = New mpnControls.ComboBox()
        Me.lblContFontSize = New System.Windows.Forms.Label()
        Me.cmbAlignment = New mpnControls.ComboBox()
        Me.cmbContFontName = New mpnControls.ComboBox()
        Me.lblAlignment = New System.Windows.Forms.Label()
        Me.lblContFontName = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.toolStrip3 = New System.Windows.Forms.ToolStrip()
        Me.tbtnNumber = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbtnParagraph = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbtnContStyle = New System.Windows.Forms.ToolStripButton()
        Me.tsToolbar.SuspendLayout()
        Me.pnlLevels.SuspendLayout()
        Me.tsLevel.SuspendLayout()
        CType(Me.splitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitContainer1.Panel1.SuspendLayout()
        Me.splitContainer1.Panel2.SuspendLayout()
        Me.splitContainer1.SuspendLayout()
        Me.tabControl1.SuspendLayout()
        Me.tabPage1.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.pnlNumberFont.SuspendLayout()
        Me.pnlNumber.SuspendLayout()
        CType(Me.udStartAt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPage2.SuspendLayout()
        Me.pnlParagraph.SuspendLayout()
        Me.pnlFont.SuspendLayout()
        Me.pnlPosition.SuspendLayout()
        Me.pnlOther.SuspendLayout()
        Me.tabPage3.SuspendLayout()
        Me.pnlContStyle.SuspendLayout()
        Me.pnlContPagination.SuspendLayout()
        Me.pnlContSpacing.SuspendLayout()
        Me.pnlContIndentation.SuspendLayout()
        Me.pnlContGeneral.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.toolStrip3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsToolbar
        '
        Me.tsToolbar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsToolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsToolbar.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.tsToolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdCancel, Me.toolStripSeparator1, Me.cmdSave, Me.toolStripSeparator3, Me.cmdZoom, Me.toolStripSeparator2, Me.cmdUpdate, Me.ToolStripSeparator4})
        Me.tsToolbar.Location = New System.Drawing.Point(0, 416)
        Me.tsToolbar.Name = "tsToolbar"
        Me.tsToolbar.Size = New System.Drawing.Size(521, 48)
        Me.tsToolbar.Stretch = True
        Me.tsToolbar.TabIndex = 84
        Me.tsToolbar.Text = "ToolStrip1"
        '
        'cmdCancel
        '
        Me.cmdCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdCancel.AutoSize = False
        Me.cmdCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.cmdCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdCancel.Size = New System.Drawing.Size(60, 45)
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 48)
        '
        'cmdSave
        '
        Me.cmdSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdSave.AutoSize = False
        Me.cmdSave.Image = Global.MacPacNumbering.My.Resources.Resources.OK
        Me.cmdSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdSave.Size = New System.Drawing.Size(60, 45)
        Me.cmdSave.Text = "O&K"
        Me.cmdSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 48)
        '
        'cmdZoom
        '
        Me.cmdZoom.AutoSize = False
        Me.cmdZoom.CheckOnClick = True
        Me.cmdZoom.Image = Global.MacPacNumbering.My.Resources.Resources.Zoom
        Me.cmdZoom.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdZoom.Name = "cmdZoom"
        Me.cmdZoom.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdZoom.Size = New System.Drawing.Size(60, 45)
        Me.cmdZoom.Text = "&Zoom"
        Me.cmdZoom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator2
        '
        Me.toolStripSeparator2.Name = "toolStripSeparator2"
        Me.toolStripSeparator2.Size = New System.Drawing.Size(6, 48)
        '
        'cmdUpdate
        '
        Me.cmdUpdate.AutoSize = False
        Me.cmdUpdate.Image = Global.MacPacNumbering.My.Resources.Resources.Refresh
        Me.cmdUpdate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdUpdate.Size = New System.Drawing.Size(60, 45)
        Me.cmdUpdate.Text = "&Refresh"
        Me.cmdUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 48)
        '
        'pnlLevels
        '
        Me.pnlLevels.Controls.Add(Me.tsLevel)
        Me.pnlLevels.Location = New System.Drawing.Point(13, 18)
        Me.pnlLevels.Name = "pnlLevels"
        Me.pnlLevels.Size = New System.Drawing.Size(38, 384)
        Me.pnlLevels.TabIndex = 15
        '
        'tsLevel
        '
        Me.tsLevel.AutoSize = False
        Me.tsLevel.BackColor = System.Drawing.Color.Transparent
        Me.tsLevel.Dock = System.Windows.Forms.DockStyle.Left
        Me.tsLevel.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsLevel.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.tsLevel.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbtnLevel1, Me.tbtnLevel2, Me.tbtnLevel3, Me.tbtnLevel4, Me.tbtnLevel5, Me.tbtnLevel6, Me.tbtnLevel7, Me.tbtnLevel8, Me.tbtnLevel9, Me.cmdAddLevel, Me.cmdDeleteLevel})
        Me.tsLevel.Location = New System.Drawing.Point(0, 0)
        Me.tsLevel.Name = "tsLevel"
        Me.tsLevel.Size = New System.Drawing.Size(36, 384)
        Me.tsLevel.Stretch = True
        Me.tsLevel.TabIndex = 15
        Me.tsLevel.Text = "Top Toolstrip"
        Me.tsLevel.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical90
        '
        'tbtnLevel1
        '
        Me.tbtnLevel1.AutoSize = False
        Me.tbtnLevel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel1.Image = Global.MacPacNumbering.My.Resources.Resources.Level1
        Me.tbtnLevel1.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel1.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel1.Name = "tbtnLevel1"
        Me.tbtnLevel1.Padding = New System.Windows.Forms.Padding(1)
        Me.tbtnLevel1.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel1.Text = "1"
        Me.tbtnLevel1.ToolTipText = "Switch to Level 1 (Alt+1)"
        '
        'tbtnLevel2
        '
        Me.tbtnLevel2.AutoSize = False
        Me.tbtnLevel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel2.Image = Global.MacPacNumbering.My.Resources.Resources.Level2
        Me.tbtnLevel2.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel2.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel2.Name = "tbtnLevel2"
        Me.tbtnLevel2.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel2.Text = "toolStripButton5"
        Me.tbtnLevel2.ToolTipText = "Switch to Level 2 (Alt+2)"
        '
        'tbtnLevel3
        '
        Me.tbtnLevel3.AutoSize = False
        Me.tbtnLevel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel3.Image = Global.MacPacNumbering.My.Resources.Resources.Level3
        Me.tbtnLevel3.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel3.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel3.Name = "tbtnLevel3"
        Me.tbtnLevel3.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel3.Text = "toolStripButton5"
        Me.tbtnLevel3.ToolTipText = "Switch to Level 3 (Alt+3)"
        '
        'tbtnLevel4
        '
        Me.tbtnLevel4.AutoSize = False
        Me.tbtnLevel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel4.Image = Global.MacPacNumbering.My.Resources.Resources.Level4
        Me.tbtnLevel4.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel4.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel4.Name = "tbtnLevel4"
        Me.tbtnLevel4.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel4.Text = "toolStripButton5"
        Me.tbtnLevel4.ToolTipText = "Switch to Level 4 (Alt+4)"
        '
        'tbtnLevel5
        '
        Me.tbtnLevel5.AutoSize = False
        Me.tbtnLevel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel5.Image = Global.MacPacNumbering.My.Resources.Resources.Level5
        Me.tbtnLevel5.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel5.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel5.Name = "tbtnLevel5"
        Me.tbtnLevel5.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel5.Text = "toolStripButton5"
        Me.tbtnLevel5.ToolTipText = "Switch to Level 5 (Alt+5)"
        '
        'tbtnLevel6
        '
        Me.tbtnLevel6.AutoSize = False
        Me.tbtnLevel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel6.Image = Global.MacPacNumbering.My.Resources.Resources.Level6
        Me.tbtnLevel6.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel6.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel6.Name = "tbtnLevel6"
        Me.tbtnLevel6.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel6.Text = "toolStripButton5"
        Me.tbtnLevel6.ToolTipText = "Switch to Level 6 (Alt+6)"
        '
        'tbtnLevel7
        '
        Me.tbtnLevel7.AutoSize = False
        Me.tbtnLevel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel7.Image = Global.MacPacNumbering.My.Resources.Resources.Level7
        Me.tbtnLevel7.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel7.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel7.Name = "tbtnLevel7"
        Me.tbtnLevel7.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel7.Text = "toolStripButton5"
        Me.tbtnLevel7.ToolTipText = "Switch to Level 7 (Alt+7)"
        '
        'tbtnLevel8
        '
        Me.tbtnLevel8.AutoSize = False
        Me.tbtnLevel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel8.Image = Global.MacPacNumbering.My.Resources.Resources.Level8
        Me.tbtnLevel8.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel8.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel8.Name = "tbtnLevel8"
        Me.tbtnLevel8.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel8.Text = "toolStripButton5"
        Me.tbtnLevel8.ToolTipText = "Switch to Level 8 (Alt+8)"
        '
        'tbtnLevel9
        '
        Me.tbtnLevel9.AutoSize = False
        Me.tbtnLevel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tbtnLevel9.CheckOnClick = True
        Me.tbtnLevel9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbtnLevel9.Image = Global.MacPacNumbering.My.Resources.Resources.Level9
        Me.tbtnLevel9.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnLevel9.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.tbtnLevel9.Name = "tbtnLevel9"
        Me.tbtnLevel9.Size = New System.Drawing.Size(24, 22)
        Me.tbtnLevel9.Text = "toolStripButton5"
        Me.tbtnLevel9.ToolTipText = "Switch to Level 9 (Alt+9)"
        '
        'cmdAddLevel
        '
        Me.cmdAddLevel.AutoSize = False
        Me.cmdAddLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdAddLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.cmdAddLevel.Image = Global.MacPacNumbering.My.Resources.Resources.plus
        Me.cmdAddLevel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdAddLevel.Margin = New System.Windows.Forms.Padding(1, 12, 0, 4)
        Me.cmdAddLevel.Name = "cmdAddLevel"
        Me.cmdAddLevel.Size = New System.Drawing.Size(24, 22)
        Me.cmdAddLevel.Text = "toolStripButton5"
        Me.cmdAddLevel.ToolTipText = "Add level (Alt++)"
        '
        'cmdDeleteLevel
        '
        Me.cmdDeleteLevel.AutoSize = False
        Me.cmdDeleteLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.cmdDeleteLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.cmdDeleteLevel.Image = Global.MacPacNumbering.My.Resources.Resources.minus
        Me.cmdDeleteLevel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdDeleteLevel.Margin = New System.Windows.Forms.Padding(1, 0, 0, 4)
        Me.cmdDeleteLevel.Name = "cmdDeleteLevel"
        Me.cmdDeleteLevel.Size = New System.Drawing.Size(24, 22)
        Me.cmdDeleteLevel.Text = "toolStripButton5"
        Me.cmdDeleteLevel.ToolTipText = "Delete level (Alt+-)"
        '
        'splitContainer1
        '
        Me.splitContainer1.IsSplitterFixed = True
        Me.splitContainer1.Location = New System.Drawing.Point(0, 37)
        Me.splitContainer1.Name = "splitContainer1"
        '
        'splitContainer1.Panel1
        '
        Me.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.splitContainer1.Panel1.Controls.Add(Me.pnlLevels)
        '
        'splitContainer1.Panel2
        '
        Me.splitContainer1.Panel2.Controls.Add(Me.tabControl1)
        Me.splitContainer1.Size = New System.Drawing.Size(543, 376)
        Me.splitContainer1.SplitterDistance = 47
        Me.splitContainer1.SplitterWidth = 1
        Me.splitContainer1.TabIndex = 20
        Me.splitContainer1.TabStop = False
        '
        'tabControl1
        '
        Me.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tabControl1.Controls.Add(Me.tabPage1)
        Me.tabControl1.Controls.Add(Me.tabPage2)
        Me.tabControl1.Controls.Add(Me.tabPage3)
        Me.tabControl1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabControl1.Location = New System.Drawing.Point(2, -36)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(493, 412)
        Me.tabControl1.TabIndex = 15
        '
        'tabPage1
        '
        Me.tabPage1.Controls.Add(Me.pnlButtons)
        Me.tabPage1.Location = New System.Drawing.Point(4, 27)
        Me.tabPage1.Name = "tabPage1"
        Me.tabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage1.Size = New System.Drawing.Size(485, 381)
        Me.tabPage1.TabIndex = 0
        Me.tabPage1.Text = "Number"
        Me.tabPage1.UseVisualStyleBackColor = True
        '
        'pnlButtons
        '
        Me.pnlButtons.Controls.Add(Me.pnlNumberFont)
        Me.pnlButtons.Controls.Add(Me.pnlNumber)
        Me.pnlButtons.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlButtons.Location = New System.Drawing.Point(3, 3)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(479, 375)
        Me.pnlButtons.TabIndex = 13
        '
        'pnlNumberFont
        '
        Me.pnlNumberFont.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlNumberFont.Controls.Add(Me.cmbNumFontSize)
        Me.pnlNumberFont.Controls.Add(Me.cmbNumFontName)
        Me.pnlNumberFont.Controls.Add(Me.cmbUnderlineStyle)
        Me.pnlNumberFont.Controls.Add(Me.lblNumFontSize)
        Me.pnlNumberFont.Controls.Add(Me.lblNumFontName)
        Me.pnlNumberFont.Controls.Add(Me.chkNumberCaps)
        Me.pnlNumberFont.Controls.Add(Me.chkNumberItalic)
        Me.pnlNumberFont.Controls.Add(Me.chkNumberBold)
        Me.pnlNumberFont.Controls.Add(Me.lblNumFontUnderline)
        Me.pnlNumberFont.Location = New System.Drawing.Point(4, 241)
        Me.pnlNumberFont.Name = "pnlNumberFont"
        Me.pnlNumberFont.Size = New System.Drawing.Size(451, 125)
        Me.pnlNumberFont.TabIndex = 13
        Me.pnlNumberFont.TabStop = False
        Me.pnlNumberFont.Text = "Font"
        '
        'cmbNumFontSize
        '
        Me.cmbNumFontSize.AllowEmptyValue = False
        Me.cmbNumFontSize.AutoSize = True
        Me.cmbNumFontSize.Borderless = False
        Me.cmbNumFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumFontSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumFontSize.IsDirty = False
        Me.cmbNumFontSize.LimitToList = False
        Me.cmbNumFontSize.Location = New System.Drawing.Point(252, 86)
        Me.cmbNumFontSize.MaxDropDownItems = 8
        Me.cmbNumFontSize.Name = "cmbNumFontSize"
        Me.cmbNumFontSize.SelectedIndex = -1
        Me.cmbNumFontSize.SelectedValue = Nothing
        Me.cmbNumFontSize.SelectionLength = 0
        Me.cmbNumFontSize.SelectionStart = 0
        Me.cmbNumFontSize.Size = New System.Drawing.Size(182, 23)
        Me.cmbNumFontSize.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumFontSize.SupportingValues = ""
        Me.cmbNumFontSize.TabIndex = 22
        Me.cmbNumFontSize.Tag2 = Nothing
        Me.cmbNumFontSize.Value = ""
        '
        'cmbNumFontName
        '
        Me.cmbNumFontName.AllowEmptyValue = False
        Me.cmbNumFontName.AutoSize = True
        Me.cmbNumFontName.Borderless = False
        Me.cmbNumFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumFontName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumFontName.IsDirty = False
        Me.cmbNumFontName.LimitToList = True
        Me.cmbNumFontName.Location = New System.Drawing.Point(253, 37)
        Me.cmbNumFontName.MaxDropDownItems = 8
        Me.cmbNumFontName.Name = "cmbNumFontName"
        Me.cmbNumFontName.SelectedIndex = -1
        Me.cmbNumFontName.SelectedValue = Nothing
        Me.cmbNumFontName.SelectionLength = 0
        Me.cmbNumFontName.SelectionStart = 0
        Me.cmbNumFontName.Size = New System.Drawing.Size(180, 23)
        Me.cmbNumFontName.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumFontName.SupportingValues = ""
        Me.cmbNumFontName.TabIndex = 20
        Me.cmbNumFontName.Tag2 = Nothing
        Me.cmbNumFontName.Value = ""
        '
        'cmbUnderlineStyle
        '
        Me.cmbUnderlineStyle.AllowEmptyValue = False
        Me.cmbUnderlineStyle.AutoSize = True
        Me.cmbUnderlineStyle.Borderless = False
        Me.cmbUnderlineStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbUnderlineStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbUnderlineStyle.IsDirty = False
        Me.cmbUnderlineStyle.LimitToList = True
        Me.cmbUnderlineStyle.Location = New System.Drawing.Point(16, 86)
        Me.cmbUnderlineStyle.MaxDropDownItems = 8
        Me.cmbUnderlineStyle.Name = "cmbUnderlineStyle"
        Me.cmbUnderlineStyle.SelectedIndex = -1
        Me.cmbUnderlineStyle.SelectedValue = Nothing
        Me.cmbUnderlineStyle.SelectionLength = 0
        Me.cmbUnderlineStyle.SelectionStart = 0
        Me.cmbUnderlineStyle.Size = New System.Drawing.Size(176, 23)
        Me.cmbUnderlineStyle.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbUnderlineStyle.SupportingValues = ""
        Me.cmbUnderlineStyle.TabIndex = 18
        Me.cmbUnderlineStyle.Tag2 = Nothing
        Me.cmbUnderlineStyle.Value = ""
        '
        'lblNumFontSize
        '
        Me.lblNumFontSize.AutoSize = True
        Me.lblNumFontSize.Location = New System.Drawing.Point(254, 69)
        Me.lblNumFontSize.Name = "lblNumFontSize"
        Me.lblNumFontSize.Size = New System.Drawing.Size(30, 15)
        Me.lblNumFontSize.TabIndex = 21
        Me.lblNumFontSize.Text = "&Size:"
        '
        'lblNumFontName
        '
        Me.lblNumFontName.AutoSize = True
        Me.lblNumFontName.Location = New System.Drawing.Point(253, 20)
        Me.lblNumFontName.Name = "lblNumFontName"
        Me.lblNumFontName.Size = New System.Drawing.Size(42, 15)
        Me.lblNumFontName.TabIndex = 19
        Me.lblNumFontName.Text = "N&ame:"
        '
        'chkNumberCaps
        '
        Me.chkNumberCaps.AutoSize = True
        Me.chkNumberCaps.Location = New System.Drawing.Point(123, 39)
        Me.chkNumberCaps.Name = "chkNumberCaps"
        Me.chkNumberCaps.Size = New System.Drawing.Size(69, 19)
        Me.chkNumberCaps.TabIndex = 16
        Me.chkNumberCaps.Text = "All &Caps"
        Me.chkNumberCaps.UseVisualStyleBackColor = True
        '
        'chkNumberItalic
        '
        Me.chkNumberItalic.AutoSize = True
        Me.chkNumberItalic.Location = New System.Drawing.Point(72, 39)
        Me.chkNumberItalic.Name = "chkNumberItalic"
        Me.chkNumberItalic.Size = New System.Drawing.Size(51, 19)
        Me.chkNumberItalic.TabIndex = 15
        Me.chkNumberItalic.Text = "&Italic"
        Me.chkNumberItalic.UseVisualStyleBackColor = True
        '
        'chkNumberBold
        '
        Me.chkNumberBold.AutoSize = True
        Me.chkNumberBold.Location = New System.Drawing.Point(16, 39)
        Me.chkNumberBold.Name = "chkNumberBold"
        Me.chkNumberBold.Size = New System.Drawing.Size(50, 19)
        Me.chkNumberBold.TabIndex = 14
        Me.chkNumberBold.Text = "&Bold"
        Me.chkNumberBold.UseVisualStyleBackColor = True
        '
        'lblNumFontUnderline
        '
        Me.lblNumFontUnderline.AutoSize = True
        Me.lblNumFontUnderline.Location = New System.Drawing.Point(17, 69)
        Me.lblNumFontUnderline.Name = "lblNumFontUnderline"
        Me.lblNumFontUnderline.Size = New System.Drawing.Size(61, 15)
        Me.lblNumFontUnderline.TabIndex = 17
        Me.lblNumFontUnderline.Text = "&Underline:"
        '
        'pnlNumber
        '
        Me.pnlNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlNumber.Controls.Add(Me.txtStartAt)
        Me.pnlNumber.Controls.Add(Me.lstPrevLevels)
        Me.pnlNumber.Controls.Add(Me.spnTabPosition)
        Me.pnlNumber.Controls.Add(Me.cmbTrailingChar)
        Me.pnlNumber.Controls.Add(Me.cmbNumberStyle)
        Me.pnlNumber.Controls.Add(Me.rtfNumberFormat)
        Me.pnlNumber.Controls.Add(Me.chkNonbreakingSpaces)
        Me.pnlNumber.Controls.Add(Me.chkRightAlign)
        Me.pnlNumber.Controls.Add(Me.chkLegalStyle)
        Me.pnlNumber.Controls.Add(Me.chkReset)
        Me.pnlNumber.Controls.Add(Me.btnAddPrevLevel)
        Me.pnlNumber.Controls.Add(Me.lblTabPosition)
        Me.pnlNumber.Controls.Add(Me.lblStartAt)
        Me.pnlNumber.Controls.Add(Me.lblTrailingChar)
        Me.pnlNumber.Controls.Add(Me.lbNumberStyle)
        Me.pnlNumber.Controls.Add(Me.lblNumberFormat)
        Me.pnlNumber.Controls.Add(Me.lblPreviousLevels)
        Me.pnlNumber.Controls.Add(Me.udStartAt)
        Me.pnlNumber.Location = New System.Drawing.Point(3, 7)
        Me.pnlNumber.Name = "pnlNumber"
        Me.pnlNumber.Size = New System.Drawing.Size(452, 231)
        Me.pnlNumber.TabIndex = 12
        Me.pnlNumber.TabStop = False
        Me.pnlNumber.Text = "Number"
        '
        'txtStartAt
        '
        Me.txtStartAt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStartAt.Location = New System.Drawing.Point(165, 90)
        Me.txtStartAt.Name = "txtStartAt"
        Me.txtStartAt.Size = New System.Drawing.Size(26, 20)
        Me.txtStartAt.TabIndex = 5
        '
        'lstPrevLevels
        '
        Me.lstPrevLevels.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.LevelName, Me.LevelValue})
        Me.lstPrevLevels.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstPrevLevels.FullRowSelect = True
        Me.lstPrevLevels.GridLines = True
        Me.lstPrevLevels.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstPrevLevels.HideSelection = False
        Me.lstPrevLevels.Location = New System.Drawing.Point(254, 37)
        Me.lstPrevLevels.Margin = New System.Windows.Forms.Padding(3, 1, 3, 1)
        Me.lstPrevLevels.Name = "lstPrevLevels"
        Me.lstPrevLevels.Scrollable = False
        Me.lstPrevLevels.ShowItemToolTips = True
        Me.lstPrevLevels.Size = New System.Drawing.Size(180, 139)
        Me.lstPrevLevels.TabIndex = 12
        Me.lstPrevLevels.UseCompatibleStateImageBehavior = False
        Me.lstPrevLevels.View = System.Windows.Forms.View.Details
        '
        'LevelName
        '
        Me.LevelName.Text = "Name"
        Me.LevelName.Width = 100
        '
        'LevelValue
        '
        Me.LevelValue.Text = "Value"
        Me.LevelValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'spnTabPosition
        '
        Me.spnTabPosition.AllowFractions = True
        Me.spnTabPosition.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnTabPosition.AppendSymbol = True
        Me.spnTabPosition.AutoSize = True
        Me.spnTabPosition.DisplayText = "0"""
        Me.spnTabPosition.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnTabPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnTabPosition.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnTabPosition.Location = New System.Drawing.Point(165, 142)
        Me.spnTabPosition.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnTabPosition.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnTabPosition.Name = "spnTabPosition"
        Me.spnTabPosition.SelLength = 0
        Me.spnTabPosition.SelStart = 0
        Me.spnTabPosition.SelText = ""
        Me.spnTabPosition.Size = New System.Drawing.Size(44, 20)
        Me.spnTabPosition.TabIndex = 9
        Me.spnTabPosition.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnTabPosition.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'cmbTrailingChar
        '
        Me.cmbTrailingChar.AllowEmptyValue = False
        Me.cmbTrailingChar.AutoSize = True
        Me.cmbTrailingChar.Borderless = False
        Me.cmbTrailingChar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbTrailingChar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTrailingChar.IsDirty = False
        Me.cmbTrailingChar.LimitToList = True
        Me.cmbTrailingChar.Location = New System.Drawing.Point(17, 142)
        Me.cmbTrailingChar.MaxDropDownItems = 8
        Me.cmbTrailingChar.Name = "cmbTrailingChar"
        Me.cmbTrailingChar.SelectedIndex = -1
        Me.cmbTrailingChar.SelectedValue = Nothing
        Me.cmbTrailingChar.SelectionLength = 0
        Me.cmbTrailingChar.SelectionStart = 0
        Me.cmbTrailingChar.Size = New System.Drawing.Size(139, 23)
        Me.cmbTrailingChar.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbTrailingChar.SupportingValues = ""
        Me.cmbTrailingChar.TabIndex = 7
        Me.cmbTrailingChar.Tag2 = Nothing
        Me.cmbTrailingChar.Value = ""
        '
        'cmbNumberStyle
        '
        Me.cmbNumberStyle.AllowEmptyValue = False
        Me.cmbNumberStyle.AutoSize = True
        Me.cmbNumberStyle.Borderless = False
        Me.cmbNumberStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumberStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumberStyle.IsDirty = False
        Me.cmbNumberStyle.LimitToList = True
        Me.cmbNumberStyle.Location = New System.Drawing.Point(17, 90)
        Me.cmbNumberStyle.MaxDropDownItems = 8
        Me.cmbNumberStyle.Name = "cmbNumberStyle"
        Me.cmbNumberStyle.SelectedIndex = -1
        Me.cmbNumberStyle.SelectedValue = Nothing
        Me.cmbNumberStyle.SelectionLength = 0
        Me.cmbNumberStyle.SelectionStart = 0
        Me.cmbNumberStyle.Size = New System.Drawing.Size(139, 23)
        Me.cmbNumberStyle.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumberStyle.SupportingValues = ""
        Me.cmbNumberStyle.TabIndex = 3
        Me.cmbNumberStyle.Tag2 = Nothing
        Me.cmbNumberStyle.Value = ""
        '
        'rtfNumberFormat
        '
        Me.rtfNumberFormat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rtfNumberFormat.Location = New System.Drawing.Point(17, 37)
        Me.rtfNumberFormat.Name = "rtfNumberFormat"
        Me.rtfNumberFormat.Size = New System.Drawing.Size(190, 23)
        Me.rtfNumberFormat.TabIndex = 1
        Me.rtfNumberFormat.Text = ""
        '
        'chkNonbreakingSpaces
        '
        Me.chkNonbreakingSpaces.AutoSize = True
        Me.chkNonbreakingSpaces.Location = New System.Drawing.Point(255, 203)
        Me.chkNonbreakingSpaces.Name = "chkNonbreakingSpaces"
        Me.chkNonbreakingSpaces.Size = New System.Drawing.Size(153, 19)
        Me.chkNonbreakingSpaces.TabIndex = 13
        Me.chkNonbreakingSpaces.Text = "Use nonbr&eaking spaces"
        Me.chkNonbreakingSpaces.UseVisualStyleBackColor = True
        '
        'chkRightAlign
        '
        Me.chkRightAlign.AutoSize = True
        Me.chkRightAlign.Location = New System.Drawing.Point(255, 182)
        Me.chkRightAlign.Name = "chkRightAlign"
        Me.chkRightAlign.Size = New System.Drawing.Size(128, 19)
        Me.chkRightAlign.TabIndex = 12
        Me.chkRightAlign.Text = "Right &align number"
        Me.chkRightAlign.UseVisualStyleBackColor = True
        '
        'chkLegalStyle
        '
        Me.chkLegalStyle.AutoSize = True
        Me.chkLegalStyle.Location = New System.Drawing.Point(18, 203)
        Me.chkLegalStyle.Name = "chkLegalStyle"
        Me.chkLegalStyle.Size = New System.Drawing.Size(143, 19)
        Me.chkLegalStyle.TabIndex = 11
        Me.chkLegalStyle.Text = "&Legal style numbering"
        Me.chkLegalStyle.UseVisualStyleBackColor = True
        '
        'chkReset
        '
        Me.chkReset.AutoSize = True
        Me.chkReset.Location = New System.Drawing.Point(18, 182)
        Me.chkReset.Name = "chkReset"
        Me.chkReset.Size = New System.Drawing.Size(215, 19)
        Me.chkReset.TabIndex = 10
        Me.chkReset.Text = "Restart numbering after &higher level"
        Me.chkReset.UseVisualStyleBackColor = True
        '
        'btnAddPrevLevel
        '
        Me.btnAddPrevLevel.BackgroundImage = Global.MacPacNumbering.My.Resources.Resources.LeftArrow
        Me.btnAddPrevLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddPrevLevel.FlatAppearance.BorderSize = 0
        Me.btnAddPrevLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddPrevLevel.Location = New System.Drawing.Point(218, 37)
        Me.btnAddPrevLevel.Name = "btnAddPrevLevel"
        Me.btnAddPrevLevel.Size = New System.Drawing.Size(24, 24)
        Me.btnAddPrevLevel.TabIndex = 12
        Me.btnAddPrevLevel.UseVisualStyleBackColor = True
        '
        'lblTabPosition
        '
        Me.lblTabPosition.AutoSize = True
        Me.lblTabPosition.Location = New System.Drawing.Point(163, 124)
        Me.lblTabPosition.Name = "lblTabPosition"
        Me.lblTabPosition.Size = New System.Drawing.Size(42, 15)
        Me.lblTabPosition.TabIndex = 8
        Me.lblTabPosition.Text = "Wid&th:"
        '
        'lblStartAt
        '
        Me.lblStartAt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblStartAt.AutoSize = True
        Me.lblStartAt.Location = New System.Drawing.Point(162, 70)
        Me.lblStartAt.Name = "lblStartAt"
        Me.lblStartAt.Size = New System.Drawing.Size(49, 15)
        Me.lblStartAt.TabIndex = 4
        Me.lblStartAt.Text = "&Start At:"
        '
        'lblTrailingChar
        '
        Me.lblTrailingChar.AutoSize = True
        Me.lblTrailingChar.Location = New System.Drawing.Point(16, 124)
        Me.lblTrailingChar.Name = "lblTrailingChar"
        Me.lblTrailingChar.Size = New System.Drawing.Size(116, 15)
        Me.lblTrailingChar.TabIndex = 6
        Me.lblTrailingChar.Text = "&Follow number with:"
        '
        'lbNumberStyle
        '
        Me.lbNumberStyle.AutoSize = True
        Me.lbNumberStyle.Location = New System.Drawing.Point(16, 70)
        Me.lbNumberStyle.Name = "lbNumberStyle"
        Me.lbNumberStyle.Size = New System.Drawing.Size(35, 15)
        Me.lbNumberStyle.TabIndex = 2
        Me.lbNumberStyle.Text = "St&yle:"
        '
        'lblNumberFormat
        '
        Me.lblNumberFormat.AutoSize = True
        Me.lblNumberFormat.Location = New System.Drawing.Point(16, 20)
        Me.lblNumberFormat.Name = "lblNumberFormat"
        Me.lblNumberFormat.Size = New System.Drawing.Size(48, 15)
        Me.lblNumberFormat.TabIndex = 0
        Me.lblNumberFormat.Text = "For&mat:"
        '
        'lblPreviousLevels
        '
        Me.lblPreviousLevels.AutoSize = True
        Me.lblPreviousLevels.Location = New System.Drawing.Point(252, 20)
        Me.lblPreviousLevels.Name = "lblPreviousLevels"
        Me.lblPreviousLevels.Size = New System.Drawing.Size(90, 15)
        Me.lblPreviousLevels.TabIndex = 11
        Me.lblPreviousLevels.Text = "Pre&vious Levels:"
        '
        'udStartAt
        '
        Me.udStartAt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udStartAt.Location = New System.Drawing.Point(191, 89)
        Me.udStartAt.Name = "udStartAt"
        Me.udStartAt.Size = New System.Drawing.Size(18, 20)
        Me.udStartAt.TabIndex = 6
        '
        'tabPage2
        '
        Me.tabPage2.Controls.Add(Me.pnlParagraph)
        Me.tabPage2.Location = New System.Drawing.Point(4, 27)
        Me.tabPage2.Name = "tabPage2"
        Me.tabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage2.Size = New System.Drawing.Size(485, 381)
        Me.tabPage2.TabIndex = 1
        Me.tabPage2.Text = "Paragraph"
        Me.tabPage2.UseVisualStyleBackColor = True
        '
        'pnlParagraph
        '
        Me.pnlParagraph.Controls.Add(Me.pnlFont)
        Me.pnlParagraph.Controls.Add(Me.pnlPosition)
        Me.pnlParagraph.Controls.Add(Me.pnlOther)
        Me.pnlParagraph.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlParagraph.Location = New System.Drawing.Point(3, 3)
        Me.pnlParagraph.Name = "pnlParagraph"
        Me.pnlParagraph.Size = New System.Drawing.Size(479, 375)
        Me.pnlParagraph.TabIndex = 14
        '
        'pnlFont
        '
        Me.pnlFont.Controls.Add(Me.optStyleFormats)
        Me.pnlFont.Controls.Add(Me.optDirectFormats)
        Me.pnlFont.Controls.Add(Me.chkTCCaps)
        Me.pnlFont.Controls.Add(Me.chkTCSmallCaps)
        Me.pnlFont.Controls.Add(Me.chkTCUnderline)
        Me.pnlFont.Controls.Add(Me.chkTCItalic)
        Me.pnlFont.Controls.Add(Me.chkTCBold)
        Me.pnlFont.Location = New System.Drawing.Point(3, 224)
        Me.pnlFont.Name = "pnlFont"
        Me.pnlFont.Size = New System.Drawing.Size(237, 152)
        Me.pnlFont.TabIndex = 2
        Me.pnlFont.TabStop = False
        Me.pnlFont.Text = "Heading"
        '
        'optStyleFormats
        '
        Me.optStyleFormats.AutoSize = True
        Me.optStyleFormats.Location = New System.Drawing.Point(15, 118)
        Me.optStyleFormats.Name = "optStyleFormats"
        Me.optStyleFormats.Size = New System.Drawing.Size(212, 19)
        Me.optStyleFormats.TabIndex = 52
        Me.optStyleFormats.TabStop = True
        Me.optStyleFormats.Text = "Sa&ve in style (Stand-Alone heading)"
        Me.optStyleFormats.UseVisualStyleBackColor = True
        '
        'optDirectFormats
        '
        Me.optDirectFormats.AutoSize = True
        Me.optDirectFormats.Location = New System.Drawing.Point(15, 97)
        Me.optDirectFormats.Name = "optDirectFormats"
        Me.optDirectFormats.Size = New System.Drawing.Size(190, 19)
        Me.optDirectFormats.TabIndex = 51
        Me.optDirectFormats.TabStop = True
        Me.optDirectFormats.Text = "Direct &format (Run-in Heading)"
        Me.optDirectFormats.UseVisualStyleBackColor = True
        '
        'chkTCCaps
        '
        Me.chkTCCaps.AutoSize = True
        Me.chkTCCaps.Location = New System.Drawing.Point(119, 45)
        Me.chkTCCaps.Name = "chkTCCaps"
        Me.chkTCCaps.Size = New System.Drawing.Size(69, 19)
        Me.chkTCCaps.TabIndex = 50
        Me.chkTCCaps.Text = "All &Caps"
        Me.chkTCCaps.UseVisualStyleBackColor = True
        '
        'chkTCSmallCaps
        '
        Me.chkTCSmallCaps.AutoSize = True
        Me.chkTCSmallCaps.Location = New System.Drawing.Point(119, 23)
        Me.chkTCSmallCaps.Name = "chkTCSmallCaps"
        Me.chkTCSmallCaps.Size = New System.Drawing.Size(84, 19)
        Me.chkTCSmallCaps.TabIndex = 49
        Me.chkTCSmallCaps.Text = "S&mall Caps"
        Me.chkTCSmallCaps.UseVisualStyleBackColor = True
        '
        'chkTCUnderline
        '
        Me.chkTCUnderline.AutoSize = True
        Me.chkTCUnderline.Location = New System.Drawing.Point(15, 68)
        Me.chkTCUnderline.Name = "chkTCUnderline"
        Me.chkTCUnderline.Size = New System.Drawing.Size(77, 19)
        Me.chkTCUnderline.TabIndex = 48
        Me.chkTCUnderline.Text = "&Underline"
        Me.chkTCUnderline.UseVisualStyleBackColor = True
        '
        'chkTCItalic
        '
        Me.chkTCItalic.AutoSize = True
        Me.chkTCItalic.Location = New System.Drawing.Point(15, 45)
        Me.chkTCItalic.Name = "chkTCItalic"
        Me.chkTCItalic.Size = New System.Drawing.Size(51, 19)
        Me.chkTCItalic.TabIndex = 47
        Me.chkTCItalic.Text = "&Italic"
        Me.chkTCItalic.UseVisualStyleBackColor = True
        '
        'chkTCBold
        '
        Me.chkTCBold.AutoSize = True
        Me.chkTCBold.Location = New System.Drawing.Point(15, 23)
        Me.chkTCBold.Name = "chkTCBold"
        Me.chkTCBold.Size = New System.Drawing.Size(50, 19)
        Me.chkTCBold.TabIndex = 46
        Me.chkTCBold.Text = "&Bold"
        Me.chkTCBold.UseVisualStyleBackColor = True
        '
        'pnlPosition
        '
        Me.pnlPosition.Controls.Add(Me.spnRightIndent)
        Me.pnlPosition.Controls.Add(Me.spnTextPosition)
        Me.pnlPosition.Controls.Add(Me.spnNumberPosition)
        Me.pnlPosition.Controls.Add(Me.spnSpaceAfter)
        Me.pnlPosition.Controls.Add(Me.spnSpaceBefore)
        Me.pnlPosition.Controls.Add(Me.spnAt)
        Me.pnlPosition.Controls.Add(Me.cmbTextAlign)
        Me.pnlPosition.Controls.Add(Me.cmbLineSpacing)
        Me.pnlPosition.Controls.Add(Me.cmbFontSize)
        Me.pnlPosition.Controls.Add(Me.cmbFontName)
        Me.pnlPosition.Controls.Add(Me.cmdSynchronize)
        Me.pnlPosition.Controls.Add(Me.lblSynchronize)
        Me.pnlPosition.Controls.Add(Me.lblFontSize)
        Me.pnlPosition.Controls.Add(Me.lblLineSpacing)
        Me.pnlPosition.Controls.Add(Me.lblFontName)
        Me.pnlPosition.Controls.Add(Me.lblTextAlign)
        Me.pnlPosition.Controls.Add(Me.lblAt)
        Me.pnlPosition.Controls.Add(Me.lblSpaceAfter)
        Me.pnlPosition.Controls.Add(Me.lblSpaceBefore)
        Me.pnlPosition.Controls.Add(Me.lblRightIndent)
        Me.pnlPosition.Controls.Add(Me.lblTextPosition)
        Me.pnlPosition.Controls.Add(Me.lblNumberPosition)
        Me.pnlPosition.Location = New System.Drawing.Point(3, 7)
        Me.pnlPosition.Name = "pnlPosition"
        Me.pnlPosition.Size = New System.Drawing.Size(451, 215)
        Me.pnlPosition.TabIndex = 11
        Me.pnlPosition.TabStop = False
        Me.pnlPosition.Text = "Format"
        '
        'spnRightIndent
        '
        Me.spnRightIndent.AllowFractions = True
        Me.spnRightIndent.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnRightIndent.AppendSymbol = True
        Me.spnRightIndent.AutoSize = True
        Me.spnRightIndent.DisplayText = "0"""
        Me.spnRightIndent.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnRightIndent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnRightIndent.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnRightIndent.Location = New System.Drawing.Point(18, 126)
        Me.spnRightIndent.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnRightIndent.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnRightIndent.Name = "spnRightIndent"
        Me.spnRightIndent.SelLength = 0
        Me.spnRightIndent.SelStart = 0
        Me.spnRightIndent.SelText = ""
        Me.spnRightIndent.Size = New System.Drawing.Size(125, 20)
        Me.spnRightIndent.TabIndex = 29
        Me.spnRightIndent.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnRightIndent.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnTextPosition
        '
        Me.spnTextPosition.AllowFractions = True
        Me.spnTextPosition.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnTextPosition.AppendSymbol = True
        Me.spnTextPosition.AutoSize = True
        Me.spnTextPosition.DisplayText = "0"""
        Me.spnTextPosition.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnTextPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnTextPosition.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnTextPosition.Location = New System.Drawing.Point(18, 82)
        Me.spnTextPosition.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnTextPosition.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnTextPosition.Name = "spnTextPosition"
        Me.spnTextPosition.SelLength = 0
        Me.spnTextPosition.SelStart = 0
        Me.spnTextPosition.SelText = ""
        Me.spnTextPosition.Size = New System.Drawing.Size(125, 20)
        Me.spnTextPosition.TabIndex = 27
        Me.spnTextPosition.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnTextPosition.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnNumberPosition
        '
        Me.spnNumberPosition.AllowFractions = True
        Me.spnNumberPosition.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnNumberPosition.AppendSymbol = True
        Me.spnNumberPosition.AutoSize = True
        Me.spnNumberPosition.DisplayText = "0"""
        Me.spnNumberPosition.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnNumberPosition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnNumberPosition.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnNumberPosition.Location = New System.Drawing.Point(18, 37)
        Me.spnNumberPosition.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnNumberPosition.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnNumberPosition.Name = "spnNumberPosition"
        Me.spnNumberPosition.SelLength = 0
        Me.spnNumberPosition.SelStart = 0
        Me.spnNumberPosition.SelText = ""
        Me.spnNumberPosition.Size = New System.Drawing.Size(125, 20)
        Me.spnNumberPosition.TabIndex = 25
        Me.spnNumberPosition.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnNumberPosition.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnSpaceAfter
        '
        Me.spnSpaceAfter.AllowFractions = False
        Me.spnSpaceAfter.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnSpaceAfter.AppendSymbol = True
        Me.spnSpaceAfter.AutoSize = True
        Me.spnSpaceAfter.DisplayText = "0 pt"
        Me.spnSpaceAfter.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnSpaceAfter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnSpaceAfter.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnSpaceAfter.Location = New System.Drawing.Point(265, 126)
        Me.spnSpaceAfter.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnSpaceAfter.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnSpaceAfter.Name = "spnSpaceAfter"
        Me.spnSpaceAfter.SelLength = 0
        Me.spnSpaceAfter.SelStart = 0
        Me.spnSpaceAfter.SelText = ""
        Me.spnSpaceAfter.Size = New System.Drawing.Size(63, 20)
        Me.spnSpaceAfter.TabIndex = 43
        Me.spnSpaceAfter.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnSpaceAfter.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnSpaceBefore
        '
        Me.spnSpaceBefore.AllowFractions = False
        Me.spnSpaceBefore.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnSpaceBefore.AppendSymbol = True
        Me.spnSpaceBefore.AutoSize = True
        Me.spnSpaceBefore.DisplayText = "0 pt"
        Me.spnSpaceBefore.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnSpaceBefore.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnSpaceBefore.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnSpaceBefore.Location = New System.Drawing.Point(159, 126)
        Me.spnSpaceBefore.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnSpaceBefore.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnSpaceBefore.Name = "spnSpaceBefore"
        Me.spnSpaceBefore.SelLength = 0
        Me.spnSpaceBefore.SelStart = 0
        Me.spnSpaceBefore.SelText = ""
        Me.spnSpaceBefore.Size = New System.Drawing.Size(71, 20)
        Me.spnSpaceBefore.TabIndex = 41
        Me.spnSpaceBefore.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnSpaceBefore.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnAt
        '
        Me.spnAt.AllowFractions = False
        Me.spnAt.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnAt.AppendSymbol = True
        Me.spnAt.AutoSize = True
        Me.spnAt.DisplayText = "0 pt"
        Me.spnAt.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnAt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnAt.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnAt.Location = New System.Drawing.Point(344, 82)
        Me.spnAt.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnAt.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnAt.Name = "spnAt"
        Me.spnAt.SelLength = 0
        Me.spnAt.SelStart = 0
        Me.spnAt.SelText = ""
        Me.spnAt.Size = New System.Drawing.Size(92, 20)
        Me.spnAt.TabIndex = 39
        Me.spnAt.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnAt.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'cmbTextAlign
        '
        Me.cmbTextAlign.AllowEmptyValue = False
        Me.cmbTextAlign.AutoSize = True
        Me.cmbTextAlign.Borderless = False
        Me.cmbTextAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbTextAlign.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTextAlign.IsDirty = False
        Me.cmbTextAlign.LimitToList = True
        Me.cmbTextAlign.Location = New System.Drawing.Point(18, 170)
        Me.cmbTextAlign.MaxDropDownItems = 8
        Me.cmbTextAlign.Name = "cmbTextAlign"
        Me.cmbTextAlign.SelectedIndex = -1
        Me.cmbTextAlign.SelectedValue = Nothing
        Me.cmbTextAlign.SelectionLength = 0
        Me.cmbTextAlign.SelectionStart = 0
        Me.cmbTextAlign.Size = New System.Drawing.Size(125, 23)
        Me.cmbTextAlign.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbTextAlign.SupportingValues = ""
        Me.cmbTextAlign.TabIndex = 31
        Me.cmbTextAlign.Tag2 = Nothing
        Me.cmbTextAlign.Value = ""
        '
        'cmbLineSpacing
        '
        Me.cmbLineSpacing.AllowEmptyValue = False
        Me.cmbLineSpacing.AutoSize = True
        Me.cmbLineSpacing.Borderless = False
        Me.cmbLineSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbLineSpacing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLineSpacing.IsDirty = False
        Me.cmbLineSpacing.LimitToList = True
        Me.cmbLineSpacing.Location = New System.Drawing.Point(159, 82)
        Me.cmbLineSpacing.MaxDropDownItems = 8
        Me.cmbLineSpacing.Name = "cmbLineSpacing"
        Me.cmbLineSpacing.SelectedIndex = -1
        Me.cmbLineSpacing.SelectedValue = Nothing
        Me.cmbLineSpacing.SelectionLength = 0
        Me.cmbLineSpacing.SelectionStart = 0
        Me.cmbLineSpacing.Size = New System.Drawing.Size(169, 23)
        Me.cmbLineSpacing.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbLineSpacing.SupportingValues = ""
        Me.cmbLineSpacing.TabIndex = 37
        Me.cmbLineSpacing.Tag2 = Nothing
        Me.cmbLineSpacing.Value = ""
        '
        'cmbFontSize
        '
        Me.cmbFontSize.AllowEmptyValue = False
        Me.cmbFontSize.AutoSize = True
        Me.cmbFontSize.Borderless = False
        Me.cmbFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbFontSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFontSize.IsDirty = False
        Me.cmbFontSize.LimitToList = False
        Me.cmbFontSize.Location = New System.Drawing.Point(344, 37)
        Me.cmbFontSize.MaxDropDownItems = 8
        Me.cmbFontSize.Name = "cmbFontSize"
        Me.cmbFontSize.SelectedIndex = -1
        Me.cmbFontSize.SelectedValue = Nothing
        Me.cmbFontSize.SelectionLength = 0
        Me.cmbFontSize.SelectionStart = 0
        Me.cmbFontSize.Size = New System.Drawing.Size(92, 23)
        Me.cmbFontSize.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbFontSize.SupportingValues = ""
        Me.cmbFontSize.TabIndex = 35
        Me.cmbFontSize.Tag2 = Nothing
        Me.cmbFontSize.Value = ""
        '
        'cmbFontName
        '
        Me.cmbFontName.AllowEmptyValue = False
        Me.cmbFontName.AutoSize = True
        Me.cmbFontName.Borderless = False
        Me.cmbFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbFontName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFontName.IsDirty = False
        Me.cmbFontName.LimitToList = True
        Me.cmbFontName.Location = New System.Drawing.Point(159, 37)
        Me.cmbFontName.MaxDropDownItems = 8
        Me.cmbFontName.Name = "cmbFontName"
        Me.cmbFontName.SelectedIndex = -1
        Me.cmbFontName.SelectedValue = Nothing
        Me.cmbFontName.SelectionLength = 0
        Me.cmbFontName.SelectionStart = 0
        Me.cmbFontName.Size = New System.Drawing.Size(169, 23)
        Me.cmbFontName.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbFontName.SupportingValues = ""
        Me.cmbFontName.TabIndex = 33
        Me.cmbFontName.Tag2 = Nothing
        Me.cmbFontName.Value = ""
        '
        'cmdSynchronize
        '
        Me.cmdSynchronize.Location = New System.Drawing.Point(346, 171)
        Me.cmdSynchronize.Name = "cmdSynchronize"
        Me.cmdSynchronize.Size = New System.Drawing.Size(75, 23)
        Me.cmdSynchronize.TabIndex = 45
        Me.cmdSynchronize.Text = "&Synch"
        Me.cmdSynchronize.UseVisualStyleBackColor = True
        '
        'lblSynchronize
        '
        Me.lblSynchronize.AutoSize = True
        Me.lblSynchronize.Location = New System.Drawing.Point(154, 175)
        Me.lblSynchronize.Name = "lblSynchronize"
        Me.lblSynchronize.Size = New System.Drawing.Size(192, 15)
        Me.lblSynchronize.TabIndex = 44
        Me.lblSynchronize.Text = "Apply format to cont&inuation style:"
        '
        'lblFontSize
        '
        Me.lblFontSize.AutoSize = True
        Me.lblFontSize.Location = New System.Drawing.Point(344, 20)
        Me.lblFontSize.Name = "lblFontSize"
        Me.lblFontSize.Size = New System.Drawing.Size(30, 15)
        Me.lblFontSize.TabIndex = 34
        Me.lblFontSize.Text = "Siz&e:"
        '
        'lblLineSpacing
        '
        Me.lblLineSpacing.AutoSize = True
        Me.lblLineSpacing.Location = New System.Drawing.Point(158, 66)
        Me.lblLineSpacing.Name = "lblLineSpacing"
        Me.lblLineSpacing.Size = New System.Drawing.Size(77, 15)
        Me.lblLineSpacing.TabIndex = 36
        Me.lblLineSpacing.Text = "&Line Spacing:"
        '
        'lblFontName
        '
        Me.lblFontName.AutoSize = True
        Me.lblFontName.Location = New System.Drawing.Point(158, 20)
        Me.lblFontName.Name = "lblFontName"
        Me.lblFontName.Size = New System.Drawing.Size(69, 15)
        Me.lblFontName.TabIndex = 32
        Me.lblFontName.Text = "Font Na&me:"
        '
        'lblTextAlign
        '
        Me.lblTextAlign.AutoSize = True
        Me.lblTextAlign.Location = New System.Drawing.Point(15, 154)
        Me.lblTextAlign.Name = "lblTextAlign"
        Me.lblTextAlign.Size = New System.Drawing.Size(66, 15)
        Me.lblTextAlign.TabIndex = 30
        Me.lblTextAlign.Text = "Ali&gnment:"
        '
        'lblAt
        '
        Me.lblAt.AutoSize = True
        Me.lblAt.Location = New System.Drawing.Point(341, 66)
        Me.lblAt.Name = "lblAt"
        Me.lblAt.Size = New System.Drawing.Size(22, 15)
        Me.lblAt.TabIndex = 38
        Me.lblAt.Text = "A&t:"
        '
        'lblSpaceAfter
        '
        Me.lblSpaceAfter.AutoSize = True
        Me.lblSpaceAfter.Location = New System.Drawing.Point(262, 110)
        Me.lblSpaceAfter.Name = "lblSpaceAfter"
        Me.lblSpaceAfter.Size = New System.Drawing.Size(70, 15)
        Me.lblSpaceAfter.TabIndex = 42
        Me.lblSpaceAfter.Text = "Space &After:"
        '
        'lblSpaceBefore
        '
        Me.lblSpaceBefore.AutoSize = True
        Me.lblSpaceBefore.Location = New System.Drawing.Point(156, 110)
        Me.lblSpaceBefore.Name = "lblSpaceBefore"
        Me.lblSpaceBefore.Size = New System.Drawing.Size(78, 15)
        Me.lblSpaceBefore.TabIndex = 40
        Me.lblSpaceBefore.Text = "Space &Before:"
        '
        'lblRightIndent
        '
        Me.lblRightIndent.AutoSize = True
        Me.lblRightIndent.Location = New System.Drawing.Point(15, 110)
        Me.lblRightIndent.Name = "lblRightIndent"
        Me.lblRightIndent.Size = New System.Drawing.Size(75, 15)
        Me.lblRightIndent.TabIndex = 28
        Me.lblRightIndent.Text = "Rig&ht Indent:"
        '
        'lblTextPosition
        '
        Me.lblTextPosition.AutoSize = True
        Me.lblTextPosition.Location = New System.Drawing.Point(15, 65)
        Me.lblTextPosition.Name = "lblTextPosition"
        Me.lblTextPosition.Size = New System.Drawing.Size(68, 15)
        Me.lblTextPosition.TabIndex = 26
        Me.lblTextPosition.Text = "&Text Indent:"
        '
        'lblNumberPosition
        '
        Me.lblNumberPosition.AutoSize = True
        Me.lblNumberPosition.Location = New System.Drawing.Point(15, 20)
        Me.lblNumberPosition.Name = "lblNumberPosition"
        Me.lblNumberPosition.Size = New System.Drawing.Size(91, 15)
        Me.lblNumberPosition.TabIndex = 24
        Me.lblNumberPosition.Text = "Number In&dent:"
        '
        'pnlOther
        '
        Me.pnlOther.Controls.Add(Me.cmbNextParagraph)
        Me.pnlOther.Controls.Add(Me.chkPageBreak)
        Me.pnlOther.Controls.Add(Me.chkKeepWithNext)
        Me.pnlOther.Controls.Add(Me.chkKeepLinesTogether)
        Me.pnlOther.Controls.Add(Me.chkWidowOrphan)
        Me.pnlOther.Controls.Add(Me.lblNextParagraph)
        Me.pnlOther.Location = New System.Drawing.Point(246, 224)
        Me.pnlOther.Name = "pnlOther"
        Me.pnlOther.Size = New System.Drawing.Size(208, 152)
        Me.pnlOther.TabIndex = 1
        Me.pnlOther.TabStop = False
        Me.pnlOther.Text = "Other"
        '
        'cmbNextParagraph
        '
        Me.cmbNextParagraph.AllowEmptyValue = False
        Me.cmbNextParagraph.AutoSize = True
        Me.cmbNextParagraph.Borderless = False
        Me.cmbNextParagraph.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNextParagraph.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNextParagraph.IsDirty = False
        Me.cmbNextParagraph.LimitToList = True
        Me.cmbNextParagraph.Location = New System.Drawing.Point(18, 36)
        Me.cmbNextParagraph.MaxDropDownItems = 8
        Me.cmbNextParagraph.Name = "cmbNextParagraph"
        Me.cmbNextParagraph.SelectedIndex = -1
        Me.cmbNextParagraph.SelectedValue = Nothing
        Me.cmbNextParagraph.SelectionLength = 0
        Me.cmbNextParagraph.SelectionStart = 0
        Me.cmbNextParagraph.Size = New System.Drawing.Size(125, 23)
        Me.cmbNextParagraph.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNextParagraph.SupportingValues = ""
        Me.cmbNextParagraph.TabIndex = 54
        Me.cmbNextParagraph.Tag2 = Nothing
        Me.cmbNextParagraph.Value = ""
        '
        'chkPageBreak
        '
        Me.chkPageBreak.AutoSize = True
        Me.chkPageBreak.Location = New System.Drawing.Point(18, 125)
        Me.chkPageBreak.Name = "chkPageBreak"
        Me.chkPageBreak.Size = New System.Drawing.Size(121, 19)
        Me.chkPageBreak.TabIndex = 58
        Me.chkPageBreak.Text = "Page &break before"
        Me.chkPageBreak.UseVisualStyleBackColor = True
        '
        'chkKeepWithNext
        '
        Me.chkKeepWithNext.AutoSize = True
        Me.chkKeepWithNext.Location = New System.Drawing.Point(18, 104)
        Me.chkKeepWithNext.Name = "chkKeepWithNext"
        Me.chkKeepWithNext.Size = New System.Drawing.Size(103, 19)
        Me.chkKeepWithNext.TabIndex = 57
        Me.chkKeepWithNext.Text = "Keep with ne&xt"
        Me.chkKeepWithNext.UseVisualStyleBackColor = True
        '
        'chkKeepLinesTogether
        '
        Me.chkKeepLinesTogether.AutoSize = True
        Me.chkKeepLinesTogether.Location = New System.Drawing.Point(18, 85)
        Me.chkKeepLinesTogether.Name = "chkKeepLinesTogether"
        Me.chkKeepLinesTogether.Size = New System.Drawing.Size(127, 19)
        Me.chkKeepLinesTogether.TabIndex = 56
        Me.chkKeepLinesTogether.Text = "Keep lines to&gether"
        Me.chkKeepLinesTogether.UseVisualStyleBackColor = True
        '
        'chkWidowOrphan
        '
        Me.chkWidowOrphan.AutoSize = True
        Me.chkWidowOrphan.Location = New System.Drawing.Point(18, 65)
        Me.chkWidowOrphan.Name = "chkWidowOrphan"
        Me.chkWidowOrphan.Size = New System.Drawing.Size(108, 19)
        Me.chkWidowOrphan.TabIndex = 55
        Me.chkWidowOrphan.Text = "&Widow/Orphan"
        Me.chkWidowOrphan.UseVisualStyleBackColor = True
        '
        'lblNextParagraph
        '
        Me.lblNextParagraph.AutoSize = True
        Me.lblNextParagraph.Location = New System.Drawing.Point(16, 20)
        Me.lblNextParagraph.Name = "lblNextParagraph"
        Me.lblNextParagraph.Size = New System.Drawing.Size(119, 15)
        Me.lblNextParagraph.TabIndex = 53
        Me.lblNextParagraph.Text = "Next Paragrap&h Style:"
        '
        'tabPage3
        '
        Me.tabPage3.Controls.Add(Me.pnlContStyle)
        Me.tabPage3.Location = New System.Drawing.Point(4, 27)
        Me.tabPage3.Name = "tabPage3"
        Me.tabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage3.Size = New System.Drawing.Size(485, 381)
        Me.tabPage3.TabIndex = 2
        Me.tabPage3.Text = "Continuation Style"
        Me.tabPage3.UseVisualStyleBackColor = True
        '
        'pnlContStyle
        '
        Me.pnlContStyle.Controls.Add(Me.pnlContPagination)
        Me.pnlContStyle.Controls.Add(Me.pnlContSpacing)
        Me.pnlContStyle.Controls.Add(Me.pnlContIndentation)
        Me.pnlContStyle.Controls.Add(Me.pnlContGeneral)
        Me.pnlContStyle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlContStyle.Location = New System.Drawing.Point(3, 3)
        Me.pnlContStyle.Name = "pnlContStyle"
        Me.pnlContStyle.Size = New System.Drawing.Size(479, 375)
        Me.pnlContStyle.TabIndex = 15
        '
        'pnlContPagination
        '
        Me.pnlContPagination.Controls.Add(Me.chkContKeepLinesTogether)
        Me.pnlContPagination.Controls.Add(Me.chkContKeepWithNext)
        Me.pnlContPagination.Controls.Add(Me.chkContWidowOrphan)
        Me.pnlContPagination.Location = New System.Drawing.Point(3, 292)
        Me.pnlContPagination.Name = "pnlContPagination"
        Me.pnlContPagination.Size = New System.Drawing.Size(455, 82)
        Me.pnlContPagination.TabIndex = 3
        Me.pnlContPagination.TabStop = False
        Me.pnlContPagination.Text = "Pagination"
        '
        'chkContKeepLinesTogether
        '
        Me.chkContKeepLinesTogether.AutoSize = True
        Me.chkContKeepLinesTogether.Location = New System.Drawing.Point(18, 49)
        Me.chkContKeepLinesTogether.Name = "chkContKeepLinesTogether"
        Me.chkContKeepLinesTogether.Size = New System.Drawing.Size(127, 19)
        Me.chkContKeepLinesTogether.TabIndex = 82
        Me.chkContKeepLinesTogether.Text = "Keep &lines together"
        Me.chkContKeepLinesTogether.UseVisualStyleBackColor = True
        '
        'chkContKeepWithNext
        '
        Me.chkContKeepWithNext.AutoSize = True
        Me.chkContKeepWithNext.Location = New System.Drawing.Point(209, 27)
        Me.chkContKeepWithNext.Name = "chkContKeepWithNext"
        Me.chkContKeepWithNext.Size = New System.Drawing.Size(103, 19)
        Me.chkContKeepWithNext.TabIndex = 83
        Me.chkContKeepWithNext.Text = "Keep with ne&xt"
        Me.chkContKeepWithNext.UseVisualStyleBackColor = True
        '
        'chkContWidowOrphan
        '
        Me.chkContWidowOrphan.AutoSize = True
        Me.chkContWidowOrphan.Location = New System.Drawing.Point(18, 27)
        Me.chkContWidowOrphan.Name = "chkContWidowOrphan"
        Me.chkContWidowOrphan.Size = New System.Drawing.Size(149, 19)
        Me.chkContWidowOrphan.TabIndex = 81
        Me.chkContWidowOrphan.Text = "&Widow/Orphan control"
        Me.chkContWidowOrphan.UseVisualStyleBackColor = True
        '
        'pnlContSpacing
        '
        Me.pnlContSpacing.Controls.Add(Me.spnContAt)
        Me.pnlContSpacing.Controls.Add(Me.spnContSpaceAfter)
        Me.pnlContSpacing.Controls.Add(Me.spnContSpaceBefore)
        Me.pnlContSpacing.Controls.Add(Me.cmbContLineSpacing)
        Me.pnlContSpacing.Controls.Add(Me.lblContAt)
        Me.pnlContSpacing.Controls.Add(Me.lblContLineSpacing)
        Me.pnlContSpacing.Controls.Add(Me.lblContSpaceAfter)
        Me.pnlContSpacing.Controls.Add(Me.lblContSpaceBefore)
        Me.pnlContSpacing.Location = New System.Drawing.Point(3, 197)
        Me.pnlContSpacing.Name = "pnlContSpacing"
        Me.pnlContSpacing.Size = New System.Drawing.Size(455, 91)
        Me.pnlContSpacing.TabIndex = 2
        Me.pnlContSpacing.TabStop = False
        Me.pnlContSpacing.Text = "Spacing"
        '
        'spnContAt
        '
        Me.spnContAt.AllowFractions = False
        Me.spnContAt.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnContAt.AppendSymbol = True
        Me.spnContAt.AutoSize = True
        Me.spnContAt.DisplayText = "0 pt"
        Me.spnContAt.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnContAt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnContAt.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnContAt.Location = New System.Drawing.Point(273, 56)
        Me.spnContAt.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnContAt.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContAt.Name = "spnContAt"
        Me.spnContAt.SelLength = 0
        Me.spnContAt.SelStart = 0
        Me.spnContAt.SelText = ""
        Me.spnContAt.Size = New System.Drawing.Size(95, 20)
        Me.spnContAt.TabIndex = 80
        Me.spnContAt.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContAt.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnContSpaceAfter
        '
        Me.spnContSpaceAfter.AllowFractions = False
        Me.spnContSpaceAfter.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnContSpaceAfter.AppendSymbol = True
        Me.spnContSpaceAfter.AutoSize = True
        Me.spnContSpaceAfter.DisplayText = "0 pt"
        Me.spnContSpaceAfter.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnContSpaceAfter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnContSpaceAfter.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnContSpaceAfter.Location = New System.Drawing.Point(60, 56)
        Me.spnContSpaceAfter.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnContSpaceAfter.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContSpaceAfter.Name = "spnContSpaceAfter"
        Me.spnContSpaceAfter.SelLength = 0
        Me.spnContSpaceAfter.SelStart = 0
        Me.spnContSpaceAfter.SelText = ""
        Me.spnContSpaceAfter.Size = New System.Drawing.Size(95, 20)
        Me.spnContSpaceAfter.TabIndex = 76
        Me.spnContSpaceAfter.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContSpaceAfter.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnContSpaceBefore
        '
        Me.spnContSpaceBefore.AllowFractions = False
        Me.spnContSpaceBefore.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnContSpaceBefore.AppendSymbol = True
        Me.spnContSpaceBefore.AutoSize = True
        Me.spnContSpaceBefore.DisplayText = "0 pt"
        Me.spnContSpaceBefore.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnContSpaceBefore.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnContSpaceBefore.IncrementValue = New Decimal(New Integer() {6, 0, 0, 0})
        Me.spnContSpaceBefore.Location = New System.Drawing.Point(60, 23)
        Me.spnContSpaceBefore.MaxValue = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.spnContSpaceBefore.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContSpaceBefore.Name = "spnContSpaceBefore"
        Me.spnContSpaceBefore.SelLength = 0
        Me.spnContSpaceBefore.SelStart = 0
        Me.spnContSpaceBefore.SelText = ""
        Me.spnContSpaceBefore.Size = New System.Drawing.Size(95, 20)
        Me.spnContSpaceBefore.TabIndex = 74
        Me.spnContSpaceBefore.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContSpaceBefore.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'cmbContLineSpacing
        '
        Me.cmbContLineSpacing.AllowEmptyValue = False
        Me.cmbContLineSpacing.AutoSize = True
        Me.cmbContLineSpacing.Borderless = False
        Me.cmbContLineSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbContLineSpacing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContLineSpacing.IsDirty = False
        Me.cmbContLineSpacing.LimitToList = True
        Me.cmbContLineSpacing.Location = New System.Drawing.Point(273, 23)
        Me.cmbContLineSpacing.MaxDropDownItems = 8
        Me.cmbContLineSpacing.Name = "cmbContLineSpacing"
        Me.cmbContLineSpacing.SelectedIndex = -1
        Me.cmbContLineSpacing.SelectedValue = Nothing
        Me.cmbContLineSpacing.SelectionLength = 0
        Me.cmbContLineSpacing.SelectionStart = 0
        Me.cmbContLineSpacing.Size = New System.Drawing.Size(95, 23)
        Me.cmbContLineSpacing.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbContLineSpacing.SupportingValues = ""
        Me.cmbContLineSpacing.TabIndex = 78
        Me.cmbContLineSpacing.Tag2 = Nothing
        Me.cmbContLineSpacing.Value = ""
        '
        'lblContAt
        '
        Me.lblContAt.AutoSize = True
        Me.lblContAt.Location = New System.Drawing.Point(187, 58)
        Me.lblContAt.Name = "lblContAt"
        Me.lblContAt.Size = New System.Drawing.Size(22, 15)
        Me.lblContAt.TabIndex = 79
        Me.lblContAt.Text = "A&t:"
        Me.lblContAt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContLineSpacing
        '
        Me.lblContLineSpacing.AutoSize = True
        Me.lblContLineSpacing.Location = New System.Drawing.Point(187, 25)
        Me.lblContLineSpacing.Name = "lblContLineSpacing"
        Me.lblContLineSpacing.Size = New System.Drawing.Size(77, 15)
        Me.lblContLineSpacing.TabIndex = 77
        Me.lblContLineSpacing.Text = "&Line Spacing:"
        Me.lblContLineSpacing.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContSpaceAfter
        '
        Me.lblContSpaceAfter.AutoSize = True
        Me.lblContSpaceAfter.Location = New System.Drawing.Point(15, 58)
        Me.lblContSpaceAfter.Name = "lblContSpaceAfter"
        Me.lblContSpaceAfter.Size = New System.Drawing.Size(36, 15)
        Me.lblContSpaceAfter.TabIndex = 75
        Me.lblContSpaceAfter.Text = "&After:"
        Me.lblContSpaceAfter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContSpaceBefore
        '
        Me.lblContSpaceBefore.AutoSize = True
        Me.lblContSpaceBefore.Location = New System.Drawing.Point(15, 25)
        Me.lblContSpaceBefore.Name = "lblContSpaceBefore"
        Me.lblContSpaceBefore.Size = New System.Drawing.Size(44, 15)
        Me.lblContSpaceBefore.TabIndex = 73
        Me.lblContSpaceBefore.Text = "&Before:"
        Me.lblContSpaceBefore.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'pnlContIndentation
        '
        Me.pnlContIndentation.Controls.Add(Me.spnBy)
        Me.pnlContIndentation.Controls.Add(Me.spnContRightIndent)
        Me.pnlContIndentation.Controls.Add(Me.spnContLeftIndent)
        Me.pnlContIndentation.Controls.Add(Me.cmbSpecial)
        Me.pnlContIndentation.Controls.Add(Me.lblBy)
        Me.pnlContIndentation.Controls.Add(Me.lblSpecial)
        Me.pnlContIndentation.Controls.Add(Me.lblContRightIndent)
        Me.pnlContIndentation.Controls.Add(Me.lblContLeftIndent)
        Me.pnlContIndentation.Location = New System.Drawing.Point(3, 102)
        Me.pnlContIndentation.Name = "pnlContIndentation"
        Me.pnlContIndentation.Size = New System.Drawing.Size(455, 91)
        Me.pnlContIndentation.TabIndex = 1
        Me.pnlContIndentation.TabStop = False
        Me.pnlContIndentation.Text = "Indentation"
        '
        'spnBy
        '
        Me.spnBy.AllowFractions = True
        Me.spnBy.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnBy.AppendSymbol = True
        Me.spnBy.AutoSize = True
        Me.spnBy.DisplayText = "0"""
        Me.spnBy.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnBy.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnBy.Location = New System.Drawing.Point(273, 56)
        Me.spnBy.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnBy.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnBy.Name = "spnBy"
        Me.spnBy.SelLength = 0
        Me.spnBy.SelStart = 0
        Me.spnBy.SelText = ""
        Me.spnBy.Size = New System.Drawing.Size(95, 20)
        Me.spnBy.TabIndex = 72
        Me.spnBy.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnBy.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnContRightIndent
        '
        Me.spnContRightIndent.AllowFractions = True
        Me.spnContRightIndent.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnContRightIndent.AppendSymbol = True
        Me.spnContRightIndent.AutoSize = True
        Me.spnContRightIndent.DisplayText = "0"""
        Me.spnContRightIndent.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnContRightIndent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnContRightIndent.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnContRightIndent.Location = New System.Drawing.Point(60, 56)
        Me.spnContRightIndent.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnContRightIndent.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnContRightIndent.Name = "spnContRightIndent"
        Me.spnContRightIndent.SelLength = 0
        Me.spnContRightIndent.SelStart = 0
        Me.spnContRightIndent.SelText = ""
        Me.spnContRightIndent.Size = New System.Drawing.Size(95, 20)
        Me.spnContRightIndent.TabIndex = 68
        Me.spnContRightIndent.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContRightIndent.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnContLeftIndent
        '
        Me.spnContLeftIndent.AllowFractions = True
        Me.spnContLeftIndent.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnContLeftIndent.AppendSymbol = True
        Me.spnContLeftIndent.AutoSize = True
        Me.spnContLeftIndent.DisplayText = "0"""
        Me.spnContLeftIndent.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnContLeftIndent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnContLeftIndent.IncrementValue = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.spnContLeftIndent.Location = New System.Drawing.Point(60, 23)
        Me.spnContLeftIndent.MaxValue = New Decimal(New Integer() {576, 0, 0, 0})
        Me.spnContLeftIndent.MinValue = New Decimal(New Integer() {576, 0, 0, -2147483648})
        Me.spnContLeftIndent.Name = "spnContLeftIndent"
        Me.spnContLeftIndent.SelLength = 0
        Me.spnContLeftIndent.SelStart = 0
        Me.spnContLeftIndent.SelText = ""
        Me.spnContLeftIndent.Size = New System.Drawing.Size(95, 20)
        Me.spnContLeftIndent.TabIndex = 66
        Me.spnContLeftIndent.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnContLeftIndent.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'cmbSpecial
        '
        Me.cmbSpecial.AllowEmptyValue = False
        Me.cmbSpecial.AutoSize = True
        Me.cmbSpecial.Borderless = False
        Me.cmbSpecial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbSpecial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbSpecial.IsDirty = False
        Me.cmbSpecial.LimitToList = True
        Me.cmbSpecial.Location = New System.Drawing.Point(273, 23)
        Me.cmbSpecial.MaxDropDownItems = 8
        Me.cmbSpecial.Name = "cmbSpecial"
        Me.cmbSpecial.SelectedIndex = -1
        Me.cmbSpecial.SelectedValue = Nothing
        Me.cmbSpecial.SelectionLength = 0
        Me.cmbSpecial.SelectionStart = 0
        Me.cmbSpecial.Size = New System.Drawing.Size(95, 23)
        Me.cmbSpecial.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbSpecial.SupportingValues = ""
        Me.cmbSpecial.TabIndex = 70
        Me.cmbSpecial.Tag2 = Nothing
        Me.cmbSpecial.Value = ""
        '
        'lblBy
        '
        Me.lblBy.AutoSize = True
        Me.lblBy.Location = New System.Drawing.Point(221, 59)
        Me.lblBy.Name = "lblBy"
        Me.lblBy.Size = New System.Drawing.Size(23, 15)
        Me.lblBy.TabIndex = 71
        Me.lblBy.Text = "B&y:"
        Me.lblBy.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSpecial
        '
        Me.lblSpecial.AutoSize = True
        Me.lblSpecial.Location = New System.Drawing.Point(221, 25)
        Me.lblSpecial.Name = "lblSpecial"
        Me.lblSpecial.Size = New System.Drawing.Size(47, 15)
        Me.lblSpecial.TabIndex = 69
        Me.lblSpecial.Text = "&Special:"
        Me.lblSpecial.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContRightIndent
        '
        Me.lblContRightIndent.AutoSize = True
        Me.lblContRightIndent.Location = New System.Drawing.Point(15, 59)
        Me.lblContRightIndent.Name = "lblContRightIndent"
        Me.lblContRightIndent.Size = New System.Drawing.Size(38, 15)
        Me.lblContRightIndent.TabIndex = 67
        Me.lblContRightIndent.Text = "Rig&ht:"
        Me.lblContRightIndent.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContLeftIndent
        '
        Me.lblContLeftIndent.AutoSize = True
        Me.lblContLeftIndent.Location = New System.Drawing.Point(15, 25)
        Me.lblContLeftIndent.Name = "lblContLeftIndent"
        Me.lblContLeftIndent.Size = New System.Drawing.Size(30, 15)
        Me.lblContLeftIndent.TabIndex = 65
        Me.lblContLeftIndent.Text = "Le&ft:"
        Me.lblContLeftIndent.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'pnlContGeneral
        '
        Me.pnlContGeneral.Controls.Add(Me.cmbContFontSize)
        Me.pnlContGeneral.Controls.Add(Me.lblContFontSize)
        Me.pnlContGeneral.Controls.Add(Me.cmbAlignment)
        Me.pnlContGeneral.Controls.Add(Me.cmbContFontName)
        Me.pnlContGeneral.Controls.Add(Me.lblAlignment)
        Me.pnlContGeneral.Controls.Add(Me.lblContFontName)
        Me.pnlContGeneral.Location = New System.Drawing.Point(3, 7)
        Me.pnlContGeneral.Name = "pnlContGeneral"
        Me.pnlContGeneral.Size = New System.Drawing.Size(455, 91)
        Me.pnlContGeneral.TabIndex = 0
        Me.pnlContGeneral.TabStop = False
        Me.pnlContGeneral.Text = "General"
        '
        'cmbContFontSize
        '
        Me.cmbContFontSize.AllowEmptyValue = False
        Me.cmbContFontSize.AutoSize = True
        Me.cmbContFontSize.Borderless = False
        Me.cmbContFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbContFontSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContFontSize.IsDirty = False
        Me.cmbContFontSize.LimitToList = False
        Me.cmbContFontSize.Location = New System.Drawing.Point(345, 23)
        Me.cmbContFontSize.MaxDropDownItems = 8
        Me.cmbContFontSize.Name = "cmbContFontSize"
        Me.cmbContFontSize.SelectedIndex = -1
        Me.cmbContFontSize.SelectedValue = Nothing
        Me.cmbContFontSize.SelectionLength = 0
        Me.cmbContFontSize.SelectionStart = 0
        Me.cmbContFontSize.Size = New System.Drawing.Size(92, 23)
        Me.cmbContFontSize.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbContFontSize.SupportingValues = ""
        Me.cmbContFontSize.TabIndex = 62
        Me.cmbContFontSize.Tag2 = Nothing
        Me.cmbContFontSize.Value = ""
        '
        'lblContFontSize
        '
        Me.lblContFontSize.AutoSize = True
        Me.lblContFontSize.Location = New System.Drawing.Point(285, 26)
        Me.lblContFontSize.Name = "lblContFontSize"
        Me.lblContFontSize.Size = New System.Drawing.Size(57, 15)
        Me.lblContFontSize.TabIndex = 61
        Me.lblContFontSize.Text = "Font Si&ze:"
        Me.lblContFontSize.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmbAlignment
        '
        Me.cmbAlignment.AllowEmptyValue = False
        Me.cmbAlignment.AutoSize = True
        Me.cmbAlignment.Borderless = False
        Me.cmbAlignment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbAlignment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAlignment.IsDirty = False
        Me.cmbAlignment.LimitToList = True
        Me.cmbAlignment.Location = New System.Drawing.Point(92, 56)
        Me.cmbAlignment.MaxDropDownItems = 8
        Me.cmbAlignment.Name = "cmbAlignment"
        Me.cmbAlignment.SelectedIndex = -1
        Me.cmbAlignment.SelectedValue = Nothing
        Me.cmbAlignment.SelectionLength = 0
        Me.cmbAlignment.SelectionStart = 0
        Me.cmbAlignment.Size = New System.Drawing.Size(169, 23)
        Me.cmbAlignment.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbAlignment.SupportingValues = ""
        Me.cmbAlignment.TabIndex = 64
        Me.cmbAlignment.Tag2 = Nothing
        Me.cmbAlignment.Value = ""
        '
        'cmbContFontName
        '
        Me.cmbContFontName.AllowEmptyValue = False
        Me.cmbContFontName.AutoSize = True
        Me.cmbContFontName.Borderless = False
        Me.cmbContFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbContFontName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbContFontName.IsDirty = False
        Me.cmbContFontName.LimitToList = True
        Me.cmbContFontName.Location = New System.Drawing.Point(92, 23)
        Me.cmbContFontName.MaxDropDownItems = 8
        Me.cmbContFontName.Name = "cmbContFontName"
        Me.cmbContFontName.SelectedIndex = -1
        Me.cmbContFontName.SelectedValue = Nothing
        Me.cmbContFontName.SelectionLength = 0
        Me.cmbContFontName.SelectionStart = 0
        Me.cmbContFontName.Size = New System.Drawing.Size(169, 23)
        Me.cmbContFontName.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbContFontName.SupportingValues = ""
        Me.cmbContFontName.TabIndex = 60
        Me.cmbContFontName.Tag2 = Nothing
        Me.cmbContFontName.Value = ""
        '
        'lblAlignment
        '
        Me.lblAlignment.AutoSize = True
        Me.lblAlignment.Location = New System.Drawing.Point(15, 60)
        Me.lblAlignment.Name = "lblAlignment"
        Me.lblAlignment.Size = New System.Drawing.Size(66, 15)
        Me.lblAlignment.TabIndex = 63
        Me.lblAlignment.Text = "Ali&gnment:"
        Me.lblAlignment.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblContFontName
        '
        Me.lblContFontName.AutoSize = True
        Me.lblContFontName.Location = New System.Drawing.Point(15, 27)
        Me.lblContFontName.Name = "lblContFontName"
        Me.lblContFontName.Size = New System.Drawing.Size(69, 15)
        Me.lblContFontName.TabIndex = 59
        Me.lblContFontName.Text = "Font Na&me:"
        Me.lblContFontName.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.toolStrip3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(521, 33)
        Me.Panel1.TabIndex = 21
        '
        'toolStrip3
        '
        Me.toolStrip3.AutoSize = False
        Me.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.toolStrip3.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.toolStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbtnNumber, Me.toolStripSeparator12, Me.tbtnParagraph, Me.toolStripSeparator11, Me.tbtnContStyle})
        Me.toolStrip3.Location = New System.Drawing.Point(0, 0)
        Me.toolStrip3.Name = "toolStrip3"
        Me.toolStrip3.Size = New System.Drawing.Size(521, 34)
        Me.toolStrip3.Stretch = True
        Me.toolStrip3.TabIndex = 20
        Me.toolStrip3.Text = "ToolStrip1"
        '
        'tbtnNumber
        '
        Me.tbtnNumber.CheckOnClick = True
        Me.tbtnNumber.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnNumber.Margin = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.tbtnNumber.Name = "tbtnNumber"
        Me.tbtnNumber.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnNumber.Size = New System.Drawing.Size(55, 30)
        Me.tbtnNumber.Text = "&Number"
        Me.tbtnNumber.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator12
        '
        Me.toolStripSeparator12.Name = "toolStripSeparator12"
        Me.toolStripSeparator12.Size = New System.Drawing.Size(6, 34)
        '
        'tbtnParagraph
        '
        Me.tbtnParagraph.CheckOnClick = True
        Me.tbtnParagraph.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbtnParagraph.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.tbtnParagraph.Name = "tbtnParagraph"
        Me.tbtnParagraph.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnParagraph.Size = New System.Drawing.Size(65, 30)
        Me.tbtnParagraph.Text = "&Paragraph"
        Me.tbtnParagraph.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator11
        '
        Me.toolStripSeparator11.Name = "toolStripSeparator11"
        Me.toolStripSeparator11.Size = New System.Drawing.Size(6, 34)
        '
        'tbtnContStyle
        '
        Me.tbtnContStyle.CheckOnClick = True
        Me.tbtnContStyle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tbtnContStyle.Image = CType(resources.GetObject("tbtnContStyle.Image"), System.Drawing.Image)
        Me.tbtnContStyle.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbtnContStyle.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.tbtnContStyle.Name = "tbtnContStyle"
        Me.tbtnContStyle.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnContStyle.Size = New System.Drawing.Size(109, 30)
        Me.tbtnContStyle.Text = "C&ontinuation Style"
        Me.tbtnContStyle.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'frmEditScheme
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(521, 464)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.splitContainer1)
        Me.Controls.Add(Me.tsToolbar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditScheme"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Scheme"
        Me.tsToolbar.ResumeLayout(False)
        Me.tsToolbar.PerformLayout()
        Me.pnlLevels.ResumeLayout(False)
        Me.tsLevel.ResumeLayout(False)
        Me.tsLevel.PerformLayout()
        Me.splitContainer1.Panel1.ResumeLayout(False)
        Me.splitContainer1.Panel2.ResumeLayout(False)
        CType(Me.splitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitContainer1.ResumeLayout(False)
        Me.tabControl1.ResumeLayout(False)
        Me.tabPage1.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        Me.pnlNumberFont.ResumeLayout(False)
        Me.pnlNumberFont.PerformLayout()
        Me.pnlNumber.ResumeLayout(False)
        Me.pnlNumber.PerformLayout()
        CType(Me.udStartAt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPage2.ResumeLayout(False)
        Me.pnlParagraph.ResumeLayout(False)
        Me.pnlFont.ResumeLayout(False)
        Me.pnlFont.PerformLayout()
        Me.pnlPosition.ResumeLayout(False)
        Me.pnlPosition.PerformLayout()
        Me.pnlOther.ResumeLayout(False)
        Me.pnlOther.PerformLayout()
        Me.tabPage3.ResumeLayout(False)
        Me.pnlContStyle.ResumeLayout(False)
        Me.pnlContPagination.ResumeLayout(False)
        Me.pnlContPagination.PerformLayout()
        Me.pnlContSpacing.ResumeLayout(False)
        Me.pnlContSpacing.PerformLayout()
        Me.pnlContIndentation.ResumeLayout(False)
        Me.pnlContIndentation.PerformLayout()
        Me.pnlContGeneral.ResumeLayout(False)
        Me.pnlContGeneral.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.toolStrip3.ResumeLayout(False)
        Me.toolStrip3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsToolbar As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdSave As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdZoom As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdUpdate As System.Windows.Forms.ToolStripButton
    Private WithEvents pnlLevels As System.Windows.Forms.Panel
    Friend WithEvents tsLevel As System.Windows.Forms.ToolStrip
    Private WithEvents tbtnLevel1 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel2 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel3 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel4 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel5 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel6 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel7 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel8 As System.Windows.Forms.ToolStripButton
    Private WithEvents tbtnLevel9 As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdAddLevel As System.Windows.Forms.ToolStripButton
    Private WithEvents cmdDeleteLevel As System.Windows.Forms.ToolStripButton
    Private WithEvents splitContainer1 As System.Windows.Forms.SplitContainer
    Private WithEvents tabControl1 As System.Windows.Forms.TabControl
    Private WithEvents tabPage1 As System.Windows.Forms.TabPage
    Private WithEvents pnlButtons As System.Windows.Forms.Panel
    Private WithEvents pnlNumberFont As System.Windows.Forms.GroupBox
    Private WithEvents lblNumFontSize As System.Windows.Forms.Label
    Private WithEvents lblNumFontName As System.Windows.Forms.Label
    Private WithEvents chkNumberCaps As System.Windows.Forms.CheckBox
    Private WithEvents chkNumberItalic As System.Windows.Forms.CheckBox
    Private WithEvents chkNumberBold As System.Windows.Forms.CheckBox
    Private WithEvents lblNumFontUnderline As System.Windows.Forms.Label
    Private WithEvents pnlNumber As System.Windows.Forms.GroupBox
    Private WithEvents chkNonbreakingSpaces As System.Windows.Forms.CheckBox
    Private WithEvents chkRightAlign As System.Windows.Forms.CheckBox
    Private WithEvents chkLegalStyle As System.Windows.Forms.CheckBox
    Private WithEvents chkReset As System.Windows.Forms.CheckBox
    Private WithEvents btnAddPrevLevel As System.Windows.Forms.Button
    Private WithEvents lblPreviousLevels As System.Windows.Forms.Label
    Private WithEvents lblTabPosition As System.Windows.Forms.Label
    Private WithEvents lblStartAt As System.Windows.Forms.Label
    Private WithEvents udStartAt As System.Windows.Forms.NumericUpDown
    Private WithEvents lblTrailingChar As System.Windows.Forms.Label
    Private WithEvents lbNumberStyle As System.Windows.Forms.Label
    Private WithEvents lblNumberFormat As System.Windows.Forms.Label
    Private WithEvents tabPage2 As System.Windows.Forms.TabPage
    Private WithEvents pnlParagraph As System.Windows.Forms.Panel
    Private WithEvents pnlFont As System.Windows.Forms.GroupBox
    Private WithEvents optStyleFormats As System.Windows.Forms.RadioButton
    Private WithEvents optDirectFormats As System.Windows.Forms.RadioButton
    Private WithEvents chkTCCaps As System.Windows.Forms.CheckBox
    Private WithEvents chkTCSmallCaps As System.Windows.Forms.CheckBox
    Private WithEvents chkTCUnderline As System.Windows.Forms.CheckBox
    Private WithEvents chkTCItalic As System.Windows.Forms.CheckBox
    Private WithEvents chkTCBold As System.Windows.Forms.CheckBox
    Private WithEvents pnlPosition As System.Windows.Forms.GroupBox
    Private WithEvents cmdSynchronize As System.Windows.Forms.Button
    Private WithEvents lblSynchronize As System.Windows.Forms.Label
    Private WithEvents lblFontSize As System.Windows.Forms.Label
    Private WithEvents lblLineSpacing As System.Windows.Forms.Label
    Private WithEvents lblFontName As System.Windows.Forms.Label
    Private WithEvents lblTextAlign As System.Windows.Forms.Label
    Private WithEvents lblAt As System.Windows.Forms.Label
    Private WithEvents lblSpaceAfter As System.Windows.Forms.Label
    Private WithEvents lblSpaceBefore As System.Windows.Forms.Label
    Private WithEvents lblRightIndent As System.Windows.Forms.Label
    Private WithEvents lblTextPosition As System.Windows.Forms.Label
    Private WithEvents lblNumberPosition As System.Windows.Forms.Label
    Private WithEvents pnlOther As System.Windows.Forms.GroupBox
    Private WithEvents chkPageBreak As System.Windows.Forms.CheckBox
    Private WithEvents chkKeepWithNext As System.Windows.Forms.CheckBox
    Private WithEvents chkKeepLinesTogether As System.Windows.Forms.CheckBox
    Private WithEvents chkWidowOrphan As System.Windows.Forms.CheckBox
    Private WithEvents lblNextParagraph As System.Windows.Forms.Label
    Private WithEvents tabPage3 As System.Windows.Forms.TabPage
    Private WithEvents pnlContStyle As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents toolStrip3 As System.Windows.Forms.ToolStrip
    Friend WithEvents tbtnNumber As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbtnParagraph As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbtnContStyle As System.Windows.Forms.ToolStripButton
    Friend WithEvents rtfNumberFormat As System.Windows.Forms.RichTextBox
    Friend WithEvents cmbTrailingChar As mpnControls.ComboBox
    Friend WithEvents cmbNumberStyle As mpnControls.ComboBox
    Friend WithEvents txtStartAt As System.Windows.Forms.TextBox
    Friend WithEvents cmbNumFontSize As mpnControls.ComboBox
    Friend WithEvents cmbNumFontName As mpnControls.ComboBox
    Friend WithEvents cmbUnderlineStyle As mpnControls.ComboBox
    Friend WithEvents cmbTextAlign As mpnControls.ComboBox
    Friend WithEvents cmbLineSpacing As mpnControls.ComboBox
    Friend WithEvents cmbFontSize As mpnControls.ComboBox
    Friend WithEvents cmbFontName As mpnControls.ComboBox
    Friend WithEvents cmbNextParagraph As mpnControls.ComboBox
    Friend WithEvents pnlContPagination As System.Windows.Forms.GroupBox
    Friend WithEvents pnlContSpacing As System.Windows.Forms.GroupBox
    Friend WithEvents pnlContIndentation As System.Windows.Forms.GroupBox
    Friend WithEvents pnlContGeneral As System.Windows.Forms.GroupBox
    Private WithEvents lblAlignment As System.Windows.Forms.Label
    Private WithEvents lblContFontName As System.Windows.Forms.Label
    Friend WithEvents cmbAlignment As mpnControls.ComboBox
    Friend WithEvents cmbContFontName As mpnControls.ComboBox
    Friend WithEvents cmbContFontSize As mpnControls.ComboBox
    Private WithEvents lblContFontSize As System.Windows.Forms.Label
    Private WithEvents lblContRightIndent As System.Windows.Forms.Label
    Private WithEvents lblContLeftIndent As System.Windows.Forms.Label
    Friend WithEvents cmbSpecial As mpnControls.ComboBox
    Private WithEvents lblBy As System.Windows.Forms.Label
    Private WithEvents lblSpecial As System.Windows.Forms.Label
    Friend WithEvents cmbContLineSpacing As mpnControls.ComboBox
    Private WithEvents lblContAt As System.Windows.Forms.Label
    Private WithEvents lblContLineSpacing As System.Windows.Forms.Label
    Private WithEvents lblContSpaceAfter As System.Windows.Forms.Label
    Private WithEvents lblContSpaceBefore As System.Windows.Forms.Label
    Private WithEvents chkContKeepLinesTogether As System.Windows.Forms.CheckBox
    Private WithEvents chkContKeepWithNext As System.Windows.Forms.CheckBox
    Private WithEvents chkContWidowOrphan As System.Windows.Forms.CheckBox
    Friend WithEvents spnTabPosition As SpinTextInternational
    Friend WithEvents spnSpaceAfter As SpinTextInternational
    Friend WithEvents spnSpaceBefore As SpinTextInternational
    Friend WithEvents spnAt As SpinTextInternational
    Friend WithEvents spnRightIndent As SpinTextInternational
    Friend WithEvents spnTextPosition As SpinTextInternational
    Friend WithEvents spnNumberPosition As SpinTextInternational
    Friend WithEvents spnContSpaceAfter As SpinTextInternational
    Friend WithEvents spnContSpaceBefore As SpinTextInternational
    Friend WithEvents spnBy As SpinTextInternational
    Friend WithEvents spnContRightIndent As SpinTextInternational
    Friend WithEvents spnContLeftIndent As SpinTextInternational
    Friend WithEvents spnContAt As SpinTextInternational
    Friend WithEvents lstPrevLevels As System.Windows.Forms.ListView
    Friend WithEvents LevelName As System.Windows.Forms.ColumnHeader
    Friend WithEvents LevelValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
End Class
