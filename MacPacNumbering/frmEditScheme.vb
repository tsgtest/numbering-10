﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cNumbers
Imports LMP.Numbering.Base.cTripleStateChkbox
Imports LMP.Numbering.Base.cIO
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpDialog_VB
Imports MacPacNumbering.LMP.Numbering.CError
Imports System.Windows.Forms
Imports System.Collections
Imports LMP

Friend Class frmEditScheme
    Inherits System.Windows.Forms.Form

    Private m_bForceSelContent As Boolean
    Private m_xCode As String
    Private xUserAll As String
    Private bRestore As Boolean
    Private m_bFormInit As Boolean
    Private m_bAllowRefresh As Boolean
    Private m_bClicked As Boolean
    Private m_iPrevTRISTATE As System.Windows.Forms.CheckState
    Private m_bStopChangeEvent As Boolean
    Private m_bNoSelChangeEvent As Boolean
    Private m_bIsPrevLevels As Boolean
    Private m_iSelStart As Short
    Private m_bLevelChanged As Boolean
    Private m_bLevelDirty As Boolean
    Private m_bTCDirty As Boolean
    Private m_bTOCDirty As Boolean
    Private m_bPropDirty As Boolean
    Private m_bParaDirty As Boolean
    Private m_bNextParaDirty As Boolean
    Private m_iLastSelLevel As Short
    Private m_xarPrevLevels(,) As String
    Private m_oPreview As MacPacNumbering.LMP.Numbering.cPreview
    Private m_bSelChangeRunning As Boolean
    Private m_bInsertingPrevLevel As Boolean
    Private m_bAlertDisplayed As Boolean
    Private m_bSwitchingTabs As Boolean
    Private m_xPrevCtl As String
    Private m_bRawNumberFormat As Boolean
    Private m_iFixedDigitNumberingOffset As Short
    Private m_xBulletFont As String
    Private m_xBulletCharNum As String
    Private m_xarNumFontName(,) As String
    Private m_xarLineSpacing(,) As String
    Private m_xarNextPara As ArrayList
    Private m_xarNumStyle As ArrayList
    Private m_bFillingNextParaList As Boolean

    'these are exposed as properties
    Private m_iInitLevels As Short
    Private m_iCurLevels As Short
    Private m_bCancelled As Boolean
    Private m_xarIsDirty(,) As String
    Private m_docStart As Word.Document
    Private m_docPreview As Word.Document
    Private m_bCreateBitmap As Boolean
    Private m_bZoomClicked As Boolean 'GLOG 5549
    Private m_iLevel As Short

    Public Property StartDoc() As Word.Document
        Get
            StartDoc = m_docStart
        End Get
        Set(ByVal Value As Word.Document)
            m_docStart = Value
        End Set
    End Property


    Public Property PreviewDoc() As Word.Document
        Get
            PreviewDoc = m_docPreview
        End Get
        Set(ByVal Value As Word.Document)
            m_docPreview = Value
        End Set
    End Property

    ReadOnly Property InitialLevels() As Short
        Get
            InitialLevels = m_iInitLevels
        End Get
    End Property

    ReadOnly Property CurrentLevels() As Short
        Get
            CurrentLevels = m_iCurLevels
        End Get
    End Property

    ReadOnly Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
    End Property


    Property ZoomClicked() As Boolean
        Get
            ZoomClicked = m_bZoomClicked
        End Get
        Set(ByVal Value As Boolean)
            m_bZoomClicked = Value
        End Set
    End Property

    ReadOnly Property xarDirty() As Array
        Get
            xarDirty = m_xarIsDirty
        End Get
    End Property


    Private Sub btnAddPrevLevel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnAddPrevLevel.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            InsertPrevLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    'UPGRADE_WARNING: Event chkKeepLinesTogether.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkKeepLinesTogether_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkKeepLinesTogether.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkKeepLinesTogether_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkKeepLinesTogether.Leave
        m_xPrevCtl = "chkKeepLinesTogether"
    End Sub
    'UPGRADE_WARNING: Event chkKeepWithNext.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkKeepWithNext_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkKeepWithNext.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkKeepWithNext_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkKeepWithNext.Leave
        m_xPrevCtl = "chkKeepWithNext"
    End Sub

    'UPGRADE_WARNING: Event chkWidowOrphan.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkWidowOrphan_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkWidowOrphan.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkWidowOrphan_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkWidowOrphan.Leave
        m_xPrevCtl = "chkWidowOrphan"
    End Sub
    'UPGRADE_WARNING: Event chkPageBreak.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkPageBreak_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPageBreak.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkPageBreak_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPageBreak.Leave
        m_xPrevCtl = "chkPageBreak"
    End Sub
    'UPGRADE_WARNING: Event chkLegalStyle.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkLegalStyle_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLegalStyle.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
            FormatLegalNumber()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkLegalStyle_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLegalStyle.Leave
        m_xPrevCtl = "chkLegalStyle"
    End Sub

    'UPGRADE_WARNING: Event chkNonbreakingSpaces.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkNonbreakingSpaces_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNonbreakingSpaces.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkNonbreakingSpaces_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNonbreakingSpaces.Leave
        m_xPrevCtl = "chkNonbreakingSpaces"
    End Sub

    'UPGRADE_WARNING: Event chkNumberBold.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkNumberBold_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberBold.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkNumberBold_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberBold.Leave
        m_xPrevCtl = "chkNumberBold"
    End Sub

    Private Sub chkNumberBold_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles chkNumberBold.MouseDown
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_iPrevTRISTATE = Me.chkNumberBold.CheckState
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'UPGRADE_WARNING: Event chkNumberCaps.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkNumberCaps_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberCaps.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkNumberCaps_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberCaps.Leave
        m_xPrevCtl = "chkNumberCaps"
    End Sub

    Private Sub chkNumberCaps_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles chkNumberCaps.MouseDown
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_iPrevTRISTATE = Me.chkNumberCaps.CheckState
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'UPGRADE_WARNING: Event chkNumberItalic.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkNumberItalic_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberItalic.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkNumberItalic_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkNumberItalic.Leave
        m_xPrevCtl = "chkNumberItalic"
    End Sub

    Private Sub chkNumberItalic_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles chkNumberItalic.MouseDown
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_iPrevTRISTATE = Me.chkNumberItalic.CheckState
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'UPGRADE_WARNING: Event chkReset.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkReset_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkReset.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkReset_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkReset.Leave
        m_xPrevCtl = "chkReset"
    End Sub

    'UPGRADE_WARNING: Event chkRightAlign.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkRightAlign_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRightAlign.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkRightAlign_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkRightAlign.Leave
        m_xPrevCtl = "chkRightAlign"
    End Sub

    'UPGRADE_WARNING: Event chkTCBold.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkTCBold_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCBold.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            If Me.optStyleFormats.Checked Then m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkTCBold_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCBold.Leave
        m_xPrevCtl = "chkTCBold"
    End Sub

    'UPGRADE_WARNING: Event chkTCCaps.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkTCCaps_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCCaps.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            If Me.optStyleFormats.Checked Then m_bParaDirty = True
            If chkTCCaps.CheckState = 1 Then chkTCSmallCaps.CheckState = System.Windows.Forms.CheckState.Unchecked
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkTCCaps_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCCaps.Leave
        m_xPrevCtl = "chkTCCaps"
    End Sub

    'UPGRADE_WARNING: Event chkTCItalic.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkTCItalic_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCItalic.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            If Me.optStyleFormats.Checked Then m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkTCItalic_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCItalic.Leave
        m_xPrevCtl = "chkTCItalic"
    End Sub

    'UPGRADE_WARNING: Event chkTCSmallCaps.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkTCSmallCaps_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCSmallCaps.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            If Me.optStyleFormats.Checked Then m_bParaDirty = True
            If chkTCSmallCaps.CheckState = 1 Then chkTCCaps.CheckState = System.Windows.Forms.CheckState.Unchecked
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkTCSmallCaps_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCSmallCaps.Leave
        m_xPrevCtl = "chkTCSmallCaps"
    End Sub

    'UPGRADE_WARNING: Event chkTCUnderline.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkTCUnderline_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCUnderline.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            If Me.optStyleFormats.Checked Then m_bParaDirty = True
            If Not m_bStopChangeEvent Then RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub chkTCUnderline_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCUnderline.Leave
        m_xPrevCtl = "chkTCUnderline"
    End Sub

    Private Sub cmbContFontSize_Change(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            'GLOG 5262 (9.9.5003) - I was previously setting the wrong flags here
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'Private Sub cmbFontName_Click()
    Private Sub cmbFontName_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFontName.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            m_bParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbFontName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFontName.Leave
        Try
            m_xPrevCtl = "cmbFontName"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbFontSize_Change(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            m_bParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'Private Sub cmbFontSize_Click()
    Private Sub cmbFontSize_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bTCDirty = True
            m_bParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbFontSize_Validated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbFontSize.Validated
        'GLOG 15978
        Try
            m_xPrevCtl = "cmbFontSize"

            'GLOG 5031 (2/1/12) - don't refresh if cancelling
            If Not Me.ActiveControl Is Me.cmdCancel Then
                RefreshLevel()
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'Private Sub cmbContFontName_Click()
    Private Sub cmbContFontName_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContFontName.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbContFontName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContFontName.Leave
        m_xPrevCtl = "cmbContFontName"
    End Sub

    'Private Sub cmbContFontSize_Click()
    Private Sub cmbContFontSize_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbContFontSize_Validated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContFontSize.Validated
        m_xPrevCtl = "cmbContFontSize"
    End Sub

    'Private Sub cmbAlignment_Click()
    Private Sub cmbAlignment_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbAlignment.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbAlignment_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbAlignment.Leave
        m_xPrevCtl = "cmbAlignment"
    End Sub

    Private Sub cmbNumFontName_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumFontName.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNumFontName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumFontName.Leave
        Try
            m_xPrevCtl = "cmbNumFontName"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNumFontSize_Change(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'Private Sub cmbNumFontSize_Click()
    Private Sub cmbNumFontSize_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumFontSize.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNumFontSize_Validated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumFontSize.Validated
        'GLOG 15987
        Try
            m_xPrevCtl = "cmbNumFontSize"

            'GLOG 5031 (2/1/12) - don't refresh if cancelling
            If Not Me.ActiveControl Is Me.cmdCancel Then
                RefreshLevel()
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbLineSpacing_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbLineSpacing.ValueChanged
        Dim iSpacing As Microsoft.Office.Interop.Word.WdLineSpacing

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbLineSpacing.Bookmark. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iSpacing = Me.cmbLineSpacing.SelectedIndex
            Me.lblAt.Enabled = (iSpacing > 2)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnAt
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Enabled = (iSpacing > 2)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .AppendSymbol = (iSpacing <> Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple)
                If iSpacing = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .MinValue = 0.5
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .IncrementValue = 0.5
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .MinValue = 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .IncrementValue = 1
                End If
                If Not m_bLevelChanged Then
                    Select Case iSpacing
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceSingle, Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceAtLeast, Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceExactly
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 12
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpace1pt5
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 18
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceDouble
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 24
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 3
                    End Select
                    If iSpacing < 3 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .DisplayText = ""
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Refresh()
                    End If
                End If
            End With
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbLineSpacing_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbLineSpacing.Leave
        Try
            m_xPrevCtl = "cmbLineSpacing"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNextParagraph_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNextParagraph.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            If Not m_bFillingNextParaList Then
                m_bParaDirty = True
                m_bNextParaDirty = True
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNextParagraph_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNextParagraph.Leave
        m_xPrevCtl = "cmbNextParagraph"
    End Sub

    Private Sub cmbNumberStyle_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumberStyle.ValueChanged
        Dim iNumberStyle As Short
        Dim iLevel As Short
        Dim iStartAt As Short
        Dim iPos As Short
        Dim iOldNumLength As Short
        Dim xNewNumber As String
        Dim bEnabled As Boolean
        Dim xLT As String
        Dim bReformatForMax As Boolean
        Dim lMax As Integer

        Try
            iNumberStyle = cmbNumberStyle.SelectedIndex
            If iNumberStyle = -1 Then Exit Sub
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            If iNumberStyle > 9 Then iNumberStyle = iNumberStyle + m_iFixedDigitNumberingOffset

            Dim oDlg As Word.Dialog
            If iNumberStyle = mpListNumberStyleNewBullet Then
                oDlg = CurWordApp.Dialogs.Item(Microsoft.Office.Interop.Word.WdWordDialog.wdDialogInsertSymbol)
                If oDlg.Display() = -1 Then
                    If oDlg.CharNum Then
                        m_xBulletFont = oDlg.Font
                        m_xBulletCharNum = ChrW(oDlg.CharNum)
                    End If
                End If
                iNumberStyle = mpListNumberStyleBullet
                Me.cmbNumberStyle.SelectedIndex = Me.cmbNumberStyle.SelectedIndex - 1
                Me.cmbNumberStyle.Focus()
            End If

            'if this is an unsupported style, we'll leave the level tokens unevaluated
            m_bRawNumberFormat = (iNumberStyle > 13)

            iLevel = m_iLevel
            xLT = xGetFullLTName((g_oCurScheme.Name))

            '   set max StartAt value - some styles have limits
            Select Case Me.cmbNumberStyle.Text
                Case "Zodiac 1"
                    lMax = 10
                Case "Zodiac 2"
                    lMax = 12
                Case "Zodiac 3"
                    lMax = 60
                Case Else
                    lMax = 1000
            End Select
            Me.udStartAt.Maximum = lMax

            '   fill StartAt
            If m_bLevelChanged Then
                iStartAt = CurWordApp.ActiveDocument.ListTemplates.Item(xLT).ListLevels.Item(iLevel).StartAt
                If iStartAt > 1000 Then
                    '           this won't by itself trigger save or
                    '           refresh preview, but it's the easiest way
                    '           to prevent errors and unintended consequences
                    CurWordApp.ActiveDocument.ListTemplates.Item(xLT).ListLevels.Item(iLevel).StartAt = 1000
                    iStartAt = 1000
                    bReformatForMax = True
                End If
            Else
                iStartAt = 1
            End If

            If iStartAt <> 0 Then
                '       we don't allow up/down to go to 0, but Word does;
                '       0 setting is preserved in bCreatelevel
                udStartAt.Value = iStartAt
            End If

            If m_bRawNumberFormat Then
                'style is unsupported - display arabic
                Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic) & " "
            Else
                Select Case iNumberStyle
                    Case mpListNumberStyleNone, mpListNumberStyleBullet
                        'no start at value
                        Me.txtStartAt.Text = ""
                    Case mpListNumberStyleOrdinal, mpListNumberStyleOrdinalText, mpListNumberStyleCardinalText
                        'display arabic
                        Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic) & " "
                    Case Else
                        'display actual character
                        Me.txtStartAt.Text = xIntToListNumStyle(iStartAt, iMapNumberStyle_MPToWord(iNumberStyle)) & Space(1)
                End Select
            End If

            bEnabled = (iNumberStyle <> mpListNumberStyleNone) And (iNumberStyle <> mpListNumberStyleBullet)
            Me.udStartAt.Enabled = bEnabled
            Me.txtStartAt.Enabled = bEnabled
            Me.lblStartAt.Enabled = bEnabled

            '   enable/disable legal style
            If iNumberStyle = mpListNumberStyleArabic Or iNumberStyle = mpListNumberStyleArabicLZ Then
                Me.chkLegalStyle.Enabled = True
            Else
                With Me.chkLegalStyle
                    .Enabled = False
                    If .CheckState = System.Windows.Forms.CheckState.Checked Then
                        .CheckState = System.Windows.Forms.CheckState.Unchecked
                    End If
                End With
            End If

            '   enable/disable restart numbering
            With Me.chkReset
                If iLevel = 1 Or iNumberStyle = mpListNumberStyleNone Or iNumberStyle = mpListNumberStyleBullet Then
                    m_bStopChangeEvent = True
                    .CheckState = System.Windows.Forms.CheckState.Unchecked
                    .Enabled = False
                Else
                    .Enabled = True
                End If
                If Not m_bLevelChanged Then .CheckState = System.Math.Abs(CInt(.Enabled))
            End With

            '   enable/disable previous levels
            Me.lstPrevLevels.Enabled = Not mpListNumberStyleBullet

            '   if no number, default to no trailing character
            '    If (iNumberStyle = mpListNumberStyleNone) And _
            ''            (Me.rtfNumberFormat.Text = "") And _
            ''            (Not m_bLevelChanged) Then
            '       Me.cmbTrailingChar.ListIndex = mpTrailingChar_None
            '    End If

            '   update number format
            If (Not m_bLevelChanged) Or bReformatForMax Then
                m_bStopChangeEvent = True
                If iNumberStyle = mpListNumberStyleBullet Then
                    With Me.rtfNumberFormat
                        .Enabled = False
                        .Text = ChrW(&HB7)
                        .SelectionFont = New Drawing.Font("Symbol", .Font.Size)
                        m_xCode = ChrW(&HB7)
                    End With

                    '           update Extras tab
                    Me.cmbNumFontName.Value = m_xBulletFont

                Else 'anything other than bullet
                    If m_bRawNumberFormat Then
                        xNewNumber = "%" & iLevel
                    Else
                        xNewNumber = xIntToListNumStyle(iStartAt, iMapNumberStyle_MPToWord(iNumberStyle))
                    End If
                    iPos = InStr(m_xCode, LTrim(Str(iLevel)))
                    If iPos = 0 Then 'no number
                        iPos = 1
                    End If
                    iOldNumLength = lCountChrs(m_xCode, LTrim(Str(iLevel)))

                    '           insert and format new number
                    With Me.rtfNumberFormat
                        '               if style was previously bullet
                        '               clear and enable text box
                        If .Enabled = False Or .Font.Name = "Symbol" Then
                            .Enabled = True
                            .SelectionFont = New Drawing.Font("Microsoft Sans Serif", .Font.Size)
                            .Text = ""
                            m_xCode = ""
                            .SelectionColor = System.Drawing.Color.Blue

                            '                   update Extras tab
                            If Me.cmbNumFontName.Text = "Symbol" Or Me.cmbNumFontName.Text = "Wingdings" Then
                                Me.cmbNumFontName.Value = Me.cmbFontName.Text
                            End If
                        End If
                        .SelectionStart = iPos - 1
                        .SelectionLength = iOldNumLength
                        .SelectedText = xNewNumber
                        If iOldNumLength = 0 Then
                            .SelectionStart = iPos - 1
                            .SelectionLength = Len(xNewNumber)
                            .SelectionColor = System.Drawing.Color.Blue
                            .SelectionStart = 0
                        End If
                        xUserAll = .Rtf
                        '                .SelColor = vbBlue
                    End With

                    If m_xCode = "" Then
                        '               start new code
                        m_xCode = New String(LTrim(Str(iLevel)), Len(xNewNumber))
                    Else
                        '               edit existing code
                        m_xCode = VB.Left(m_xCode, iPos - 1) & New String(LTrim(Str(iLevel)), Len(xNewNumber)) & VB.Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
                    End If
                End If
                m_bStopChangeEvent = False
            End If
            m_bAllowRefresh = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbNumberStyle_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNumberStyle.Leave
        Try
            m_xPrevCtl = "cmbNumberStyle"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbTextAlign_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTextAlign.ValueChanged
        Dim bEnabled As Boolean

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
            m_bPropDirty = True

            '   enable/disable indent controls
            bEnabled = (Not (Me.cmbTextAlign.Text = "Centered")) And Me.cmbTrailingChar.SelectedIndex = mpTrailingChars.mpTrailingChar_Tab

            Me.lblTabPosition.Enabled = bEnabled
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.spnTabPosition.Enabled = bEnabled
            If Not bEnabled Then Me.chkRightAlign.CheckState = System.Windows.Forms.CheckState.Unchecked

            If (m_bStopChangeEvent = False) And (Me.cmbTextAlign.Text = "Centered") Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnTextPosition.Value = Me.spnRightIndent.Value
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnNumberPosition.Value = Me.spnRightIndent.Value
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbTextAlign_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTextAlign.Leave
        Try
            m_xPrevCtl = "cmbTextAlign"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbTrailingChar_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTrailingChar.ValueChanged
        Dim bEnabled As Boolean

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            m_bPropDirty = True

            With Me.cmbTrailingChar
                bEnabled = (.SelectedIndex = mpTrailingChars.mpTrailingChar_Tab)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnTabPosition.Enabled = bEnabled
                Me.lblTabPosition.Enabled = bEnabled

                '        bEnabled = _
                ''            (.ListIndex <> mpTrailingChar_ShiftReturn) And _
                ''            (.ListIndex <> mpTrailingChar_DoubleShiftReturn) And _
                ''            (.ListIndex <> mpTrailingChar_None) And _
                ''            Me.chkNumberUnderline = vbChecked
                '
                '        Me.chkTrailingUnderline.Enabled = bEnabled
                '        If Not Me.chkTrailingUnderline.Enabled Then
                '            Me.chkTrailingUnderline = vbUnchecked
                '        End If
            End With
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbTrailingChar_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTrailingChar.Leave
        '   This will err when tring to add space to NumberFormat property;
        '   11/8/01 - two spaces moved outside number, so this is no longer problematic
        '    If (Me.cmbNumberStyle = "Bullet") And _
        ''            (Me.cmbTrailingChar.ListIndex = _
        ''            mpTrailingChar_DoubleSpace) Then
        '        MsgBox "Two spaces are not available as the trailing characters " & _
        ''            "for bullets.  Please select one of the other options.", _
        ''            vbInformation, g_xAppName
        '        With Me.cmbTrailingChar
        '            .ListIndex = mpTrailingChar_Tab
        '            .SetFocus
        '        End With
        '        Exit Sub
        '    End If
        Try
            m_xPrevCtl = "cmbTrailingChar"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'Private Sub cmbUnderlineStyle_Click()
    Private Sub cmbUnderlineStyle_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbUnderlineStyle.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bPropDirty = True
            m_bLevelDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbUnderlineStyle_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbUnderlineStyle.Leave
        Try
            m_xPrevCtl = "cmbUnderlineStyle"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmdAddLevel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAddLevel.Click
        Dim xNumberFormat As String
        Dim xContStyle As String

        Try
            '   can't add tenth level
            If m_iCurLevels = 9 Then
                MsgBox("The current scheme already has nine levels.", MsgBoxStyle.Information, g_xAppName)
                Exit Sub
            End If

            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            EchoOff()

            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '   update current level
            If bLevelIsDirty() Then
                '       implement changes
                'UPGRADE_WARNING: Couldn't resolve default property of object bCreateLevel(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bCreateLevel((g_oCurScheme.Name), m_iLevel)

                '       cont style
                If m_bNextParaDirty Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bRet = bCreateContLevel((g_oCurScheme.Name), m_iLevel)
                End If

                '       update dirty flag array
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bSetDirt(m_iLevel, m_bLevelDirty, m_bTCDirty, m_bTOCDirty, m_bPropDirty, m_bParaDirty, m_bNextParaDirty)

                '       reset dirty flags
                ClearSchemeDirtFlags()
            End If

            '   add new level
            m_iCurLevels = m_iCurLevels + 1
            lRet = lAddLevel((g_oCurScheme.Name), m_iCurLevels)

            '   update dirty flag array
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bSetDirt(m_iCurLevels, True, True, True, True, True, True)

            '   update next paragraph dropdown
            FillNextParaList(m_iCurLevels)
            '    xContStyle = xGetStyleRoot(g_oCurScheme.Name) & _
            ''        " Cont " & m_iCurLevels
            '    Me.cmbNextParagraph.AddItem xContStyle

            '   recreate preview
            m_oPreview.RefreshPreviewLevel((g_oCurScheme.Name), m_iCurLevels)

            '   enable/disable add/delete level buttons
            Me.cmdAddLevel.Enabled = (m_iCurLevels < 9)
            Me.cmdDeleteLevel.Enabled = True

            '   move buttons
            RedrawLevelButtons()

            'switch to new level
            Dim oItem As ToolStripItem
            oItem = tsLevel.Items(m_iCurLevels - 1)
            oItem.PerformClick()

        Finally
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()
            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            '   send focus back to form - bug in Word 2000
            '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
            'screen capture and design mode execution
            If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bCancelled = True
            Me.Close()
            '    Application.ScreenUpdating = False
            '    ResetListNumberFonts
            '    Application.ScreenUpdating = True
            '    Application.ScreenRefresh
            'CurWordApp.Activate()
            '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
            'screen capture and design mode execution
            If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmdDeleteLevel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDeleteLevel.Click
        Dim iLevel As Short
        Dim i As Short
        Dim oItem As ToolStripItem
        Dim oTbtn As ToolStripButton

        Try
            '   disallow deletion of only level
            If m_iCurLevels = 1 Then
                MsgBox("The current scheme has only one level.", MsgBoxStyle.Information, g_xAppName)
                Exit Sub
            End If

            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '   get level
            iLevel = m_iLevel

            '   prompt for confirmation
            lRet = MsgBox("Delete level " & iLevel & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, g_xAppName)

            If lRet = MsgBoxResult.Yes Then
                'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                EchoOff()
                SetUserSetting("Numbering", "Zoom", CStr(cmdZoom.CheckState))
                '       delete level
                lRet = lDeleteLevel((g_oCurScheme.Name), m_iCurLevels, iLevel)

                '       update dirty flag array
                If iLevel < m_iCurLevels Then
                    For i = iLevel To m_iCurLevels - 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        bRet = bSetDirt(i, True, True, True, True, True, True)
                    Next i
                End If

                '       mark deleted level as dirty - this will force
                '       the "deleted" listlevel to be saved.
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bSetDirt(m_iCurLevels, True, 0, 0, True, 0, 0)

                '       remove level from list
                m_iCurLevels = m_iCurLevels - 1

                'uncheck last level
                oItem = tsLevel.Items(m_iCurLevels)
                oTbtn = CType(oItem, ToolStripButton)
                oTbtn.Checked = False

                '       recreate preview
                CurWordApp.ActiveDocument.Content.Delete()
                m_oPreview.ShowPreview(g_oCurScheme.Name, mpSchemeTypes.mpSchemeType_Document)

                '       remove next para style
                FillNextParaList(m_iCurLevels)

                '        Me.cmbNextParagraph.RemoveItem Me.cmbNextParagraph.ListCount - 1

                '       select appropriate level chkbox
                If iLevel > m_iCurLevels Then
                    'select last level
                    oItem = tsLevel.Items(m_iCurLevels - 1)
                    oItem.PerformClick()
                Else
                    m_iLastSelLevel = m_iLastSelLevel + 1
                    '           refresh dlg for selected level
                    SwitchLevels()
                End If

                m_oPreview.HighlightLevel(m_iLevel)

                '       enable/disable add/delete level buttons
                Me.cmdDeleteLevel.Enabled = Me.tsLevel.Items(1).Visible
                Me.cmdAddLevel.Enabled = True

                '       redraw buttons
                RedrawLevelButtons()
            End If
        Catch e As Exception
            [Error].Show(e)
        Finally
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()

            '   send focus back to form - bug in Word 2000
            '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
            'screen capture and design mode execution
            If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()

            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Private Sub cmdSynchronize_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSynchronize.Click
        Dim xRoot As String
        Dim xCont As String
        Dim xStyle As String
        Dim iLevel As Short

        Try
            '   ensure that list level tab position reflects
            '   actual indents - user may have edited style
            '   indents in Word
            m_bLevelDirty = True
            RefreshLevel()

            '   update style
            CreateContStyles(g_oCurScheme.Name, m_iLevel, True)

            '   update dialog
            m_bLevelChanged = True
            lLoadContLevel()
            m_bLevelChanged = False

            '   update dirty flags
            xarDirty(m_iLevel, mpNextParaIsDirty) = "True"
            m_bNextParaDirty = False

            '   notify user
            iLevel = m_iLevel
            xStyle = xGetStyleName(g_oCurScheme.Name, iLevel)
            xRoot = xGetStyleRoot(g_oCurScheme.Name)
            xCont = xRoot & " Cont " & iLevel
            CurWordApp.StatusBar = xCont & " style has been synched with " & xStyle & " style."
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmdSynchronize_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSynchronize.Leave
        m_xPrevCtl = "cmdSynchronize"
    End Sub

    Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '1/11/17 (dm) - validate spinners
            If Not ValidateSpinners() Then
                Exit Sub
            End If

            RefreshLevel()
            m_bCancelled = False
            Me.Close()
            '    Application.ScreenUpdating = False
            '    ResetListNumberFonts
            '    Application.ScreenUpdating = True
            '    Application.ScreenRefresh
            'CurWordApp.Activate()
            '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
            'screen capture and design mode execution
            If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmdUpdate_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUpdate.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub frmEditScheme_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Try
            If eventArgs.Modifiers = Keys.Alt Then
                Select Case KeyCode
                    Case Keys.D1, Keys.NumPad1
                        Me.tbtnLevel1.PerformClick()
                    Case Keys.D2, Keys.NumPad2
                        Me.tbtnLevel2.PerformClick()
                    Case Keys.D3, Keys.NumPad3
                        Me.tbtnLevel3.PerformClick()
                    Case Keys.D4, Keys.NumPad4
                        Me.tbtnLevel4.PerformClick()
                    Case Keys.D5, Keys.NumPad5
                        Me.tbtnLevel5.PerformClick()
                    Case Keys.D6, Keys.NumPad6
                        Me.tbtnLevel6.PerformClick()
                    Case Keys.D7, Keys.NumPad7
                        Me.tbtnLevel7.PerformClick()
                    Case Keys.D8, Keys.NumPad8
                        Me.tbtnLevel8.PerformClick()
                    Case Keys.D9, Keys.NumPad9
                        Me.tbtnLevel9.PerformClick()
                    Case Keys.Oemplus
                        Me.cmdAddLevel.PerformClick()
                    Case Keys.OemMinus
                        Me.cmdDeleteLevel.PerformClick()
                End Select
            Else
                Select Case KeyCode
                    Case System.Windows.Forms.Keys.F5
                        RefreshLevel()
                    Case Keys.Escape
                        Me.cmdCancel.PerformClick()
                End Select
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub frmEditScheme_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim j As Short
        Dim xStyleRoot As String
        Dim sDlgTop As Single
        Dim sDlgLeft As Single
        Dim iZoom As Short
        Dim bCreateBitmap As Boolean
        Dim xLT As String
        Dim aFont As Object
        Dim iNumFonts As Short
        Dim xFonts() As String
        Dim bRestrictFonts As Boolean
        Dim lUnit As Integer
        Dim sIncrement As Single
        Dim xarUnderlineStyles(,) As String
        Dim xarFontName(,) As String
        Dim xarSize(,) As String
        Dim xarLineSpacing(,) As String
        Dim xarSpecial(,) As String
        Dim xarTrailingChars(,) As String
        Dim xarAlignment(,) As String
        Dim iNumStyleCount As Short
        Dim xNumStyle As String
        Dim bActiveDocIs2010Compatible As Boolean 'GLOG 5622

        Try
            Me.KeyPreview = True

            'don't reinitialize if form is being reshown after zoom
            If m_bZoomClicked Then
                m_bZoomClicked = False
                Exit Sub
            End If

            'ReDim g_sNumberFontSizes(8) 'GLOG 15793 (dm)

            m_bFormInit = True
            m_bCancelled = True
            m_bAllowRefresh = True
            m_iSelStart = -1

            'adjust for scaling
            Dim sFactor = CScaling.GetScalingFactor()

            If sFactor > 1 Then
                Me.cmdSave.Width = Me.cmdSave.Width * sFactor
                Me.cmdCancel.Width = Me.cmdCancel.Width * sFactor
                Me.cmdUpdate.Width = Me.cmdUpdate.Width * sFactor
                Me.cmdZoom.Width = Me.cmdZoom.Width * sFactor
                Me.cmdSave.Height = Me.cmdSave.Height + (sFactor - 1) * 20
                Me.cmdCancel.Height = Me.cmdCancel.Height + (sFactor - 1) * 40
                Me.cmdUpdate.Height = Me.cmdUpdate.Height + (sFactor - 1) * 40
                Me.cmdZoom.Height = Me.cmdZoom.Height + (sFactor - 1) * 40

                Dim oParentCtl As ToolStrip
                oParentCtl = Me.splitContainer1.Panel1.Controls("pnlLevels").Controls("tsLevel")

                Dim oCtl As ToolStripItem
                For Each oCtl In oParentCtl.Items
                    oCtl.Width = oCtl.Width * sFactor
                    oCtl.Height = oCtl.Height * sFactor
                Next

                Me.txtStartAt.Width = (Me.udStartAt.Left + Me.udStartAt.Width) - Me.txtStartAt.Left - 18
            End If

            'Adjust Column 2 to fill right space
            lstPrevLevels.Columns(1).Width = lstPrevLevels.Width - lstPrevLevels.Columns(0).Width
            '   set top/left to ini values
            'remmed out 1/31/14 (see note below)
            '    On Error Resume Next
            '    sDlgTop = GetUserSetting("Numbering", "DlgTop")
            '    sDlgLeft = GetUserSetting("Numbering", "DlgLeft")
            '    On Error GoTo ProcError

            'GLOG 5266 (9.9.5004) - this is problematic for users moving between different
            'environments/monitors - Word locks up if positions are off the screen - trap errors
            '1/31/14 (9.9.5005) - removed this code altogether, as there were settings that
            'positioned the dialog without the screen without causing an error - dialog is now
            'configured for center owner
            '    If sDlgTop Then
            '        On Error Resume Next
            '        Me.Top = sDlgTop
            '        If Err > 0 Then _
            ''            sDlgTop = 0
            '        On Error GoTo ProcError
            '    End If
            '
            '    If sDlgTop = 0 Then
            ''       vertically center top
            '        Me.Top = Screen.Height / 2 - Me.Height / 2
            '    End If
            '
            '    If sDlgLeft Then
            '        On Error Resume Next
            '        Me.Left = sDlgLeft
            '        If Err > 0 Then _
            ''            sDlgLeft = 0
            '        On Error GoTo ProcError
            '    End If
            '
            '    If sDlgLeft = 0 Then
            ''       horizontally center width
            '        Me.Left = Screen.Width / 2 - Me.Width / 2
            '    End If

            m_oPreview = New MacPacNumbering.LMP.Numbering.cPreview
            'm_xarIsDirty = New XArrayObject.XArray
            'm_xarPrevLevels = New XArrayObject.XArray
            'xarFontName = New XArrayObject.XArray
            'm_xarNumFontName = New XArrayObject.XArray
            'xarSize = New XArrayObject.XArray
            'xarLineSpacing = New XArrayObject.XArray

            If Not g_oStatus Is Nothing Then
                g_oStatus.StatusText = "Creating Preview.  Please wait..."
            End If

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            'initialize bullet variables to default
            m_xBulletFont = "Symbol"
            m_xBulletCharNum = ChrW(&HB7)

            '   get number of levels
            m_iInitLevels = iGetLevels(g_oCurScheme.Name, mpSchemeTypes.mpSchemeType_Document)
            m_iCurLevels = m_iInitLevels

            '   disable chkboxes for non-existent levels
            For i = m_iCurLevels To 8
                With Me.tsLevel.Items(i)
                    .Visible = False
                    .Text = ""
                End With
            Next i

            '   set caption
            Me.Text = " Edit " & g_oCurScheme.DisplayName & " Scheme - Number"

            '   fill Special array
            ReDim xarSpecial(2, 1)
            xarSpecial(0, 0) = "(none)"
            xarSpecial(0, 1) = "(none)"
            xarSpecial(1, 0) = "First Line"
            xarSpecial(1, 1) = "First Line"
            xarSpecial(2, 0) = "Hanging"
            xarSpecial(2, 1) = "Hanging"

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbSpecial.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbSpecial.SetList(xarSpecial)

            '   fill alignment array
            ReDim xarAlignment(4, 1)
            xarAlignment(0, 0) = "Left"
            xarAlignment(0, 1) = "0"
            xarAlignment(1, 0) = "Centered"
            xarAlignment(1, 1) = "1"
            xarAlignment(2, 0) = "Right"
            xarAlignment(2, 1) = "2"
            xarAlignment(3, 0) = "Justified"
            xarAlignment(3, 1) = "3"
            xarAlignment(4, 0) = "Adjust to Normal Style"
            xarAlignment(4, 1) = "4"

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbAlignment.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbAlignment.SetList(xarAlignment)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbTextAlign.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbTextAlign.SetList(xarAlignment)

            '   fill line spacing array
            ReDim m_xarLineSpacing(5, 1)
            m_xarLineSpacing(0, 0) = "Single"
            m_xarLineSpacing(0, 1) = "Single"
            m_xarLineSpacing(1, 0) = "1.5 lines"
            m_xarLineSpacing(1, 1) = "1.5 lines"
            m_xarLineSpacing(2, 0) = "Double"
            m_xarLineSpacing(2, 1) = "Double"
            m_xarLineSpacing(3, 0) = "At least"
            m_xarLineSpacing(3, 1) = "At least"
            m_xarLineSpacing(4, 0) = "Exactly"
            m_xarLineSpacing(4, 1) = "Exactly"
            m_xarLineSpacing(5, 0) = "Multiple"
            m_xarLineSpacing(5, 1) = "Multiple"

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbLineSpacing.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbLineSpacing.SetList(m_xarLineSpacing)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContLineSpacing.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbContLineSpacing.SetList(m_xarLineSpacing)

            '   fill trailing chars array
            ReDim xarTrailingChars(5, 1)
            xarTrailingChars(0, 0) = "(Nothing)"
            xarTrailingChars(0, 1) = "0"
            xarTrailingChars(1, 0) = "Tab"
            xarTrailingChars(1, 1) = "1"
            xarTrailingChars(2, 0) = "Space"
            xarTrailingChars(2, 1) = "2"
            xarTrailingChars(3, 0) = "Two Spaces"
            xarTrailingChars(3, 1) = "3"
            xarTrailingChars(4, 0) = "Line Break"
            xarTrailingChars(4, 1) = "4"
            xarTrailingChars(5, 0) = "Two Line Breaks"
            xarTrailingChars(5, 1) = "5"

            '   fill next paragraph list
            FillNextParaList(m_iInitLevels)

            '   underline dropdown
            With Me.cmbUnderlineStyle
                .ClearList()

                ReDim xarUnderlineStyles(UBound(g_xUnderlineFormats), 1)
                For i = 0 To UBound(g_xUnderlineFormats)
                    xarUnderlineStyles(i, 0) = g_xUnderlineFormats(i, 0)
                    xarUnderlineStyles(i, 1) = CInt(g_xUnderlineFormats(i, 1))
                Next i
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbUnderlineStyle.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .SetList(xarUnderlineStyles)

                '        For i = 0 To UBound(g_xUnderlineFormats)
                '            .AddItem g_xUnderlineFormats(i, 0)
                '            .ItemData(.NewIndex) = CLng(g_xUnderlineFormats(i, 1))
                '        Next i
                '        .Refresh
            End With

            '   fill trailing char dropdown
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbTrailingChar.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbTrailingChar.SetList(xarTrailingChars)

            'GLOG 15793 - moved to beEditScheme()
            ''   store list template font sizes - these will have to be forced
            ''   for preview
            'xLT = xGetFullLTName((g_oCurScheme.Name))
            'With CurWordApp.ActiveDocument.ListTemplates.Item(xLT)
            '    For i = 1 To m_iCurLevels
            '        g_sNumberFontSizes(i - 1) = .ListLevels.Item(i).Font.Size
            '    Next i
            '    For i = m_iCurLevels + 1 To 9
            '        g_sNumberFontSizes(i - 1) = Microsoft.Office.Interop.Word.WdConstants.wdUndefined
            '    Next i
            'End With

            '   get fonts
            Try
                bRestrictFonts = CBool(GetAppSetting("Numbering", "RestrictFontList"))
            Catch
            End Try

            If bRestrictFonts Then
                '       load list from ini
                If g_xPermittedFonts(0) <> "" Then

                    'fill cmbNumFontName
                    ReDim m_xarNumFontName(UBound(g_xPermittedFonts) + 2, 1)
                    ReDim xarFontName(UBound(g_xPermittedFonts), 1)

                    m_xarNumFontName(0, 0) = "-Undefined-"
                    m_xarNumFontName(0, 1) = "-Undefined-"

                    For i = LBound(g_xPermittedFonts) To UBound(g_xPermittedFonts)
                        m_xarNumFontName(i + 1, 0) = g_xPermittedFonts(i)
                        m_xarNumFontName(i + 1, 1) = g_xPermittedFonts(i)
                        xarFontName(i, 0) = g_xPermittedFonts(i)
                        xarFontName(i, 1) = g_xPermittedFonts(i)
                    Next i

                    m_xarNumFontName(i + 1, 0) = "Symbol"
                    m_xarNumFontName(i + 1, 1) = "Symbol"

                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.cmbNumFontName.SetList(m_xarNumFontName)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.cmbFontName.SetList(xarFontName)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.cmbContFontName.SetList(xarFontName)

                End If
            Else
                'reset counter
                i = 0

                '       load system fonts
                'GLOG 4839 - omit vertical fonts (names start with '@')
                For Each aFont In CurWordApp.FontNames
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If VB.Left(aFont, 1) <> "@" Then
                        iNumFonts = iNumFonts + 1
                    End If
                Next aFont
                ReDim xFonts(iNumFonts - 1)

                '       stick in variant array
                For Each aFont In CurWordApp.FontNames
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If VB.Left(aFont, 1) <> "@" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        xFonts(i) = aFont
                        i = i + 1
                    End If
                Next aFont

                '       sort array
                'UPGRADE_WARNING: Couldn't resolve default property of object WordBasic.SortArray. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                CurWordApp.WordBasic.SortArray(xFonts)

                '       add array items to lists
                ReDim m_xarNumFontName(UBound(xFonts) + 1, 1)
                m_xarNumFontName(0, 0) = "-Undefined-"
                m_xarNumFontName(0, 1) = "-Undefined-"

                ReDim xarFontName(UBound(xFonts), 1)

                For i = 0 To iNumFonts - 1
                    'GLOG 4839 - omit vertical fonts (names start with '@')
                    If VB.Left(xFonts(i), 1) <> "@" Then
                        m_xarNumFontName(i + 1, 0) = xFonts(i)
                        m_xarNumFontName(i + 1, 1) = xFonts(i)
                        xarFontName(i, 0) = xFonts(i)
                        xarFontName(i, 1) = xFonts(i)
                    End If
                Next i

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cmbNumFontName.SetList(m_xarNumFontName)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cmbFontName.SetList(xarFontName)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContFontName.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cmbContFontName.SetList(xarFontName)

            End If

            '   get font sizes
            ReDim xarSize(20, 1)
            xarSize(0, 0) = "-Undefined-"
            xarSize(0, 1) = "-Undefined-"

            For i = 7 To 26
                xarSize(i - 6, 0) = CStr(i)
                xarSize(i - 6, 1) = CStr(i)
            Next i

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumFontSize.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbNumFontSize.SetList(xarSize)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContFontSize.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbContFontSize.SetList(xarSize)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbFontSize.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbFontSize.SetList(xarSize)

            '   set spinners to display Word unit of measurement
            lUnit = CurWordApp.Options.MeasurementUnit
            sIncrement = GetStandardIncrement(lUnit)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnNumberPosition
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnRightIndent
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnTabPosition
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnTextPosition
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnContLeftIndent
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnContRightIndent
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnBy
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .DisplayUnit = lUnit
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .IncrementValue = sIncrement
            End With

            '   set base count
            iNumStyleCount = 12

            'new fixed-digit numbering formats are only available in documents with
            'Word 2010 compatibility mode
            Dim bAllow As Boolean
            If g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                'if editing a document scheme, only display if source doc also allows
                bAllow = (CurWordApp.Documents.Item(g_xCurDest).CompatibilityMode >= 14)
            Else
                bAllow = True
            End If

            'GLOG 5622 - put in variable here, since can't call in Word 2007
            bActiveDocIs2010Compatible = (CurWordApp.ActiveDocument.CompatibilityMode >= 14)
            If bAllow And bActiveDocIs2010Compatible Then
                '           reset count
                iNumStyleCount = 15
            Else
                m_iFixedDigitNumberingOffset = 3
            End If

            'account for supplemental styles
            If g_xSupplementalNumberStyles(0) <> "" Then
                For i = 0 To UBound(g_xSupplementalNumberStyles)
                    '           increment count
                    iNumStyleCount = iNumStyleCount + 1
                Next i
            End If

            m_xarNumStyle = New ArrayList()
            m_xarNumStyle.Add("No Number")
            m_xarNumStyle.Add("1, 2, 3, ...")
            m_xarNumStyle.Add("I, II, III, ...")
            m_xarNumStyle.Add("i, ii, iii, ... ")
            m_xarNumStyle.Add("A, B, C, ...")
            m_xarNumStyle.Add("a, b, c, ... ")
            m_xarNumStyle.Add("1st, 2nd, ...")
            m_xarNumStyle.Add("One, Two, ...")
            m_xarNumStyle.Add("First, Second, ...")
            m_xarNumStyle.Add("01, 02, ...")
            'GLOG 5618 - modified conditional - was crashing in Word 2007
            'GLOG 5622 - modified again - was problematic on New Scheme
            If bAllow And bActiveDocIs2010Compatible Then
                m_xarNumStyle.Add("001, 002, ...")
                m_xarNumStyle.Add("0001, 0002, ...")
                m_xarNumStyle.Add("00001, 00002, ...")
                m_xarNumStyle.Add("Bullet")
                m_xarNumStyle.Add("New Bullet...")
                i = 15
            Else
                m_xarNumStyle.Add("Bullet")
                m_xarNumStyle.Add("New Bullet...")
                i = 12
            End If

            'add supplemental styles to Number Style list
            If g_xSupplementalNumberStyles(0) <> "" Then
                For j = 0 To UBound(g_xSupplementalNumberStyles)
                    xNumStyle = GetNumStyleDisplayName(CShort(g_xSupplementalNumberStyles(j)))
                    m_xarNumStyle.Add(xNumStyle)
                Next j
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumberStyle.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbNumberStyle.SetList(m_xarNumStyle)

            '   fill level list
            m_iLastSelLevel = 0

            If Not (g_oStatus Is Nothing) Then
                g_oStatus.Hide()
                'UPGRADE_NOTE: Object g_oStatus may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                g_oStatus = Nothing
            End If

            'get current zoom status
            Try
                iZoom = CShort(GetUserSetting("Numbering", "Zoom"))
            Catch
            End Try
            Me.cmdZoom.CheckState = iZoom

            'subsequent code was in VB6 Activate handler
            System.Windows.Forms.Application.DoEvents()

            'this will trigger loading of all properties
            Me.tbtnLevel1.PerformClick()

            '   initialize m_xarIsDirty Array
            ReDim m_xarIsDirty(9, 6)

            '   update flags
            m_bFormInit = False
            m_bLevelDirty = False
            m_bTCDirty = False
            m_bTOCDirty = False
            m_bPropDirty = False
            m_bParaDirty = False
            m_bNextParaDirty = False
            g_bSchemeIsDirty = False

            '   initialize focus marker
            Me.rtfNumberFormat.Select()
            m_xPrevCtl = "rtfNumberFormat"

            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            '   enable/disable add/delete level buttons
            Me.cmdDeleteLevel.Enabled = Me.tsLevel.Items(1).Visible
            Me.cmdAddLevel.Enabled = Not Me.tsLevel.Items(8).Visible

            RedrawLevelButtons()

            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()

            '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
            'screen capture and design mode execution
            If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()
        Catch e As Exception
            [Error].Show(e)
        Finally
            If Not g_oStatus Is Nothing Then
                g_oStatus.Hide()
                g_oStatus = Nothing
            End If
        End Try
    End Sub
    Private Sub frmEditScheme_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            '   save dlg position
            SetUserSetting("Numbering", "DlgTop", CStr(Me.Top))
            SetUserSetting("Numbering", "DlgLeft", CStr(Me.Left))

            '   delete the refresh buffer file -
            '   the file will only exist if the
            '   number of refreshes that have
            '   occurred exceeds the threshold
            Try
                Kill(g_xUserPath & "\Refresh.mpf")
            Catch
            End Try
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub lblStartAt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lblStartAt.Click
        m_xPrevCtl = "lblStartAt"
    End Sub

    Private Sub lstPrevLevels_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        m_xPrevCtl = "lstPrevLevels"
    End Sub

    Private Sub optDirectFormats_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optDirectFormats.Leave
        Try
            m_xPrevCtl = "optDirectFormats"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub optStyleFormats_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optStyleFormats.Leave
        Try
            m_xPrevCtl = "optStyleFormats"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub rtfNumberFormat_Enter(sender As Object, e As EventArgs) Handles rtfNumberFormat.Enter
        Try
            Me.rtfNumberFormat.SelectionStart = 0
            Me.rtfNumberFormat.SelectionLength = 0
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub rtfNumberFormat_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles rtfNumberFormat.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        'added in 9.9.4014
        Dim xChar As String
        Dim iSelLength As Short
        Dim xFormat As String
        Dim iLen As Short

        Try
            xChar = LCase(Chr(KeyAscii))
            'GLOG 8794 - pressing Alt+NumPad# to switch levels with this control selected was
            'inserting a question mark - disallow this character altogether
            If (KeyAscii = 8) Or (KeyAscii = 63) Then
                'backspace key pressed
                GoTo EventExitSub
            End If

            iSelLength = Me.rtfNumberFormat.SelectionLength
            xFormat = xGetNumFormat(m_xCode, Me.rtfNumberFormat.Text, (Me.chkNonbreakingSpaces).CheckState)
            iLen = Len(xFormat) - lCountChrs(xFormat, "%")

            If (iLen = 31) And (iSelLength = 0) Then
                xMsg = "You have reached the maximum number of characters that " & "Word allows for a number format."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            End If
EventExitSub:
            eventArgs.KeyChar = Chr(KeyAscii)
            If (KeyAscii = 0) Or (KeyAscii = 63) Then 'GLOG 8794
                eventArgs.Handled = True
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub spnAt_Change(Sender As Object, e As EventArgs) Handles spnAt.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnAt_Leave(sender As Object, e As EventArgs) Handles spnAt.Leave
        Try
            m_xPrevCtl = "spnAt"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTabPosition_Leave(sender As Object, e As EventArgs) Handles spnTabPosition.Leave
        Try
            m_xPrevCtl = "spnTabPosition"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTextPosition_Leave(sender As Object, e As EventArgs) Handles spnTextPosition.Leave
        Try
            m_xPrevCtl = "spnTextPosition"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnRightIndent_Leave(sender As Object, e As EventArgs) Handles spnRightIndent.Leave
        Try
            m_xPrevCtl = "spnRightIndent"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtStartAt_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtStartAt.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim iOffset As Short
        Dim bIsNavigationKey As Boolean

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            If (KeyCode = System.Windows.Forms.Keys.Up) Or (KeyCode = System.Windows.Forms.Keys.Down) Then
                ChangeUpDown((Me.udStartAt), KeyCode = System.Windows.Forms.Keys.Up)
                ChangeStartAt()
            Else
                bIsNavigationKey = (KeyCode = System.Windows.Forms.Keys.Left) Or (KeyCode = System.Windows.Forms.Keys.Right) Or (KeyCode = System.Windows.Forms.Keys.Home) Or (KeyCode = System.Windows.Forms.Keys.End) Or (KeyCode = System.Windows.Forms.Keys.Insert) Or (KeyCode = System.Windows.Forms.Keys.Up) Or (KeyCode = System.Windows.Forms.Keys.Down) Or (KeyCode = System.Windows.Forms.Keys.PageUp) Or (KeyCode = System.Windows.Forms.Keys.PageDown) Or (KeyCode = System.Windows.Forms.Keys.ControlKey) Or (KeyCode = System.Windows.Forms.Keys.Menu) Or (KeyCode = System.Windows.Forms.Keys.ShiftKey)

                If Not bIsNavigationKey Then
                    'GLOG 5284 (9.9.5004) - modified keys in message below
                    xMsg = "Please use the arrows to the right to change " & "the 'start at' position.  " & vbCr & "Alternatively, you may use the up/down " & "arrow keys. " & vbCr & vbCr & "Pressing CTRL+Up/Down Arrow will change " & "the value by 10.  " & vbCr & "Pressing CTRL+ALT+SHIFT+Up/Down Arrow key " & "will change the value by 100."
                    MsgBox(xMsg, MsgBoxStyle.Information, g_xAppName)
                End If
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub txtStartAt_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStartAt.Leave
        Try
            m_xPrevCtl = "txtStartAt"
            If Not ActiveControl Is Me.udStartAt Then
                RefreshLevel()
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    ''UPGRADE_NOTE: Split was upgraded to Split_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Private Sub lstPrevLevels_FetchCellStyle(ByVal Condition As Short, ByVal Split_Renamed As Short, ByRef Bookmark As Object, ByVal Col As Short, ByVal CellStyle As TrueDBList60.StyleDisp)
    '    'UPGRADE_WARNING: Couldn't resolve default property of object Me.lstPrevLevels.Array.Value. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '    If Me.lstPrevLevels.Array.Value(Bookmark, 1) = ChrW(&HB7) Then
    '        CellStyle.Font = VB6.FontChangeName(CellStyle.Font, "Symbol")
    '    End If
    '    lstPrevLevels.set()
    'End Sub

    'Private Sub lstPrevLevels_KeyDownEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxTrueDBList60.TrueDBListEvents_KeyDownEvent) Handles lstPrevLevels.KeyDownEvent
    '    If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
    '    If eventArgs.KeyCode = 13 Then
    '        InsertPrevLevel()
    '    End If
    'End Sub

    'Private Sub lstPrevLevels_MouseDownEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxTrueDBList60.TrueDBListEvents_MouseDownEvent) Handles lstPrevLevels.MouseDownEvent
    '    If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
    '    If Not (Me.ActiveControl Is Me.lstPrevLevels) Then
    '        Me.lstPrevLevels.Focus()
    '    End If
    'End Sub

    'UPGRADE_WARNING: Event optDirectFormats.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optDirectFormats_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optDirectFormats.CheckedChanged
        Try
            If eventSender.Checked Then
                If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
                m_bPropDirty = True
                m_bTCDirty = True
                m_bLevelDirty = True
                m_bParaDirty = True
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    'UPGRADE_WARNING: Event optStyleFormats.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optStyleFormats_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optStyleFormats.CheckedChanged
        Try
            If eventSender.Checked Then
                If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
                m_bPropDirty = True
                m_bTCDirty = True
                m_bLevelDirty = True
                m_bParaDirty = True
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub rtfNumberFormat_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles rtfNumberFormat.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Dim iSelStart As Short
        Dim iSelLen As Short
        Dim xSelCode As String
        Dim iLevel As Short
        Dim bNumIsInSel As Boolean
        Dim bCharIsNum As Boolean
        Dim xPreSel As String
        Dim xPostSel As String
        Dim iKey As Short
        Dim bIsAllowed As Boolean
        Dim bIsNavigationKey As Boolean
        Dim xNew As String
        Dim xChar As String
        Dim iPrevLevel As Short
        Dim i As Short

        Try
            '   get selection detail
            With Me.rtfNumberFormat
                iSelStart = .SelectionStart
                iSelLen = .SelectionLength
                xSelCode = Mid(m_xCode, iSelStart + 1, iSelLen)
            End With

            If Control.ModifierKeys = Keys.Control And (KeyCode = 86 Or KeyCode = 88) And iSelLen Then
                'user is pasting over or cutting a selection -
                'delete the corresponding chars in m_xCode
                xPreSel = VB.Left(m_xCode, iSelStart)
                xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
                m_xCode = xPreSel & xPostSel
            End If

            '   exit if ctl or alt is down
            If (Control.ModifierKeys = Keys.Control) Or (Control.ModifierKeys = Keys.Alt) Then
                Exit Sub
            End If

            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '   define valid characters -
            '   shift = 16
            '   GLOG 3505 (9.9.5001) - allowed smart quotes and apostrophes (145-148)
            iKey = CShort(UCase(CStr(KeyCode)))
            bIsAllowed = (iKey >= 32 And iKey <= 96) Or (iKey >= 123 And iKey <= 126) Or (iKey = System.Windows.Forms.Keys.Delete) Or (iKey = System.Windows.Forms.Keys.Back) Or (iKey = 190) Or (iKey >= 145 And iKey <= 148)

            bIsNavigationKey = (iKey = System.Windows.Forms.Keys.Left) Or (iKey = System.Windows.Forms.Keys.Right) Or (iKey = System.Windows.Forms.Keys.Home) Or (iKey = System.Windows.Forms.Keys.End) Or (iKey = System.Windows.Forms.Keys.Insert) Or (iKey = System.Windows.Forms.Keys.Up) Or (iKey = System.Windows.Forms.Keys.Down) Or (iKey = System.Windows.Forms.Keys.PageUp) Or (iKey = System.Windows.Forms.Keys.PageDown)

            '   allow non-input keys to go through unmodified
            If Not bIsAllowed Then
                Exit Sub
            ElseIf bIsNavigationKey Then
                Exit Sub
            End If

            iLevel = m_iLevel

            With Me.rtfNumberFormat
                '       different actions for selection/non-selection
                If iSelLen Then
                    '           there is a selection-
                    '           delete the corresponding chars
                    '           in m_xCode and insert new character
                    xPreSel = VB.Left(m_xCode, iSelStart)
                    xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
                    m_xCode = xPreSel & xPostSel
                Else
                    '           there is no selection - only
                    '           delete and backspace need to be addressed
                    If KeyCode = System.Windows.Forms.Keys.Delete Then
                        If iSelStart > Len(.Text) - 1 Then
                            Exit Sub
                        End If

                        '               get char to delete
                        xChar = Mid(m_xCode, iSelStart + 1, 1)

                        '               test if char is a previous level
                        If IsNumeric(xChar) Then
                            iPrevLevel = CShort(xChar)
                            i = 0
                            '                   modify selection to select all chars in number
                            While xChar = CStr(iPrevLevel)
                                i = i + 1
                                xChar = Mid(m_xCode, iSelStart + 1 + i, 1)
                            End While
                            .SelectionLength = i

                            xPostSel = Mid(m_xCode, iSelStart + .SelectionLength + 1)
                        Else
                            xPostSel = Mid(m_xCode, iSelStart + 2)
                        End If

                        '               delete the corresponding chars in m_xCode
                        xPreSel = VB.Left(m_xCode, iSelStart)
                        m_xCode = xPreSel & xPostSel
                    ElseIf KeyCode = System.Windows.Forms.Keys.Back Then
                        If iSelStart = 0 Then
                            Exit Sub
                        End If

                        '               get char to delete
                        xChar = Mid(m_xCode, iSelStart, 1)

                        '               test if char is a previous level
                        If IsNumeric(xChar) Then
                            iPrevLevel = CShort(xChar)
                            i = 0
                            '                   modify selection to select all chars in number
                            While (xChar = CStr(iPrevLevel))
                                i = i + 1
                                xChar = ""
                                Try
                                    xChar = Mid(m_xCode, iSelStart - i, 1)
                                Catch
                                End Try
                            End While
                            iSelStart = .SelectionStart - i
                            .SelectionStart = iSelStart
                            .SelectionLength = i
                            If iSelStart Then
                                xPreSel = VB.Left(m_xCode, iSelStart)
                            End If
                            xPostSel = Mid(m_xCode, iSelStart + .SelectionLength + 1)
                        Else
                            xPreSel = VB.Left(m_xCode, iSelStart - 1)
                            xPostSel = Mid(m_xCode, iSelStart + 1)
                        End If

                        '               delete the corresponding chars in m_xCode
                        m_xCode = xPreSel & xPostSel
                    End If
                    '           prevent rtNumberFormat_SelChange event from running
                    '           before m_xCode is updated by rtfNumberFormat_Change
                    m_bNoSelChangeEvent = True
                End If
            End With
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

#If False Then
	'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression False did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
	Private Sub rtfNumberFormat_KeyDown(KeyCode As Integer, Shift As Integer)
	Dim iSelStart As Integer
	Dim iSelLen As Integer
	Dim xSelCode As String
	Dim iLevel As Integer
	Dim bNumIsInSel As Boolean
	Dim bCharIsNum As Boolean
	Dim xPreSel As String
	Dim xPostSel As String
	Dim iKey As Integer
	Dim bIsAllowed As Boolean
	Dim bIsNavigationKey As Boolean
	Dim xNew As String
	Dim xChar As String
	Dim iPrevLevel As Integer
	Dim i As Integer

	'   exit if ctl or alt is down
	If Shift = vbCtrlMask Or Shift = vbAltMask Then
	Exit Sub
	End If

	If ActiveDocument <> g_docPreview Then _
	        g_docPreview.Activate

	'   define valid characters -
	'   shift = 16
	iKey = UCase(KeyCode)
	bIsAllowed = (iKey >= 32 And iKey <= 96) Or _
	                 (iKey >= 123 And iKey <= 126) Or _
	                 (iKey = vbKeyDelete) Or _
	                 (iKey = vbKeyBack) Or (iKey = 190)

	bIsNavigationKey = (iKey = vbKeyLeft) Or _
	                       (iKey = vbKeyRight) Or _
	                       (iKey = vbKeyHome) Or _
	                       (iKey = vbKeyEnd) Or _
	                       (iKey = vbKeyInsert) Or _
	                       (iKey = vbKeyUp) Or _
	                       (iKey = vbKeyDown) Or _
	                       (iKey = vbKeyPageUp) Or _
	                       (iKey = vbKeyPageDown)

	'   allow non-input keys to go through unmodified
	If Not bIsAllowed Then
	Exit Sub
	ElseIf bIsNavigationKey Then
	Exit Sub
	End If

	iLevel = Me.cmbLevel


	With Me.rtfNumberFormat
	'       get selection detail
	iSelStart = .SelStart
	iSelLen = .SelLength
	xSelCode = Mid(m_xCode, iSelStart + 1, iSelLen)

	'       different actions for selection/non-selection
	If iSelLen Then
	'           there is a selection - test for existence of
	'           current level number in selection
	bNumIsInSel = (InStr(xSelCode, iLevel) > 0)
	If bNumIsInSel Then
	'               prevent any key from going through and alert user
	KeyCode = 0
	xMsg = "Cannot delete current level number. Please modify " & _
	                    "your selection to exclude the Level " & iLevel & " number."
	MsgBox xMsg, vbExclamation, g_xAppName
	Else
	'               delete the corresponding chars
	'               in m_xCode and insert new character
	xPreSel = Left(m_xCode, iSelStart)
	xPostSel = Mid(m_xCode, iSelStart + iSelLen + 1)
	m_xCode = xPreSel & xPostSel
	End If
	Else
	'           there is no selection - only
	'           delete and backspace need to be addressed
	If KeyCode = vbKeyDelete Then
	If iSelStart > Len(.Text) - 1 Then
	Exit Sub
	End If

	'               get char to delete
	xChar = Mid(m_xCode, iSelStart + 1, 1)

	'               test if the char to be deleted is current level number
	bCharIsNum = (xChar = CStr(iLevel))

	If bCharIsNum Then
	'                   prevent key from going through and alert user
	KeyCode = 0
	xMsg = "Cannot delete the current level number."
	MsgBox xMsg, vbExclamation, g_xAppName
	Else
	'                   test if char is a previous level
	If IsNumeric(xChar) Then
	iPrevLevel = xChar
	i = 0
	'                       modify selection to select all chars in number
	While xChar = CStr(iPrevLevel)
	i = i + 1
	xChar = Mid(m_xCode, iSelStart + 1 + i, 1)
	Wend
	.SelLength = i

	xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
	Else
	xPostSel = Mid(m_xCode, iSelStart + 2)
	End If

	'                   delete the corresponding chars in m_xCode
	xPreSel = Left(m_xCode, iSelStart)
	m_xCode = xPreSel & xPostSel
	End If

	ElseIf KeyCode = vbKeyBack Then
	If iSelStart = 0 Then
	Exit Sub
	End If

	'               get char to delete
	xChar = Mid(m_xCode, iSelStart, 1)

	'               test if the char to be deleted is current level number
	bCharIsNum = (xChar = CStr(iLevel))

	If bCharIsNum Then
	'                   prevent key from going through and alert user
	KeyCode = 0
	xMsg = "Cannot delete the current level number."
	MsgBox xMsg, vbExclamation, g_xAppName
	Else
	'                   test if char is a previous level
	If IsNumeric(xChar) Then
	iPrevLevel = xChar
	i = 0
	'                       modify selection to select all chars in number
	While (xChar = CStr(iPrevLevel))
	i = i + 1
	On Error Resume Next
	xChar = ""
	xChar = Mid(m_xCode, iSelStart - i, 1)
	On Error GoTo 0
	Wend
	iSelStart = .SelStart - i
	.SelStart = iSelStart
	.SelLength = i
	If iSelStart Then
	xPreSel = Left(m_xCode, iSelStart)
	End If
	xPostSel = Mid(m_xCode, iSelStart + .SelLength + 1)
	Else
	xPreSel = Left(m_xCode, iSelStart - 1)
	xPostSel = Mid(m_xCode, iSelStart + 1)
	End If

	'                   delete the corresponding chars in m_xCode
	m_xCode = xPreSel & xPostSel
	End If
	End If
	'           prevent rtNumberFormat_SelChange event from running
	'           before m_xCode is updated by rtfNumberFormat_Change
	m_bNoSelChangeEvent = True
	End If
	End With
	End Sub
#End If

    Private Sub rtfNumberFormat_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles rtfNumberFormat.TextChanged
        'deal with number format backend string
        Dim iStart As Short
        Dim xCodeSelection As String
        Dim iLevel As Short
        Dim iAdded As Short
        Dim xAdded As String
        Dim i As Short
        Dim iPrevLevel As Short
        Dim xLevelCode As String
        Dim xPreCode As String
        Dim xPostCode As String
        Dim iColor As Short
        Dim bRawNumberFormat As Boolean
        Dim xLT As String

        Try
            If m_bFormInit Or m_bStopChangeEvent Then Exit Sub

            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            m_bLevelDirty = True
            iLevel = m_iLevel

            With Me.rtfNumberFormat
                If InStr(m_xCode, CStr(iLevel)) = 0 Then
                    '           current level is not in number -
                    '           set number style to "no number"
                    Me.cmbNumberStyle.SelectedIndex = 0
                End If

                '       get num of chars added
                System.Windows.Forms.Application.DoEvents()
                iAdded = Len(.Text) - Len(m_xCode)

                '       get start pos from current selection
                '       pos minus the number of chars entered
                iStart = mpMax(.SelectionStart - iAdded, 0)

                '       if chars were added...
                If iAdded > 0 Then
                    If m_bIsPrevLevels Then
                        '               the added chars were previous levels
                        With Me.lstPrevLevels.SelectedIndices
                            '                   cycle through all added levels
                            For i = 0 To .Count - 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object Me.lstPrevLevels.SelBookmarks.Item(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                iPrevLevel = .Item(i) + 1 'GLOG 8571 (dm)
                                '                       converts selected level number to
                                '                       appropriate code - each char in the
                                '                       number is represented by the number's
                                '                       level - eg the level 2 number 'One'
                                '                       is represented as '222'
                                xLT = xGetFullLTName((g_oCurScheme.Name))
                                bRawNumberFormat = (iMapNumberStyle_WordToMP((CurWordApp.ActiveDocument.ListTemplates.Item(xLT).ListLevels.Item(iPrevLevel).NumberStyle)) > 16)
                                If bRawNumberFormat Then
                                    'always two characters long - % + level
                                    xLevelCode = New String(CStr(iPrevLevel), 2)
                                Else
                                    xLevelCode = New String(CStr(iPrevLevel), Len(xGetListNumber((g_oCurScheme.Name), iPrevLevel)))
                                End If

                                '                       build code string
                                xAdded = xAdded & xLevelCode
                            Next
                        End With
                        iColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
                    Else
                        '               the added chars were straight text -
                        '               a straight text char is represented by '$'
                        xAdded = New String(mpTextCode, iAdded)
                        iColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black)
                    End If

                    '           modify number code representation as appropriate
                    If iStart Then
                        xPreCode = VB.Left(m_xCode, iStart)
                        xPostCode = Mid(m_xCode, iStart + 1)
                        m_xCode = xPreCode & xAdded & xPostCode
                    Else
                        m_xCode = xAdded & m_xCode
                    End If

                    m_bStopChangeEvent = True

                    '           ensure that the added text displays the
                    '           correct color for the type of text that
                    '           was added - prev levels are red,
                    '           straight text is black
                    .SelectionStart = iStart
                    .SelectionLength = iAdded
                    .SelectionColor = System.Drawing.ColorTranslator.FromOle(iColor)


                    '           set selection point to the pos
                    '           after the inserted chars
                    m_bNoSelChangeEvent = True

                    'GLOG 15790 (dm) - Focus() changes .SelectionStart to 0
                    iStart = iStart + iAdded
                    .SelectionStart = iStart

                    m_bNoSelChangeEvent = True
                    .SelectionLength = 0
                    Try
                        .Focus()
                    Catch
                    End Try

                    .SelectionStart = iStart 'GLOG 15790 (dm)

                    m_bStopChangeEvent = False
                    m_bNoSelChangeEvent = False
                End If
            End With
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub rtfNumberFormat_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles rtfNumberFormat.Leave
        Try
            m_xPrevCtl = "rtfNumberFormat"
            RefreshLevel()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub rtfNumberFormat_SelectionChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles rtfNumberFormat.SelectionChanged
        Try
            If (m_bFormInit Or m_bStopChangeEvent Or m_bNoSelChangeEvent) Then
                m_bNoSelChangeEvent = False
                Exit Sub
            End If

            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '   m_bSelChangeRunning prevents the SelChange
            '   event from firing a second time
            If Not m_bSelChangeRunning Then
                m_bSelChangeRunning = True
                bModifySelection()
                m_bSelChangeRunning = False
            End If

            '   set tooltip text for current selection
            SetNumFormatTooltip()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Function SetNumFormatTooltip() As Object
        '   set tooltip text to descriptor of selection
        Dim xText As String
        Dim xCode As String
        Dim xChr1 As String
        Dim iLevel As Short

        With Me.rtfNumberFormat
            Select Case .SelectionLength
                Case -1
                    xText = ""
                Case 0, 1
                    '               get code behind selection
                    xCode = Mid(m_xCode, .SelectionStart + 1, 1)
                    Select Case xCode
                        Case mpTextCode
                            xText = "Text"
                        Case Else
                            If Len(xCode) Then
                                xText = "Level " & xCode
                                iLevel = CShort(xCode)
                            End If
                    End Select
                Case Else
                    '               get code behind selection
                    xCode = Mid(m_xCode, .SelectionStart + 1, .SelectionLength)

                    '               get code of first chr in selection
                    xChr1 = Mid(m_xCode, .SelectionStart + 1, 1)


                    '               check if code string is all same chr
                    If Len(xChr1) = 0 Or (xChr1 = "$") Then
                        xText = ""
                    ElseIf New String(xChr1, .SelectionLength) = xCode Then
                        xText = "Level " & xChr1
                        iLevel = CShort(xChr1)
                    Else
                        xText = ""
                    End If
            End Select

            '       set tooltip
            Dim oTooltip As New ToolTip()
            oTooltip.SetToolTip(Me.rtfNumberFormat, xText)

            ''       select row in previous levels
            ''       if selection is prev level
            '        If iLevel > 0 Then
            '            If iLevel < CInt(Me.cmbLevel) Then
            ''               it's a prev level - clear
            ''               existing prev level selections
            '                ClearPrevLevelSelections
            ''               select prev level
            '                With Me.lstPrevLevels
            '                    .Row = iLevel - 1
            '                    .SelBookmarks.Add iLevel - 1
            '                End With
            '            End If
            '        End If
        End With
    End Function

    Private Function bModifySelection(Optional ByRef bGoForward As Boolean = False, Optional ByRef bGoBack As Boolean = False) As Boolean
        'modifies current selection to include the selection
        'of entire numbers - prevents numbers from being
        'split apart
        Dim xLeftChar As String
        Dim xRightChar As String
        Dim xNextChar As String
        Dim xPrevChar As String
        Dim bIsSameLevel As Boolean
        Dim iStart As Short
        Dim iEnd As Short
        Dim bInNumber As Boolean

        With Me.rtfNumberFormat
            If .SelectionStart = 0 Then
                Exit Function
            End If

            '       test for selection
            If Len(.SelectedText) = 0 Then
                xLeftChar = Mid(m_xCode, .SelectionStart, 1)
                xRightChar = Mid(m_xCode, .SelectionStart + 1, 1)
                bInNumber = IsNumeric(xLeftChar) And IsNumeric(xRightChar) And (xLeftChar = xRightChar)
                If bInNumber Then
                    iStart = .SelectionStart
                    iEnd = .SelectionStart
                Else
                    Exit Function
                End If
            Else
                '           get start and end points
                iStart = .SelectionStart
                iEnd = .SelectionStart + .SelectionLength
            End If

            '       check left border of selection for a
            '       prev level or current level number
            xLeftChar = Mid(m_xCode, iStart + 1, 1)

            '       check right border of selection for a
            '       prev level or current level number
            xRightChar = Mid(m_xCode, iEnd, 1)

            '       if left side is a level, extend
            '       the selection to the left
            '       for as far as that number goes
            If IsNumeric(xLeftChar) And iStart Then
                xPrevChar = Mid(m_xCode, iStart, 1)

                While (xPrevChar = xLeftChar) And iStart
                    iStart = iStart - 1
                    On Error Resume Next
                    xPrevChar = Mid(m_xCode, iStart, 1)
                    On Error GoTo 0
                End While
            End If

            '       if right side is a level, extend
            '       the selection to the right
            '       for as far as that number goes
            If IsNumeric(xRightChar) And iEnd <> (.SelectionStart + .SelectionLength + 1) Then
                xNextChar = Mid(m_xCode, iEnd + 1, 1)

                While (xNextChar = xRightChar) And iEnd <> Len(.Text)
                    iEnd = iEnd + 1
                    xNextChar = Mid(m_xCode, iEnd + 1, 1)
                End While
            End If

            '       make new selection
            .SelectionStart = iStart
            .SelectionLength = iEnd - .SelectionStart
        End With
        m_bSelChangeRunning = True
    End Function

    Function lLoadLevelProperties() As Integer
        Dim xStyle As String
        Dim xTCStyle As String
        Dim iLevel As Short
        Dim iNumStyle As Short
        Dim xNumFmt As String
        Dim xNumFmtTemp As String
        Dim xNumber As String
        Dim iLoc As Short
        Dim iSelLevel As Short
        Dim iSelStart As Short
        Dim xNextPara As String
        Dim bEnabled As Boolean
        Dim bLegal As Boolean
        Dim fntTCHeading As Word.Font
        Dim xLT As String
        Dim iLineSpaceRule As Short
        Dim pfNormal As Word.ParagraphFormat
        Dim sTabPosition As Single
        Dim sMod As Single
        Dim sDef As Single
        Dim iAlignment As Short
        Dim iHeadingFormat As Short
        Dim xRoot As String
        Dim bRawNumberFormat As Boolean
        Dim oLT As Word.ListTemplate

        On Error GoTo ProcError

        m_bLevelChanged = True
        m_bAllowRefresh = False

        iLevel = m_iLevel
        xStyle = xGetStyleName(g_oCurScheme.Name, iLevel)
        xLT = xGetFullLTName((g_oCurScheme.Name))
        oLT = CurWordApp.ActiveDocument.ListTemplates(xLT)
        xRoot = xGetStyleRoot(g_oCurScheme.Name)

        With oLT.ListLevels.Item(iLevel)
            'fill number format rtf text box
            m_xCode = ""
            xNumFmt = Trim(.NumberFormat)
            'determine whether we can display the actual characters of
            'the dynamic portion of the number - if the style is not supported,
            'we'll leave the level tokens unevaluated
            iNumStyle = iMapNumberStyle_WordToMP(.NumberStyle)
            m_bRawNumberFormat = (iNumStyle > 16)
            If .NumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet Then
                '9.9.6001 - support all bullets
                m_xBulletFont = .Font.Name
                m_xBulletCharNum = .NumberFormat

                With Me.rtfNumberFormat
                    .Enabled = False
                    .Font = New Drawing.Font("Symbol", .Font.Size)
                    .Text = ChrW(&HB7)
                    .SelectionStart = 0
                    .SelectionLength = 1
                    .SelectionColor = System.Drawing.Color.Black
                    .SelectionLength = 0
                    m_xCode = ChrW(&HB7)
                End With
            Else 'anything other than bullet
                '9.9.6001 - restore bullet defaults
                m_xBulletFont = "Symbol"
                m_xBulletCharNum = ChrW(&HB7)

                With Me.rtfNumberFormat
                    .Enabled = True
                    .Font = New Drawing.Font("Microsoft Sans Serif", .Font.Size)
                    .Text = xNumFmt
                    .SelectionStart = 0
                    .SelectionLength = Len(.Text)
                    .SelectionColor = System.Drawing.Color.Black
                    iLoc = InStr(.Text, "%")
                    While iLoc
                        .SelectionStart = iLoc - 1
                        iSelStart = .SelectionStart
                        .SelectionLength = 2
                        iSelLevel = CShort(Mid(.Text, iLoc + 1, 1))
                        If iSelLevel = iLevel Then
                            bRawNumberFormat = m_bRawNumberFormat
                        Else
                            bRawNumberFormat = (iMapNumberStyle_WordToMP((oLT.ListLevels.Item(iSelLevel).NumberStyle)) > 16)
                        End If
                        If bRawNumberFormat Then
                            xNumber = "%" & iSelLevel
                        Else
                            xNumber = LTrim(xGetListNumber((g_oCurScheme.Name), iSelLevel))
                        End If
                        .SelectedText = xNumber
                        .SelectionStart = iSelStart
                        .SelectionLength = Len(xNumber)
                        If iSelLevel = iLevel Then
                            .SelectionColor = System.Drawing.Color.Blue
                        Else
                            .SelectionColor = System.Drawing.Color.Red
                        End If
                        m_xCode = m_xCode & New String(mpTextCode, iLoc - 1 - Len(m_xCode)) & New String(LTrim(Str(iSelLevel)), Len(xNumber))
                        iLoc = InStr(iLoc + 1, .Text, "%")
                    End While
                    m_xCode = m_xCode & New String(mpTextCode, Len(.Text) - Len(m_xCode))
                    xUserAll = .Rtf
                    .SelectionStart = 0
                End With
            End If

            '       get font properties
            With .Font
                On Error Resume Next
                Me.chkNumberBold.CheckState = ConvToTripleState(.Bold)
                Me.chkNumberCaps.CheckState = ConvToTripleState(.AllCaps)
                Me.chkNumberItalic.CheckState = ConvToTripleState(.Italic)

                Me.cmbUnderlineStyle.Value = CStr(.Underline)
                If Me.cmbUnderlineStyle.Value = "" Then
                    Me.cmbUnderlineStyle.SelectedIndex = 0
                End If

                If .Name = "" Then
                    Me.cmbNumFontName.SelectedIndex = 0
                Else
                    Me.cmbNumFontName.Value = .Name
                    If Me.cmbNumFontName.Value = "" Then
                        Me.cmbNumFontName.SelectedIndex = 0
                    End If
                End If
                If g_sNumberFontSizes(iLevel - 1) = Microsoft.Office.Interop.Word.WdConstants.wdUndefined Then
                    Me.cmbNumFontSize.SelectedIndex = 0
                Else
                    Me.cmbNumFontSize.Value = Trim(Str(g_sNumberFontSizes(iLevel - 1)))
                End If
                On Error GoTo ProcError
            End With

            '       get number style
            Select Case iNumStyle
                Case mpListNumberStyleArabic, mpListNumberStyleArabicLZ
                    Me.chkLegalStyle.CheckState = System.Windows.Forms.CheckState.Unchecked
                Case mpListNumberStyleLegal
                    iNumStyle = mpListNumberStyleArabic
                    Me.chkLegalStyle.CheckState = System.Windows.Forms.CheckState.Checked
                    FormatLegalNumber()
                Case mpListNumberStyleLegalLZ
                    iNumStyle = mpListNumberStyleArabicLZ
                    Me.chkLegalStyle.CheckState = System.Windows.Forms.CheckState.Checked
                    FormatLegalNumber()
                Case Else
                    Me.chkLegalStyle.CheckState = System.Windows.Forms.CheckState.Unchecked
                    If Me.chkLegalStyle.Enabled Then FormatLegalNumber()
            End Select

            '        Dim iListIndex As Integer
            '        On Error Resume Next
            '        iListIndex = Me.cmbNumberStyle.SelectedIndex
            '        On Error GoTo ProcError
            '        If iListIndex > 9 Then _
            ''            iListIndex = iListIndex + m_iFixedDigitNumberingOffset
            '        If iListIndex <> iNumStyle Then
            If m_bRawNumberFormat Then
                'the two built-in legal styles are not on the list
                Me.cmbNumberStyle.SelectedIndex = iNumStyle - 2 - m_iFixedDigitNumberingOffset
            ElseIf iNumStyle > 9 Then
                Me.cmbNumberStyle.SelectedIndex = iNumStyle - m_iFixedDigitNumberingOffset
            Else
                Me.cmbNumberStyle.SelectedIndex = iNumStyle
            End If
            '        Else
            ''           this will ensure dependent items, e.g. start at
            '            cmbNumberStyle_Click
            '        End If

            '       get other number properties
            Me.chkReset.CheckState = System.Math.Abs(CShort(.ResetOnHigher > 0))
            Me.chkRightAlign.CheckState = System.Math.Abs(CInt(.Alignment = Microsoft.Office.Interop.Word.WdListLevelAlignment.wdListLevelAlignRight))
            Me.chkNonbreakingSpaces.CheckState = System.Math.Abs(CInt(CBool(InStr(.NumberFormat, ChrW(&HA0)))))

            '        Me.txtNumberPosition = Format(.NumberPosition / 72, "#,##0.00")
            '        If .TabPosition = 9999999 Then
            '            Me.txtTabPosition = "0.00"
            '        Else
            '            Me.txtTabPosition = _
            ''                Format((.TabPosition - .NumberPosition) / 72, "#,##0.00")
            '        End If
            '        Me.txtTextPosition = Format(.TextPosition / 72, "#,##0.00")
            sTabPosition = .TabPosition
        End With

        '   get trailing character from level properties
        Me.cmbTrailingChar.Value = CStr(CInt(xGetLevelProp((g_oCurScheme.Name), iLevel, mpNumLevelProps.mpNumLevelProp_TrailChr, mpSchemeTypes.mpSchemeType_Document)))

        bEnabled = (Me.cmbTrailingChar.Text = "Tab")
        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Me.spnTabPosition.Enabled = bEnabled
        Me.lblTabPosition.Enabled = bEnabled

        '   get tc format from level properties-
        '   format info is held as bits
        '   in an integer - get integer
        iHeadingFormat = CShort(xGetLevelProp((g_oCurScheme.Name), iLevel, mpNumLevelProps.mpNumLevelProp_HeadingFormat, mpSchemeTypes.mpSchemeType_Document))

        ' test bits for format value
        Me.chkTCBold.CheckState = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Bold)
        Me.chkTCItalic.CheckState = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Italic)
        Me.chkTCCaps.CheckState = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_AllCaps)
        Me.chkTCUnderline.CheckState = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_Underline)
        Me.chkTCSmallCaps.CheckState = iHasTCFormat(iHeadingFormat, mpTCFormatFields.mpTCFormatField_SmallCaps)

        '   get type
        If (iHeadingFormat And mpTCFormatFields.mpTCFormatField_Type) Then
            Me.optStyleFormats.Checked = True
        Else
            Me.optDirectFormats.Checked = True
        End If

        '   get whether to honor right aligned styles
        iAlignment = CShort(xGetLevelProp((g_oCurScheme.Name), iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document))

        With CurWordApp.ActiveDocument.Styles.Item(xStyle)
            '       get paragraph format
            With .ParagraphFormat
                '           alignment
                If ((.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight) And (iAlignment < 2)) Or bBitwisePropIsTrue(iAlignment, mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal) Then
                    '               base on normal style; after editing in 9.8,
                    '               iAlignment will always be greater than 2
                    Me.cmbTextAlign.Value = CStr(mpBaseAlignmentOnNormal)
                Else
                    Me.cmbTextAlign.Value = CStr(.Alignment)
                End If

                '           line spacing
                Me.cmbLineSpacing.Value = m_xarLineSpacing(.LineSpacingRule, 0)
                '            Me.cmbLineSpacing.Text = m_xarLineSpacing(.LineSpacingRule, 0)
                If .LineSpacingRule = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnAt.Value = .LineSpacing / 12
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnAt.Value = .LineSpacing
                End If

                If m_bFormInit Then
                    '               doevents is only safe here (and only necessary) on form load;
                    '               after that, it will wreak havoc if Alt+1, Alt+2, etc. is used
                    '               to switch levels
                    System.Windows.Forms.Application.DoEvents()
                End If

                If .LineSpacingRule < 3 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnAt.DisplayText = ""
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnAt.Refresh()
                End If

                '           space after is really space between
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnSpaceAfter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnSpaceAfter.Value = .SpaceAfter
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnSpaceBefore. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnSpaceBefore.Value = .SpaceBefore
                Me.chkKeepLinesTogether.CheckState = System.Math.Abs(.KeepTogether)
                Me.chkKeepWithNext.CheckState = System.Math.Abs(.KeepWithNext)
                Me.chkWidowOrphan.CheckState = System.Math.Abs(.WidowControl)
                Me.chkPageBreak.CheckState = System.Math.Abs(.PageBreakBefore)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnRightIndent.Value = .RightIndent

                '           style indents now take priority over LT positions
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnTextPosition.Value = .LeftIndent
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnNumberPosition.Value = (.LeftIndent + .FirstLineIndent)
                If sTabPosition = 9999999 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnTabPosition.Value = 0
                ElseIf sTabPosition > (.LeftIndent + .FirstLineIndent) Then
                    '               this is the normal situation
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnTabPosition.Value = (sTabPosition - .LeftIndent - .FirstLineIndent)
                Else
                    '               if tab is to left of number, Word will use default tab stop
                    sDef = CurWordApp.ActiveDocument.DefaultTabStop
                    'UPGRADE_WARNING: Mod has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                    sMod = System.Math.Abs((.LeftIndent + .FirstLineIndent + sDef) Mod sDef)
                    If sMod = 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Me.spnTabPosition.Value = sDef
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Me.spnTabPosition.Value = sMod
                    End If
                End If
            End With

            '       get next paragraph style
            Dim oStyle As Word.Style
            oStyle = CType(.NextParagraphStyle, Word.Style)

            'GLOG 15891 (dm) - we're no longer displaying heading styles as proprietary styles, but we are
            'now displaying proprietary styles as heading styles for private schemes when the firm uses Word Heading styles
            If (g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Private) And g_bUseWordHeadingStyles And
                    (InStr(oStyle.NameLocal, xRoot & "_L") > 0) Then
                xNextPara = xTranslateHeadingStyle(CShort(VB.Right(oStyle.NameLocal, 1)))
                'If bIsHeadingStyle(oStyle.NameLocal) Then
                '    xNextPara = xRoot & "_L" & VB.Right(oStyle.NameLocal, 1)
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object ActiveDocument.Styles().NextParagraphStyle. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xNextPara = oStyle.NameLocal
            End If

            '       GLOG : 5445 : ceh
            '       On Error GoTo AddStyleToNextParaList
            Me.cmbNextParagraph.Value = xNextPara
            If Me.cmbNextParagraph.Value = "" Then
                m_xarNextPara.Add(xNextPara)
                Me.cmbNextParagraph.ClearList()
                Me.cmbNextParagraph.SetList(m_xarNextPara)
                Me.cmbNextParagraph.Value = xNextPara 'GLOG 15897 (dm)
            End If
            '       On Error GoTo ProcError

            '       get font
            On Error Resume Next
            With .Font
                Me.cmbFontName.Value = .Name
                If Me.cmbFontName.Value = "" Then
                    Me.cmbFontName.Value = "Times New Roman"
                End If
                Me.cmbFontSize.Value = Trim(Str(.Size))
            End With
            On Error GoTo ProcError
        End With

        '   enable/disable trailing underline
        '    SetTrailUnderlineState

        '   cont style
        lLoadContLevel()

        '   enable/disable restart checkbox
        Me.chkReset.Enabled = (iLevel <> 1)

        'GLOG 5664 - moved DoEvents above flag resets because it can trigger control
        'event handlers that might run unnecessary code - this was specifically
        'a problem with the Bylaws scheme after implementing the TrueDB combos
        System.Windows.Forms.Application.DoEvents()

        ClearSchemeDirtFlags()
        m_bLevelChanged = False
        m_bStopChangeEvent = False
        m_bAllowRefresh = True

        Exit Function

AddStyleToNextParaList:
        '    GLOG : 5445 : ceh
        '    Me.cmbNextParagraph.AddItem xNextPara
        '    Resume

ProcError:
        If Err.Number = mpnErrors.mpError_InvalidSchemePropValue Then
            If Not m_bAlertDisplayed Then
                xMsg = "This scheme has been edited with Microsoft Word. " & "You may continue" & vbCr & "to edit this scheme using TSG, " & "but note that some fields in this dialog " & vbCr & "may be empty.  Empty fields will not affect your " & "previous edits to this style.  In addition, " & vbCr & "line spacing in the scheme preview may not display correctly."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                m_bAlertDisplayed = True
            End If
            Resume Next
        Else
            Throw Err.GetException()
            Exit Function
        End If
    End Function

    Private Function lLoadContLevel() As Integer
        Dim iLevel As Short
        Dim xRoot As String
        Dim xCont As String
        Dim iAlignment As Short
        Dim styCont As Word.Style

        iLevel = m_iLevel
        xRoot = xGetStyleRoot(g_oCurScheme.Name)
        xCont = xRoot & " Cont " & iLevel

        iAlignment = CShort(xGetLevelProp((g_oCurScheme.Name), iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document))

        On Error Resume Next
        styCont = CurWordApp.ActiveDocument.Styles(xCont)
        On Error GoTo 0
        If styCont Is Nothing Then
            '9.9.6001 - copy cont style from source
            CreateContStyles(g_oCurScheme.Name, iLevel, , (g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Document))
            styCont = CurWordApp.ActiveDocument.Styles(xCont)
        End If

        With styCont
            '       get font
            On Error Resume Next
            With .Font
                Me.cmbContFontName.Value = .Name
                If Me.cmbContFontName.Value = "" Then
                    Me.cmbContFontName.Value = "Times New Roman"
                End If
                Me.cmbContFontSize.Value = Trim(Str(.Size))
            End With
            On Error GoTo 0

            '       paragraph format
            With .ParagraphFormat
                '           alignment
                If bBitwisePropIsTrue(iAlignment, mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal) Then
                    '               base on normal style
                    Me.cmbAlignment.Value = CStr(mpBaseAlignmentOnNormal)
                Else
                    Me.cmbAlignment.Value = CStr(.Alignment)
                End If

                '           indents
                If .FirstLineIndent > 0 Then
                    Me.cmbSpecial.Value = "First line"
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContLeftIndent.Value = .LeftIndent
                ElseIf .FirstLineIndent < 0 Then
                    Me.cmbSpecial.Value = "Hanging"
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContLeftIndent.Value = .LeftIndent + .FirstLineIndent
                Else
                    Me.cmbSpecial.Value = "(none)"
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContLeftIndent.Value = .LeftIndent
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnBy.Value = System.Math.Abs(.FirstLineIndent)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnContRightIndent.Value = .RightIndent

                '           line spacing
                Me.cmbContLineSpacing.Value = m_xarLineSpacing(.LineSpacingRule, 0)
                If .LineSpacingRule = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContAt.Value = .LineSpacing / 12
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContAt.Value = .LineSpacing
                End If

                If m_bFormInit Then
                    '               doevents is only safe here (and only necessary) on form load;
                    '               after that, it will wreak havoc if Alt+1, Alt+2, etc. is used
                    '               to switch levels
                    System.Windows.Forms.Application.DoEvents()
                End If

                If .LineSpacingRule < 3 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContAt.DisplayText = ""
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnContAt.Refresh()
                End If

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContSpaceAfter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnContSpaceAfter.Value = .SpaceAfter
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContSpaceBefore. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnContSpaceBefore.Value = .SpaceBefore

                '           other
                Me.chkContWidowOrphan.CheckState = System.Math.Abs(.WidowControl)
                Me.chkContKeepWithNext.CheckState = System.Math.Abs(.KeepWithNext)
                Me.chkContKeepLinesTogether.CheckState = System.Math.Abs(.KeepTogether)
            End With
        End With
    End Function

    Private Sub udStartAt_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles udStartAt.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            ChangeStartAt()
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub ChangeStartAt()
        Dim iNumberStyle As Short
        Dim iLevel As Short
        Dim iStartAt As Short
        Dim iPos As Short
        Dim iOldNumLength As Short
        Dim xNewNumber As String
        Dim xStartAt As String
        Dim iListIndex As Short

        m_bLevelDirty = True

        '   convert appropriate format
        If Not m_bRawNumberFormat Then
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumberStyle.SelectedIndex. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iListIndex = Me.cmbNumberStyle.SelectedIndex
            If iListIndex > 9 Then iListIndex = iListIndex + m_iFixedDigitNumberingOffset
            iNumberStyle = iMapNumberStyle_MPToWord(iListIndex)
        End If
        iStartAt = mpMax(mpMin((Me.udStartAt.Value), 1000), 1)

        '   update start at textbox
        If m_bRawNumberFormat Or (iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinal) Or (iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinalText) Or (iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleCardinalText) Then
            Me.txtStartAt.Text = LTrim(Str(iStartAt)) & Space(1)
        Else
            xStartAt = xIntToListNumStyle(iStartAt, iNumberStyle)
            Me.txtStartAt.Text = xStartAt & Space(1)
        End If

        '   update number format
        Dim bCurLevelExists As Boolean
        Dim iPosCurLevel As Short
        If Not m_bLevelChanged Then
            m_bStopChangeEvent = True

            If iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet Then
                With Me.rtfNumberFormat
                    .Text = ChrW(&HB7)
                    .Font = New Drawing.Font("Symbol", .Font.Size)
                End With
                m_xCode = ChrW(&HB7)
            Else
                iLevel = m_iLevel
                If m_bRawNumberFormat Then
                    xNewNumber = "%" & iLevel
                Else
                    xNewNumber = xIntToListNumStyle(iStartAt, iNumberStyle)
                End If
                If m_xCode <> ChrW(&HB7) Then
                    '           get position of current level number
                    iPosCurLevel = InStr(m_xCode, LTrim(CStr(iLevel)))

                    If iPosCurLevel Then
                        '               there is a current level # - store pos
                        iPos = iPosCurLevel
                    Else
                        '               there is no current level # -
                        '               get end position of code
                        iPos = Len(m_xCode) + 1
                    End If
                Else
                    iPos = 1
                End If

                iOldNumLength = lCountChrs(m_xCode, LTrim(CStr(iLevel)))

                '       update code behind rtf number
                m_xCode = VB.Left(m_xCode, iPos - 1) & New String(LTrim(CStr(iLevel)), Len(xNewNumber)) & VB.Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)

                '       insert text in number format box
                With Me.rtfNumberFormat
                    .SelectionStart = mpMax(iPos - 1, 0)
                    .SelectionLength = iOldNumLength
                    If iOldNumLength = 0 Then
                        .SelectionColor = System.Drawing.Color.Blue
                    End If
                    .SelectedText = xNewNumber
                    xUserAll = .RTF
                End With
            End If

            m_bStopChangeEvent = False
        End If
    End Sub

    Private Sub ClearPrevLevelSelectionsButLast()
        Dim i As Short

        '   remove listing selections
        With Me.lstPrevLevels.SelectedIndices
            For i = .Count - 2 To 0 Step -1
                .Remove(i)
            Next
            'System.Windows.Forms.Application.DoEvents()
        End With
    End Sub

    Private Sub ClearPrevLevelSelections()
        Dim i As Short

        '   remove listing selections
        With Me.lstPrevLevels.SelectedIndices
            For i = .Count - 1 To 0 Step -1
                .Remove(i)
            Next
            'System.Windows.Forms.Application.DoEvents()
        End With
    End Sub

    '
    'Private Sub lstPrevLevels_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    '    Dim dtoDataObj As MSForms.DataObject
    '    Dim xPrev As String
    '    Dim i As Integer
    '    Dim iPrevLevel As Integer
    '    Dim xLT As String
    '
    ''   this will be true when inserting a
    ''   previous level - you don't want
    ''   to run anything during that time
    '    If m_bInsertingPrevLevel Then
    '        Exit Sub
    '    End If
    '
    '    If ActiveDocument <> g_docPreview Then _
    ''        g_docPreview.Activate
    '
    '    xLT = xGetFullLTName(g_oCurScheme.Name)
    '
    ''   do only if user is initiating a drag
    '    If Button = vbLeftButton Then
    '        With Me.lstPrevLevels
    ''           make appropriate selections
    '            SelPrevLevels Y
    '
    ''           start drag
    '            .Drag vbBeginDrag
    '
    '            For i = 0 To .SelBookmarks.Count - 1
    '                If .SelBookmarks(i) > -1 Then
    '                    iPrevLevel = .SelBookmarks(i) + 1
    ''                   alert and exit if user is attempting
    ''                   to insert a previous level twice.
    '                    If InStr(m_xCode, iPrevLevel) Then
    '                        .Drag vbEndDrag
    '                        Exit Sub
    '                    End If
    '
    '                    If Me.chkLegalStyle Then
    '                        xPrev = xPrev & _
    ''                            ActiveDocument.ListTemplates(xLT) _
    ''                            .ListLevels(iPrevLevel).StartAt
    '                    Else
    '                        xPrev = xPrev & xGetListNumber(g_oCurScheme.Name, _
    ''                                            iPrevLevel)
    '
    '    '                   disallow bullet
    '                        If Right(xPrev, 1) = ChrW(&HB7) Then _
    ''                            xPrev = Left(xPrev, Len(xPrev) - 1)
    '                    End If
    '                End If
    '            Next i
    '
    '            If xPrev <> "" Then
    '                m_bIsPrevLevels = True
    '                Set dtoDataObj = New MSForms.DataObject
    '                dtoDataObj.SetText xPrev
    '                dtoDataObj.StartDrag
    '            Else
    '                .Drag vbEndDrag
    '            End If
    '        End With
    '        m_bIsPrevLevels = False
    '    End If
    '
    'End Sub

    Private Sub spnSpaceAfter_Change(Sender As Object, e As EventArgs) Handles spnSpaceAfter.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnSpaceAfter_Leave(sender As Object, e As EventArgs) Handles spnSpaceAfter.Leave
        Try
            m_xPrevCtl = "spnSpaceAfter"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnSpaceBefore_Change(Sender As Object, e As EventArgs) Handles spnSpaceBefore.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnSpaceBefore_Leave(sender As Object, e As EventArgs) Handles spnSpaceBefore.Leave
        Try
            m_xPrevCtl = "spnSpaceBefore"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    'UPGRADE_WARNING: Event txtStartAt.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtStartAt_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStartAt.TextChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub spnTabPosition_Change(Sender As Object, e As EventArgs) Handles spnTabPosition.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTextPosition_Change(Sender As Object, e As EventArgs) Handles spnTextPosition.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bLevelDirty = True
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Function bSetDirt(ByRef iLevel As Short, ByRef bSaveLevel As Boolean, ByRef bSaveTC As Boolean, ByRef bSaveTOC As Boolean, ByRef bSaveProp As Boolean, ByRef bSavePara As Boolean, ByRef bSaveNextPara As Boolean) As Boolean
        'appropriate element is switched on once any change
        'to that level component has been implemented in buffer;
        'elements will never be set to "False"

        If bSaveLevel Then xarDirty(iLevel, mpLevelIsDirty) = "True"
        If bSaveTC Then xarDirty(iLevel, mpTCIsDirty) = "True"
        If bSaveTOC Then xarDirty(iLevel, mpTOCIsDirty) = "True"
        If bSaveProp Then xarDirty(iLevel, mpDocPropIsDirty) = "True"
        If bSavePara Then xarDirty(iLevel, mpParaIsDirty) = "True"
        If bSaveNextPara Then xarDirty(iLevel, mpNextParaIsDirty) = "True"

    End Function

    Private Sub FillPrevLevelsList(ByRef xScheme As String)
        'fill previous level list

        Dim iPrevLevel As Short
        Dim i As Short
        Dim xNum As String
        Dim xText As String
        Dim iNumItems As Short
        Dim oItem As ListViewItem
        Dim oItems() As ListViewItem

        iPrevLevel = m_iLevel - 1
        iNumItems = lstPrevLevels.Items.Count
        oItems = New ListViewItem(iPrevLevel - 1) {}
        For i = 1 To iPrevLevel
            xText = "Level " & i
            xNum = xGetListNumber(xScheme, i)
            If xNum <> "" Then 'GLOG 8865 (dm)
                If AscW(xNum) = &HB7 Then
                    'Use a bullet that will display in list without font change
                    xNum = ChrW(&H25CF)
                End If
            End If
            oItem = New ListViewItem(New String() {xText, xNum})
            oItems(i - 1) = oItem
        Next i
        lstPrevLevels.Items.Clear()
        If oItems.Length > 0 Then
            lstPrevLevels.Items.AddRange(oItems)
            lstPrevLevels.Items(0).Selected = True
        End If
    End Sub

    Sub RedrawLevelButtons()
        Dim i As Short

        With Me.tsLevel.Items
            '       make visible all buttons up to last level
            For i = 0 To (m_iCurLevels - 1)
                .Item(i).Visible = True
            Next i

            '       ensure that levels past last
            '       level in scheme are invisible
            For i = m_iCurLevels To 8
                .Item(i).Visible = False
            Next
        End With
    End Sub

    Private Sub udStartAt_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles udStartAt.Leave
        Try
            m_xPrevCtl = "txtStartAt"
            If Not ActiveControl Is Me.txtStartAt Then
                RefreshLevel()
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Function bLevelIsDirty() As Boolean
        'returns TRUE if one of the module level
        'flags representing level dirt has been set true
        bLevelIsDirty = ((CShort(CShort(CShort(CShort(CShort(m_bLevelDirty) + CShort(m_bTCDirty)) + CShort(m_bParaDirty)) + CShort(m_bPropDirty)) + CShort(m_bNextParaDirty)) + CShort(m_bTOCDirty)) < 0)
    End Function

    Private Sub RefreshLevel()
        Dim i As Short
        Dim iPrevLevel As Short
        Dim xNum As String
        Dim xNumFormat As String
        Dim bIsDirty As Boolean

        m_bStopChangeEvent = True

        '   create level based on user choices
        bIsDirty = bLevelIsDirty()

        If (Not m_bFormInit) And m_bAllowRefresh And bIsDirty Then
            EchoOff()

            CurWordApp.ActiveDocument.UndoClear()

            '       implement changes
            'UPGRADE_WARNING: Couldn't resolve default property of object bCreateLevel(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bCreateLevel((g_oCurScheme.Name), m_iLastSelLevel)

            '       cont style
            If m_bNextParaDirty Then
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bCreateContLevel((g_oCurScheme.Name), m_iLastSelLevel)
            End If

            '       update dirty flag array
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bSetDirt(m_iLastSelLevel, m_bLevelDirty, m_bTCDirty, m_bTOCDirty, m_bPropDirty, m_bParaDirty, m_bNextParaDirty)

            On Error Resume Next
            m_oPreview.RefreshPreviewLevel((g_oCurScheme.Name), m_iLastSelLevel)
            EchoOn()
        End If

        '   reset flags
        m_bStopChangeEvent = False
        ClearSchemeDirtFlags()
        'System.Windows.Forms.Application.DoEvents() 'remmed for GLOG 8713
        '9.9.5001 - seems unnecessary in Word 2013 and interferes with both
        'screen capture and design mode execution
        If g_iWordVersion < mpWordVersions.mpWordVersion_2013 Then SendShiftKey()
    End Sub

    Private Sub SwitchLevels()
        Dim i As Short
        Dim iPrevLevel As Short
        Dim xNum As String
        Dim xNumFormat As String
        Dim bIsDirty As Boolean
        Dim bEnabled As Boolean

        Try

            m_bStopChangeEvent = True
            m_bAllowRefresh = False

            EchoOff()
            CurWordApp.ScreenUpdating = False

            '   create level based on user choices
            bIsDirty = bLevelIsDirty()

            If (Not m_bFormInit) And bIsDirty Then
                '       implement changes
                'UPGRADE_WARNING: Couldn't resolve default property of object bCreateLevel(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bCreateLevel((g_oCurScheme.Name), m_iLastSelLevel)

                '       cont style
                If m_bNextParaDirty Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bRet = bCreateContLevel((g_oCurScheme.Name), m_iLastSelLevel)
                End If

                '       update dirty flag array
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bSetDirt(m_iLastSelLevel, m_bLevelDirty, m_bTCDirty, m_bTOCDirty, m_bPropDirty, m_bParaDirty, m_bNextParaDirty)
            End If

            If Not m_bFormInit Then
                If bIsDirty Then m_oPreview.RefreshPreviewLevel((g_oCurScheme.Name), m_iLastSelLevel)
                m_oPreview.HighlightLevel(m_iLevel)
            End If

            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()

            '   load properties of level
            lRet = lLoadLevelProperties()

            '   fill previous level list
            FillPrevLevelsList((g_oCurScheme.Name))

            '   set text in 'Heading' frame (removed in 9.5.1)
            '    Me.optStyleFormats.Caption = "Save In " & _
            ''        xGetStyleRoot(g_oCurScheme.Name) & "_L" & _
            ''            Me.cmbLevel.ListIndex + 1 & " St&yle"

            bEnabled = (Me.cmbNumberStyle.Text <> "Bullet") And (m_iLevel <> 1)

            '    enable/disable prev levels - disabled on level 1 and bullet
            Me.lstPrevLevels.Enabled = bEnabled
            Me.lblPreviousLevels.Enabled = bEnabled
            Me.btnAddPrevLevel.Enabled = bEnabled

            '   reset flags
            ClearSchemeDirtFlags()

            m_bStopChangeEvent = False
            m_bLevelChanged = False
            m_bAllowRefresh = True

            '   set as last selected level
            m_iLastSelLevel = m_iLevel
            System.Windows.Forms.Application.DoEvents()
        Finally
            EchoOn()
        End Try
    End Sub

    Sub ClearSchemeDirtFlags()
        m_bLevelDirty = False
        m_bTCDirty = False
        m_bTOCDirty = False
        m_bPropDirty = False
        m_bParaDirty = False
        m_bNextParaDirty = False
    End Sub

    Sub InsertPrevLevel()
        'insert selected previous levels directly
        'before current level number
        Dim i As Short
        Dim iLevel As Short
        Dim iPos As Short
        Dim xPrePos As String
        Dim xPostPos As String
        Dim xPrev As String
        Dim xNumFormat As String
        Dim iPrevLevel As Short
        Dim xNum As String
        Dim bRawNumberFormat As Boolean
        Dim xLT As String


        m_bInsertingPrevLevel = True

        iLevel = m_iLevel

        ''   build string of numbers

        For i = 0 To lstPrevLevels.SelectedIndices.Count - 1
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.lstPrevLevels.SelBookmarks.Item(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iPrevLevel = Me.lstPrevLevels.SelectedIndices(i) + 1
            '           alert and exit if user is
            '           attempting to add level a
            '           second time
            If InStr(m_xCode, CStr(iPrevLevel)) Then
                xMsg = "Level " & iLevel & " already " & "contains a Level " & iPrevLevel & " number."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                m_bInsertingPrevLevel = False
                Exit Sub
            End If

            'determine whether we can display the actual characters of
            'the dynamic portion of the number - if the style is not supported,
            'we'll leave the level tokens unevaluated
            xLT = xGetFullLTName((g_oCurScheme.Name))
            bRawNumberFormat = (iMapNumberStyle_WordToMP((CurWordApp.ActiveDocument.ListTemplates.Item(xLT).ListLevels.Item(iPrevLevel).NumberStyle)) > 16)
            If bRawNumberFormat Then
                xNum = "%" & iPrevLevel
            Else
                xNum = xGetListNumber((g_oCurScheme.Name), iPrevLevel, Me.chkLegalStyle.CheckState = 1)
            End If

            'disallow bullet
            If xNum <> ChrW(&HB7) Then
                xPrev = xPrev & xNum
            End If

        Next i
        '   do only if there are selected previous levels
        If Len(xPrev) > 0 Then
            '       get position of current level number
            iPos = InStr(m_xCode, CStr(iLevel))

            With Me.rtfNumberFormat
                '           prevent sel change from running
                '           when we make our selection
                m_bNoSelChangeEvent = True
                '           if current level number exists...
                If iPos Then
                    '               insert previous levels directly
                    '               before current level number
                    .SelectionStart = iPos - 1
                Else
                    '               insert previous levels at end of text
                    .SelectionStart = Len(.Text) 'GLOG 8571 (dm, 4/11/17)
                End If
                m_bIsPrevLevels = True
                m_iSelStart = .SelectionStart
                '           again, prevent change code from running
                m_bNoSelChangeEvent = True
                .SelectionLength = 0
                .SelectionColor = System.Drawing.Color.Red
                .SelectedText = xPrev
                m_bIsPrevLevels = False
            End With
        End If
        RefreshLevel()
        m_bInsertingPrevLevel = False
    End Sub

    Sub SetTriStateChkbox(ByRef chkP As System.Windows.Forms.CheckBox)
        If Not m_bClicked Then
            m_bLevelDirty = True
        End If
    End Sub

    Sub FormatLegalNumber()
        Exit Sub
        '  STILL UNDER DEVELOPMENT

        'insert selected previous levels directly
        'before current level number
        Dim i As Short
        Dim iLevel As Short
        Dim iPos As Short
        Dim xNum As String
        Dim iStartSel As Short
        Dim iOldNumLength As Short

        m_bInsertingPrevLevel = True


        iLevel = m_iLevel
        iStartSel = Me.rtfNumberFormat.SelectionStart

        For i = 1 To iLevel - 1
            iPos = InStr(m_xCode, LTrim(Str(i)))

            If iPos > 0 Then
                iOldNumLength = lCountChrs(m_xCode, LTrim(Str(i)))

                xNum = xGetListNumber((g_oCurScheme.Name), i, Me.chkLegalStyle.CheckState = 1)
                iStartSel = iStartSel + (Len(xNum) - iOldNumLength)

                '           insert and format new number
                With Me.rtfNumberFormat
                    .SelectionStart = iPos - 1
                    .SelectionLength = iOldNumLength
                    .SelectedText = xNum
                    xUserAll = .Rtf
                    '                .SelColor = vbBlue
                End With
                m_xCode = VB.Left(m_xCode, iPos - 1) & New String(LTrim(Str(i)), Len(xNum)) & VB.Right(m_xCode, Len(m_xCode) - iPos - iOldNumLength + 1)
            End If
        Next i
        Me.rtfNumberFormat.SelectionStart = iStartSel

        RefreshLevel()
        m_bInsertingPrevLevel = False
    End Sub

    Private Sub spnRightIndent_Change(Sender As Object, e As EventArgs) Handles spnRightIndent.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub FillNextParaList(ByRef iLevels As Short)
        Dim xStyleRoot As String
        Dim i As Short
        Dim iCount As Short

        m_xarNextPara = New ArrayList()

        '   these are from doc prop in firm's tsgNumbers.sty
        If g_xNextParaStyles(0) <> "" Then
            For i = 0 To UBound(g_xNextParaStyles)
                m_xarNextPara.Add(xTranslateBuiltInStyle(g_xNextParaStyles(i)))
            Next i
        End If

        '   add num continue style if this
        '   is an upgrade to v8.0
        If g_iUpgradeFrom = 80 Then
            m_xarNextPara.Add("Num Continue")
        End If

        xStyleRoot = xGetStyleRoot(g_oCurScheme.Name)

        '   add numbering styles
        For i = 1 To iLevels
            'GLOG 15891 (dm) - display Heading 1-9 when appropriate
            If bIsHeadingScheme(g_oCurScheme.Name) Or
                    ((g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Private) And g_bUseWordHeadingStyles) Then
                m_xarNextPara.Add(xTranslateHeadingStyle(i))
            Else
                m_xarNextPara.Add(xStyleRoot & "_L" & i)
            End If
        Next i

        '   these are offered for every scheme
        If g_bDisplayContStylesInNextParaList Then 'GLOG 15891 (dm)
            For i = 1 To iLevels
                m_xarNextPara.Add(xStyleRoot & " Cont " & i)
            Next i
        End If

        '   fill next paragraph list
        m_bFillingNextParaList = True
        Me.cmbNextParagraph.SetList(m_xarNextPara)
        m_bFillingNextParaList = False
    End Sub

    Private Sub spnContLeftIndent_Change(Sender As Object, e As EventArgs) Handles spnContLeftIndent.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnContLeftIndent_Leave(sender As Object, e As EventArgs) Handles spnContLeftIndent.Leave
        m_xPrevCtl = "spnContLeftIndent"
    End Sub


    Private Sub spnContRightIndent_Change(Sender As Object, e As EventArgs) Handles spnContRightIndent.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnContRightIndent_Leave(sender As Object, e As EventArgs) Handles spnContRightIndent.Leave
        m_xPrevCtl = "spnContRightIndent"
    End Sub

    Private Sub cmbSpecial_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSpecial.ValueChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True

            If Not m_bLevelChanged Then
                m_bLevelChanged = True
                If cmbSpecial.Text = "(none)" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnBy.Value = 0
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                ElseIf Me.spnBy.Value = 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.spnBy.Value = 36
                End If
                m_bLevelChanged = False
            End If
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbSpecial_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSpecial.Leave
        m_xPrevCtl = "cmbSpecial"
    End Sub

    Private Sub spnBy_Change(Sender As Object, e As EventArgs) Handles spnBy.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True

            If Not m_bLevelChanged Then
                m_bLevelChanged = True
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.cmbSpecial.Text = "(none)" And Me.spnBy.DisplayText <> "" Then
                    Me.cmbSpecial.Value = "First line"
                End If
                m_bLevelChanged = False
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnBy_SpinUp()
        m_bLevelChanged = True
        If Me.cmbSpecial.Text = "(none)" Then
            Me.cmbSpecial.Value = "First line"
        End If
        m_bLevelChanged = False
    End Sub

    Private Sub spnBy_Leave(sender As Object, e As EventArgs) Handles spnBy.Leave
        m_xPrevCtl = "spnBy"
    End Sub

    Private Sub spnContSpaceBefore_Change(Sender As Object, e As EventArgs) Handles spnContSpaceBefore.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnContSpaceBefore_Leave(sender As Object, e As EventArgs) Handles spnContSpaceBefore.Leave
        m_xPrevCtl = "spnContSpaceBefore"
    End Sub

    Private Sub spnContSpaceAfter_Change(Sender As Object, e As EventArgs) Handles spnContSpaceAfter.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnContSpaceAfter_Leave(sender As Object, e As EventArgs) Handles spnContSpaceAfter.Leave
        m_xPrevCtl = "spnContSpaceAfter"
    End Sub

    Private Sub cmbContLineSpacing_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContLineSpacing.ValueChanged
        Dim iSpacing As Microsoft.Office.Interop.Word.WdLineSpacing

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContLineSpacing.Bookmark. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            iSpacing = Me.cmbContLineSpacing.SelectedIndex
            Me.lblContAt.Enabled = (iSpacing > 2)
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            With Me.spnContAt
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .Enabled = (iSpacing > 2)
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .AppendSymbol = (iSpacing <> Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple)
                If iSpacing = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .MinValue = 0.5
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .IncrementValue = 0.5
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .MinValue = 1
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .IncrementValue = 1
                End If
                If Not m_bLevelChanged Then
                    Select Case iSpacing
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceSingle, Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceAtLeast, Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceExactly
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 12
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpace1pt5
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 18
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceDouble
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 24
                        Case Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Value = 3
                    End Select
                    If iSpacing < 3 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .DisplayText = ""
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Refresh()
                    End If
                End If
            End With
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub

    Private Sub cmbContLineSpacing_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbContLineSpacing.Leave
        m_xPrevCtl = "cmbContLineSpacing"
    End Sub

    'UPGRADE_WARNING: Event chkContKeepLinesTogether.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkContKeepLinesTogether_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContKeepLinesTogether.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkContKeepLinesTogether_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContKeepLinesTogether.Leave
        m_xPrevCtl = "chkContKeepLinesTogether"
    End Sub
    'UPGRADE_WARNING: Event chkContKeepWithNext.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkContKeepWithNext_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContKeepWithNext.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkContKeepWithNext_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContKeepWithNext.Leave
        m_xPrevCtl = "chkContKeepWithNext"
    End Sub
    'UPGRADE_WARNING: Event chkContWidowOrphan.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkContWidowOrphan_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContWidowOrphan.CheckStateChanged
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch e As Exception
            [Error].Show(e)
        End Try
    End Sub
    Private Sub chkContWidowOrphan_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkContWidowOrphan.Leave
        m_xPrevCtl = "chkContWidowOrphan"
    End Sub

    Private Sub spnContAt_Change(Sender As Object, e As EventArgs) Handles spnContAt.Change
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            m_bNextParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnContAt_Leave(sender As Object, e As EventArgs) Handles spnContAt.Leave
        m_xPrevCtl = "spnContAt"
    End Sub

    Private Function iMapNumberStyle_MPToWord(ByRef iConst As Short) As Short
        Dim i As Short

        Select Case iConst
            Case mpListNumberStyleNone
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleNone
            Case mpListNumberStyleArabic
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic
            Case mpListNumberStyleUppercaseRoman
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman
            Case mpListNumberStyleLowercaseRoman
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseRoman
            Case mpListNumberStyleUppercaseLetter
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseLetter
            Case mpListNumberStyleLowercaseLetter
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseLetter
            Case mpListNumberStyleOrdinal
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinal
            Case mpListNumberStyleCardinalText
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleCardinalText
            Case mpListNumberStyleOrdinalText
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinalText
            Case mpListNumberStyleArabicLZ
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabicLZ
            Case mpListNumberStyleArabicLZ2
                i = 62
            Case mpListNumberStyleArabicLZ3
                i = 63
            Case mpListNumberStyleArabicLZ4
                i = 64
            Case mpListNumberStyleBullet
                i = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet
            Case Else
                'supplemental style - get index from array
                i = CShort(g_xSupplementalNumberStyles(iConst - 17))
        End Select

        iMapNumberStyle_MPToWord = i
    End Function

    Private Function iMapNumberStyle_WordToMP(ByRef iConst As Microsoft.Office.Interop.Word.WdListNumberStyle) As Short
        Dim i As Short
        Dim iCount As Short
        Dim j As Short

        Select Case iConst
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleNone
                i = mpListNumberStyleNone
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic
                i = mpListNumberStyleArabic
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman
                i = mpListNumberStyleUppercaseRoman
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseRoman
                i = mpListNumberStyleLowercaseRoman
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseLetter
                i = mpListNumberStyleUppercaseLetter
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseLetter
                i = mpListNumberStyleLowercaseLetter
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinal
                i = mpListNumberStyleOrdinal
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleCardinalText
                i = mpListNumberStyleCardinalText
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinalText
                i = mpListNumberStyleOrdinalText
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabicLZ
                i = mpListNumberStyleArabicLZ
            Case 62
                i = mpListNumberStyleArabicLZ2
            Case 63
                i = mpListNumberStyleArabicLZ3
            Case 64
                i = mpListNumberStyleArabicLZ4
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet
                i = mpListNumberStyleBullet
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegal
                i = mpListNumberStyleLegal
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegalLZ
                i = mpListNumberStyleLegalLZ
            Case Else
                'supplemental style - get index from array
                iCount = UBound(g_xSupplementalNumberStyles)
                For j = 0 To iCount
                    If g_xSupplementalNumberStyles(j) = CStr(iConst) Then
                        i = j + 17
                        Exit For
                    End If
                Next j

                'if not in array, add now
                If i = 0 Then
                    If g_xSupplementalNumberStyles(0) <> "" Then iCount = iCount + 1
                    ReDim Preserve g_xSupplementalNumberStyles(iCount)
                    g_xSupplementalNumberStyles(iCount) = CStr(iConst)
                    'GLOG : 5445 : ceh - needs to be tested
                    '                Me.cmbNumberStyle.AddItem GetNumStyleDisplayName(iConst)
                    m_xarNumStyle.Add(GetNumStyleDisplayName(iConst))
                    Me.cmbNumberStyle.ClearList()
                    Me.cmbNumberStyle.SetList(m_xarNumStyle)
                    i = iCount + 17
                End If
        End Select

        iMapNumberStyle_WordToMP = i
    End Function

    Private Function GetNumStyleDisplayName(ByRef iStyle As Microsoft.Office.Interop.Word.WdListNumberStyle) As String
        Dim xDisplayName As String

        'we need to use integers because the supplemental styles
        'were not in the Word 8 library
        Select Case iStyle
            Case 20
                xDisplayName = "Aiueo"
            Case 12
                xDisplayName = "Aiueo Half Width"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic
                xDisplayName = "1, 2, 3, ..."
            Case 46
                xDisplayName = "Arabic 1"
            Case 48
                xDisplayName = "Arabic 2"
            Case 14
                xDisplayName = "Arabic Full Width"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabicLZ
                xDisplayName = "01, 02, ..."
            Case 62
                xDisplayName = "001, 002, ..."
            Case 63
                xDisplayName = "0001, 0002, ..."
            Case 64
                xDisplayName = "00001, 00002, ..."
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet
                xDisplayName = "Bullet"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleCardinalText
                xDisplayName = "One, Two, ..."
                xDisplayName = ""
            Case 25
                xDisplayName = "Chosung"
            Case 24
                xDisplayName = "Ganada"
            Case 26
                xDisplayName = "GB Numeric 1"
            Case 27
                xDisplayName = "GB Numeric 2"
            Case 28
                xDisplayName = "GB Numeric 3"
            Case 29
                xDisplayName = "GB Numeric 4"
            Case 43
                xDisplayName = "Hangul"
            Case 44
                xDisplayName = "Hanja"
            Case 41
                xDisplayName = "Hanja Read"
            Case 42
                xDisplayName = "Hanja Read Digit"
            Case 45
                xDisplayName = "Hebrew 1"
            Case 47
                xDisplayName = "Hebrew 2"
            Case 51
                xDisplayName = "Hindi Arabic"
            Case 52
                xDisplayName = "Hindi Cardinal Text"
            Case 49
                xDisplayName = "Hindi Letter 1"
            Case 50
                xDisplayName = "Hindi Letter 2"
            Case 21
                xDisplayName = "Iroha"
            Case 13
                xDisplayName = "Iroha Half Width"
            Case 10
                xDisplayName = "Kanji"
            Case 11
                xDisplayName = "Kanji Digit"
            Case 16
                xDisplayName = "KanjiTraditional"
            Case 17
                xDisplayName = "KanjiTraditional 2"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegal
                xDisplayName = "Legal"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegalLZ
                xDisplayName = "Legal LZ"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseLetter
                xDisplayName = "a, b, c, ... "
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLowercaseRoman
                xDisplayName = "i, ii, iii, ... "
            Case 58
                xDisplayName = "Lowercase Russian"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleNone
                xDisplayName = "(No Number)"
            Case 18
                xDisplayName = "Number In Circle"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinal
                xDisplayName = "1st, 2nd, ..."
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleOrdinalText
                xDisplayName = "First, Second, ..."
            Case 249
                xDisplayName = "Picture Bullet"
            Case 37
                xDisplayName = "Simplified Chinese 1"
            Case 38
                xDisplayName = "Simplified Chinese 2"
            Case 39
                xDisplayName = "Simplified Chinese 3"
            Case 40
                xDisplayName = "Simplified Chinese 4"
            Case 54
                xDisplayName = "Thai Arabic"
            Case 55
                xDisplayName = "Thai Cardinal Text"
            Case 53
                xDisplayName = "Thai Letter"
            Case 33
                xDisplayName = "Traditional Chinese 1"
            Case 34
                xDisplayName = "Traditional Chinese 2"
            Case 35
                xDisplayName = "Traditional Chinese 3"
            Case 36
                xDisplayName = "Traditional Chinese 4"
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseLetter
                xDisplayName = "A, B, C, ..."
            Case Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman
                xDisplayName = "I, II, III, ..."
            Case 59
                xDisplayName = "Uppercase Russian"
            Case 56
                xDisplayName = "Vietnamese Cardinal Text"
            Case 30
                xDisplayName = "Zodiac 1"
            Case 31
                xDisplayName = "Zodiac 2"
            Case 32
                xDisplayName = "Zodiac 3"
        End Select

        GetNumStyleDisplayName = xDisplayName
    End Function

    Private Function bCreateLevel(ByRef xScheme As String, ByRef iLevel As Short) As Object
        Dim xNumberFormat As String
        Dim iNumberStyle As Short
        Dim xStyle As String
        Dim xNextLevel As String
        Dim xHeadingSourceStyle As String
        Dim lNextColor As Integer
        Dim lNextBold As Integer
        Dim lNextAllCaps As Integer
        Dim lNextUnderline As Integer
        Dim lNextItalic As Integer
        Dim lNextSmallCaps As Integer
        Dim bIsBullet As Boolean
        Dim iHeadingFormat As Short
        Dim iNextAlign As Short
        Dim iNextLineSpace As Single
        Dim iNextLineSpaceRule As Short
        Dim iNextSpaceBefore As Short
        Dim iNextSpaceAfter As Short
        Dim iNextKeepTogether As Short
        Dim iNextKeepWithNext As Short
        Dim iNextWidowControl As Short
        Dim iNextPageBreak As Short
        Dim iLevels As Short
        Dim iLineSpaceRule As Short
        Dim styContNew As Word.Style
        Dim xLT As String
        Dim xNextFontName As String
        Dim sNextFontSize As Single
        Dim sNextRightIndent As Single
        Dim xNextContFontName As String
        Dim sNextContFontSize As Single
        Dim xCont As String
        Dim xNextLevelCont As String
        Dim styContNext As Word.Style
        Dim xStyRoot As String
        Dim pfNormal As Word.ParagraphFormat
        Dim sNextFirstIndent As Single
        Dim sNextLeftIndent As Single
        Dim iContlevel As Short
        Dim iAlignment As Short
        Dim iTrailUnderline As Short
        Dim iStyleIndex As Short
        Dim oLL As Word.ListLevel

        xStyle = xGetStyleName(xScheme, iLevel)
        iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)
        xLT = xGetFullLTName(xScheme)
        xStyRoot = xGetStyleRoot(xScheme)
        xCont = xStyRoot & " Cont " & iLevel

        With CurWordApp.ActiveDocument
            '       list template properties
            If m_bLevelDirty Then
                '           modify list level props according to Edit dlg props
                oLL = .ListTemplates.Item(xLT).ListLevels.Item(iLevel)
                With oLL
                    '               get number style
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbNumberStyle.SelectedIndex. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    iStyleIndex = Me.cmbNumberStyle.SelectedIndex
                    If iStyleIndex > 9 Then iStyleIndex = iStyleIndex + m_iFixedDigitNumberingOffset
                    If iStyleIndex > 13 Then
                        'supplemental style - add 2 because legal
                        'styles aren't on the list
                        iStyleIndex = iStyleIndex + 2
                    End If
                    iNumberStyle = iMapNumberStyle_MPToWord(iStyleIndex)

                    '               modify num style as appropriate if legal style is checked
                    If Me.chkLegalStyle.CheckState Then
                        If iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic Then
                            iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegal
                        Else
                            iNumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleLegalLZ
                        End If
                    End If

                    '               reformat rtf value
                    xNumberFormat = xGetNumFormat(m_xCode, Me.rtfNumberFormat.Text, (Me.chkNonbreakingSpaces).CheckState)

                    '               clear out existing format/style to avoid
                    '               incompatibilities - ie code errs when number format
                    '               has a number placeholder and an attempt is made to
                    '               set the style to bullets, etc.
                    '               12/21/00 - don't overwrite preexisting bullet
                    '                If (iNumberStyle <> wdListNumberStyleBullet) Or _
                    ''                        (.NumberStyle <> wdListNumberStyleBullet) Then
                    .NumberFormat = ""
                    .NumberStyle = Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleArabic
                    .NumberStyle = iNumberStyle
                    If .NumberStyle <> Microsoft.Office.Interop.Word.WdListNumberStyle.wdListNumberStyleBullet Then
                        .NumberFormat = xNumberFormat
                    Else
                        .NumberFormat = m_xBulletCharNum
                    End If
                    '                End If

                    '               set start at
                    If Me.txtStartAt.Text = "0 " Then
                        '                   we don't allow user to set this to 0, but it
                        '                   may have been inherited from Word
                        .StartAt = 0
                    ElseIf Me.txtStartAt.Text <> "" Then
                        .StartAt = Me.udStartAt.Value
                    Else
                        .StartAt = 1
                    End If

                    'set reset on higher
                    If iLevel > 1 Then
                        If Me.chkReset.CheckState = 0 Then
                            'this was a boolean property in Word 97, so our checkbox is
                            'sufficient; otherwise, it's sufficient only to turn off
                            .ResetOnHigher = Me.chkReset.CheckState
                        ElseIf .ResetOnHigher = False Then
                            'if checked in another Word version, only set to True if value
                            'was previously False; otherwise, preserve existing setting
                            .ResetOnHigher = True
                        End If
                    End If

                    '               turn off error checking here to prevent messages
                    '               when data is invalid - this is handled by the
                    '               interface, and brings up unnecessary msgs because
                    '               of the relationship between validation and preview refresh
                    On Error Resume Next
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .NumberPosition = Me.spnNumberPosition.Value
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .TextPosition = Me.spnTextPosition.Value
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTabPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .TabPosition = Me.spnNumberPosition.Value + Me.spnTabPosition.Value
                    On Error GoTo 0

                    If Me.chkRightAlign.CheckState Then
                        .Alignment = Microsoft.Office.Interop.Word.WdListLevelAlignment.wdListLevelAlignRight
                    Else
                        .Alignment = Microsoft.Office.Interop.Word.WdListLevelAlignment.wdListLevelAlignLeft
                    End If

                    '               trailing characters are part of
                    '               number unless they're shift-returns
                    Select Case Me.cmbTrailingChar.SelectedIndex
                        Case mpTrailingChars.mpTrailingChar_Tab
                            .TrailingCharacter = Microsoft.Office.Interop.Word.WdTrailingCharacter.wdTrailingTab
                        Case mpTrailingChars.mpTrailingChar_Space
                            .TrailingCharacter = Microsoft.Office.Interop.Word.WdTrailingCharacter.wdTrailingSpace
                            '                    Case mpTrailingChar_DoubleSpace
                            '                        .NumberFormat = .NumberFormat & Space(1)
                            '                        .TrailingCharacter = wdTrailingSpace
                        Case Else
                            .TrailingCharacter = Microsoft.Office.Interop.Word.WdTrailingCharacter.wdTrailingNone
                    End Select

                    bIsBullet = (xNumberFormat = ChrW(&HB7))

                    With .Font
                        '                    If bIsBullet Then
                        '                        .Name = "Symbol"
                        '                    ElseIf .Name = "Symbol" Then
                        '                        .Name = ActiveDocument.Styles(wdStyleNormal).Font.Name
                        '                    End If

                        '                   get bold, italic, underline from Edit Scheme dlg
                        .AllCaps = ConvFromTripleState((Me.chkNumberCaps).CheckState)
                        .Bold = ConvFromTripleState((Me.chkNumberBold).CheckState)
                        .Italic = ConvFromTripleState((Me.chkNumberItalic).CheckState)

                        '                   different types of underline
                        '                   for different scenarios
                        If Me.cmbUnderlineStyle.SelectedIndex >= 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbUnderlineStyle.SelectedIndex. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Underline = Me.cmbUnderlineStyle.SelectedIndex
                        Else
                            .Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineNone
                        End If

                        If g_bApplyHeadingColor Then
                            If Me.optDirectFormats.Checked Then
                                .ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdAuto
                            Else
                                .ColorIndex = iHeadingColor()
                            End If
                        End If

                        If Me.cmbNumFontName.Text <> "-Undefined-" Then .Name = Me.cmbNumFontName.Text
                        If Me.cmbNumFontSize.Text <> "-Undefined-" Then
                            '                       font size is stored in array because it needs
                            '                       to be forced for preview in buffer doc
                            g_sNumberFontSizes(iLevel - 1) = CSng(Me.cmbNumFontSize.Text)
                        End If
                    End With
                End With
            End If

            '       style paragraph format
            If m_bParaDirty Then
                '           save alignment and line spacing for next
                '           para - then return these values after
                '           setting values for cur para - this is
                '           made necessary because the styles are
                '           built hierarchically, but the UI treats
                '           them as if they are not
                If iLevel < iLevels Then
                    xNextLevel = xGetStyleName(xScheme, iLevel + 1)
                    With CurWordApp.ActiveDocument.Styles.Item(xNextLevel).ParagraphFormat
                        iNextAlign = .Alignment
                        iNextLineSpaceRule = .LineSpacingRule
                        iNextLineSpace = .LineSpacing
                        iNextSpaceBefore = .SpaceBefore
                        iNextSpaceAfter = .SpaceAfter
                        iNextKeepTogether = .KeepTogether
                        iNextKeepWithNext = .KeepWithNext
                        iNextWidowControl = .WidowControl
                        iNextPageBreak = .PageBreakBefore
                        sNextRightIndent = .RightIndent
                        sNextLeftIndent = .LeftIndent
                        sNextFirstIndent = .FirstLineIndent
                    End With
                End If

                With .Styles.Item(xStyle).ParagraphFormat
                    '               alignment
                    If Me.cmbTextAlign.SelectedIndex = mpBaseAlignmentOnNormal Then
                        .Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbTextAlign.Bookmark. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        .Alignment = Me.cmbTextAlign.SelectedIndex
                    End If

                    '               change line spacing only if list index has a value and
                    '               it doesn't match current line spacing
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If Not IsDBNull(Me.cmbLineSpacing.SelectedIndex) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbLineSpacing.Bookmark. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        iLineSpaceRule = Me.cmbLineSpacing.SelectedIndex
                        .LineSpacingRule = iLineSpaceRule
                        If iLineSpaceRule = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .LineSpacing = CurWordApp.LinesToPoints(Me.spnAt.Value)
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .LineSpacing = Me.spnAt.Value
                        End If
                        '                    Set pfNormal = ActiveDocument.Styles(wdStyleNormal) _
                        ''                        .ParagraphFormat
                        '                    If (pfNormal.LineSpacingRule = wdLineSpaceExactly) And _
                        ''                            (.LineSpacingRule = wdLineSpaceExactly) Then
                        ''                       exact line spacing is now set relative to Normal
                        '                        iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, _
                        ''                            .LineSpacing, pfNormal.LineSpacing)
                        '                    Else
                        '                        iLineSpaceRule = ConvertLineSpace(.LineSpacingRule, _
                        ''                            .LineSpacing)
                        '                    End If
                        '
                        '                    If Me.cmbLineSpacing.ListIndex <> iLineSpaceRule Then
                        '                        .LineSpacingRule = Me.cmbLineSpacing.ListIndex
                        '                    End If
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnSpaceBefore. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .SpaceBefore = Me.spnSpaceBefore.Value
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnSpaceAfter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .SpaceAfter = Me.spnSpaceAfter.Value
                    .KeepTogether = -1 * Me.chkKeepLinesTogether.CheckState
                    .KeepWithNext = -1 * Me.chkKeepWithNext.CheckState
                    .WidowControl = -1 * Me.chkWidowOrphan.CheckState
                    .PageBreakBefore = -1 * Me.chkPageBreak.CheckState
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .RightIndent = Me.spnRightIndent.Value

                    '                style indents now takes priority over LT positions
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnTextPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .LeftIndent = Me.spnTextPosition.Value
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnNumberPosition. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .FirstLineIndent = Me.spnNumberPosition.Value - .LeftIndent
                End With

                If iLevel < iLevels Then
                    '               return next level to original
                    '               alignment/spacing
                    With CurWordApp.ActiveDocument.Styles.Item(xNextLevel).ParagraphFormat
                        .Alignment = iNextAlign
                        .LineSpacingRule = iNextLineSpaceRule
                        .LineSpacing = iNextLineSpace
                        .SpaceAfter = iNextSpaceAfter
                        .SpaceBefore = iNextSpaceBefore
                        .KeepTogether = -1 * System.Math.Abs(iNextKeepTogether)
                        .KeepWithNext = -1 * System.Math.Abs(iNextKeepWithNext)
                        .WidowControl = -1 * System.Math.Abs(iNextWidowControl)
                        .PageBreakBefore = -1 * System.Math.Abs(iNextPageBreak)
                        .RightIndent = sNextRightIndent
                        .LeftIndent = sNextLeftIndent
                        .FirstLineIndent = sNextFirstIndent
                    End With
                End If

                '           next paragraph style
                Err.Clear()
                On Error Resume Next
                'UPGRADE_NOTE: Object styContNew may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                styContNew = Nothing
                styContNew = .Styles(Me.cmbNextParagraph.Text)
                On Error GoTo 0
                If styContNew Is Nothing Then
                    iContlevel = Val(VB.Right(Me.cmbNextParagraph.Text, 1))
                    If (InStr(Me.cmbNextParagraph.Text, " Cont ") <> 0) And (iContlevel >= 1) And (iContlevel <= 9) Then
                        '                   MacPac proprietary cont style
                        CreateContStyles(xScheme, iContlevel)
                        styContNew = .Styles(Me.cmbNextParagraph.Text)
                    Else
                        If g_bCreateUnlinkedStyles Then
                            '9.9.4010
                            styContNew = AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument, Me.cmbNextParagraph.Text)
                        Else
                            styContNew = .Styles.Add(Me.cmbNextParagraph.Text, Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph)
                        End If
                        styContNew.Font = CurWordApp.ActiveDocument.Styles.Item(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleNormal).Font
                    End If
                End If
                If (g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Private) And bIsHeadingStyle(styContNew.NameLocal) Then
                    'GLOG 15891 (dm) - convert to proprietary style
                    .Styles.Item(xStyle).NextParagraphStyle = xStyRoot & "_L" & VB.Right(styContNew.NameLocal, 1)
                Else
                    .Styles.Item(xStyle).NextParagraphStyle = styContNew
                End If
                If g_iUpgradeFrom < 90 And styContNew.NameLocal = "Num Continue" Then
                    styContNew.BaseStyle = Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleBodyText
                    styContNew.ParagraphFormat = CurWordApp.ActiveDocument.Styles.Item(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleBodyText).ParagraphFormat
                End If
            End If

            '       heading format/style font format
            If m_bTCDirty Then
                '           next level style is based on this level
                '           so capture font attributes before changing
                If iLevel < iLevels Then
                    xNextLevel = xGetStyleName(xScheme, iLevel + 1)
                    With CurWordApp.ActiveDocument.Styles.Item(xNextLevel).Font
                        lNextColor = .ColorIndex
                        lNextBold = .Bold
                        lNextAllCaps = .AllCaps
                        lNextItalic = .Italic
                        lNextUnderline = .Underline
                        lNextSmallCaps = .SmallCaps
                        xNextFontName = .Name
                        sNextFontSize = .Size
                    End With

                    '               "cont" style
                    '                If Me.chkNextParaFont Then
                    '                    xNextLevelCont = xStyRoot & " Cont " & (iLevel + 1)
                    '                    On Error Resume Next
                    '                    Set styContNext = ActiveDocument.Styles(xNextLevelCont)
                    '                    On Error GoTo 0
                    '                    If Not styContNext Is Nothing Then
                    '                        xNextContFontName = styContNext.Font.Name
                    '                        sNextContFontSize = styContNext.Font.Size
                    '                    End If
                    '                End If
                End If

                '           set bit for heading type
                If Me.optStyleFormats.Checked Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_Type
                End If

                '           set bit for heading bold
                If Me.chkTCBold.CheckState Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_Bold
                End If

                '           set bit for heading all caps
                If Me.chkTCCaps.CheckState Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_AllCaps
                End If

                '           set bit for heading italic
                If Me.chkTCItalic.CheckState Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_Italic
                End If

                '           set bit for heading small caps
                If Me.chkTCSmallCaps.CheckState Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_SmallCaps
                End If

                '           set bit for heading small caps
                If Me.chkTCUnderline.CheckState Then
                    iHeadingFormat = iHeadingFormat Or mpTCFormatFields.mpTCFormatField_Underline
                End If

                '           save heading format
                lSetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_HeadingFormat, CStr(iHeadingFormat), mpSchemeTypes.mpSchemeType_Document)

                With .Styles.Item(xStyle).Font
                    If Me.optDirectFormats.Checked Then
                        .Bold = False
                        .Italic = False
                        .AllCaps = False
                        .SmallCaps = False
                        .Underline = False
                        If g_bApplyHeadingColor Then .ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdAuto
                    Else
                        '                   font formats are style-based,
                        '                   so modify style
                        .Bold = Me.chkTCBold.CheckState
                        .Italic = Me.chkTCItalic.CheckState
                        .AllCaps = Me.chkTCCaps.CheckState
                        .SmallCaps = Me.chkTCSmallCaps.CheckState
                        .Underline = Me.chkTCUnderline.CheckState
                        If g_bApplyHeadingColor Then .ColorIndex = iHeadingColor()
                    End If
                    .Name = Me.cmbFontName.Text
                    If Me.cmbFontSize.Text <> "-Undefined-" Then
                        .Size = CSng(Me.cmbFontSize.Text)
                    End If

                    '               "cont" style
                    '                If Me.chkNextParaFont Then
                    '                    Set styContNew = Nothing
                    '                    On Error Resume Next
                    '                    Set styContNew = ActiveDocument.Styles(xCont)
                    '                    On Error GoTo 0
                    '                    If Not styContNew Is Nothing Then
                    '                        styContNew.Font.Name = Me.cmbFontName.Text
                    '                        If Me.cmbFontSize.Text <> "-Undefined-" Then
                    '                            styContNew.Font.Size = Me.cmbFontSize.Text
                    '                        End If
                    '                    End If
                    '                End If
                End With

                '           restore next level font attributes
                If iLevel < iLevels Then
                    With CurWordApp.ActiveDocument.Styles.Item(xNextLevel).Font
                        .ColorIndex = lNextColor
                        .Bold = lNextBold
                        .AllCaps = lNextAllCaps
                        .Italic = lNextItalic
                        .Underline = lNextUnderline
                        .SmallCaps = lNextSmallCaps
                        .Name = xNextFontName
                        .Size = sNextFontSize
                    End With

                    '               "cont" style
                    '                If Me.chkNextParaFont Then
                    '                    If Not styContNext Is Nothing Then
                    '                        styContNext.Font.Name = xNextFontName
                    '                        styContNext.Font.Size = sNextFontSize
                    '                    End If
                    '                End If
                End If

            End If

            '       store more level properties
            If m_bPropDirty Then
                '           do trailing char
                lSetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_TrailChr, (Me.cmbTrailingChar.SelectedIndex), mpSchemeTypes.mpSchemeType_Document)

                '           do alignment
                iTrailUnderline = CShort(xGetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document))
                If bBitwisePropIsTrue(iTrailUnderline, mpTrailUnderlineFields.mpTrailUnderlineField_Underline) Then
                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_Underline
                End If
                If bBitwisePropIsTrue(iTrailUnderline, mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal) Then
                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal
                End If
                If Me.cmbTextAlign.SelectedIndex = mpBaseAlignmentOnNormal Then
                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal
                Else
                    iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_HonorAlignment
                End If
                lSetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, CStr(iAlignment), mpSchemeTypes.mpSchemeType_Document)
            End If

        End With 'active document

        g_bSchemeIsDirty = True
    End Function

    Private Function bCreateContLevel(ByRef xScheme As String, ByRef iLevel As Short) As Boolean
        Dim xCont As String
        Dim xNext As String
        Dim styCont As Word.Style
        Dim styNext As Word.Style
        Dim iAlign As Microsoft.Office.Interop.Word.WdParagraphAlignment
        Dim sFirstLineIndent As Single
        Dim sLeftIndent As Single
        Dim sRightIndent As Single
        Dim sLineSpace As Single
        Dim iLineSpaceRule As Microsoft.Office.Interop.Word.WdLineSpacing
        Dim iLineSpaceRuleNext As Microsoft.Office.Interop.Word.WdLineSpacing
        Dim sSpaceAfter As Single
        Dim sSpaceBefore As Single
        Dim bWidowCtl As Boolean
        Dim bKeepTogether As Boolean
        Dim bKeepWithNext As Boolean
        Dim bRestoreNext As Boolean
        Dim xFont As String
        Dim sSize As Single
        Dim xRoot As String
        Dim iProp As Short

        xRoot = xGetStyleRoot(xScheme)
        xCont = xRoot & " Cont " & iLevel

        If iLevel < 9 Then
            xNext = xRoot & " Cont " & iLevel + 1
            On Error Resume Next
            styNext = CurWordApp.ActiveDocument.Styles(xNext)
            On Error GoTo 0
            If Not styNext Is Nothing Then
                bRestoreNext = True
                With styNext
                    With .ParagraphFormat
                        iAlign = .Alignment
                        sFirstLineIndent = .FirstLineIndent
                        sLeftIndent = .LeftIndent
                        sRightIndent = .RightIndent
                        iLineSpaceRuleNext = .LineSpacingRule
                        sLineSpace = .LineSpacing
                        sSpaceAfter = .SpaceAfter
                        sSpaceBefore = .SpaceBefore
                        bWidowCtl = .WidowControl
                        bKeepTogether = .KeepTogether
                        bKeepWithNext = .KeepWithNext
                    End With
                    With .Font
                        xFont = .Name
                        sSize = .Size
                    End With
                End With
            End If
        End If

        '   modify level prop
        iProp = CShort(xGetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document))
        If Me.cmbAlignment.SelectedIndex = mpBaseAlignmentOnNormal Then
            iProp = iProp Or mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal
        Else
            iProp = iProp And (Not mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal)
        End If
        lSetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_TrailUnderline, CStr(iProp), mpSchemeTypes.mpSchemeType_Document)

        '   edit style
        styCont = CurWordApp.ActiveDocument.Styles(xCont)
        With styCont
            '       paragraph
            With .ParagraphFormat
                If Me.cmbAlignment.SelectedIndex = mpBaseAlignmentOnNormal Then
                    .Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbAlignment.SelectedIndex. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .Alignment = Me.cmbAlignment.SelectedIndex
                End If

                If Me.cmbSpecial.Text = "Hanging" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .FirstLineIndent = -(Me.spnBy.Value)
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .LeftIndent = Me.spnContLeftIndent.Value - .FirstLineIndent
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBy. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .FirstLineIndent = Me.spnBy.Value
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContLeftIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .LeftIndent = Me.spnContLeftIndent.Value
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContRightIndent. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .RightIndent = Me.spnContRightIndent.Value
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbContLineSpacing.Bookmark. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                iLineSpaceRule = Me.cmbContLineSpacing.SelectedIndex
                .LineSpacingRule = iLineSpaceRule
                If iLineSpaceRule = Microsoft.Office.Interop.Word.WdLineSpacing.wdLineSpaceMultiple Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .LineSpacing = CurWordApp.LinesToPoints(Me.spnContAt.Value)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContAt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    .LineSpacing = Me.spnContAt.Value
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContSpaceAfter. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .SpaceAfter = Me.spnContSpaceAfter.Value
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnContSpaceBefore. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                .SpaceBefore = Me.spnContSpaceBefore.Value
                .WidowControl = CBool(Me.chkContWidowOrphan.CheckState)
                .KeepWithNext = CBool(Me.chkContKeepWithNext.CheckState)
                .KeepTogether = CBool(Me.chkContKeepLinesTogether.CheckState)
            End With

            '       font
            With .Font
                .Name = Me.cmbContFontName.Text
                If Me.cmbContFontSize.Text <> "-Undefined-" Then
                    .Size = CSng(Me.cmbContFontSize.Text)
                End If
            End With
        End With

        '   restore next cont style
        If bRestoreNext Then
            With styNext
                With .ParagraphFormat
                    .Alignment = iAlign
                    .FirstLineIndent = sFirstLineIndent
                    .LeftIndent = sLeftIndent
                    .RightIndent = sRightIndent
                    .LineSpacingRule = iLineSpaceRuleNext
                    .LineSpacing = sLineSpace
                    .SpaceAfter = sSpaceAfter
                    .SpaceBefore = sSpaceBefore
                    .WidowControl = bWidowCtl
                    .KeepWithNext = bKeepWithNext
                    .KeepTogether = bKeepTogether
                End With
                With .Font
                    .Name = xFont
                    .Size = sSize
                End With
            End With
        End If
    End Function

    Private Sub tbtnNumber_Click(sender As Object, e As EventArgs) Handles tbtnNumber.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            Me.tabControl1.SelectedTab = Me.tabPage1
            Me.tbtnNumber.Checked = True
            Me.tbtnParagraph.Checked = False
            Me.tbtnContStyle.Checked = False
            Me.Text = " Edit " & g_oCurScheme.DisplayName & " Scheme - Number"
            Me.rtfNumberFormat.Focus()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnParagraph_Click(sender As Object, e As EventArgs) Handles tbtnParagraph.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            Me.tabControl1.SelectedTab = Me.tabPage2
            Me.tbtnNumber.Checked = False
            Me.tbtnParagraph.Checked = True
            Me.tbtnContStyle.Checked = False
            Me.Text = " Edit " & g_oCurScheme.DisplayName & " Scheme - Paragraph"
            Me.spnNumberPosition.Focus()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnContStyle_Click(sender As Object, e As EventArgs) Handles tbtnContStyle.Click
        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
            Me.tabControl1.SelectedTab = Me.tabPage3
            Me.tbtnNumber.Checked = False
            Me.tbtnParagraph.Checked = False
            Me.tbtnContStyle.Checked = True
            Me.Text = " Edit " & g_oCurScheme.DisplayName & " Scheme - Continuation Style"
            Me.cmbContFontName.Focus()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tsLevel_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles tsLevel.ItemClicked
        Dim xName As String
        Dim i As Short
        Dim oTbtn As ToolStripButton
        Dim oItem As ToolStripItem

        Try
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

            '1/11/17 (dm) - validate spinners
            If Not ValidateSpinners() Then
                Exit Sub
            End If

            xName = e.ClickedItem.Name

            If (xName <> "cmdDeleteLevel") And (xName <> "cmdAddLevel") Then
                'button for selected level now remains enabled - exit if pressed
                If CInt(Strings.Right(xName, 1)) = m_iLevel Then
                    Exit Sub
                End If

                'restore image for previously selected level
                If m_iLevel > 0 Then
                    tsLevel.Items(m_iLevel - 1).Image = My.Resources.ResourceManager.GetObject(tsLevel.Items(m_iLevel - 1).Name.Substring(4))
                End If

                m_iLevel = CInt(Strings.Right(xName, 1))

                'change image for selected level
                tsLevel.Items(m_iLevel - 1).Image = My.Resources.ResourceManager.GetObject(tsLevel.Items(m_iLevel - 1).Name.Substring(4) & "Selected")

                'switch levels
                SwitchLevels()
                CurWordApp.ScreenUpdating = True
                CurWordApp.ScreenRefresh()

                'reselect previous control
                Try
                    With CType(Me.Controls(m_xPrevCtl), Object)
                        '12/8/16 - focus() is erring every time, so remmed out for now -
                        'obviously, this try block should be refined
                        'If m_xPrevCtl = "optDirectFormats" Then
                        '    If Not Me.optDirectFormats.Checked Then
                        '        Me.optStyleFormats.Focus()
                        '    End If
                        'ElseIf m_xPrevCtl = "optStyleFormats" Then
                        '    If Not Me.optStyleFormats.Checked Then
                        '        Me.optDirectFormats.Focus()
                        '    End If
                        'Else
                        '    .Focus()
                        'End If
                        If m_xPrevCtl <> "rtfNumberFormat" Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object Me.Controls(m_xPrevCtl).SelLength. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .SelectionLength = Len(.Text)
                        Else
                            Me.rtfNumberFormat.SelectionStart = 0
                            Me.rtfNumberFormat.SelectionLength = 0
                        End If
                    End With
                Catch
                End Try
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdZoom_Click(sender As Object, e As EventArgs) Handles cmdZoom.Click
        Dim Preview As MacPacNumbering.LMP.Numbering.cPreview
        Dim bZoomIn As Boolean

        Try
            If Not m_bFormInit Then
                If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()

                '9.9.6006 (GLOG 5549)
                If g_iWordVersion = mpWordVersions.mpWordVersion_2013 Then
                    'workaround for ruler not reappearing in Word 2013
                    'with modal dialog visible
                    m_bZoomClicked = True
                    Me.Hide()
                Else
                    'moved code into shared method outside of form
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    bZoomIn = (Me.cmdZoom.CheckState = System.Windows.Forms.CheckState.Checked)
                    SetPreviewZoom(bZoomIn)
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnNumberPosition_Change(Sender As Object, e As EventArgs) Handles spnNumberPosition.Change
        Try
            If m_bStopChangeEvent Then
                Exit Sub
            End If
            If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then _
                g_docPreview.Activate()
            m_bLevelDirty = True
            m_bParaDirty = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnNumberPosition_Leave(sender As Object, e As EventArgs) Handles spnNumberPosition.Leave
        Try
            m_xPrevCtl = "spnNumberPosition"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnNumberPosition_Validated(sender As Object, e As EventArgs) Handles spnNumberPosition.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnAt_Validated(sender As Object, e As EventArgs) Handles spnAt.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnSpaceAfter_Validated(sender As Object, e As EventArgs) Handles spnSpaceAfter.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnSpaceBefore_Validated(sender As Object, e As EventArgs) Handles spnSpaceBefore.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    'Validate Spinner Text and keep focus if invalid
    Private Sub ValidatingSpinnerText(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles spnSpaceAfter.Validating, spnSpaceBefore.Validating, spnTextPosition.Validating, spnTabPosition.Validating, spnRightIndent.Validating, spnNumberPosition.Validating, spnContSpaceBefore.Validating, spnContSpaceAfter.Validating, spnContRightIndent.Validating, spnContLeftIndent.Validating, spnContAt.Validating, spnBy.Validating, spnAt.Validating
        Dim oSpinner As SpinTextInternational = sender
        Try
            If Not oSpinner.IsValid Then e.Cancel = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub lstPrevLevels_DoubleClick(sender As Object, e As EventArgs) Handles lstPrevLevels.DoubleClick
        Try
            If lstPrevLevels.SelectedIndices.Count > 0 Then
                If CurWordApp.ActiveDocument.Name <> g_docPreview.Name Then g_docPreview.Activate()
                InsertPrevLevel()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Function ValidateSpinners() As Boolean
        If Not Me.spnAt.Validate() Then
            Me.spnAt.Focus()
            Return False
        ElseIf Not Me.spnBy.Validate() Then
            Me.spnBy.Focus()
            Return False
        ElseIf Not Me.spnContAt.Validate() Then
            Me.spnContAt.Focus()
            Return False
        ElseIf Not Me.spnContLeftIndent.Validate() Then
            Me.spnContLeftIndent.Focus()
            Return False
        ElseIf Not Me.spnContRightIndent.Validate() Then
            Me.spnContRightIndent.Focus()
            Return False
        ElseIf Not Me.spnContSpaceAfter.Validate() Then
            Me.spnContSpaceAfter.Focus()
            Return False
        ElseIf Not Me.spnContSpaceBefore.Validate() Then
            Me.spnContSpaceBefore.Focus()
            Return False
        ElseIf Not Me.spnNumberPosition.Validate() Then
            Me.spnNumberPosition.Focus()
            Return False
        ElseIf Not Me.spnRightIndent.Validate() Then
            Me.spnRightIndent.Focus()
            Return False
        ElseIf Not Me.spnSpaceAfter.Validate() Then
            Me.spnSpaceAfter.Focus()
            Return False
        ElseIf Not Me.spnSpaceBefore.Validate() Then
            Me.spnSpaceBefore.Focus()
            Return False
        ElseIf Not Me.spnTabPosition.Validate() Then
            Me.spnTabPosition.Focus()
            Return False
        ElseIf Not Me.spnTextPosition.Validate() Then
            Me.spnTextPosition.Focus()
            Return False
        End If

        ValidateSpinners = True
    End Function

    Private Sub spnRightIndent_Validated(sender As Object, e As EventArgs) Handles spnRightIndent.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTabPosition_Validated(sender As Object, e As EventArgs) Handles spnTabPosition.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTextPosition_Validated(sender As Object, e As EventArgs) Handles spnTextPosition.Validated
        Try
            RefreshLevel()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub ValidatingFontSize(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cmbNumFontSize.Validating, cmbFontSize.Validating, cmbContFontSize.Validating
        'GLOG 15978: Allow manually entered font sizes
        Try
            Dim xVal As String = sender.Text
            Dim sVal As Single = 0
            Dim iOut As Integer
            Dim xMsg As String = ""
            If xVal.ToUpper() = "-UNDEFINED-" Then
                Return
            ElseIf Not IsNumeric(xVal) Then
                xMsg = "Numeric value required"
            Else
                Try
                    sVal = Single.Parse(xVal)
                Finally
                End Try
                If (sVal < 1 Or sVal > 1638) Then
                    xMsg = "Value must be between 1 and 1638"
                ElseIf Not Integer.TryParse((sVal * 2).ToString(), iOut) Then
                    xMsg = "Value must be whole number or half-point increment"
                End If
            End If
            If xMsg <> "" Then
                MessageBox.Show(xMsg, "TSG Numbering", MessageBoxButtons.OK, MessageBoxIcon.Information)
                e.Cancel = True
            End If

        Catch oE As Exception
            [Error].Show(oE)
        End Try

    End Sub
End Class