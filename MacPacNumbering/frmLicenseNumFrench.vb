Option Strict Off
Option Explicit On

Imports LMP

Friend Class frmLicenseNumFrench
	Inherits System.Windows.Forms.Form
	
	Private m_xName As String
	Private m_bCancelled As Boolean
	Private m_xMessage As String
	
	
	Public Property Cancelled() As Boolean
		Get
			Cancelled = m_bCancelled
		End Get
		Set(ByVal Value As Boolean)
			m_bCancelled = Value
		End Set
	End Property
	
	
	Public Property Key() As String
		Get
			Key = m_xName
		End Get
		Set(ByVal Value As String)
			m_xName = Value
		End Set
	End Property
	
	
	Public Property Message() As String
		Get
			Message = m_xMessage
		End Get
		Set(ByVal Value As String)
			m_xMessage = Value
			Me.Label1.Text = Value
		End Set
	End Property
	
    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            m_bCancelled = False
            Me.Key = Me.txtKey.Text
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub frmLicenseNumFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		m_bCancelled = True
	End Sub
	
    Private Sub txtCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCancel.Click
        Try
            m_bCancelled = True
            Me.Key = CStr(Nothing)
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	'UPGRADE_WARNING: Event txtKey.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtKey_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKey.TextChanged
        Try
            '   enable OK btn only when there
            '   is text in the Name txt box
            If txtKey.Text = "" Then
                btnOK.Enabled = False
            Else
                btnOK.Enabled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub txtKey_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKey.Enter
        Try
            With Me.txtKey
                .SelectionStart = 0
                .SelectionLength = Len(.Text)
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class