Option Strict Off
Option Explicit On

Imports LMP.Numbering.Base.cStrings
Imports MacPacNumbering.LMP.Numbering.mpDialog_VB
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports LMP

Friend Class frmLineSpacing
	Inherits System.Windows.Forms.Form
	
	Private m_bCancelled As Boolean
	
	Public ReadOnly Property Cancelled() As Boolean
		Get
			Cancelled = m_bCancelled
		End Get
	End Property
	
    Private Sub cmbLineSpacing_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbLineSpacing.ValueChanged
        Try
            Select Case Me.cmbLineSpacing.Text
                Case "At Least", "Exactly"
                    '           at least, exactly
                    Me.lblAt.Enabled = True
                    Me.txtAt.Enabled = True
                    Me.udAt.Enabled = True
                    Me.lblAtUnits.Visible = True
                    Me.txtAt.Text = "12"
                    Me.udAt.Value = 12
                Case "Multiple"
                    '           multiple
                    Me.lblAt.Enabled = True
                    Me.txtAt.Enabled = True
                    Me.udAt.Enabled = True
                    Me.lblAtUnits.Visible = False
                    Me.txtAt.Text = "3"
                    Me.udAt.Value = 3
                Case Else
                    Me.lblAt.Enabled = False
                    Me.txtAt.Enabled = False
                    Me.udAt.Enabled = False
                    Me.lblAtUnits.Visible = False
                    Me.txtAt.Text = ""
            End Select
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            m_bCancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Try
            If Me.txtAt.Text <> "" Then
                If Me.cmbLineSpacing.Text = "Multiple" Then
                    If Not bValidateLinesInput(Me.txtAt) Then
                        Return
                    End If
                Else
                    If Not bValidateSpaceInput(Me.txtAt) Then
                        Return
                    End If
                End If
            End If

            If Me.txtSpaceBefore.Text <> "-Use Current-" Then
                If Not bValidateSpaceInput(Me.txtSpaceBefore) Then
                    Return
                End If
            End If

            If Me.txtSpaceAfter.Text <> "-Use Current-" Then
                If Not bValidateSpaceInput(Me.txtSpaceAfter) Then
                    Return
                End If
            End If

            m_bCancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmLineSpacing_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                cmdCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

	Private Sub frmLineSpacing_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'adjust for scaling
        Me.txtAt.Width = (Me.udAt.Left + Me.udAt.Width) - Me.txtAt.Left - 18
        Me.txtSpaceBefore.Width = (Me.udSpaceBefore.Left + Me.udSpaceBefore.Width) - Me.txtSpaceBefore.Left - 18
        Me.txtSpaceAfter.Width = (Me.udSpaceAfter.Left + Me.udSpaceAfter.Width) - Me.txtSpaceAfter.Left - 18

        'adjust for scaling
        Dim sFactorIncrease As Single = CScaling.GetScalingFactor()
        If sFactorIncrease > 1 Then
            Me.cmdOK.Width = Me.cmdOK.Width * sFactorIncrease
            Me.cmdCancel.Width = Me.cmdCancel.Width * sFactorIncrease
            Me.cmdOK.Height = Me.cmdOK.Height + (sFactorIncrease - 1) * 40
            Me.cmdCancel.Height = Me.cmdCancel.Height + (sFactorIncrease - 1) * 40
        End If

        Dim xarLineSpacing(6, 1) As String

        Try
            xarLineSpacing(0, 0) = "-Use Current-"
            xarLineSpacing(0, 1) = "-Use Current-"
            xarLineSpacing(1, 0) = "Single"
            xarLineSpacing(1, 1) = "Single"
            xarLineSpacing(2, 0) = "1.5 Lines"
            xarLineSpacing(2, 1) = "1.5 Lines"
            xarLineSpacing(3, 0) = "Double"
            xarLineSpacing(3, 1) = "Double"
            xarLineSpacing(4, 0) = "At Least"
            xarLineSpacing(4, 1) = "At Least"
            xarLineSpacing(5, 0) = "Exactly"
            xarLineSpacing(5, 1) = "Exactly"
            xarLineSpacing(6, 0) = "Multiple"
            xarLineSpacing(6, 1) = "Multiple"
            Me.cmbLineSpacing.SetList(xarLineSpacing)

            'initialize
            Me.cmbLineSpacing.SelectedIndex = 0
            Me.txtSpaceAfter.Text = "-Use Current-"
            Me.txtSpaceBefore.Text = "-Use Current-"

            'GLOG 5689
            m_bCancelled = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub txtAt_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAt.Leave
        Try
            If Me.txtAt.Text = "" Then
                Me.txtAt.Text = CStr(Me.udAt.Value)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    'UPGRADE_WARNING: Event txtSpaceAfter.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtSpaceAfter_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSpaceAfter.TextChanged
        Try
            Dim iValue As Integer
            With Me.txtSpaceAfter
                If Len(.Text) = 0 Then
                    .Text = "-Use Current-"
                    .SelectionStart = 0
                    .SelectionLength = 13
                ElseIf .Text <> "-Use Current-" Then
                    .Text = .Text.Replace("-Use Current-", "")
                    iValue = CInt(Me.txtSpaceAfter.Text)
                    If (iValue >= Me.udSpaceAfter.Minimum) And (iValue <= Me.udSpaceAfter.Maximum) Then
                        Me.udSpaceAfter.Value = CInt(.Text)
                        .SelectionStart = Len(.Text)
                    End If
                End If
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub txtSpaceAfter_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSpaceAfter.Enter
        Try
            bEnsureSelectedContent((Me.txtSpaceAfter))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub


    'UPGRADE_WARNING: Event txtSpaceBefore.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtSpaceBefore_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSpaceBefore.TextChanged
        Try
            Dim iValue As Integer
            With Me.txtSpaceBefore
                If Len(.Text) = 0 Then
                    .Text = "-Use Current-"
                    .SelectionStart = 0
                    .SelectionLength = 13
                ElseIf .Text <> "-Use Current-" Then
                    .Text = .Text.Replace("-Use Current-", "")
                    iValue = CInt(Me.txtSpaceBefore.Text)
                    If (iValue >= Me.udSpaceBefore.Minimum) And (iValue <= Me.udSpaceBefore.Maximum) Then
                        Me.udSpaceBefore.Value = CInt(.Text)
                        .SelectionStart = Len(.Text)
                    End If
                End If
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtSpaceBefore_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSpaceBefore.Enter
        Try
            bEnsureSelectedContent((Me.txtSpaceBefore))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub txtSpaceAfter_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSpaceAfter.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Try
            If Me.txtSpaceAfter.Text = "-Use Current-" Then Me.txtSpaceAfter.Text = ""
            If (KeyCode = System.Windows.Forms.Keys.Up) Or (KeyCode = System.Windows.Forms.Keys.Down) Then
                ChangeUpDown((Me.udSpaceAfter), (KeyCode = System.Windows.Forms.Keys.Up), 6, 6, 6)
                udSpaceAfter_Click(udSpaceAfter, New System.EventArgs())
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtSpaceAfter_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtSpaceAfter.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            KeyAscii = AllowDigitsOnly(KeyAscii, (Me.txtSpaceAfter), False, 4)
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtSpaceBefore_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtSpaceBefore.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Try
            If Me.txtSpaceBefore.Text = "-Use Current-" Then Me.txtSpaceBefore.Text = ""
            If (KeyCode = System.Windows.Forms.Keys.Up) Or (KeyCode = System.Windows.Forms.Keys.Down) Then
                ChangeUpDown((Me.udSpaceBefore), (KeyCode = System.Windows.Forms.Keys.Up), 6, 6, 6)
                udSpaceBefore_Click(udSpaceBefore, New System.EventArgs())
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtSpaceBefore_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtSpaceBefore.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            KeyAscii = AllowDigitsOnly(KeyAscii, (Me.txtSpaceBefore), False, 4)
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub udSpaceAfter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles udSpaceAfter.Click
        Try
            If Me.txtSpaceAfter.Text = "-Use Current-" Then Me.txtSpaceAfter.Text = "0"
            Me.txtSpaceAfter.Text = CStr(Me.udSpaceAfter.Value)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub udSpaceBefore_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles udSpaceBefore.Click
        Try
            If Me.txtSpaceBefore.Text = "-Use Current-" Then Me.txtSpaceBefore.Text = "0"
            Me.txtSpaceBefore.Text = CStr(Me.udSpaceBefore.Value)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub txtAt_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAt.Enter
        Try
            bEnsureSelectedContent((Me.txtAt))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtAt_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtAt.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Try
            If (KeyCode = System.Windows.Forms.Keys.Up) Or (KeyCode = System.Windows.Forms.Keys.Down) Then
                ChangeUpDown((Me.udAt), (KeyCode = System.Windows.Forms.Keys.Up), 1, 1, 1)
                udAt_Click(udAt, New System.EventArgs())
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub txtAt_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtAt.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            KeyAscii = AllowDigitsOnly(KeyAscii, (Me.txtAt), False, 4)
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub udAt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles udAt.Click
        Try
            Me.txtAt.Text = CStr(Me.udAt.Value)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    '	Private Sub cmbLineSpacing_Mismatch(ByVal eventSender As System.Object, ByVal eventArgs As AxTrueDBList60.TrueDBComboEvents_MismatchEvent) Handles cmbLineSpacing.Mismatch
    '		On Error GoTo ProcError
    '		CorrectTDBComboMismatch((Me.cmbLineSpacing), eventArgs.Reposition)
    '		Exit Sub
    'ProcError: 
    '		RaiseError("frmLineSpacing.cmbLineSpacing_Mismatch")
    '		Exit Sub
    '	End Sub

    Private Sub txtAt_TextChanged(sender As Object, e As EventArgs) Handles txtAt.TextChanged
        Try
            Dim iValue As Integer
            If Me.txtAt.Text <> "" Then
                iValue = CInt(Me.txtAt.Text)
                If (iValue >= Me.udAt.Minimum) And (iValue <= Me.udAt.Maximum) Then
                    Me.udAt.Value = CInt(Me.txtAt.Text)
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtSpaceBefore_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtSpaceBefore.Validating
        Try
            If Me.txtSpaceBefore.Text <> "-Use Current-" Then
                bValidateSpaceInput(Me.txtSpaceBefore)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtSpaceAfter_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtSpaceAfter.Validating
        Try
            If Me.txtSpaceAfter.Text <> "-Use Current-" Then
                bValidateSpaceInput(Me.txtSpaceAfter)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtAt_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles txtAt.Validating
        Try
            If Me.txtAt.Text = "" Then
                Me.txtAt.Text = CStr(Me.udAt.Value)
            ElseIf Me.cmbLineSpacing.Text = "Multiple" Then
                bValidateLinesInput(Me.txtAt)
            Else
                bValidateSpaceInput(Me.txtAt)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class