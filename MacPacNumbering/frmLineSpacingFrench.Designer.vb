<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLineSpacingFrench
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txtAt As System.Windows.Forms.TextBox
    Public WithEvents txtSpaceAfter As System.Windows.Forms.TextBox
    Public WithEvents txtSpaceBefore As System.Windows.Forms.TextBox
    Public WithEvents udSpaceAfter As System.Windows.Forms.NumericUpDown
    Public WithEvents udSpaceBefore As System.Windows.Forms.NumericUpDown
    Public WithEvents udAt As System.Windows.Forms.NumericUpDown
    Public WithEvents lblAtUnits As System.Windows.Forms.Label
    Public WithEvents lblAt As System.Windows.Forms.Label
    Public WithEvents lblSpaceAfter As System.Windows.Forms.Label
    Public WithEvents lblSpaceAfterUnits As System.Windows.Forms.Label
    Public WithEvents lblSpaceBeforeUnits As System.Windows.Forms.Label
    Public WithEvents lblSpaceBefore As System.Windows.Forms.Label
    Public WithEvents lblLineSpacing As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLineSpacingFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtAt = New System.Windows.Forms.TextBox()
        Me.txtSpaceAfter = New System.Windows.Forms.TextBox()
        Me.txtSpaceBefore = New System.Windows.Forms.TextBox()
        Me.udSpaceAfter = New System.Windows.Forms.NumericUpDown()
        Me.udSpaceBefore = New System.Windows.Forms.NumericUpDown()
        Me.udAt = New System.Windows.Forms.NumericUpDown()
        Me.lblAtUnits = New System.Windows.Forms.Label()
        Me.lblAt = New System.Windows.Forms.Label()
        Me.lblSpaceAfter = New System.Windows.Forms.Label()
        Me.lblSpaceAfterUnits = New System.Windows.Forms.Label()
        Me.lblSpaceBeforeUnits = New System.Windows.Forms.Label()
        Me.lblSpaceBefore = New System.Windows.Forms.Label()
        Me.lblLineSpacing = New System.Windows.Forms.Label()
        Me.cmbLineSpacing = New mpnControls.ComboBox()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.cmdCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        CType(Me.udSpaceAfter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udSpaceBefore, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.udAt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtAt
        '
        Me.txtAt.AcceptsReturn = True
        Me.txtAt.BackColor = System.Drawing.SystemColors.Window
        Me.txtAt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAt.Enabled = False
        Me.txtAt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAt.Location = New System.Drawing.Point(211, 35)
        Me.txtAt.MaxLength = 0
        Me.txtAt.Multiline = True
        Me.txtAt.Name = "txtAt"
        Me.txtAt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtAt.Size = New System.Drawing.Size(42, 23)
        Me.txtAt.TabIndex = 3
        '
        'txtSpaceAfter
        '
        Me.txtSpaceAfter.AcceptsReturn = True
        Me.txtSpaceAfter.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpaceAfter.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpaceAfter.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpaceAfter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSpaceAfter.Location = New System.Drawing.Point(157, 104)
        Me.txtSpaceAfter.MaxLength = 0
        Me.txtSpaceAfter.Multiline = True
        Me.txtSpaceAfter.Name = "txtSpaceAfter"
        Me.txtSpaceAfter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpaceAfter.Size = New System.Drawing.Size(97, 23)
        Me.txtSpaceAfter.TabIndex = 11
        '
        'txtSpaceBefore
        '
        Me.txtSpaceBefore.AcceptsReturn = True
        Me.txtSpaceBefore.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpaceBefore.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpaceBefore.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpaceBefore.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSpaceBefore.Location = New System.Drawing.Point(15, 104)
        Me.txtSpaceBefore.MaxLength = 0
        Me.txtSpaceBefore.Multiline = True
        Me.txtSpaceBefore.Name = "txtSpaceBefore"
        Me.txtSpaceBefore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpaceBefore.Size = New System.Drawing.Size(97, 23)
        Me.txtSpaceBefore.TabIndex = 7
        '
        'udSpaceAfter
        '
        Me.udSpaceAfter.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udSpaceAfter.Increment = New Decimal(New Integer() {6, 0, 0, 0})
        Me.udSpaceAfter.Location = New System.Drawing.Point(253, 104)
        Me.udSpaceAfter.Maximum = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.udSpaceAfter.Name = "udSpaceAfter"
        Me.udSpaceAfter.Size = New System.Drawing.Size(18, 23)
        Me.udSpaceAfter.TabIndex = 12
        Me.udSpaceAfter.TabStop = False
        '
        'udSpaceBefore
        '
        Me.udSpaceBefore.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udSpaceBefore.Increment = New Decimal(New Integer() {6, 0, 0, 0})
        Me.udSpaceBefore.Location = New System.Drawing.Point(111, 104)
        Me.udSpaceBefore.Maximum = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.udSpaceBefore.Name = "udSpaceBefore"
        Me.udSpaceBefore.Size = New System.Drawing.Size(18, 23)
        Me.udSpaceBefore.TabIndex = 9
        Me.udSpaceBefore.TabStop = False
        '
        'udAt
        '
        Me.udAt.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.udAt.Location = New System.Drawing.Point(252, 35)
        Me.udAt.Maximum = New Decimal(New Integer() {1584, 0, 0, 0})
        Me.udAt.Name = "udAt"
        Me.udAt.Size = New System.Drawing.Size(18, 23)
        Me.udAt.TabIndex = 4
        Me.udAt.TabStop = False
        '
        'lblAtUnits
        '
        Me.lblAtUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblAtUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAtUnits.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAtUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAtUnits.Location = New System.Drawing.Point(270, 40)
        Me.lblAtUnits.Name = "lblAtUnits"
        Me.lblAtUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAtUnits.Size = New System.Drawing.Size(21, 25)
        Me.lblAtUnits.TabIndex = 5
        Me.lblAtUnits.Text = "pts"
        '
        'lblAt
        '
        Me.lblAt.BackColor = System.Drawing.SystemColors.Control
        Me.lblAt.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAt.Enabled = False
        Me.lblAt.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAt.Location = New System.Drawing.Point(211, 20)
        Me.lblAt.Name = "lblAt"
        Me.lblAt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAt.Size = New System.Drawing.Size(39, 20)
        Me.lblAt.TabIndex = 2
        Me.lblAt.Text = "D&e:"
        '
        'lblSpaceAfter
        '
        Me.lblSpaceAfter.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpaceAfter.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpaceAfter.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpaceAfter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpaceAfter.Location = New System.Drawing.Point(158, 89)
        Me.lblSpaceAfter.Name = "lblSpaceAfter"
        Me.lblSpaceAfter.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpaceAfter.Size = New System.Drawing.Size(83, 20)
        Me.lblSpaceAfter.TabIndex = 10
        Me.lblSpaceAfter.Text = "Espace &apr�s:"
        '
        'lblSpaceAfterUnits
        '
        Me.lblSpaceAfterUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpaceAfterUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpaceAfterUnits.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpaceAfterUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpaceAfterUnits.Location = New System.Drawing.Point(270, 109)
        Me.lblSpaceAfterUnits.Name = "lblSpaceAfterUnits"
        Me.lblSpaceAfterUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpaceAfterUnits.Size = New System.Drawing.Size(21, 25)
        Me.lblSpaceAfterUnits.TabIndex = 13
        Me.lblSpaceAfterUnits.Text = "pts"
        '
        'lblSpaceBeforeUnits
        '
        Me.lblSpaceBeforeUnits.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpaceBeforeUnits.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpaceBeforeUnits.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpaceBeforeUnits.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpaceBeforeUnits.Location = New System.Drawing.Point(128, 109)
        Me.lblSpaceBeforeUnits.Name = "lblSpaceBeforeUnits"
        Me.lblSpaceBeforeUnits.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpaceBeforeUnits.Size = New System.Drawing.Size(21, 25)
        Me.lblSpaceBeforeUnits.TabIndex = 8
        Me.lblSpaceBeforeUnits.Text = "pts"
        '
        'lblSpaceBefore
        '
        Me.lblSpaceBefore.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpaceBefore.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpaceBefore.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpaceBefore.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpaceBefore.Location = New System.Drawing.Point(15, 89)
        Me.lblSpaceBefore.Name = "lblSpaceBefore"
        Me.lblSpaceBefore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpaceBefore.Size = New System.Drawing.Size(82, 19)
        Me.lblSpaceBefore.TabIndex = 6
        Me.lblSpaceBefore.Text = "Espac&e avant:"
        '
        'lblLineSpacing
        '
        Me.lblLineSpacing.BackColor = System.Drawing.SystemColors.Control
        Me.lblLineSpacing.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLineSpacing.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLineSpacing.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLineSpacing.Location = New System.Drawing.Point(16, 21)
        Me.lblLineSpacing.Name = "lblLineSpacing"
        Me.lblLineSpacing.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLineSpacing.Size = New System.Drawing.Size(81, 20)
        Me.lblLineSpacing.TabIndex = 0
        Me.lblLineSpacing.Text = "&Interligne:"
        '
        'cmbLineSpacing
        '
        Me.cmbLineSpacing.AllowEmptyValue = False
        Me.cmbLineSpacing.AutoSize = True
        Me.cmbLineSpacing.Borderless = False
        Me.cmbLineSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbLineSpacing.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLineSpacing.IsDirty = False
        Me.cmbLineSpacing.LimitToList = True
        Me.cmbLineSpacing.Location = New System.Drawing.Point(15, 36)
        Me.cmbLineSpacing.MaxDropDownItems = 8
        Me.cmbLineSpacing.Name = "cmbLineSpacing"
        Me.cmbLineSpacing.SelectedIndex = -1
        Me.cmbLineSpacing.SelectedValue = Nothing
        Me.cmbLineSpacing.SelectionLength = 0
        Me.cmbLineSpacing.SelectionStart = 0
        Me.cmbLineSpacing.Size = New System.Drawing.Size(167, 23)
        Me.cmbLineSpacing.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbLineSpacing.SupportingValues = ""
        Me.cmbLineSpacing.TabIndex = 1
        Me.cmbLineSpacing.Tag2 = Nothing
        Me.cmbLineSpacing.Value = ""
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdCancel, Me.toolStripSeparator1, Me.cmdOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 154)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(302, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 16
        Me.tsButtons.Text = "ToolStrip1"
        '
        'cmdCancel
        '
        Me.cmdCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdCancel.AutoSize = False
        Me.cmdCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.cmdCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdCancel.Size = New System.Drawing.Size(60, 45)
        Me.cmdCancel.Text = "Annuler"
        Me.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'cmdOK
        '
        Me.cmdOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdOK.AutoSize = False
        Me.cmdOK.Image = Global.MacPacNumbering.My.Resources.Resources.OK
        Me.cmdOK.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdOK.Size = New System.Drawing.Size(60, 45)
        Me.cmdOK.Text = "O&K"
        Me.cmdOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 49)
        '
        'frmLineSpacingFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(302, 203)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.cmbLineSpacing)
        Me.Controls.Add(Me.txtAt)
        Me.Controls.Add(Me.txtSpaceAfter)
        Me.Controls.Add(Me.txtSpaceBefore)
        Me.Controls.Add(Me.udSpaceAfter)
        Me.Controls.Add(Me.udSpaceBefore)
        Me.Controls.Add(Me.udAt)
        Me.Controls.Add(Me.lblAtUnits)
        Me.Controls.Add(Me.lblAt)
        Me.Controls.Add(Me.lblSpaceAfter)
        Me.Controls.Add(Me.lblSpaceAfterUnits)
        Me.Controls.Add(Me.lblSpaceBeforeUnits)
        Me.Controls.Add(Me.lblSpaceBefore)
        Me.Controls.Add(Me.lblLineSpacing)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(5, 29)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLineSpacingFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "###"
        CType(Me.udSpaceAfter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udSpaceBefore, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.udAt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbLineSpacing As mpnControls.ComboBox
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region
End Class