Option Strict Off
Option Explicit On

Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mpDialog_VB
Imports LMP

Friend Class frmNameScheme
	Inherits System.Windows.Forms.Form
	Private m_bCancelled As Boolean
	Private m_bAliasEdited As Boolean
	Public ReadOnly Property Cancelled() As Boolean
		Get
			Cancelled = m_bCancelled
		End Get
	End Property
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            Me.Hide()
            Me.Refresh()
            m_bCancelled = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Try
            'validate info
            Me.DialogResult = System.Windows.Forms.DialogResult.None
            m_bCancelled = False
            If Len(Me.txtName.Text) = 0 Then
                xMsg = "Scheme name is required information."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                Me.txtAlias.Focus()
                Exit Sub
            ElseIf Len(Me.txtAlias.Text) = 0 Then
                xMsg = "Scheme display name is required information."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                Me.txtAlias.Focus()
                Exit Sub
            ElseIf Not bStyleNameIsValid(mpPrefix & Me.txtName.Text, True) Then
                With Me.txtName
                    .SelectionStart = 0
                    .SelectionLength = Len(.Text)
                    .Focus()
                End With
                Exit Sub
            ElseIf Not bSchemeNameIsValid((Me.txtAlias).Text, True) Then
                With Me.txtAlias
                    .SelectionStart = 0
                    .SelectionLength = Len(.Text)
                    .Focus()
                End With
                Exit Sub
            Else
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Hide()
                System.Windows.Forms.Application.DoEvents()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	'UPGRADE_WARNING: Form event frmNameScheme.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmNameScheme_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
        Try
            Me.txtAlias.Focus()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	'UPGRADE_WARNING: Event txtAlias.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtAlias_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAlias.TextChanged
        Try
            Me.cmdOK.Enabled = (Len(Me.txtAlias.Text) > 0) And (Len(Me.txtName.Text) > 0)
            '   set flag based on whether the 2 names are different-
            '   this will be used to prevent changes in scheme
            '   name from getting mirrored in alias - once set
            '   no mirroring can occur
            m_bAliasEdited = m_bAliasEdited Or (Me.txtAlias.Text <> Me.txtName.Text)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub txtAlias_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAlias.Enter
        Try
            bEnsureSelectedContent((Me.txtAlias), True)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	'UPGRADE_WARNING: Event txtName.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtName_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.TextChanged
        Try
            If Not m_bAliasEdited Then Me.txtAlias.Text = Me.txtName.Text
            Me.cmdOK.Enabled = (Len(Me.txtAlias.Text) > 0) And (Len(Me.txtName.Text) > 0)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub txtName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.Enter
        Try
            bEnsureSelectedContent((Me.txtName), True)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	Private Sub txtName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtName.Leave
        Try
            If (txtName.Text <> "") And (txtAlias.Text = "") Then txtAlias.Text = txtName.Text
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class