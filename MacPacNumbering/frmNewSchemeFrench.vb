Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpDialog_VB
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cNumTOC
Imports LMP

Friend Class frmNewSchemeFrench
    Inherits System.Windows.Forms.Form
    Private bNoChange As Boolean
    Private m_bCancelled As Boolean
    Private m_bAliasEdited As Boolean
    Private m_xDefaultBasedOnScheme As String
    Private m_iDefaultBasedOnSchemeType As mpSchemeTypes
    Private m_xarTOCSchemes(,) As String

    Public ReadOnly Property BasedOnScheme() As String
        Get
            On Error Resume Next

            'GLOG 5531 - add handling for favorites
            If Me.tvwSchemes.SelectedNode.Parent.Text = mpFavoriteSchemes Then
                BasedOnScheme = Mid(Me.tvwSchemes.SelectedNode.Name, 4)
            Else
                BasedOnScheme = Mid(Me.tvwSchemes.SelectedNode.Name, 2)
            End If
        End Get
    End Property

    Public ReadOnly Property BasedOnSchemeDisplayName() As String
        Get
            On Error Resume Next
            BasedOnSchemeDisplayName = Me.tvwSchemes.SelectedNode.Text
        End Get
    End Property

    Public ReadOnly Property BasedOnSchemeType() As mpSchemeTypes
        Get
            Dim iIndex As Short 'GLOG 5531
            Dim oParent As System.Windows.Forms.TreeNode

            oParent = Me.tvwSchemes.SelectedNode.Parent

            If oParent Is Nothing Then
                BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Category
            Else
                'GLOG 5531 - add handling for favorites
                iIndex = oParent.Index
                Select Case iIndex
                    Case 0
                        BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Document
                    Case 1
                        If VB.Left(Me.tvwSchemes.SelectedNode.Name, 3) = CStr(mpFavoritePublic) Then
                            BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Public
                        Else
                            BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Private
                        End If
                    Case 2
                        BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Private
                    Case Else
                        BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Public
                End Select

                '   this is a workaround for missing public node
                If g_bIsAdmin And (BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Public) Then BasedOnSchemeType = mpSchemeTypes.mpSchemeType_Private
            End If
        End Get
    End Property

    Public Property DefaultBasedOnScheme() As String
        Get
            DefaultBasedOnScheme = m_xDefaultBasedOnScheme
        End Get
        Set(ByVal Value As String)
            m_xDefaultBasedOnScheme = Value
        End Set
    End Property

    Public Property DefaultBasedOnSchemeType() As mpSchemeTypes
        Get
            DefaultBasedOnSchemeType = m_iDefaultBasedOnSchemeType
        End Get
        Set(ByVal Value As mpSchemeTypes)
            m_iDefaultBasedOnSchemeType = Value
        End Set
    End Property

    ReadOnly Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
    End Property

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            m_bCancelled = True
            Me.Hide()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Dim i As Short
        Dim iNumSchemes As Short
        Dim xNewScheme As String
        Dim xNewAlias As String
        Dim bStyleNameValid As Boolean
        Dim bSchemeNameValid As Boolean
        Dim bIsValidBasis As Boolean

        Try
            xNewScheme = Me.txtStyleName.Text
            xNewAlias = Me.txtSchemeName.Text

            bSchemeNameValid = bSchemeNameIsValid(xNewAlias, True)
            If bSchemeNameValid Then
                bStyleNameValid = bStyleNameIsValid(xNewScheme, True)
            End If

            Try
                bIsValidBasis = Not (Me.tvwSchemes.SelectedNode.Parent Is Nothing)
            Catch
            End Try

            '   verify that name and alias are valid
            If bStyleNameValid And bSchemeNameValid Then
                If Not bIsValidBasis Then
                    Me.tvwSchemes.Select()
                    xMsg = "Veuillez s�lectionner un th�me valide 'Bas� sur'."
                    MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                    'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                    Me.DialogResult = System.Windows.Forms.DialogResult.None
                Else
                    m_bCancelled = False
                    Me.Hide()
                    CurWordApp.ScreenRefresh()
                End If
            ElseIf Not bSchemeNameValid Then
                With Me.txtSchemeName
                    .SelectionStart = 0
                    .SelectionLength = Len(.Text)
                    .Select()
                End With
                'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.DialogResult = System.Windows.Forms.DialogResult.None
            Else
                With Me.txtStyleName
                    .SelectionStart = 0
                    .SelectionLength = Len(.Text)
                    .Select()
                End With
                'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Me.DialogResult = System.Windows.Forms.DialogResult.None
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmNewSchemeFrench_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                cmdCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmNewSchemeFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim bTOCInstalled As Boolean
        Dim xarLevels(,) As String

        Try
            m_bCancelled = True

            'adjust for scaling
            Dim sFactorIncrease As Single = CScaling.GetScalingFactor()
            Me.tvwSchemes.ItemHeight = 18 + (sFactorIncrease - 1) * 14
            If sFactorIncrease > 1 Then
                Me.cmdOK.Width = Me.cmdOK.Width * sFactorIncrease
                Me.cmdCancel.Width = Me.cmdCancel.Width * sFactorIncrease
                Me.cmdOK.Height = Me.cmdOK.Height + (sFactorIncrease - 1) * 40
                Me.cmdCancel.Height = Me.cmdCancel.Height + (sFactorIncrease - 1) * 40
            End If

            Me.cmbTOCScheme.Visible = g_bAllowTOCLink
            Me.lblTOCScheme.Visible = g_bAllowTOCLink

            If g_bAllowTOCLink Then
                '       first ensure TOC link - may not be there if
                '       Word was started with a protected document
                lInitializeTOCLink()

                ReDim m_xarTOCSchemes(UBound(g_xTOCSchemes), 1)
                For i = LBound(g_xTOCSchemes) To UBound(g_xTOCSchemes)
                    m_xarTOCSchemes(i, 0) = g_xTOCSchemes(i, 0)
                    m_xarTOCSchemes(i, 1) = g_xTOCSchemes(i, 1)
                Next i
                Me.cmbTOCScheme.SetList(m_xarTOCSchemes)
            End If

            ReDim xarLevels(8, 1)
            For i = 0 To 8
                xarLevels(i, 0) = i + 1
                xarLevels(i, 1) = i + 1
            Next i
            Me.cmbLevels.SetList(xarLevels)

            RefreshSchemesList(tvwSchemes)

            'select default based on scheme
            If Me.DefaultBasedOnSchemeType <> mpSchemeTypes.mpSchemeType_Category Then
                tvwSchemes.SelectedNode = tvwSchemes.Nodes.Find(CStr(CInt(Me.DefaultBasedOnSchemeType)) & _
                                                                Me.DefaultBasedOnScheme, True).Single
            Else
                'select first public scheme
                If g_bIsAdmin Then
                    tvwSchemes.SelectedNode = tvwSchemes.Nodes(1).Nodes(0)
                Else
                    tvwSchemes.SelectedNode = tvwSchemes.Nodes(3).Nodes(0)
                End If
            End If

            'subsequent code was in the Activate handler
            Me.cmbLevels.SelectedIndex = iGetLevels(Me.BasedOnScheme, Me.BasedOnSchemeType) - 1

            Me.txtSchemeName.Select()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_AfterSelect(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvwSchemes.AfterSelect
        Dim bEnabled As Boolean
        Dim oScheme As cNumScheme

        Try
            If Me.BasedOnSchemeType <> mpSchemeTypes.mpSchemeType_Category Then
                Me.cmbLevels.SelectedValue = CStr(iGetLevels(Me.BasedOnScheme, Me.BasedOnSchemeType))
                If g_bAllowTOCLink Then
                    Try
                        Me.cmbTOCScheme.Value = GetTOCField(CShort(iGetTOCSchemeID()), mpTOCRecordFields.mpTOCRecField_Alias, mpSchemeTypes.mpSchemeType_TOC)
                    Catch
                    End Try
                    If Me.cmbTOCScheme.Text = "" Then Me.cmbTOCScheme.SelectedIndex = 0
                End If

                '       default checkboxes as per based on scheme (new to 9.8)
                oScheme = GetRecord(Me.BasedOnScheme, Me.BasedOnSchemeType)
                Me.chkAdjustSpacing.CheckState = oScheme.DynamicSpacing
                Me.chkBaseOnNormal.CheckState = oScheme.DymanicFonts

                'description (9.9.5001)
                Me.txtDescription.Text = oScheme.Description
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtSchemeName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSchemeName.Enter
        Try
            bEnsureSelectedContent((Me.txtSchemeName))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtSchemeName_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtSchemeName.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            xMsg = "Caract�re invalide. Le nom ne doit contenir aucun des caract�res suivants: \ : * ? " & Chr(34) & " < > | ="

            If KeyAscii = 8 Then
                '       backspace key pressed
                GoTo EventExitSub
            ElseIf KeyAscii = Asc("|") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc("*") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc("?") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc("\") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc(":") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = 34 Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc("<") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc(">") Then
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            ElseIf KeyAscii = Asc("=") Then
                '       Windows allows this character, but we're
                '       using as a substitute for "/"
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
                'removed character limit in 9.9.3
                '    ElseIf Len(Me.txtSchemeName) = 15 Then
                '        xMsg = "Scheme names are limited to 15 characters."
                '        MsgBox xMsg, vbExclamation, g_xAppName
                '        KeyAscii = 0
            End If
EventExitSub:
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtSchemeName_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSchemeName.Leave
        Try
            Me.txtStyleName.Text = xCreateStyleName(Me.txtSchemeName.Text)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtStyleName_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtStyleName.Enter
        Try
            bEnsureSelectedContent((Me.txtStyleName))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Function iGetTOCSchemeID() As String
        '   get linked toc scheme of 'based on' scheme
        iGetTOCSchemeID = GetField(Me.BasedOnScheme, mpRecordFields.mpRecField_TOCScheme, Me.BasedOnSchemeType)

    End Function

    Private Function xCreateStyleName(ByVal xSchemeName As String) As String
        'creates a valid Style Name from the scheme name
        Dim xTemp As String
        Dim xChar As String
        Dim iChar As Short
        Dim i As Short
        Dim bIsUnicode As Boolean

        '   remove non-alphanumeric characters
        For i = 1 To Len(xSchemeName)
            xChar = Mid(xSchemeName, i, 1)
            iChar = Asc(LCase(xChar))
            bIsUnicode = (AscW(xChar) <> Asc(xChar))

            '       must be numeric, or lowercase alpha -
            If IsNumeric(xChar) Or (iChar >= 97 And iChar <= 122) Or bIsUnicode Then
                xTemp = xTemp & xChar
                '           stop concatenation when
                '           style name is 10 chars
                If Len(xTemp) = 10 Then
                    Exit For
                End If
            End If
        Next i

        xCreateStyleName = xMatchFirmSchemeCase(xTemp)
    End Function

    Private Sub txtStyleName_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtStyleName.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        'allow only alphanumeric characters
        Dim xChar As String
        Dim iSelLength As Short
        Dim bIsUnicode As Boolean

        Try
            xChar = LCase(Chr(KeyAscii))
            If KeyAscii = 8 Then
                '       backspace key pressed
                GoTo EventExitSub
            End If

            iSelLength = Me.txtStyleName.SelectionLength
            bIsUnicode = (AscW(xChar) <> Asc(xChar))

            If (Len(Me.txtStyleName.Text) = 10) And (iSelLength = 0) Then
                xMsg = "Le nom de Style est limit� � 10  caract�res."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
            ElseIf Not (IsNumeric(xChar) Or (Asc(xChar) >= 97 And Asc(xChar) <= 122) Or bIsUnicode) Then
                xMsg = "Caract�re invalide. Le nom de Style doit contenir que des caract�res alphanum�rique."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                KeyAscii = 0
            End If
EventExitSub:
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class