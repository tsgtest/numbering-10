<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPublicSchemesFrench
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents tvwSchemes As System.Windows.Forms.TreeView
    Public WithEvents lblPublicSchemes As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPublicSchemesFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tvwSchemes = New System.Windows.Forms.TreeView()
        Me.lblPublicSchemes = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.vsbPreview = New System.Windows.Forms.VScrollBar()
        Me.imgContainer = New System.Windows.Forms.Panel()
        Me.Image1 = New System.Windows.Forms.PictureBox()
        Me.imgPreview = New System.Windows.Forms.PictureBox()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnViewProfile = New System.Windows.Forms.ToolStripButton()
        Me.btnImport = New System.Windows.Forms.ToolStripButton()
        Me.btnDelete = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.imgContainer.SuspendLayout()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'tvwSchemes
        '
        Me.tvwSchemes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvwSchemes.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvwSchemes.HideSelection = False
        Me.tvwSchemes.Indent = 0
        Me.tvwSchemes.Location = New System.Drawing.Point(3, 27)
        Me.tvwSchemes.Name = "tvwSchemes"
        Me.tvwSchemes.Size = New System.Drawing.Size(170, 309)
        Me.tvwSchemes.Sorted = True
        Me.tvwSchemes.TabIndex = 1
        '
        'lblPublicSchemes
        '
        Me.lblPublicSchemes.BackColor = System.Drawing.SystemColors.Control
        Me.lblPublicSchemes.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPublicSchemes.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPublicSchemes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPublicSchemes.Location = New System.Drawing.Point(3, 7)
        Me.lblPublicSchemes.Name = "lblPublicSchemes"
        Me.lblPublicSchemes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPublicSchemes.Size = New System.Drawing.Size(183, 25)
        Me.lblPublicSchemes.TabIndex = 0
        Me.lblPublicSchemes.Text = "&Th�mes partag�s disponibles:"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Location = New System.Drawing.Point(-1, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvwSchemes)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblPublicSchemes)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.vsbPreview)
        Me.SplitContainer1.Panel2.Controls.Add(Me.imgContainer)
        Me.SplitContainer1.Size = New System.Drawing.Size(459, 340)
        Me.SplitContainer1.SplitterDistance = 171
        Me.SplitContainer1.SplitterWidth = 1
        Me.SplitContainer1.TabIndex = 9
        '
        'vsbPreview
        '
        Me.vsbPreview.Cursor = System.Windows.Forms.Cursors.Default
        Me.vsbPreview.Enabled = False
        Me.vsbPreview.LargeChange = 1025
        Me.vsbPreview.Location = New System.Drawing.Point(266, 0)
        Me.vsbPreview.Maximum = 5124
        Me.vsbPreview.Name = "vsbPreview"
        Me.vsbPreview.Size = New System.Drawing.Size(19, 340)
        Me.vsbPreview.SmallChange = 410
        Me.vsbPreview.TabIndex = 9
        Me.vsbPreview.TabStop = True
        Me.vsbPreview.Value = 375
        '
        'imgContainer
        '
        Me.imgContainer.AutoSize = True
        Me.imgContainer.BackColor = System.Drawing.Color.White
        Me.imgContainer.Controls.Add(Me.Image1)
        Me.imgContainer.Controls.Add(Me.imgPreview)
        Me.imgContainer.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.imgContainer.Enabled = False
        Me.imgContainer.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgContainer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.imgContainer.Location = New System.Drawing.Point(0, 0)
        Me.imgContainer.Name = "imgContainer"
        Me.imgContainer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.imgContainer.Size = New System.Drawing.Size(285, 338)
        Me.imgContainer.TabIndex = 10
        Me.imgContainer.TabStop = True
        '
        'Image1
        '
        Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.Image = CType(resources.GetObject("Image1.Image"), System.Drawing.Image)
        Me.Image1.Location = New System.Drawing.Point(-1, 0)
        Me.Image1.Name = "Image1"
        Me.Image1.Size = New System.Drawing.Size(279, 26)
        Me.Image1.TabIndex = 0
        Me.Image1.TabStop = False
        '
        'imgPreview
        '
        Me.imgPreview.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.imgPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.imgPreview.Cursor = System.Windows.Forms.Cursors.Default
        Me.imgPreview.Location = New System.Drawing.Point(0, 0)
        Me.imgPreview.Name = "imgPreview"
        Me.imgPreview.Size = New System.Drawing.Size(262, 340)
        Me.imgPreview.TabIndex = 1
        Me.imgPreview.TabStop = False
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnViewProfile, Me.btnImport, Me.btnDelete, Me.ToolStripSeparator2})
        Me.tsButtons.Location = New System.Drawing.Point(0, 356)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(461, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 16
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Fermer"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'btnViewProfile
        '
        Me.btnViewProfile.AutoSize = False
        Me.btnViewProfile.Image = Global.MacPacNumbering.My.Resources.Resources.Profile
        Me.btnViewProfile.ImageTransparentColor = System.Drawing.Color.White
        Me.btnViewProfile.Name = "btnViewProfile"
        Me.btnViewProfile.Size = New System.Drawing.Size(99, 45)
        Me.btnViewProfile.Text = "A&fficher profil"
        Me.btnViewProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnImport
        '
        Me.btnImport.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnImport.AutoSize = False
        Me.btnImport.Image = Global.MacPacNumbering.My.Resources.Resources.Import
        Me.btnImport.ImageTransparentColor = System.Drawing.Color.White
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(60, 45)
        Me.btnImport.Text = "&Importer"
        Me.btnImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnDelete
        '
        Me.btnDelete.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnDelete.AutoSize = False
        Me.btnDelete.Image = Global.MacPacNumbering.My.Resources.Resources.Delete
        Me.btnDelete.ImageTransparentColor = System.Drawing.Color.White
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(60, 45)
        Me.btnDelete.Text = "&Supprimer"
        Me.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 49)
        '
        'frmPublicSchemesFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(461, 405)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(1473, 719)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPublicSchemesFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Th�mes partag�s"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.imgContainer.ResumeLayout(False)
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Public WithEvents vsbPreview As System.Windows.Forms.VScrollBar
    Public WithEvents imgContainer As System.Windows.Forms.Panel
    Public WithEvents Image1 As System.Windows.Forms.PictureBox
    Public WithEvents imgPreview As System.Windows.Forms.PictureBox
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnViewProfile As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImport As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
#End Region 
End Class