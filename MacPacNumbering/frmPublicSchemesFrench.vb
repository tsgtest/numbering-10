Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.CError
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumTOC
Imports LMP

Friend Class frmPublicSchemesFrench
    Inherits System.Windows.Forms.Form

    Private m_xDir As String
    Private m_bCancelled As Boolean
    Private m_xDesc As String


    Public Property PublicDir() As String
        Get
            PublicDir = m_xDir
        End Get
        Set(ByVal Value As String)
            If VB.Right(Value, 1) <> "\" Then
                Value = Value & "\"
            End If
            m_xDir = Value
        End Set
    End Property


    Public Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
        Set(ByVal Value As Boolean)
            m_bCancelled = Value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Cancelled = True
            Me.Hide()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnDelete.Click
        Dim lRet As Integer
        Dim xFile As String
        Dim oShare As MacPacNumbering.LMP.Numbering.cShare

        Try
            oShare = New MacPacNumbering.LMP.Numbering.cShare

            With Me.tvwSchemes
                xMsg = "Supprimer le th�me " & .SelectedNode.Text & "?"
                iUserChoice = MsgBox(xMsg, MsgBoxStyle.YesNo, g_xAppName)
                If iUserChoice = MsgBoxResult.Yes Then
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    xFile = PublicSchemesDir() & .SelectedNode.Parent.Text & "\" & .SelectedNode.Name
                    lRet = oShare.DeletePublicScheme(xFile)
                    If lRet = 0 Then
                        .Nodes.Remove(.SelectedNode)
                    ElseIf lRet <> mpnErrors.mpError_NoSharedSchemeDeletePermission Then 'GLOG 8682
                        Throw New System.Exception("Unable to delete scheme.")
                    End If
                    tvwSchemes_AfterSelect(tvwSchemes, New System.Windows.Forms.TreeViewEventArgs(.SelectedNode))
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                End If
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnImport.Click
        Dim oShare As MacPacNumbering.LMP.Numbering.cShare
        Dim xScheme As String
        Dim arrSchemes() As String

        Try
            oShare = New MacPacNumbering.LMP.Numbering.cShare

            ReDim arrSchemes(0)
            With Me.tvwSchemes.SelectedNode
                arrSchemes(0) = .Parent.Text & "\" & .Name
            End With
            xScheme = oShare.ImportPublicScheme(arrSchemes)
            If xScheme <> "" Then
                g_oCurScheme = GetRecord(xScheme, mpSchemeTypes.mpSchemeType_Private)
                Cancelled = False
                Me.Hide()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        Try
            Me.Cancelled = False
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnViewProfile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnViewProfile.Click
        Try
            ShowProfile()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmPublicSchemesFrench_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmPublicSchemesFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short

        Try
            Me.Cancelled = True

            'adjust for scaling
            Dim sFactor As Single = CScaling.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnImport.Width = Me.btnImport.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnDelete.Width = Me.btnDelete.Width * sFactor
                Me.btnViewProfile.Width = Me.btnViewProfile.Width * sFactor
                Me.btnImport.Height = Me.btnImport.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
                Me.btnDelete.Height = Me.btnDelete.Height + (sFactor - 1) * 40
                Me.btnViewProfile.Height = Me.btnViewProfile.Height + (sFactor - 1) * 40
            End If

            '   add categories to tree
            GetPublicCategories()

            '   load schemes - this is necessary to get + signs
            '   next to categories
            With Me.tvwSchemes
                For i = 0 To .Nodes.Count - 1
                    GetPublicSchemes((.Nodes.Item(i).Name))
                Next i
            End With

            System.Windows.Forms.Application.DoEvents()

            '   select first category
            'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.FirstSibling was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.FirstSibling.Selected was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            Try
                Me.tvwSchemes.SelectedNode = Me.tvwSchemes.Nodes.Item(0)
            Catch
                '   if err is generated, it's due to lack
                '   of shared scheme categories - alert
                Me.Close()
                xMsg = "Aucune cat�gorie de th�me partag� n'a �t� trouv�.  Votre administrateur doit ajouter au moins une cat�gorie � vos th�mes partag�s."
                MsgBox(xMsg, vbCritical, g_xAppName)
            End Try
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Function GetPublicCategories() As Object
        '   adds all scheme categories to tree
        Dim xDir As String

        '   cycle through all dirs in shared schemes dir
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        xDir = Dir(PublicSchemesDir() & "*.*", FileAttribute.Directory)
        While xDir <> ""
            If (xDir <> ".") And (xDir <> "..") Then
                '           add
                Me.tvwSchemes.Nodes.Add(xDir, xDir)
            End If
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            xDir = Dir()
        End While
    End Function

    Private Function GetPublicSchemes(ByRef xCategory As String) As Object
        '   add shared schemes in Category
        '   xCategory to the tree
        Dim xFile As String
        Dim xDisplayName As String
        Dim nodeCat As System.Windows.Forms.TreeNode
        Dim xOld As String
        Dim xNew As String
        Dim xRnd As String
        Dim xPublicDir As String

        nodeCat = Me.tvwSchemes.Nodes.Item(xCategory)

        '  cycle through all mpn files in shared schemes dir
        xPublicDir = PublicSchemesDir()
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        xFile = Dir(xPublicDir & xCategory & "\" & "*.mpn")
        While xFile <> ""
            '       trim extension from file
            xDisplayName = VB.Left(xFile, InStr(xFile, "@@_") - 1)
            xDisplayName = xSubstitute(xDisplayName, "=", "/")
            On Error Resume Next
            Me.tvwSchemes.Nodes.Find(nodeCat.Name, True)(0).Nodes.Add(xFile, xDisplayName)
            If Err.Number = 35602 Then
                '           this is an exact duplicate of a scheme in another folder - client
                '           has manually copied files - rename doc and bitmap
                Randomize()
                xRnd = Mid(CStr(Rnd()), 3)

                '           rename doc
                xOld = xPublicDir & xCategory & "\" & xFile
                xNew = xPublicDir & xCategory & "\" & xDisplayName & "@@_" & xRnd & ".mpn"
                Rename(xOld, xNew)

                '           add to tree
                Me.tvwSchemes.Nodes.Find(nodeCat.Name, True)(0).Nodes.Add(xDisplayName & "@@_" & xRnd & ".mpn", xDisplayName)

                '           rename bitmap
                xOld = xPublicDir & xCategory & "\" & xSubstitute(xFile, ".mpn", ".mpb")
                xNew = xPublicDir & xCategory & "\" & xDisplayName & "@@_" & xRnd & ".mpb"
                Rename(xOld, xNew)
            End If
            On Error GoTo 0
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            xFile = Dir()
        End While
    End Function

    Private Function ShowProfile(Optional ByRef bVisible As Boolean = True) As String
        Dim xFile As String
        Dim tplP As Word.Template
        Dim xPostedBy As String
        Dim xPostedOn As String
        Dim xDescription As String

        If bVisible Then
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            '       add as template
            With Me.tvwSchemes.SelectedNode
                If (.Parent Is Nothing) Then
                    '               a category is selected
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Function
                End If
                xFile = PublicSchemesDir() & .Parent.Text & "\" & .Name
            End With
            CurWordApp.AddIns.Add(xFile, True)
            tplP = GetTemplate(xFile)

            '       get doc properties
            With tplP.CustomDocumentProperties
                'UPGRADE_WARNING: Couldn't resolve default property of object tplP.CustomDocumentProperties.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xDescription = .Item("Description").Value
                'UPGRADE_WARNING: Couldn't resolve default property of object tplP.CustomDocumentProperties.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xPostedBy = .Item("PostedBy").Value
                'UPGRADE_WARNING: Couldn't resolve default property of object tplP.CustomDocumentProperties.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xPostedOn = Format(.Item("PostedOn").Value, "MMMM d, yyyy")
            End With

            tplP.Saved = True

            '       remove as template
            CurWordApp.AddIns.Item(xFile).Delete()

            Dim oForm As frmSchemeProfileFrench
            oForm = New frmSchemeProfileFrench()
            With oForm
                .btnOK.Visible = False
                .toolStripSeparator3.Visible = False
                .btnCancel.Text = "Fermer"
                .txtDescription.Text = xDescription
                .txtDescription.ReadOnly = True
                .txtPostedBy.Text = xPostedBy
                .txtPostedBy.ReadOnly = True
                .lblPostedOnText.Text = xPostedOn
                .cbxCategories.Text = Me.tvwSchemes.SelectedNode.Parent.Text
                .cbxCategories.Enabled = False
                .lblSchemeText.Text = Me.tvwSchemes.SelectedNode.Text
                .cbxCategories.BackColor = System.Drawing.SystemColors.Control
                .txtDescription.BackColor = System.Drawing.SystemColors.Control
                .txtPostedBy.BackColor = System.Drawing.SystemColors.Control
                .ShowDialog()
            End With
            ShowProfile = "Posted By: " & xPostedBy & vbCr & "Posted On: " & xPostedOn & vbCr & "Description:  " & xDescription
            oForm.Close()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Function

    Private Sub tvwSchemes_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tvwSchemes.DoubleClick
        Try
            tvwSchemes_AfterSelect(tvwSchemes, New System.Windows.Forms.TreeViewEventArgs(Me.tvwSchemes.SelectedNode))
            If InStr(Me.Text, "Importer") Then
                If btnImport.Enabled Then btnImport_Click(btnImport, New System.EventArgs())
            Else
                If btnDelete.Enabled Then btnDelete_Click(btnDelete, New System.EventArgs())
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_AfterSelect(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvwSchemes.AfterSelect
        Dim Node As System.Windows.Forms.TreeNode
        Dim xBitmap As String
        Dim xFile As String
        Dim xCategory As String
        Dim bIsScheme As Boolean

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            With Me.tvwSchemes
                xFile = .SelectedNode.Name
                bIsScheme = VB.Right(xFile, 4) = ".mpn"
                Me.btnDelete.Enabled = bIsScheme
                Me.btnImport.Enabled = bIsScheme
                Me.btnViewProfile.Enabled = bIsScheme

                If bIsScheme Then
                    '           selected node is a scheme
                    xCategory = .SelectedNode.Parent.Text
                    xBitmap = PublicSchemesDir() & xCategory & "\" & xSubstitute(VB.Left(xFile, Len(xFile) - 4), "/", "=") & ".mpb"
                Else
                    xBitmap = GetAppPath() & "\" & "PubCatFrench.mpb"
                    ShowProfile(False)
                    If .SelectedNode.GetNodeCount(False) = 0 Then
                        GetPublicSchemes(.SelectedNode.Name)
                    End If

                    'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.SelectedItem.Expanded was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                    If Not .SelectedNode.IsExpanded Then
                        .SelectedNode.Expand()
                    Else
                        .SelectedNode.Collapse()
                    End If
                End If
            End With
            Me.Image1.Visible = bIsScheme

            With Me.imgPreview
                If bIsScheme Then
                    .Top = Me.Image1.Top + Me.Image1.Height
                    .Height = Me.tvwSchemes.Height
                Else
                    .Top = Me.Image1.Top
                    .Height = Me.tvwSchemes.Height + Me.Image1.Height
                End If
            End With

            '9/6/16 - load image from a file stream to avoid locking the bitmap itself,
            'in case it needs to be deleted
            'Me.imgPreview.Image = System.Drawing.Image.FromFile(xBitmap)
            Dim fs As System.IO.FileStream
            fs = New System.IO.FileStream(xBitmap, IO.FileMode.Open, IO.FileAccess.Read)
            Me.imgPreview.BackgroundImage = CScaling.ScaleImageForScreen(System.Drawing.Image.FromStream(fs)) 'GLOG 8611
            fs.Close()
        Catch oE As Exception
            Select Case Err.Number
                Case 53 'file not found
                    Me.imgPreview.Image = System.Drawing.Image.FromFile(GetAppPath() & "\NoPreview.mpb")
                Case Else
                    [Error].Show(oE)
            End Select
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    'UPGRADE_NOTE: vsbPreview.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    'UPGRADE_WARNING: VScrollBar event vsbPreview.Change has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
    Private Sub vsbPreview_Change(ByVal newScrollValue As Integer)
        ScrollPreview()
    End Sub

    Private Sub ScrollPreview()
        EchoOff()
        'UPGRADE_ISSUE: Panel property Me.imgContainer.BackgroundImage will be tiled. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED2DEFAA-C59C-4EDB-AF23-73A16C92C3AC"'
        Me.imgPreview.Top = CDbl(CObj(Me.imgContainer.BackgroundImage)) - Me.vsbPreview.Value + 375
        System.Windows.Forms.Application.DoEvents()
        EchoOn()
    End Sub

    'UPGRADE_NOTE: vsbPreview.Scroll was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
    Private Sub vsbPreview_Scroll_Renamed(ByVal newScrollValue As Integer)
        ScrollPreview()
    End Sub
    Private Sub vsbPreview_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs)
        Select Case eventArgs.Type
            Case System.Windows.Forms.ScrollEventType.ThumbTrack
                vsbPreview_Scroll_Renamed(eventArgs.NewValue)
            Case System.Windows.Forms.ScrollEventType.EndScroll
                vsbPreview_Change(eventArgs.NewValue)
        End Select
    End Sub
End Class