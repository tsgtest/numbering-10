<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSchemeFonts
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cmbNumFontName As mpnControls.ComboBox
    Public WithEvents cmbNumFontSize As mpnControls.ComboBox
    Public WithEvents lblNumFontSize As System.Windows.Forms.Label
    Public WithEvents lblNumFontName As System.Windows.Forms.Label
    Public WithEvents fraNumber As System.Windows.Forms.GroupBox
    Public WithEvents cmbFontName As mpnControls.ComboBox
    Public WithEvents cmbFontSize As mpnControls.ComboBox
    Public WithEvents lblFontName As System.Windows.Forms.Label
    Public WithEvents lblFontSize As System.Windows.Forms.Label
    Public WithEvents fraParagraph As System.Windows.Forms.GroupBox
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchemeFonts))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.fraNumber = New System.Windows.Forms.GroupBox()
        Me.lblNumFontSize = New System.Windows.Forms.Label()
        Me.lblNumFontName = New System.Windows.Forms.Label()
        Me.fraParagraph = New System.Windows.Forms.GroupBox()
        Me.lblFontName = New System.Windows.Forms.Label()
        Me.lblFontSize = New System.Windows.Forms.Label()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.cmdCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmbNumFontName = New mpnControls.ComboBox()
        Me.cmbNumFontSize = New mpnControls.ComboBox()
        Me.cmbFontName = New mpnControls.ComboBox()
        Me.cmbFontSize = New mpnControls.ComboBox()
        Me.fraNumber.SuspendLayout()
        Me.fraParagraph.SuspendLayout()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'fraNumber
        '
        Me.fraNumber.BackColor = System.Drawing.SystemColors.Control
        Me.fraNumber.Controls.Add(Me.cmbNumFontName)
        Me.fraNumber.Controls.Add(Me.cmbNumFontSize)
        Me.fraNumber.Controls.Add(Me.lblNumFontSize)
        Me.fraNumber.Controls.Add(Me.lblNumFontName)
        Me.fraNumber.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraNumber.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraNumber.Location = New System.Drawing.Point(10, 10)
        Me.fraNumber.Name = "fraNumber"
        Me.fraNumber.Padding = New System.Windows.Forms.Padding(0)
        Me.fraNumber.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraNumber.Size = New System.Drawing.Size(293, 130)
        Me.fraNumber.TabIndex = 0
        Me.fraNumber.TabStop = False
        Me.fraNumber.Text = "Font for Scheme Numbers"
        '
        'lblNumFontSize
        '
        Me.lblNumFontSize.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumFontSize.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumFontSize.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumFontSize.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumFontSize.Location = New System.Drawing.Point(13, 71)
        Me.lblNumFontSize.Name = "lblNumFontSize"
        Me.lblNumFontSize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumFontSize.Size = New System.Drawing.Size(63, 20)
        Me.lblNumFontSize.TabIndex = 2
        Me.lblNumFontSize.Text = "Si&ze:"
        '
        'lblNumFontName
        '
        Me.lblNumFontName.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumFontName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumFontName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumFontName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumFontName.Location = New System.Drawing.Point(13, 24)
        Me.lblNumFontName.Name = "lblNumFontName"
        Me.lblNumFontName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumFontName.Size = New System.Drawing.Size(117, 20)
        Me.lblNumFontName.TabIndex = 0
        Me.lblNumFontName.Text = "&Name:"
        '
        'fraParagraph
        '
        Me.fraParagraph.BackColor = System.Drawing.SystemColors.Control
        Me.fraParagraph.Controls.Add(Me.cmbFontName)
        Me.fraParagraph.Controls.Add(Me.cmbFontSize)
        Me.fraParagraph.Controls.Add(Me.lblFontName)
        Me.fraParagraph.Controls.Add(Me.lblFontSize)
        Me.fraParagraph.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraParagraph.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraParagraph.Location = New System.Drawing.Point(10, 155)
        Me.fraParagraph.Name = "fraParagraph"
        Me.fraParagraph.Padding = New System.Windows.Forms.Padding(0)
        Me.fraParagraph.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraParagraph.Size = New System.Drawing.Size(293, 133)
        Me.fraParagraph.TabIndex = 5
        Me.fraParagraph.TabStop = False
        Me.fraParagraph.Text = "Font for Scheme Paragraphs"
        '
        'lblFontName
        '
        Me.lblFontName.BackColor = System.Drawing.SystemColors.Control
        Me.lblFontName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFontName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFontName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFontName.Location = New System.Drawing.Point(11, 22)
        Me.lblFontName.Name = "lblFontName"
        Me.lblFontName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFontName.Size = New System.Drawing.Size(49, 20)
        Me.lblFontName.TabIndex = 4
        Me.lblFontName.Text = "Na&me:"
        '
        'lblFontSize
        '
        Me.lblFontSize.BackColor = System.Drawing.SystemColors.Control
        Me.lblFontSize.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFontSize.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFontSize.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFontSize.Location = New System.Drawing.Point(11, 72)
        Me.lblFontSize.Name = "lblFontSize"
        Me.lblFontSize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFontSize.Size = New System.Drawing.Size(36, 20)
        Me.lblFontSize.TabIndex = 6
        Me.lblFontSize.Text = "&Size:"
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdCancel, Me.toolStripSeparator1, Me.cmdOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 295)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(314, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 16
        Me.tsButtons.Text = "ToolStrip1"
        '
        'cmdCancel
        '
        Me.cmdCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdCancel.AutoSize = False
        Me.cmdCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.cmdCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdCancel.Size = New System.Drawing.Size(60, 45)
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'cmdOK
        '
        Me.cmdOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdOK.AutoSize = False
        Me.cmdOK.Image = Global.MacPacNumbering.My.Resources.Resources.OK
        Me.cmdOK.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdOK.Size = New System.Drawing.Size(60, 45)
        Me.cmdOK.Text = "O&K"
        Me.cmdOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 49)
        '
        'cmbNumFontName
        '
        Me.cmbNumFontName.AllowEmptyValue = False
        Me.cmbNumFontName.AutoSize = True
        Me.cmbNumFontName.Borderless = False
        Me.cmbNumFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumFontName.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumFontName.IsDirty = False
        Me.cmbNumFontName.LimitToList = True
        Me.cmbNumFontName.Location = New System.Drawing.Point(12, 40)
        Me.cmbNumFontName.MaxDropDownItems = 8
        Me.cmbNumFontName.Name = "cmbNumFontName"
        Me.cmbNumFontName.SelectedIndex = -1
        Me.cmbNumFontName.SelectedValue = Nothing
        Me.cmbNumFontName.SelectionLength = 0
        Me.cmbNumFontName.SelectionStart = 0
        Me.cmbNumFontName.Size = New System.Drawing.Size(235, 23)
        Me.cmbNumFontName.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumFontName.SupportingValues = ""
        Me.cmbNumFontName.TabIndex = 1
        Me.cmbNumFontName.Tag2 = Nothing
        Me.cmbNumFontName.Value = ""
        '
        'cmbNumFontSize
        '
        Me.cmbNumFontSize.AllowEmptyValue = False
        Me.cmbNumFontSize.AutoSize = True
        Me.cmbNumFontSize.Borderless = False
        Me.cmbNumFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumFontSize.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumFontSize.IsDirty = False
        Me.cmbNumFontSize.LimitToList = True
        Me.cmbNumFontSize.Location = New System.Drawing.Point(12, 86)
        Me.cmbNumFontSize.MaxDropDownItems = 8
        Me.cmbNumFontSize.Name = "cmbNumFontSize"
        Me.cmbNumFontSize.SelectedIndex = -1
        Me.cmbNumFontSize.SelectedValue = Nothing
        Me.cmbNumFontSize.SelectionLength = 0
        Me.cmbNumFontSize.SelectionStart = 0
        Me.cmbNumFontSize.Size = New System.Drawing.Size(235, 23)
        Me.cmbNumFontSize.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumFontSize.SupportingValues = ""
        Me.cmbNumFontSize.TabIndex = 3
        Me.cmbNumFontSize.Tag2 = Nothing
        Me.cmbNumFontSize.Value = ""
        '
        'cmbFontName
        '
        Me.cmbFontName.AllowEmptyValue = False
        Me.cmbFontName.AutoSize = True
        Me.cmbFontName.Borderless = False
        Me.cmbFontName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbFontName.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFontName.IsDirty = False
        Me.cmbFontName.LimitToList = True
        Me.cmbFontName.Location = New System.Drawing.Point(12, 38)
        Me.cmbFontName.MaxDropDownItems = 8
        Me.cmbFontName.Name = "cmbFontName"
        Me.cmbFontName.SelectedIndex = -1
        Me.cmbFontName.SelectedValue = Nothing
        Me.cmbFontName.SelectionLength = 0
        Me.cmbFontName.SelectionStart = 0
        Me.cmbFontName.Size = New System.Drawing.Size(235, 23)
        Me.cmbFontName.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbFontName.SupportingValues = ""
        Me.cmbFontName.TabIndex = 5
        Me.cmbFontName.Tag2 = Nothing
        Me.cmbFontName.Value = ""
        '
        'cmbFontSize
        '
        Me.cmbFontSize.AllowEmptyValue = False
        Me.cmbFontSize.AutoSize = True
        Me.cmbFontSize.Borderless = False
        Me.cmbFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbFontSize.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFontSize.IsDirty = False
        Me.cmbFontSize.LimitToList = True
        Me.cmbFontSize.Location = New System.Drawing.Point(12, 87)
        Me.cmbFontSize.MaxDropDownItems = 8
        Me.cmbFontSize.Name = "cmbFontSize"
        Me.cmbFontSize.SelectedIndex = -1
        Me.cmbFontSize.SelectedValue = Nothing
        Me.cmbFontSize.SelectionLength = 0
        Me.cmbFontSize.SelectionStart = 0
        Me.cmbFontSize.Size = New System.Drawing.Size(235, 23)
        Me.cmbFontSize.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbFontSize.SupportingValues = ""
        Me.cmbFontSize.TabIndex = 7
        Me.cmbFontSize.Tag2 = Nothing
        Me.cmbFontSize.Value = ""
        '
        'frmSchemeFonts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(314, 344)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.fraNumber)
        Me.Controls.Add(Me.fraParagraph)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSchemeFonts"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "###"
        Me.fraNumber.ResumeLayout(False)
        Me.fraNumber.PerformLayout()
        Me.fraParagraph.ResumeLayout(False)
        Me.fraParagraph.PerformLayout()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region
End Class