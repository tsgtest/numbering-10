Option Strict Off
Option Explicit On
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports LMP.Numbering.Base.cNumTOC
Imports VB = Microsoft.VisualBasic
Imports MacPacNumbering.LMP.Numbering
Imports LMP

Friend Class frmSchemeFontsFrench
    Inherits System.Windows.Forms.Form

    Private m_bCancelled As Boolean
    Private m_xScheme As String
    Private m_SchemeProps As MacPacNumbering.LMP.Numbering.cSchemeProps

    Public ReadOnly Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
    End Property

    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            m_bCancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Try
            m_bCancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmSchemeFontsFrench_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                cmdCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmSchemeFontsFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim xName As String
        Dim oFont As System.Drawing.Font
        Dim aFont As Object
        Dim iNumFonts As Short
        Dim xFonts() As String
        Dim bRestrictFonts As Boolean
        Dim xarFonts(,) As String
        Dim xarSize(,) As String

        Try
            'adjust for scaling
            Dim sFactorIncrease As Single = CScaling.GetScalingFactor()
            If sFactorIncrease > 1 Then
                Me.cmdOK.Width = Me.cmdOK.Width * sFactorIncrease
                Me.cmdCancel.Width = Me.cmdCancel.Width * sFactorIncrease
                Me.cmdOK.Height = Me.cmdOK.Height + (sFactorIncrease - 1) * 40
                Me.cmdCancel.Height = Me.cmdCancel.Height + (sFactorIncrease - 1) * 40
            End If

            m_SchemeProps = New cSchemeProps

            '   get fonts
            Try
                bRestrictFonts = CBool(GetAppSetting("Numbering", "RestrictFontList"))
            Catch
            End Try

            If bRestrictFonts Then
                '       load list from ini
                If g_xPermittedFonts(0) <> "" Then
                    ReDim xarFonts(UBound(g_xPermittedFonts) + 2, 1)
                    xarFonts(0, 0) = "-Utiliser police courante-"
                    xarFonts(0, 1) = "-Utiliser police courante-"
                    xarFonts(1, 0) = "-Utiliser police du style Normal-"
                    xarFonts(1, 1) = "-Utiliser police du style Normal-"
                    For i = LBound(g_xPermittedFonts) To UBound(g_xPermittedFonts)
                        xarFonts(i + 2, 0) = g_xPermittedFonts(i)
                        xarFonts(i + 2, 1) = g_xPermittedFonts(i)
                    Next i
                End If
            Else
                '       load system fonts
                'GLOG 4839 - omit vertical fonts (names start with '@')
                For Each aFont In CurWordApp.FontNames
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If VB.Left(aFont, 1) <> "@" Then
                        iNumFonts = iNumFonts + 1
                    End If
                Next aFont
                ReDim xFonts(iNumFonts - 1)

                '       stick in variant array
                For Each aFont In CurWordApp.FontNames
                    'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If VB.Left(aFont, 1) <> "@" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object aFont. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        xFonts(i) = aFont
                        i = i + 1
                    End If
                Next aFont

                '       sort array
                'UPGRADE_WARNING: Couldn't resolve default property of object WordBasic.SortArray. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                CurWordApp.WordBasic.SortArray(xFonts)

                '       add array items to lists
                ReDim xarFonts(UBound(xFonts) + 2, 1)
                xarFonts(0, 0) = "-Utiliser police courante-"
                xarFonts(0, 1) = "-Utiliser police courante-"
                xarFonts(1, 0) = "-Utiliser police du style Normal-"
                xarFonts(1, 1) = "-Utiliser police du style Normal-"
                For i = 0 To iNumFonts - 1
                    xarFonts(i + 2, 0) = xFonts(i)
                    xarFonts(i + 2, 1) = xFonts(i)
                Next i
            End If

            Me.cmbFontName.SetList(xarFonts)
            Me.cmbNumFontName.SetList(xarFonts)

            '   get font sizes
            ReDim xarSize(21, 1)
            xarSize(0, 0) = "-Utiliser taille de police courante-"
            xarSize(0, 1) = "-Utiliser taille de police courante-"
            xarSize(1, 0) = "-Utiliser taille de la police du style Normal-"
            xarSize(1, 1) = "-Utiliser taille de la police du style Normal-"
            For i = 7 To 26
                xarSize(i - 5, 0) = CStr(i)
                xarSize(i - 5, 1) = CStr(i)
            Next i

            Me.cmbFontSize.SetList(xarSize)
            Me.cmbNumFontSize.SetList(xarSize)

            '   select first item in lists
            Me.cmbFontName.SelectedIndex = 0
            Me.cmbFontSize.SelectedIndex = 0
            Me.cmbNumFontName.SelectedIndex = 0
            Me.cmbNumFontSize.SelectedIndex = 0
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class