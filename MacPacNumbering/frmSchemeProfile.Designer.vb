<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSchemeProfile
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents txtDescription As System.Windows.Forms.TextBox
	Public WithEvents txtPostedBy As System.Windows.Forms.TextBox
    Public WithEvents cbxCategories As mpnControls.ComboBox
	Public WithEvents lblPostedOnText As System.Windows.Forms.Label
	Public WithEvents lblPostedOn As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblDescription As System.Windows.Forms.Label
	Public WithEvents lblPostedBy As System.Windows.Forms.Label
	Public WithEvents lblSchemeText As System.Windows.Forms.Label
	Public WithEvents lblScheme As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchemeProfile))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.txtPostedBy = New System.Windows.Forms.TextBox()
        Me.lblPostedOnText = New System.Windows.Forms.Label()
        Me.lblPostedOn = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblPostedBy = New System.Windows.Forms.Label()
        Me.lblSchemeText = New System.Windows.Forms.Label()
        Me.lblScheme = New System.Windows.Forms.Label()
        Me.cbxCategories = New mpnControls.ComboBox()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDescription
        '
        Me.txtDescription.AcceptsReturn = True
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDescription.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(81, 97)
        Me.txtDescription.MaxLength = 255
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDescription.Size = New System.Drawing.Size(304, 114)
        Me.txtDescription.TabIndex = 3
        '
        'txtPostedBy
        '
        Me.txtPostedBy.AcceptsReturn = True
        Me.txtPostedBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtPostedBy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPostedBy.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPostedBy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtPostedBy.Location = New System.Drawing.Point(81, 223)
        Me.txtPostedBy.MaxLength = 0
        Me.txtPostedBy.Name = "txtPostedBy"
        Me.txtPostedBy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPostedBy.Size = New System.Drawing.Size(304, 20)
        Me.txtPostedBy.TabIndex = 5
        '
        'lblPostedOnText
        '
        Me.lblPostedOnText.BackColor = System.Drawing.SystemColors.Control
        Me.lblPostedOnText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPostedOnText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPostedOnText.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostedOnText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPostedOnText.Location = New System.Drawing.Point(81, 264)
        Me.lblPostedOnText.Name = "lblPostedOnText"
        Me.lblPostedOnText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPostedOnText.Size = New System.Drawing.Size(304, 27)
        Me.lblPostedOnText.TabIndex = 11
        Me.lblPostedOnText.Text = "###"
        '
        'lblPostedOn
        '
        Me.lblPostedOn.BackColor = System.Drawing.SystemColors.Control
        Me.lblPostedOn.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPostedOn.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostedOn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPostedOn.Location = New System.Drawing.Point(12, 268)
        Me.lblPostedOn.Name = "lblPostedOn"
        Me.lblPostedOn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPostedOn.Size = New System.Drawing.Size(75, 20)
        Me.lblPostedOn.TabIndex = 10
        Me.lblPostedOn.Text = "Posted On:"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(10, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(78, 27)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Category:"
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(10, 98)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(78, 27)
        Me.lblDescription.TabIndex = 2
        Me.lblDescription.Text = "&Description:"
        '
        'lblPostedBy
        '
        Me.lblPostedBy.BackColor = System.Drawing.SystemColors.Control
        Me.lblPostedBy.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPostedBy.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostedBy.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPostedBy.Location = New System.Drawing.Point(12, 228)
        Me.lblPostedBy.Name = "lblPostedBy"
        Me.lblPostedBy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPostedBy.Size = New System.Drawing.Size(75, 20)
        Me.lblPostedBy.TabIndex = 4
        Me.lblPostedBy.Text = "&Posted By:"
        '
        'lblSchemeText
        '
        Me.lblSchemeText.BackColor = System.Drawing.SystemColors.Control
        Me.lblSchemeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSchemeText.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSchemeText.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSchemeText.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSchemeText.Location = New System.Drawing.Point(81, 19)
        Me.lblSchemeText.Name = "lblSchemeText"
        Me.lblSchemeText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSchemeText.Size = New System.Drawing.Size(304, 27)
        Me.lblSchemeText.TabIndex = 9
        Me.lblSchemeText.Text = "###"
        '
        'lblScheme
        '
        Me.lblScheme.BackColor = System.Drawing.SystemColors.Control
        Me.lblScheme.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblScheme.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScheme.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblScheme.Location = New System.Drawing.Point(10, 23)
        Me.lblScheme.Name = "lblScheme"
        Me.lblScheme.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblScheme.Size = New System.Drawing.Size(70, 23)
        Me.lblScheme.TabIndex = 8
        Me.lblScheme.Text = "Name:"
        '
        'cbxCategories
        '
        Me.cbxCategories.AllowEmptyValue = False
        Me.cbxCategories.AutoSize = True
        Me.cbxCategories.Borderless = False
        Me.cbxCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxCategories.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCategories.IsDirty = False
        Me.cbxCategories.LimitToList = True
        Me.cbxCategories.Location = New System.Drawing.Point(81, 57)
        Me.cbxCategories.MaxDropDownItems = 8
        Me.cbxCategories.Name = "cbxCategories"
        Me.cbxCategories.SelectedIndex = -1
        Me.cbxCategories.SelectedValue = Nothing
        Me.cbxCategories.SelectionLength = 0
        Me.cbxCategories.SelectionStart = 0
        Me.cbxCategories.Size = New System.Drawing.Size(304, 27)
        Me.cbxCategories.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxCategories.SupportingValues = ""
        Me.cbxCategories.TabIndex = 1
        Me.cbxCategories.Tag2 = Nothing
        Me.cbxCategories.Value = ""
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 301)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(406, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 17
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Image = Global.MacPacNumbering.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 49)
        '
        'frmSchemeProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(406, 350)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.txtPostedBy)
        Me.Controls.Add(Me.cbxCategories)
        Me.Controls.Add(Me.lblPostedOnText)
        Me.Controls.Add(Me.lblPostedOn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblPostedBy)
        Me.Controls.Add(Me.lblSchemeText)
        Me.Controls.Add(Me.lblScheme)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSchemeProfile"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " Scheme Profile"
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region 
End Class