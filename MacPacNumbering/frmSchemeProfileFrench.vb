Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cStrings
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.CError
Imports MacPacNumbering.LMP.Numbering.mpDialog_VB
Imports LMP

Friend Class frmSchemeProfileFrench
	Inherits System.Windows.Forms.Form
	
	Private m_bCancelled As Boolean
	
	
	Public Property Cancelled() As Boolean
		Get
			Cancelled = m_bCancelled
		End Get
		Set(ByVal Value As Boolean)
			m_bCancelled = Value
		End Set
	End Property
	
    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Hide()
            Me.Cancelled = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            'If Me.ActiveControl.Name = "txtDescription" Then Exit Sub 'remmed for GLOG 8915 (dm)
            If bProfileIsValid() Then
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Hide()
                System.Windows.Forms.Application.DoEvents()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmSchemeProfileFrench_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

	Private Sub frmSchemeProfileFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Try
            Me.Cancelled = False

            'adjust for scaling
            Dim sFactorIncrease As Single = CScaling.GetScalingFactor()
            If sFactorIncrease > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactorIncrease
                Me.btnCancel.Width = Me.btnCancel.Width * sFactorIncrease
                Me.btnOK.Height = Me.btnOK.Height + (sFactorIncrease - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactorIncrease - 1) * 40
            End If

            '   get shared scheme categories
            GetPublicCategories()

            '   set user name and posted date
            '9/6/16 - added condition because in .NET this handler doesn't run
            'until after frmPublicSchemes.ShowProfile() has initialized controls
            If Me.txtPostedBy.Text = Nothing Then
                Me.txtPostedBy.Text = CurWordApp.UserName
                Me.lblPostedOnText.Text = Format(Now, "MMMM d, yyyy")
            End If

            '9.9.5001 - limit newly exported schemes to 120 characters
            If Not Me.txtDescription.ReadOnly Then
                'UPGRADE_WARNING: TextBox property txtDescription.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                Me.txtDescription.MaxLength = 120
                Me.txtDescription.Height = Me.txtDescription.Height - 50
                Me.lblPostedBy.Top = Me.lblPostedBy.Top - 50
                Me.txtPostedBy.Top = Me.txtPostedBy.Top - 50
                Me.lblPostedOn.Top = Me.lblPostedOn.Top - 50
                Me.lblPostedOnText.Top = Me.lblPostedOnText.Top - 50
                'Me.btnCancel.Top = Me.btnCancel.Top - 50
                'Me.btnOK.Top = Me.btnOK.Top - 50
                Me.Height = Me.Height - 50
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Function bProfileIsValid() As Boolean
		Dim xFileExport As String
		Dim xFile As String
		Dim xMsg As String
		Dim iUserChoice As Short
		
		If Me.txtPostedBy.Text = "" Then
            xMsg = "Publi� par est une information obligatoire."
			Me.txtPostedBy.Focus()
			MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
			Exit Function
		End If
		
		'   check for existence of scheme by same name
		xFileExport = Me.lblSchemeText.Text & mpFilePrefixSep
        xFileExport = xSubstitute(xFileExport, "/", "=")
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		xFile = Dir(PublicSchemesDir() & Me.cbxCategories.Text & "\" & "*.mpn")
		While xFile <> ""
			'       scheme already exists if the left chars match
			'       the prefix of the proposed export file
			If VB.Left(xFile, Len(xFileExport)) = xFileExport Then
                xMsg = "Un th�me partag� existe d�j� avec ce nom. Si vous continuez, le dossier Th�me partag� affichera plusieurs th�mes contenant ce nom." & _
                    vbCr & vbCr & "D�sirez-vous continuer?"
                iUserChoice = MsgBox(xMsg, MsgBoxStyle.Question + MsgBoxStyle.YesNo, g_xAppName)
				bProfileIsValid = (iUserChoice = MsgBoxResult.Yes)
				Exit Function
			End If
			'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			xFile = Dir()
		End While
		bProfileIsValid = True
	End Function
	
	Private Function GetPublicCategories() As Object
		'   adds all scheme categories to tree
		Dim xDir As String
		Dim xCategories As String
		Dim aCategories As Object
        Dim xarCategories(,) As String
		Dim i As Short
		
		'   cycle through all dirs in shared scheme dir
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		xDir = Dir(PublicSchemesDir() & "*.*", FileAttribute.Directory)
		While xDir <> ""
			If (xDir <> ".") And (xDir <> "..") Then
				'           add
				xCategories = xCategories & xDir & "|"
			End If
			'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			xDir = Dir()
		End While
		If xCategories <> "" Then
			xCategories = VB.Left(xCategories, Len(xCategories) - 1)
			'UPGRADE_WARNING: Couldn't resolve default property of object aCategories. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			aCategories = Split(xCategories, "|")
            ReDim xarCategories(UBound(aCategories), 1)
			For i = 0 To UBound(aCategories)
				'UPGRADE_WARNING: Couldn't resolve default property of object aCategories(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xarCategories(i, 0) = aCategories(i)
				'UPGRADE_WARNING: Couldn't resolve default property of object aCategories(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                xarCategories(i, 1) = aCategories(i)
			Next i
			'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxCategories.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cbxCategories.SetList(xarCategories)

			'UPGRADE_WARNING: Couldn't resolve default property of object aCategories(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Me.cbxCategories.Text = aCategories(0)
		Else
            xMsg = "Aucune cat�gorie de th�me partag� n'a �t� trouv�.  Votre administrateur doit ajouter au moins une cat�gorie � vos th�mes partag�s."
            MsgBox(xMsg, vbCritical, g_xAppName)
        End If
	End Function
	Private Sub txtDescription_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescription.Enter
        Try
            bEnsureSelectedContent(txtDescription, True)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	Private Sub txtDescription_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            If KeyAscii = 13 Then KeyAscii = 0
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	Private Sub txtPostedBy_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPostedBy.Enter
        Try
            bEnsureSelectedContent(txtPostedBy, True)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class