Imports LMP.Controls

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSchemePropertiesFrench
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cmbTOCScheme As mpnControls.ComboBox
    Public WithEvents txtDescription As System.Windows.Forms.TextBox
    Public WithEvents chkAdjustSpacing As System.Windows.Forms.CheckBox
    Public WithEvents chkBaseOnNormal As System.Windows.Forms.CheckBox
    Public WithEvents txtSchemeName As System.Windows.Forms.TextBox
    Public WithEvents txtStyleName As System.Windows.Forms.TextBox
    Public WithEvents lblDescription As System.Windows.Forms.Label
    Public WithEvents lblTOCScheme As System.Windows.Forms.Label
    Public WithEvents lblSchemeName As System.Windows.Forms.Label
    Public WithEvents lblStyleName As System.Windows.Forms.Label
    Public WithEvents lblMsg As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchemePropertiesFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkAdjustSpacing = New System.Windows.Forms.CheckBox()
        Me.chkBaseOnNormal = New System.Windows.Forms.CheckBox()
        Me.txtSchemeName = New System.Windows.Forms.TextBox()
        Me.txtStyleName = New System.Windows.Forms.TextBox()
        Me.lblTOCScheme = New System.Windows.Forms.Label()
        Me.lblSchemeName = New System.Windows.Forms.Label()
        Me.lblStyleName = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.cmdCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmdOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmbTOCScheme = New mpnControls.ComboBox()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkAdjustSpacing
        '
        Me.chkAdjustSpacing.BackColor = System.Drawing.SystemColors.Control
        Me.chkAdjustSpacing.Checked = True
        Me.chkAdjustSpacing.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAdjustSpacing.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkAdjustSpacing.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAdjustSpacing.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkAdjustSpacing.Location = New System.Drawing.Point(15, 192)
        Me.chkAdjustSpacing.Name = "chkAdjustSpacing"
        Me.chkAdjustSpacing.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkAdjustSpacing.Size = New System.Drawing.Size(250, 46)
        Me.chkAdjustSpacing.TabIndex = 9
        Me.chkAdjustSpacing.Text = "&Changer l'espacement simple/double afin de correspondre au style Normal pour int" & _
    "erligne au moins, exactement ou multiple"
        Me.ToolTip1.SetToolTip(Me.chkAdjustSpacing, "Changer l'espacement simple/double afin de correspondre au style Normal pour inte" & _
        "rligne au moins, exactement ou multiple")
        Me.chkAdjustSpacing.UseVisualStyleBackColor = False
        '
        'chkBaseOnNormal
        '
        Me.chkBaseOnNormal.BackColor = System.Drawing.SystemColors.Control
        Me.chkBaseOnNormal.Checked = True
        Me.chkBaseOnNormal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBaseOnNormal.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkBaseOnNormal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBaseOnNormal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkBaseOnNormal.Location = New System.Drawing.Point(15, 165)
        Me.chkBaseOnNormal.Name = "chkBaseOnNormal"
        Me.chkBaseOnNormal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkBaseOnNormal.Size = New System.Drawing.Size(177, 25)
        Me.chkBaseOnNormal.TabIndex = 8
        Me.chkBaseOnNormal.Text = "&Utiliser police du style Normal"
        Me.ToolTip1.SetToolTip(Me.chkBaseOnNormal, "Changer le nom et la taille de la police pour tous les niveaux du style Normal.")
        Me.chkBaseOnNormal.UseVisualStyleBackColor = False
        '
        'txtSchemeName
        '
        Me.txtSchemeName.AcceptsReturn = True
        Me.txtSchemeName.BackColor = System.Drawing.SystemColors.Window
        Me.txtSchemeName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSchemeName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSchemeName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSchemeName.Location = New System.Drawing.Point(10, 33)
        Me.txtSchemeName.MaxLength = 0
        Me.txtSchemeName.Name = "txtSchemeName"
        Me.txtSchemeName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSchemeName.Size = New System.Drawing.Size(223, 20)
        Me.txtSchemeName.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.txtSchemeName, "Ceci est la nom du th�me comme indiqu� dans toutes les fen�tres de Num�rotation TSG.")
        '
        'txtStyleName
        '
        Me.txtStyleName.AcceptsReturn = True
        Me.txtStyleName.BackColor = System.Drawing.SystemColors.Window
        Me.txtStyleName.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtStyleName.Enabled = False
        Me.txtStyleName.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStyleName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStyleName.Location = New System.Drawing.Point(10, 81)
        Me.txtStyleName.MaxLength = 0
        Me.txtStyleName.Name = "txtStyleName"
        Me.txtStyleName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtStyleName.Size = New System.Drawing.Size(223, 20)
        Me.txtStyleName.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.txtStyleName, "Ceci est le nom qui sera utilis� par tous les styles du th�me (ex. 'Article' dans" & _
        " 'Article_L1-9').")
        '
        'lblTOCScheme
        '
        Me.lblTOCScheme.BackColor = System.Drawing.SystemColors.Control
        Me.lblTOCScheme.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTOCScheme.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTOCScheme.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTOCScheme.Location = New System.Drawing.Point(11, 115)
        Me.lblTOCScheme.Name = "lblTOCScheme"
        Me.lblTOCScheme.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTOCScheme.Size = New System.Drawing.Size(142, 18)
        Me.lblTOCScheme.TabIndex = 4
        Me.lblTOCScheme.Text = "Th�me par d�faut &TM:"
        Me.ToolTip1.SetToolTip(Me.lblTOCScheme, " Ceci est le format par d�faut de la Table des mati�res pour un document qui util" & _
        "ise ce nouveau th�me.")
        '
        'lblSchemeName
        '
        Me.lblSchemeName.BackColor = System.Drawing.SystemColors.Control
        Me.lblSchemeName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSchemeName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSchemeName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSchemeName.Location = New System.Drawing.Point(11, 17)
        Me.lblSchemeName.Name = "lblSchemeName"
        Me.lblSchemeName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSchemeName.Size = New System.Drawing.Size(125, 18)
        Me.lblSchemeName.TabIndex = 0
        Me.lblSchemeName.Text = "Nom du &th�me:"
        Me.ToolTip1.SetToolTip(Me.lblSchemeName, "Ceci est la nom du th�me comme indiqu� dans toutes les fen�tres de Num�rotation TSG.")
        '
        'lblStyleName
        '
        Me.lblStyleName.BackColor = System.Drawing.SystemColors.Control
        Me.lblStyleName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStyleName.Enabled = False
        Me.lblStyleName.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStyleName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStyleName.Location = New System.Drawing.Point(9, 65)
        Me.lblStyleName.Name = "lblStyleName"
        Me.lblStyleName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStyleName.Size = New System.Drawing.Size(74, 18)
        Me.lblStyleName.TabIndex = 2
        Me.lblStyleName.Text = "Nom du style:"
        Me.ToolTip1.SetToolTip(Me.lblStyleName, "Ceci est le nom qui sera utilis� par tous les styles du th�me (ex. 'Article' dans" & _
        " 'Article_L1-9').")
        '
        'txtDescription
        '
        Me.txtDescription.AcceptsReturn = True
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDescription.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(262, 33)
        Me.txtDescription.MaxLength = 120
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDescription.Size = New System.Drawing.Size(215, 124)
        Me.txtDescription.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.txtDescription, "Ceci est la description du th�me comme indiqu� dans toutes les fen�tres de Num�rotation TSG.")
        '
        'lblDescription
        '
        Me.lblDescription.BackColor = System.Drawing.SystemColors.Control
        Me.lblDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDescription.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDescription.Location = New System.Drawing.Point(265, 16)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDescription.Size = New System.Drawing.Size(212, 18)
        Me.lblDescription.TabIndex = 6
        Me.lblDescription.Text = "Description (maximum 120 caract�res)"
        Me.ToolTip1.SetToolTip(Me.lblDescription, "Ceci est la description du th�me comme indiqu� dans toutes les fen�tres de Num�rotation TSG.")
        '
        'lblMsg
        '
        Me.lblMsg.BackColor = System.Drawing.SystemColors.Control
        Me.lblMsg.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMsg.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMsg.Location = New System.Drawing.Point(13, 247)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMsg.Size = New System.Drawing.Size(264, 37)
        Me.lblMsg.TabIndex = 12
        Me.lblMsg.Text = "###"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(12, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(125, 18)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Style Name:"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(12, 133)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(142, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Default &TOC Scheme:"
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.cmdCancel, Me.toolStripSeparator1, Me.cmdOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 290)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(490, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 16
        Me.tsButtons.Text = "ToolStrip1"
        '
        'cmdCancel
        '
        Me.cmdCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdCancel.AutoSize = False
        Me.cmdCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.cmdCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdCancel.Size = New System.Drawing.Size(60, 45)
        Me.cmdCancel.Text = "Annuler"
        Me.cmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'cmdOK
        '
        Me.cmdOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.cmdOK.AutoSize = False
        Me.cmdOK.Image = Global.MacPacNumbering.My.Resources.Resources.OK
        Me.cmdOK.ImageTransparentColor = System.Drawing.Color.White
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.cmdOK.Size = New System.Drawing.Size(60, 45)
        Me.cmdOK.Text = "O&K"
        Me.cmdOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 49)
        '
        'cmbTOCScheme
        '
        Me.cmbTOCScheme.AllowEmptyValue = False
        Me.cmbTOCScheme.AutoSize = True
        Me.cmbTOCScheme.Borderless = False
        Me.cmbTOCScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbTOCScheme.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTOCScheme.IsDirty = False
        Me.cmbTOCScheme.LimitToList = True
        Me.cmbTOCScheme.Location = New System.Drawing.Point(10, 131)
        Me.cmbTOCScheme.MaxDropDownItems = 8
        Me.cmbTOCScheme.Name = "cmbTOCScheme"
        Me.cmbTOCScheme.SelectedIndex = -1
        Me.cmbTOCScheme.SelectedValue = Nothing
        Me.cmbTOCScheme.SelectionLength = 0
        Me.cmbTOCScheme.SelectionStart = 0
        Me.cmbTOCScheme.Size = New System.Drawing.Size(223, 27)
        Me.cmbTOCScheme.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbTOCScheme.SupportingValues = ""
        Me.cmbTOCScheme.TabIndex = 5
        Me.cmbTOCScheme.Tag2 = Nothing
        Me.cmbTOCScheme.Value = ""
        '
        'frmSchemePropertiesFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(490, 339)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.cmbTOCScheme)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.chkAdjustSpacing)
        Me.Controls.Add(Me.chkBaseOnNormal)
        Me.Controls.Add(Me.txtSchemeName)
        Me.Controls.Add(Me.txtStyleName)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblTOCScheme)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSchemeName)
        Me.Controls.Add(Me.lblStyleName)
        Me.Controls.Add(Me.lblMsg)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSchemePropertiesFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "###"
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents cmdCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cmdOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region
End Class