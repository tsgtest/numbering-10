Option Strict Off
Option Explicit On

Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cStrings
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports LMP

Friend Class frmSchemePropertiesFrench
	Inherits System.Windows.Forms.Form
	
	Public Cancelled As Boolean
    Private m_oScheme As cNumScheme
	Private m_bReadOnly As Boolean '10/2/12
	
	
    Public Property Scheme() As cNumScheme
        Get
            Scheme = m_oScheme
        End Get
        Set(ByVal Value As cNumScheme)
            m_oScheme = Value
        End Set
    End Property
	
    Public Property IsReadOnly() As Boolean
        Get
            IsReadOnly = m_bReadOnly
        End Get
        Set(ByVal Value As Boolean)
            m_bReadOnly = Value
        End Set
    End Property
	
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            Cancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Try
            '   validate alias
            If Me.txtSchemeName.Text <> Me.Scheme.DisplayName Then
                If Not bSchemeNameIsValid((Me.txtSchemeName).Text) Then
                    Exit Sub
                End If
            End If
            Cancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmSchemePropertiesFrench_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = System.Windows.Forms.Keys.Escape Then
                cmdCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub frmSchemePropertiesFrench_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim i As Short
        Dim oScheme As cNumScheme
		Dim bEnabled As Boolean
		Dim xOrigAlias As String
		Dim bTOCInstalled As Boolean
        Dim xTOCField As String
        Dim xarTOCSchemes(,) As String

        Try
            Cancelled = True

            'adjust for scaling (10.0.12)
            Dim sFactorIncrease As Single = CScaling.GetScalingFactor()
            If sFactorIncrease > 1 Then
                Me.cmdOK.Width = Me.cmdOK.Width * sFactorIncrease
                Me.cmdCancel.Width = Me.cmdCancel.Width * sFactorIncrease
                Me.cmdOK.Height = Me.cmdOK.Height + (sFactorIncrease - 1) * 40
                Me.cmdCancel.Height = Me.cmdCancel.Height + (sFactorIncrease - 1) * 40
            End If

            Me.cmbTOCScheme.Visible = g_bAllowTOCLink
            Me.lblTOCScheme.Visible = g_bAllowTOCLink

            If g_bAllowTOCLink Then
                '       first ensure TOC link - may not be there if
                '       Word was started with a protected document
                lInitializeTOCLink()

                ReDim xarTOCSchemes(UBound(g_xTOCSchemes), 1)
                For i = LBound(g_xTOCSchemes) To UBound(g_xTOCSchemes)
                    xarTOCSchemes(i, 0) = g_xTOCSchemes(i, 0)
                    xarTOCSchemes(i, 1) = g_xTOCSchemes(i, 1)
                Next i
                Me.cmbTOCScheme.SetList(xarTOCSchemes)
            End If

            'moved from Activate event
            With Me.Scheme
                'On Error Resume Next
                'Me.Top = VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(frmSchemes.Top) + 743)
                'Me.Left = VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(frmSchemes.Left) + 135)
                'On Error GoTo 0

                '       fill controls on form with scheme properties
                Me.txtSchemeName.Text = .DisplayName
                If (.SchemeType = mpSchemeTypes.mpSchemeType_Document) And bIsHeadingScheme(.Name) Then
                    Me.txtStyleName.Text = "Heading"
                Else
                    Me.txtStyleName.Text = xGetStyleRoot(.Name)
                End If
                If g_bAllowTOCLink Then
                    Try
                        xTOCField = GetTOCField(CShort(.TOCScheme), mpTOCRecordFields.mpTOCRecField_Alias, mpSchemeTypes.mpSchemeType_TOC)
                    Catch
                    End Try
                    If xTOCField <> "" Then
                        Try
                            Me.cmbTOCScheme.Value = xTOCField
                        Catch
                        End Try
                    End If
                End If
                Me.chkBaseOnNormal.CheckState = .DymanicFonts
                Me.chkAdjustSpacing.CheckState = .DynamicSpacing
                Me.txtDescription.Text = .Description

                If (.SchemeType = mpSchemeTypes.mpSchemeType_Private) And Not Me.IsReadOnly Then
                    '            Me.lblMsg.Left = Me.cmdOK.Left
                    Me.lblMsg.Text = .DisplayName & " est un th�me Priv�."
                Else
                    '           disable controls if scheme type is not private -
                    '           dynamic fonts control enabling is set in form load
                    Me.lblSchemeName.Enabled = False
                    If g_bAllowTOCLink Then
                        Me.lblTOCScheme.Enabled = False
                        Me.cmbTOCScheme.Enabled = False
                    End If
                    Me.txtSchemeName.Enabled = False
                    Me.chkBaseOnNormal.Enabled = False
                    Me.chkAdjustSpacing.Enabled = False
                    Me.lblDescription.Enabled = False
                    Me.txtDescription.Enabled = False
                    Me.cmdOK.Visible = False
                    Me.toolStripSeparator1.Visible = False
                    Me.cmdCancel.Text = "&Fermer"

                    '           remove hotkeys
                    Me.lblSchemeName.Text = xSubstitute(Me.lblSchemeName.Text, "&", "")
                    If g_bAllowTOCLink Then Me.lblTOCScheme.Text = xSubstitute(Me.lblTOCScheme.Text, "&", "")

                    '           set message
                    If .SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                        Me.lblMsg.Text = .DisplayName & " est un th�me de document." & _
                                  vbCrLf & "Les propri�t�s du th�me ne peuvent �tre �dit�es."
                    ElseIf .SchemeType = mpSchemeTypes.mpSchemeType_Public Then
                        Me.lblMsg.Text = .DisplayName & " est un th�me Public." & _
                          vbCrLf & "Les propri�t�s du th�me ne peuvent �tre �dit�es."
                    Else
                        '                Me.lblMsg.Alignment = vbCenter
                        Me.lblMsg.Text = .DisplayName & " est un th�me Priv�."
                    End If
                End If

                '       set form caption
                Me.Text = " Propri�t�s du th�me " & .DisplayName
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class