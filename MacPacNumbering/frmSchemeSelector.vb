Option Strict Off
Option Explicit On

Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports LMP

Friend Class frmSchemeSelector
	Inherits System.Windows.Forms.Form
	
	Private m_bCancelled As Boolean
	
	Public ReadOnly Property Cancelled() As Boolean
		Get
			Cancelled = m_bCancelled
		End Get
	End Property
	
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            m_bCancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
        Try
            m_bCancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub frmSchemeSelector_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim i As Short
		Dim xActive As String

        Try
            m_bCancelled = True

            'iGetSchemes sorts by display name, so order of list box and g_xDSchemes
            'will always match - no need for item data
            iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
            xActive = xActiveScheme(CurWordApp.ActiveDocument)

            For i = 0 To UBound(g_xDSchemes)
                With Me.lstSchemes
                    .Items.Add(g_xDSchemes(i, 1))
                    If g_xDSchemes(i, 0) = xActive Then .SelectedIndex = i
                End With
            Next i
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class