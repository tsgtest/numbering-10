<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSchemes
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents mnuScheme_New As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Modify As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Delete As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Properties As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Sep3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuScheme_Use As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_ChangeTo As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Reset As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Sep2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuScheme_Relink As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_ConvertToHeadingStyles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_ConvertToMacPacStyles As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Sep1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuScheme_SetAsDefault As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_Favorites As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_UseWordHeadings As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme_InsertLevel1Automatically As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuScheme As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_New As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Modify As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Delete As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Properties As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Sep3 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuContext_Use As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_ChangeTo As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Reset As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Sep2 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuContext_Relink As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_ConvertToHeadingStyles As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_ConvertToMacPacStyles As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Sep1 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuContext_SetAsDefault As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_Favorites As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_UseWordHeadings As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext_InsertLevel1Automatically As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuContext As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuSharing_ExportToFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSharing_ImportFromFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSharing_Sep1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuSharing_ImportPublicScheme As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSharing_ExportToPublic As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSharing_DeletePublicSchemes As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuSharing As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuHelp_Sep1 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents mnuHelp_About As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
    Public WithEvents txtDescription As System.Windows.Forms.TextBox
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchemes))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
        Me.mnuScheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_New = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Modify = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Delete = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Properties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Sep3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuScheme_Use = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_ChangeTo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Reset = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Sep2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuScheme_Relink = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_ConvertToHeadingStyles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_ConvertToMacPacStyles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Sep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuScheme_SetAsDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_Favorites = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_UseWordHeadings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuScheme_InsertLevel1Automatically = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing_ExportToFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing_ImportFromFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing_Sep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSharing_ImportPublicScheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing_ExportToPublic = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSharing_DeletePublicSchemes = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp_Sep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuHelp_About = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_New = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Modify = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Delete = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Properties = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Sep3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuContext_Use = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_ChangeTo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Reset = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Sep2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuContext_Relink = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_ConvertToHeadingStyles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_ConvertToMacPacStyles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Sep1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuContext_SetAsDefault = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_Favorites = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_UseWordHeadings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuContext_InsertLevel1Automatically = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.vsbPreview = New System.Windows.Forms.VScrollBar()
        Me.tsToolbar = New System.Windows.Forms.ToolStrip()
        Me.btnReload = New System.Windows.Forms.ToolStripButton()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnReset = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnChangeScheme = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnUse = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.imgPreview = New System.Windows.Forms.PictureBox()
        Me.Image1 = New System.Windows.Forms.PictureBox()
        Me.scMain = New System.Windows.Forms.SplitContainer()
        Me.tvwSchemes = New System.Windows.Forms.TreeView()
        Me.pnlRight = New System.Windows.Forms.Panel()
        Me.pnlBottom = New System.Windows.Forms.Panel()
        Me.pnlFill = New System.Windows.Forms.Panel()
        Me.MainMenu1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.tsToolbar.SuspendLayout()
        CType(Me.imgPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.scMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.scMain.Panel1.SuspendLayout()
        Me.scMain.Panel2.SuspendLayout()
        Me.scMain.SuspendLayout()
        Me.pnlRight.SuspendLayout()
        Me.pnlBottom.SuspendLayout()
        Me.pnlFill.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScheme, Me.mnuSharing, Me.mnuHelp})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(512, 24)
        Me.MainMenu1.TabIndex = 11
        '
        'mnuScheme
        '
        Me.mnuScheme.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScheme_New, Me.mnuScheme_Modify, Me.mnuScheme_Delete, Me.mnuScheme_Properties, Me.mnuScheme_Sep3, Me.mnuScheme_Use, Me.mnuScheme_ChangeTo, Me.mnuScheme_Reset, Me.mnuScheme_Sep2, Me.mnuScheme_Relink, Me.mnuScheme_ConvertToHeadingStyles, Me.mnuScheme_ConvertToMacPacStyles, Me.mnuScheme_Sep1, Me.mnuScheme_SetAsDefault, Me.mnuScheme_Favorites, Me.mnuScheme_UseWordHeadings, Me.mnuScheme_InsertLevel1Automatically})
        Me.mnuScheme.Name = "mnuScheme"
        Me.mnuScheme.Size = New System.Drawing.Size(61, 20)
        Me.mnuScheme.Text = "&Scheme"
        '
        'mnuScheme_New
        '
        Me.mnuScheme_New.Name = "mnuScheme_New"
        Me.mnuScheme_New.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_New.Text = "&New..."
        '
        'mnuScheme_Modify
        '
        Me.mnuScheme_Modify.Name = "mnuScheme_Modify"
        Me.mnuScheme_Modify.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Modify.Text = "&Edit..."
        '
        'mnuScheme_Delete
        '
        Me.mnuScheme_Delete.Name = "mnuScheme_Delete"
        Me.mnuScheme_Delete.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Delete.Text = "&Delete..."
        '
        'mnuScheme_Properties
        '
        Me.mnuScheme_Properties.Name = "mnuScheme_Properties"
        Me.mnuScheme_Properties.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Properties.Text = "&Properties..."
        '
        'mnuScheme_Sep3
        '
        Me.mnuScheme_Sep3.Name = "mnuScheme_Sep3"
        Me.mnuScheme_Sep3.Size = New System.Drawing.Size(240, 6)
        '
        'mnuScheme_Use
        '
        Me.mnuScheme_Use.Name = "mnuScheme_Use"
        Me.mnuScheme_Use.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Use.Text = "&Use"
        '
        'mnuScheme_ChangeTo
        '
        Me.mnuScheme_ChangeTo.Name = "mnuScheme_ChangeTo"
        Me.mnuScheme_ChangeTo.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_ChangeTo.Text = "C&hange to..."
        '
        'mnuScheme_Reset
        '
        Me.mnuScheme_Reset.Name = "mnuScheme_Reset"
        Me.mnuScheme_Reset.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Reset.Text = "&Reset..."
        '
        'mnuScheme_Sep2
        '
        Me.mnuScheme_Sep2.Name = "mnuScheme_Sep2"
        Me.mnuScheme_Sep2.Size = New System.Drawing.Size(240, 6)
        '
        'mnuScheme_Relink
        '
        Me.mnuScheme_Relink.Name = "mnuScheme_Relink"
        Me.mnuScheme_Relink.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Relink.Text = "Re&link Document Schemes"
        '
        'mnuScheme_ConvertToHeadingStyles
        '
        Me.mnuScheme_ConvertToHeadingStyles.Name = "mnuScheme_ConvertToHeadingStyles"
        Me.mnuScheme_ConvertToHeadingStyles.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_ConvertToHeadingStyles.Text = "&Convert to Word Heading Styles"
        '
        'mnuScheme_ConvertToMacPacStyles
        '
        Me.mnuScheme_ConvertToMacPacStyles.Name = "mnuScheme_ConvertToMacPacStyles"
        Me.mnuScheme_ConvertToMacPacStyles.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_ConvertToMacPacStyles.Text = "&Convert to TSG Styles"
        '
        'mnuScheme_Sep1
        '
        Me.mnuScheme_Sep1.Name = "mnuScheme_Sep1"
        Me.mnuScheme_Sep1.Size = New System.Drawing.Size(240, 6)
        '
        'mnuScheme_SetAsDefault
        '
        Me.mnuScheme_SetAsDefault.Name = "mnuScheme_SetAsDefault"
        Me.mnuScheme_SetAsDefault.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_SetAsDefault.Text = "&Set as Default"
        '
        'mnuScheme_Favorites
        '
        Me.mnuScheme_Favorites.Name = "mnuScheme_Favorites"
        Me.mnuScheme_Favorites.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_Favorites.Text = "Add to &Favorites"
        '
        'mnuScheme_UseWordHeadings
        '
        Me.mnuScheme_UseWordHeadings.Name = "mnuScheme_UseWordHeadings"
        Me.mnuScheme_UseWordHeadings.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_UseWordHeadings.Text = "Use &Word Headings"
        '
        'mnuScheme_InsertLevel1Automatically
        '
        Me.mnuScheme_InsertLevel1Automatically.Name = "mnuScheme_InsertLevel1Automatically"
        Me.mnuScheme_InsertLevel1Automatically.Size = New System.Drawing.Size(243, 22)
        Me.mnuScheme_InsertLevel1Automatically.Text = "&Insert Level 1 Automatically"
        '
        'mnuSharing
        '
        Me.mnuSharing.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSharing_ExportToFile, Me.mnuSharing_ImportFromFile, Me.mnuSharing_Sep1, Me.mnuSharing_ImportPublicScheme, Me.mnuSharing_ExportToPublic, Me.mnuSharing_DeletePublicSchemes})
        Me.mnuSharing.Name = "mnuSharing"
        Me.mnuSharing.Size = New System.Drawing.Size(48, 20)
        Me.mnuSharing.Text = "Sh&are"
        '
        'mnuSharing_ExportToFile
        '
        Me.mnuSharing_ExportToFile.Name = "mnuSharing_ExportToFile"
        Me.mnuSharing_ExportToFile.Size = New System.Drawing.Size(171, 22)
        Me.mnuSharing_ExportToFile.Text = "&Export To File"
        Me.mnuSharing_ExportToFile.Visible = False
        '
        'mnuSharing_ImportFromFile
        '
        Me.mnuSharing_ImportFromFile.Name = "mnuSharing_ImportFromFile"
        Me.mnuSharing_ImportFromFile.Size = New System.Drawing.Size(171, 22)
        Me.mnuSharing_ImportFromFile.Text = "&Import From File..."
        Me.mnuSharing_ImportFromFile.Visible = False
        '
        'mnuSharing_Sep1
        '
        Me.mnuSharing_Sep1.Name = "mnuSharing_Sep1"
        Me.mnuSharing_Sep1.Size = New System.Drawing.Size(168, 6)
        Me.mnuSharing_Sep1.Visible = False
        '
        'mnuSharing_ImportPublicScheme
        '
        Me.mnuSharing_ImportPublicScheme.Name = "mnuSharing_ImportPublicScheme"
        Me.mnuSharing_ImportPublicScheme.Size = New System.Drawing.Size(171, 22)
        Me.mnuSharing_ImportPublicScheme.Text = "I&mport..."
        '
        'mnuSharing_ExportToPublic
        '
        Me.mnuSharing_ExportToPublic.Name = "mnuSharing_ExportToPublic"
        Me.mnuSharing_ExportToPublic.Size = New System.Drawing.Size(171, 22)
        Me.mnuSharing_ExportToPublic.Text = "&Export..."
        '
        'mnuSharing_DeletePublicSchemes
        '
        Me.mnuSharing_DeletePublicSchemes.Name = "mnuSharing_DeletePublicSchemes"
        Me.mnuSharing_DeletePublicSchemes.Size = New System.Drawing.Size(171, 22)
        Me.mnuSharing_DeletePublicSchemes.Text = "&Delete..."
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelp_Sep1, Me.mnuHelp_About})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "&Help"
        '
        'mnuHelp_Sep1
        '
        Me.mnuHelp_Sep1.Name = "mnuHelp_Sep1"
        Me.mnuHelp_Sep1.Size = New System.Drawing.Size(201, 6)
        '
        'mnuHelp_About
        '
        Me.mnuHelp_About.Name = "mnuHelp_About"
        Me.mnuHelp_About.Size = New System.Drawing.Size(204, 22)
        Me.mnuHelp_About.Text = "&About TSG Numbering..."
        '
        'mnuContext_New
        '
        Me.mnuContext_New.Name = "mnuContext_New"
        Me.mnuContext_New.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_New.Text = "&New..."
        '
        'mnuContext_Modify
        '
        Me.mnuContext_Modify.Name = "mnuContext_Modify"
        Me.mnuContext_Modify.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Modify.Text = "&Edit..."
        '
        'mnuContext_Delete
        '
        Me.mnuContext_Delete.Name = "mnuContext_Delete"
        Me.mnuContext_Delete.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Delete.Text = "&Delete..."
        '
        'mnuContext_Properties
        '
        Me.mnuContext_Properties.Name = "mnuContext_Properties"
        Me.mnuContext_Properties.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Properties.Text = "&Properties..."
        '
        'mnuContext_Sep3
        '
        Me.mnuContext_Sep3.Name = "mnuContext_Sep3"
        Me.mnuContext_Sep3.Size = New System.Drawing.Size(240, 6)
        '
        'mnuContext_Use
        '
        Me.mnuContext_Use.Name = "mnuContext_Use"
        Me.mnuContext_Use.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Use.Text = "&Use"
        '
        'mnuContext_ChangeTo
        '
        Me.mnuContext_ChangeTo.Name = "mnuContext_ChangeTo"
        Me.mnuContext_ChangeTo.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_ChangeTo.Text = "C&hange to..."
        '
        'mnuContext_Reset
        '
        Me.mnuContext_Reset.Name = "mnuContext_Reset"
        Me.mnuContext_Reset.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Reset.Text = "&Reset..."
        '
        'mnuContext_Sep2
        '
        Me.mnuContext_Sep2.Name = "mnuContext_Sep2"
        Me.mnuContext_Sep2.Size = New System.Drawing.Size(240, 6)
        '
        'mnuContext_Relink
        '
        Me.mnuContext_Relink.Name = "mnuContext_Relink"
        Me.mnuContext_Relink.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Relink.Text = "Re&link Document Schemes"
        '
        'mnuContext_ConvertToHeadingStyles
        '
        Me.mnuContext_ConvertToHeadingStyles.Name = "mnuContext_ConvertToHeadingStyles"
        Me.mnuContext_ConvertToHeadingStyles.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_ConvertToHeadingStyles.Text = "&Convert to Word Heading Styles"
        '
        'mnuContext_ConvertToMacPacStyles
        '
        Me.mnuContext_ConvertToMacPacStyles.Name = "mnuContext_ConvertToMacPacStyles"
        Me.mnuContext_ConvertToMacPacStyles.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_ConvertToMacPacStyles.Text = "&Convert to TSG Styles"
        '
        'mnuContext_Sep1
        '
        Me.mnuContext_Sep1.Name = "mnuContext_Sep1"
        Me.mnuContext_Sep1.Size = New System.Drawing.Size(240, 6)
        '
        'mnuContext_SetAsDefault
        '
        Me.mnuContext_SetAsDefault.Name = "mnuContext_SetAsDefault"
        Me.mnuContext_SetAsDefault.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_SetAsDefault.Text = "&Set as Default"
        '
        'mnuContext_Favorites
        '
        Me.mnuContext_Favorites.Name = "mnuContext_Favorites"
        Me.mnuContext_Favorites.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_Favorites.Text = "Add to &Favorites"
        '
        'mnuContext_UseWordHeadings
        '
        Me.mnuContext_UseWordHeadings.Name = "mnuContext_UseWordHeadings"
        Me.mnuContext_UseWordHeadings.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_UseWordHeadings.Text = "Use &Word Headings"
        '
        'mnuContext_InsertLevel1Automatically
        '
        Me.mnuContext_InsertLevel1Automatically.Name = "mnuContext_InsertLevel1Automatically"
        Me.mnuContext_InsertLevel1Automatically.Size = New System.Drawing.Size(243, 22)
        Me.mnuContext_InsertLevel1Automatically.Text = "&Insert Level 1 Automatically"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuContext_New, Me.mnuContext_Modify, Me.mnuContext_Delete, Me.mnuContext_Properties, Me.mnuContext_Sep3, Me.mnuContext_Use, Me.mnuContext_ChangeTo, Me.mnuContext_Reset, Me.mnuContext_Sep2, Me.mnuContext_Relink, Me.mnuContext_ConvertToHeadingStyles, Me.mnuContext_ConvertToMacPacStyles, Me.mnuContext_Sep1, Me.mnuContext_SetAsDefault, Me.mnuContext_Favorites, Me.mnuContext_UseWordHeadings, Me.mnuContext_InsertLevel1Automatically})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(244, 330)
        '
        'txtDescription
        '
        Me.txtDescription.AcceptsReturn = True
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Control
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDescription.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.MaxLength = 255
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(512, 29)
        Me.txtDescription.TabIndex = 9
        Me.txtDescription.TabStop = False
        '
        'vsbPreview
        '
        Me.vsbPreview.Cursor = System.Windows.Forms.Cursors.Default
        Me.vsbPreview.Dock = System.Windows.Forms.DockStyle.Right
        Me.vsbPreview.LargeChange = 994
        Me.vsbPreview.Location = New System.Drawing.Point(255, 26)
        Me.vsbPreview.Maximum = 4968
        Me.vsbPreview.Name = "vsbPreview"
        Me.vsbPreview.Size = New System.Drawing.Size(16, 363)
        Me.vsbPreview.SmallChange = 398
        Me.vsbPreview.TabIndex = 13
        Me.vsbPreview.TabStop = True
        Me.vsbPreview.Value = 375
        '
        'tsToolbar
        '
        Me.tsToolbar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsToolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsToolbar.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsToolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnReload, Me.btnCancel, Me.toolStripSeparator3, Me.btnReset, Me.toolStripSeparator1, Me.btnChangeScheme, Me.toolStripSeparator2, Me.btnUse, Me.ToolStripSeparator5})
        Me.tsToolbar.Location = New System.Drawing.Point(0, 29)
        Me.tsToolbar.Name = "tsToolbar"
        Me.tsToolbar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsToolbar.Size = New System.Drawing.Size(512, 53)
        Me.tsToolbar.Stretch = True
        Me.tsToolbar.TabIndex = 17
        Me.tsToolbar.Text = "ToolStrip1"
        '
        'btnReload
        '
        Me.btnReload.AutoSize = False
        Me.btnReload.Image = Global.MacPacNumbering.My.Resources.Resources.Refresh
        Me.btnReload.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnReload.Name = "btnReload"
        Me.btnReload.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnReload.Size = New System.Drawing.Size(123, 50)
        Me.btnReload.Text = "&Load Public Schemes"
        Me.btnReload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnReload.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Image = Global.MacPacNumbering.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 53)
        '
        'btnReset
        '
        Me.btnReset.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnReset.AutoSize = False
        Me.btnReset.Image = Global.MacPacNumbering.My.Resources.Resources.Refresh
        Me.btnReset.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnReset.Size = New System.Drawing.Size(60, 45)
        Me.btnReset.Text = "&Reset..."
        Me.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 53)
        '
        'btnChangeScheme
        '
        Me.btnChangeScheme.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnChangeScheme.AutoSize = False
        Me.btnChangeScheme.Image = Global.MacPacNumbering.My.Resources.Resources.Change
        Me.btnChangeScheme.ImageTransparentColor = System.Drawing.Color.White
        Me.btnChangeScheme.Name = "btnChangeScheme"
        Me.btnChangeScheme.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnChangeScheme.Size = New System.Drawing.Size(80, 45)
        Me.btnChangeScheme.Text = "Cha&nge To..."
        Me.btnChangeScheme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator2
        '
        Me.toolStripSeparator2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator2.Name = "toolStripSeparator2"
        Me.toolStripSeparator2.Size = New System.Drawing.Size(6, 53)
        '
        'btnUse
        '
        Me.btnUse.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnUse.AutoSize = False
        Me.btnUse.Image = Global.MacPacNumbering.My.Resources.Resources.Use
        Me.btnUse.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnUse.Name = "btnUse"
        Me.btnUse.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnUse.Size = New System.Drawing.Size(60, 45)
        Me.btnUse.Text = "&Use"
        Me.btnUse.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 53)
        Me.ToolStripSeparator5.Visible = False
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 48)
        '
        'imgPreview
        '
        Me.imgPreview.BackColor = System.Drawing.Color.White
        Me.imgPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.imgPreview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.imgPreview.Location = New System.Drawing.Point(0, 0)
        Me.imgPreview.Name = "imgPreview"
        Me.imgPreview.Size = New System.Drawing.Size(255, 363)
        Me.imgPreview.TabIndex = 14
        Me.imgPreview.TabStop = False
        '
        'Image1
        '
        Me.Image1.BackgroundImage = Global.MacPacNumbering.My.Resources.Resources.Ruler
        Me.Image1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Image1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Image1.Location = New System.Drawing.Point(0, 0)
        Me.Image1.Name = "Image1"
        Me.Image1.Size = New System.Drawing.Size(271, 26)
        Me.Image1.TabIndex = 16
        Me.Image1.TabStop = False
        '
        'scMain
        '
        Me.scMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.scMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scMain.IsSplitterFixed = True
        Me.scMain.Location = New System.Drawing.Point(0, 0)
        Me.scMain.Name = "scMain"
        '
        'scMain.Panel1
        '
        Me.scMain.Panel1.Controls.Add(Me.tvwSchemes)
        '
        'scMain.Panel2
        '
        Me.scMain.Panel2.Controls.Add(Me.pnlRight)
        Me.scMain.Panel2.Controls.Add(Me.vsbPreview)
        Me.scMain.Panel2.Controls.Add(Me.Image1)
        Me.scMain.Panel2.Enabled = False
        Me.scMain.Size = New System.Drawing.Size(512, 391)
        Me.scMain.SplitterDistance = 238
        Me.scMain.SplitterWidth = 1
        Me.scMain.TabIndex = 18
        '
        'tvwSchemes
        '
        Me.tvwSchemes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvwSchemes.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvwSchemes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvwSchemes.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvwSchemes.HideSelection = False
        Me.tvwSchemes.Indent = 23
        Me.tvwSchemes.ItemHeight = 18
        Me.tvwSchemes.Location = New System.Drawing.Point(0, 0)
        Me.tvwSchemes.Name = "tvwSchemes"
        Me.tvwSchemes.Size = New System.Drawing.Size(236, 389)
        Me.tvwSchemes.Sorted = True
        Me.tvwSchemes.TabIndex = 1
        '
        'pnlRight
        '
        Me.pnlRight.AutoSize = True
        Me.pnlRight.BackColor = System.Drawing.Color.White
        Me.pnlRight.Controls.Add(Me.imgPreview)
        Me.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlRight.Location = New System.Drawing.Point(0, 26)
        Me.pnlRight.Name = "pnlRight"
        Me.pnlRight.Size = New System.Drawing.Size(255, 363)
        Me.pnlRight.TabIndex = 17
        '
        'pnlBottom
        '
        Me.pnlBottom.Controls.Add(Me.txtDescription)
        Me.pnlBottom.Controls.Add(Me.tsToolbar)
        Me.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlBottom.Location = New System.Drawing.Point(0, 415)
        Me.pnlBottom.Name = "pnlBottom"
        Me.pnlBottom.Size = New System.Drawing.Size(512, 82)
        Me.pnlBottom.TabIndex = 19
        '
        'pnlFill
        '
        Me.pnlFill.Controls.Add(Me.scMain)
        Me.pnlFill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFill.Location = New System.Drawing.Point(0, 24)
        Me.pnlFill.Name = "pnlFill"
        Me.pnlFill.Size = New System.Drawing.Size(512, 391)
        Me.pnlFill.TabIndex = 20
        '
        'frmSchemes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(512, 497)
        Me.Controls.Add(Me.pnlFill)
        Me.Controls.Add(Me.pnlBottom)
        Me.Controls.Add(Me.MainMenu1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(440, 339)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSchemes"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Numbering Schemes"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.tsToolbar.ResumeLayout(False)
        Me.tsToolbar.PerformLayout()
        CType(Me.imgPreview, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Image1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.scMain.Panel1.ResumeLayout(False)
        Me.scMain.Panel2.ResumeLayout(False)
        Me.scMain.Panel2.PerformLayout()
        CType(Me.scMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.scMain.ResumeLayout(False)
        Me.pnlRight.ResumeLayout(False)
        Me.pnlBottom.ResumeLayout(False)
        Me.pnlBottom.PerformLayout()
        Me.pnlFill.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents vsbPreview As System.Windows.Forms.VScrollBar
    Friend WithEvents tsToolbar As System.Windows.Forms.ToolStrip
    Friend WithEvents btnReload As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnReset As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnChangeScheme As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnUse As System.Windows.Forms.ToolStripButton
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Private WithEvents imgPreview As System.Windows.Forms.PictureBox
    Public WithEvents Image1 As System.Windows.Forms.PictureBox
    Friend WithEvents scMain As System.Windows.Forms.SplitContainer
    Public WithEvents tvwSchemes As System.Windows.Forms.TreeView
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents pnlBottom As System.Windows.Forms.Panel
    Friend WithEvents pnlFill As System.Windows.Forms.Panel
    Friend WithEvents pnlRight As System.Windows.Forms.Panel
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
#End Region 
End Class