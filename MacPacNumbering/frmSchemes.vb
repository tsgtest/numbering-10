Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlWord14
Imports MacPacNumbering.LMP.Numbering.UserFunctions_Numbering
Imports MacPacNumbering.LMP.Numbering.mdlConversions
Imports MacPacNumbering.LMP.Numbering.mpTypes
Imports MacPacNumbering.LMP.Numbering.mpDocument
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cStrings
Imports LMP

Friend Class frmSchemes
    Inherits System.Windows.Forms.Form

    Const mpInvalidValue As Short = 380

    Private vbControls() As String
    Public m_iEditMode As Short
    Public m_bFinished As Boolean
    Private m_sTop As Single
    Private m_sHeight As Single
    Private m_sLeft As Single
    Private m_sWidth As Single
    Private m_iDocSchemes As Short
    Private m_bSavePrivateStyFile As Boolean
    Private WithEvents m_oMenuArray As HelpMenuArray

    Public ReadOnly Property Scheme() As String
        Get
            On Error Resume Next
            If Me.tvwSchemes.SelectedNode.Parent.Text = mpFavoriteSchemes Then
                Scheme = Mid(Me.tvwSchemes.SelectedNode.Name, 4)
            Else
                Scheme = Mid(Me.tvwSchemes.SelectedNode.Name, 2)
            End If
        End Get
    End Property

    Public ReadOnly Property SchemeDisplayName() As String
        Get
            On Error Resume Next
            SchemeDisplayName = Me.tvwSchemes.SelectedNode.Text
        End Get
    End Property

    Public ReadOnly Property SchemeType() As mpSchemeTypes
        Get
            Dim iIndex As Short
            Dim oParent As System.Windows.Forms.TreeNode

            On Error Resume Next
            oParent = Me.tvwSchemes.SelectedNode.Parent
            On Error GoTo 0

            If oParent Is Nothing Then
                SchemeType = mpSchemeTypes.mpSchemeType_Category
                Exit Property
            Else
                iIndex = oParent.Index
            End If

            'GLOG 5349 - account for absence of Private Schemes node
            If (iIndex > 1) And Not g_bShowPersonalSchemes Then iIndex = iIndex + 1

            Select Case iIndex
                Case 0
                    SchemeType = mpSchemeTypes.mpSchemeType_Document
                Case 1
                    'favorites node
                    If VB.Left(Me.tvwSchemes.SelectedNode.Name, 3) = CStr(mpFavoritePublic) Then
                        SchemeType = mpSchemeTypes.mpSchemeType_Public
                    Else
                        SchemeType = mpSchemeTypes.mpSchemeType_Private
                    End If
                Case 2
                    SchemeType = mpSchemeTypes.mpSchemeType_Private
                Case 3
                    SchemeType = mpSchemeTypes.mpSchemeType_Public
            End Select


            '   this is a workaround for missing public node
            If g_bIsAdmin And (SchemeType = mpSchemeTypes.mpSchemeType_Public) Then SchemeType = mpSchemeTypes.mpSchemeType_Private
        End Get
    End Property


    Property SavePrivateStyFile() As Boolean
        Get
            SavePrivateStyFile = m_bSavePrivateStyFile
        End Get
        Set(ByVal Value As Boolean)
            m_bSavePrivateStyFile = Value
        End Set
    End Property

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Me.m_iEditMode = mpModeSchemeDone
            Me.Close()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnChangeScheme_Click(sender As Object, e As EventArgs) Handles btnChangeScheme.Click
        Dim xScheme As String
        Dim xAlias As String
        Dim xHScheme As String
        Dim bUseWordHeadings As Boolean
        Dim styScheme As Word.Style
        Dim oScheme As cNumScheme
        Dim xWindow As String
        Dim lListParas As Integer
        Dim bTrackRevisions As Boolean

        Try
            'prompt to save document if necessary
            'UPGRADE_WARNING: Couldn't resolve default property of object WordBasic.FileNameFromWindow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If (Me.SchemeType <> mpSchemeTypes.mpSchemeType_Document) And g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow() = "") Then
                MsgBox(mpOrganizerSavePrompt, MsgBoxStyle.Information, g_xAppName)
                Exit Sub
            End If

            '   if doc scheme, ensure that proprietary styles exist;
            '   if not, prompt to reset; this will be necessary after
            '   copying numbered paras using Word Heading styles to new doc
            '   in which scheme is not loaded but Headings 1-9 are "in use"
            If (Me.SchemeType = mpSchemeTypes.mpSchemeType_Document) And (Me.Scheme <> "HeadingStyles") Then
                Try
                    styScheme = CurWordApp.ActiveDocument.Styles(xGetStyleRoot(Me.Scheme) & "_L1")
                Catch
                End Try

                If styScheme Is Nothing Then
                    MsgBox("The " & Me.SchemeDisplayName & " document scheme is " & "missing some of its components.  It may have been " & "partially copied from another document.  Please " & "click the Reset button to restore this scheme to full " & "functionality.", MsgBoxStyle.Critical, g_xAppName)
                    Exit Sub
                End If
            End If

            'message user if doc isn't in Word 2010 compatibility mode and scheme requires it
            If Me.SchemeType <> mpSchemeTypes.mpSchemeType_Document Then
                If CurWordApp.ActiveDocument.CompatibilityMode < 14 Then
                    If ContainsWord2010NumberStyle(Me.Scheme, Me.SchemeType) Then
                        MsgBox("The specified scheme cannot be loaded because it contains a " & "numbering style that is only available for documents that are " & "in Word 2010 compatibility mode.", MsgBoxStyle.Information, g_xAppName)
                        Exit Sub
                    End If
                End If
            End If

            'GLOG 5461 (9.9.5012)
            If CurWordApp.ActiveDocument.TrackRevisions Then
                lListParas = CurWordApp.ActiveDocument.ListParagraphs.Count
                If lListParas >= g_lTrackChangesWarningThreshold Then
                    lRet = MsgBox("This document contains " & CStr(lListParas) & " numbered " & "paragraphs.  In a document this size, we recommend turning off track " & "changes during the Change To process to avoid a potentially lengthy wait." & vbCr & vbCr & "Would you like us to turn off track changes before proceeding and then turn it " & "back on at the end of the process?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.Question, AppName)
                    If lRet = MsgBoxResult.Yes Then
                        bTrackRevisions = True
                        CurWordApp.ActiveDocument.TrackRevisions = False
                    ElseIf lRet = MsgBoxResult.Cancel Then
                        Exit Sub
                    End If
                End If
            End If

            Me.m_iEditMode = mpModeSchemeDone
            bUseWordHeadings = Me.mnuScheme_UseWordHeadings.Checked

            '9/26/16 - Hide() was switching focus from Word, so  now calling Close() immediately
            Dim oType As mpSchemeTypes
            xScheme = Me.Scheme
            oType = Me.SchemeType
            Me.Close()

            '   change scheme
            oScheme = GetRecord(xScheme, oType)
            iChangeScheme(xScheme, oType, xAlias, oType <> mpSchemeTypes.mpSchemeType_Document, , , , bUseWordHeadings, (oScheme.DymanicFonts))
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            If xWindow <> "" Then CurWordApp.WordBasic.Activate(xWindow)
            'GLOG 5461
            If bTrackRevisions Then CurWordApp.ActiveDocument.TrackRevisions = True
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()
        End Try
    End Sub

    Private Sub btnReload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
        Try
            If g_xPSchemes(0, 0) <> "" Then
                lRet = MsgBox("Are you sure you want to replace all schemes " & "in your Admin directory with the current public schemes?  " & New String(Chr(13), 2) & "If you want to save the set of schemes on which you are currently working, " & "you should instead click No, close the dialog box, and specify a new Admin directory " & "in " & cNumTOC.SettingsFileName & ".  You can later return to this set by pointing back to the old directory.", MsgBoxStyle.Question + MsgBoxStyle.YesNo, g_xAppName)
                If lRet = MsgBoxResult.No Then Exit Sub
            End If

            LoadPublicSchemesAsAdmin()
            RefreshSchemesList((Me.tvwSchemes))
            tvwSchemes.SelectedNode = tvwSchemes.Nodes(1)
            Me.tvwSchemes.SelectedNode.Expand()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Dim xScheme As String
        Dim styScheme As Word.Style
        Dim iOrigin As mpSchemeTypes
        Dim bOriginFound As Boolean
        Dim tmpSource As Word.Template
        Dim bIsHeading As Boolean
        Dim xMsg As String
        Dim ltSource As Word.ListTemplate

        Try
            Me.m_iEditMode = mpModeSchemeDone

            'prompt to save document if necessary
            'UPGRADE_WARNING: Couldn't resolve default property of object WordBasic.FileNameFromWindow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow() = "") Then
                MsgBox(mpOrganizerSavePrompt, MsgBoxStyle.Information, g_xAppName)
                Exit Sub
            End If

            xScheme = Me.Scheme
            iOrigin = iGetSchemeOrigin(xScheme)
            tmpSource = Source(iOrigin)

            '   check for scheme in origin tsgNumbers.sty;
            '   user may have deleted it or it may have
            '   come from another user's tsgNumbers.sty
            If (iOrigin = mpSchemeTypes.mpSchemeType_Private) And (g_xPSchemes(0, 0) <> "") Then
                Try
                    bOriginFound = bIsMPListTemplate(tmpSource.ListTemplates.Item(xScheme))
                Catch
                End Try
            End If

            '   if not found in personal or if firm is the origin,
            '   check for scheme in firm
            If Not bOriginFound Then
                iOrigin = mpSchemeTypes.mpSchemeType_Public
                If g_xFSchemes(0, 0) <> "" Then
                    Try
                        bOriginFound = bIsMPListTemplate(g_oFNumSty.ListTemplates.Item(xScheme))
                    Catch
                    End Try
                End If
            End If

            '   if still not found, alert user and exit
            If Not bOriginFound Then
                MsgBox("Could not reset selected scheme.  No " & "private or public scheme with this name exists.", MsgBoxStyle.Information, g_xAppName)
                Exit Sub
            Else
                '       give user a chance to bail out
                If iOrigin = mpSchemeTypes.mpSchemeType_Private Then
                    xMsg = "private"
                Else
                    xMsg = "public"
                End If
                xMsg = "Are you sure you want to reset the " & Me.SchemeDisplayName & " scheme to its " & xMsg & " defaults?"
                lRet = MsgBox(xMsg, MsgBoxStyle.YesNo + MsgBoxStyle.Question, g_xAppName)
                If lRet = MsgBoxResult.No Then Exit Sub
            End If

            Me.Close()

            With CurWordApp
                .ScreenRefresh()
                .ScreenUpdating = False
            End With

            '   check for scheme existence
            Dim bIsMPScheme As Boolean
            bIsMPScheme = cNumTOC.IsMacPacScheme(xScheme, , CurWordApp.ActiveDocument)

            '   reload if scheme exists, else warn user
            If bIsMPScheme Then
                bIsHeading = bIsHeadingScheme(xScheme)
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = iLoadScheme(CurWordApp.ActiveDocument, xScheme, iOrigin)

                '       relinking again is necessary, since copying styles
                '       occasionally breaks links - need to relink even if
                '       scheme appears to be linked; otherwise, schemes become
                '       unlinked when copied into a new document
                '        If bSchemeIsUnlinked(xScheme) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = bRelinkScheme(xScheme, mpSchemeTypes.mpSchemeType_Document, False, False, False)
                '        End If

                If bIsHeading Then
                    '           convert back to Word Heading Styles
                    'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bRet = bConvertToHeadingStyles(xScheme, False)
                End If

                '       ensure that left indent of numbered and para styles matches
                UpdateParaStyles(xScheme)

                '       redo trailing characters
                ApplyModifiedScheme(xScheme)

                '       backup scheme properties in case the user
                '       writes over them using the Word UI
                MacPacNumbering.LMP.Numbering.mdlConversions.BackupProps(xScheme)
            Else
                xMsg = "The " & xScheme & " scheme is not currently loaded. " & "Click the ""Use"" button " & "to insert numbers using this scheme."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()
        End Try
    End Sub

    Function UseScheme(ByVal bUseWordHeadings As Boolean, Optional ByVal xNewName As String = "", Optional ByVal xNewAlias As String = "") As Boolean
        'returns TRUE is scheme was loaded, FALSE if just activated
        Dim xScheme As String
        Dim xAlias As String
        Dim styScheme As Word.Style
        Dim oProps As MacPacNumbering.LMP.Numbering.cSchemeProps
        Dim xLT As String
        Dim bIsNew As Boolean

        CurWordApp.ScreenUpdating = False

        xScheme = Me.Scheme
        xAlias = Me.SchemeDisplayName

        '   reload styles if specified or necessary
        If xScheme <> "HeadingStyles" Then
            On Error Resume Next
            styScheme = CurWordApp.ActiveDocument.Styles(xGetStyleRoot(xScheme) & "_L" & 1)
            On Error GoTo 0

            If (styScheme Is Nothing) Or (Not bListTemplateExists(xScheme)) Then
                '           could not find styles or list template for
                '           scheme - load scheme
                'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bRet = iLoadScheme(CurWordApp.ActiveDocument, xScheme, Me.SchemeType, xNewAlias, xNewName)

                '           only convert to heading styles if
                '           this functionality is allowed when
                '           the user 'Uses' a scheme and if the
                '           menu item is checked
                If bUseWordHeadings Then
                    UseWordHeadingStyles(Scheme)
                End If

                bIsNew = True
            End If
        End If

        '    If g_bAllowFullHeadingStyleUse Then
        ''       ensure that scheme is correctly linked
        '        bRelinkScheme xScheme, _
        ''                      mpSchemeType_Document, _
        ''                      bUseWordHeadings
        '    Else
        '       ensure that scheme is correctly linked -
        '       use heading styles only if the scheme is
        '       already the heading scheme for the doc - need to relink even if
        '       scheme appears to be linked; otherwise, schemes become
        '       unlinked when copied into a new document
        '        If bSchemeIsUnlinked(xScheme) Then
        bRelinkScheme(xScheme, mpSchemeTypes.mpSchemeType_Document, bIsHeadingScheme(xScheme), False, Not bIsNew)
        '        End If
        '    End If

        '   backup scheme properties in case the user
        '   writes over them using the Word UI
        MacPacNumbering.LMP.Numbering.mdlConversions.BackupProps(xScheme)

        '   set scheme of active doc - use key so that
        '   type of scheme is carried in var
        bSetSelectedScheme(CurWordApp.ActiveDocument, Me.tvwSchemes.SelectedNode.Name)

        UseScheme = bIsNew
    End Function

    Private Sub frmSchemes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000
        Try
            If KeyCode = System.Windows.Forms.Keys.F6 Then
                Dim oForm As frmAboutNew
                oForm = New frmAboutNew()
                oForm.ShowDialog()
            ElseIf KeyCode = System.Windows.Forms.Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmSchemes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim xSelScheme As String
        Dim xSelSchemeType As String
        Dim xPath As String
        Dim bUseWordHeadings As Boolean
        '    Dim iDocSchemes As Integer
        Dim xInterval As String
        Dim i As Short
        Dim oNode As System.Windows.Forms.TreeNode

        Try
            'adjust for scaling
            '10.0.12 - don't rescale if form is simply being reshown because
            'baseline height/width will have already been changed
            If Not g_bNoSchemesDlgRescaling Then
                Dim sFactor As Single = CScaling.GetScalingFactor()
                If sFactor > 1 Then
                    Me.tvwSchemes.ItemHeight = 18 + (sFactor - 1) * 14
                    Me.btnUse.Width = Me.btnUse.Width * sFactor
                    Me.btnChangeScheme.Width = Me.btnChangeScheme.Width * sFactor
                    Me.btnReset.Width = Me.btnReset.Width * sFactor
                    Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                    Me.btnUse.Height = Me.btnUse.Height + (sFactor - 1) * 20
                    Me.btnChangeScheme.Height = Me.btnChangeScheme.Height + (sFactor - 1) * 40
                    Me.btnReset.Height = Me.btnReset.Height + (sFactor - 1) * 40
                    Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
                End If
            Else
                g_bNoSchemesDlgRescaling = False
            End If

            'get whether any scheme is being used with heading styles
            Try
                bUseWordHeadings = CBool(GetAppSetting("Numbering", "UseWordHeadingStyles"))
            Catch
            End Try

            '    ConvertWordHeadingStyles
            Me.mnuScheme_UseWordHeadings.Checked = bUseWordHeadings
            Me.mnuContext_UseWordHeadings.Checked = bUseWordHeadings

            '   hide menu items if specified
            If g_bSchemeSelectionOnly Then
                'dialog is being used to set active scheme only
                Me.mnuHelp.Visible = False
                Me.mnuScheme.Visible = False
                Me.ContextMenuStrip1.Visible = False
                Me.mnuSharing.Visible = False
                Me.btnChangeScheme.Visible = False
                Me.mnuScheme_ChangeTo.Visible = False
                Me.btnReset.Visible = False
                Me.mnuScheme_Reset.Visible = False
                Me.btnUse.Margin = Me.btnReset.Margin
                Me.Text = "Select a Numbering Scheme"
                '        Me.sbStatus.Top = Me.sbStatus.Top - 200
                Me.Height = Me.Height - 200
            Else
                Me.mnuScheme_Modify.Visible = g_bAllowSchemeEdit
                Me.mnuScheme_New.Visible = g_bAllowSchemeNew
                Me.mnuScheme_Sep2.Visible = (g_bAllowSchemeEdit Or g_bAllowSchemeNew Or g_bAllowSchemeSharing)
                Me.mnuScheme_UseWordHeadings.Visible = g_bAllowFullHeadingStyleUse
                Me.mnuContext_Modify.Visible = g_bAllowSchemeEdit
                Me.mnuContext_New.Visible = g_bAllowSchemeNew
                Me.mnuContext_Sep2.Visible = (g_bAllowSchemeEdit Or g_bAllowSchemeNew Or g_bAllowSchemeSharing)
                Me.mnuContext_UseWordHeadings.Visible = g_bAllowFullHeadingStyleUse
            End If

            '   check Insert Level 1 option if specified
            If GetUserSetting("Numbering", "InsertLevel1Automatically") = "1" Then Me.mnuScheme_InsertLevel1Automatically.Checked = True

            '   if no edits allowed, remove tooltip from preview
            If (Not g_bAllowSchemeEdit) Or g_bSchemeSelectionOnly Then ToolTip1.SetToolTip(imgPreview, "")

            '   change form for admin mode
            If g_bIsAdmin Then
                '       hide/move buttons
                Me.btnChangeScheme.Visible = False
                Me.btnReset.Visible = False
                Me.btnUse.Margin = Me.btnReset.Margin
                Me.btnReload.Visible = True
                Me.toolStripSeparator1.Visible = False
                Me.toolStripSeparator3.Visible = False
                Me.ToolStripSeparator5.Visible = True

                '       hide menu items
                Me.mnuScheme_ChangeTo.Visible = False
                Me.mnuScheme_Reset.Visible = False
                Me.mnuScheme_Relink.Visible = False
                Me.mnuScheme_SetAsDefault.Visible = False
                Me.mnuScheme_Sep1.Visible = False
                Me.mnuScheme_Sep2.Visible = False
                Me.mnuScheme_InsertLevel1Automatically.Visible = False
                Me.mnuScheme_Favorites.Visible = False
                Me.mnuContext_ChangeTo.Visible = False
                Me.mnuContext_Reset.Visible = False
                Me.mnuContext_Relink.Visible = False
                Me.mnuContext_SetAsDefault.Visible = False
                Me.mnuContext_Sep1.Visible = False
                Me.mnuContext_Sep2.Visible = False
                Me.mnuContext_InsertLevel1Automatically.Visible = False
                Me.mnuContext_Favorites.Visible = False

                '       change caption
                Me.Text = Me.Text & " - Administrative Utility"
            End If

            '   disable Sharing completely
            Me.mnuSharing.Visible = (g_bAllowSchemeSharing And (Not g_bSchemeSelectionOnly))

            'populate help menu
            'GLOG 15974 (dm) - moved up to ensure that the Help menu is repopulated
            'after reshowing the form
            Dim xTemp As String
            Dim xHelpFilesPath As String
            xTemp = CodeBasePath
            i = InStrRev(UCase(xTemp), "\") '* 9.5.0
            If i > 0 Then _
                xTemp = xTemp.Substring(0, i - 1)
            xHelpFilesPath = xTemp & "\Tools\User Documentation\pdf"
            m_oMenuArray = New HelpMenuArray(mnuHelp, xHelpFilesPath)

            'GLOG 8789 (dm) - ensure that help menu is enabled under all circumstances
            mnuHelp.Enabled = True

            'GLOG 15973 (dm) - moved up to ensure that this variable always initializes Done -
            'user was winding up in a loop when closing the Schemes dialog via the X after
            'cancelling the New Scheme dialog
            Me.m_iEditMode = mpModeSchemeDone

            'get document schemes
            'GLOG 15974 (dm) - moved up to ensure that Change To is reenabled when appropriate
            'after reshowing the form
            m_iDocSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)

            'don't reinitialize if form is being reshown
            If Me.Scheme <> "" Then Exit Sub

            EchoOff()
            CurWordApp.ScreenUpdating = False
            Me.m_bFinished = False
            LoadStyFiles()

            g_oCurScheme = New cNumScheme

            RefreshSchemesList((Me.tvwSchemes), , , g_bShowPersonalSchemes)

            '   initialize to current scheme
            With Me.tvwSchemes
                xSelScheme = xActiveScheme(CurWordApp.ActiveDocument)
                'UPGRADE_WARNING: IsEmpty was upgraded to IsNothing and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
                If IsNothing(xSelScheme) Then
                    If m_iDocSchemes Then
                        '               perhaps user copied numbered paras from another doc
                        xSelScheme = g_xDSchemes(0, 0)
                        xPath = CStr(mpSchemeTypes.mpSchemeType_Document)
                    Else
                        xSelScheme = GetUserSetting("Numbering", "DefaultScheme")
                        xPath = GetUserSetting("Numbering", "DefaultSchemeType")
                    End If
                Else
                    xPath = CStr(mpSchemeTypes.mpSchemeType_Document)
                End If

                If m_iDocSchemes Then
                    '           expand document schemes node
                    Me.tvwSchemes.Nodes.Item(mpSchemeTypes.mpSchemeType_Category & mpDocumentSchemes).Expand()
                Else
                    '           disable change button and relink menu item
                    '            Me.mnuScheme_Relink.Enabled = False
                    Me.btnChangeScheme.Enabled = False
                    Me.mnuScheme_ChangeTo.Enabled = False
                End If

                'context menu should match schemes menu
                SetContextMenuEnabled()

                '       if default is a favorite, select node in favorites folder
                If xPath <> CStr(mpSchemeTypes.mpSchemeType_Document) Then
                    If xPath = CStr(mpSchemeTypes.mpSchemeType_Private) Then
                        Try
                            oNode = .Nodes.Find(CStr(mpFavoritePrivate) & xSelScheme, True).Single
                        Catch
                        End Try
                        If Not oNode Is Nothing Then xPath = CStr(mpFavoritePrivate)
                    Else
                        Try
                            oNode = .Nodes.Find(CStr(mpFavoritePublic) & xSelScheme, True).Single
                        Catch
                        End Try
                        If Not oNode Is Nothing Then xPath = CStr(mpFavoritePublic)
                    End If
                End If

                '       select node
                Try
                    'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.Selected was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                    If Not g_bIsAdmin Then .SelectedNode = .Nodes.Find(xPath & xSelScheme, True).Single
                Catch
                End Try

                '       if no selection occurred, select first public scheme
                If (.SelectedNode Is Nothing) Then
                    Try
                        If g_bIsAdmin Then
                            'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.Selected was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            Dim oPublicNode As System.Windows.Forms.TreeNode
                            oNode = .Nodes(mpSchemeTypes.mpSchemeType_Category & mpPersonalSchemes)
                            If oNode.Nodes.Count > 0 Then
                                .SelectedNode = oNode.Nodes(0)
                            Else
                                .SelectedNode = oNode
                            End If
                        Else
                            'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.Selected was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                            .SelectedNode = .Nodes.Find(mpSchemeTypes.mpSchemeType_Public & g_xFSchemes(0, 0), True).Single
                        End If
                    Catch
                    End Try
                End If
            End With

            Me.txtDescription.Text = "Article scheme uses Article_L1-9 styles." & vbCrLf & "1Abcd " & LCase("EFGHI JKLMN OPQRS TUVWX ") & "2Abcd " & LCase("EFGHI JKLMN OPQRS TUVWX ") & "3Abcd " & LCase("EFGHI JKLMN OPQRS TUVWX ") & "4Abcd " & LCase("EFGHI JKLMN OPQRS TUVWX ") & "5Abcd " & LCase("EFGHI JKLMN OPQRS TUVWX ")

            With Me.tvwSchemes
                .Focus()
                tvwSchemes_AfterSelect(tvwSchemes, New System.Windows.Forms.TreeViewEventArgs(.SelectedNode))
            End With
        Catch oE As Exception
            Select Case Err.Number
                Case mpInvalidValue
                    'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.Nodes.Selected was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                    Me.tvwSchemes.SelectedNode = Me.tvwSchemes.Nodes.Item(1)
                Case Else
                    [Error].Show(oE)
            End Select
        Finally
            EchoOn()
            CurWordApp.ScreenUpdating = True
        End Try
    End Sub
    Private Sub scMain_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles scMain.DoubleClick
        Try
            If g_bAllowSchemeEdit And (Not g_bSchemeSelectionOnly) Then
                If Me.SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                    '           doc scheme - edit scheme available unless turned off in 97
                    '            If Not g_bNoSchemeIndents Then
                    mnuScheme_Modify_Click(mnuScheme_Modify, New System.EventArgs())
                ElseIf Me.SchemeType = mpSchemeTypes.mpSchemeType_Private Then
                    '           private scheme - edit scheme always available
                    mnuScheme_Modify_Click(mnuScheme_Modify, New System.EventArgs())
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuHelp_About_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelp_About.Click
        Dim oForm As frmAboutNew
        Try
            oForm = New frmAboutNew()
            oForm.ShowDialog()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_ChangeTo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_ChangeTo.Click, mnuContext_ChangeTo.Click
        Try
            btnChangeScheme_Click(btnChangeScheme, New System.EventArgs())
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_ConvertToHeadingStyles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_ConvertToHeadingStyles.Click, mnuContext_ConvertToHeadingStyles.Click
        'change selected document scheme to use Heading Styles-
        'change all paragraphs marked with MacPac styles of scheme
        'to Heading styles
        Try
            '   ensure styles exist
            If Not bEnsureSchemeStyles((Me.Scheme)) Then Exit Sub

            Me.Close()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            EchoOff()

            '   do conversion
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bConvertToHeadingStyles(Me.Scheme)

            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If bRet Then
                '       change menu items
                Me.mnuScheme_ConvertToHeadingStyles.Visible = False
                Me.mnuScheme_ConvertToMacPacStyles.Visible = True
                Me.mnuScheme_ConvertToMacPacStyles.Enabled = True
                Me.mnuContext_ConvertToHeadingStyles.Visible = False
                Me.mnuContext_ConvertToMacPacStyles.Visible = True
                Me.mnuContext_ConvertToMacPacStyles.Enabled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            EchoOn()
            With CurWordApp
                .ScreenUpdating = True
                .ScreenRefresh()
            End With
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Public Sub mnuScheme_ConvertToMacPacStyles_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_ConvertToMacPacStyles.Click, mnuContext_ConvertToMacPacStyles.Click
        'change selected document scheme to use MacPac Styles-
        'change all paragraphs marked with Heading styles of schem
        'to MacPac styles
        Try
            Me.Close()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            CurWordApp.ScreenUpdating = True

            '   do conversion
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bConvertToMacPacStyles(Me.Scheme)

            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If bRet Then
                '       change menu items
                Me.mnuScheme_ConvertToHeadingStyles.Visible = True
                Me.mnuScheme_ConvertToHeadingStyles.Enabled = True
                Me.mnuScheme_ConvertToMacPacStyles.Visible = False
                Me.mnuContext_ConvertToHeadingStyles.Visible = True
                Me.mnuContext_ConvertToHeadingStyles.Enabled = True
                Me.mnuContext_ConvertToMacPacStyles.Visible = False
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            EchoOn()
            With CurWordApp
                .ScreenUpdating = True
                .ScreenRefresh()
            End With
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Public Sub mnuScheme_Delete_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Delete.Click, mnuContext_Delete.Click
        Dim xDefaultScheme As String
        Dim xMsg As String
        Dim xScheme As String
        Dim xAlias As String
        Dim iSchemeType As mpSchemeTypes
        Dim xSelScheme As String
        Dim lNumSchemes As Integer
        Dim bIsActive As Boolean

        Try
            xScheme = Me.Scheme
            xAlias = Me.SchemeDisplayName
            iSchemeType = Me.SchemeType
            bIsActive = (xActiveScheme(CurWordApp.ActiveDocument) = xScheme)
            xDefaultScheme = GetUserSetting("Numbering", "DefaultScheme")

            '   make sure user really wants to delete scheme
            xMsg = "Delete " & xAlias & " scheme?"
            If (iSchemeType = mpSchemeTypes.mpSchemeType_Private) And (xScheme = xDefaultScheme) Then
                xMsg = xMsg & "  This is currently your default scheme."
            ElseIf iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                If bSchemeIsApplied(xScheme) Then
                    xMsg = xAlias & " scheme is currently applied to text in " & "this document.  If you delete the scheme, numbering " & "will be removed from any paragraphs to which this " & "scheme was previously applied"
                    If Not bIsHeadingScheme(xScheme) Then
                        xMsg = xMsg & ", and the paragraphs will be reformatted " & "to Normal style"
                    End If
                    xMsg = xMsg & ".  Delete " & xAlias & " scheme?"
                End If
            End If

            lRet = MsgBox(xMsg, MsgBoxStyle.YesNo + MsgBoxStyle.Question, g_xAppName)
            If lRet = MsgBoxResult.No Then
                Exit Sub
            End If

            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            '    Me.sbStatus.SimpleText = _
            ''        "Deleting " & xAlias & " scheme.  Please wait ..."

            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            bRet = bDeleteScheme(xScheme, xAlias, iSchemeType)
            lNumSchemes = UpdateSchemeArray(iSchemeType)
            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then m_iDocSchemes = lNumSchemes
            'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Dim oNode As System.Windows.Forms.TreeNode
            If bRet Then
                With Me.tvwSchemes
                    'UPGRADE_WARNING: MSComctlLib.Nodes method tvwSchemes.Nodes.Remove has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                    '.Nodes.RemoveAt(.SelectedNode.Index)
                    .SelectedNode.Remove()

                    'delete favorite
                    Try
                        oNode = .Nodes.Find(CStr(mpFavoritePrivate) & xScheme, True).Single
                    Catch
                    End Try
                    If Not oNode Is Nothing Then
                        'UPGRADE_WARNING: MSComctlLib.Nodes method tvwSchemes.Nodes.Remove has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                        .Nodes.RemoveAt(CStr(mpFavoritePrivate) & xScheme)
                        DeleteFavoriteScheme(xScheme)
                    End If

                    '           refresh preview bitmap
                    tvwSchemes_AfterSelect(tvwSchemes, New System.Windows.Forms.TreeViewEventArgs(.SelectedNode))
                End With
            End If

            '    sbStatus.SimpleText = "Ready"
            'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            'GLOG 5539 (9.9.6006) - sty file needs to be saved with form hidden - it
            'needs to be done outside of the form to avoid leaving the active control,
            'in this case the right-click menu, in suspended animation
            If iSchemeType = mpSchemeTypes.mpSchemeType_Private Then
                m_bSavePrivateStyFile = True
                Me.Hide()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Public Sub mnuScheme_Favorites_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Favorites.Click, mnuContext_Favorites.Click
        Dim xFavorite As String
        Dim vProps As Object
        Dim i As Short
        Dim xType As String
        Dim bFound As Boolean

        Try
            If Me.tvwSchemes.SelectedNode.Parent.Text <> mpFavoriteSchemes Then
                'add if not already a favorite
                i = 1
                xFavorite = GetUserSetting("Numbering", "FavoriteScheme" & CStr(i))
                While (xFavorite <> "") And Not bFound
                    'UPGRADE_WARNING: Couldn't resolve default property of object vProps. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    vProps = Split(xFavorite, "|")
                    'UPGRADE_WARNING: Couldn't resolve default property of object vProps(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bFound = (vProps(0) = Me.Scheme)
                    If Not bFound Then
                        i = i + 1
                        xFavorite = GetUserSetting("Numbering", "FavoriteScheme" & CStr(i))
                    End If
                End While

                If Not bFound Then
                    'get type
                    If Me.SchemeType = mpSchemeTypes.mpSchemeType_Public Then
                        xType = CStr(mpFavoritePublic)
                    Else
                        xType = CStr(mpFavoritePrivate)
                    End If

                    'add to user ini
                    SetUserSetting("Numbering", "FavoriteScheme" & CStr(i), Me.Scheme & "|" & Me.SchemeDisplayName & "|" & xType)

                    'add to tree
                    'UPGRADE_WARNING: Add method behavior has changed Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DBD08912-7C17-401D-9BE9-BA85E7772B99"'
                    Me.tvwSchemes.Nodes(1).Nodes.Add(xType & Me.Scheme, Me.SchemeDisplayName)
                End If
            Else
                'delete key from ini
                DeleteFavoriteScheme(Me.Scheme)

                'remove node from tree
                'UPGRADE_WARNING: MSComctlLib.Nodes method tvwSchemes.Nodes.Remove has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
                'GLOG 15989 (dm) - avoid type conversion error caused by change of behavior
                Me.tvwSchemes.SelectedNode.Remove()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_InsertLevel1Automatically_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_InsertLevel1Automatically.Click, mnuContext_InsertLevel1Automatically.Click
        Try
            With Me.mnuScheme_InsertLevel1Automatically
                .Checked = Not .Checked
                Me.mnuContext_InsertLevel1Automatically.Checked = .Checked
                SetUserSetting("Numbering", "InsertLevel1Automatically", CStr(System.Math.Abs(CInt(.Checked))))
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_Modify_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Modify.Click, mnuContext_Modify.Click
        Try
            '   ensure styles exist
            If Me.SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                If Not bEnsureSchemeStyles((Me.Scheme)) Then
                    Exit Sub
                End If
            End If

            m_iEditMode = mpModeSchemeEdit
            Me.Hide()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_New_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_New.Click, mnuContext_New.Click
        Dim xMsg As String

        Try
            '   validate
            If g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Category Then
                If g_bIsAdmin Then
                    If Me.tvwSchemes.Nodes.Item(mpSchemeTypes.mpSchemeType_Category & mpPersonalSchemes).GetNodeCount(False) = 0 Then
                        xMsg = "There are no available public schemes " & "on which to base a new scheme.  Start with " & "a document scheme or click the Load " & "Public Schemes button."
                    End If
                ElseIf Me.tvwSchemes.Nodes.Item(mpSchemeTypes.mpSchemeType_Category & mpPublicSchemes).GetNodeCount(False) = 0 Then
                    xMsg = "There are no available public schemes " & "on which to base a new scheme.  Start with " & "a document or private scheme selected.  If " & "no schemes of any type are available, please " & "contact your system administrator."
                End If
                If xMsg <> "" Then
                    MsgBox(xMsg, MsgBoxStyle.Information, g_xAppName)
                    Exit Sub
                End If
            ElseIf Me.SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                '       ensure styles exist
                If Not bEnsureSchemeStyles((Me.Scheme)) Then
                    Exit Sub
                End If
            End If

            m_iEditMode = mpModeSchemeNew
            Me.Hide()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Public Sub mnuScheme_Properties_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Properties.Click, mnuContext_Properties.Click
        Dim xFolder As String
        Dim oNode As System.Windows.Forms.TreeNode
        Dim i As Short
        Dim xFavorite As String
        Dim vProps As Object
        Dim bFound As Boolean
        Dim xDescription As String '9.9.5001

        Try
            'store existing description
            xDescription = g_oCurScheme.Description

            xFolder = Me.tvwSchemes.SelectedNode.Parent.Text
            EditSchemeProperties(g_oCurScheme.Name, g_oCurScheme.SchemeType, False)
            Me.tvwSchemes.SelectedNode.Text = g_oCurScheme.DisplayName

            'update favorite
            If xFolder = mpPersonalSchemes Then
                Try
                    oNode = Me.tvwSchemes.Nodes.Find(CStr(mpFavoritePrivate) & g_oCurScheme.Name, True).Single
                Catch
                End Try
            ElseIf (xFolder = mpFavoriteSchemes) And (g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Private) Then
                Try
                    oNode = Me.tvwSchemes.Nodes.Find("3" & g_oCurScheme.Name, True).Single
                Catch
                End Try
            End If

            If Not oNode Is Nothing Then
                oNode.Text = g_oCurScheme.DisplayName

                'update ini key
                i = 1
                xFavorite = GetUserSetting("Numbering", "FavoriteScheme" & CStr(i))
                While (xFavorite <> "") And Not bFound
                    'UPGRADE_WARNING: Couldn't resolve default property of object vProps. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    vProps = Split(xFavorite, "|")
                    'UPGRADE_WARNING: Couldn't resolve default property of object vProps(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bFound = (vProps(0) = g_oCurScheme.Name)
                    If Not bFound Then
                        i = i + 1
                        xFavorite = GetUserSetting("Numbering", "FavoriteScheme" & CStr(i))
                    End If
                End While
                If bFound Then
                    SetUserSetting("Numbering", "FavoriteScheme" & CStr(i), g_oCurScheme.Name & "|" & g_oCurScheme.DisplayName & "|" & CStr(mpFavoritePrivate))
                End If
            End If

            'update description in dialog if necessary
            If g_oCurScheme.Description <> xDescription Then DisplayAssociatedStyles()

            'GLOG 5539 (9.9.6006) - sty file needs to be saved with form hidden - it
            'needs to be done outside of the form to avoid leaving the active control,
            'in this case the right-click menu, in suspended animation
            If g_oCurScheme.SchemeType = mpSchemeTypes.mpSchemeType_Private Then
                m_bSavePrivateStyFile = True
                Me.Hide()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_Reset_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Reset.Click, mnuContext_Reset.Click
        Try
            btnReset_Click(btnReset, New System.EventArgs())
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_SetAsDefault_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_SetAsDefault.Click, mnuContext_SetAsDefault.Click
        Try
            SetUserSetting("Numbering", "DefaultScheme", (Me.Scheme))
            SetUserSetting("Numbering", "DefaultSchemeType", CStr(Me.SchemeType))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_Relink_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Relink.Click, mnuContext_Relink.Click
        'relink schemes in specified source file
        Dim iFound As Short

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            CurWordApp.ScreenUpdating = False
            Select Case Me.mnuScheme_Relink.Text
                Case "Re&link Document Schemes"
                    '            sbStatus.SimpleText = _
                    ''                "Relinking Document Schemes.  Please wait..."
                    'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    bRet = bRelinkSchemes(mpSchemeTypes.mpSchemeType_Document, True)
                    iFound = iRenameLTsFromVars()
                    If iFound Then
                        m_iDocSchemes = UpdateSchemeArray(mpSchemeTypes.mpSchemeType_Document)
                        RefreshSchemesList((Me.tvwSchemes), Me.Scheme, (Me.SchemeType), g_bShowPersonalSchemes)
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object bRet. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If bRet Then
                        If m_iDocSchemes Then
                            xMsg = "The schemes in the current " & "document have been relinked."
                        Else
                            xMsg = "There are no schemes in the current document."
                        End If
                    Else
                        '11/2/12
                        xMsg = "The schemes in the current document could not be relinked."
                    End If
                Case "Re&link Private Schemes"
                    '            sbStatus.SimpleText = _
                    ''                "Relinking Private Schemes.  Please wait..."
                    bRelinkSchemes(mpSchemeTypes.mpSchemeType_Private)
                    xMsg = "Your Private Schemes have been relinked."
                    '           save personal numbers sty
                    With CurWordApp.Templates.Item(g_xPNumSty)
                        .Saved = False
                        'workaround for Word 2010 to avoid error 5986 -
                        '"this command is not available in an unsaved document"
                        .OpenAsDocument.Save()
                        CurWordApp.ActiveDocument.Close(False)
                    End With
                Case "Re&link Public Schemes"
                    '            sbStatus.SimpleText = _
                    ''                "Relinking Public Schemes.  Please wait..."
                    bRelinkSchemes(mpSchemeTypes.mpSchemeType_Public)
                    xMsg = "The schemes in the current " & "document have been relinked."
            End Select
            MsgBox(xMsg, MsgBoxStyle.Information, g_xAppName)
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            CurWordApp.ScreenUpdating = True
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    Public Sub mnuScheme_Use_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_Use.Click, mnuContext_Use.Click
        Try
            btnUse_Click(btnUse, New System.EventArgs())
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuScheme_UseWordHeadings_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuScheme_UseWordHeadings.Click, mnuContext_UseWordHeadings.Click
        Dim xHScheme As String
        Try
            Me.mnuScheme_UseWordHeadings.Checked = Not Me.mnuScheme_UseWordHeadings.Checked
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub ShowPublicSchemes(ByVal bImport As Boolean)
        Dim oShare As MacPacNumbering.LMP.Numbering.cShare
        Dim xFile As String
        Dim xDisplayName As String
        Dim udtDocEnv As mpDocEnvironment
        Dim udtAppEnv As mpAppEnvironment
        Dim bCancel As Boolean

        Try
            'GLOG 5350 - hide dialog during import to prevent
            'error #4198 (command failed) when attempting to
            'close the mpn file - this became
            'an issue with FileSite/Desksite 8.5 SP3
            '9/28/16 - hopefully, won't be an issue with .NET,
            'because Hide() switches focus from Word and selection
            'in Schemes dialog wasn't getting preserved on Import|Close
            'If bImport Then Me.Hide()

            '   get/set environment
            'UPGRADE_WARNING: Couldn't resolve default property of object udtDocEnv. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            udtDocEnv = envGetDocEnvironment()
            'UPGRADE_WARNING: Couldn't resolve default property of object udtAppEnv. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            udtAppEnv = envGetAppEnvironment()

            Err.Clear()

            'UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
            Dim oForm As frmPublicSchemes
            oForm = New frmPublicSchemes()
            With oForm
                '       set up caption
                If bImport Then
                    .Text = " Import Shared Schemes"
                    '.AcceptButton = .btnImport
                Else
                    .Text = " Delete Shared Schemes"
                    '.AcceptButton = .btnDelete
                End If

                '       set up btns
                .btnDelete.Visible = Not bImport
                .btnImport.Visible = bImport
                .ShowDialog()
                bCancel = .Cancelled
                .Close()
            End With

            If bImport And Not bCancel Then
                '        sbStatus.SimpleText = "Imported " & g_oCurScheme.Alias & " scheme"
                'UPGRADE_ISSUE: Control tvwSchemes could not be resolved because it was within the generic namespace Form. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
                RefreshSchemesList(Me.tvwSchemes, g_oCurScheme.Name, mpSchemeTypes.mpSchemeType_Private)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            'reset environment
            bSetAppEnvironment(udtAppEnv)
            bSetDocEnvironment(udtDocEnv)

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            EchoOn()
            CurWordApp.ScreenUpdating = True
            CurWordApp.ScreenRefresh()
            'UPGRADE_NOTE: Object oShare may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            oShare = Nothing
        End Try
    End Sub

    Public Sub mnuSharing_DeletePublicSchemes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSharing_DeletePublicSchemes.Click
        Try
            ShowPublicSchemes(False)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtDescription_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)

        Try
            KeyAscii = 0
            eventArgs.KeyChar = Chr(KeyAscii)
            If KeyAscii = 0 Then
                eventArgs.Handled = True
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_AfterLabelEdit(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.NodeLabelEditEventArgs)
        Dim Cancel As Boolean = eventArgs.CancelEdit
        Dim NewString As String = eventArgs.Label
        Dim xScheme As String
        Dim lRet As Integer

        Try
            If bSchemeNameIsValid(NewString, True, True) Then
                xScheme = Mid(Me.tvwSchemes.SelectedNode.Name, 2)
                lRet = lRenameScheme(xScheme, NewString, Me.tvwSchemes.SelectedNode.Text)
                Me.AcceptButton = Me.btnCancel
                If lRet Then
                    Throw New System.Exception("Unable to rename scheme.")
                End If
            Else
                Cancel = True
            End If
        Catch oE As Exception
            Cancel = True
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_BeforeLabelEdit(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.NodeLabelEditEventArgs)
        Try
            Dim Cancel As Boolean = eventArgs.CancelEdit
            'allow only private schemes to be renamed
            Cancel = Me.SchemeType <> mpSchemeTypes.mpSchemeType_Private
            Me.CancelButton = Me.btnCancel
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_AfterCollapse(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeViewEventArgs) Handles tvwSchemes.AfterCollapse
        Try
            Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
            '   enable/disable form controls/menus
            '   based on selected item in tree
            SetControlsEnable()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tvwSchemes.DoubleClick
        Try
            '   double-click is equivalent to
            '   selecting scheme and clicking 'Use'
            If Me.SchemeType > mpSchemeTypes.mpSchemeType_Category Then
                btnUse_Click(btnUse, New System.EventArgs())
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Sub LoadBitmap(ByVal xFile As String, ByVal iSchemeType As mpSchemeTypes)
        Dim fs As System.IO.FileStream

        '9/6/16 - load image from a file stream to avoid locking the bitmap itself,
        'in case it needs to be deleted
        fs = New System.IO.FileStream(xFile, IO.FileMode.Open, IO.FileAccess.Read)

        With Me.imgPreview
            'UPGRADE_NOTE: Object Me.imgPreview.Picture may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            .BackgroundImage = Nothing
            If iSchemeType = mpSchemeTypes.mpSchemeType_Category Then
                .Top = Me.tvwSchemes.Top
                .Height = Me.tvwSchemes.Height
            Else
                .Top = Me.Image1.Top + Me.Image1.Height
                .Height = Me.tvwSchemes.Height - Me.Image1.Height
            End If
            On Error GoTo ProcError
            '.Image = System.Drawing.Image.FromFile(xFile)

            .BackgroundImage = CScaling.ScaleImageForScreen(System.Drawing.Image.FromStream(fs)) 'GLOG 8611
            .Refresh()
        End With

        fs.Close()

        EchoOn()

        Exit Sub
ProcError:
        xFile = GetAppPath() & "\NoPreview.mpb"
        Resume
    End Sub
    Private Sub SetControlsEnable()
        'enable/disable buttons/menus
        Dim bIsDScheme As Boolean
        Dim bIsPScheme As Boolean
        Dim bIsFScheme As Boolean
        Dim bIsCategory As Boolean
        Dim bIsHScheme As Boolean
        Dim styHeading As Word.Style
        Dim bNoEnglishHeadings As Boolean
        Dim bIsFavorite As Boolean

        '   Heading 1-9 may be missing on foreign system
        On Error Resume Next
        styHeading = CurWordApp.ActiveDocument.Styles(Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleHeading1)
        If styHeading Is Nothing Then bNoEnglishHeadings = True
        On Error GoTo 0

        '   set controls
        With Me
            bIsCategory = (.SchemeType = mpSchemeTypes.mpSchemeType_Category)
            bIsPScheme = (.SchemeType = mpSchemeTypes.mpSchemeType_Private)
            bIsFScheme = (.SchemeType = mpSchemeTypes.mpSchemeType_Public)
            bIsDScheme = (.SchemeType = mpSchemeTypes.mpSchemeType_Document)
            bIsHScheme = bIsHeadingScheme(.Scheme)
            If Not bIsCategory Then bIsFavorite = (.tvwSchemes.SelectedNode.Parent.Text = mpFavoriteSchemes)
            If bIsFavorite Then
                .mnuScheme_Favorites.Text = "Remove from &Favorites"
            Else
                .mnuScheme_Favorites.Text = "Add to &Favorites"
            End If
            .mnuContext_Favorites.Text = mnuScheme_Favorites.Text

            '       if no admin schemes loaded, disable all menu items
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            If Dir(g_xPNumSty) = "" Then
                .mnuScheme_ConvertToHeadingStyles.Enabled = False
                .mnuScheme_ConvertToMacPacStyles.Enabled = False
                .mnuScheme_Delete.Enabled = False
                .mnuScheme_Modify.Enabled = False
                .mnuScheme_New.Enabled = False
                .mnuScheme_Properties.Enabled = False
                .mnuScheme_Relink.Enabled = False
                .mnuScheme_SetAsDefault.Enabled = False
                .mnuScheme_Favorites.Enabled = False
                .mnuSharing_DeletePublicSchemes.Enabled = False
                .mnuSharing_ExportToPublic.Enabled = False
                .mnuSharing_ImportPublicScheme.Enabled = False
            Else
                .mnuScheme_New.Enabled = True
                .mnuScheme_Modify.Enabled = ((bIsPScheme Or bIsDScheme) And Not bIsFavorite)
                .mnuScheme_Delete.Enabled = ((bIsPScheme Or bIsDScheme) And Not bIsFavorite)
                .mnuScheme_Properties.Enabled = Not bIsCategory
                .mnuScheme_SetAsDefault.Enabled = (bIsPScheme Or bIsFScheme)
                .mnuScheme_Favorites.Enabled = (bIsPScheme Or bIsFScheme)
                .mnuSharing_DeletePublicSchemes.Enabled = True
                .mnuSharing_ImportPublicScheme.Enabled = True
                .mnuSharing_ExportToPublic.Enabled = bIsPScheme

                If bNoEnglishHeadings Then
                    .mnuScheme_ConvertToHeadingStyles.Visible = False
                    .mnuScheme_ConvertToMacPacStyles.Visible = False
                    .mnuContext_ConvertToHeadingStyles.Visible = False
                    .mnuContext_ConvertToMacPacStyles.Visible = False
                Else
                    If .mnuScheme_UseWordHeadings.Checked Then
                        .mnuScheme_ConvertToHeadingStyles.Visible = (bIsDScheme And Not bIsHScheme)
                        With .mnuScheme_ConvertToMacPacStyles
                            .Visible = Not (bIsDScheme And Not bIsHScheme)
                            .Enabled = (bIsDScheme And bIsHScheme)
                        End With
                        .mnuContext_ConvertToHeadingStyles.Visible = (bIsDScheme And Not bIsHScheme)
                        .mnuContext_ConvertToMacPacStyles.Visible = Not (bIsDScheme And Not bIsHScheme)
                    Else
                        With .mnuScheme_ConvertToHeadingStyles
                            .Visible = Not (bIsDScheme And bIsHScheme)
                            .Enabled = (bIsDScheme And Not bIsHScheme)
                        End With
                        .mnuScheme_ConvertToMacPacStyles.Visible = (bIsDScheme And bIsHScheme)
                        .mnuContext_ConvertToHeadingStyles.Visible = Not (bIsDScheme And bIsHScheme)
                        .mnuContext_ConvertToMacPacStyles.Visible = (bIsDScheme And bIsHScheme)
                    End If
                End If
            End If

            .btnUse.Enabled = Not bIsCategory
            .btnChangeScheme.Enabled = ((bIsCategory = False) And (m_iDocSchemes > 0))
            .btnReset.Enabled = bIsDScheme

            '9.9.5001 - added button functions to Schemes menu
            .mnuScheme_Use.Enabled = Not bIsCategory
            If bIsDScheme Then
                .mnuScheme_Use.Text = "&Make Active"
            Else
                .mnuScheme_Use.Text = "&Use"
            End If
            mnuContext_Use.Text = mnuScheme_Use.Text

            .mnuScheme_ChangeTo.Enabled = ((bIsCategory = False) And (m_iDocSchemes > 0))
            .mnuScheme_Reset.Enabled = bIsDScheme

            'context menu should match schemes menu
            SetContextMenuEnabled()
        End With
    End Sub

    Private Sub SetContextMenuEnabled()
        mnuContext_ChangeTo.Enabled = mnuScheme_ChangeTo.Enabled
        mnuContext_ConvertToHeadingStyles.Enabled = mnuScheme_ConvertToHeadingStyles.Enabled
        mnuContext_ConvertToMacPacStyles.Enabled = mnuScheme_ConvertToMacPacStyles.Enabled
        mnuContext_Delete.Enabled = mnuScheme_Delete.Enabled
        mnuContext_Favorites.Enabled = mnuScheme_Favorites.Enabled
        mnuContext_InsertLevel1Automatically.Enabled = mnuScheme_InsertLevel1Automatically.Enabled
        mnuContext_Modify.Enabled = mnuScheme_Modify.Enabled
        mnuContext_New.Enabled = mnuScheme_New.Enabled
        mnuContext_Properties.Enabled = mnuScheme_Properties.Enabled
        mnuContext_Relink.Enabled = mnuScheme_Relink.Enabled
        mnuContext_Reset.Enabled = mnuScheme_Reset.Enabled
        mnuContext_SetAsDefault.Enabled = mnuScheme_SetAsDefault.Enabled
        mnuContext_Use.Enabled = mnuScheme_Use.Enabled
        mnuContext_UseWordHeadings.Enabled = mnuScheme_UseWordHeadings.Enabled
    End Sub

    Public Sub mnuSharing_ExportToPublic_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSharing_ExportToPublic.Click
        Dim oShare As MacPacNumbering.LMP.Numbering.cShare

        Try
            'GLOG 5350 - hide dialog during export to prevent
            'error #4198 (command failed) when attempting to
            'close the mpn file after saving it - this became
            'an issue with FileSite/Desksite 8.5 SP3
            '9/28/16 - hopefully, won't be an issue with .NET,
            'because Hide() switches focus from Word and selection
            'in Schemes dialog wasn't getting preserved on Import|Close
            'Me.Hide()

            oShare = New MacPacNumbering.LMP.Numbering.cShare
            oShare.ExportPublicScheme((Me.Scheme), (Me.SchemeDisplayName))
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Sub mnuSharing_ImportPublicScheme_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuSharing_ImportPublicScheme.Click
        Try
            ShowPublicSchemes(True)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub ScrollPreview()
        '    Dim iTopMin As Short

        '    EchoOff()
        '    If Me.SchemeType = mpSchemeTypes.mpSchemeType_Category Then
        '        iTopMin = 0
        '    Else
        '        iTopMin = 375
        '    End If

        '    'UPGRADE_ISSUE: Panel property Me.imgContainer.BackgroundImage will be tiled. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED2DEFAA-C59C-4EDB-AF23-73A16C92C3AC"'
        '    Me.imgPreview.Top = CDbl(CObj(Me.imgContainer.BackgroundImage) - Me.vsbPreview.Value + iTopMin)
        '    '    DoEvents
        '    EchoOn()
    End Sub

    Public Sub ForcePreviewRefresh()
        tvwSchemes_AfterSelect(tvwSchemes, New System.Windows.Forms.TreeViewEventArgs(Me.tvwSchemes.SelectedNode))
    End Sub
    Private Function RestorePos(ByRef sTopPos As Single, ByRef sLeftPos As Single, ByRef sWidth As Single, ByRef sHeight As Single) As Object
        On Error Resume Next
        With Me
            .Top = sTopPos
            .Left = sLeftPos
            .Width = sWidth
            .Height = sHeight
        End With
    End Function

    Private Sub DisplayAssociatedStyles()
        Dim iLevels As Short
        Dim xStatus As String
        Dim xActive As String
        Dim bShowDescription As Boolean

        bShowDescription = (Me.SchemeType <> mpSchemeTypes.mpSchemeType_Category)

        If bShowDescription Then
            'selected scheme style info
            xStatus = Me.SchemeDisplayName & " "
            xActive = xActiveScheme(CurWordApp.ActiveDocument)
            If xActive = "" Then xStatus = xStatus & "scheme "
            If Me.SchemeType = mpSchemeTypes.mpSchemeType_Document Then
                xStatus = xStatus & "is using "
            ElseIf Me.mnuScheme_UseWordHeadings.Checked = True Then
                xStatus = xStatus & "will use "
            Else
                xStatus = xStatus & "uses "
            End If
            If ((Me.SchemeType = mpSchemeTypes.mpSchemeType_Document) And bIsHeadingScheme(Me.Scheme)) Or ((Me.mnuScheme_UseWordHeadings.Checked = True) And (xHeadingScheme() = "")) Then
                xStatus = xStatus & "Word Heading Styles"
            Else
                iLevels = iGetLevels(Me.Scheme, Me.SchemeType)
                xStatus = xStatus & xGetStyleRoot(Me.Scheme) & "_L1-" & iLevels & " styles"
            End If

            'append active scheme info
            If xActive <> "" Then
                If xStatus <> "" Then xStatus = xStatus & ".  "
                If xActive = Me.Scheme Then
                    xStatus = xStatus & "This is the active scheme."
                Else
                    xActive = GetField(xActive, mpRecordFields.mpRecField_Alias, mpSchemeTypes.mpSchemeType_Document)
                    xStatus = xStatus & xActive & " is the active scheme."
                End If
            End If

            'append scheme description to generic message
            If g_oCurScheme.Description <> "" Then
                If xStatus <> "" Then
                    If VB.Right(xStatus, 1) <> "." Then xStatus = xStatus & "."
                    xStatus = xStatus & vbCrLf
                End If
                xStatus = xStatus & g_oCurScheme.Description
            End If
        End If

        '9.9.5001 - status bar replaced with textbox
        Me.txtDescription.Text = xStatus

        '    'hide textbox and expand preview when nothing to show
        '    Me.txtDescription.Visible = bShowDescription
        '    Me.Frame2.Visible = bShowDescription
        '    If Not bShowDescription Then
        '        Me.vsbPreview.Height = 5010
        '    Else
        '        Me.vsbPreview.Height = 4020
        '    End If
    End Sub
    Private Sub vsbPreview_Scroll(sender As Object, e As System.Windows.Forms.ScrollEventArgs) Handles vsbPreview.Scroll
        Try
            ScrollPreview()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tvwSchemes_AfterSelect(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvwSchemes.AfterSelect
        Dim Node As System.Windows.Forms.TreeNode
        'change preview
        Dim xPreviewFilePath As String
        Dim iSchemeType As mpSchemeTypes
        Dim oPreview As MacPacNumbering.LMP.Numbering.cPreview
        Dim xFile As String

        Try
            iSchemeType = Me.SchemeType
        Catch
        End Try

        Try
            'GLOG 15888: No additional handling required for this here
            'If iSchemeType = mpSchemeTypes.mpSchemeType_Category Then
            '    With e.Node
            '        'UPGRADE_ISSUE: MSComctlLib.Node property tvwSchemes.SelectedItem.Expanded was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            '        If Not .IsExpanded Then
            '            .Expand()
            '        Else
            '            .Collapse()
            '        End If
            '    End With
            'End If

            If g_bAllowSchemeEdit And (Not g_bSchemeSelectionOnly) And (iSchemeType = mpSchemeTypes.mpSchemeType_Private Or iSchemeType = mpSchemeTypes.mpSchemeType_Document) Then
                Me.ToolTip1.SetToolTip(Me.imgPreview, "Double-click this preview area to edit the selected scheme.")
            Else
                Me.ToolTip1.SetToolTip(Me.imgPreview, "")
            End If

            '   enable/disable form controls/menus
            '   based on selected item in tree
            SetControlsEnable()

            oPreview = New MacPacNumbering.LMP.Numbering.cPreview
            Me.Image1.Visible = (iSchemeType <> mpSchemeTypes.mpSchemeType_Category)
            If (Not g_bSchemeSelectionOnly) Or (iSchemeType > mpSchemeTypes.mpSchemeType_Document) Then
                xFile = oPreview.GetFile(Me.SchemeDisplayName, iSchemeType)
            Else
                xFile = GetAppPath() & "\Blank.mpb"
            End If
            LoadBitmap(xFile, iSchemeType)

            Me.vsbPreview.Value = 0

            If Me.SchemeType = mpSchemeTypes.mpSchemeType_Private Then
                '       if private scheme is unlinked...
                If Not bIsLinked(Me.Scheme) Then
                    '           relink scheme - we'll save on form unload
                    bRelinkScheme(Me.Scheme, mpSchemeTypes.mpSchemeType_Private, False)
                End If
            End If

            If Me.SchemeType <> mpSchemeTypes.mpSchemeType_Category Then
                g_oCurScheme = GetRecord(Me.Scheme, Me.SchemeType)
            End If

            '   update status bar
            DisplayAssociatedStyles()
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            oPreview = Nothing
        End Try
    End Sub

    Private Sub btnUse_Click(sender As Object, e As EventArgs) Handles btnUse.Click
        Dim xHScheme As String
        Dim bUseWordHeadings As Boolean
        Dim ltP As Word.ListTemplate
        Dim xNewName As String
        Dim xAlias As String
        Dim xNewAlias As String
        Dim iLevels As Short
        Dim i As Short
        Dim xStyleRoot As String
        Dim bIsNew As Boolean

        Try
            Me.m_iEditMode = mpModeSchemeDone
            xNewAlias = Me.SchemeDisplayName
            xStyleRoot = xGetStyleRoot(Me.Scheme)

            If Me.SchemeType = mpSchemeTypes.mpSchemeType_Private Or Me.SchemeType = mpSchemeTypes.mpSchemeType_Public Then
                'UPGRADE_WARNING: Couldn't resolve default property of object WordBasic.FileNameFromWindow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow() = "") Then
                    'prompt to save document
                    MsgBox(mpOrganizerSavePrompt, MsgBoxStyle.Information, g_xAppName)
                    Exit Sub
                ElseIf bListTemplateIsMissing(CurWordApp.ActiveDocument, (Me.Scheme)) And (Not bListTemplateExists(xStyleRoot)) Then
                    If bLTExistsIgnoreCasing((Me.Scheme)) Then
                        '               scheme with same name, different casing
                        '               exists in document - alert and exit
                        xMsg = "There is already a scheme in this document that uses the " & xStyleRoot & " style root.  Please select this " & "scheme from the list of Document Schemes in this dialog."
                        MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                        Exit Sub
                    Else
                        '               styles exist without corresponding list template;
                        '               give user choice of exiting
                        xMsg = "TSG styles " & xStyleRoot & "_L1-x already exist in this document, but the " & xStyleRoot & " document scheme can no longer be found.  If numbered " & "paragraphs have been converted to text (this may have " & "occurred when saving changes in an earlier version of Word), " & "use of this scheme could result in the same numbers being applied " & "twice per paragraph.  Would you like to reload this scheme?"
                        lRet = MsgBox(xMsg, MsgBoxStyle.Information + MsgBoxStyle.YesNo, g_xAppName)
                        If lRet = MsgBoxResult.No Then Exit Sub

                        '               add notification flag
                        Try
                            CurWordApp.ActiveDocument.Variables.Add("CorruptionNotice", Today)
                        Catch
                        End Try
                    End If

                ElseIf CurWordApp.ActiveDocument.ListTemplates.Count Then
                    '           test for scheme in doc with same name
                    Try
                        ltP = CurWordApp.ActiveDocument.ListTemplates(xGetFullLTName((Me.Scheme)))
                    Catch
                    End Try

                    If Not (ltP Is Nothing) Then
                        '               scheme with same name exists in
                        '               document alert and exit
                        xAlias = GetField(Me.Scheme, mpRecordFields.mpRecField_Alias, mpSchemeTypes.mpSchemeType_Document)
                        iLevels = iGetLevels(Me.Scheme, mpSchemeTypes.mpSchemeType_Document)
                        xMsg = "There is already a scheme in this document that uses the " & xStyleRoot & "_L1-" & iLevels & " styles.  Please select the " & xAlias & " scheme from the list of Document Schemes in this dialog."
                        MsgBox(xMsg, MsgBoxStyle.Exclamation, g_xAppName)
                        Exit Sub
                    End If
                End If

                'message user if doc isn't in Word 2010 compatibility mode and scheme requires it
                If CurWordApp.ActiveDocument.CompatibilityMode < 14 Then
                    If ContainsWord2010NumberStyle(Me.Scheme, Me.SchemeType) Then
                        MsgBox("The specified scheme cannot be loaded because it contains a " & "numbering style that is only available for documents that are " & "in Word 2010 compatibility mode.", MsgBoxStyle.Information, g_xAppName)
                        Exit Sub
                    End If
                End If

                '       test for existing alias
                While Not bSchemeNameIsValid(xNewAlias, True, False, mpSchemeTypes.mpSchemeType_Document)
                    i = i + 1
                    xNewAlias = Me.SchemeDisplayName & i
                End While
            End If

            '   test if there is already a MacPac
            '   scheme that uses Word Heading styles
            xHScheme = xHeadingScheme()

            If Me.SchemeType <> mpSchemeTypes.mpSchemeType_Document And (Len(xHScheme) = 0) Then
                bUseWordHeadings = Me.mnuScheme_UseWordHeadings.Checked
            End If

            '9/26/16 - Hide() was switching focus from Word, so  now calling Close() immediately
            Dim xScheme As String
            Dim bInsert As Boolean
            xScheme = Me.Scheme
            bInsert = Me.mnuScheme_InsertLevel1Automatically.Checked

            Me.Close()

            bIsNew = UseScheme(bUseWordHeadings, xNewName, xNewAlias)

            If bUseWordHeadings Then
                '           document may start with non-numbered Heading 1-9 paras
                ApplyModifiedScheme(xScheme)
            End If

            If (Not g_bIsAdmin) And bIsNew And bInsert And (CurWordApp.Selection.Range.ListParagraphs.Count = 0) Then
                'insert level one if specified,
                'but only if selection is not already numbered
                bInsertNumberFromToolbar(1)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            CurWordApp.ScreenUpdating = True
            EchoOn()
        End Try
    End Sub

    Private Sub m_oMenuArray_Click(sender As Object, e As EventArgs) Handles m_oMenuArray.Click
        Dim xTag As String
        Try
            xTag = CStr(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
            LaunchDocumentByExtension(xTag)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class