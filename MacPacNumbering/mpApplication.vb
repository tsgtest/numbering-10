Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cWP
Imports LMP.Numbering.Base.cSchemeRecords
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpTypes
Imports MacPacNumbering.LMP.Numbering.mpnN80
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports LMP.INI2XML
Imports System.Math
Imports System.IO
Imports System.IO.Path
Imports System.Reflection.Assembly
Imports System.Runtime.InteropServices

Namespace LMP.Numbering
    Friend Class mpApplication
        Public Const KEY_SHIFT = &H10
        Public Const KEY_CTL = &H11
        Public Const KEY_MENU = &H12
        Public Const KEY_F1 = &H70
        Public Const KEY_F5 = &H74
        Public Const KEY_a = 65
        Public Const KEY_DEL = 46
        Public Const KEY_ENTER = 13
        Public Const KEY_TAB = 9
        Public Const mpDemoRegKey = _
            "HKEY_LOCAL_MACHINE\SOFTWARE\Legal MacPac\" & _
            "MacPac Numbering 2000 Demo Version\9.2.0"


        'API declarations for LockWindowUpdate() and FindWindow()
        <DllImport("User32.dll")>
        Public Shared Function GetKeyState(nVertKey As Integer) As Integer
        End Function

        <DllImport("User32.dll")>
        Public Shared Function LockWindowUpdate(ByVal hwndLock As IntPtr) As Boolean
        End Function

        <DllImport("User32.dll", SetLastError:=True)>
        Public Shared Function SetActiveWindow(ByVal hWnd As IntPtr) As IntPtr
        End Function

        'HTML help
        Public Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" _
                       (ByVal hwndCaller As Long, ByVal pszFile As String, _
                        ByVal uCommand As Long, ByVal dwData As Long) As Long
        Public Const HH_DISPLAY_TOPIC = &H0         ' select last opened tab, [display a specified topic]
        Public Const HH_DISPLAY_TOC = &H1           ' select contents tab, [display a specified topic]
        Public Const HH_DISPLAY_INDEX = &H2         ' select index tab and searches for a keyword
        Public Const HH_DISPLAY_SEARCH = &H3        ' select search tab and perform a search

        Public Shared g_xTOCSchemes(,) As String
        Private Shared m_xAppPath As String

        Public Declare Function ShellExecute Lib "shell32.dll" _
          Alias "ShellExecuteA" (ByVal hwnd As IntPtr, _
          ByVal lpOperation As String, ByVal lpFile As String, _
          ByVal lpParameters As String, ByVal lpDirectory As String, _
          ByVal nShowCmd As Integer) As IntPtr

        Private Const SW_SHOWNORMAL = 1

        Private Structure OSVERSIONINFO
            Shared dwOSVersionInfoSize As Long
            Shared dwMajorVersion As Long
            Shared dwMinorVersion As Long
            Shared dwBuildNumber As Long
            Shared dwPlatformId As Long
            Shared szCSDVersion As String '  Maintenance string for PSS usage
        End Structure

        <DllImport("kernel32")> _
        Private Shared Function GetVersionEx(ByRef osvi As OSVERSIONINFO) As Boolean
        End Function

        '9.9.5011
        Public Declare Function GetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
            ByVal lAction As Integer, ByVal lParam As Integer, ByRef lpvParam As Integer, ByVal lWinIni As Integer) As Integer
        Public Declare Function SetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
            ByVal lAction As Integer, ByVal lParam As Integer, ByVal lpvParam As Integer, ByVal lWinIni As Integer) As Integer
        Public Const SPI_GETKEYBOARDCUES = 4106
        Public Const SPI_SETKEYBOARDCUES = 4107

        Public Shared Function PublicSchemesDir() As String
            'returns the path to the public schemes dir
            'as specified in the user ini
            Dim xDir As String

            xDir = GetAppSetting("Numbering", "SharedDirectory")
            If xDir <> "" Then
                xDir = GetEnvironVarPath(xDir)
                If Right(xDir, 1) <> "\" Then _
                    xDir = xDir & "\"
            End If
            PublicSchemesDir = xDir
        End Function

        Public Shared Function bSetMenuCtlState(bEnable As Boolean, _
                                  Optional xScheme As String = "") As Boolean
            'enable/disable all menu items based on bEnable-
            'enable/disable Mark and Format item based on
            'whether there is a macpac scheme in the doc
            Dim ctlMenu As CommandBarControl
            Dim tmpActive As Template

            On Error Resume Next

            With CurWordApp.CommandBars( _
                    "Menu Bar").Controls("Numbering")
                For Each ctlMenu In .Controls
                    ctlMenu.Enabled = bEnable
                Next ctlMenu
            End With

            tmpActive = CurWordApp.ActiveDocument.AttachedTemplate
            If tmpActive Is CurWordApp.NormalTemplate Then _
                tmpActive.Saved = True

            MarkStartupSaved()
        End Function

        Public Shared Sub EchoOff()
            Call LockWindowUpdate(FindWindow("OpusApp", 0&))
        End Sub

        Public Shared Sub EchoOn()
            Call LockWindowUpdate(0&)
        End Sub


        Public Shared Function envGetAppEnvironment() As mpAppEnvironment
            'fills type mpAppEnvironment with
            'application environment settings -
            'g_envMpApp below is a global var
            On Error Resume Next
            With CurWordApp
                g_envMpApp.lBrowserTarget = .Browser.Target
                .Assistant.Animation = Microsoft.Office.Core.MsoAnimationType.msoAnimationWritingNotingSomething
            End With

            envGetAppEnvironment = g_envMpApp

        End Function

        Public Shared Function bSetAppEnvironment(envCurrent As mpAppEnvironment, _
                                    Optional bClearUndo As Boolean = False) As Boolean
            'sets application environment settings
            'using values in type mpAppEnvironment

            '   added to ensure that Edit Find is always cleared
            bEditFindReset()

            With CurWordApp
                On Error Resume Next
                .Browser.Target = envCurrent.lBrowserTarget

                If bClearUndo Then
                    '           clear "undo" list
                    CurWordApp.ActiveDocument.UndoClear()
                End If

                '       assistant animation sometimes
                '       sticks, so reset
                With .Assistant
                    .Animation = Microsoft.Office.Core.MsoAnimationType.msoAnimationIdle
                End With
                On Error GoTo 0
            End With


            bSetAppEnvironment = True
        End Function

        Public Shared Function GetAppFiles() As Boolean
            '   fill global vars with appropriate paths
            Dim xAppPath As String

            xAppPath = GetAppPath()

            If Dir(xAppPath & "\tsgNumberingBoilerpl.ate") <> "" Then
                g_xBP = xAppPath & "\tsgNumberingBoilerpl.ate"
            Else
                g_xBP = xAppPath & "\bp.doc"
            End If

            g_xFirmIni = xAppPath & "\tsgNumbering.ini"
            g_xFirmXMLConfig = xAppPath & "\tsgNumberingConfig.xml"
            g_xUserIni = g_xUserPath & "\NumTOC.INI"
            g_xUserXMLConfig = g_xUserPath & "\NumTOCConfig.xml"
            g_xFNum80Sty = xAppPath & "\mpNumbers80.sty"

            If g_bIsAdmin Then
                g_xFNumSty = xAppPath & "\tsgNumbers.sty.clean"
                g_xPNumSty = g_xAdminSchemesPath & "\tsgNumbers.sty"
            Else
                g_xFNumSty = xAppPath & "\tsgNumbers.sty"
                g_xPNumSty = g_xUserPath & "\tsgNumbers.sty"
            End If
        End Function

        Public Shared Function bAppInitialize() As Boolean
            'initializes MPN90
            Dim bRet As Boolean
            Dim xErrSource As String
            Dim bDo As Boolean
            Dim oAT As Word.AutoTextEntry
            Dim oAddIn As Word.AddIn
            Dim xValue As String
            Dim oVersion As OSVERSIONINFO

            Try
                xErrSource = "mpApplication.bAppInitialize"

                'GLOG 5085 - the following line isn't necessary and sometimes errs
                '    Set g_oWord = GetObject(, "CurWordApp")

                ''   (removed in 9.8.2002) ensure correct WD functions for Word version
                '    SetVersionFiles

                'get app path
                m_xAppPath = CodeBasePath

                'GLOG 5224 - get Windows Version
                oVersion.dwOSVersionInfoSize = Len(oVersion)
                GetVersionEx(oVersion)
                g_lWinMajorVersion = oVersion.dwMajorVersion
                g_lWinMinorVersion = oVersion.dwMinorVersion

                '   get paths
                bRet = GetAppPaths()

                '   get files
                bRet = GetAppFiles()

                'get ui language if we don't already have it
                If g_lUILanguage = 0 Then
                    xValue = GetAppSetting("General", "UILanguage")
                    If xValue <> "" Then
                        g_lUILanguage = CLng(xValue)
                        If g_lUILanguage = 0 Then _
                            g_lUILanguage = wdLanguageID.wdEnglishUS
                    End If
                End If

                '   get global template names - moved after previous two functions in 9.9.2008
                '   because we now need to locate ini first
                GetGlobalTemplateNames()

                '   get startup path - this was moved out of GetAppPaths in 9.9.2008
                g_xStartPath = CurWordApp.Options.DefaultFilePath(WdDefaultFilePath.wdStartupPath)

                '   GLOG 5421 - the following call to Dir() was intermittently
                '   erring when g_xStartPath was empty
                Dim bStartupTemplateFound As Boolean
                If g_xStartPath <> "" Then
                    bStartupTemplateFound = (Dir(g_xStartPath & "\" & g_xMPNDot) <> "")
                End If
                If Not bStartupTemplateFound Then
                    '       startup templates may be in Word's default startup directory
                    Try
                        oAddIn = CurWordApp.AddIns(g_xMPNDot)
                    Catch
                    End Try

                    'remmed for GLOG 8773 (dm) - this message was appearing when Word was launched by a 3rd-party with
                    'a different startup path or with Word add-ins disabled - this check is no longer essential in Numbering 10 -
                    'the only reference to the add-in or it's path is in MarkStartupSaved(), which is already error
                    'trapped for its absence
                    'If oAddIn Is Nothing Then
                    '    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                    '        xMsg = "Le mod�le global pr�cis� dans tsgNumbering.ini (" & Chr(34) & _
                    '            g_xMPNDot & Chr(34) & ") n'est pas charg�.  Veuillez contacter votre administrateur."
                    '    Else
                    '        xMsg = "The global template specified in tsgNumbering.ini (" & Chr(34) & _
                    '            g_xMPNDot & Chr(34) & ") is not loaded.  Please contact your " & _
                    '            "system administrator."
                    '    End If
                    '    MsgBox(xMsg, vbExclamation, g_xAppName)
                    '    Exit Function
                    'End If
                    If Not oAddIn Is Nothing Then
                        g_xStartPath = CurWordApp.AddIns(g_xMPNDot).Path
                    End If
                End If

                bRet = bLoadFiles(g_bAllowTOCLink)
                If Not bRet Then
                    Exit Function
                End If

                '9.9.3006 - hold onto tsgNumbers.sty to avoid having to cycle
                'through Templates collection to find it in a network location in Word 2007
                g_oPNumSty = GetTemplate(g_xPNumSty)
                g_oFNumSty = GetTemplate(g_xFNumSty)

                '   transfer level props to correct
                '   storage if necessary - ie if none
                '   are currently there
                '    If Word.Templates(g_xFNumSty).CustomDocumentProperties.Count = 1 Then
                '        TransferPropsToDocProps Word.Templates(g_xFNumSty)
                '    End If

                '   initialize numbering
                lRet = lInitializeNumbering()

                '   get lists
                bRet = bAppGetLists()

                '   get schemes
                lRet = iGetSchemes(g_xPSchemes, mpSchemeTypes.mpSchemeType_Private)
                lRet = iGetSchemes(g_xFSchemes, mpSchemeTypes.mpSchemeType_Public)

                If Not g_bIsAdmin Then
                    '       reset compatibility options in private sty file if necessary (9.8.1001)
                    Try
                        bDo = CBool(GetAppSetting("Numbering", "EnsurePrivateStyCompatibility"))
                    Catch
                    End Try
                    If bDo Then
                        '9.9.6015 (GLOG 5166) - convert private sty file if necessary -
                        ConvertPrivateStyFileIfNecessary()
                    End If

                    '       delete autotext entries in private sty file (9.8.1007)
                    With g_oPNumSty
                        For Each oAT In .AutoTextEntries
                            oAT.Delete()
                        Next oAT
                    End With

                    'GLOG 8779 - convert user ini to xml
                    If InDevelopmentModeState(8) Then
                        If File.Exists(g_xUserIni) Then
                            Convert(g_xUserIni, g_xUserXMLConfig)
                            Kill(g_xUserIni)
                        End If
                    End If
                End If

                '   create global variable for session
                CreateSessionVariable()

                '   remove sty files as add-ins
                UnLoadStyFiles()

                '   delete 00mpnStart.dot
                RemoveStartDot()

                bAppInitialize = True

            Finally
                EchoOn()
            End Try
        End Function

        Public Shared Function GetAppPaths()
            '   loads start, work, user and litigation paths
            '   into respective global vars
            Dim xAppPath As String
            Dim lPos As Long

            xAppPath = GetAppPath()
            g_xWorkgroupPath = CurWordApp.Options.DefaultFilePath(WdDefaultFilePath.wdWorkgroupTemplatesPath)

            '   get admin schemes directory
            On Error Resume Next
            g_xAdminSchemesPath = GetAppSetting("Numbering", "AdminDirectory")
            If g_xAdminSchemesPath = "" Then
                g_xAdminSchemesPath = xAppPath & "\Admin Schemes"
            ElseIf (InStr(UCase(g_xAdminSchemesPath), "<USERNAME>") > 0) Or
                    (InStr(UCase(g_xAdminSchemesPath), "<USER>") > 0) Then
                '       use API to get Windows user name
                g_xAdminSchemesPath = GetUserVarPath(g_xAdminSchemesPath)
            Else
                '       use environmental variable
                g_xAdminSchemesPath = GetEnvironVarPath(g_xAdminSchemesPath)
            End If
            If Right(g_xAdminSchemesPath, 1) = "\" Then
                g_xAdminSchemesPath = Left(g_xAdminSchemesPath,
                    Len(g_xAdminSchemesPath) - 1)
            End If
            On Error GoTo 0

            '   get personal directory
            g_xUserPath = GetUserDir()

            If g_bIsAdmin Then
                g_xMPBPath = g_xAdminSchemesPath
            Else
                g_xMPBPath = g_xUserPath
            End If

            'get pdf help file path (9.9.4006)
            lPos = InStrRev(m_xAppPath, "\")
            g_xHelpPath = Left$(m_xAppPath, lPos) & "Tools\User Documentation\pdf"
        End Function

        Public Shared Function bQuit()
            CurWordApp.Quit()
        End Function

        Public Shared Function RaiseError(xSource As String)
            Dim oError As CError
            oError = New CError
            Err.Source = xSource
            EchoOn()
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            CurWordApp.StatusBar = ""
            'SendShiftKey() 'remmmed 9.9.6014
            oError.Raise(Err)
            oError = Nothing
        End Function

        Public Shared Sub GetAppOptions()
            Dim xKey As String
            Dim bDisallow As Boolean
            Dim xDMS As String
            Dim xValue As String

            '   get options
            On Error GoTo ProcError
            xKey = "AllowSchemeSharing"
            g_bAllowSchemeSharing = GetAppSetting(
                        "Numbering", "AllowSchemeSharing")

            xKey = "AllowSchemeEdit"
            g_bAllowSchemeEdit = GetAppSetting(
                        "Numbering", "AllowSchemeEdit")

            xKey = "AllowSchemeNew"
            g_bAllowSchemeNew = GetAppSetting(
                        "Numbering", "AllowSchemeNew")

            xKey = "AllowLinkToTOCScheme"
            g_bAllowTOCLink = CBool(GetAppSetting("Numbering",
                "AllowLinkToTOCScheme")) And g_bAllowSchemeNew

            g_bShowPersonalSchemes = (g_bAllowSchemeSharing Or
                                      g_bAllowSchemeNew)

            On Error Resume Next

            g_iUpgradeFrom = GetAppSetting("Numbering", "UpgradeFrom")
            g_bResetFonts = GetAppSetting("Numbering", "ResetFontsOnFileOpen")
            g_bApplyHeadingColor = GetAppSetting("Numbering", "ApplyColorToHeadings")
            g_bDocumentChange = GetAppSetting("Numbering", "DocumentChangeEvent")
            g_bAlwaysAdjustSpacing = GetAppSetting("Numbering", "AdjustSpacingInAllSchemes")
            g_bAcceptAllFlags = GetAppSetting("Numbering", "AcceptAll9.7Flags")
            g_bCodeForMSBug = GetAppSetting("Numbering", "CodeForMicrosoftIndentBug")
            g_bPost97AdminWarning = GetAppSetting("Numbering", "Post97AdminWarning")
            g_bPreserveRestarts = GetAppSetting("Numbering", "PreserveRestartsOnChangeTo")
            g_bGenerateBufferFromCopy = GetAppSetting("Numbering", "GenerateBufferFromCopy")
            g_lActiveSchemeIcon = GetAppSetting("Numbering", "ActiveSchemeIcon")
            g_xBufferTemplate = GetAppSetting("Numbering", "BufferTemplate")

            '   set to no upgrade if key is not present
            If g_iUpgradeFrom = 0 Then
                g_iUpgradeFrom = 90
            End If

            '   get style to apply when removing a number
            g_xRemoveNumberStyle = GetAppSetting(
                "Numbering", "RemoveNumberStyle")
            If Len(g_xRemoveNumberStyle) = 0 Then
                g_xRemoveNumberStyle = "Normal"
            End If

            '   get language-specific list num text
            g_xListNum = GetAppSetting("Numbering", "ListNumText")
            If g_xListNum = "" Then g_xListNum = "LISTNUM"

            '   get DMS - first look in MacPac.ini
            If g_xMP90Dir <> "" Then
                'look in 9.7.1 location
                xDMS = CurWordApp.System.PrivateProfileString(g_xMP90Dir &
                    "MacPac.ini", "DMS", "DMS")
                If xDMS = "" Then
                    'look in pre-9.7.1 location
                    xDMS = CurWordApp.System.PrivateProfileString(g_xMP90Dir &
                        "MacPac.ini", "General", "DMS")
                End If
                g_iDMS = CInt(xDMS)
            Else
                g_iDMS = GetAppSetting("General", "DMS")
            End If

            '9.9.4007 - added new ini key for organizer save prompt that's
            'not specific to any Word version
            If Not g_bOrganizerSavePrompt Then _
                g_bOrganizerSavePrompt = GetAppSetting("General", "OrganizerSavePrompt")

            '9.9.4004 - option to not clear undo list
            xValue = GetAppSetting("General", "PreserveUndoList")
            g_bPreserveUndoList = (UCase$(xValue) = "TRUE")

            '9.9.4010 (GLOG 4917) - option to use unlinked paragraph styles
            xValue = ""
            xValue = GetAppSetting("Numbering", "CreateUnlinkedStyles")
            If xValue <> "" Then _
                    g_bCreateUnlinkedStyles = (UCase$(xValue) = "TRUE")

            '9.9.4014 (GLOG 5025) - option to prompt before automatically relinking
            xValue = ""
            xValue = GetAppSetting("Numbering", "PromptBeforeAutoRelinking")
            If xValue <> "" Then _
                g_bPromptBeforeAutoRelinking = (UCase$(xValue) = "TRUE")

            '9.9.4018 (GLOG 5160) - option to bypass timer by setting existing key to 0
            g_xTimerDelay = GetAppSetting("Timer", "Delay")

            '9.9.5012 (GLOG 5461) - option to warn if track changes is on
            xValue = GetAppSetting("Numbering", "TrackChangesWarningThreshold")
            If IsNumeric(xValue) Then
                g_lTrackChangesWarningThreshold = CLng(xValue)
            Else
                g_lTrackChangesWarningThreshold = 1000
            End If

            '9.9.6004 (GLOG 5525) - option to load cont styles with scheme or on demand
            xValue = GetAppSetting("Numbering", "LoadContinuationStyles")
            If (xValue = "1") Or (xValue = "2") Then
                g_iLoadContStyles = CInt(xValue)
            Else
                g_iLoadContStyles = mpLoadContStyles.mpLoadWithScheme
            End If

            '9.9.6012 (GLOG 5625) - different loop lengths to get ruler to display
            'in edit scheme preview under different circumstances
            'edit from New Scheme dialog
            xValue = ""
            xValue = GetAppSetting("Numbering", "RulerDisplayDelay_New")
            If xValue = "" Then
                g_lRulerDisplayDelay_New = 125
            Else
                g_lRulerDisplayDelay_New = CLng(xValue)
            End If

            'edit from Schemes dialog
            xValue = ""
            xValue = GetAppSetting("Numbering", "RulerDisplayDelay_Edit")
            If xValue = "" Then
                g_lRulerDisplayDelay_Edit = 4
            Else
                g_lRulerDisplayDelay_Edit = CLng(xValue)
            End If

            'edit from ribbon
            xValue = ""
            xValue = GetAppSetting("Numbering", "RulerDisplayDelay_EditDirect")
            If xValue = "" Then
                g_lRulerDisplayDelay_EditDirect = 3
            Else
                g_lRulerDisplayDelay_EditDirect = CLng(xValue)
            End If

            'GLOG 5624 (9.9.6012)
            xValue = ""
            xValue = GetAppSetting("Numbering", "DefaultZoomPercentage")
            If xValue = "" Then
                g_lDefaultZoomPercentage = 100
            Else
                g_lDefaultZoomPercentage = CLng(xValue)
            End If

            'GLOG 5643 (9.9.6014)
            xValue = ""
            xValue = GetAppSetting("Numbering", "CollapseRibbonOnDirectEdit")
            g_bCollapseRibbon = (UCase$(xValue) <> "FALSE")

            'GLOG 15891 (dm)
            xValue = ""
            xValue = GetAppSetting("Numbering", "DisplayContStylesInNextParaList")
            g_bDisplayContStylesInNextParaList = (UCase$(xValue) <> "FALSE")

            'GLOG 15891 (dm)
            xValue = ""
            xValue = GetAppSetting("Numbering", "UseWordHeadingStyles")
            g_bUseWordHeadingStyles = (UCase$(xValue) = "TRUE")

            Err.Clear()
            Exit Sub

ProcError:
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                xMsg = "Impossible de r�cup�rer les valeurs de demarrage.  " &
                       "La cl� '" & xKey & "' key est manquante. Veuillez contacter votre administrateur."
            Else
                xMsg = "Could not retrieve required initialization values.  " &
                       "The '" & xKey & "' key is missing.  Please contact " &
                       "your administrator."
            End If
            MsgBox(xMsg, vbCritical, g_xAppName)
            Exit Sub
        End Sub

        Public Shared Function MarkStartupSaved()
            'force startup templates to be 'saved'
            On Error Resume Next
            With CurWordApp
                GetTemplate(g_xStartPath & "\" & g_xMPNDot).Saved = True
                '       error trap isn't enough for Word 10;
                '       if files aren't there, trying to set .Saved will
                '       disable mouse pointer for rest of session
                If Dir(g_xStartPath & "\" & g_xMPNConvertDot) <> "" Then _
                    GetTemplate(g_xStartPath & "\" & g_xMPNConvertDot).Saved = True
                If Dir(g_xStartPath & "\" & g_xMPNAdminDot) <> "" Then _
                    GetTemplate(g_xStartPath & "\" & g_xMPNAdminDot).Saved = True
            End With
            Err.Clear()
        End Function

        Public Shared Sub AlertBadFileVer(ByVal xFile As String)
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                If Dir(xFile) = "" Then
                    xMsg = "Le fichier '" & xFile & "' n'est pas install� sur votre ordinateur. Veuillez contacter votre administrateur pour obtenir la version appropri�e."
                Else
                    xMsg = "La version de ce fichier '" & xFile & "' install�e sur votre ordinateur ne peut pas utiliser Microsoft Word version " &
                            CurWordApp.Version & "." & vbCr & vbCr &
                            "Veuillez contacter votre administrateur pour obtenir la version appropri�e."
                End If
            Else
                If Dir(xFile) = "" Then
                    xMsg = "The file '" & xFile & "' is not installed on " &
                           "this machine." & vbCr & "Please contact your administrator " &
                           "to obtain the appropriate version of this file."
                Else
                    xMsg = "The version of the file '" & xFile &
                           "' that is " & vbCr & "currently installed on " &
                           "this machine can not run in Microsoft Word version " &
                            CurWordApp.Version & "." & vbCr & vbCr &
                            "Please contact your administrator to obtain " &
                            "the appropriate version of this file."
                End If
            End If
            MsgBox(xMsg, vbCritical, g_xAppName)
        End Sub

        Public Shared Function WorkAsAdmin() As Long
            Dim xFile As String

            '   set admin flags
            g_bIsAdmin = True
            g_bAdminIsDirty = False
            lSetAdminFlag(True)

            '   remove user limitations
            g_bAllowSchemeEdit = True
            g_bAllowSchemeNew = True
            g_bAllowTOCLink = True
            g_bShowPersonalSchemes = True

            '   point to admin sty files
            GetAppPaths()
            GetAppFiles()

            '   load clean sty as public
            CurWordApp.AddIns.Add(g_xFNumSty)
            lRet = iGetSchemes(g_xFSchemes, mpSchemeTypes.mpSchemeType_Public)

            '   if admin directory exists, load schemes as private
            If Dir(g_xPNumSty) = "" Then
                lRet = iGetSchemes(g_xPSchemes, mpSchemeTypes.mpSchemeType_Public)
            Else
                CurWordApp.AddIns.Add(g_xPNumSty)
                lRet = iGetSchemes(g_xPSchemes, mpSchemeTypes.mpSchemeType_Private)
            End If

            '9.9.3006 - hold onto tsgNumbers.sty to avoid having to cycle
            'through Templates collection to find it in a network location in Word 2007
            g_oPNumSty = GetTemplate(g_xPNumSty)
            g_oFNumSty = GetTemplate(g_xFNumSty)

            '   show dialogs
            g_iSchemeEditMode = mpModeSchemeMain
            ShowSchemesDialogs()
        End Function

        Public Shared Function WorkAsUser() As Long
            If Not g_bIsAdmin Then _
                Exit Function

            g_bIsAdmin = False
            lSetAdminFlag(False)

            With CurWordApp.AddIns
                If Dir(g_xPNumSty) <> "" Then _
                    .Item(g_xPNumSty).Delete()
                .Item(g_xFNumSty).Delete()
            End With

            bAppInitialize()
        End Function

        Public Shared Function LoadPublicSchemesAsAdmin()
            Dim xFile As String
            Dim xAppPath As String

            xAppPath = GetAppPath()

            '   create admin schemes directory if necessary
            If Not Directory.Exists(g_xAdminSchemesPath) Then
                Try
                    Directory.CreateDirectory(g_xAdminSchemesPath)
                Catch oE As System.Exception
                    If Err.Number = 76 Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            MsgBox("Le r�pertoire Admin n'a pas �t� cr��, l'emplacement precise dans " & cNumTOC.SettingsFileName & ", " & Chr(34) & g_xAdminSchemesPath &
                                Chr(34) & ", est invalide.  Veuillez fermer la bo�te de dialogue, modifier l'emplacement et essayez de nouveau.", vbCritical, g_xAppName)
                        Else
                            MsgBox("The Admin Directory could not be created because the " &
                                "path specified in " & cNumTOC.SettingsFileName & ", " & Chr(34) & g_xAdminSchemesPath &
                                Chr(34) & ", is invalid.  Please close this dialog box, " &
                                "edit the path, and try again.", vbCritical, g_xAppName)
                        End If
                        Exit Function
                    Else
                        Throw oE
                    End If
                End Try
            End If

            '   remove as add-in before trying to delete
            Try
                CurWordApp.AddIns(g_xPNumSty).Installed = False
            Catch
            End Try

            '   clear admin schemes directory
            Try
                Kill(g_xAdminSchemesPath & "\*.*")
            Catch
            End Try

            '   copy public bitmaps
            xFile = Dir(xAppPath & "\*.mpb")
            While xFile <> ""
                FileCopy(xAppPath & "\" & xFile,
                    g_xAdminSchemesPath & "\" & xFile)
                xFile = Dir()
            End While

            '   copy public tsgNumbers.sty
            CurWordApp.AddIns(xAppPath & "\tsgNumbers.sty").Installed = False
            FileCopy(xAppPath & "\tsgNumbers.sty", g_xPNumSty)

            '   load admin schemes
            Try
                CurWordApp.AddIns.Add(g_xPNumSty)
            Catch
            End Try

            CurWordApp.AddIns(g_xPNumSty).Installed = True
            g_oPNumSty = GetTemplate(g_xPNumSty)
            lRet = iGetSchemes(g_xPSchemes, mpSchemeTypes.mpSchemeType_Private)
        End Function

        Public Shared Function DeleteMPFiles() As Long
            Dim xFile As String
            Dim xNumbersSty As String

            On Error Resume Next
            CurWordApp.AddIns(g_xPNumSty).Installed = False

            On Error GoTo DeleteMPFiles_Error

            '   delete personal files
            If Dir(g_xPNumSty) <> "" Then _
                Kill(g_xPNumSty)
            If Dir(g_xUserPath & "\NumTOC.ini") <> "" Then _
                Kill(g_xUserPath & "\NumTOC.ini")
            If Dir(g_xUserPath & "\NumTOCConfig.xml") <> "" Then _
                Kill(g_xUserPath & "\NumTOCConfig.xml")
            xFile = Dir(g_xUserPath & "\*.mpb")
            While xFile <> ""
                Kill(g_xUserPath & "\" & xFile)
                xFile = Dir()
            End While

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                xMsg = "Les fichiers TSG ont �t� supprim�s. Pour supprimer enti�rement TSG Num�rotation:" & New String(Chr(13), 2) &
                    "1. Enlevez " & g_xMPNDot & ", " & g_xMPNAdminDot & ", et " &
                    g_xMPNConvertDot & " du r�pertoire de D�marrage Word." &
                    New String(Chr(13), 2) & "2. Allez � " & Chr(34) & "Ajout/Suppression de programmes" & Chr(34) & " dans le Panneau de configuration Windows et s�lectionnez TSG dans la liste des programmes."
            Else
                xMsg = "The specified TSG files have been deleted.  To remove " &
                    "the remainder of TSG Numbering:" & New String(Chr(13), 2) &
                    "1. Remove " & g_xMPNDot & ", " & g_xMPNAdminDot & ", and " &
                    g_xMPNConvertDot & " from your Word Startup directory." &
                    New String(Chr(13), 2) & "2. Go to " & Chr(34) & "Add/Remove Programs" & Chr(34) &
                    " on the Windows Control Panel, and select " & Chr(34) &
                    "TSG Numbering" & Chr(34) & " from the list of programs."
            End If
            MsgBox(xMsg, vbInformation, g_xAppName)

            Exit Function

DeleteMPFiles_Error:
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                xMsg = "Impossible de supprimer les fichiers pr�cis�s. Pour supprimer TSG Num�rotation:" & New String(Chr(13), 2) &
                    "1. Supprimez tsgNumbers.sty, NumTOC.ini, NumTOCConfig.xml et tous fichiers avec l'extension .mpb du r�pertoire Word mod�les utilisateur." & New String(Chr(13), 2) &
                    "2. Supprimez " & g_xMPNDot & ", " & g_xMPNAdminDot & ", et " &
                    g_xMPNConvertDot & " du r�pertoire de D�marrage Word." &
                    New String(Chr(13), 2) & "3. Allez � " & Chr(34) & "Ajout/Suppression de programmes" & Chr(34) & " dans le Panneau de configuration Windows et s�lectionnez MacPac dans la liste des programmes."
            Else
                xMsg = "Unable to delete specified files.  To remove " &
                    "TSG Numbering:" & New String(Chr(13), 2) &
                    "1. Delete tsgNumbers.sty, NumTOC.ini, NumTOCConfig.xml, and all files with an .mpb extension, " &
                    "from your Word User templates directory." & New String(Chr(13), 2) &
                    "2. Remove " & g_xMPNDot & ", " & g_xMPNAdminDot & ", and " &
                    g_xMPNConvertDot & " from your Word Startup directory." &
                    New String(Chr(13), 2) & "3. Go to " & Chr(34) & "Add/Remove Programs" & Chr(34) &
                    " on the Windows Control Panel, and select " & Chr(34) &
                    "TSG Numbering" & Chr(34) & " from the list of programs."
            End If
            MsgBox(xMsg, vbInformation, g_xAppName)
            Exit Function

        End Function

        Public Shared Sub RemoveBufferFromList()
            'GLOG 15883 (dm) - we were previously only deleting buffer and if it was at the top of the list-
            'after creating a new scheme, one or more sty entries will be above it
            Dim i As Integer
            Dim iCount As Integer
            Dim xName As String

            On Error GoTo ProcError

            i = 1
            With CurWordApp.RecentFiles
                iCount = .Count
                While (i <= iCount)
                    With .Item(i)
                        xName = .Name
                        If (InStr(UCase(xName), "BUFFER.MPF")) +
                                (InStr(UCase(xName), "REFRESH.MPF")) +
                                (InStr(UCase(xName), "TSGNUMBERS.STY")) > 0 Then
                            If (InStr(UCase(xName), "TSGNUMBERS.STY")) = 0 Then
                                .Delete()
                                Exit Sub
                            End If
                            i = i + 1
                        Else
                            Exit Sub
                        End If
                    End With
                End While
            End With
ProcError:
        End Sub

        Public Shared Function MacPacIsLegal() As Boolean
            Dim d As Date
            Dim xCode As String
            Dim bIsValid As Boolean
            Dim oForm As System.Windows.Forms.Form
            Dim iPos As Integer
            Dim xMPPath As String
            Dim oDlg As frmLicenseNum
            Dim oDlgFrench As frmLicenseNumFrench

            '   get MacPac app directory if it exists
            g_xMP90Dir = GetMacPacAppPath()

            If g_xMP90Dir = "" Then
                '       registry may be unavailable, e.g. Citrix;
                '       try to locate MacPac.ini by relative location to Numbering
                iPos = InStr(UCase(m_xAppPath), "\NUMBERING\APP")
                If iPos Then
                    xMPPath = Left(m_xAppPath, iPos) & "MacPac90\App\"
                    If Dir(xMPPath & "MacPac.ini") <> "" Then
                        g_xMP90Dir = xMPPath
                    End If
                End If
            End If

            '   first look for license in tsgNumbering.ini
            xCode = GetAppSetting("General", "Signature")

            '   if no license in tsgNumbering.ini, look in MacPac.ini
            If xCode = "" Then
                If g_xMP90Dir <> "" Then
                    xCode = CurWordApp.System.PrivateProfileString(g_xMP90Dir &
                        "MacPac.ini", "General", "Signature")
                    If xCode <> "" Then
                        '               write license to tsgNumbering.ini
                        SetAppSetting("General", "Signature", xCode)
                    End If
                End If
            End If

            If xCode = "" Then
                bIsValid = False
            Else
                '       unencrypt date
                d = UnEncryptDate(xCode)
                bIsValid = (Now < d)
            End If

            '   load license key prompt
            If Not bIsValid Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    oForm = New frmLicenseNumFrench
                    oDlgFrench = CType(oForm, frmLicenseNumFrench)
                Else
                    oForm = New frmLicenseNum
                    oDlg = CType(oForm, frmLicenseNum)
                End If
            End If

            While Not bIsValid
                With oForm
                    'show prompt
                    .ShowDialog()

                    If .DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                        Exit Function
                    Else
                        '               unencrypt date
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            With oDlgFrench
                                d = UnEncryptDate(.Key)

                                '               test for validity
                                bIsValid = (Now < UnEncryptDate(.Key))

                                '               write to ini if valid
                                If bIsValid Then
                                    SetAppSetting("General", "Signature", .Key)
                                    If g_xMP90Dir <> "" Then
                                        '                       prevent second prompt when MacPac initializes
                                        CurWordApp.System.PrivateProfileString(g_xMP90Dir &
                                            "MacPac.ini", "General", "Signature") = .Key
                                    End If
                                Else
                                    '                   set message for next time around
                                    .Message = "Saisie incorrecte.  Veuillez saisir un num�ro de license correct:"
                                End If
                            End With
                        Else
                            With oDlg
                                d = UnEncryptDate(.Key)

                                '               test for validity
                                bIsValid = (Now < UnEncryptDate(.Key))

                                '               write to ini if valid
                                If bIsValid Then
                                    SetAppSetting("General", "Signature", .Key)
                                    If g_xMP90Dir <> "" Then
                                        '                       prevent second prompt when MacPac initializes
                                        CurWordApp.System.PrivateProfileString(g_xMP90Dir &
                                            "MacPac.ini", "General", "Signature") = .Key
                                    End If
                                Else
                                    '                   set message for next time around
                                    .Message = "Invalid entry.  Please enter a valid license number:"
                                End If
                            End With
                        End If
                    End If
                End With
            End While

            '   unload form if necessary
            If Not oForm Is Nothing Then
                oForm.Close()
            End If

            MacPacIsLegal = bIsValid
        End Function

        Public Shared Function UnEncryptDate(ByVal xCode As String) As Date
            'returns the date encrypted in xcode
            Dim bRnd As Byte
            Dim xCodeMod As String
            Dim i As Integer
            Dim xDate As String
            Dim X As String

            On Error GoTo ProcError

            '   remove garbage 1st digit
            xCode = Mid(xCode, 2)

            '   store and remove random factor
            'GLOG 8800: This will generate error if 2nd character of license isn't numeric
            bRnd = Left(xCode, 1)
            xCode = Mid(xCode, 2)

            '   collect chars at even indexes
            For i = 2 To Len(xCode) Step 2
                xCodeMod = xCodeMod & Mid(xCode, i, 1)
            Next

            While Len(xCodeMod)
                X = Left(xCodeMod, 1)
                If Not IsNumeric(X) Then
                    X = Asc(X) - 100
                End If

                xDate = xDate & X

                '       trim left char
                xCodeMod = Mid(xCodeMod, 2)
            End While

            'DanCore 9.2.0 - UnEncryptDateFix - remember to add fix to PostInstall utility
            '   convert to date
            UnEncryptDate = DateSerial(Mid(xDate, 3, 4), Mid(xDate, 7, 2), Mid(xDate, 1, 2))
            'GLOG 8800: Ignore dated past 2099 to reduce chance of random string returning valid value
            If UnEncryptDate.Year > 2099 Then
                UnEncryptDate = DateTime.MinValue
            End If
            Exit Function

ProcError:
            UnEncryptDate = DateTime.MinValue
            Exit Function
        End Function

        Public Shared Function UnloadTOCSty() As Boolean
            Dim xTOCSTY As String

            On Error Resume Next

            xTOCSTY = GetAppPath() & "\tsgTOC.sty"
            CurWordApp.AddIns(xTOCSTY).Installed = False
        End Function

        Public Shared Function RemoveStartDot()
            Dim aiAddIn As Word.AddIn

            On Error Resume Next

            For Each aiAddIn In CurWordApp.AddIns
                With aiAddIn
                    If ((UCase(.Name) = "00MPNSTART.DOT") Or
                            (UCase(.Name) = "00MPNSTART.DOTM")) And
                            (.Installed = True) Then
                        .Delete()
                        Kill(.Path & "\" & .Name)
                        Exit Function
                    End If
                End With
            Next aiAddIn
        End Function

        Private Shared Sub GetGlobalTemplateNames()
            '9.9.2008 - the name of the main global template can now be specified in the ini
            '9.9.3 - if it's "MacPac Numbering.dotm" or "MacPac Numbering French.dotm", we
            'already have the name
            If g_xMPNDot = "" Then _
                g_xMPNDot = GetAppSetting("General", "GlobalTemplateName")

            If g_xMPNDot = "" Then _
                g_xMPNDot = "tsgNumbering.dotm"
            g_xMPNAdminDot = "MacPac Numbering Administration.dotm"
            g_xMPNConvertDot = "MacPac Numbering Convert.dotm"
        End Sub

        '********************************
        'Benchmark functions - use these
        'instead of Now - they're much
        'more precise

        Public Shared Function CurrentTick() As Integer
            CurrentTick = System.Environment.TickCount()
        End Function

        Public Shared Function ElapsedTime(lStartTick As Integer) As Single
            'returns the time elapsed from lStartTick-
            'precision in milliseconds
            ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
        End Function

        Public Shared Function GetTemplate(xFile As String) As Word.Template
            'added in 9.9.2009 to workaround native Word 2007 SP2 bug whereby
            'Word.Templates() no longer takes a network path -
            'moved from cShare in 9.9.3006
            Dim oTemplate As Word.Template
            Dim i As Integer
            Dim xName As String
            Dim lPos As Long

            On Error Resume Next
            oTemplate = CurWordApp.Templates(xFile)
            On Error GoTo 0

            If oTemplate Is Nothing Then
                lPos = InStrRev(xFile, "\")
                xName = Mid$(xFile, lPos + 1)
                For i = 1 To CurWordApp.Templates.Count
                    If CurWordApp.Templates(i).Name = xName Then
                        oTemplate = CurWordApp.Templates(i)
                        Exit For
                    End If
                Next i
            End If

            GetTemplate = oTemplate
        End Function

        Public Shared Sub LaunchDocumentByExtension(xFileName As String)
            Dim iRet As Integer

            iRet = ShellExecute(0, "open", xFileName, "", "", SW_SHOWNORMAL)
            If iRet <= 32 Then
                MsgBox("The document could not be opened.  " &
                    "There may not be an installed application associated with this extension.", vbExclamation)
            End If
        End Sub

        Public Shared Sub SendShiftKey()
            'GLOG 5617 - an unqualified call to SendKeys causes a permission denied error
            'when running in design on Windows 10 - needs to be error trapped for earlier
            'operating systems without this object
            On Error Resume Next
            CreateObject("Wscript.Shell").SendKeys("+", True)
            If Err.Number <> 0 Then
                System.Windows.Forms.SendKeys.Send("+")
            End If
        End Sub

        Public Shared Sub ConvertPrivateStyFileIfNecessary()
            'added in 9.9.6015 (GLOG 5166)
            Dim iVersion As Integer
            Dim iNewVersion As Integer
            Dim oDoc As Word.Document
            Dim oDoc1 As Word.Document

            Try
                'check doc prop for current compatibility - this wasn't
                'maintained in the past, but at worst will indicate older
                'version, forcing us to open the template unnecessarily -
                'no need to convert 2010 and later because there have been
                'no subsequent compatibility changes relevant to Numbering
                Try
                    iVersion = g_oPNumSty.CustomDocumentProperties("WordVersion").Value
                Catch
                End Try
                If iVersion >= mpWordVersions.mpWordVersion_2010 Then _
                    Exit Sub

                'opening template as doc will close untouched document 1
                For Each oDoc In CurWordApp.Documents
                    If oDoc.Name = "Document1" Then
                        If oDoc.Saved = True Then
                            oDoc.Saved = False
                            oDoc1 = oDoc
                            Exit For
                        End If
                    End If
                Next oDoc

                EchoOff()
                CurWordApp.ScreenUpdating = False

                'convert if necessary
                oDoc = g_oPNumSty.OpenAsDocument
                If oDoc.CompatibilityMode < 14 Then
                    oDoc.Convert()
                End If

                With oDoc
                    'add/update doc prop
                    iNewVersion = oDoc.CompatibilityMode
                    If iVersion = 0 Then
                        .CustomDocumentProperties.Add("WordVersion", False,
                            MsoDocProperties.msoPropertyTypeNumber, iNewVersion)
                    Else
                        .CustomDocumentProperties("WordVersion").Value = iNewVersion
                    End If

                    'save template
                    .Saved = False
                    .SaveAs(g_oPNumSty.FullName)
                    .Close(False)
                End With

                'retore document 1 if necessary
                If Not oDoc1 Is Nothing Then
                    oDoc1.Saved = True
                End If

                'remove mpNumbers.sty from recent files list
                With CurWordApp.RecentFiles
                    If .Count Then
                        With .Item(1)
                            If UCase(.Name) = "TSGNUMBERS.STY" Then
                                .Delete()
                            End If
                        End With
                    End If
                End With
            Finally
                CurWordApp.ScreenUpdating = True
                EchoOn()
            End Try
        End Sub
    End Class
End Namespace

