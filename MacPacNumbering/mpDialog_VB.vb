Option Explicit On

Imports System.Windows.Forms
Imports LMP.Numbering.Base.cIO
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumbers
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlN90

Namespace LMP.Numbering
    Friend Class mpDialog_VB
        'TODO: redo class for VB.Net
        Structure POINTAPI
            Shared X As Long
            Shared Y As Long
        End Structure

        Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
        Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
        Public Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
        Public Declare Function MapVirtualKey Lib "user32" Alias "MapVirtualKeyA" (ByVal wCode As Long, ByVal wMapType As Long) As Long
        Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
        Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

        Public Const KEYEVENTF_KEYUP = &H2
        Public Const VK_SHIFT = &H10
        Public Const VK_CONTROL = &H11
        Public Const SM_CXSCREEN = 0
        Public Const SM_CYSCREEN = 1
        Public Const MOUSEEVENTF_MOVE = &H1 '  mouse move
        Public Const MOUSEEVENTF_LEFTDOWN = &H2 '  left button down
        Public Const MOUSEEVENTF_LEFTUP = &H4 '  left button up
        Public Const MOUSEEVENTF_RIGHTDOWN = &H8 '  right button down
        Public Const MOUSEEVENTF_RIGHTUP = &H10 '  right button up
        Public Const MOUSEEVENTF_MIDDLEDOWN = &H20 '  middle button down
        Public Const MOUSEEVENTF_MIDDLEUP = &H40 '  middle button up
        Public Const MOUSEEVENTF_ABSOLUTE = &H8000 '  absolute move

        Public Shared Function bEnsureSelectedContent(ctlP As Control, _
            Optional ByVal bLastCharOnly As Boolean = False, _
            Optional ByVal bAlways As Boolean = False) As Boolean
            '   select content only if tabbed
            '   or hotkeyed into
            If IsPressed(KEY_TAB) Or IsPressed(KEY_MENU) Or bAlways Then
                With ctlP
                    If TypeOf (ctlP) Is ListBox Then
                        Dim oLB As ListBox
                        oLB = CType(ctlP, ListBox)
                        If oLB.SelectedIndex < 0 Then _
                            oLB.SelectedIndex = 0
                    ElseIf TypeOf (ctlP) Is ComboBox Then
                        Dim oCB As ComboBox
                        oCB = CType(ctlP, ComboBox)
                        If bLastCharOnly = False Then
                            oCB.SelectionStart = 0
                            oCB.SelectionLength = Len(.Text)
                        Else
                            oCB.SelectionStart = Len(.Text)
                            oCB.SelectionLength = 1
                        End If
                    ElseIf TypeOf (ctlP) Is TextBox Then
                        Dim oTB As TextBox
                        oTB = CType(ctlP, TextBox)
                        If bLastCharOnly = False Then
                            oTB.SelectionStart = 0
                            oTB.SelectionLength = Len(.Text)
                        Else
                            oTB.SelectionStart = Len(.Text)
                            oTB.SelectionLength = 1
                        End If
                    ElseIf TypeOf (ctlP) Is RichTextBox Then
                        Dim oTB As RichTextBox
                        oTB = CType(ctlP, RichTextBox)
                        If bLastCharOnly = False Then
                            oTB.SelectionStart = 0
                            oTB.SelectionLength = Len(.Text)
                        Else
                            oTB.SelectionStart = Len(.Text)
                            oTB.SelectionLength = 1
                        End If
                    End If
                End With
            End If
        End Function

        '        Public shared Sub IncrementBuddyCtl(ctlP As UpDown, _
        '                             Optional ByVal sOffset As Single, _
        '                             Optional ByVal sMajorTick As Single = 0.25)
        '            Dim xFormat As String
        '            Dim dNew As Double
        '            Dim sCur As Single
        '            Dim dNextMajorTick As Double
        '            Dim xTag As String
        '            Dim xDecimal As String

        '            '   regional setting for PC may use something
        '            '   other than period (e.g. comma) as decimal symbol
        '            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
        '            xTag = xSubstitute(ctlP.Tag, ".", xDecimal)

        '            '   get offset from tag
        '            '   prop if not supplied
        '            If sOffset = 0 Then
        '                sOffset = CSng(xTag)
        '            End If

        '            '   set format based on int/sng
        '            If InStr(sOffset, xDecimal) Then
        '                xFormat = "#,##0.00"
        '            Else
        '                xFormat = "#,##0"
        '            End If

        '            With Screen.ActiveForm.Controls(ctlP.BuddyControl)
        '                If .Text = "" Then
        '                    .Text = 0
        '                End If
        '                '       get current value of control
        '                sCur = CSng(.Text)
        '                ''       new value is current plus offset, or the nearest greater
        '                ''       major tick, whichever is less
        '                '        dNextMajorTick = sCur + (sMajorTick - (((sCur * 100) Mod (sMajorTick * 100)) / 100))
        '                '        dNew = mpMin(sCur + sOffset, dNextMajorTick)
        '                dNew = sCur + sOffset
        '                '       ensure that value is <= maximum acceptable value
        '                dNew = mpMin(dNew, ctlP.Max)
        '                .SetFocus()
        '                .Text = Format(dNew, xFormat)
        '            End With
        '        End Sub

        '        Public shared Sub DecrementBuddyCtl(ctlP As UpDown, _
        '                             Optional ByVal sOffset As Single, _
        '                             Optional ByVal sMajorTick As Single = 0.25)
        '            Dim xFormat As String
        '            Dim dNew As Double
        '            Dim sCur As Single
        '            Dim dPrevMajorTick As Double
        '            Dim xTag As String
        '            Dim xDecimal As String

        '            '   regional setting for PC may use something
        '            '   other than period (e.g. comma) as decimal symbol
        '            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
        '            xTag = xSubstitute(ctlP.Tag, ".", xDecimal)

        '            '   get offset from tag
        '            '   prop if not supplied
        '            If sOffset = 0 Then
        '                sOffset = CSng(xTag)
        '            End If

        '            '   set format based on int/sng
        '            If InStr(sOffset, xDecimal) Then
        '                xFormat = "#,##0.00"
        '            Else
        '                xFormat = "#,##0"
        '            End If

        '            With Screen.ActiveForm.Controls.Item(ctlP.BuddyControl)
        '                '       get current value of control
        '                sCur = CSng(.Text)
        '                ''       new value is current minus offset, or the nearest lower
        '                ''       major tick, whichever is greater
        '                '        dPrevMajorTick = sCur - (((sCur * 100) Mod (sMajorTick * 100)) / 100)
        '                '        dNew = mpMax(sCur - sOffset, dPrevMajorTick)
        '                dNew = sCur - sOffset
        '                '       ensure that value is >= mininum acceptable value
        '                dNew = mpMax(dNew, ctlP.Min)
        '                .SetFocus()
        '                .Text = Format(dNew, xFormat)
        '            End With
        '        End Sub

        Public Shared Function bIsDigit(ByVal iKey As Integer, ctlP As TextBox, Optional ByVal bAllowDecimal As Boolean = True) As Boolean
            Dim bIsNumeric As Boolean
            Dim bIsDecimal As Boolean
            Dim bIsGeneralFunction As Boolean
            Dim bShiftPressed As Boolean
            Dim xDecimal As String

            '   regional setting for PC may use something
            '   other than period (e.g. comma) as decimal symbol
            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)

            '   test ascii id for integer val
            bIsNumeric = (iKey >= 48) And (iKey <= 57)
            bIsDecimal = (iKey = Asc(xDecimal))
            bShiftPressed = IsPressed(KEY_SHIFT)
            bIsGeneralFunction = (iKey = Keys.Left) Or _
                                 (iKey = Keys.Right) Or _
                                 (iKey = Keys.Home) Or _
                                 (iKey = Keys.End) Or _
                                 (iKey = 1000) Or _
                                 (iKey = Keys.Back)

            If bShiftPressed Then
                bIsDigit = False
            ElseIf (bAllowDecimal And bIsDecimal) Then
                '       allow only 1 decimal place
                bIsDigit = (InStr(ctlP.Text, xDecimal) = 0) Or _
                        ctlP.SelectionLength = Len(ctlP.Text)
            Else
                bIsDigit = bIsNumeric Or _
                           (bAllowDecimal And bIsDecimal) Or _
                           bIsGeneralFunction
            End If

        End Function

        Public Shared Function AllowDigitsOnly(ByVal KeyAscii As Integer, _
                    ctlP As TextBox, _
                    Optional ByVal bAllowDecimal As Boolean = True, _
                    Optional ByVal iMaxDigits As Integer = 100) As Integer
            'Returns 0 if keyAscii is not an acceptable
            'character for a numeric field
            If Not bIsDigit(KeyAscii, ctlP, bAllowDecimal) Then
                KeyAscii = 0
            ElseIf (KeyAscii <> System.Windows.Forms.Keys.Back) And _
                (ctlP.Text.Length = iMaxDigits) And (ctlP.SelectionLength = 0) Then
                'prevent overflow
                KeyAscii = 0
            End If
            AllowDigitsOnly = KeyAscii
        End Function

        '        Public Function bValidatePositionInput(ctlText As VB.TextBox, _
        '                                               Optional sMin As Single = 0) As Boolean
        '            'alerts to invalid numeric input, else formats
        '            'to correct number of significant digits
        '            Dim xFormat As String

        '            With ctlText
        '                If Not bPositionIsValid(.Text, sMin) Then
        '                    On Error Resume Next
        '                    .SetFocus()
        '                    .Text = Format(.Text, "#,##0.00")
        '                Else
        '                    .Text = Format(.Text, "#,##0.00")
        '                    bValidatePositionInput = True
        '                End If
        '            End With
        '        End Function

        Public Shared Function bValidateSpaceInput(ctlText As TextBox) As Boolean
            'alerts to invalid numeric input, else formats
            'to correct number of significant digits
            With ctlText
                If Not bPointsAreValid(.Text) Then
                    On Error Resume Next
                    .Focus()
                    On Error GoTo 0
                    '.Text = Format(.Text, "#,##0")
                Else
                    '.Text = Format(.Text, "#,##0")
                    bValidateSpaceInput = True
                End If
            End With
        End Function

        Public Shared Function bValidateLinesInput(ctlText As TextBox) As Boolean
            'alerts to invalid numeric input, else formats
            'to correct number of significant digits
            With ctlText
                If Not bLinesAreValid(.Text) Then
                    On Error Resume Next
                    .Focus()
                    On Error GoTo 0
                    '.Text = Format(.Text, "#,##0")
                Else
                    '.Text = Format(.Text, "#,##0")
                    bValidateLinesInput = True
                End If
            End With
        End Function

        Public Shared Sub ChangeUpDown(udP As NumericUpDown, _
                         Optional ByVal bUp As Boolean = True, _
                         Optional ByVal sSmall As Single = 1, _
                         Optional ByVal sMed As Single = 10, _
                         Optional ByVal sLarge As Single = 100)
            'changes the value of UpDown ctl by iOffset
            Dim iOffset As Integer
            With udP
                If IsPressed(KEY_CTL) And _
                    IsPressed(KEY_MENU) Then
                    iOffset = sLarge
                ElseIf IsPressed(KEY_CTL) Then
                    iOffset = sMed
                Else
                    iOffset = sSmall
                End If

                If Not bUp Then
                    iOffset = iOffset * -1
                End If

                .Value = mpMin(mpMax(.Value + iOffset, .Minimum), .Maximum)
            End With
        End Sub

        '        Public shared Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
        '            Dim xarP As xArray
        '            Dim iRows As Integer
        '            On Error Resume Next
        '            With tdbCBX
        '                xarP = tdbCBX.Array
        '                iRows = mpMin(xarP.Count(1), CDbl(iMaxRows))
        '                .DropdownHeight = (iRows * .RowHeight)
        '                If Err.Number = 6 Then
        '                    '---use hard value if there's a processor problem -- err 6 is overflow
        '                    .DropdownHeight = (iRows * 239.811)
        '                End If
        '            End With
        '        End Sub

        '        Public Shared Sub CorrectTDBComboMismatch(oTDBCombo As TrueDBList60.TDBCombo, iReposition As Integer)
        '            'resets the tdb combo value to the previous match -
        '            'this procedure should be called in the
        '            'TDBCombo Mismatch even procedure only
        '            Dim bytStart As Byte
        '            Dim oNum As CNumbers

        '            oNum = New CNumbers

        '            iReposition = False

        '            With oTDBCombo
        '                '       get current selection start
        '                bytStart = .SelStart

        '                '       reset the text to the current list text
        '                If .ListField = Empty Then
        '                    .Text = .Columns(0)
        '                Else
        '                    .Text = .Columns(.ListField)
        '                End If

        '                '       return selection to original selection
        '                .SelStart = mpMax(CDbl(bytStart - 1), 0)
        '                .SelLength = Len(.Text)
        '            End With
        '        End Sub


        '        Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
        '            'returns FALSE if invalid author has been specified, else TRUE
        '            With oTDBCombo
        '                If .BoundText = "" Then
        '                    MsgBox("Invalid entry.  Please select an item " & _
        '                           "from the list.", vbExclamation, App.Title)
        '                Else
        '                    '           set text of control equal to the
        '                    '           display text of the selected row
        '                    .Text = .Columns(.ListField)
        '                    bValidateBoundTDBCombo = True
        '                End If
        '            End With
        '        End Function

        '        Function GetTDBListCount(oTDBCombo As TrueDBList60.TDBCombo) As Long
        '            Dim xarP As xArray
        '            Dim iCount As Integer

        '            If Not (oTDBCombo.Array Is Nothing) Then
        '                xarP = oTDBCombo.Array

        '                '   count rows in xarray
        '                iCount = xarP.Count(1)
        '            End If

        '            GetTDBListCount = iCount
        '        End Function

        '        Public Shared Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
        '            'selects the first/next entry in an xarray-based list control whose
        '            'first character is KeyCode
        '            Dim lRows As Long
        '            Dim lCurRow As Long
        '            Dim xarP As xArray
        '            Dim lStartRow As Long
        '            Dim xChr As String

        '            '   get starting row
        '            lStartRow = ctlP.Bookmark

        '            '   start with next row
        '            lCurRow = lStartRow + 1

        '            xarP = ctlP.Array

        '            '   count rows in xarray
        '            lRows = xarP.Count(1)

        '            '   get the char that we're searching for
        '            xChr = UCase(Chr(KeyCode))

        '            '   loop through xarray until match with first letter is found
        '            While (xChr <> UCase(Left(xarP.Value(lCurRow, 1), 1)))
        '                '       if at end of xarray, start from beginning,
        '                '       exit if we've looped through all rows in xarray
        '                If lCurRow = lStartRow Then
        '                    Exit Sub
        '                End If

        '                '       else move to next row
        '                If lCurRow < (lRows - 1) Then
        '                    lCurRow = lCurRow + 1
        '                Else
        '                    lCurRow = 0
        '                End If

        '            End While

        '            '   select the matched item
        '            With ctlP
        '                .Bookmark = lCurRow
        '                .SelBookmarks.Remove 0
        '                .SelBookmarks.Add lCurRow
        '                .SetFocus()
        '            End With
        '        End Sub

        '        Public Shared Sub MatchCompleteInXArrayList(xString As String, ctlP As Control)
        '            'selects the first/next entry in an xarray-based list control whose
        '            'full string is xString
        '            Dim lRows As Long
        '            Dim lCurRow As Long
        '            Dim xarP As xArray
        '            Dim lStartRow As Long
        '            Dim xChrs As String

        '            '   start at the top
        '            lStartRow = 0

        '            '   start with next row
        '            lCurRow = lStartRow + 1

        '            xarP = ctlP.Array

        '            '   count rows in xarray
        '            lRows = xarP.Count(1)

        '            '   get the char that we're searching for
        '            xChrs = UCase(xString)

        '            '   loop through xarray until match with first letter is found
        '            While (xChrs <> UCase(xarP.Value(lCurRow, 0)))
        '                '       if at end of xarray, start from beginning,
        '                '       exit if we've looped through all rows in xarray
        '                If lCurRow = lStartRow Then
        '                    Exit Sub
        '                End If

        '                '       else move to next row
        '                If lCurRow < (lRows - 1) Then
        '                    lCurRow = lCurRow + 1
        '                Else
        '                    lCurRow = 0
        '                End If

        '            End While

        '            '   select the matched item
        '            ctlP.Bookmark = lCurRow

        '        End Sub

        '        Public Shared Sub AddItemToTDBCombo(oCtl As TrueDBList60.TDBCombo, _
        '                                     xItem As String, _
        '                                     Optional iIndex As Integer = -1, _
        '                                     Optional lColValue As Long = -1)

        '            '   Adds new row to TDBCombo list
        '            Dim lRows As Long
        '            Dim xarP As xArray

        '            xarP = oCtl.Array

        '            '   count rows in xarray
        '            lRows = xarP.UpperBound(1)

        '            '   add new row at iIndex
        '            If iIndex >= 0 Then
        '                xarP.Insert(1, iIndex)
        '            Else    'add as last item
        '                xarP.Insert(1, lRows + 1)
        '            End If

        '            '   set DisplayName column
        '            xarP(xarP.UpperBound(1), 0) = xItem

        '            '   set Value is specified
        '            If lColValue >= 0 Then
        '                xarP(xarP.UpperBound(1), 1) = CStr(lColValue)
        '            Else
        '                xarP(xarP.UpperBound(1), 1) = xItem
        '            End If

        '            oCtl.Array = xarP

        '        End Sub

        '        Public Shared Sub RemoveItemFromTDBCombo(oCtl As TrueDBList60.TDBCombo, _
        '                                          iIndex As Integer)

        '            '   removes row at iIndex
        '            Dim xarP As xArray

        '            xarP = oCtl.Array

        '            If Not (xarP Is Nothing) Then
        '                xarP.Delete(1, iIndex)
        '                oCtl.Array = xarP
        '            End If

        '        End Sub
    End Class
End Namespace





