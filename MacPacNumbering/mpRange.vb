Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cWP
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cContentControls
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnConstants

Namespace LMP.Numbering
    Friend Class mpRange
        Public Shared Function rngGetField(rngFieldCode As Word.Range) As Word.Range
            'returns the range of the field
            'whose code is rngFieldCode

            Dim lStartPos As Long
            Dim lEndPos As Long

            With rngFieldCode
                lStartPos = .Start
                lEndPos = .End
            End With

            rngFieldCode.SetRange(lStartPos - 1, lEndPos + 1)
            rngGetField = rngFieldCode
        End Function

        Public Shared Function bUnderLineHeadingToggle() As Boolean
            ' converted from CurWordApp.WordBasic code in MacPac 6/7.
            ' eventually should be rewritten -
            ' partially rewritten 1/11/98 - df

            Dim ShowAllStart As Boolean
            Dim IsUnderlined As Boolean
            Dim CurPosInInches As Single
            Dim bUnderlined As Boolean
            Dim bSelectEOPara As Boolean
            Dim rngLocation As Word.Range
            Dim bIsOneLineHanging As Boolean
            Dim iTabPosition As Integer
            Dim iFirstTabStop As Integer
            Dim iNumTabStops As Integer
            Dim tabExisting As TabStop
            Dim rngStart As Word.Range
            Dim i As Integer
            Dim rngShiftReturn As Word.Range
            Dim iVPos
            Dim xHiddenText As String
            Dim bIsField As Boolean
            Dim oPara As Word.Paragraph
            Dim bShowHidden As Boolean 'GLOG 5205
            Dim bParaAdded As Boolean

            Try
                CurWordApp.ScreenUpdating = False

                'GLOG 5205
                bShowHidden = CurWordApp.ActiveWindow.View.ShowHiddenText
                CurWordApp.ActiveWindow.View.ShowHiddenText = False

                rngStart = CurWordApp.Selection.Range

                'GLOG 5273 - prevent issues at end of content control
                bParaAdded = AddTempParaToContentControl(rngStart)

                With rngStart.Paragraphs(1).Range
                    If rngStart.Start = .End - 1 And _
                        rngStart.End = .End - 1 Then
                        bSelectEOPara = True
                    End If
                End With

                With CurWordApp.WordBasic
                    '   get showall
                    ShowAllStart = .ShowAll()

                    'GLOG 5204 - if in style separator paragraph, move to after style separator
                    CurWordApp.ActiveWindow.View.ShowAll = True
                    If CurWordApp.Selection.Paragraphs(1).IsStyleSeparator Then
                        CurWordApp.Selection.MoveStart(WdUnits.wdParagraph)
                    End If
                    CurWordApp.ActiveWindow.View.ShowAll = False

                    '   test for current underlining -
                    '   remove if it exists
                    .WW7_EditGoTo("\Para")

                    'if centered, remove any hidden text at the end of the paragraph to
                    'avoid errors in bUnderlineToLengthOfLongest(), which needs to be run with
                    'ShowAll=false, and ensure that hard spaces get removed with underlining
                    oPara = CurWordApp.Selection.Paragraphs(1)
                    If CurWordApp.Selection.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter Then _
                        RemoveHiddenEndOfPara(oPara, xHiddenText, bIsField)

                    '   trim trailing para from Selection if necessary
                    If Right(CurWordApp.Selection.Text, 1) = vbCr Or _
                            Asc(Right(CurWordApp.Selection.Text, 1)) = 7 Then '*cell marker
                        .CharLeft(1, 1)
                    End If

                    '   select last line
                    .CharRight()
                    .StartOfLine(1)

                    '   check for one-line hanging indent (e.g. reline)
                    With CurWordApp.Selection
                        If .Characters.Last.Text <> vbTab And _
                                .ParagraphFormat.FirstLineIndent < 0 And _
                                iTabPosition > 0 And Left(.Text, 1) = vbTab Then
                            bIsOneLineHanging = True
                            .MoveStart(WdUnits.wdCharacter, iTabPosition)
                        End If
                    End With

                    '   remove underlining, hard spaces,
                    '   trailing tab and last tab marker'  if they exist - then exit
                    IsUnderlined = CurWordApp.Selection.Range.Underline
                    If IsUnderlined Then
                        bEditFindReset()
                        .EditReplace(Find:="^s", Replace:="", ReplaceAll:=1)
                        .EndOfLine()
                        .CharLeft(1, 1)
                        If CurWordApp.Selection.Text = vbTab Then
                            .WW6_EditClear()
                            CurPosInInches = (.SelInfo(7) / 1440)
                            CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)

                            While Right(CurWordApp.Selection.Text, 1) = vbTab
                                CurWordApp.Selection.Delete()
                                CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)
                            End While

                            '--             clear last tab the VBA way
                            With CurWordApp.Selection.ParagraphFormat
                                For i = .TabStops.Count To 1 Step -1
                                    With .TabStops(i)
                                        If .CustomTab And .Alignment = WdTabAlignment.wdAlignTabRight Then
                                            .Clear()
                                            Exit For
                                        End If
                                    End With
                                Next i

                                '                   check for numbering - if trailing tab
                                '                   for number, skip first tab stop
                                iFirstTabStop = 1
                                With CurWordApp.Selection.Range.ListFormat
                                    If .ListType <> WdListType.wdListNoNumbering Then
                                        'GLOG 5205 - prevent error when there's a list num in
                                        'non-numbered paragraph
                                        If Not .ListTemplate Is Nothing Then
                                            If .ListTemplate.ListLevels(.ListLevelNumber) _
                                                    .TrailingCharacter = WdTrailingCharacter.wdTrailingTab Then
                                                iFirstTabStop = 2
                                            End If
                                        End If
                                    End If
                                End With

                                iNumTabStops = .TabStops.Count
                                'On Error Resume Next
                                Dim bClear As Boolean = False
                                For i = iNumTabStops To iFirstTabStop Step -1
                                    Try
                                        bClear = ((.TabStops(i).Position Mod 36 = 0) And _
                                                (.TabStops(i).Position <> .LeftIndent))
                                    Catch
                                    End Try
                                    If bClear Then
                                        Try
                                            .TabStops(i).Clear()
                                        Catch
                                        End Try
                                    End If
                                Next i
                                'On Error GoTo 0
                            End With
                        End If

                        'remove shift-return added by us to preserve correct wrapping
                        'in Asian/complex script environments, which is identifiable by
                        'the space preceding it, ensuring that this has no effect on
                        'how the text wraps
                        With CurWordApp.Selection
                            rngLocation = .Range
                            .Expand(WdUnits.wdParagraph)
                            .MoveEnd(WdUnits.wdCharacter, -1)
                            .Collapse(WdCollapseDirection.wdCollapseEnd)
                            .MoveUp(WdUnits.wdLine)
                            .Expand(WdUnits.wdLine)
                            With .Characters.Last
                                If .Text = vbVerticalTab Then
                                    If .Previous(WdUnits.wdCharacter).Text = " " Then
                                        iVPos = .Next(WdUnits.wdCharacter) _
                                            .Information(wdInformation.wdVerticalPositionRelativeToPage)
                                        .Delete()
                                        If .Next(WdUnits.wdCharacter) _
                                            .Information(wdInformation.wdVerticalPositionRelativeToPage) < iVPos Then
                                            'next character has moved up - restore the shift-return
                                            .InsertAfter(vbVerticalTab)
                                        End If
                                    End If
                                End If
                            End With
                        End With

                        '2/10/09 - restore Selection - we were previously removing underlining
                        'from the previous paragraph when the target paragraph was just one line
                        rngLocation.Select()

                        'remove underline
                        CurWordApp.Selection.Paragraphs(1).Range.Underline = wdUnderline.wdUnderlineNone
                    Else
                        '*c
                        If CurWordApp.Selection.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight And _
                                CurWordApp.Selection.Information(wdinformation.wdWithInTable) Then
                            CurWordApp.Selection.Font.Underline = wdUnderline.wdUnderlineSingle
                        Else
                            If bIsOneLineHanging Then
                                rngLocation = CurWordApp.Selection.Range
                            Else
                                rngLocation = CurWordApp.Selection.Paragraphs(1).Range
                                rngLocation.MoveEnd(WdUnits.wdCharacter, -1)
                            End If

                            bUnderlined = bUnderlineToLengthOfLongest(rngLocation)
                        End If
                    End If


                    'restore hidden text
                    If xHiddenText <> "" Then _
                        RestoreHiddenEndOfPara(oPara, xHiddenText, bIsField)

                    '   ensure insertion point is at end of paragraph
                    .WW7_EditGoTo("\Para")

                    '   trim trailing para from selection if necessary
                    If Right(CurWordApp.Selection.Text, 1) = vbCr Then _
                        .CharLeft(1, 1)
                    .CharRight()
                    .Underline(0)

                    '   set environment
                    .ShowAll(ShowAllStart)
                    bEditFindReset()

                    'GLOG 5205 - restore hidden text display
                    CurWordApp.ActiveWindow.View.ShowHiddenText = bShowHidden

                    '       redefine selection range if eo para
                    '       should be selected - see top
                    If bSelectEOPara Then
                        rngStart = rngStart.Paragraphs(1).Range
                        If rngStart.Characters.Last.Text = vbCr Then _
                            rngStart.MoveEnd(WdUnits.wdCharacter, -1)
                        rngStart.EndOf()
                    End If

                    'GLOG 5273 - delete temp paragraph
                    If bParaAdded Then
                        If (rngStart.Paragraphs.Count > 1) And (rngStart.Characters.Last.Text = vbCr) Then
                            rngStart.Paragraphs.Last.Range.Delete()
                        Else
                            rngStart.Next(WdUnits.wdParagraph).Delete()
                        End If
                    End If

                    rngStart.Select()
                End With

            Finally
                CurWordApp.ScreenUpdating = True
            End Try
        End Function

        Public Shared Function bUnderlineToLengthOfLongest(rngLocation As Word.Range, _
                                             Optional sDefaultTabStop As Single = 36) As Boolean

            Const mpHPos = wdInformation.wdHorizontalPositionRelativeToTextBoundary
            Const mpVPos = wdInformation.wdVerticalPositionRelativeToPage

            Dim iPosition
            Dim iLongestLine
            Dim iLeftEdge
            Dim iRightEdge
            Dim iTabStops As Integer
            Dim sRightIndentPos As Single
            Dim lTabPos As Long
            Dim iNumTabs As Integer
            Dim bIsHangingIndent As Boolean
            Dim i As Integer
            Dim tsExisting As Word.TabStop
            Dim iListLevel As Integer
            Dim rngLineEnd As Word.Range
            Dim iVPosition
            Dim iVPrevCharPos
            Dim rngTest As Word.Range
            Dim iLeftSpaces As Integer
            Dim iRightSpaces As Integer
            Dim iCompatibilityMode As Integer

            CurWordApp.ScreenUpdating = False

            CurWordApp.ActiveWindow.View.ShowAll = False

            rngLocation.Select()
            With CurWordApp.Selection
                lTabPos = InStr(.Text, vbTab)
                iNumTabs = lCountChrs(.Text, vbTab)
                .Collapse(WdCollapseDirection.wdCollapseStart)
                .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                If .Text = vbCr Then
                    If lTabPos And .ParagraphFormat.FirstLineIndent < 0 Then
                        rngLocation.MoveStart(wdUnits.wdCharacter, lTabPos)
                    End If
                    rngLocation.Underline = wdUnderline.wdUnderlineSingle
                    Exit Function
                End If

                If .ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter Then
                    iRightEdge = .Information(mpHPos)
                    .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                    iLeftEdge = .Information(mpHPos)
                    .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                    .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    While CurWordApp.Selection.InRange(rngLocation)
                        iPosition = .Information(mpHPos)
                        If iPosition > iRightEdge Then _
                            iRightEdge = iPosition
                        .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                        iPosition = .Information(mpHPos)
                        If iPosition < iLeftEdge Then _
                            iLeftEdge = iPosition
                        .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    End While
                    .HomeKey(WdUnits.wdLine, WdMovementType.wdExtend)
                    rngLineEnd = CurWordApp.ActiveDocument.Range(.End, .End)

                    If .Next(WdUnits.wdCharacter).Information(mpHPos) = iRightEdge Then
                        '               lines are the same length - add no spaces
                        GoTo skipHardSpaces
                    End If

                    '           get the vertical position of the last character on the second to
                    '           last line - we'll use this to test whether hard spaces are
                    '           affecting the way the paragraph wraps
                    rngTest = .Previous(WdUnits.wdCharacter).Previous(WdUnits.wdCharacter)
                    iVPrevCharPos = rngTest.Information(mpVPos)

                    iVPosition = .Information(mpVPos)
                    .Collapse(WdCollapseDirection.wdCollapseEnd)
                    While (.Information(mpHPos) < iRightEdge) And _
                            (.Information(mpVPos) = iVPosition)
                        'add space to end
                        .InsertAfter(ChrW(&HA0))
                        .Collapse(WdCollapseDirection.wdCollapseEnd)
                        If .Information(mpVPos) = iVPosition Then
                            'still on same line - add space to start
                            .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                            .InsertAfter(ChrW(&HA0))
                            If rngTest.Information(mpVPos) > iVPrevCharPos Then
                                'if the document is set to support complex scripts,
                                'a hard space added at the start of a line will attach
                                'to the last word on the previous line, causing that word
                                'to move down to the start of the current line - add a
                                'shift-return to restore the correct wrap
                                .Collapse(WdCollapseDirection.wdCollapseEnd)
                                .Previous(WdUnits.wdCharacter).InsertBefore(vbVerticalTab)
                            End If
                            .Expand(WdUnits.wdParagraph)
                            .MoveEnd(WdUnits.wdCharacter, -1)
                            .Collapse(WdCollapseDirection.wdCollapseEnd)
                        End If
                    End While

                    If .Information(mpVPos) > iVPosition Then
                        '               last line wrapped - delete responsible spaces
                        .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                        While .Characters(1).Text = ChrW(&HA0)
                            .Delete()
                        End While
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        While .Information(mpVPos) > iVPosition
                            .Move(WdUnits.wdCharacter, -1)
                            If .Characters(1).Text = ChrW(&HA0) Then _
                                .Delete()
                            .Expand(WdUnits.wdParagraph)
                            .MoveEnd(WdUnits.wdCharacter, -1)
                            .Collapse(WdCollapseDirection.wdCollapseEnd)
                        End While
                    End If

                    'ensure that there are the same number of spaces on both sides
                    .Expand(WdUnits.wdParagraph)
                    .MoveEnd(WdUnits.wdCharacter, -1)
                    .Collapse(WdCollapseDirection.wdCollapseEnd)

                    'measure right-hand side
                    .MoveStartWhile(CStr(ChrW(&HA0)), WdConstants.wdBackward)
                    If .Characters(1).Text = ChrW(&HA0) Then _
                        iRightSpaces = Len(.Text)

                    'measure left-hand side
                    .Collapse(WdCollapseDirection.wdCollapseEnd)
                    .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                    .MoveEndWhile(CStr(ChrW(&HA0)))
                    If .Characters(1).Text = ChrW(&HA0) Then _
                        iLeftSpaces = Len(.Text)

                    'delete left space if necessary
                    If iLeftSpaces > iRightSpaces Then _
                        .Characters.First.Delete()

                    .Expand(WdUnits.wdParagraph)
                    .MoveEnd(WdUnits.wdCharacter, -1)
                    .Collapse(WdCollapseDirection.wdCollapseEnd)

                    'delete right space if necessary
                    If iRightSpaces > iLeftSpaces Then _
                        .Previous(WdUnits.wdCharacter).Delete()
skipHardSpaces:
                    i = i
                Else
                    '*c
                    iLongestLine = .Information(mpHPos)
                    If Str(Asc(CurWordApp.Selection.Text)) <> 13 Then
                        .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    End If
                    While CurWordApp.Selection.InRange(rngLocation)
                        .MoveWhile(CStr(Chr(32)), WdConstants.wdBackward)
                        iPosition = .Information(mpHPos)
                        If iPosition > iLongestLine Then _
                            iLongestLine = iPosition
                        .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    End While
                    iPosition = .Information(mpHPos)

                    If iPosition < iLongestLine Then
                        '               add tab stops every x points if hanging indent
                        '               and >1 tab or not hanging and > 0 tabs.
                        With .ParagraphFormat
                            bIsHangingIndent = (.FirstLineIndent < .LeftIndent)
                            If bIsHangingIndent Then
                                '                       added 6/6/01 - ensure expected tab stop at left indent
                                .TabStops.Add(.LeftIndent())
                            End If
                            With rngLocation
                                'GLOG 5205 - tab count was inaccurate in partner paragraph and
                                'list format code was erring when the paragraph contained list nums -
                                'adjust the range to target style separator paragraph
                                Dim bShowAll As Boolean
                                bShowAll = CurWordApp.ActiveWindow.View.ShowAll
                                CurWordApp.ActiveWindow.View.ShowAll = True
                                If .Paragraphs(1).IsStyleSeparator Then
                                    .SetRange(.Paragraphs(1).Range.Start, .Paragraphs(1).Range.End)
                                End If
                                CurWordApp.ActiveWindow.View.ShowAll = bShowAll

                                iNumTabs = lCountChrs(.Text, vbTab)
                                With .ListFormat
                                    If .ListType <> WdListType.wdListNoNumbering Then
                                        iListLevel = .ListLevelNumber
                                        If .ListTemplate.ListLevels(iListLevel).TrailingCharacter = WdTrailingCharacter.wdTrailingTab Then
                                            iNumTabs = iNumTabs + 1
                                        End If
                                    End If
                                End With
                            End With

                            If bIsHangingIndent + iNumTabs Then
                                '                       get position of 1st custom tab stop,
                                '                       then specify next half -inch
                                For Each tsExisting In .TabStops
                                    If tsExisting.CustomTab Then
                                        i = tsExisting.Position
                                        '                               specify next half inch
                                        i = i + (36 - (i Mod 36))
                                        Exit For
                                    End If
                                Next tsExisting

                                'GLOG 8592 (dm)
                                If i = 0 Then _
                                    i = sDefaultTabStop

                                While i <= iLongestLine
                                    .TabStops.Add(i)
                                    i = i + sDefaultTabStop
                                End While
                            End If
                        End With
                        With .ParagraphFormat.TabStops
                            .Add(iLongestLine, WdTabAlignment.wdAlignTabRight)
                        End With

                        'GLOG 15771 (dm) - Word 2013/2016 compatibility mode does not allow tabbing past the right margin -
                        'account for this in the conditional below to avoid an infinite loop in justified paragraphs
                        iCompatibilityMode = CurWordApp.ActiveDocument.CompatibilityMode
                        With CurWordApp.ActiveDocument.Sections(.Sections.First.Index).PageSetup
                            sRightIndentPos = .PageWidth - (.LeftMargin + .RightMargin)
                        End With

                        While (.Information(mpHPos) < Int(iLongestLine)) And _
                            ((iCompatibilityMode < 15) Or (.Information(mpHPos) < sRightIndentPos))
                            .InsertAfter(vbTab)
                            .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        End While
                    End If
                End If
                .HomeKey(WdUnits.wdLine, WdMovementType.wdExtend)
                .Font.Underline = wdUnderline.wdUnderlineSingle
            End With
        End Function

        Private Shared Sub RemoveHiddenEndOfPara(ByVal oPara As Word.Paragraph, _
                                          ByRef xHiddenText As String, _
                                          ByRef bIsField As Boolean)
            Dim rngLocation As Word.Range
            Dim bContinue As Boolean
            Dim oResult As Word.Range

            CurWordApp.ActiveWindow.View.ShowAll = True

            rngLocation = oPara.Range
            With rngLocation
                If .Text = vbCr Then Exit Sub
                .EndOf()
                If .Paragraphs(1).Range.Start <> oPara.Range.Start Then _
                    .Move(wdUnits.wdCharacter, -1)
                .MoveStart(wdUnits.wdCharacter, -1)
                If .Fields.Count = 1 Then
                    'there's a field code at end of para - remove only if
                    'it has no result, i.e. it's hidden when ShowAll is off
                    On Error Resume Next
                    oResult = .Fields(1).Result
                    On Error GoTo 0
                    If oResult Is Nothing Then
                        bIsField = True
                        xHiddenText = .Fields(1).Code.Text
                        .Fields(1).Delete()
                    End If
                ElseIf .Font.Hidden Then
                    'there's hidden text at the end of the para
                    bIsField = False
                    bContinue = True
                    While bContinue
                        .MoveStart(wdUnits.wdCharacter, -1)
                        If .Start <> oPara.Range.Start Then
                            bContinue = .Previous(wdUnits.wdCharacter).Font.Hidden
                        Else
                            bContinue = False
                        End If
                    End While
                    xHiddenText = .Text
                    .Text = ""
                Else
                    Exit Sub
                End If
            End With
        End Sub

        Private Shared Sub RestoreHiddenEndOfPara(ByVal oPara As Word.Paragraph, _
                                           ByVal xHiddenText As String, _
                                           ByVal bIsField As Boolean)
            Dim rngLocation As Word.Range
            Dim oField As Word.Field

            If xHiddenText = "" Then Exit Sub

            rngLocation = oPara.Range
            With rngLocation
                .EndOf()
                If .Paragraphs(1).Range.Start <> oPara.Range.Start Then _
                    .Move(wdUnits.wdCharacter, -1)
                If bIsField Then
                    'restore field code
                    oField = .Fields.Add(rngLocation, , xHiddenText, False)

                    'trim extra spaces
                    With oField.Code
                        If .Characters.First.Text = " " Then _
                            .Characters.First.Delete()
                        If .Characters.Last.Text = " " Then _
                            .Characters.Last.Delete()
                    End With
                Else
                    'restore hidden text
                    .InsertAfter(xHiddenText)
                    .Font.Hidden = 1
                End If
            End With
        End Sub
    End Class
End Namespace

