Option Explicit On

Imports LMP.Numbering.Base.cSchemeRecords

Namespace LMP.Numbering
    Friend Class mpTypes
        'defines a scheme record -
        'held as doc var
        Structure mpScheme
            Shared Name As String
            Shared DisplayName As String
            Shared Type As mpSchemeTypes
            Shared Origin As mpSchemeTypes
            Shared TOCScheme As String
        End Structure

        Structure mpDocEnvironment
            Public bShowAll As Boolean
            Public sVScroll As Single
            Public sHScroll As Single
            Public iView As Integer
            Public bFieldCodes As Boolean
            Public bBookmarks As Boolean
            Public bTabs As Boolean
            Public bSpaces As Boolean
            Public bHyphens As Boolean
            Public bHiddenText As Boolean
            Public bParagraphs As Boolean
            Public bTextBoundaries As Boolean
            Public iProtectionType As Integer
            Public iSelectionStory As Integer
            Public lSelectionStartPos As Long
            Public lSelectionEndPos As Long
        End Structure

        Structure mpAppEnvironment
            Shared lBrowserTarget As Long
        End Structure
    End Class
End Namespace


