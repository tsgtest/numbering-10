Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.MacPac
Imports MacPacNumbering.LMP.Numbering.mpTypes
Imports LMP.Numbering.Base

Namespace LMP.Numbering
    Friend Class mpVariables
        'global environment vars
        Public Shared g_oWord As Application
        Public Shared g_envMpApp As mpAppEnvironment
        Public Shared g_envMpDoc As mpDocEnvironment
        Public Shared g_bPreviouslyInitialized As Boolean
        Public Shared g_bPreventDocChangeEvent As Boolean
        Public Shared g_dlgScheme As System.Windows.Forms.Form
        Public Shared g_bIsAdmin As Boolean
        Public Shared g_sStyleArea As Single
        Public Shared g_bLeaveDocDirty As Boolean
        Public Shared g_bAdminIsDirty As Boolean
        Public Shared g_bForceWindowsInTaskbar As Boolean

        'dialogs/shift-tabbing
        'Dialog
        Public Shared g_bInVBALostFocus As Boolean
        Public Shared g_ActivatingTabProgrammatically As Boolean
        Public Shared g_VShiftDown As Boolean
        Public Shared g_bTabbed As Boolean
        Public Shared g_xSelNode As String
        Public Shared g_oStatus As ProgressForm

        'Options
        Public Shared g_bAllowSchemeSharing As Boolean
        Public Shared g_bAllowSchemeEdit As Boolean
        Public Shared g_bAllowSchemeNew As Boolean
        Public Shared g_bAllowPublicSchemes As Boolean
        Public Shared g_bAllowTOCLink As Boolean
        Public Shared g_bShowPersonalSchemes As Boolean
        Public Shared g_bAllowFullHeadingStyleUse As Boolean
        Public Shared g_bDefaultToNumContinue As Boolean
        Public Shared g_iUpgradeFrom As Integer
        Public Shared g_xRemoveNumberStyle As String
        Public Shared g_bResetFonts As Boolean
        Public Shared g_bApplyHeadingColor As Boolean
        Public Shared g_bDocumentChange As Boolean
        Public Shared g_xListNum As String
        Public Shared g_bAcceptAllFlags As Boolean
        Public Shared g_bCodeForMSBug As Boolean
        Public Shared g_bPost97AdminWarning As Boolean
        Public Shared g_bPreserveRestarts As Boolean
        Public Shared g_bGenerateBufferFromCopy As Boolean
        Public Shared g_lActiveSchemeIcon As Long
        Public Shared g_bIsMP10DesignMode As Boolean
        Public Shared g_bIsMP10 As Boolean
        Public Shared g_xBufferTemplate As String

        'paths
        Public Shared g_xStartPath As String
        Public Shared g_xWorkgroupPath As String
        Public Shared g_xUserPath As String
        Public Shared g_xAdminSchemesPath As String
        Public Shared g_xMPBPath As String
        Public Shared g_xHelpPath As String

        'files
        Public Shared g_xBP As String
        Public Shared g_xUserIni As String
        Public Shared g_xFirmIni As String
        Public Shared g_xPNumSty As String
        Public Shared g_xFNumSty As String
        Public Shared g_xFNum80Sty As String
        Public Shared g_docPreview As Word.Document
        Public Shared g_docStart As Word.Document
        Public Shared g_xMPNDot As String
        Public Shared g_xMPNAdminDot As String
        Public Shared g_xMPNConvertDot As String
        Public Shared g_oPNumSty As Word.Template
        Public Shared g_oFNumSty As Word.Template
        Public Shared g_xFirmXMLConfig As String
        Public Shared g_xUserXMLConfig As String

        'misc
        Public Shared bRet As Object
        Public Shared lRet As Long
        Public Shared xMsg As String
        Public Shared iUserChoice As Integer
        Public Shared xFirmName As String
        Public Shared datTestStartTime As Date
        Public Shared datTestEndTime As Date
        Public Shared xRestart As Object
        Public Shared bfrmInitializing As Boolean
        Public Shared bPleadingDBoxActive As Boolean
        Public Shared g_xPreviewLevelText As String

        'Misc
        Public Shared g_xBasedOn As String
        Public Shared g_xPSchemes(,) As String
        Public Shared g_xFSchemes(,) As String
        Public Shared g_xDSchemes(,) As String
        Public Shared g_xNextParaStyles() As String
        Public Shared g_xUnderlineFormats(,) As String
        Public Shared g_xPermittedFonts() As String
        Public Shared g_oCurScheme As CNumScheme
        Public Shared g_xCurDest As String
        Public Shared g_bCurSchemeIsHeading As Boolean
        Public Shared g_sNumberFontSizes() As Single
        Public Shared g_iDMS As Integer
        Public Shared g_xMP90Dir As String
        Public Shared g_xSyncContStyles() As String
        Public Shared g_bIsXP As Boolean
        Public Shared g_xPleadingSchemes() As String
        Public Shared g_bAlwaysAdjustSpacing As Boolean
        Public Shared g_iWordVersion As MacPacNumbering.LMP.Numbering.mpnConstants.mpWordVersions
        Public Shared g_xSupplementalNumberStyles() As String
        Public Shared g_bSchemeSelectionOnly As Boolean
        Public Shared g_bXMLSupport As Boolean
        Public Shared g_bRefreshTaskPane As Boolean
        Public Shared g_lUILanguage As WdLanguageID
        Public Shared g_bOrganizerSavePrompt As Boolean
        Public Shared g_bPreserveUndoList As Boolean
        Public Shared g_bCreateUnlinkedStyles As Boolean
        Public Shared g_bPromptBeforeAutoRelinking As Boolean
        Public Shared g_xTimerDelay As String
        Public Shared g_lWinMajorVersion As Long
        Public Shared g_lWinMinorVersion As Long
        Public Shared g_xAppName As String
        Public Shared g_lTrackChangesWarningThreshold As Long
        Public Shared g_xFavoriteSchemes(,) As String
        Public Shared g_iLoadContStyles As MacPacNumbering.LMP.Numbering.mpnConstants.mpLoadContStyles '9.9.6004
        Public Shared g_lRulerDisplayDelay_New As Long '9.9.6012
        Public Shared g_lRulerDisplayDelay_Edit As Long '9.9.6012
        Public Shared g_lRulerDisplayDelay_EditDirect As Long '9.9.6012
        Public Shared g_lDefaultZoomPercentage As Long '9.9.6012
        Public Shared g_bCollapseRibbon As Boolean '9.9.6014
        Public Shared g_bNoSchemesDlgRescaling As Boolean '10.0.12
        Public Shared g_bDisplayContStylesInNextParaList As Boolean '10.0.27
        Public Shared g_bUseWordHeadingStyles As Boolean '10.0.27

        'Forms
        Public Shared g_bSchemeIsDirty As Boolean
        Public Shared g_iSchemeEditMode As Integer

        'TOC
        Public Shared g_xTCPrefix As String
    End Class
End Namespace

