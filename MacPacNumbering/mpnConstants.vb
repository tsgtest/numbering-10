Option Explicit On

Imports System.Runtime.InteropServices

Namespace LMP.Numbering
    Friend Class mpnConstants
        Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long

        <DllImport("User32.dll")>
        Friend Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
        End Function


        'numbering type - this is the switch to flip
        'for using Native Word v. LISTNUM numbering
        Public Const mpUseListNum As Boolean = False

        'schemes
        Public Const mpSchemeTypeTCCodeBased As Integer = 1
        Public Const mpSchemeTypeStyleBased As Integer = 2

        Public Const mpNumberingToolbar As String = "TSG Numbering"
        Public Const mpNumberingMenu As String = "Numbering"
        Public Const mpGalleryItem As String = "Gallery..."
        Public Const mpActiveSchemeDocVar As String = "zzmpFixedCurScheme_9.0"
        Public Const mpActiveScheme80DocVar As String = "zzmpFixedCurScheme"
        Public Const mpLastEditMSWordVer As String = "zzmpLastEditMSWordVer"
        Public Const mpLTFontsClean As String = "zzmpLTFontsClean"
        Public Const mpDocumentSchemes As String = "Document Schemes"
        Public Const mpPublicSchemes As String = "Public Schemes"
        Public Const mpPersonalSchemes As String = "Private Schemes"
        Public Const mpSharedSchemes As String = "Shared Schemes"
        Public Const mpAdminSchemes As String = "Public Schemes"
        Public Const mpFavoriteSchemes As String = "Favorite Schemes"
        Public Const mpOrganizerSavePrompt = "This macro is not available in an unsaved document.  Please save your document and run this macro again." & vbCr & vbCr & "We apologize for the inconvenience but this is a temporary workaround for a Microsoft bug that affects copying styles via code using Word's Organizer feature.  Microsoft's hotfix should be available within a couple months."
        Public Const mpOrganizerSavePromptFrench = "Cette macro n'est pas disponible dans un document non sauf-gard�.  Veuillez sauf-garder votre document avant d�ex�cuter cette macro." & vbCr & vbCr & "Nous vous prions de nous excuser pour tout inconv�nient mais c'est un workaround provisoire pour un bogue de Microsoft.  Le bogue a �t� introduit r�cemment dans Word 2007 SP2 et affecte la copie des styles par l'interm�diaire du code utilisant la fonction de l�Organisateur de Word.  Le correctif de Microsoft devrait �tre disponible dans les deux prochains mois."
        Public Const mpDocumentSchemesFrench As String = "Th�mes de document"
        Public Const mpPublicSchemesFrench As String = "Th�mes publics"
        Public Const mpPersonalSchemesFrench As String = "Th�mes priv�s"
        Public Const mpSharedSchemesFrench As String = "Th�mes partag�s"
        Public Const mpAdminSchemesFrench As String = "Th�mes publics"
        Public Const mpFavoriteSchemesFrench As String = "Th�mes favoris"

        'numbering toolbar control indices - you can
        'avoid this by tagging the controls, but it
        'seems clearer this way.
        Public Const mpCtlSchemes As Integer = 1
        Public Const mpCtlLevel1 As Integer = 2
        Public Const mpCtlLevel2 As Integer = 3
        Public Const mpCtlLevel3 As Integer = 4
        Public Const mpCtlLevel4 As Integer = 5
        Public Const mpCtlLevel5 As Integer = 6
        Public Const mpCtlLevel6 As Integer = 7
        Public Const mpCtlLevel7 As Integer = 8
        Public Const mpCtlLevel8 As Integer = 9
        Public Const mpCtlLevel9 As Integer = 10
        Public Const mpCtlPromote As Integer = 11
        Public Const mpCtlDemote As Integer = 12
        Public Const mpCtlRemove As Integer = 13
        Public Const mpCtlRestart As Integer = 14
        Public Const mpCtlContinue As Integer = 15
        Public Const mpCtlAutoFormat As Integer = 16
        Public Const mpCtlMarkFormatForTOC As Integer = 17
        Public Const mpCtlMarkForTOC As Integer = 18
        Public Const mpCtlUnMarkForTOC As Integer = 19
        Public Const mpCtlMarkFormatAll As Integer = 20
        Public Const mpCtlTOC As Integer = 21

        'numbering scheme items
        Public Const mpWordHeading As String = "Heading"
        Public Const mpPleadingHeading1Item As String = "Pleading1"
        Public Const mpPleadingHeading2Item As String = "Pleading2"
        Public Const mpDefaultScheme As String = "Standard"

        'LISTNUM
        Public Const mpListNumNoStartAt As Integer = -1

        'misc
        Public Const mpContinueFromPrevious As Integer = -1
        Public Const mpRemoveTOCMarking As Integer = -1
        Public Const mpTextCode As String = "$"
        Public Const mpFunctionInsertNumber As Integer = 1
        Public Const mpFunctionChangeScheme As Integer = 2
        Public Const mpMaxPositionalValue As Single = 8
        Public Const mpFilePrefixSep As String = "@@_"
        Public Const mpHSchemeDocVar As String = "HeadingScheme"
        Public Const mpPrefix As String = "zzmp"
        Public Const mpTag As String = "|ZZMPTAG|"
        Public Const mpBaseAlignmentOnNormal As Integer = 4

        'styles
        Public Const mpDefaultNonNumberedStyleGeneral = "Body Text"
        Public Const mpDefaultNonNumberedStylePleading = "Body Text"

        'correspond to number style dropdown
        Public Const mpListNumberStyleNone As Integer = 0
        Public Const mpListNumberStyleArabic As Integer = 1
        Public Const mpListNumberStyleUppercaseRoman As Integer = 2
        Public Const mpListNumberStyleLowercaseRoman As Integer = 3
        Public Const mpListNumberStyleUppercaseLetter As Integer = 4
        Public Const mpListNumberStyleLowercaseLetter As Integer = 5
        Public Const mpListNumberStyleOrdinal As Integer = 6
        Public Const mpListNumberStyleCardinalText As Integer = 7
        Public Const mpListNumberStyleOrdinalText As Integer = 8
        Public Const mpListNumberStyleArabicLZ As Integer = 9
        Public Const mpListNumberStyleArabicLZ2 As Integer = 10
        Public Const mpListNumberStyleArabicLZ3 As Integer = 11
        Public Const mpListNumberStyleArabicLZ4 As Integer = 12
        Public Const mpListNumberStyleBullet As Integer = 13
        Public Const mpListNumberStyleNewBullet As Integer = 14
        Public Const mpListNumberStyleLegal As Integer = 15
        Public Const mpListNumberStyleLegalLZ As Integer = 16

        'doc properties "table"
        Public Const mpDPSchemeName As Integer = 1
        Public Const mpDPSchemeAlias As Integer = 2
        Public Const mpDPSchemeOrigin As Integer = 3

        'these are used with xIsDirty xArray object
        Public Const mpLevelIsDirty As Integer = 1
        Public Const mpTCIsDirty As Integer = 2
        Public Const mpTOCIsDirty As Integer = 3
        Public Const mpDocPropIsDirty As Integer = 4
        Public Const mpParaIsDirty As Integer = 5
        Public Const mpNextParaIsDirty As Integer = 6

        'messages
        Public Const mpMsgUpgradeTo2000 As String = "Upgrading MacPac Numbering for Office 2000.  Please wait..."

        'development flag
        Public Const mpDeveloperEnvironment As Boolean = False

        'DMS
        Public Const mpDMSIManage5x As Integer = 3
        Public Const mpDMSNetDocs As Integer = 6

        'Flags
        Public Const mpFlagMSIndentsBug970 As String = "*"
        Public Const mpFlagMSIndentsBug973 As String = "z"

        'Word versions
        Public Enum mpWordVersions
            mpWordVersion_97 = 8
            mpWordVersion_2000 = 9
            mpWordVersion_XP = 10
            mpWordVersion_2003 = 11
            mpWordVersion_2007 = 12
            mpWordVersion_2010 = 14
            mpWordVersion_2013 = 15
        End Enum

        'Cont styles (9.9.6004)
        Public Enum mpLoadContStyles
            mpLoadWithScheme = 0
            mpLoadAllOnDemand = 1
            mpLoadIndividuallyOnDemand = 2
        End Enum

        'Favorites
        Public Const mpFavoritePublic As Integer = 102
        Public Const mpFavoritePrivate As Integer = 103
    End Class
End Namespace


