Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.Base
Imports LMP.Numbering.Base.cNumbers
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cWP
Imports LMP.Numbering.Base.cWordXML
Imports LMP.Numbering.Base.cContentControls
Imports MacPacNumbering.LMP.Numbering.mpVariables
Imports MacPacNumbering.LMP.Numbering.mpnConstants
Imports MacPacNumbering.LMP.Numbering.mpApplication
Imports MacPacNumbering.LMP.Numbering.mdlN90
Imports MacPacNumbering.LMP.Numbering.mdlConversions
Imports MacPacNumbering.LMP.Numbering.mpRange
Imports System.Math

Namespace LMP.Numbering
    Friend Class mpnN80
        Public Enum mpNumberingSelectionTypes
            mpNumberingSelectionType_InsertionPoint = 0
            mpNumberingSelectionType_PartialParagraph = 1
            mpNumberingSelectionType_FullParagraph = 2
        End Enum

        Public Shared Function lInitializeNumbering() As Long
            Dim i As Integer
            Dim bTest As Boolean
            Dim oAddIn As Word.AddIn

            '   get version
            With CurWordApp
                If InStr(.Version, "14.") <> 0 Then
                    g_iWordVersion = mpWordVersions.mpWordVersion_2010
                Else
                    g_iWordVersion = mpWordVersions.mpWordVersion_2013
                End If
            End With

            '   determine whether mp10 is loaded
            'GLOG 5551 (9.9.6006) - also check whether COM addin is loaded
            For Each oAddIn In CurWordApp.AddIns
                If UCase$(oAddIn.Name) = "MP10.DOTM" Then
                    g_bIsMP10 = COMAddInIsLoaded("MacPac102007")
                    Exit For
                ElseIf UCase$(oAddIn.Name) = "FORTE.DOTM" Then
                    g_bIsMP10 = COMAddInIsLoaded("ForteAddIn")
                    Exit For
                End If
            Next oAddIn

            '   determine whether xml is supported
            On Error Resume Next
            bTest = CursorIsAtStartOfBlockTag()
            g_bXMLSupport = (Err.Number <> 6140)
            On Error GoTo 0

            '   get text for level description in preview
            g_xPreviewLevelText = GetUserSetting("Preview", "LevelText")
            If Len(g_xPreviewLevelText) = 0 Then
                g_xPreviewLevelText = "Level "
            End If

            '   get app options
            GetAppOptions()
lab_SkipTOCLink:

            '   get tc prefix
            g_xTCPrefix = GetAppSetting("TOC", "TCPrefix")
            If g_xTCPrefix = "" Then g_xTCPrefix = "TC"

            '   force autoformat options off -
            '   these cause unexpected results with numbering-
            '   user can later turn on
            With CurWordApp.Options
                .AutoFormatAsYouTypeFormatListItemBeginning = False
                .AutoFormatAsYouTypeDefineStyles = False
                .AutoFormatAsYouTypeApplyNumberedLists = False
            End With

            '   prevent save prompt for mpVBA.dot
            MarkStartupSaved()

            lInitializeNumbering = Err.Number
        End Function

        Public Shared Function bChangeListNumCode(fldListNum As Word.Field, _
                                    iListLevel As Integer, _
                                    Optional iStartAt As Integer = _
                                        mpListNumNoStartAt) As Boolean
            'changes text of fldListNum to
            'ltListTemplate, iListLevel, iStartAt

            Dim rngListNumCode As Word.Range
            Dim xNewCode As String

            rngListNumCode = fldListNum.Code

            xNewCode = " " & g_xListNum & "  \l" & iListLevel & " "
            '    xNewCode = " LISTNUM  \l" & iListLevel & " "

            '   add start at switch, if specified
            If iStartAt <> mpListNumNoStartAt Then _
                xNewCode = xNewCode & "\s" & iStartAt & " "

            rngListNumCode.Text = xNewCode

        End Function

        Public Shared Function bDemoteLevel() As Boolean
            'demote level based on type of
            'numbering - native v. LISTNUM

            '   get numbering type
            With CurWordApp.Selection.Range.ListFormat
                Select Case .ListType
                    Case Word.WdListType.wdListNoNumbering
                    Case Else
                        iChangeLevel_NativeWord(CurWordApp.Selection, 1)
                End Select
            End With
        End Function

        Public Shared Function bRemoveRestartSwitch(fldExisting As Word.Field) As Boolean
            'removes the restart switch
            'in field fldExisting

            Dim rngFieldCode As Word.Range
            Dim iSwitchPosStart As Integer
            Dim iSwitchPosEnd As Integer
            Dim xNewCode As String

            rngFieldCode = fldExisting.Code

            '   get start position of switch
            iSwitchPosStart = InStr(UCase(rngFieldCode.Text), "\S")

            '   if switch exists
            If iSwitchPosStart Then
                '       find end of switch position
                iSwitchPosEnd = InStr(iSwitchPosStart, rngFieldCode.Text, " ")

                '       build new field code text without switch
                xNewCode = Left(rngFieldCode.Text, iSwitchPosStart - 1) & _
                           Mid(rngFieldCode.Text, iSwitchPosEnd + 1)
                rngFieldCode.Text = xNewCode
            End If
        End Function

        Public Shared Function bSetSelectedScheme(docP As Word.Document, _
                                    ByVal xKey As String) As Boolean
            '   sets doc var in document
            '   docDoc to value xKey - xkey is of the form
            '   'iXXX' where XXX is the scheme name and
            '   i is the scheme type - var is retrieved
            '   when inserting numbers from toolbar
            Dim xScheme As String

            '9.9.5001 - trim key for favorites
            If Left$(xKey, 2) = "10" Then _
                xKey = Mid$(xKey, 3)

            '   set doc variable
            docP.Variables(mpActiveSchemeDocVar).Value = xKey

            '   enable toolbar controls
            xScheme = Mid(xKey, 2)

            '   set for downward compatibility
            docP.Variables(mpActiveScheme80DocVar).Value = Mid(xKey, 6)

        End Function

        Public Shared Function iRemove(rngScope As Word.Range) As Integer
            '   sets all paras in rngScope to style xStyle

            Dim paraExisting As Word.Paragraph
            Dim xStyle As String
            Dim styStyle As Word.Style
            Dim rngP As Word.Range
            Dim styPara As Word.Style
            Dim bStyIsNum As Boolean
            Dim ltStyle As Word.ListTemplate
            Dim rngExpand As Word.Range

            '   GLOG 5194 - expand to include partner style separator paragraph
            rngExpand = rngScope.Duplicate
            rngExpand.Expand(wdUnits.wdParagraph)

            'if the previous paragraph is style separator, add it to range
            If rngExpand.Start > 0 Then
                If rngExpand.Previous(WdUnits.wdParagraph).Paragraphs.First.IsStyleSeparator Then
                    rngExpand.MoveStart(WdUnits.wdParagraph, -1)
                End If
            End If

            'if the last paragraph is style separator, include the next paragraph
            If rngExpand.Paragraphs.Last.IsStyleSeparator Then
                rngExpand.MoveEnd(WdUnits.wdParagraph)
            End If

            If rngExpand.Paragraphs.Count > rngScope.Paragraphs.Count Then _
                rngScope.SetRange(rngExpand.Start, rngExpand.End)

            xStyle = xTranslateBuiltInStyle(g_xRemoveNumberStyle)

            '   ensure that default style exists in doc
            On Error Resume Next
            styStyle = CurWordApp.ActiveDocument.Styles(xStyle)
            On Error GoTo 0
            If styStyle Is Nothing Then _
                    styStyle = CurWordApp.ActiveDocument.Styles(wdBuiltInStyle.wdStyleNormal)

            For Each paraExisting In rngScope.Paragraphs
                rngP = paraExisting.Range
                With rngP
                    .StartOf()
                    '           delete shift-returns
                    If Left(paraExisting.Range.Text, 1) = Chr(11) Then
                        .MoveEndWhile(CStr(Chr(11)), 2)
                        .Delete()
                    End If

                    '           do only if paragraph or style is outline numbered;
                    '           need to check style because user may have inadvertently
                    '           removed number with backspace key
                    styPara = Nothing
                    ltStyle = Nothing

                    On Error Resume Next
                    styPara = CurWordApp.ActiveDocument.Styles(.Style)
                    If Not styPara Is Nothing Then
                        ltStyle = styPara.ListTemplate
                    End If
                    On Error GoTo 0

                    If (.ListFormat.ListType <> wdListType.wdListNoNumbering) Or
                            (Not ltStyle Is Nothing) Then
                        '               delete leading spaces (moved outside number 11/8/01)
                        If Left(paraExisting.Range.Text, 1) = Chr(32) Then
                            .MoveEndWhile(CStr(Chr(32)), 2)
                            .Delete()
                        End If

                        '               apply default style
                        paraExisting.Style = styStyle

                        'GLOG 5194 - remove style separator if necessary
                        If paraExisting.IsStyleSeparator Then
                            'remove style separator
                            paraExisting.Range.Characters.Last.Delete()
                        End If
                    End If
                End With
            Next paraExisting

        End Function

        Public Shared Function rngInsertNumWordNum(rngCur As Word.Range, _
                                     xScheme As String, _
                                     iLevel As Integer, _
                                     Optional iCaller As Integer = mpFunctionInsertNumber, _
                                     Optional bIsDoubleSpaced As Boolean = False) As Word.Range

            '   insert a Native Word number-
            '   first apply style, then, if that
            '   failed to insert a number, apply
            '   the supplied list template - iCaller
            '   determines how this function behaves

            Dim xStyle As String
            Dim llCur As Word.ListLevel
            Dim styP As Word.Style
            Dim iMaxLevelLoaded As Integer
            Dim bIsHScheme As Boolean
            Dim bIsLinked As Boolean
            Dim bHiddenPara As Boolean
            Dim lColor As Long

            '   check to see if active scheme uses Heading styles
            bIsHScheme = bIsHeadingScheme(xScheme)
            On Error GoTo ProcError

            '   get style to apply
            xStyle = CurWordApp.ActiveDocument _
                        .ListTemplates(xGetFullLTName(xScheme)) _
                            .ListLevels(iLevel).LinkedStyle

            '   scheme is linked correctly if either a) it's a heading
            '   scheme and linked to the appropriate heading 1-9 style
            '   or b)it's is a MacPac scheme, and the level is linked to
            '   the appropriate MacPac _L1-X style
            bIsLinked = bLevelIsLinked(xScheme, iLevel)

            '   if not appropriately linked, attempt relink
            If Not bIsLinked Then
                bRelinkScheme(xScheme, _
                              mpSchemeTypes.mpSchemeType_Document, _
                              bIsHScheme)

                '       get style to apply
                xStyle = CurWordApp.ActiveDocument _
                            .ListTemplates(xGetFullLTName(xScheme)) _
                                .ListLevels(iLevel).LinkedStyle
            End If

            With rngCur
                '       load scheme if necessary -
                On Error Resume Next
                styP = CurWordApp.ActiveDocument.Styles(xStyle)
                If styP Is Nothing Then
                    '           exit if loaded scheme does not have that level
                    On Error Resume Next
                    styP = CurWordApp.ActiveDocument.Styles(xStyle)
                    If styP Is Nothing Then
                        '               display msg if user is trying to insert number,
                        '               else change to Normal style
                        If iCaller = mpFunctionInsertNumber Then
                            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                                xMsg = "Niveau " & iLevel & _
                                       " n'est pas defini pour le th�me " & _
                                       xGetStyleRoot(xScheme) & "."
                            Else
                                xMsg = "Level " & iLevel & _
                                       " is not defined for the " & _
                                       xGetStyleRoot(xScheme) & " scheme."
                            End If
                            MsgBox(xMsg, vbExclamation, g_xAppName)
                        Else
                            .Style = wdBuiltInStyle.wdStyleNormal
                        End If
                        Exit Function
                    End If
                End If

                On Error GoTo ProcError

                '***************************************************
                'added all but .Style = xStyle on 1/6/00 to preserve
                'directly applied attributes
                Dim rngFText As Range
                Dim bIsStyleBased As Boolean
                Dim rngBookmark As Word.Range
                Dim iCount As Integer
                Dim xBookmarks(,) As String
                Dim i As Integer

                bIsStyleBased = bTOCLevelIsStyleBased(xScheme, iLevel)

                If iCaller = mpFunctionChangeScheme Then
                    '           preserve hidden paragraph mark
                    bHiddenPara = ((.Font.Hidden = False) And _
                        (.Characters.Last.Font.Hidden = True))
                    If bHiddenPara Then _
                        lColor = .Characters.Last.Font.ColorIndex

                    If Not bIsStyleBased Then
                        '               preserve bookmarks
                        iCount = .Bookmarks.Count
                        If iCount > 0 Then
                            ReDim xBookmarks(iCount - 1, 2)
                            For i = 1 To iCount
                                With .Bookmarks(i)
                                    xBookmarks(i - 1, 0) = .Name
                                    xBookmarks(i - 1, 1) = .Range.Start
                                    xBookmarks(i - 1, 2) = .Range.End
                                End With
                            Next i
                        End If

                        '               get current font formatting
                        .MoveEnd(wdUnits.wdCharacter, -1)
                        rngFText = .FormattedText

                        '               insert new para
                        .InsertParagraphAfter()
                        .MoveStart(wdUnits.wdParagraph)
                        .Expand(wdUnits.wdParagraph)
                        .Move(wdUnits.wdCharacter, -1)
                    End If
                End If

                '       apply style
                .Style = xStyle

                If iCaller = mpFunctionChangeScheme Then
                    '           restore hiddenparagraph mark
                    If bHiddenPara Then
                        With .Characters.Last.Font
                            .Hidden = True
                            .ColorIndex = lColor
                        End With
                    End If

                    If Not bIsStyleBased Then
                        '               apply formatted range to new para
                        .FormattedText = rngFText

                        '               delete old para
                        .Previous(wdUnits.wdParagraph).Delete()
                        .MoveEnd(wdUnits.wdCharacter)

                        '               restore bookmarks
                        For i = 0 To iCount - 1
                            With CurWordApp.ActiveDocument
                                rngBookmark = .Range(xBookmarks(i, 1), _
                                    xBookmarks(i, 2))
                                .Bookmarks.Add(xBookmarks(i, 0), rngBookmark)
                            End With
                        Next i
                    End If
                End If
                '***************************************************

                '       get list level object of level iLevel
                llCur = .ListFormat.ListTemplate _
                                .ListLevels(iLevel)

                '       insert/remove trailing characters
                rngCur = rngEditTrailChr11(rngCur, llCur)

                '       return character pos after trailing chr
                rngInsertNumWordNum = rngCur

            End With
            Exit Function

ProcError:
            Select Case Err.Number
                Case Else
                    rngInsertNumWordNum = Nothing
                    Exit Function
            End Select
        End Function

        Public Shared Function iLoadScheme(docDoc As Word.Document, _
                             ByVal xScheme As String, _
                             ByVal iSchemeType As mpSchemeTypes, _
                             Optional ByVal xNewAlias As String = "", _
                             Optional ByVal xNewName As String = "") As Integer
            'copies styles XXL1-9 into
            'docDoc, where XX = xScheme -
            'returns the last loaded level

            Dim xStyleSourceFile As String
            Dim xStyleDestFile As String
            Dim xStyle As String
            Dim xStyleRoot As String
            Dim i As Integer
            Dim styScheme As Word.Style
            Dim styNumCont As Word.Style
            Dim styNormal As Word.Style
            Dim styBody As Word.Style
            Dim iListLevels As Integer
            Dim bLTIsOld As Boolean
            Dim oScheme As CNumScheme
            Dim xProps As String
            Dim xLT As String
            Dim ltScheme As Word.ListTemplate
            Dim xStyleNext As String
            Dim xNextParasAdded As String
            Dim xStyleNames(8, 1) As String
            Dim ltNative As Word.ListTemplate
            Dim oStyleSource As Word.Template
            Dim bInUse As Boolean

            On Error GoTo ProcError

            '   prompt for status
            If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                CurWordApp.StatusBar = "Chargement du th�me. Veuillez patienter..."
            Else
                CurWordApp.StatusBar = "Loading scheme.  Please wait..."
            End If

            With CurWordApp
                .ScreenUpdating = False
                If iSchemeType = mpSchemeTypes.mpSchemeType_Public Then
                    xStyleSourceFile = g_xFNumSty
                    oStyleSource = g_oFNumSty
                Else
                    xStyleSourceFile = g_xPNumSty
                    oStyleSource = g_oPNumSty
                End If

                '       ensure numbers.sty is an addin
                CurWordApp.AddIns(xStyleSourceFile).Installed = True

                '       use WordBasic function to bypass ODMA based return
                xStyleDestFile = CurWordApp.WordBasic.FileName$()
            End With

            styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
            styBody = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleBodyText)

            xStyleRoot = xGetStyleRoot(xScheme)

            '   create num continue style
            '   if necessary - only for upgrade clients
            If g_iUpgradeFrom < 90 Then
                On Error Resume Next
                styNumCont = CurWordApp.ActiveDocument _
                        .Styles("Num Continue")
                On Error GoTo ProcError

                If styNumCont Is Nothing Then
                    If g_bCreateUnlinkedStyles Then
                        '9.9.4010
                        styNumCont = AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument,
                            "Num Continue")
                    Else
                        styNumCont = CurWordApp.ActiveDocument.Styles.Add("Num Continue")
                    End If
                    With styNumCont
                        .BaseStyle = WdBuiltinStyle.wdStyleBodyText
                        .Font = styNormal.Font
                        .ParagraphFormat.SpaceAfter =
                            CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleBodyText).ParagraphFormat.SpaceAfter
                    End With
                End If
            End If

            '   ensure presence of firm-wide next paragraph
            '   styles; copy only if missing
            If g_xNextParaStyles(0) <> "" Then
                '       styles in this pipe delineated list will have their
                '       font (where appropriate) and line spacing adjusted to Normal
                '       in the same way as "L1-9" and "Cont" styles
                xNextParasAdded = "|"
                For i = 0 To UBound(g_xNextParaStyles)
                    xStyleNext =
                        xTranslateBuiltInStyle(g_xNextParaStyles(i))
                    styNumCont = Nothing
                    On Error Resume Next
                    styNumCont = CurWordApp.ActiveDocument _
                        .Styles(xStyleNext)
                    On Error Resume Next
                    Err.Clear()
                    If styNumCont Is Nothing Then
                        xNextParasAdded = xNextParasAdded & xStyleNext & "|"
                        CurWordApp.OrganizerCopy(xStyleSourceFile,
                                                  xStyleDestFile,
                                                  xStyleNext,
                                                  WdOrganizerObject.wdOrganizerObjectStyles)
                    End If
                    If Err.Number <> 0 Then
                        styNumCont = CurWordApp.ActiveDocument.Styles _
                            .Add(xStyleNext, WdStyleType.wdStyleTypeParagraph)
                        styNumCont.ParagraphFormat.SpaceAfter = 12
                    End If

                    On Error GoTo ProcError
                Next i
            End If

            '   preserve aliases on reset
            On Error Resume Next
            For i = 1 To 9
                xStyleNames(i - 1, 0) =
                    CurWordApp.ActiveDocument.Styles(xStyleRoot & "_L" & i).NameLocal
                If Err.Number <> 0 Then Exit For
                xStyleNames(i - 1, 1) =
                    CurWordApp.ActiveDocument.Styles(xStyleRoot & " Cont " & i).NameLocal
                Err.Number = 0
            Next i
            On Error GoTo ProcError

            '   get number of levels
            iListLevels = iGetLevels(xScheme, iSchemeType)

            '9.9.6001 - track which cont styles are already in the document
            Dim bContStyles(8) As Boolean
            Dim oStyle As Word.Style
            If g_iLoadContStyles <> mpLoadContStyles.mpLoadWithScheme Then '9.9.6004
                For i = 1 To iListLevels
                    On Error Resume Next
                    oStyle = CurWordApp.ActiveDocument.Styles(xStyleRoot & " Cont " & i)
                    On Error GoTo ProcError
                    bContStyles(i - 1) = (Not oStyle Is Nothing)
                    oStyle = Nothing
                Next i
            End If

            '   copy scheme-specific next paragraph styles;
            '   first create dummies, in case any are
            '   next paragraph style for a previous level
            On Error Resume Next
            For i = 1 To iListLevels
                If g_bCreateUnlinkedStyles Then
                    '9.9.4010
                    AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument, xStyleRoot & " Cont " & i)
                Else
                    CurWordApp.ActiveDocument.Styles.Add(xStyleRoot & " Cont " & i)
                End If
            Next i

            For i = 1 To iListLevels
                xStyle = xStyleRoot & " Cont " & i
                CurWordApp.OrganizerCopy(xStyleSourceFile,
                                          xStyleDestFile,
                                          xStyle,
                                          WdOrganizerObject.wdOrganizerObjectStyles)

                '       restore original name
                If xStyleNames(i - 1, 1) <> "" Then _
                    CurWordApp.ActiveDocument.Styles(xStyle).NameLocal = xStyleNames(i - 1, 1)
            Next i
            On Error GoTo ProcError

            '   if list template and styles already exist in document,
            '   redefine list template
            '   if not, add document property to hold name and alias
            On Error Resume Next
            styScheme = CurWordApp.ActiveDocument.Styles(xStyleRoot & "_L" & 1)
            On Error GoTo ProcError

            If Not styScheme Is Nothing Then
                If Not bListTemplateExists(xScheme) Then
                    '           check for unconverted 8.0 list template
                    If bListTemplateExists(xStyleRoot) Then _
                        CurWordApp.ActiveDocument.ListTemplates(xStyleRoot).Name = xScheme
                End If
                If bListTemplateExists(xScheme) Then
                    iLoadListTemplate(docDoc, xScheme, iSchemeType)
                    bLTIsOld = True
                End If
            Else
                '       create dummies of styles in case any are
                '       next paragraph style for a previous level
                On Error Resume Next
                For i = 1 To iListLevels
                    If g_bCreateUnlinkedStyles Then
                        '9.9.4010
                        AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument, xStyleRoot & "_L" & i)
                    Else
                        CurWordApp.ActiveDocument.Styles.Add(xStyleRoot & "_L" & i)
                    End If
                Next i
                On Error GoTo ProcError
            End If

            '   cycle through levels, copying styles and doc props
            For i = 1 To iListLevels
                xStyle = xStyleRoot & "_L" & i

                '       copy style into active document
                CurWordApp.OrganizerCopy(xStyleSourceFile,
                                          xStyleDestFile,
                                          xStyle,
                                          WdOrganizerObject.wdOrganizerObjectStyles)

                '       in Word 12, the next paragraph style of the first copied numbered
                '       style is always itself - copy again to ensure correct setting
                If (i = 1) And (g_xDSchemes(0, 0) = "") Then
                    CurWordApp.OrganizerCopy(xStyleSourceFile,
                                              xStyleDestFile,
                                              xStyle,
                                              WdOrganizerObject.wdOrganizerObjectStyles)
                End If

                '       copy doc props into appropriate list level properties
                xProps = xProps & oStyleSource.CustomDocumentProperties(xScheme & i).Value

                '       restore original name
                If xStyleNames(i - 1, 0) <> "" Then _
                    CurWordApp.ActiveDocument.Styles(xStyle).NameLocal = xStyleNames(i - 1, 0)
            Next i

            For i = iListLevels + 1 To 9
                '       if level previously existed, delete style
                On Error Resume Next
                xStyle = xStyleRoot & "_L" & i
                With CurWordApp.ActiveDocument.Styles
                    styScheme = Nothing
                    styScheme = .Item(xStyle)
                    If Not styScheme Is Nothing Then
                        styScheme.Delete()
                    End If
                End With
                On Error GoTo ProcError

                '       copy doc props into appropriate
                '       list level properties - bring it
                '       up to level 9 - levels not used
                '       in the scheme are given a value
                '       = PropertyNotAvailable
                xProps = xProps & PropertyNotAvailable
            Next i

            '   rename list template
            xLT = xScheme & "|" & xProps & "|"
            On Error Resume Next
            ltScheme =
                CurWordApp.ActiveDocument.ListTemplates(xGetFullLTName(xScheme))
            On Error GoTo ProcError

            If ltScheme Is Nothing Then
                '       this will happen if scheme has previously been deleted
                '       from doc; even if list template is unnamed and converted
                '       to single level, Word will find and reuse it
                ltScheme = CurWordApp.ActiveDocument.Styles(xStyleRoot & "_L" & 1) _
                    .ListTemplate
                '       reset list template
                ltScheme.Name = xLT
                iLoadListTemplate(docDoc, xScheme, iSchemeType)
                bLTIsOld = True
            End If

            ltScheme.Name = xLT

            '   clean up residual font substitution;
            '   do not do to pre-existing list template
            If Not bLTIsOld Then _
                ResetLLFontsToThemselves(ltScheme)

            '   modify formats of scheme
            '   styles as necessary
            Dim bDynFonts As Boolean
            Dim bDynSpacing As Boolean
            oScheme = GetRecord(xScheme, mpSchemeTypes.mpSchemeType_Document)
            bDynFonts = oScheme.DymanicFonts
            bDynSpacing = oScheme.DynamicSpacing
            bSetSchemeProperties(xScheme, bDynFonts, xNextParasAdded, bDynSpacing)
            If bDynFonts Then
                '       apply normal font to numbers
                For i = 1 To 9
                    With ltScheme.ListLevels(i).Font
                        If (.Name <> "Symbol") And
                                (.Name <> "Wingdings") Then
                            If .Name <> "" Then _
                                .Name = styNormal.Font.Name
                            If .Size <> WdConstants.wdUndefined Then _
                                .Size = styNormal.Font.Size
                        End If
                    End With
                Next i
            End If

            '   turn on dynamic spacing - this is necessary to ensure
            '   that doc is compatible with pre-9.8 versions of numbering
            If Not bDynSpacing Then
                SetField(xScheme, mpRecordFields.mpRecField_DynamicFonts, Abs(CInt(bDynFonts)),
                    mpSchemeTypes.mpSchemeType_Document)
            End If

            If Len(xNewName) Then
                bRenameExistingScheme(xNewName,
                                      xScheme)
                xScheme = xNewName
            End If

            If Len(xNewAlias) Then
                SetField(xScheme,
                         mpRecordFields.mpRecField_Alias,
                         xNewAlias, ,
                         CurWordApp.ActiveDocument)
            End If

            '   remove duplicate list template; an example of when
            '   this might happen is when using Word Heading styles,
            '   copying the scheme into a new doc manually or with
            '   MacPac 2K Reuse, and then clicking Reset
            For Each ltScheme In CurWordApp.ActiveDocument.ListTemplates
                If UCase(ltScheme.Name) = UCase(xScheme) Then _
                    ltScheme.Name = ""
            Next ltScheme

            '9.9.6001 - delete cont styles that aren't being used and
            'weren't already in the document
            '9.9.6004 - split this into three options
            If g_iLoadContStyles <> mpLoadContStyles.mpLoadWithScheme Then
                If g_iLoadContStyles = mpLoadContStyles.mpLoadAllOnDemand Then
                    For i = 1 To iListLevels
                        'leave all if any pre-existing or in use
                        bInUse = (CurWordApp.ActiveDocument.Styles(xStyleRoot & "_L" & i).NextParagraphStyle.NameLocal =
                            xStyleRoot & " Cont " & i) Or bContStyles(i - 1)
                        If bInUse Then Exit For
                    Next i
                End If

                For i = 1 To iListLevels
                    If g_iLoadContStyles = mpLoadContStyles.mpLoadIndividuallyOnDemand Then
                        'leave only those that are pre-existing or in use
                        bInUse = (CurWordApp.ActiveDocument.Styles(xStyleRoot & "_L" & i).NextParagraphStyle.NameLocal =
                            xStyleRoot & " Cont " & i) Or bContStyles(i - 1)
                    End If
                    If Not bInUse Then
                        CurWordApp.ActiveDocument.Styles(xStyleRoot & " Cont " & i).Delete()
                    End If
                Next i
            End If

            CurWordApp.StatusBar = ""
            SendShiftKey()
            Exit Function

ProcError:
            Select Case Err.Number
                Case mpErrors.mpStyleDoesNotExist
                    '           return last level loaded
                    CurWordApp.StatusBar = ""
                    SendShiftKey()
                    iLoadScheme = i
                Case mpErrors.mpErrListNumNameInUse
                    xLT = ModifyUnusableListName(xLT)
                    '           the following line will prevent any later attempt to
                    '           restore the original name
                    xNewAlias = ""
                    Resume
                Case Else
                    MsgBox(Err.Number & ":" & Err.Description)
            End Select
            Exit Function
        End Function

        Public Shared Function iLoadListTemplate(docDoc As Word.Document, xScheme As String, iSchemeType As mpSchemeTypes) As Integer
            'redefines scheme xScheme to that
            'located in tsgNumbers.sty

            Dim ltSource As Word.ListTemplate
            Dim llSource As Word.ListLevel
            Dim fontSource As Word.Font
            Dim ltDest As Word.ListTemplate
            Dim llDest As Word.ListLevel
            Dim i As Integer
            Dim styLevel As Word.Style
            Dim styNormal As Word.Style
            Dim styBody As Word.Style
            Dim xStyle As String
            Dim xSource As String
            Dim bIs97DocScheme As Boolean

            If iSchemeType = mpSchemeTypes.mpSchemeType_Public Then
                xSource = g_xFNumSty
                ltSource = g_oFNumSty.ListTemplates(xScheme)
            Else
                xSource = g_xPNumSty
                ltSource = g_oPNumSty.ListTemplates(xScheme)
            End If
            ltDest = docDoc.ListTemplates(xGetFullLTName(xScheme))

            For i = 1 To 9
                llSource = ltSource.ListLevels(i)
                llDest = ltDest.ListLevels(i)
                iCopyListLevel(xScheme, llSource, llDest, True, bIs97DocScheme)

                If llSource.LinkedStyle <> "" Then
                    On Error Resume Next
                    styLevel = Nothing
                    styLevel = CurWordApp.ActiveDocument.Styles(llSource.LinkedStyle)
                    On Error GoTo 0
                    If styLevel Is Nothing Then
                        '       ensure numbers.sty is an addin
                        CurWordApp.AddIns(xSource).Installed = True

                        xStyle = llSource.LinkedStyle

                        '       copy style into active document
                        CurWordApp.OrganizerCopy(xSource,
                                          CurWordApp.WordBasic.FileName$(),
                                          xStyle,
                                          WdOrganizerObject.wdOrganizerObjectStyles)

                        '       ensure that font formats of scheme match
                        '       the font formats of the Normal style
                        styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
                        styBody = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleBodyText)
                        With CurWordApp.ActiveDocument.Styles.Item(xStyle)
                            '           set name and size
                            .Font.Name = styNormal.Font.Name
                            .Font.Size = styNormal.Font.Size
                        End With
                    End If
                End If

                If Not bIs97DocScheme Then
                    llDest.LinkedStyle = llSource.LinkedStyle
                End If

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "R�tabli th�me num�ros.  " &
                        "Pourcentage compl�t�: " & Format(i / 9, "0%")
                Else
                    CurWordApp.StatusBar = "Resetting scheme numbers.  " &
                        "Percent complete: " & Format(i / 9, "0%")
                End If

            Next i

            '   relink
            RelinkPreserveIndents(docDoc, ltDest, xScheme, 1, 9)

            CurWordApp.StatusBar = ""
            SendShiftKey()
        End Function

        Public Shared Function bSchemeIsNativeWordBased(xScheme As String) As Boolean
            'returns whether xScheme uses
            'Native Word's numbering or
            'LISTNUM numbering - always true
            'if mpUseListNum switch is FALSE

            If mpUseListNum = False Or
                xScheme = mpGalleryItem Or
                xScheme = mpPleadingHeading1Item Or
                xScheme = mpPleadingHeading2Item Then

                bSchemeIsNativeWordBased = True
            Else
                bSchemeIsNativeWordBased = False
            End If

        End Function

        Public Shared Function iGetListType(rngLocation As Word.Range) As Integer
            'returns type of list in listformat object -
            'could use WORD's .listtype, but it's buggy

            Dim iNumTotalNumbers As Integer
            Dim iNumParagraphNumbers As Integer
            Dim iNumListNumNumbers As Integer


            With rngLocation.Paragraphs(1).Range.ListFormat
                '       count number of Native numbers
                iNumParagraphNumbers = .CountNumberedItems(WdNumberType.wdNumberParagraph)

                '       count number of ListNum numbers
                iNumListNumNumbers = .CountNumberedItems(WdNumberType.wdNumberListNum)

                '       determine type based on above counts
                If iNumParagraphNumbers > 0 And iNumListNumNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeMixed
                ElseIf iNumParagraphNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeNative
                ElseIf iNumListNumNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeListNum
                Else
                    iGetListType = mpListTypes.mpListTypeNoNumbering
                End If
            End With
        End Function


        Public Shared Function iChangeLevel(iOffset As Integer) As Integer
            'promote level

            Dim paraExisting As Paragraph
            Dim rngCurrent As Word.Range
            Dim rngPara As Word.Range
            Dim fldPrimaryParaNumber As Word.Field
            Dim rngPrimaryParaNumber As Word.Range
            Dim bShowAll As Boolean
            Dim rngExpand As Word.Range
            Dim iSelectionType As mpNumberingSelectionTypes 'GLOG 5196

            '   ensure that hidden paragraph marks are showing
            With CurWordApp.ActiveWindow.View
                bShowAll = .ShowAll
                .ShowAll = True
            End With

            '   operate differently based on selection type
            rngCurrent = CurWordApp.Selection.Range

            'GLOG 5196 - get selection type before expanding for style separator
            If rngCurrent.Text = "" Then
                'target only paragraph numbering
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint
            ElseIf rngCurrent.Paragraphs.Count > 1 Then
                'GLOG 5260 - partial selection of multiple paragraphs should target everything
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_FullParagraph
            ElseIf rngCurrent.Characters.Count <
                    rngCurrent.Paragraphs(1).Range.Characters.Count Then
                'target only list nums
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph
            Else
                'target all numbering
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_FullParagraph
            End If

            '   10/9/12 - expand to include partner style separator paragraph
            'GLOG 5196 - don't expand if partial paragraph selected -
            'in this case, we should be targeting only on list nums
            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph Then
                rngExpand = rngCurrent.Duplicate
                rngExpand.Expand(WdUnits.wdParagraph)

                'if the previous paragraph is style separator, add it to range
                If rngExpand.Start > 0 Then
                    If rngExpand.Previous(WdUnits.wdParagraph).Paragraphs.First.IsStyleSeparator Then
                        rngExpand.MoveStart(WdUnits.wdParagraph, -1)
                    End If
                End If

                'if the last paragraph is style separator, include the next paragraph
                If rngExpand.Paragraphs.Last.IsStyleSeparator Then
                    rngExpand.MoveEnd(WdUnits.wdParagraph)
                End If

                If rngExpand.Paragraphs.Count > rngCurrent.Paragraphs.Count Then
                    'expand
                    rngCurrent.SetRange(rngExpand.Start, rngExpand.End)
                ElseIf (rngCurrent.Paragraphs.Count = 2) And
                        rngCurrent.Paragraphs(1).IsStyleSeparator And
                        ((rngCurrent.Start > rngExpand.Start) Or
                        (rngCurrent.End < rngExpand.End)) Then
                    'partial paragraph selection spans style separator - reclassify
                    'GLOG 5260 - fixed mistake in conditional
                    iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph
                End If
            End If

            '   for insertion point...
            '   10/10/12 - modified condition to account for expansion code above
            '    If Selection.Type = wdSelectionIP Then
            If rngCurrent.Text = "" Then
                Select Case iGetListType(rngCurrent)
                    Case mpListTypes.mpListTypeNative
                        iChangeLevel_NativeWord(rngCurrent, iOffset)
                    Case mpListTypes.mpListTypeMixed
                        iChangeLevel_NativeWord(rngCurrent, iOffset)
                    Case mpListTypes.mpListTypeNoNumbering
                        iChangeLevel_ContStyle(rngCurrent, iOffset)
                End Select

                'if paragraph contains only trailing characters added by MacPac,
                'move cursor to end - 9.8.1011
                rngPara = CurWordApp.Selection.Paragraphs(1).Range
                With rngPara
                    If .Text <> vbCr Then
                        .MoveEnd(WdUnits.wdCharacter, -1)
                        If (.Text = New String(Chr(11), 2)) Or (.Text = Chr(11)) Or
                        (.Text = "  ") Then
                            .EndOf()
                            .Select()
                        End If
                    End If
                End With
                '   user has selected less than one paragraph
                '   GLOG 5260 - excluded partial selection of multiple paragraphs from the following branch
            ElseIf iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph Then
                '       change selected LISTNUMs - if none,
                '       change existing native number, if any
                Select Case iGetListType(rngCurrent)
                    Case mpListTypes.mpListTypeNative
                        iChangeLevel_NativeWord(rngCurrent, iOffset)
                        'GLOG 5196 - added support for list nums in non-numbered paragraph
                    Case mpListTypes.mpListTypeMixed, mpListTypes.mpListTypeListNum
                        iChangeLevel_ListNum(rngCurrent, iOffset)
                    Case mpListTypes.mpListTypeNoNumbering
                        iChangeLevel_ContStyle(rngCurrent, iOffset)
                End Select

                '   user has selected at least one paragraph
            Else
                '       cycle through all paras
                For Each paraExisting In rngCurrent.Paragraphs
                    rngPara = paraExisting.Range
                    '           changing all numbers in each paragraph
                    Select Case iGetListType(rngPara)
                        Case mpListTypes.mpListTypeNative
                            iChangeLevel_NativeWord(rngPara, iOffset)
                        Case mpListTypes.mpListTypeMixed
                            iChangeLevel_NativeWord(rngPara, iOffset)
                            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint Then 'GLOG 5196
                                iChangeLevel_ListNum(rngPara, iOffset)
                            End If
                        Case mpListTypes.mpListTypeListNum
                            'GLOG 5196 - added support for list nums in non-numbered paragraphs
                            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint Then
                                iChangeLevel_ListNum(rngPara, iOffset)
                            End If
                        Case mpListTypes.mpListTypeNoNumbering
                            iChangeLevel_ContStyle(rngPara, iOffset)
                    End Select

                Next paraExisting

            End If

            '   restore view
            CurWordApp.ActiveWindow.View.ShowAll = bShowAll

        End Function
        Public Shared Function iChangeStartAt(iStartAt As Integer) As Integer
            'restarts number at # iStartAt
            'different techniques for Native / LISTNUM -
            'if iStartAt = mpContinueFromPrevious then, continues

            Dim paraExisting As Paragraph
            Dim rngCurrent As Word.Range
            Dim rngPara As Word.Range
            Dim fldPrimaryParaNumber As Word.Field
            Dim rngPrimaryParaNumber As Word.Range
            Dim iListLevel As Integer
            Dim bReformatTC As Boolean
            Dim oFont As Word.Font
            Dim bShowAll As Boolean
            Dim rngExpand As Word.Range
            Dim iSelectionType As mpNumberingSelectionTypes 'GLOG 5196

            '   turn on show all - this will prevent error when preceding paragraph is hidden
            With CurWordApp.ActiveWindow.View
                bShowAll = .ShowAll
                .ShowAll = True
            End With

            rngCurrent = CurWordApp.Selection.Range

            'GLOG 5196 - get selection type before expanding for style separator
            If rngCurrent.Text = "" Then
                'target only paragraph numbering
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint
            ElseIf rngCurrent.Paragraphs.Count > 1 Then
                'GLOG 5260 - partial selection of multiple paragraphs should target everything
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_FullParagraph
            ElseIf rngCurrent.Characters.Count <
                    rngCurrent.Paragraphs(1).Range.Characters.Count Then
                'target only list nums
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph
            Else
                'target all numbering
                iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_FullParagraph
            End If

            '   10/19/12 - if the previous paragraph is style separator, add it to range
            'GLOG 5196 - don't expand if partial paragraph selected -
            'in this case, we should be targeting only on list nums
            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph Then
                rngExpand = rngCurrent.Duplicate
                rngExpand.Expand(WdUnits.wdParagraph)

                'if the previous paragraph is style separator, add it to range
                If rngExpand.Start > 0 Then
                    If rngExpand.Previous(WdUnits.wdParagraph).Paragraphs.First.IsStyleSeparator Then
                        rngExpand.MoveStart(WdUnits.wdParagraph, -1)
                    End If
                End If

                'if the last paragraph is style separator, include the next paragraph
                If rngExpand.Paragraphs.Last.IsStyleSeparator Then
                    rngExpand.MoveEnd(WdUnits.wdParagraph)
                End If

                If rngExpand.Paragraphs.Count > rngCurrent.Paragraphs.Count Then
                    'expand
                    rngCurrent.SetRange(rngExpand.Start, rngExpand.End)
                ElseIf (rngCurrent.Paragraphs.Count = 2) And
                        rngCurrent.Paragraphs(1).IsStyleSeparator And
                        ((rngCurrent.Start > rngExpand.Start) Or
                        (rngCurrent.End < rngExpand.End)) Then
                    'partial paragraph selection spans style separator - reclassify
                    'GLOG 5260 - fixed mistake in conditional
                    iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph
                End If
            End If

            '   operate differently based on selection type

            '   for insertion point...
            If CurWordApp.Selection.Type = WdSelectionType.wdSelectionIP Then
                '       store font attributes for reformatting
                With rngCurrent
                    paraExisting = .Paragraphs(1)
                    bReformatTC = bIsMarkedAndFormatted(paraExisting)
                    If Not bReformatTC Then
                        oFont = paraExisting.Range.Font.Duplicate
                    End If
                End With

                Select Case iGetListType(rngCurrent)
                    Case mpListTypes.mpListTypeListNum
                        '               change primary paragraph number only
                        fldPrimaryParaNumber =
                            fldGetPrimaryParaNumber(rngCurrent)
                        rngPrimaryParaNumber =
                            rngGetField(fldPrimaryParaNumber.Code)
                        '               change the number
                        iChangeStartAt_ListNum(rngPrimaryParaNumber,
                                               iStartAt)
                    Case mpListTypes.mpListTypeNative
                        iChangeStartAt_NativeWord(rngCurrent,
                                                  iStartAt)
                    Case mpListTypes.mpListTypeMixed
                        iChangeStartAt_NativeWord(rngCurrent,
                                                  iStartAt)
                    Case mpListTypes.mpListTypeNoNumbering
                End Select

                '       reapply font formatting
                If bReformatTC Then
                    On Error Resume Next
                    iListLevel = paraExisting.Range _
                        .ListFormat.ListLevelNumber
                    On Error GoTo 0
                    rngReFormatTCHeading(rngCurrent, iListLevel)
                Else
                    rngReapplyFont(paraExisting.Range, oFont)
                End If

                '   user has selected less than one paragraph -
                '   maybe change to use .InRange
                '   GLOG 5260 - excluded partial selection of multiple paragraphs from the following branch
            ElseIf iSelectionType = mpNumberingSelectionTypes.mpNumberingSelectionType_PartialParagraph Then
                '       store font attributes for reformatting
                With rngCurrent
                    paraExisting = .Paragraphs(1)
                    bReformatTC = bIsMarkedAndFormatted(paraExisting)
                    If Not bReformatTC Then
                        oFont = paraExisting.Range.Font.Duplicate
                    End If
                End With

                '       change selected LISTNUMs - if none,
                '       change existing native number, if any
                Select Case iGetListType(rngCurrent)
                    Case mpListTypes.mpListTypeListNum
                        iChangeStartAt_ListNum(rngCurrent,
                                               iStartAt)
                    Case mpListTypes.mpListTypeNative
                        iChangeStartAt_NativeWord(rngCurrent,
                                                  iStartAt)
                    Case mpListTypes.mpListTypeMixed
                        iChangeStartAt_ListNum(rngCurrent,
                                               iStartAt)
                    Case mpListTypes.mpListTypeNoNumbering
                End Select

                '       reapply font formatting
                If bReformatTC Then
                    On Error Resume Next
                    iListLevel = paraExisting.Range _
                        .ListFormat.ListLevelNumber
                    On Error GoTo 0
                    rngReFormatTCHeading(rngCurrent, iListLevel)
                Else
                    rngReapplyFont(paraExisting.Range, oFont)
                End If

                '   user has selected at least one paragraph
            Else
                '       cycle through all paras
                For Each paraExisting In rngCurrent.Paragraphs
                    '           store font attributes for reformatting
                    bReformatTC = bIsMarkedAndFormatted(paraExisting)
                    rngPara = paraExisting.Range
                    If Not bReformatTC Then
                        oFont = rngPara.Font.Duplicate
                    End If

                    '           changing all numbers in each paragraph
                    Select Case iGetListType(rngPara)
                        Case mpListTypes.mpListTypeListNum
                            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint Then 'GLOG 5196
                                iChangeStartAt_ListNum(rngPara,
                                                       iStartAt)
                            End If
                        Case mpListTypes.mpListTypeNative
                            iChangeStartAt_NativeWord(rngPara,
                                                      iStartAt)
                        Case mpListTypes.mpListTypeMixed
                            iChangeStartAt_NativeWord(rngPara,
                                                      iStartAt)

                            If iSelectionType <> mpNumberingSelectionTypes.mpNumberingSelectionType_InsertionPoint Then 'GLOG 5196
                                iChangeStartAt_ListNum(rngPara,
                                                       iStartAt)
                            End If
                        Case mpListTypes.mpListTypeNoNumbering
                    End Select

                    '           reapply font formatting
                    If bReformatTC Then
                        On Error Resume Next
                        iListLevel = rngPara.ListFormat.ListLevelNumber
                        On Error GoTo 0
                        rngReFormatTCHeading(rngPara, iListLevel)
                    Else
                        rngReapplyFont(rngPara, oFont)
                    End If
                Next paraExisting
            End If

            '   restore user's setting
            CurWordApp.ActiveWindow.View.ShowAll = bShowAll

        End Function

        Public Shared Function iChangeScheme(ByVal xScheme As String,
                                ByVal iSchemeType As mpSchemeTypes,
                                ByVal xAlias As String,
                                ByVal bResetScheme As Boolean,
                                Optional ByVal bIsDoubleSpaced As Boolean = False,
                                Optional ByVal bDisplayMsg As Boolean = True,
                                Optional ByVal xTargetScheme As String = "",
                                Optional ByVal bUseWordHeadings As Boolean = False,
                                Optional ByVal bBaseOnNormalFont As Boolean = True) As Integer
            'changes numbers to scheme xScheme

            Dim rngScope As Word.Range
            Dim bAutoDefineStylesStart As Boolean
            Dim iUserChoice As Integer
            Dim dlgNumbering As Dialog
            Dim paraExisting As Word.Paragraph
            Dim bSetSchemeAsCurrent As Boolean
            Dim xStyle As String
            Dim xStyleRoot As String
            Dim xMsg As String
            Dim styLevel As Word.Style
            Dim fLoadScheme As Boolean
            Dim i As Integer
            Dim bAutoFormat As Boolean
            Dim xHScheme As String
            Dim iLevels As Integer

            On Error GoTo iChangeScheme_Error

            '   force options
            With CurWordApp.Options
                bAutoDefineStylesStart = .AutoFormatAsYouTypeDefineStyles
                .AutoFormatAsYouTypeDefineStyles = False
            End With

            '   get display name if necessary
            If Len(xAlias) = 0 Then
                xAlias = GetField(xScheme, mpRecordFields.mpRecField_Alias, iSchemeType)
            End If

            '   set scope
            With CurWordApp.Selection
                If .Type = WdSelectionType.wdSelectionIP Or
                    (.Start = 0 And .End = CurWordApp.ActiveDocument.Content.End) Then
                    '           change all numbers of a particular scheme
                    rngScope = CurWordApp.ActiveDocument.Content
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Tous paragraphes num�rot�s dans ce document seront chang�s pour " & xAlias & "."
                    Else
                        xMsg = "All numbered paragraphs in " &
                               "this document will be changed to " & xAlias & "."
                    End If
                    bSetSchemeAsCurrent = True

                ElseIf .Range.ListFormat.CountNumberedItems > 0 Then
                    '           modify only those numbers in selection
                    rngScope = .Range
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Les paragraphes num�rot�s s�lectionn�s seront chang�s pour " & xAlias & "."
                    Else
                        xMsg = "Selected numbered " &
                               "paragraphs will be changed to " & xAlias & "."
                    End If
                    bSetSchemeAsCurrent = False
                    bUseWordHeadings = False
                Else
                    '           there are no numbers in selection, so exit
                    Exit Function
                End If
            End With

            If bDisplayMsg Then
                '       prompt for confirmation
                Dim xAppend As String
                '**************************************************************
                'removed 11/7/01
                '        With rngScope.Duplicate.Find
                '            .ClearFormatting
                '            .Text = "^d " & g_xTCPrefix
                '            .Replacement.Text = ""
                '            .Forward = True
                '            .Wrap = wdFindStop
                '            .Format = False
                '            .MatchCase = False
                '            .MatchWholeWord = False
                '            .MatchWildcards = False
                '            .MatchSoundsLike = False
                '            .MatchAllWordForms = False
                '            .Execute
                '            If .Found Then
                '                xAppend = "Do you want to reformat marked headings " & _
                '                          "to the directly applied format associated " & _
                '                          "with the new scheme?"
                '
                ''               prompt for confirmation and reformatting
                '                iUserChoice = MsgBox(xMsg & vbCr & xAppend, _
                '                                     vbQuestion + vbYesNoCancel, _
                '                                     AppName)
                '            Else
                '**************************************************************
                '               prompt for confirmation
                iUserChoice = MsgBox(xMsg & vbCr,
                                     vbInformation + vbOKCancel,
                                     g_xAppName)
                '            End If
                '        End With
            Else
                iUserChoice = vbOK
            End If

            '   quit function if cancelled
            If iUserChoice = vbCancel Then
                Exit Function
            End If

            '   flag to refresh mp10 task pane because tags may be
            '   deleted and replaced by this process
            g_bRefreshTaskPane = True

            '   set state of tc format in ini - this is a
            '   holdover from v8.0 - the value of this
            '   key is read later in the code;
            '**************************************************************
            '   11/7/01 - We decided to remove formatting prompt
            '   above and force ini to False
            '**************************************************************
            SetUserSetting("Numbering",
                                "FormatHeadings",
                                (iUserChoice = vbYes))

            '   get heading scheme
            xHScheme = xHeadingScheme()

            '   load scheme if specified - ie
            '   if user chose to change to a firm
            '   or private scheme
            fLoadScheme = (iSchemeType = mpSchemeTypes.mpSchemeType_Public) Or
                          (iSchemeType = mpSchemeTypes.mpSchemeType_Private)

            xStyleRoot = xGetStyleRoot(xScheme)

            If fLoadScheme Or bResetScheme Then
                bRet = iLoadScheme(CurWordApp.ActiveDocument,
                                   xScheme,
                                   iSchemeType)

                If Not bUseWordHeadings Then
                    If xHScheme = xScheme Then
                        '               change all paras with Heading style applied to
                        '               proprietary MacPac style of same level
                        For i = 1 To 9
                            With CurWordApp.ActiveDocument.Content.Find
                                .ClearFormatting()
                                .Format = True
                                .Style = xTranslateHeadingStyle(i)
                                .Replacement.Style = xStyleRoot & "_L" & i
                                .Execute(Replace:=WdReplace.wdReplaceAll)
                            End With
                        Next i
                    End If
                    '            If bSchemeIsUnlinked(xScheme) Then
                    bRelinkScheme(xScheme,
                                  mpSchemeTypes.mpSchemeType_Document,
                                  False, False, False)
                    '            End If
                End If

                '       backup scheme properties in case the user
                '       writes over them using the Word UI
                MacPacNumbering.LMP.Numbering.mdlConversions.BackupProps(xScheme)
            End If

            '   if scope is entire doc
            If bSetSchemeAsCurrent And xScheme <> "" Then
                '       set xScheme as selected scheme for new numbers
                bRet = bSetSelectedScheme(CurWordApp.ActiveDocument,
                        iSchemeType & xScheme)

                '       if every numbered paragraph in the doc will be changed,
                '       there's no reason not to relink now - if we wait until an unlinked
                '       level is discovered, we'll need to preserve styles when relinking,
                '       which will result in the loss of restarts
                If (xTargetScheme = "") And Not (fLoadScheme Or bResetScheme) Then _
                    bRelinkScheme(xScheme, mpSchemeTypes.mpSchemeType_Document, False, False, False)
            End If

            '   change scheme
            iChangeScheme_NativeWord(rngScope, xScheme,
                  bIsDoubleSpaced, xTargetScheme)

            '   convert to Word headings
            If bUseWordHeadings Then
                bConvertToHeadingStyles(xScheme, False)
            End If

            '   clean up
            With CurWordApp
                EchoOn()
                .StatusBar = ""
                SendShiftKey()
                .Options.AutoFormatAsYouTypeDefineStyles =
                                    bAutoDefineStylesStart
            End With

            Exit Function

iChangeScheme_Error:
            EchoOn()
            MsgBox(Err.Description)
        End Function

#If False Then
Function bSetLineSpacing(docDoc As Word.Document, _
                         styP As Word.Style, _
                         iLineSpacing As Integer, _
                         bForceBeforeAfter As Boolean) As Boolean

'defines formats of styles
'1-9 of scheme at run time-
'line spacing is the only
'one currently available

    Dim sBeforeAfter As Single

    With styP.ParagraphFormat
'       get total space between
'       exactly puts before; single/double puts after
        sBeforeAfter = .SpaceBefore + .SpaceAfter

'       set line spacing based on line spacing
'       rule in Normal style of active doc
        If CurWordApp.ActiveDocument.Styles(wdStyleNormal) _
            .ParagraphFormat.LineSpacingRule = _
                    wdLineSpaceExactly Then
            If iLineSpacing = 2 Then
                .LineSpacingRule = wdLineSpaceExactly
                .LineSpacing = 24
                .SpaceAfter = 0
                If bForceBeforeAfter Then
                    .SpaceBefore = 0
                Else
                    .SpaceBefore = sBeforeAfter
                End If
            ElseIf iLineSpacing = 1 Then
                .LineSpacingRule = wdLineSpaceExactly
                .LineSpacing = 18
                .SpaceAfter = 0
                If bForceBeforeAfter Then
                    .SpaceBefore = 0
                Else
                    .SpaceBefore = sBeforeAfter
                End If
            Else
                .LineSpacingRule = wdLineSpaceExactly
                .LineSpacing = 12
                .SpaceAfter = 0
                If bForceBeforeAfter Then
                    .SpaceBefore = 12
                Else
                    .SpaceBefore = sBeforeAfter
                End If
            End If
        Else
            If iLineSpacing = 2 Then
                .LineSpacingRule = wdLineSpaceDouble
                .SpaceBefore = 0
                If bForceBeforeAfter Then
                    .SpaceAfter = 0
                Else
                    .SpaceAfter = sBeforeAfter
                End If
            ElseIf iLineSpacing = 1 Then
                .LineSpacingRule = wdLineSpace1pt5
                .SpaceBefore = 0
                If bForceBeforeAfter Then
                    .SpaceAfter = 0
                Else
                    .SpaceAfter = sBeforeAfter
                End If
            Else
                .LineSpacingRule = wdLineSpaceSingle
                .SpaceBefore = 0
                If bForceBeforeAfter Then
                    .SpaceAfter = 12
                Else
                    .SpaceAfter = sBeforeAfter
                End If
            End If
        End If
    End With

End Function
#End If

        Public Shared Function bSetSchemeProperties(ByVal xScheme As String,
                Optional bBaseOnNormalFont As Boolean = True,
                Optional xNextParasAdded As String = "",
                Optional bAdjustSpacing As Boolean = True) As Boolean
            'defines formats of styles
            '1-9 of scheme at run time-
            'line spacing is the only
            'one currently available

            Dim i As Integer
            Dim iLineSpacing As WdLineSpacing
            Dim xStyle As String
            Dim xCont As String
            Dim xStyleRoot As String
            Dim stySchemeL As Word.Style
            Dim styCont As Word.Style
            Dim iNCAlignment As Integer
            Dim styNormal As Word.Style
            Dim styBody As Word.Style
            Dim pfNormal As Word.ParagraphFormat
            Dim iLevels As Integer
            Dim bIsMPPleadingStyle As Boolean
            Dim styContAlt As Word.Style
            Dim pfBody As Word.ParagraphFormat
            Dim lLanguage As Long
            Dim sNormalSpacing As Single
            Dim styNext As Word.Style
            Dim lNextAlignment As Long
            Dim sNextSpacing As Single
            Dim lNextRule As Long
            Dim sNextBefore As Single
            Dim sNextAfter As Single
            Dim bIsAlt As Boolean
            Dim styContNext As Word.Style
            Dim lContNextAlignment As Long
            Dim sContNextSpacing As Single
            Dim lContNextRule As Long
            Dim sContNextBefore As Single
            Dim sContNextAfter As Single
            Dim sLines As Single
            Dim bIsExact As Boolean
            Dim iTrailUnderline As Integer
            Dim iAlignment As Integer

            iNCAlignment = -1
            styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
            styBody = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleBodyText)
            pfNormal = styNormal.ParagraphFormat
            pfBody = styBody.ParagraphFormat

            xStyleRoot = xGetStyleRoot(xScheme)
            iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)
            lLanguage = styNormal.LanguageID

            '   in generic pleading schemes, we're adjusting space after
            '   to space before when line spacing is exact, i.e. pleading paper
            '    bIsMPPleadingStyle = (Left(UCase(xStyleRoot), 8) = "PLEADING")
            bIsMPPleadingStyle = g_bAlwaysAdjustSpacing
            If Not bIsMPPleadingStyle Then
                If g_xPleadingSchemes(0) <> "" Then
                    For i = 0 To UBound(g_xPleadingSchemes)
                        If UCase(g_xPleadingSchemes(i)) = UCase(xStyleRoot) Then
                            bIsMPPleadingStyle = True
                            Exit For
                        End If
                    Next i
                End If
            End If

            For i = 9 To 1 Step -1
                xStyle = xStyleRoot & "_L" & i
                xCont = xStyleRoot & " Cont" & " " & i

                '       change para line spacing according to
                '       current rule and passed parameter
                On Error Resume Next
                With CurWordApp.ActiveDocument.Styles
                    stySchemeL = .Item(xStyle)
                    styCont = .Item(xCont)
                    styContAlt = .Item(xStyle).NextParagraphStyle
                    styNext = .Item(xStyleRoot & "_L" & (i + 1))
                    styContNext = .Item(xStyleRoot & " Cont " & (i + 1))
                End With

                '       check for alternate cont style
                If Not (styContAlt Is Nothing) Then
                    If (styContAlt.NameLocal <> styCont.NameLocal) And
                            (InStr(UCase(xNextParasAdded), "|" &
                            UCase(styContAlt.NameLocal) & "|") <> 0) Then
                        bIsAlt = True
                    End If
                End If

                '       adjust language and font
                If Not (stySchemeL Is Nothing) Then
                    Dim pfP As Word.ParagraphFormat
                    pfP = stySchemeL.ParagraphFormat

                    '           ensure that language matches normal style
                    stySchemeL.LanguageID = lLanguage

                    If bBaseOnNormalFont Then
                        '               ensure that font formats of style match
                        '               the font formats of the Normal style
                        With stySchemeL.Font
                            .Name = styNormal.Font.Name
                            .Size = styNormal.Font.Size
                        End With
                    End If

                    If Not (styCont Is Nothing) Then
                        '               do for continued style
                        If bBaseOnNormalFont Then
                            With styCont.Font
                                .Name = styNormal.Font.Name
                                .Size = styNormal.Font.Size
                            End With
                        End If
                        styCont.LanguageID = lLanguage
                    End If

                    If bIsAlt Then
                        '               do for alternate cont style
                        If bBaseOnNormalFont Then
                            With styContAlt.Font
                                .Name = styNormal.Font.Name
                                .Size = styNormal.Font.Size
                            End With
                        End If
                        styContAlt.LanguageID = lLanguage
                    End If

                    '           do para formats

                    '           store subsequent level attributes
                    If Not (styNext Is Nothing) Then
                        With styNext.ParagraphFormat
                            lNextAlignment = .Alignment
                            lNextRule = .LineSpacingRule
                            sNextSpacing = .LineSpacing
                            sNextBefore = .SpaceBefore
                            sNextAfter = .SpaceAfter
                        End With
                    End If

                    '           cont style
                    If Not (styContNext Is Nothing) Then
                        With styContNext.ParagraphFormat
                            lContNextAlignment = .Alignment
                            lContNextRule = .LineSpacingRule
                            sContNextSpacing = .LineSpacing
                            sContNextBefore = .SpaceBefore
                            sContNextAfter = .SpaceAfter
                        End With
                    End If

                    '           right alignment vs. base on normal is now stored in
                    '           TrailUnderline level prop
                    iTrailUnderline = xGetLevelProp(xScheme, i,
                        mpNumLevelProps.mpNumLevelProp_TrailUnderline, mpSchemeTypes.mpSchemeType_Document)

                    '           set alignment if user chose 'based on Normal'
                    If ((pfP.Alignment = WdParagraphAlignment.wdAlignParagraphRight) And
                            (iTrailUnderline < 2)) Then
                        '               pre 9.8
                        pfP.Alignment = pfNormal.Alignment

                        '               flip bit
                        If bBitwisePropIsTrue(iTrailUnderline,
                                mpTrailUnderlineFields.mpTrailUnderlineField_Underline) Then
                            iAlignment = iAlignment Or
                                mpTrailUnderlineFields.mpTrailUnderlineField_Underline
                        End If
                        If bBitwisePropIsTrue(iTrailUnderline,
                                mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal) Then
                            iAlignment = iAlignment Or
                                mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal
                        End If
                        iAlignment = iAlignment Or mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal
                        lSetLevelProp(xScheme, i, mpNumLevelProps.mpNumLevelProp_TrailUnderline,
                            CStr(iAlignment), mpSchemeTypes.mpSchemeType_Document)
                    ElseIf bBitwisePropIsTrue(iTrailUnderline,
                            mpTrailUnderlineFields.mpTrailUnderlineField_AdjustToNormal) Then
                        '               post 9.8
                        pfP.Alignment = pfNormal.Alignment
                    End If

                    '           cont style
                    If Not (styCont Is Nothing) Then
                        With styCont.ParagraphFormat
                            If bBitwisePropIsTrue(iTrailUnderline,
                                    mpTrailUnderlineFields.mpTrailUnderlineField_AdjustContToNormal) Then
                                .Alignment = pfNormal.Alignment
                            End If
                        End With
                    End If

                    With pfP
                        If bAdjustSpacing Then
                            '                   set line spacing based on line spacing
                            '                   rule in Normal style of active doc
                            If pfNormal.LineSpacingRule = WdLineSpacing.wdLineSpaceExactly Then
                                sNormalSpacing = pfNormal.LineSpacing

                                '                       get current line spacing
                                iLineSpacing = .LineSpacingRule
                                bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                    (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                '                       convert to exact spacing
                                If Not bIsExact Then
                                    sLines = .LineSpacing / 12
                                    .LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                                    .LineSpacing = sNormalSpacing * sLines
                                End If

                                '                       adjust before/after in MP pleading style
                                If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                    If bIsMPPleadingStyle And
                                            (.SpaceAfter = 12) And
                                            (.SpaceBefore = 0) Then
                                        .SpaceAfter = 0
                                        .SpaceBefore = 12
                                    End If
                                End If

                                '                       cont style
                                If Not (styCont Is Nothing) Then
                                    With styCont.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule
                                        bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                            (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                        '                               convert to exact spacing
                                        If Not bIsExact Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If

                                        '                               adjust before/after in MP pleading style
                                        If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                            If bIsMPPleadingStyle And
                                                    (.SpaceAfter = 12) And
                                                    (.SpaceBefore = 0) Then
                                                .SpaceAfter = 0
                                                .SpaceBefore = 12
                                            End If
                                        End If
                                    End With
                                End If

                                '                       alternate cont style
                                If bIsAlt Then
                                    With styContAlt.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule
                                        bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                            (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                        '                               convert to exact spacing
                                        If Not bIsExact Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If

                                        '                               adjust before/after in MP pleading style
                                        If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                            If bIsMPPleadingStyle And
                                                    (.SpaceAfter = 12) And
                                                    (.SpaceBefore = 0) Then
                                                .SpaceAfter = 0
                                                .SpaceBefore = 12
                                            End If
                                        End If
                                    End With
                                End If
                            ElseIf pfNormal.LineSpacingRule = WdLineSpacing.wdLineSpaceMultiple Then
                                'GLOG5683 - added branch
                                sNormalSpacing = pfNormal.LineSpacing

                                '                       get current line spacing
                                iLineSpacing = .LineSpacingRule

                                '                       convert to multiple spacing
                                If (iLineSpacing = WdLineSpacing.wdLineSpaceSingle) Or
                                        (iLineSpacing = WdLineSpacing.wdLineSpace1pt5) Or
                                        (iLineSpacing = WdLineSpacing.wdLineSpaceDouble) Then
                                    sLines = .LineSpacing / 12
                                    .LineSpacingRule = WdLineSpacing.wdLineSpaceMultiple
                                    .LineSpacing = sNormalSpacing * sLines
                                End If

                                '                       cont style
                                If Not (styCont Is Nothing) Then
                                    With styCont.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule

                                        '                               convert to multiple spacing
                                        If (iLineSpacing = WdLineSpacing.wdLineSpaceSingle) Or
                                                (iLineSpacing = WdLineSpacing.wdLineSpace1pt5) Or
                                                (iLineSpacing = WdLineSpacing.wdLineSpaceDouble) Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceMultiple
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If
                                    End With
                                End If

                                '                       alternate cont style
                                If bIsAlt Then
                                    With styContAlt.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule

                                        '                               convert to multiple spacing
                                        If (iLineSpacing = WdLineSpacing.wdLineSpaceSingle) Or
                                                (iLineSpacing = WdLineSpacing.wdLineSpace1pt5) Or
                                                (iLineSpacing = WdLineSpacing.wdLineSpaceDouble) Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceMultiple
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If
                                    End With
                                End If
                            ElseIf pfNormal.LineSpacingRule = WdLineSpacing.wdLineSpaceAtLeast Then
                                'GLOG8625 - added branch
                                sNormalSpacing = pfNormal.LineSpacing

                                '                       get current line spacing
                                iLineSpacing = .LineSpacingRule
                                bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                    (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                '                       convert to exact spacing
                                If Not bIsExact Then
                                    sLines = .LineSpacing / 12
                                    .LineSpacingRule = WdLineSpacing.wdLineSpaceAtLeast
                                    .LineSpacing = sNormalSpacing * sLines
                                End If

                                '                       adjust before/after in MP pleading style
                                If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                    If bIsMPPleadingStyle And
                                            (.SpaceAfter = 12) And
                                            (.SpaceBefore = 0) Then
                                        .SpaceAfter = 0
                                        .SpaceBefore = 12
                                    End If
                                End If

                                '                       cont style
                                If Not (styCont Is Nothing) Then
                                    With styCont.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule
                                        bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                            (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                        '                               convert to exact spacing
                                        If Not bIsExact Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceAtLeast
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If

                                        '                               adjust before/after in MP pleading style
                                        If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                            If bIsMPPleadingStyle And
                                                    (.SpaceAfter = 12) And
                                                    (.SpaceBefore = 0) Then
                                                .SpaceAfter = 0
                                                .SpaceBefore = 12
                                            End If
                                        End If
                                    End With
                                End If

                                '                       alternate cont style
                                If bIsAlt Then
                                    With styContAlt.ParagraphFormat
                                        '                               get current line spacing
                                        iLineSpacing = .LineSpacingRule
                                        bIsExact = ((iLineSpacing = WdLineSpacing.wdLineSpaceAtLeast) Or
                                            (iLineSpacing = WdLineSpacing.wdLineSpaceExactly))

                                        '                               convert to exact spacing
                                        If Not bIsExact Then
                                            sLines = .LineSpacing / 12
                                            .LineSpacingRule = WdLineSpacing.wdLineSpaceAtLeast
                                            .LineSpacing = sNormalSpacing * sLines
                                        End If

                                        '                               adjust before/after in MP pleading style
                                        If iLineSpacing = WdLineSpacing.wdLineSpaceSingle Then
                                            If bIsMPPleadingStyle And
                                                    (.SpaceAfter = 12) And
                                                    (.SpaceBefore = 0) Then
                                                .SpaceAfter = 0
                                                .SpaceBefore = 12
                                            End If
                                        End If
                                    End With
                                End If
                            ElseIf pfNormal.LineSpacingRule =
                                     WdLineSpacing.wdLineSpaceSingle Then
                                '                       single/double - adjust only "pleading" schemes
                                If bIsMPPleadingStyle Then
                                    If (.LineSpacingRule = WdLineSpacing.wdLineSpaceSingle) And
                                            (.SpaceAfter = 0) And
                                            (.SpaceBefore = 12) Then
                                        .SpaceAfter = 12
                                        .SpaceBefore = 0
                                    End If

                                    '                           cont style
                                    If Not (styCont Is Nothing) Then
                                        With styCont.ParagraphFormat
                                            If (.LineSpacingRule = WdLineSpacing.wdLineSpaceSingle) And
                                                    (.SpaceAfter = 0) And
                                                    (.SpaceBefore = 12) Then
                                                .SpaceAfter = 12
                                                .SpaceBefore = 0
                                            End If
                                        End With
                                    End If

                                    '                           alternate cont style
                                    If bIsAlt Then
                                        With styContAlt.ParagraphFormat
                                            If (.LineSpacingRule = WdLineSpacing.wdLineSpaceSingle) And
                                                    (.SpaceAfter = 0) And
                                                    (.SpaceBefore = 12) Then
                                                .SpaceAfter = 12
                                                .SpaceBefore = 0
                                            End If
                                        End With
                                    End If
                                End If 'pleading style
                            End If 'line spacing rule
                        End If 'adjust

                        '               get alignment for num continued style
                        '               by getting the alignment of first left
                        '               or justified paragraph
                        If iNCAlignment = -1 Then
                            If .Alignment = WdParagraphAlignment.wdAlignParagraphLeft Or
                                    .Alignment = WdParagraphAlignment.wdAlignParagraphJustify Then
                                iNCAlignment = .Alignment
                            End If
                        End If
                    End With

                    '           restore subsequent level attributes
                    If Not (styNext Is Nothing) Then
                        With styNext.ParagraphFormat
                            .Alignment = lNextAlignment
                            .LineSpacingRule = lNextRule
                            .LineSpacing = sNextSpacing
                            .SpaceBefore = sNextBefore
                            .SpaceAfter = sNextAfter
                        End With
                    End If

                    '           cont style
                    If Not (styContNext Is Nothing) Then
                        With styContNext.ParagraphFormat
                            .Alignment = lContNextAlignment
                            .LineSpacingRule = lContNextRule
                            .LineSpacing = sContNextSpacing
                            .SpaceBefore = sContNextBefore
                            .SpaceAfter = sContNextAfter
                        End With
                    End If
                End If

                '       clear variables
                stySchemeL = Nothing
                styCont = Nothing
                styContAlt = Nothing
                styNext = Nothing
                styContNext = Nothing
                bIsAlt = False
            Next i

            '   format num continue style
            styCont = Nothing
            styCont = CurWordApp.ActiveDocument.Styles("Num Continue")

            If Not (styCont Is Nothing) Then
                With styCont.Font
                    .Name = styNormal.Font.Name
                    .Size = styNormal.Font.Size
                End With

                With styCont.ParagraphFormat
                    '           reset alignment if retrieved from scheme above
                    If iNCAlignment <> -1 Then _
                        .Alignment = iNCAlignment

                    '           set line spacing based on line spacing
                    '           rule in Normal style of active doc
                    .LineSpacingRule = pfBody.LineSpacingRule
                    .LineSpacing = pfBody.LineSpacing
                    .SpaceBefore = pfBody.SpaceBefore
                    .SpaceAfter = pfBody.SpaceAfter
                End With

                '       language
                styCont.LanguageID = lLanguage
            End If
        End Function

        Public Shared Function bInsertNumberFromToolbar(iLevel As Integer) As Long
            'inserts either gallery based number,
            'or native word style based number-
            'applies TOC scheme if none has yet been applied

            Dim xCurrentTOCScheme As String
            Dim xSelectedScheme As String
            Dim rngCurrent As Word.Range
            Dim rngInsertEnd As Word.Range
            Dim iNumLevels As Integer
            Dim xAlias As String
            Dim bShowAll As Boolean

            On Error GoTo bInsertNumberFromToolbar_Error

            '   turn on show all - this will prevent error when preceding paragraph is hidden
            With CurWordApp.ActiveWindow.View
                bShowAll = .ShowAll
                .ShowAll = True
            End With

            rngCurrent = CurWordApp.Selection.Range

            '   get scheme to be used
            xSelectedScheme = xActiveScheme(CurWordApp.ActiveDocument)

            '   level isn't defined
            iNumLevels = iGetLevels(xSelectedScheme, mpSchemeTypes.mpSchemeType_Document)
            If iLevel > iNumLevels Then
                xAlias = GetField(xSelectedScheme,
                                  mpRecordFields.mpRecField_Alias,
                                  mpSchemeTypes.mpSchemeType_Document,
                                  CurWordApp.ActiveDocument)
                If xAlias = "" Then _
                    xAlias = xSelectedScheme
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("Le th�me " & xAlias & " a seulement " & iNumLevels &
                        " niveaux. Pour ajouter des niveaux, cliquez le bouton th�me et selectionnez �diter du menu Th�me.", vbInformation, g_xAppName)
                Else
                    MsgBox("The " & xAlias & " scheme has only " & iNumLevels &
                        " levels.  To add levels, click the Schemes button and select" &
                        " Edit from the Scheme menu.", vbInformation, g_xAppName)
                End If
                Exit Function
            End If

            If xSelectedScheme = mpGalleryItem Then
                '       insert number from Gallery
                rngInsertEnd = rngInsertNumber(rngCurrent,
                                                   iLevel,
                                                   "",
                                                   False)

                If Not rngInsertEnd Is Nothing Then _
                    rngInsertEnd.Select()

            Else
                '       insert number using selected scheme
                'GLOG 2866 (11/7/11) - use alternate method for tables
                Dim bUseAltMethod As Boolean
                If CurWordApp.Selection.Information(WdInformation.wdWithInTable) And (xSelectedScheme <> "") Then
                    bUseAltMethod = (CurWordApp.Selection.Cells.Count > 1)
                End If
                If bUseAltMethod Then
                    rngInsertEnd = rngInsertNumberInTable(iLevel, xSelectedScheme)
                Else
                    rngInsertEnd = rngInsertNumber(rngCurrent,
                                                       iLevel,
                                                       xSelectedScheme,
                                                       False)
                End If

                If Not rngInsertEnd Is Nothing Then _
                    rngInsertEnd.Select()
            End If

            '   restore user's setting
            CurWordApp.ActiveWindow.View.ShowAll = bShowAll

            Exit Function

bInsertNumberFromToolbar_Error:
            Select Case Err.Number
                Case mpErrors.mpCollectionMemberDoesNotExist
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = xSelectedScheme & " n'est pas un th�me valide. Veuillez s�lectionner un th�me dans la barre d'outils."
                    Else
                        xMsg = xSelectedScheme & " is not a valid scheme.  " &
                            "Please select a scheme from the toolbar."
                    End If
                    MsgBox(xMsg, vbExclamation, g_xAppName)
                    Exit Function
                Case mpErrors.mpItemNameNotFound
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Th�mes Plaidoyer non disponibles dans ce document."
                    Else
                        xMsg = "Pleading Schemes are not available in this document."
                    End If
                    MsgBox(xMsg, vbExclamation, g_xAppName)
                    Exit Function
                Case Else
                    MsgBox(Err.Description)
            End Select
            bInsertNumberFromToolbar = Err.Number
            Exit Function
        End Function

        Public Shared Function bIsPrimaryParaNumber(fldField As Word.Field) As Boolean
            'returns TRUE if fldField
            'is first number of para
            'containing fldField

            Dim fldPrimaryNumber As Word.Field
            Dim rngField As Word.Range

            rngField = fldField.Code
            fldPrimaryNumber = fldGetPrimaryParaNumber(rngField)

            If rngField.ListFormat.ListType <> WdListType.wdListListNumOnly Then
                bIsPrimaryParaNumber = False
            ElseIf rngField.IsEqual(fldPrimaryNumber.Code) Then
                bIsPrimaryParaNumber = True
            End If
        End Function

        Public Shared Function iChangeScheme_NativeWord(rngScope As Word.Range,
                                          ByVal xScheme As String,
                                          ByVal bIsDoubleSpaced As Boolean,
                                          Optional ByVal xTargetScheme As String = "") _
                                          As Integer
            'changes each field in range rngScope
            'to LISTNUM of same list template,
            'next lower level number. Returns
            'number of promotions.

            Dim paraP As Paragraph
            Dim ltCurrent As ListTemplate
            Dim ltApplied As Word.ListTemplate
            Dim iListLevel As Integer
            Dim iTrailingChar As Integer
            Dim fldField As Word.Field
            Dim bReFormatTCHeading As Boolean
            Dim i As Integer
            Dim iNumParas As Integer
            Dim iStartAt As Integer
            Dim xCurParaScheme As String
            Dim rngLocation As Word.Range
            Dim xCurLT As String
            Dim xStyleRoot As String
            Dim styNumPara As Word.Style
            Dim xNextPara As String
            Dim iNewLevels As Integer
            Dim xNewCont As String
            Dim styCont As Word.Style
            Dim iCont As Integer
            Dim iPos As Integer
            Dim xStyle As String
            Dim rngNextPara As Range
            Dim bShowAll As Boolean
            Dim bHiddenBkmks As Boolean
            Dim lListValue As Long
            Dim oRange As Word.Range
            Dim styFootnote As Word.Style
            Dim xCont As String
            Dim k As Integer

            Try
                '   ensure that hidden paragraph marks are showing
                With CurWordApp.ActiveWindow.View
                    bShowAll = .ShowAll
                    .ShowAll = True
                End With

                '   ensure that hidden bookmarks are showing
                With CurWordApp.ActiveDocument.Bookmarks
                    bHiddenBkmks = .ShowHidden
                    .ShowHidden = True
                End With

                '   get style root
                xStyleRoot = xGetStyleRoot(xScheme)

                '   get number of levels in new scheme
                iNewLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)

                '   get how to format headings for below
                bReFormatTCHeading = GetUserSetting("Numbering",
                                                         "FormatHeadings")

                '   get number of numbers in rngScope
                iNumParas = rngScope.ListParagraphs.Count

                '   tag unnatural restarts
                If g_bPreserveRestarts Then
                    'cycle through numbered paragraphs - if value = 1, then
                    'see if the value changes when the list is continued from above -
                    'we use separate cycle because although the cycle below goes in
                    'reverse, this test is not always accurate when done during it
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Identification de liste.  Veuillez patienter..."
                    Else
                        CurWordApp.StatusBar = "Identifying list restarts.  Please wait..."
                    End If
                    EchoOff()
                    For i = 1 To iNumParas
                        paraP = rngScope.ListParagraphs(i)
                        oRange = paraP.Range
                        oRange.StartOf()
                        With oRange.ListFormat
                            lListValue = .ListValue
                            If lListValue = 1 Then
                                .ApplyListTemplate(.ListTemplate, True, WdListApplyTo.wdListApplyToSelection)
                                If .ListValue <> 1 Then
                                    oRange.Expand(WdUnits.wdParagraph)
                                    oRange.MoveEnd(WdUnits.wdCharacter, -1)
                                    oRange.InsertAfter(mpTag)
                                End If
                            End If
                        End With
                    Next i
                    i = 0
                End If

                '   change Native Word numbers
                For Each paraP In rngScope.ListParagraphs
                    With paraP
                        'GLOG 5196 - prevent non-numbered paragraphs with list nums from becoming
                        'numbered - this is essential now that we have para style paragraphs
                        If iGetListType(.Range) = mpListTypes.mpListTypeListNum Then
                            GoTo lblNextParagraph
                        End If

                        With .Range.ListFormat
                            '               skip bullets
                            If .ListType = WdListType.wdListBullet Then
                                GoTo lblNextParagraph
                            End If

                            '               if we're looking for only certain numbered paras,
                            '               get current para list template
                            If Len(xTargetScheme) Then
                                xCurLT = xGetLTRoot(.ListTemplate.Name)
                            End If
                        End With

                        '           do only if we're looking at all paras or if the
                        '           current para is numbered with the list template we're targeting
                        If xScheme <> "" And (xTargetScheme = "" Or
                                xTargetScheme = xCurLT) Then
                            '               remove existing, then insert new number
                            With .Range.ListFormat
                                'get level
                                iListLevel = .ListLevelNumber

                                'remove number
                                .RemoveNumbers(WdNumberType.wdNumberParagraph)
                            End With

                            rngInsertNumWordNum(.Range,
                                                xScheme,
                                                iListLevel,
                                                mpFunctionChangeScheme,
                                                bIsDoubleSpaced)

                            '***************************************************
                            'removed 1/6/00 to preserve directly applied attributes;
                            'now runs only if user wants to reformat headings (see below)
                            '                With .Range
                            '                    .ParagraphFormat.Reset
                            ''                   move past trailing characters
                            '                    .MoveStartWhile Chr(11) & Chr(32) & vbTab
                            '                    .Font.Reset
                            '                    .Expand wdParagraph
                            '                End With

                            'realized 4/2/01 that we should still be resetting font
                            'for style based levels
                            If iListLevel <= iNewLevels Then
                                If bTOCLevelIsStyleBased(xScheme, iListLevel) Then
                                    'GLOG 3716 - preserve footnote reference formatting
                                    If .Range.Footnotes.Count > 0 Then
                                        styFootnote = .Range.Footnotes(1).Reference.Style
                                    End If

                                    .Range.Font.Reset()

                                    'GLOG 3716 - restore footnote style
                                    For i = 1 To .Range.Footnotes.Count
                                        .Range.Footnotes(i).Reference.Style = styFootnote
                                    Next i
                                End If
                            End If
                            '***************************************************

                            '               change all fields in range
                            For Each fldField In .Range.Fields
                                '                   ensure it's a LISTNUM
                                If fldField.Type = WdFieldType.wdFieldListNum Then

                                    '                       get listnum specs
                                    bRet = bGetListNumInfo(fldField,
                                                          ltCurrent,
                                                          iListLevel,
                                                          iStartAt,
                                                          iTrailingChar)

                                    '                       get applied list template
                                    ltApplied = fldField.Code _
                                            .ListFormat.ListTemplate

                                    '                       change number to new list template
                                    bRet = bChangeListNum(fldField,
                                                          iListLevel,
                                                          ltApplied,
                                                          iStartAt,
                                                          iTrailingChar)

                                ElseIf fldField.Type = WdFieldType.wdFieldTOCEntry Then
                                    Try
                                        xCurParaScheme = xGetLTRoot(.Range.ListFormat.ListTemplate.Name)
                                    Catch
                                    End Try
                                    If bTOCLevelIsStyleBased(xCurParaScheme, iListLevel) Then
                                        '                           remove tc marking - format is removed
                                        '                           when style is applied above
                                        'GLOG 3547 - don't delete TC code
                                        '                            fldField.Delete
                                    Else
                                        If bReFormatTCHeading Then
                                            With .Range
                                                .ParagraphFormat.Reset()
                                                '                                   move past trailing characters
                                                .MoveStartWhile(Chr(11) & Chr(32) & vbTab)
                                                .Font.Reset()
                                                .Expand(WdUnits.wdParagraph)
                                            End With
                                            rngReFormatTCHeading(.Range, iListLevel)
                                        End If
                                    End If
                                End If
                            Next fldField

                            '               remove user-added shift-returns from left aligned paragraphs
                            'GLOG 5315 (9.9.5005) - ensure that shift-returns are actually user-added
                            If iListLevel <= iNewLevels Then '9.9.5011
                                iTrailingChar = xGetLevelProp(xScheme, iListLevel,
                                    mpNumLevelProps.mpNumLevelProp_TrailChr, mpSchemeTypes.mpSchemeType_Document)
                                If (iTrailingChar <> mpTrailingChars.mpTrailingChar_ShiftReturn) And
                                        (iTrailingChar <> mpTrailingChars.mpTrailingChar_DoubleShiftReturn) Then
                                    rngLocation = .Range
                                    With rngLocation
                                        If .ParagraphFormat.Alignment <>
                                                WdParagraphAlignment.wdAlignParagraphCenter Then
                                            With .Find
                                                .ClearFormatting()
                                                .Text = Chr(11)
                                                .Replacement.ClearFormatting()
                                                .Replacement.Text = " "
                                                .Execute(Replace:=WdReplace.wdReplaceAll)
                                            End With
                                        End If
                                    End With
                                End If
                            End If

                            'GLOG 5213 (9.9.5005) - the following code was moved out of the cycle - see below
                            ''               change only proprietary next para styles;
                            ''               we're now forcing cont styles to change to cont styles,
                            ''               regardless of how incoming numbering style is defined
                            '                Set rngNextPara = paraP.Range.Duplicate
                            '                With rngNextPara
                            '                    xNextPara = ""
                            '                    On Error Resume Next
                            '                    xNextPara = .Next(wdParagraph).Style
                            '                    On Error GoTo ProcError
                            '                    If xNextPara = "" Then
                            ''                       end of doc
                            '                        GoTo lblNextParagraph
                            '                    End If
                            '
                            '                    'GLOG 5163 (2/27/13) - added para styles to the loop to include
                            '                    'inadvertent standalone instances - the code was previously only
                            '                    'changing these when in split paragraphs
                            '                    While InStr(xNextPara, " Cont ") Or _
                            '                            InStr(xNextPara, "_Para") Or _
                            '                            InStr(xNextPara, " Para ")
                            '                        If InStr(xNextPara, " Para ") Then
                            '                            'get level
                            '                            iCont = Right(xNextPara, 1)
                            '
                            '                            'restyle
                            '                            .Next(wdParagraph).Style = GetSplitParaStyle(xScheme, iCont)
                            '                        Else
                            '                            'strip alias from style name
                            '                            xNextPara = StripStyleAlias(xNextPara)
                            '
                            '                            'get level
                            '                            iCont = Right(xNextPara, 1)
                            '
                            '                            If iCont > iNewLevels Then
                            '                                'restyle paragraph to Normal
                            '                                Set styCont = CurWordApp.ActiveDocument.Styles(wdStyleNormal)
                            '                            Else
                            '                                'get actual next paragraph style for level;
                            '                                'incoming scheme may use aliases for cont styles
                            '                                xStyle = xGetStyleName(xScheme, iCont)
                            '                                On Error Resume Next
                            '                                xNewCont = CurWordApp.ActiveDocument.Styles(xStyle).NextParagraphStyle
                            '                                On Error GoTo ProcError
                            '                                If InStr(xNewCont, " Cont ") = 0 Then
                            '                                    'not defined to be followed by a Cont style;
                            '                                    'use the standard one appropriate for the level
                            '                                    xNewCont = xStyleRoot & " Cont " & iCont
                            '                                End If
                            '
                            '                                'restyle
                            '                                Set styCont = Nothing
                            '                                On Error Resume Next
                            '                                Set styCont = CurWordApp.ActiveDocument.Styles(xNewCont)
                            '                                On Error GoTo ProcError
                            '                                If styCont Is Nothing Then
                            '                                    CreateContStyles xScheme, iCont
                            '                                End If
                            '                            End If
                            '                            .Next(wdParagraph).Style = styCont
                            '                        End If
                            '
                            '                        .Move wdParagraph
                            '                        xNextPara = ""
                            '                        On Error Resume Next
                            '                        xNextPara = .Next(wdParagraph).Style
                            '                        On Error GoTo ProcError
                            '                        If xNextPara = "" Then
                            ''                           end of doc
                            '                            GoTo lblNextParagraph
                            '                        End If
                            '                    Wend
                            '                End With
lblNextParagraph:
                        End If
                    End With

                    '       update status
                    i = i + 1
                    EchoOn()
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = Int(i / iNumParas * 100) &
                                                "% Compl�t�"
                    Else
                        CurWordApp.StatusBar = Int(i / iNumParas * 100) &
                                                "% Complete"
                    End If
                    EchoOff()
                Next paraP

                'GLOG 5213 (9.9.5005) - cont and para styles were previously changed while cycling
                'through numbered paragraphs, but that excluded instances not adjacent to numbering -
                'we'll now explicitly search for and replace each of these styles
                Dim iSchemes As Integer
                Dim j As Integer
                Dim oStyle As Word.Style
                Dim rngSearch As Word.Range
                iSchemes = iGetSchemes(g_xDSchemes, mpSchemeTypes.mpSchemeType_Document)
                For i = 1 To iSchemes
                    If g_xDSchemes(i - 1, 0) <> xScheme Then
                        'cont styles
                        For j = 1 To 9
                            oStyle = Nothing
                            xStyle = xGetStyleRoot(g_xDSchemes(i - 1, 0)) & " Cont " & CStr(j)
                            Try
                                oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
                            Catch
                            End Try
                            If Not oStyle Is Nothing Then
                                rngSearch = rngScope.Duplicate
                                With rngSearch.Find
                                    .ClearFormatting()
                                    .Format = True
                                    .Style = xStyle
                                    .Wrap = WdFindWrap.wdFindContinue
                                    .Execute()
                                    Do While .Found
                                        oRange = .Parent
                                        If (oRange.Start < rngScope.Start) Or (oRange.End > rngScope.End) Then
                                            'no longer in target range
                                            Exit Do
                                        Else
                                            If j > iNewLevels Then
                                                'restyle paragraph to Normal
                                                styCont = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
                                            Else
                                                'get actual next paragraph style for level;
                                                'incoming scheme may use aliases for cont styles
                                                xStyle = xGetStyleName(xScheme, j)
                                                Try
                                                    xNewCont = CurWordApp.ActiveDocument.Styles(xStyle).NextParagraphStyle.NameLocal
                                                Catch
                                                End Try
                                                If InStr(xNewCont, " Cont ") = 0 Then
                                                    'not defined to be followed by a Cont style;
                                                    'use the standard one appropriate for the level
                                                    xNewCont = xStyleRoot & " Cont " & j
                                                End If

                                                'restyle
                                                styCont = Nothing
                                                Try
                                                    styCont = CurWordApp.ActiveDocument.Styles(xNewCont)
                                                Catch
                                                End Try
                                                If styCont Is Nothing Then
                                                    If g_iLoadContStyles <> mpLoadContStyles.mpLoadIndividuallyOnDemand Then '9.9.6004
                                                        For k = 1 To iNewLevels
                                                            xCont = xStyleRoot & " Cont " & k
                                                            Try
                                                                styCont = CurWordApp.ActiveDocument.Styles(xCont)
                                                            Catch
                                                            End Try
                                                            If styCont Is Nothing Then
                                                                CreateContStyles(xScheme, k, , True)
                                                            End If
                                                            styCont = Nothing
                                                        Next k
                                                    Else
                                                        CreateContStyles(xScheme, j, , True)
                                                    End If
                                                    styCont = CurWordApp.ActiveDocument.Styles(xNewCont)
                                                End If
                                            End If
                                            oRange.Style = styCont
                                            .Execute()
                                        End If
                                    Loop
                                End With
                            End If
                        Next j

                        'para styles
                        For j = 1 To 9
                            oStyle = Nothing
                            xStyle = xGetStyleRoot(g_xDSchemes(i - 1, 0)) & " Para " & CStr(j)
                            Try
                                oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
                            Catch
                            End Try
                            If Not oStyle Is Nothing Then
                                rngSearch = rngScope.Duplicate
                                With rngSearch.Find
                                    .ClearFormatting()
                                    .Format = True
                                    .Style = xStyle
                                    .Wrap = WdFindWrap.wdFindContinue
                                    .Execute()
                                    Do While .Found
                                        oRange = .Parent
                                        If (oRange.Start < rngScope.Start) Or (oRange.End > rngScope.End) Then
                                            'no longer in target range
                                            Exit Do
                                        Else
                                            oRange.Style = GetSplitParaStyle(xScheme, j)
                                            .Execute()
                                        End If
                                    Loop
                                End With
                            End If
                        Next j
                    End If
                Next i

                '   restore restarts
                If g_bPreserveRestarts Then
                    'search for tags
                    With rngScope.Find
                        .ClearFormatting()
                        .Text = mpTag
                        .Execute()
                        While .Found
                            'delete tag and restart paragraph
                            oRange = .Parent
                            oRange.Text = ""
                            oRange.Expand(WdUnits.wdParagraph)
                            oRange.StartOf()
                            With oRange.ListFormat
                                .ApplyListTemplate(.ListTemplate, False)
                            End With
                            .Execute()
                        End While
                    End With
                End If
            Finally
                '   restore view
                CurWordApp.ActiveWindow.View.ShowAll = bShowAll
                CurWordApp.ActiveDocument.Bookmarks.ShowHidden = bHiddenBkmks

                EchoOn()
                CurWordApp.StatusBar = ""
                SendShiftKey()
            End Try
        End Function

        Public Shared Function iChangeLevel_ListNum(rngLocation As Word.Range,
                                      iLevelOffset As Integer) As Integer
            'changes each field  in range rngLocation
            'to LISTNUM of same list template,
            'next lower level number. Returns
            'number of promotions.

            Dim ltCurrent As ListTemplate
            Dim iListLevel As Integer
            Dim iTrailingChar As Integer
            Dim fldField As Word.Field
            Dim bPrimaryParaNumber As Boolean
            Dim iNewLevel As Integer
            Dim rngScope As Word.Range
            Dim rngHeading As Word.Range
            Dim bApplyTCEntryStyle As Boolean
            Dim iStartAt As Integer
            Dim rngField As Word.Range

            '   get how to format headings for below
            bApplyTCEntryStyle = GetUserSetting("Numbering",
                                                     "FormatHeadings")

            '   promote all fields in range
            For Each fldField In rngLocation.Fields
                '       ensure it's a LISTNUM
                If fldField.Type = WdFieldType.wdFieldListNum Then
                    '           get level and listtemplate
                    bRet = bGetListNumInfo(fldField,
                                          ltCurrent,
                                          iListLevel,
                                          iStartAt,
                                          iTrailingChar)

                    '           ensure that new level is between 1 and 9
                    iNewLevel = mpMax(mpMin(iListLevel + iLevelOffset, 9), 1)

                    '           change level
                    bRet = bChangeListNum(fldField,
                                         iNewLevel,
                                         ltCurrent,
                                         iStartAt,
                                         iTrailingChar)

                End If
            Next fldField
        End Function


        Public Shared Function iChangeStartAt_ListNum(rngLocation As Word.Range,
                                        iNewStartAt As Integer) As Integer
            'changes start at for each
            'field in range rngLocation

            Dim ltCurrent As ListTemplate
            Dim iListLevel As Integer
            Dim fldField As Word.Field
            Dim iStartAt As Integer
            Dim vStartAt As Object

            '   if iNewStartAt = mpContinueFromPrevious then remove
            '   restart switch, else add restart switch
            If iNewStartAt = mpContinueFromPrevious Then
                '       remove restart switch
                For Each fldField In rngLocation.Fields
                    bRet = bRemoveRestartSwitch(fldField)
                Next fldField
            Else
                '       change all fields in range
                For Each fldField In rngLocation.Fields
                    '           ensure it's a LISTNUM
                    If fldField.Type = WdFieldType.wdFieldListNum Then
                        '               get level and listtemplate
                        bGetListNumInfo(fldField,
                                        ltCurrent,
                                        iListLevel,
                                        iStartAt)

                        '              change start at
                        bChangeListNumCode(fldField,
                                           iListLevel,
                                           1)
                    End If
                Next fldField
            End If
        End Function

        Public Shared Function iChangeStartAt_NativeWord(rngScope As Word.Range,
                                           iStartAt As Integer) As Integer
            'can only restart at 1 because
            'of Word bug when styles are
            'attached to list levels
            Dim bContinue As Boolean
            Dim xScheme As String
            Dim xStyIndents() As String
            Dim sLeft As Single
            Dim sFirst As Single
            Dim rngLocation As Word.Range

            rngLocation = rngScope.Duplicate
            'in Word 2007, restarting doesn't work
            'when the entire paragraph is selected
            rngLocation.StartOf()

            With rngLocation.ListFormat
                If iStartAt = mpContinueFromPrevious Then
                    bContinue = True
                Else
                    bContinue = False
                End If

                ''       when the list template is reapplied,
                ''       the scheme is effectively relinked;
                ''       so preserve indents for both scheme and restarted paragraph
                '        xScheme = xGetLTRoot(.ListTemplate.Name)
                '        If xScheme <> "" Then _
                '            StoreIndents xScheme, xStyIndents()
                '        With rngScope.ParagraphFormat
                '            sLeft = .LeftIndent
                '            sFirst = .FirstLineIndent
                '        End With

                '       restart/continue
                .ApplyListTemplate(ListTemplate:= .ListTemplate,
                                   ContinuePreviousList:=bContinue)

                ''       restore indents
                '        If xScheme <> "" Then _
                '            RestoreIndents xStyIndents()
                '
                ''       restore indents in restarted paragraph
                '        With rngScope.ParagraphFormat
                '            .LeftIndent = sLeft
                '            .FirstLineIndent = sFirst
                '        End With
            End With

        End Function

        Public Shared Function iChangeLevel_NativeWord(rngLocation As Word.Range,
                                         iOffset As Integer) As Integer
            'changes paragraph level of each
            'para in rngLocation based on offset

            Dim iCurLevel As Integer
            Dim iNewLevel As Integer
            Dim paraP As Word.Paragraph
            Dim xStyle As String
            Dim xStyleRoot As String
            Dim styNew As Word.Style
            Dim xNewStyle As String
            Dim xScheme As String
            Dim ltP As Word.ListTemplate
            Dim iLevels As Integer
            Dim bIsHScheme As Boolean
            Dim bReformatTC As Boolean
            Dim bReformatPara As Boolean
            Dim rngPara As Word.Range
            Dim iPos As Integer
            Dim iFormat As Integer

            '   change all numbered paras in range
            For Each paraP In rngLocation.ListParagraphs
                With paraP.Range.ListFormat
                    '           do to only outline numbers
                    '            If .ListType = wdListOutlineNumbering Then
                    If .ListTemplate.ListLevels.Count = 9 Then
                        '            NOTE: Changed the above conditional, because .ListType is
                        '            so unreliable.  For example, if any simple list was ever in
                        '            document, Word 2000 will forever return level two list type
                        '            as wdListSimpleNumbering.  The listlevels.count test includes
                        '            too much, so this procedure now contains subsequent tests for
                        '            inappropriate items, e.g. where new level has no linked style.
                        '               if object is not valid error (5825)
                        '               is generated while gettting list
                        '               template info, try relinking schemes
                        On Error Resume Next
                        ltP = .ListTemplate
                        xScheme = xGetLTRoot(ltP.Name)

                        If Err.Number = 5825 Then
                            On Error GoTo 0

                            bRelinkSchemes(mpSchemeTypes.mpSchemeType_Document)
                            ltP = .ListTemplate
                            xScheme = xGetLTRoot(ltP.Name)
                        End If
                        On Error GoTo 0

                        '               this will return nine levels if
                        '               xScheme is not a MacPac scheme
                        iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)

                        '               get level
                        iCurLevel = .ListLevelNumber

                        '               ensure that new level is between 1 and 9
                        iNewLevel = mpMax(
                            mpMin(iCurLevel + iOffset, CDbl(iLevels)), 1)

                        If iNewLevel = iCurLevel Then
                            Exit Function
                        End If

                        '               ensure that style for new level exists -
                        xNewStyle = ltP.ListLevels(iNewLevel).LinkedStyle

                        '               relink scheme if necessary
                        '                If IsMacPacScheme(xScheme, mpSchemeTypes.mpSchemeType_Document) And _
                        '                        Len(xNewStyle) = 0 Then
                        If bIsMPListTemplate(ltP) And (Len(xNewStyle) = 0) Then
                            bIsHScheme = bIsHeadingScheme(xScheme)
                            '                    If bSchemeIsUnlinked(xScheme) Then
                            bRelinkScheme(xScheme,
                                          mpSchemeTypes.mpSchemeType_Document,
                                          bIsHScheme, False)
                            '                    End If
                            '                   get style name
                            xNewStyle = ltP.ListLevels(iNewLevel).LinkedStyle
                        End If

                        '               get style
                        On Error Resume Next
                        styNew = CurWordApp.ActiveDocument.Styles(xNewStyle)
                        On Error GoTo 0

                        '               if style exists...
                        If (Not (styNew Is Nothing)) Then
                            '                   the following conditional is necessary because
                            '                   user may have used native Word to demote to a
                            '                   non-existent level
                            'GLOG 5124 - remmed the following block - the reapplication of
                            'direct formatting was buggy and inconsistent, partially due to
                            'bugs in bIsHeadingFormatted - according to Linda, we should never
                            'apply direct formatting the new level
                            '                    If iCurLevel <= iLevels Then
                            ''                       reformat heading if current heading
                            ''                       has been marked and formatted
                            '                        bReformatTC = bIsMarkedAndFormatted(paraP)
                            '
                            ''                       may be formatted but not marked
                            '                        If Not bReformatTC And _
                            '                                (Not bTOCLevelIsStyleBased(xScheme, _
                            '                                iCurLevel)) Then
                            '                            bReformatPara = bIsHeadingFormatted(paraP)
                            '                        End If
                            '                    End If

                            '                   set level
                            .ListLevelNumber = iNewLevel

                            ''                   this is to deal with situation where a restarted paragraph
                            ''                   is promoted/demoted to a level in which indents are out of sync;
                            ''                   starting with 9.7.0, this will be more common
                            '                    With paraP.Range.ParagraphFormat
                            '                        .LeftIndent = styNew.ParagraphFormat.LeftIndent
                            '                        .FirstLineIndent = styNew.ParagraphFormat.FirstLineIndent
                            '                    End With

                            '                   insert trailing chr 11 when llCurrent
                            '                   has no trailing character
                            '                    If ltP.ListLevels(iNewLevel).NumberFormat <> "" Then
                            rngEditTrailChr11(paraP.Range,
                                .ListTemplate.ListLevels(iNewLevel))
                            '                    End If

                            '                   reformat if specified
                            If bReformatTC Then
                                rngReFormatTCHeading(paraP.Range,
                                                     iNewLevel)
                            ElseIf bReformatPara And
                                    (Not bTOCLevelIsStyleBased(xScheme,
                                    iNewLevel)) Then
                                rngPara = paraP.Range
                                With rngPara
                                    iPos = InStr(.Text, ".  ")
                                    If iPos Then
                                        .StartOf()
                                        .MoveEnd(WdUnits.wdCharacter, iPos)
                                    End If
                                End With
                                iFormat = xGetLevelProp(xScheme,
                                                        iNewLevel,
                                                        mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                                        mpSchemeTypes.mpSchemeType_Document)
                                lApplyFontFormat(rngPara, iFormat)
                            End If
                        ElseIf IsMacPacScheme(xScheme, mpSchemeTypes.mpSchemeType_Document) Then
                            '                   there's a problem here - the style is not in the doc
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                xMsg = "Impossible de changer le niveau de ce paragraphe.  Le style " & xNewStyle & " n'est pas dans ce document.  Essayez de r�initialiser le th�me."
                            Else
                                xMsg = "Could not change the level of this paragraph.  " &
                                       "The style " & xNewStyle & " is not in the document.  " &
                                       "Try resetting the scheme."
                            End If
                            MsgBox(xMsg, vbExclamation, g_xAppName)
                        Else
                            '                   set level
                            .ListLevelNumber = iNewLevel
                        End If
                    End If
                End With
            Next paraP
        End Function

        Public Shared Function bSetFormatHeadingsType(ByVal bUseTCEntryStyleDef As Boolean) As Boolean
            CurWordApp.System.PrivateProfileString _
                (g_xUserIni, "Numbering", "FormatHeadings") = bUseTCEntryStyleDef
        End Function

        Public Shared Function iGetListTemplateNames(docDoc As Word.Document, xArray() As String) As Integer
            'fills xArray with names of the
            'list templates stored in docDoc

            Dim ltitem As ListTemplate
            Dim iNumNamedListTemplates As Integer
            ReDim xArray(0)

            '   loop through list templates,
            '   adding named ones to xArray()
            For Each ltitem In docDoc.ListTemplates
                If ltitem.Name <> "" Then
                    xArray(UBound(xArray)) = ltitem.Name
                    ReDim Preserve xArray(UBound(xArray) + 1)
                    iNumNamedListTemplates = iNumNamedListTemplates + 1
                End If
            Next ltitem

            '   redim to one lower, min 0
            ReDim Preserve xArray(mpMax(UBound(xArray) - 1, 0))

            '   return number of named templates
            iGetListTemplateNames = iNumNamedListTemplates
        End Function

        Public Shared Function rngInsertNumListNum(iLevel As Integer,
                                     rngPointer As Word.Range,
                                     ltCur As ListTemplate) As Word.Range
            '   inserts a listnum field of level
            '   ilevel based on list template
            '   ltCur at location
            '   rngPointer

            Dim fldListNum As Word.Field
            Dim llCur As ListLevel
            Dim xTrailingChar As String
            Dim rngListNum As Word.Range
            Dim xListNumText As String
            Dim xScheme As String

            On Error GoTo rngInsertNumListNum_Error

            '   insert appropriate LISTNUM field
            'GLOG 5195 - use supplied ltCur, since rngPointer may now be para style
            If ltCur Is Nothing Then _
                    ltCur = rngPointer.ListFormat.ListTemplate
            xListNumText = " \l" & iLevel

            '   get trailing characters (this block added 3/16/01)
            If ltCur.Name <> "" Then
                If bIsMPListTemplate(ltCur) Then
                    xScheme = xGetLTRoot(ltCur.Name)
                    'GLOG 5156 - don't attempt to get trailing char for non-existent level
                    If iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document) >= iLevel Then
                        xTrailingChar = xGetTrailChrs(xGetLevelProp(xScheme,
                            iLevel, mpNumLevelProps.mpNumLevelProp_TrailChr,
                            mpSchemeTypes.mpSchemeType_Document))
                        With rngPointer
                            '               if not immediately following primary number,
                            '               follow list num with a space
                            If .Start - Len(xTrailingChar) >
                                    .Paragraphs(1).Range.Start Then
                                xTrailingChar = " "
                            End If
                        End With
                    End If
                End If
            End If

            If ltCur.ListLevels(iLevel).NumberFormat <> "" Then
                fldListNum = CurWordApp.ActiveDocument.Fields.Add(Range:=rngPointer,
                                                           Type:=WdFieldType.wdFieldListNum,
                                                           Text:=xListNumText,
                                                           PreserveFormatting:=False)

                '       get field range
                rngListNum = rngGetField(fldListNum.Code)

                '       get/set format for LISTNUM field
                llCur = ltCur.ListLevels(iLevel)

                With rngListNum
                    '           apply appropriate font
                    '           formats to number
                    .Font = llCur.Font

                    '           remove inadvertent shading (Word 2K)
                    With .Shading
                        .ForegroundPatternColorIndex = WdColorIndex.wdNoHighlight
                        .BackgroundPatternColorIndex = WdColorIndex.wdNoHighlight
                    End With

                    'GLOG 5212 - in Word 2007 only, whole paragraph gets
                    'shaded black when in content control
                    With .Paragraphs(1).Shading
                        If .BackgroundPatternColorIndex = WdColorIndex.wdBlack Then _
                            .BackgroundPatternColorIndex = WdColorIndex.wdAuto
                    End With

                    .EndOf()

                    '           insert trailing character
                    '            xTrailingChar = xGetTrailingChar(llCur.TrailingCharacter)
                    If xTrailingChar = "" Then _
                        xTrailingChar = " "
                    .InsertAfter(xTrailingChar)

                    .Characters.Last.Font.Reset()
                    .EndOf()

                End With

                '       return
                rngInsertNumListNum = rngListNum
            End If
            Exit Function

rngInsertNumListNum_Error:
            Select Case Err.Number
                Case Else
                    MsgBox(Err.Description)
            End Select
            Exit Function
        End Function

        Public Shared Function bChangeListNum(fldListNum As Word.Field,
                               iListLevel As Integer,
                               ltCurrent As ListTemplate,
                               iStartAt As Integer,
                               Optional iTrailingChar As Integer = WdTrailingCharacter.wdTrailingTab) As Boolean

            'edits/formats field fldListNum to
            'supplied specifications - modifies
            'para as necessary

            Dim llCurrent As ListLevel
            Dim rngListNum As Word.Range
            Dim xTrailChr As String
            Dim rngCurTrailChr As Word.Range

            On Error GoTo bChangeListNum_Error

            '   convert trailing char integer to string
            xTrailChr = xGetTrailingChar(iTrailingChar)

            '   edit code text
            bRet = bChangeListNumCode(fldListNum,
                                      iListLevel,
                                      iStartAt)

            '   get the range of the field
            rngListNum = rngGetField(fldListNum.Code)

            If Not ltCurrent Is Nothing Then
                '       get/set format for LISTNUM field
                llCurrent = ltCurrent.ListLevels(iListLevel)
                If llCurrent.NumberFormat <> "" Then
                    With rngListNum
                        '               apply font to number
                        .Font = llCurrent.Font

                        '               remove inadvertent shading (Word 2K)
                        With .Shading
                            .ForegroundPatternColorIndex = WdColorIndex.wdNoHighlight
                            .BackgroundPatternColorIndex = WdColorIndex.wdNoHighlight
                        End With

                        '               get current trailing char
                        rngCurTrailChr = CurWordApp.ActiveDocument.Range(.End, .End + 1)

                        ''               if the current trailing character is
                        ''               the correct character for the original number,
                        ''               delete and insert a new trailing char, else
                        ''               just insert a new trailing char
                        '                If rngCurTrailChr = xTrailChr Then _
                        '                    rngCurTrailChr.Delete

                        '               delete existing trailing character
                        With rngCurTrailChr
                            If (.Text = " ") Or
                                    (.Text = vbTab) Or
                                    (.Text = Chr(11)) Then
                                If (.Next(WdUnits.wdCharacter).Text = " ") Or
                                        (.Next(WdUnits.wdCharacter).Text = vbTab) Or
                                        (.Next(WdUnits.wdCharacter).Text = Chr(11)) Then
                                    .MoveEnd(WdUnits.wdCharacter)
                                End If
                                .Delete()
                            End If
                        End With

                        '               if not immediately following primary number,
                        '               follow list num with a space
                        xTrailChr = xGetTrailingChar(llCurrent.TrailingCharacter)
                        If .Start - Len(xTrailChr) >
                                .Paragraphs(1).Range.Start Then
                            xTrailChr = " "
                        End If

                        .InsertAfter(xTrailChr)
                    End With
                Else
                    With rngListNum
                        '               get current trailing char
                        rngCurTrailChr = CurWordApp.ActiveDocument.Range(.End, .End + 1)

                        '               delete trailing char if it exists
                        '                If InStr(" " & vbTab & String(2, 11), rngCurTrailChr) Then
                        '                    rngCurTrailChr.Delete
                        '                End If
                        With rngCurTrailChr
                            If (.Text = " ") Or
                                    (.Text = vbTab) Or
                                    (.Text = Chr(11)) Then
                                If (.Next(WdUnits.wdCharacter).Text = " ") Or
                                        (.Next(WdUnits.wdCharacter).Text = vbTab) Or
                                        (.Next(WdUnits.wdCharacter).Text = Chr(11)) Then
                                    .MoveEnd(WdUnits.wdCharacter)
                                End If
                                .Delete()
                            End If
                        End With

                        '               delete number
                        rngListNum.Delete()
                    End With
                End If
            End If
            Exit Function

bChangeListNum_Error:
            Select Case Err.Number
                Case Else
                    MsgBox(Err.Description)
            End Select
            Exit Function

Err_StyleDoesNotExist:
            '   set to normal style
            rngListNum.Style = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)
            Resume Next
        End Function


        Public Shared Function rngInsertNumber(rngScope As Word.Range,
                               iListLevel As Integer,
                               xScheme As String,
                               Optional bAsListNum As Boolean = True) As Word.Range
            'inserts a number of level iListLevel
            'from list template xScheme
            'at the current selection

            Dim rngParaCurrent As Word.Range
            Dim bIsNumbered As Boolean
            Dim ltCurrent As ListTemplate
            Dim iUserChoice As Integer
            Dim paraExisting As Paragraph
            Dim rngInsertion As Word.Range
            Dim bMultipleParagraphs As Boolean
            Dim iListType As Integer
            Dim iNumParas As Integer
            Dim i As Integer
            Dim iNumNumbers As Integer
            Dim sngPercent As Single
            Dim bDlgAlreadyShown As Boolean

            '   use as percent complete denominator
            iNumParas = rngScope.Paragraphs.Count

            '   force  number to be level 1-9
            iListLevel = mpMax(mpMin(CDbl(iListLevel), 9), 1)

            bMultipleParagraphs = rngScope.Paragraphs.Count > 1

            For Each paraExisting In rngScope.Paragraphs
                '       if multiple paragraphs in scope,
                '       insert at start of paragraph
                '       else at insertion, if possible
                If bMultipleParagraphs Then
                    rngInsertion = paraExisting.Range
                    rngInsertion.StartOf()
                Else
                    rngInsertion = rngScope
                End If

                iListType = iGetListType(rngInsertion)

                bIsNumbered = (iListType = mpListTypes.mpListTypeNative Or
                              iListType = mpListTypes.mpListTypeMixed)

                'GLOG 5195 - behavior should be the same in partner para style
                'paragraph as it is in numbered paragraph
                If bIsNumbered Then
                    ltCurrent = rngInsertion.ListFormat.ListTemplate
                Else
                    rngParaCurrent = rngInsertion.Duplicate
                    If rngParaCurrent.Paragraphs(1).Range.Start > 0 Then
                        rngParaCurrent = rngParaCurrent.Previous(WdUnits.wdParagraph)
                        If rngParaCurrent.Paragraphs.First.IsStyleSeparator Then
                            iListType = iGetListType(rngParaCurrent)
                            bIsNumbered = (iListType = mpListTypes.mpListTypeNative Or
                                iListType = mpListTypes.mpListTypeMixed)
                            If bIsNumbered Then
                                ltCurrent = rngParaCurrent.ListFormat.ListTemplate
                            End If
                        End If
                    End If
                End If

                '       if numbered insert LISTNUM,
                '       else insert Native Number
                If bIsNumbered Then
                    '           insert as LISTNUM field, if selection is IP
                    If CurWordApp.Selection.Type = WdSelectionType.wdSelectionIP Then
                        rngInsertNumber = rngInsertNumListNum _
                                                        (iListLevel,
                                                         rngInsertion,
                                                         ltCurrent)
                    End If
                Else
                    '           use native word numbering
                    If xScheme = "" Then
                        '               "Gallery.." position selected or selected
                        '               list template does not exist - use gallery
                        With CurWordApp.Dialogs(WdWordDialog.wdDialogFormatBulletsAndNumbering)
                            .DefaultTab = WdWordDialogTab.wdDialogFormatBulletsAndNumberingTabOutlineNumbered

                            '                   show dlg only once during loop- use
                            '                   values from first showing
                            If bDlgAlreadyShown Then
                                .Execute()
                            Else
                                .Show()
                                bDlgAlreadyShown = True
                            End If
                            With rngScope.ListFormat
                                If .ListType = WdListType.wdListOutlineNumbering Then
                                    .ListLevelNumber = iListLevel
                                Else
                                    If .ListType <> WdListType.wdListNoNumbering Then
                                        CurWordApp.ActiveDocument.Undo()
                                        CurWordApp.ScreenRefresh()
                                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                            xMsg = "Seulement des num�ros de num�rotation hi�rarchis�e peuvent �tre ins�r�s avec la barre d'outils num�rotation TSG.  Cliquez sur Format/Puces et num�ros pour ins�rer d'autres types de num�ros."
                                        Else
                                            xMsg = "Only Outline Numbered numbers may be inserted " &
                                                   "using the TSG numbering toolbar.  Click Format" &
                                                   "/Bullets and Numbering to insert other types of numbers."
                                        End If
                                        MsgBox(xMsg, vbExclamation, g_xAppName)
                                    End If
                                    Exit For
                                End If
                            End With
                        End With
                    Else
                        '               insert Native word number from defined scheme
                        rngInsertNumber = rngInsertNumWordNum(rngInsertion,
                                                         xScheme,
                                                         iListLevel)
                    End If
                End If

                '       update status
                i = i + 1

                '       4/2/01 - removed the SendKeys command at the end of this
                '       function, because it seemed to be causing problems with the
                '       ALT key and NumLock - this means we can't use the status bar
                '        Application.StatusBar = _
                '            Int((i / iNumParas) * 100) & "% Complete"
            Next paraExisting

            '    Application.StatusBar = ""
            '    mdlApplication.SendShiftKey
        End Function

        Public Shared Function bRestartAt(iStartAt As Integer) As Integer
            'restarts the numbered paragraph at iListLevel
            Dim rngCurrent As Word.Range
            Dim ltCurrent As ListTemplate
            Dim iListLevel As Integer

            rngCurrent = CurWordApp.Selection.Range
            ltCurrent = rngCurrent.ListFormat.ListTemplate
            iListLevel = rngCurrent.ListFormat.ListLevelNumber

            rngCurrent.ListFormat.RemoveNumbers()

            rngCurrent.ListFormat.ApplyListTemplate(ltCurrent, False, WdListApplyTo.wdListApplyToSelection)
            ltCurrent = rngCurrent.ListFormat.ListTemplate

            ltCurrent.ListLevels(iListLevel).StartAt = iStartAt
        End Function

        Public Shared Function bGetListNumInfo(fldListNum As Word.Field,
                                ByRef ltCurrent As ListTemplate,
                                ByRef iListLevel As Integer,
                                ByRef iStartAt As Integer,
                                Optional ByRef vTrailChr As Object = Nothing) As Boolean

            '   gets the list template and level
            '   of LISTNUM field fldListNum
            'GLOG 8763 (dm) - arguments changed to byref
            Dim xTemp As String
            Dim iFirstSwitchPos As Integer
            Dim bViewFieldCodesStart As Boolean
            Dim rngFind As Word.Range
            Dim iLevelSwitchPos As Integer
            Dim iStartAtSwitchPos As Integer
            Dim i As Integer
            Dim xTest As String

            CurWordApp.ScreenUpdating = False

            With CurWordApp.ActiveWindow.View 'GLOG 8763 (dm)
                bViewFieldCodesStart = .ShowFieldCodes
                .ShowFieldCodes = True
            End With

            '   get List Template name
            ltCurrent = fldListNum.Code.ListFormat.ListTemplate

            '   get listnum switch text
            '    xTemp = Mid(fldListNum.Code.Text, 9)
            xTemp = Mid(fldListNum.Code.Text, Len(g_xListNum) + 2)

            '   ensure that there's some
            '   string to evaluate
            If Len(xTemp) > 0 Then
                iFirstSwitchPos = InStr(xTemp, "\")
                If iFirstSwitchPos <> 0 Then
                    '           find level switch and, if found, return
                    '           character two positions to the right
                    iLevelSwitchPos = InStr(UCase(xTemp), "\L")
                    If iLevelSwitchPos <> 0 Then
                        '               the following is necessary because there can be
                        '               any number of spaces between slash and number
                        xTest = Left(LTrim(Mid(xTemp,
                            iLevelSwitchPos + 2)), 1)
                        If IsNumeric(xTest) Then _
                            iListLevel = xTest
                    End If

                    '           unable to determine level - set to 1
                    If iListLevel = 0 Then _
                        iListLevel = 1

                    '           find start at switch and, if found, return
                    '           character two positions to the right
                    iStartAtSwitchPos = InStr(UCase(xTemp), "\S")
                    If iStartAtSwitchPos <> 0 Then
                        '               the following is necessary because there can be
                        '               any number of spaces between slash and number (GLOG 4501)
                        xTest = Left(LTrim(Mid(xTemp,
                            iStartAtSwitchPos + 2)), 1)
                        If IsNumeric(xTest) Then _
                            iStartAt = xTest
                    Else
                        iStartAt = mpListNumNoStartAt
                    End If
                End If

                '       get trailing char for number if requested
                'GLOG 5196 - this function is now sometimes called in non-numbered paragraph
                If (Not vTrailChr Is Nothing) And (Not ltCurrent Is Nothing) Then
                    Dim llCurrent As ListLevel
                    llCurrent = ltCurrent.ListLevels(iListLevel)
                    vTrailChr = llCurrent.TrailingCharacter
                End If

            Else
                ltCurrent = Nothing
            End If

            CurWordApp.ActiveWindow.View.ShowFieldCodes = bViewFieldCodesStart 'GLOG 8763 (dm)
        End Function


        Public Shared Function fldGetPrimaryParaNumber(rngLocation As Word.Range) As Word.Field
            'returns the first para number
            'of first para containing rngLocation
            Dim fldField As Word.Field

            For Each fldField In rngLocation.Paragraphs(1).Range.Fields
                If fldField.Type = WdFieldType.wdFieldListNum Then
                    fldGetPrimaryParaNumber = fldField
                    Exit For
                End If
            Next fldField

        End Function


        Public Shared Function xActiveScheme(docDoc As Word.Document) As String
            'gets doc var that designates
            'the scheme to use for number
            'insertion

            Dim varScheme As Variable

            '   test for existence of var
            On Error Resume Next
            varScheme = docDoc.Variables(mpActiveSchemeDocVar)

            xActiveScheme = Mid(xTrimSpaces(varScheme.Value), 2)

        End Function
        Public Shared Function xGetTrailingChar(ByVal iTrailingChar As Integer) As String
            'returns the string char
            'designated by iTrailingChar -
            'you can use any case to substitute
            'a character for the iTrailingChar
            'constant - eg designate that
            'wdTrailingSpace is chr(11).

            Select Case iTrailingChar
                Case WdTrailingCharacter.wdTrailingSpace
                    xGetTrailingChar = " "
                Case WdTrailingCharacter.wdTrailingTab
                    xGetTrailingChar = vbTab
                Case WdTrailingCharacter.wdTrailingNone
                    xGetTrailingChar = Chr(11)
            End Select

        End Function

        Public Shared Function iRelinkAllListTemplateStyles() As Integer
            'relinks numbering styles to list levels for
            'every MacPac scheme in document;
            'returns # of schemes relinked, or
            '-1 if error occurred
            Dim ltList As Word.ListTemplate
            Dim xScheme As String
            Dim xStyle As String
            Dim xStyleRoot As String
            Dim styScheme As Word.Style
            Dim i As Integer
            Dim iNumSchemes As Integer

            iRelinkAllListTemplateStyles = -1

            For Each ltList In CurWordApp.ActiveDocument.ListTemplates
                If ltList.Name = "" Then GoTo nextListTemplate
                xScheme = xGetLTRoot(ltList.Name)
                xStyleRoot = xGetStyleRoot(xScheme)
                For i = 1 To 9
                    xStyle = xStyleRoot & "_L" & i

                    On Error Resume Next
                    styScheme = CurWordApp.ActiveDocument.Styles(xStyle)
                    On Error GoTo 0

                    If styScheme Is Nothing Then
                        i = i - 1
                        Exit For
                    Else
                        styScheme = Nothing
                    End If
                Next i

                If i > 0 Then
                    iNumSchemes = iNumSchemes + 1
                    RelinkPreserveIndents(CurWordApp.ActiveDocument, ltList, xScheme, 1, i)
                End If

nextListTemplate:
            Next ltList

            iRelinkAllListTemplateStyles = iNumSchemes
        End Function

        Public Shared Function rngReFormatTCHeading(rngLocation As Word.Range,
                                      iNewLevel As Integer) As Word.Range
            '   reformats TC Heading of para
            '   containing range rngLocation

            Dim rngPara As Word.Range
            Dim rngField As Word.Range
            Dim fldField As Word.Field
            Dim bAutoFormat As Boolean

            rngPara = rngLocation.Paragraphs(1).Range

            For Each fldField In rngPara.Fields
                With fldField
                    If .Type = WdFieldType.wdFieldTOCEntry Then
                        rngField = rngGetField(.Code)
                        rngReFormatTCHeading =
                            rngFormatTCHeading(rngField,
                                               iNewLevel,
                                               True)
                        Exit For
                    End If
                End With
            Next fldField
        End Function

        Public Shared Function rngReapplyFont(rngLocation As Word.Range,
                                oFont As Word.Font) As Word.Range
            'reformats range to stored font attributes
            With oFont
                rngLocation.Font.Bold = .Bold
                rngLocation.Font.Italic = .Italic
                '       avoid the notorious Word 97 toggle effect
                If rngLocation.Font.AllCaps <> .AllCaps Then _
                    rngLocation.Font.AllCaps = .AllCaps
                rngLocation.Font.SmallCaps = .SmallCaps
                rngLocation.Font.Underline = .Underline
            End With
        End Function

        Public Shared Function rngEditTrailChr11(rngCur As Word.Range,
                                   llCur As ListLevel,
                                   Optional bIsDoubleSpaced As Boolean = False) _
                                   As Word.Range
            'inserts chr(11) after number if
            'trailing character for llCur
            'is defined as "Nothing" - for
            'Native Numbering ONLY

            Dim rngParaStart As Word.Range
            Dim xSubChr As String
            Dim rngTrailingChars As Word.Range
            Dim xCurrentTrailChrs As String
            Dim xSubCharStandard As String
            Dim xSubCharPleading As String
            Dim xStyle As String
            Dim iTrailingChar As mpTrailingChars
            Dim iUnderline As Integer
            Dim bIsStyleBased As Boolean
            Dim xScheme As String
            Dim iLevel As Integer
            Dim xLevelName As String
            Dim bTrackChanges As Boolean
            Dim lShowTags As Long
            Dim lStart As Long
            Dim iTags As Integer

            '***************NOTE*****************************
            '   MODIFY SUBSTITUTE CHARACTER HERE ONLY-
            '   THIS WILL SUFFICE TO SUBSTITUTE ANY
            '   STRING FOR THE WORD CONSTANT wdTrailingCharacter.wdTrailingNone
            '************************************************
            '   get range of current paragraph
            rngParaStart = rngCur.Paragraphs(1).Range

            '   get line spacing
            With rngParaStart.ParagraphFormat
                If .LineSpacingRule = WdLineSpacing.wdLineSpaceDouble Or
                        .LineSpacing > 14 Then
                    bIsDoubleSpaced = True
                End If
            End With

            xSubCharStandard = New String(Chr(11), 2)
            xSubCharPleading = Chr(11)
            xStyle = llCur.LinkedStyle

            On Error Resume Next
            xScheme = xGetLTRoot(rngParaStart.ListFormat.ListTemplate.Name)
            On Error GoTo 0

            On Error Resume Next
            iLevel = Right(xStyle, 1)
            If (iLevel = 0) Or (xScheme = "") Then
                rngEditTrailChr11 = rngCur
                Exit Function
            End If
            On Error GoTo 0

            '   get trailing character from level property
            iTrailingChar = xGetLevelProp(xScheme,
                                iLevel,
                                mpNumLevelProps.mpNumLevelProp_TrailChr,
                                mpSchemeTypes.mpSchemeType_Document)

            xSubChr = xGetTrailChrs(iTrailingChar)

            With rngParaStart
                .StartOf()
                .MoveEnd()

                '       delete any existing trailing characters;
                '       if track changes is on, deletion won't work and
                '       code will loop forever
                bTrackChanges = CurWordApp.ActiveDocument.TrackRevisions
                CurWordApp.ActiveDocument.TrackRevisions = False
                While (.Paragraphs(1).Range.Characters.Count > 1) And
                        (.Text = Chr(11) Or .Text = " " Or .Text = vbTab)
                    .Text = ""
                    .MoveEnd()
                End While
                CurWordApp.ActiveDocument.TrackRevisions = bTrackChanges

                .StartOf()

                '       insert new trailing characters
                If llCur.TrailingCharacter = WdTrailingCharacter.wdTrailingNone Then
                    '           you'll get here if xSubChr is nothing,
                    '           chr(11) or string(2,11)
                    If g_bXMLSupport Then
                        'adjust for XML tags
                        lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, True)
                        lStart = GetTagSafeParagraphStart(rngParaStart, iTags)
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        If iTags = 0 Then _
                            lStart = GetCCSafeParagraphStart(rngParaStart)
                        .SetRange(lStart, lStart)
                    End If

                    '--         handle page break bug
                    If .Characters.Last.Text <> "" Then
                        If Asc(.Characters.Last.Text) = 12 Then
                            .MoveEnd(WdUnits.wdCharacter)
                        End If
                    End If

                    If Len(xSubChr) Then
                        .InsertAfter(xSubChr)
                        '               remove underlining where not appropriate
                        If CurWordApp.ActiveDocument.Styles(.Style).Font _
                                .Underline = WdUnderline.wdUnderlineNone Then
                            .Font.Underline = WdUnderline.wdUnderlineNone
                        End If
                        .EndOf()

                        If (rngCur.Start = rngParaStart.Start - Len(xSubChr)) Or
                                (Left(rngCur.Text, Len(xSubChr)) = xSubChr) Then
                            '                   move start to end of trailing characters
                            rngCur.MoveStart(WdUnits.wdCharacter, Len(xSubChr))
                        End If
                    End If

                    If g_bXMLSupport And lShowTags Then
                        'rehide tags if necessary
                        SetXMLMarkupState(CurWordApp.ActiveDocument, False)
                    End If
                End If

                rngEditTrailChr11 = rngCur
            End With
        End Function

        Public Shared Function UseWordHeadingStyles(ByVal xScheme As String)
            '   redefine Word heading styles
            '   to "look like" selected scheme
            RedefineHeadingStyles(xScheme)
        End Function

        Private Shared Sub RedefineHeadingStyles(ByVal xScheme As String)
            Dim stySource As Word.Style
            Dim styHeading As Word.Style
            Dim i As Integer
            Dim xStyle As String
            Dim xStyleRoot As String
            Dim iLevels As Integer
            Dim xLT As String
            Dim iBase As Integer
            Dim iNext As Integer

            xStyleRoot = xGetStyleRoot(xScheme)
            iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)
            xLT = xGetFullLTName(xScheme)

            With CurWordApp.ActiveDocument
                For i = 1 To 9
                    xStyle = xStyleRoot & "_L" & i
                    On Error Resume Next
                    stySource = .Styles(xStyle)
                    On Error GoTo 0
                    If Not (stySource Is Nothing) Then
                        styHeading = .Styles(xTranslateHeadingStyle(i))
                        With styHeading
                            If stySource.BaseStyle.NameLocal <> "" Then
                                '                       see if style is based on another level of scheme
                                iBase = iGetProprietaryLevel(xStyleRoot,
                                    stySource.BaseStyle.NameLocal)
                            End If
                            If iBase <> 0 Then
                                '                       switch base style to Heading style of same level
                                .BaseStyle = xTranslateHeadingStyle(iBase)
                            Else
                                .BaseStyle = stySource.BaseStyle
                            End If
                            .Font = stySource.Font
                            '                       in XP, setting the ParagraphFormat object can
                            '                       cause the next use of either this object or .NextParagraphStyle
                            '                       to freeze Word; seems to happen only after MacPac has initialized,
                            '                       when there's at least two levels of numbering in doc, and when
                            '                       ConvertToHeadingStyles is the first function run in the Schemes dlg
                            With .ParagraphFormat
                                .Alignment = stySource.ParagraphFormat.Alignment
                                .RightIndent = stySource.ParagraphFormat.RightIndent
                                .LineSpacingRule = stySource.ParagraphFormat.LineSpacingRule
                                .LineSpacing = stySource.ParagraphFormat.LineSpacing
                                .SpaceAfter = stySource.ParagraphFormat.SpaceAfter
                                .SpaceBefore = stySource.ParagraphFormat.SpaceBefore
                                .WidowControl = stySource.ParagraphFormat.WidowControl
                                .KeepTogether = stySource.ParagraphFormat.KeepTogether
                                .KeepWithNext = stySource.ParagraphFormat.KeepWithNext
                                .LeftIndent = stySource.ParagraphFormat.LeftIndent
                                .FirstLineIndent = stySource.ParagraphFormat.FirstLineIndent
                            End With
                            iNext = iGetProprietaryLevel(xStyleRoot, stySource.NextParagraphStyle.NameLocal)
                            If iNext <> 0 Then
                                '                       change next para style to corresponding Word Heading style
                                .NextParagraphStyle = xTranslateHeadingStyle(iNext)
                            Else
                                .NextParagraphStyle = stySource.NextParagraphStyle
                            End If
                        End With
                    End If
                    stySource = Nothing
                Next i

                '        For i = iLevels To 1 Step -1
                '            If g_bIsXP Then
                ''               under some circumstances in XP (see above), the other branch
                ''               will create a new list template for each level
                '                .ListTemplates(xLT).ListLevels(i).LinkedStyle = xTranslateHeadingStyle(i)
                '            Else
                '                Set styHeading = .Styles(xTranslateHeadingStyle(i))
                '                styHeading.LinkToListTemplate .ListTemplates(xLT), i
                '            End If
                RelinkPreserveIndents(CurWordApp.ActiveDocument, .ListTemplates(xLT), xScheme, 1, iLevels, True)
                '        Next i

                'GLOG 15853 (dm) - delete proprietary styles
                For i = 1 To iLevels
                    xStyle = xStyleRoot & "_L" & i
                    On Error Resume Next
                    stySource = .Styles(xStyle)
                    On Error GoTo 0
                    If Not (stySource Is Nothing) Then _
                        stySource.Delete()
                Next i
            End With
        End Sub

        Private Shared Sub DeleteSchemeStyles(ByVal xScheme As String)
            Dim i As Integer
            Dim xStyle As String
            Dim xStyleRoot As String

            xStyleRoot = xGetStyleRoot(xScheme)

            For i = 1 To 9
                xStyle = xStyleRoot & "_L" & i
                With CurWordApp.ActiveDocument
                    On Error Resume Next
                    .Styles(xStyle).Delete()
                    .Styles(xStyleRoot & " Cont " & i).Delete()
                End With
            Next i
        End Sub

        Public Shared Function bConvertToHeadingStyles(ByVal xScheme As String,
                                                Optional bShowMessage As Boolean = True) _
                                                As Boolean
            'converts xScheme to be used with Heading styles
            Dim i As Integer
            Dim xHScheme As String
            Dim iLevels As Integer
            Dim xStyleRoot As String
            Dim xAlias As String
            Dim iLevel As Integer
            Dim xCerpt As String

            iUserChoice = 0

            '   only do if scheme is not currently
            '   using Heading Styles
            If Not bIsHeadingScheme(xScheme) Then
                '       test for existing Heading Scheme
                xHScheme = xHeadingScheme()

                If Len(xHScheme) Then
                    '           another scheme uses Heading 1-9 styles;
                    '           prompt user to convert this scheme to
                    '           use MacPac styles
                    If bShowMessage Then
                        xAlias = GetField(xHScheme, mpRecordFields.mpRecField_Alias, , CurWordApp.ActiveDocument)
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xMsg = "Le th�me " & xAlias & " utilise actuellement les styles Titre 1-9 Word. Ce th�me sera converti pour utiliser les styles " & xGetStyleRoot(xHScheme) & "."
                        Else
                            xMsg = "The " & xAlias & " scheme currently uses " &
                                   "Word's Heading 1-9 styles." & vbCr & "This scheme will " &
                                   "be converted to use the " & xGetStyleRoot(xHScheme) & " styles. "
                        End If
                        iUserChoice = MsgBox(xMsg, vbInformation + vbOKCancel)
                    Else
                        iUserChoice = vbOK
                    End If

                    If iUserChoice = vbCancel Then
                        '               user cancelled
                        Exit Function
                    Else
                        '               convert current heading
                        '               scheme to use MacPac styles
                        bRet = bConvertToMacPacStyles(xHScheme, , bShowMessage)
                        If Not bRet Then _
                            Exit Function
                    End If
                End If

                iUserChoice = 0

                '       get num levels
                iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)

                If iLevels < 9 Then
                    If bHeadingStylesUsed(iLevels + 1) Then
                        '               no need to prompt if we've already converted Heading 1-9
                        If bShowMessage And xHScheme = "" Then
                            xAlias = GetField(xScheme, mpRecordFields.mpRecField_Alias, , CurWordApp.ActiveDocument)
                            If iLevels = 8 Then
                                xCerpt = "9"
                            Else
                                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                    xCerpt = "styles " & iLevels + 1 & "-9"
                                Else
                                    xCerpt = iLevels + 1 & "-9 styles"
                                End If
                            End If
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                xMsg = "Le th�me " & xAlias & " a seulement " & iLevels &
                                       " niveaux.  Tous paragraphes format�s avec Titre " &
                                        xCerpt & " seront convis au style 'Normal'."
                            Else
                                xMsg = "The " & xAlias & " scheme has only " & iLevels &
                                       " levels.  Any paragraphs formatted with Heading " &
                                        xCerpt & " will be converted to the 'Normal' style."
                            End If
                            iUserChoice = MsgBox(xMsg, vbOKCancel, g_xAppName)
                        Else
                            iUserChoice = vbOK
                        End If

                        '               exit if cancelled
                        If iUserChoice = vbCancel Then
                            Exit Function
                        End If

                        '               update status
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            CurWordApp.StatusBar =
                                "Convertion Titre " & xCerpt &
                                " � Normal. Veuillez patienter..."
                        Else
                            CurWordApp.StatusBar =
                                "Converting Heading " & xCerpt &
                                " to Normal. Please wait..."
                        End If

                        '               change all heading styles that have no
                        '               corresponding MacPac style to Normal
                        For i = iLevels + 1 To 9
                            '11/20/12 (dm) - EditReplace removes style separators
                            With CurWordApp.ActiveDocument.Content.Find
                                .ClearFormatting()
                                .Format = True
                                .Wrap = WdFindWrap.wdFindContinue
                                .Style = xTranslateHeadingStyle(i)
                                .Execute()
                                While .Found
                                    .Parent.Style = CurWordApp.ActiveDocument _
                                        .Styles(WdBuiltinStyle.wdStyleNormal).NameLocal
                                    .Execute()
                                End While
                            End With
                        Next i
                    End If
                End If

                '       update status
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar =
                            "Convertion TSG styles " & xStyleRoot &
                            "_L1-" & iLevel & " � Styles Titre Word. Veuillez patienter..."
                Else
                    CurWordApp.StatusBar =
                            "Converting TSG " & xStyleRoot &
                            "_L1-" & iLevel & " styles to Word " &
                            "Heading Styles. Please wait..."
                End If

                xStyleRoot = xGetStyleRoot(xScheme)

                '       change any paras of same scheme to heading styles
                For i = 1 To iLevels
                    '11/20/12 (dm) - EditReplace removes style separators
                    With CurWordApp.ActiveDocument.Content.Find
                        .ClearFormatting()
                        .Format = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Style = xStyleRoot & "_L" & i
                        .Execute()
                        While .Found
                            .Parent.Style = xTranslateHeadingStyle(i)
                            .Execute()
                        End While
                    End With
                Next i

                '       relink scheme to heading styles
                UseWordHeadingStyles(xScheme)

                '       inform of completion
                If bShowMessage Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Conversion est compl�t�e. Tous paragraphes pr�c�demment format�s  avec styles " & xStyleRoot & "_L1-" &
                               iLevels & " sont maintenant format�s avec styles Titre 1-" & iLevels & "."
                        If xCerpt <> "" Then
                            xMsg = xMsg & vbCr & "Tous paragraphes pr�c�demment format�s avec Titre " & xCerpt &
                               " sont maintenant format�s avec le style 'Normal'" & vbCr
                        End If
                    Else
                        xMsg = "Conversion is complete." & vbCr & "All paragraphs " &
                               "previously formatted with " & xStyleRoot & "_L1-" &
                               iLevels & " styles are now formatted with " &
                               "Heading 1-" & iLevels & " styles."
                        If xCerpt <> "" Then
                            xMsg = xMsg & vbCr & "All paragraphs previously formatted " &
                               "with Heading " & xCerpt &
                               " are now formatted with " &
                               "the 'Normal' style." & vbCr
                        End If
                    End If
                    MsgBox(xMsg, vbInformation, g_xAppName)
                End If
                bConvertToHeadingStyles = True
            End If
        End Function
        Public Shared Function bConvertToMacPacStyles(ByVal xScheme As String,
                    Optional ByVal bForce As Boolean = False,
                    Optional ByVal bInform As Boolean = True) As Boolean
            'converts xScheme to be used with MacPac (ie XXX_LY) styles
            Dim i As Integer
            Dim iLevels As Integer
            Dim xStyleRoot As String
            Dim xAlias As String
            Dim xStyle As String
            Dim styMacPac As Word.Style
            Dim styHeading As Word.Style
            Dim rngSelection As Word.Range
            Dim xCerpt As String
            Dim xNextPara As String
            Dim styNextPara As Word.Style
            Dim iBase As Integer
            Dim iNext As Integer
            Dim oTempRange As Word.Range
            Dim iMoved As Integer

            iUserChoice = 0

            '   only do if scheme is currently
            '   using Heading styles
            If xScheme = xHeadingScheme() Or bForce Then

                '       this is necessary to deal with Word quirk when
                '       switching from Heading 1-9 to proprietary styles
                rngSelection = CurWordApp.Selection.Range.Duplicate
                CurWordApp.Selection.Collapse(WdCollapseDirection.wdCollapseStart)

                'GLOG 15853 (dm) - when adding TSG styles on the fly, link to list template persists in
                'Heading style at cursor even through relnking - move cursor to safe paragraph
                On Error Resume Next
                xStyle = CurWordApp.Selection.Style.NameLocal
                On Error GoTo 0
                While bIsHeadingStyle(xStyle)
                    iMoved = CurWordApp.Selection.Move(WdUnits.wdParagraph)
                    If iMoved = 0 Then
                        'end of doc - add a temp Normal paragraph
                        oTempRange = CurWordApp.Selection.Range 'GLOG 15879 (dm)
                        oTempRange.Expand(WdUnits.wdParagraph)
                        oTempRange.InsertParagraphAfter()
                        oTempRange.Collapse(WdCollapseDirection.wdCollapseEnd)
                        oTempRange.Style = WdBuiltinStyle.wdStyleNormal
                        oTempRange.Select()
                    End If

                    On Error Resume Next
                    xStyle = CurWordApp.Selection.Style.NameLocal
                    On Error GoTo 0
                End While

                xStyleRoot = xGetStyleRoot(xScheme)

                '       get num levels
                iLevels = iGetLevels(xScheme, mpSchemeTypes.mpSchemeType_Document)

                '       alert if there are less than 9 levels -
                '       alert that some levels will be converted to Normal
                If iLevels < 9 Then
                    If bHeadingStylesUsed(iLevels + 1) Then
                        If bInform Then
                            xAlias = GetField(xScheme, mpRecordFields.mpRecField_Alias, , CurWordApp.ActiveDocument)
                            If iLevels = 8 Then
                                xCerpt = "9"
                            Else
                                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                    xCerpt = "styles " & iLevels + 1 & "-9"
                                Else
                                    xCerpt = iLevels + 1 & "-9 styles"
                                End If
                            End If
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                xMsg = "Le th�me " & xAlias & " a seulement " & iLevels &
                                       " niveaux.  Tous paragraphes format�s avec Titre " & xCerpt &
                                       " seront convis au style 'Normal'."
                            Else
                                xMsg = "The " & xAlias & " scheme has only " & iLevels &
                                       " levels. Any paragraphs " &
                                       "formatted with Heading " & xCerpt &
                                       " will be converted to the 'Normal' style."
                            End If
                            iUserChoice = MsgBox(xMsg, vbOKCancel, g_xAppName)
                        Else
                            iUserChoice = vbOK
                        End If
                    End If

                    '           exit if cancelled
                    If iUserChoice = vbCancel Then
                        Exit Function
                    End If

                    '           change all heading styles that have no
                    '           corresponding MacPac style to Normal
                    For i = iLevels + 1 To 9
                        '11/20/12 (dm) - EditReplace removes style separators
                        With CurWordApp.ActiveDocument.Content.Find
                            .ClearFormatting()
                            .Format = True
                            .Wrap = WdFindWrap.wdFindContinue
                            .Style = xTranslateHeadingStyle(i)
                            .Execute()
                            While .Found
                                .Parent.Style = CurWordApp.ActiveDocument _
                                    .Styles(WdBuiltinStyle.wdStyleNormal).NameLocal
                                .Execute()
                            End While
                        End With
                    Next i
                End If

                '       update status
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "Convertion Styles Titre � TSG styles " & xStyleRoot &
                                            "_L1-9. Veuillez patienter..."
                Else
                    CurWordApp.StatusBar = "Converting Heading Styles " &
                                            "to TSG " & xStyleRoot &
                                            "_L1-9 styles. Please wait..."
                End If

                For i = 1 To iLevels
                    '           redefine MacPac styles to corresponding
                    '           Heading Style definition
                    xStyle = xStyleRoot & "_L" & i
                    styHeading = CurWordApp.ActiveDocument.Styles(xTranslateHeadingStyle(i))

                    styMacPac = Nothing

                    On Error Resume Next
                    styMacPac = CurWordApp.ActiveDocument _
                        .Styles(xStyle)
                    On Error GoTo 0

                    '           create style if necessary
                    If (styMacPac Is Nothing) Then
                        If g_bCreateUnlinkedStyles Then
                            '9.9.4010
                            styMacPac = AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument, xStyle)
                        Else
                            styMacPac = CurWordApp.ActiveDocument.Styles.Add(xStyle)
                        End If
                    End If

                    With styMacPac
                        If styHeading.BaseStyle.NameLocal <> "" Then
                            '                   see if style is based on another Word Heading style
                            iBase = iGetWordHeadingLevel(styHeading.BaseStyle.NameLocal)
                        End If
                        If iBase <> 0 Then
                            '                   switch base style to proprietary style of same level
                            .BaseStyle = xStyleRoot & "_L" & iBase
                        Else
                            .BaseStyle = styHeading.BaseStyle
                        End If

                        iNext = iGetWordHeadingLevel(styHeading.NextParagraphStyle.NameLocal)
                        If iNext <> 0 Then
                            '                   change next para style to corresponding proprietary style
                            xNextPara = xStyleRoot & "_L" & iNext

                            '                   create style if necessary
                            styNextPara = Nothing
                            On Error Resume Next
                            styNextPara = CurWordApp.ActiveDocument.Styles(xNextPara)
                            On Error GoTo 0
                            If (styNextPara Is Nothing) Then
                                If g_bCreateUnlinkedStyles Then
                                    '9.9.4010
                                    AddUnlinkedParagraphStyle(CurWordApp.ActiveDocument, xNextPara)
                                Else
                                    CurWordApp.ActiveDocument.Styles.Add(xNextPara)
                                End If
                            End If

                            .NextParagraphStyle = xNextPara
                        Else
                            .NextParagraphStyle = styHeading.NextParagraphStyle
                        End If
                        .Font = styHeading.Font
                        .ParagraphFormat = styHeading.ParagraphFormat
                    End With

                    '           change all paras with Heading style applied to
                    '           proprietary MacPac style of same level
                    On Error Resume Next
                    '11/20/12 (dm) - EditReplace removes style separators
                    With CurWordApp.ActiveDocument.Content.Find
                        .ClearFormatting()
                        .Format = True
                        .Wrap = WdFindWrap.wdFindContinue
                        .Style = xTranslateHeadingStyle(i)
                        .Execute()
                        While .Found
                            .Parent.Style = xStyle
                            .Execute()
                        End While
                    End With
                Next i

                SendShiftKey()
                System.Windows.Forms.Application.DoEvents()

                If xScheme = "HeadingStyles" Then
                    bRelinkHeadingStylesToMacPacStyles()
                Else
                    '           relink scheme to proprietary styles
                    bRelinkScheme(xScheme, _
                                  mpSchemeTypes.mpSchemeType_Document, _
                                  False, False)
                End If

                CurWordApp.StatusBar = ""
                SendShiftKey()

                If bInform Then
                    '           inform of completion
                    If g_lUILanguage = wdLanguageID.wdFrenchCanadian Then
                        xMsg = "Conversion est compl�t�e. Tous paragraphes pr�c�demment format�s  avec styles Titre 1-" & iLevels & _
                               " sont maintenant format�s avec styles " & _
                               xStyleRoot & "_L1-" & iLevels & "."
                        If iUserChoice = vbOK Then
                            xMsg = xMsg & vbCr & "Tous paragraphes pr�c�demment format�s avec Titre " & xCerpt & _
                               " sont maintenant format�s avec le style 'Normal'" & vbCr
                        End If
                    Else
                        xMsg = "Conversion is complete." & vbCr & "All paragraphs " & _
                               "previously formatted with Heading 1-" & iLevels & _
                               " styles are now formatted with " & _
                               xStyleRoot & "_L1-" & iLevels & " styles."
                        If iUserChoice = vbOK Then
                            xMsg = xMsg & vbCr & "All paragraphs previously formatted " & _
                               "with Heading " & xCerpt & _
                               " are now formatted with " & _
                               "the 'Normal' style." & vbCr
                        End If
                    End If
                    MsgBox(xMsg, vbInformation, g_xAppName)
                End If
                rngSelection.Select()

                'GLOG 15853 (dm)
                If Not oTempRange Is Nothing Then
                    oTempRange.Paragraphs(1).Range.Delete()
                End If

                bConvertToMacPacStyles = True
            End If
        End Function
        Public Shared Function bHeadingStylesUsed(Optional ByVal iFirstLevel As Integer = 0) As Boolean
            'returns TRUE if any Heading Style
            'level iLevel or greater is .InUse
            Dim i As Integer

            '   ensure that first level is at least 1
            iFirstLevel = mpMax(CDbl(iFirstLevel), 1)

            '   cycle through levels
            For i = iFirstLevel To 9
                '       return true if .InUse
                If CurWordApp.ActiveDocument.Styles(xTranslateHeadingStyle(i)).InUse Then
                    bHeadingStylesUsed = True
                    Exit For
                End If
            Next i
        End Function

        Public Shared Function EditPaste() As Long
            CurWordApp.Selection.Paste()
            System.Windows.Forms.Application.DoEvents()
            EchoOff()
            bRelinkSchemes(mpSchemeTypes.mpSchemeType_Document)
            EchoOn()
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Function

        Public Shared Sub LoadStyFiles()
            If Dir(g_xPNumSty) = "" Then _
                Exit Sub

            With CurWordApp.AddIns
                With .Item(g_xFNumSty)
                    If .Installed = False Then .Installed = True
                End With
                With .Item(g_xPNumSty)
                    If .Installed = False Then .Installed = True
                End With
            End With
        End Sub

        Public Shared Sub UnLoadStyFiles()
            If Dir(g_xPNumSty) = "" Then _
                Exit Sub

            '   save pnumsty if necessary
            With g_oPNumSty
                If Not .Saved Then
                    'workaround for Word 2010 to avoid error 5986 -
                    '"this command is not available in an unsaved document"
                    .OpenAsDocument.Save()
                    CurWordApp.ActiveDocument.Close(False)
                End If
            End With

            '   uninstall add-ins (restored 8/9/00 to hide auto text list);
            '   9.8.1007 - we found that loading and unloading large sty files was
            '   causing Word 2003 to crash after Edit Scheme.  There's no longer any reason
            '   to do this, since we no longer use autotext for the previews;
            '   I decided to modify this function rather than removing the calls to it,
            '   because I'm not sure where we might actually be relying on the above code to
            '   save the sty file
            '    With Word.AddIns
            '        .Item(g_xFNumSty).Installed = False
            '        .Item(g_xPNumSty).Installed = False
            '    End With
        End Sub

        Public Shared Function lInitializeTOCLink() As Long
            '   load tsgTOC.sty and get TOC schemes
            Dim xTOCSTY As String
            Dim addTOC As AddIn
            Dim lTest As Long

            'load tsgTOC.sty if necessary
            xTOCSTY = GetAppPath() & "\tsgTOC.sty"

            On Error Resume Next
            addTOC = CurWordApp.AddIns(xTOCSTY)
            On Error GoTo 0

            If addTOC Is Nothing Then _
                    addTOC = CurWordApp.AddIns.Add(xTOCSTY)

            If Not addTOC.Installed Then _
                CurWordApp.AddIns(xTOCSTY).Installed = True

            'get schemes if necessary
            On Error Resume Next
            lTest = UBound(g_xTOCSchemes)
            If Err.Number Then
                On Error GoTo 0
                iGetTOCSchemes(g_xTOCSchemes)
            Else
                On Error GoTo 0
            End If

            '   this appears to be necessary after loading
            '   an add-in on some machines
            GetTemplate(xTOCSTY).Saved = True

            lInitializeTOCLink = Err.Number
        End Function

        Public Shared Function iChangeLevel_ContStyle(rngLocation As Word.Range, _
                                         iOffset As Integer) As Integer
            Dim xStyle As String
            Dim iLevel As Integer
            Dim oStyle As Word.Style
            Dim oPara As Word.Paragraph
            Dim iPos As Integer
            Dim xStyleRoot As String
            Dim xScheme As String

            For Each oPara In rngLocation.Paragraphs
                '       GLOG 2847 - trap for unstyled paragraphs
                On Error Resume Next
                xStyle = oPara.Style.NameLocal 'GLOG 8888 (dm)
                On Error GoTo 0

                iPos = InStr(xStyle, " Cont ") 'GLOG 15859 (dm)
                If iPos <> 0 Then
                    '           strip alias from style name
                    xStyle = StripStyleAlias(xStyle)

                    '           get level
                    iLevel = Right(xStyle, 1)

                    '           increment/decrement
                    xStyle = Left(xStyle, Len(xStyle) - 1) & (iLevel + iOffset)

                    '           test for existence of new style
                    On Error Resume Next
                    oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
                    On Error GoTo 0

                    'GLOG 15859 (dm) - create cont style if it doesn't already exist
                    If oStyle Is Nothing Then
                        xStyleRoot = Left$(xStyle, iPos - 1)
                        If xStyleRoot = "Heading" Then
                            xScheme = "HeadingStyles"
                        Else
                            xScheme = "zzmp" & xStyleRoot
                        End If
                        CreateContStyles(xScheme, iLevel + iOffset, , True)
                        On Error Resume Next
                        oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
                        On Error GoTo 0
                    End If

                    '           restyle
                    If Not oStyle Is Nothing Then _
                        oPara.Style = xStyle
                End If

                '10/9/12 - added para styles
                iPos = InStr(xStyle, " Para ")
                If iPos <> 0 Then
                    '           strip alias from style name
                    xStyle = StripStyleAlias(xStyle)

                    '           get level
                    iLevel = Right(xStyle, 1)

                    '           get scheme
                    xStyleRoot = Left$(xStyle, iPos - 1)
                    If xStyleRoot = "Heading" Then
                        xScheme = "HeadingStyles"
                    Else
                        xScheme = "zzmp" & xStyleRoot
                    End If

                    '           restyle
                    oPara.Style = GetSplitParaStyle(xScheme, iLevel + iOffset)
                End If
            Next oPara
        End Function

        Private Shared Function rngInsertNumberInTable(iListLevel As Integer, xScheme As String) As Word.Range
            'GLOG 2866 (11/7/11) - uses selection rather than range to avoid adding
            'numbers to unselected table columns
            Dim bIsNumbered As Boolean
            Dim ltCurrent As ListTemplate
            Dim paraExisting As Paragraph
            Dim rngInsertion As Word.Range
            Dim bMultipleParagraphs As Boolean
            Dim iListType As Integer
            Dim iNumParas As Integer
            Dim rngParaCurrent As Word.Range

            '   use as percent complete denominator
            iNumParas = CurWordApp.Selection.Paragraphs.Count

            '   force  number to be level 1-9
            iListLevel = mpMax(mpMin(CDbl(iListLevel), 9), 1)

            bMultipleParagraphs = CurWordApp.Selection.Paragraphs.Count > 1

            For Each paraExisting In CurWordApp.Selection.Paragraphs
                '       if multiple paragraphs in scope,
                '       insert at start of paragraph
                '       else at insertion, if possible
                If bMultipleParagraphs Then
                    rngInsertion = paraExisting.Range
                    rngInsertion.StartOf()
                Else
                    rngInsertion = CurWordApp.Selection.Range
                End If

                iListType = iGetListType(rngInsertion)

                bIsNumbered = (iListType = mpListTypes.mpListTypeNative Or _
                              iListType = mpListTypes.mpListTypeMixed)

                'GLOG 5195 - behavior should be the same in partner para style
                'paragraph as it is in numbered paragraph
                If bIsNumbered Then
                    ltCurrent = rngInsertion.ListFormat.ListTemplate
                Else
                    rngParaCurrent = rngInsertion.Duplicate
                    If rngParaCurrent.Paragraphs(1).Range.Start > 0 Then
                        rngParaCurrent = rngParaCurrent.Previous(WdUnits.wdParagraph)
                        If rngParaCurrent.Paragraphs.First.IsStyleSeparator Then
                            iListType = iGetListType(rngParaCurrent)
                            bIsNumbered = (iListType = mpListTypes.mpListTypeNative Or
                                iListType = mpListTypes.mpListTypeMixed)
                            If bIsNumbered Then
                                ltCurrent = rngParaCurrent.ListFormat.ListTemplate
                            End If
                        End If
                    End If
                End If

                '       if numbered insert LISTNUM,
                '       else insert Native Number
                If bIsNumbered Then
                    '           insert as LISTNUM field, if selection is IP
                    If CurWordApp.Selection.Type = WdSelectionType.wdSelectionIP Then
                        rngInsertNumberInTable = rngInsertNumListNum(iListLevel, _
                            rngInsertion, ltCurrent)
                    End If
                Else
                    'insert Native word number from defined scheme
                    rngInsertNumberInTable = rngInsertNumWordNum(rngInsertion, _
                        xScheme, iListLevel)
                End If
            Next paraExisting
        End Function
    End Class
End Namespace


