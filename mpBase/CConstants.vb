Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.mpNumSchemes

Namespace LMP.Numbering.Base
    Public Class cConstants
        'item locations
        Public Enum DocPosition
            mpAtBOF = 0
            mpAtEOF = 1
            mpAtInsertion = 2
            mpAboveTOA = 3
        End Enum

        'User Operations
        Public Enum mpUserActions
            mpFinish = -1
            mpDlgCancelled = 0
            mpPrint = 1
            mpAddToDoc = 2
        End Enum

        'keyboard
        Public Enum mpKey
            mpKey_UpArrow = 38
            mpKey_DownArrow = 40
            mpKey_Hex_LeftArrow = &H25
            mpKey_Hex_UpArrow = &H26
            mpKey_Hex_RightArrow = &H27
            mpKey_Hex_DownArrow = &H28
            mpKey_Hex_Tab = &H9
            mpKey_Hex_Alt = &H12
            mpKey_Hex_Ctl = &H11
        End Enum

        Public Enum mpPleadingFirmNameTypes
            mpPleadingFirmNameNone = 0
            mpPleadingFirmNameInFooter = 1
            mpPleadingFirmNameLandscaped = 2
        End Enum

        'pleading paper types
        Public Enum mpPleadingPaperTypes
            mpPleadingPaperNone = 0
            mpPleadingPaper26Line = 26
            mpPleadingPaper28Line = 28
            mpPleadingPaperNoneFNPort = 100
            mpPleadingPaper26LineFNPort = 126
            mpPleadingPaper28LineFNPort = 128
            mpPleadingPaperNoneFNLand = 200
            mpPleadingPaper26LineFNLand = 226
            mpPleadingPaper28LineFNLand = 228
        End Enum

        'error values
        Public Enum mpErrors
            mpCancel = 0
            mpInitializeRetry = 3
            mpTCEntryStylesNotLoaded = 9
            mpTypeMismatch = 13
            mpBlockNotSet = 91
            mpErrInvalidNull = 94
            mpWB_CommandFailed = 102  'off wbasic object
            mpNoListArrays = 380
            mpInvalidMacPacApplicationPath = 612
            mpErrDupePrimaryKey = 3022
            mpErrFileNotFound = 3044
            mpErrDBOpenFailed = 3051
            mpErrDBSQLTooFewParameters = 3061
            mpDBTableDoesNotExist = 3078
            mpItemNotInCollection = 3265
            mpBoilerPlateMissing = 4198
            mpParagraphIsNotNumbered = 4608
            mpBookmarkMissing = 5101
            mpNotValidListLevel = 5148
            mpIncorrectPassword = 5485
            mpStyleDoesNotExist = 5608
            mpItemNameNotFound = 5834
            mpErrListNumNameInUse = 5890
            mpCollectionMemberDoesNotExist = 5941
        End Enum

        Friend Const mpAppName As String = "TSG Numbering"
        Friend Const mpSlogan As String = "A Professional Corporation"
        Friend Const mpDisplayBenchmarks As Boolean = False
        Friend Const mpPointsPerIn As Integer = 72
        Friend Const mpTwipsPerPoint As Integer = 1440 / 72

        'templates
        Friend Const mpTemplateBusiness As String = "Business"
        Friend Const mpTemplateFax As String = "Fax"
        Friend Const mpTemplateDPhrase As String = "DPhrase"
        Friend Const mpTemplateLetter As String = "Letter"
        Friend Const mpTemplateMemo As String = "Memo"
        Friend Const mpTemplatePleading As String = "Pleading"
        Friend Const mpTemplatePOS As String = "POS"
        Friend Const mpPOSTitle As String = "Proof of Service"
        Friend Const mpTemplateVer As String = "Verification"
        Friend Const mpTemplateNotary As String = "Notary"
        Friend Const mpTemplateDepo As String = "Depo Summary"
        Friend Const mpTemplatePleadingIL As String = "PleadingIL"
        Friend Const mpPleadingILTitle As String = "Pleading IL"
        Friend Const mpTemplateCert As String = "Cert"
        Friend Const mpCertTitle As String = "Certificate of Service"
        Friend Const mpTemplateAppellate As String = "PleadingAppellate"
        Friend Const mpAppellateTitle As String = "Pleading Appellate"
        Friend Const mpPropNotAvailable As String = "|mpNA|"
        Friend Const mpPrefix As String = "zzmp"
        Friend Const mpNativeHeadingScheme As String = "HeadingStyles"

        'PleadingPaper
        Friend Const mpHeaderDistDefault As String = ".5"
        Friend Const mpFooterDistDefault As String = ".5"

        Friend Const mpPleadingHeaderDistance As String = ".3"
        Friend Const mpPleadingFooterDistance As String = ".25"

        Friend Const mpTopMarginDefault As String = "1"
        Friend Const mpBottomMarginDefault As String = "1"
        Friend Const mpLeftMarginDefault As String = "1"
        Friend Const mpRightMarginDefault As String = "1"

        Friend Const mpPleading26TopMargin As String = "-1.35"
        Friend Const mpPleading26BotMargin As String = "1.12"
        Friend Const mpPleading26LeftMargin As String = "1.45"
        Friend Const mpPleading26RightMargin As String = ".5"

        Friend Const mpPleading28LeftMargin As String = "1.45"
        Friend Const mpPleading28RightMargin As String = ".5"
        Friend Const mpPleading28TopMargin As String = "-.92"
        Friend Const mpPleading28BotMargin As String = ".75"

        'msgs
        Friend Const mpMsgAppInitializing As String = "Initializing TSG, please wait..."
        Friend Const mpMsgInitializing As String = "Initializing.  Please wait..."
        Friend Const mpMsgReady As String = "Ready"
        Friend Const mpMsgEditingDPhrase As String = "Editing Delivery Phrases.  Please wait..."
        Friend Const mpMsgEditingBSig As String = "Editing Business Signature.  Please wait..."
        Friend Const mpMsgInsertingRogs As String = "Inserting Interrogatories.  Please wait..."
        Friend Const mpMsgCreatingDoc As String = "Creating document.  Please wait..."
        Friend Const mpMsgWritingTrailer As String = "Updating trailer.  Please wait..."
        Friend Const mpMsgBeginTyping As String = "Begin typing"
        Friend Const mpMsgDefAuthorSet As String = "Default author set to "
        Friend Const mpMsgBeginInput As String = "Please enter a name"
        Friend Const mpMsgUpdatingDBTables As String = "Saving record.  Please wait..."
        Friend Const mpMsgEditingDBTables As String = "Updating table.  Please wait..."
        Friend Const mpMsgRequiredField As String = "Required field.  Please enter a value."
        Friend Const mpMsgFinished As String = "Finished"
        Friend Const mpMsgLSigCreate As String = "Inserting Letter Signature.  Please wait..."
        Friend Const mpMsgInsertingPPaper As String = "Inserting Pleading Paper.  Please wait..."
        Friend Const mpMsgRemovingPPaper As String = "Removing Pleading Paper.  Please wait..."

        'page number format
        Friend Const mpPageNumFormatTOC As String = "\* roman"
        Friend Const mpPageNumIntroChrDefault As String = "-"
        Friend Const mpPageNumTrailChrDefault As String = "-"

        Public Shared ReadOnly Property PropertyNotAvailable As String
            Get
                PropertyNotAvailable = mpPropNotAvailable
            End Get
        End Property

        Public Shared ReadOnly Property AppName As String
            Get
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    AppName = "Numérotation TSG"
                Else
                    AppName = mpAppName
                End If
            End Get
        End Property

        Public Shared ReadOnly Property Slogan() As String
            Get
                Slogan = mpSlogan
            End Get
        End Property

        Public Shared ReadOnly Property DisplayBenchmarks() As String
            Get
                DisplayBenchmarks = mpDisplayBenchmarks
            End Get
        End Property

        Public Shared ReadOnly Property PointsPerIn() As String
            Get
                PointsPerIn = mpPointsPerIn
            End Get
        End Property

        Public Shared ReadOnly Property TwipsPerPoint() As String
            Get
                TwipsPerPoint = mpTwipsPerPoint
            End Get
        End Property

        Public Shared ReadOnly Property TemplateBusiness() As String
            Get
                TemplateBusiness = mpTemplateBusiness
            End Get
        End Property

        Public Shared ReadOnly Property TemplateFax() As String
            Get
                TemplateFax = mpTemplateFax
            End Get
        End Property

        Public Shared ReadOnly Property TemplateDPhrase() As String
            Get
                TemplateDPhrase = mpTemplateDPhrase
            End Get
        End Property

        Public Shared ReadOnly Property TemplateLetter() As String
            Get
                TemplateLetter = mpTemplateLetter
            End Get
        End Property

        Public Shared ReadOnly Property TemplateMemo() As String
            Get
                TemplateMemo = mpTemplateMemo
            End Get
        End Property

        Public Shared ReadOnly Property TemplatePleading() As String
            Get
                TemplatePleading = mpTemplatePleading
            End Get
        End Property

        Public Shared ReadOnly Property TemplatePOS() As String
            Get
                TemplatePOS = mpTemplatePOS
            End Get
        End Property

        Public Shared ReadOnly Property POSTitle() As String
            Get
                POSTitle = mpPOSTitle
            End Get
        End Property

        Public Shared ReadOnly Property TemplateVer() As String
            Get
                TemplateVer = mpTemplateVer
            End Get
        End Property

        Public Shared ReadOnly Property TemplateDepo() As String
            Get
                TemplateDepo = mpTemplateDepo
            End Get
        End Property

        Public Shared ReadOnly Property TemplatePleadingIL() As String
            Get
                TemplatePleadingIL = mpTemplatePleadingIL
            End Get
        End Property

        Public Shared ReadOnly Property PleadingILTitle() As String
            Get
                PleadingILTitle = mpPleadingILTitle
            End Get
        End Property

        Public Shared ReadOnly Property TemplateCert() As String
            Get
                TemplateCert = mpTemplateCert
            End Get
        End Property

        Public Shared ReadOnly Property CertTitle() As String
            Get
                CertTitle = mpCertTitle
            End Get
        End Property

        Public Shared ReadOnly Property TemplateAppellate() As String
            Get
                TemplateAppellate = mpTemplateAppellate
            End Get
        End Property

        Public Shared ReadOnly Property AppellateTitle() As String
            Get
                AppellateTitle = mpAppellateTitle
            End Get
        End Property

        Public Shared ReadOnly Property TemplateNotary() As String
            Get
                TemplateNotary = mpTemplateNotary
            End Get
        End Property

        Public Shared ReadOnly Property TopMarginDefault() As Single
            Get
                TopMarginDefault = mpTopMarginDefault
            End Get
        End Property

        Public Shared ReadOnly Property BottomMarginDefault() As Single
            Get
                BottomMarginDefault = mpBottomMarginDefault
            End Get
        End Property

        Public Shared ReadOnly Property LeftMarginDefault() As Single
            Get
                LeftMarginDefault = mpLeftMarginDefault
            End Get
        End Property

        Public Shared ReadOnly Property RightMarginDefault() As Single
            Get
                RightMarginDefault = mpRightMarginDefault
            End Get
        End Property

        Public Shared ReadOnly Property HeaderDistDefault() As Single
            Get
                HeaderDistDefault = mpHeaderDistDefault
            End Get
        End Property

        Public Shared ReadOnly Property FooterDistDefault() As Single
            Get
                FooterDistDefault = mpFooterDistDefault
            End Get
        End Property

        Public Shared ReadOnly Property PleadingHeaderDistance() As String
            Get
                PleadingHeaderDistance = mpPleadingHeaderDistance
            End Get
        End Property

        Public Shared ReadOnly Property PleadingFooterDistance() As String
            Get
                PleadingFooterDistance = mpPleadingFooterDistance
            End Get
        End Property

        Public Shared ReadOnly Property Pleading26TopMargin() As String
            Get
                Pleading26TopMargin = mpPleading26TopMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading26BotMargin() As String
            Get
                Pleading26BotMargin = mpPleading26BotMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading26LeftMargin() As String
            Get
                Pleading26LeftMargin = mpPleading26LeftMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading26RightMargin() As String
            Get
                Pleading26RightMargin = mpPleading26RightMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading28LeftMargin() As String
            Get
                Pleading28LeftMargin = mpPleading28LeftMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading28RightMargin() As String
            Get
                Pleading28RightMargin = mpPleading28RightMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading28TopMargin() As String
            Get
                Pleading28TopMargin = mpPleading28TopMargin
            End Get
        End Property

        Public Shared ReadOnly Property Pleading28BotMargin() As String
            Get
                Pleading28BotMargin = mpPleading28BotMargin
            End Get
        End Property

        Public Shared ReadOnly Property MsgAppInitializing() As String
            Get
                MsgAppInitializing = mpMsgAppInitializing
            End Get
        End Property

        Public Shared ReadOnly Property MsgInitializing() As String
            Get
                MsgInitializing = mpMsgInitializing
            End Get
        End Property

        Public Shared ReadOnly Property MsgReady() As String
            Get
                MsgReady = mpMsgReady
            End Get
        End Property

        Public Shared ReadOnly Property MsgEditingDPhrase() As String
            Get
                MsgEditingDPhrase = mpMsgEditingDPhrase
            End Get
        End Property

        Public Shared ReadOnly Property MsgEditingBSig() As String
            Get
                MsgEditingBSig = mpMsgEditingBSig
            End Get
        End Property

        Public Shared ReadOnly Property MsgInsertingRogs() As String
            Get
                MsgInsertingRogs = mpMsgInsertingRogs
            End Get
        End Property

        Public Shared ReadOnly Property MsgCreatingDoc() As String
            Get
                MsgCreatingDoc = mpMsgCreatingDoc
            End Get
        End Property

        Public Shared ReadOnly Property MsgWritingTrailer() As String
            Get
                MsgWritingTrailer = mpMsgWritingTrailer
            End Get
        End Property

        Public Shared ReadOnly Property MsgBeginTyping() As String
            Get
                MsgBeginTyping = mpMsgBeginTyping
            End Get
        End Property

        Public Shared ReadOnly Property MsgDefAuthorSet() As String
            Get
                MsgDefAuthorSet = mpMsgDefAuthorSet
            End Get
        End Property

        Public Shared ReadOnly Property MsgBeginInput() As String
            Get
                MsgBeginInput = mpMsgBeginInput
            End Get
        End Property

        Public Shared ReadOnly Property MsgUpdatingDBTables() As String
            Get
                MsgUpdatingDBTables = mpMsgUpdatingDBTables
            End Get
        End Property

        Public Shared ReadOnly Property MsgEditingDBTables() As String
            Get
                MsgEditingDBTables = mpMsgEditingDBTables
            End Get
        End Property

        Public Shared ReadOnly Property MsgRequiredField() As String
            Get
                MsgRequiredField = mpMsgRequiredField
            End Get
        End Property

        Public Shared ReadOnly Property MsgFinished() As String
            Get
                MsgFinished = mpMsgFinished
            End Get
        End Property

        Public Shared ReadOnly Property MsgLSigCreate() As String
            Get
                MsgLSigCreate = mpMsgLSigCreate
            End Get
        End Property

        Public Shared ReadOnly Property MsgInsertingPPaper() As String
            Get
                MsgInsertingPPaper = mpMsgInsertingPPaper
            End Get
        End Property

        Public Shared ReadOnly Property MsgRemovingPPaper() As String
            Get
                MsgRemovingPPaper = mpMsgRemovingPPaper
            End Get
        End Property

        Public Shared ReadOnly Property PageNumFormatTOC() As String
            Get
                PageNumFormatTOC = mpPageNumFormatTOC
            End Get
        End Property

        Public Shared ReadOnly Property PageNumIntroChrDefault() As String
            Get
                PageNumIntroChrDefault = mpPageNumIntroChrDefault
            End Get
        End Property

        Public Shared ReadOnly Property PageNumTrailChrDefault() As String
            Get
                PageNumTrailChrDefault = mpPageNumTrailChrDefault
            End Get
        End Property
    End Class
End Namespace



