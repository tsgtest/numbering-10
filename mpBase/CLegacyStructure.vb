
Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports System.Drawing

Namespace LMP.Numbering.LegacyStructure
    Public Class cWDXX
        Public Shared Function xGetPropFld(oFont As Word.Font) As String
            'returns the value the of the property that
            'contains the level prop - either the NameFarEast
            'field (Word 97) or the NameBi field (Word 2000)
            Dim xScheme As String
            Dim iLevel As Integer
            Dim oSource As Object
            Dim bIsSourceFile As Boolean

            oSource = oFont.Parent.Parent.Parent.Parent.Parent

            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            If bIsSourceFile Then
                With oFont.Parent
                    iLevel = .Index
                    xScheme = .Parent.Parent.Name
                End With
                xGetPropFld = oSource.CustomDocumentProperties(xScheme & iLevel).Value
            Else
                'use late bound object for post-97 property
                xGetPropFld = oFont.Parent.Font.NameBi
            End If
        End Function

        Public Shared Function SetPropFld(oFont As Word.Font, ByVal xValue As String) As Long
            'sets the value the of the property that
            'contains the level prop - either the NameFarEast
            'field (Word 97) or the NameBi field (Word 2000)
            Dim xScheme As String
            Dim iLevel As Integer
            Dim oSource As Object
            Dim bIsSourceFile As Boolean

            oSource = oFont.Parent.Parent.Parent.Parent.Parent
            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            If bIsSourceFile Then
                With oFont.Parent
                    iLevel = .Index
                    xScheme = .Parent.Parent.Name
                End With
                On Error Resume Next
                oSource.CustomDocumentProperties(
                    xScheme & iLevel).Value = xValue
                If Err.Number Then
                    oSource.CustomDocumentProperties.Add(
                        xScheme & iLevel, False, 4, xValue)
                End If
            Else
                'use late bound object for post-97 property
                oFont.Parent.Font.NameBi = xValue
            End If
        End Function

        Public Shared Function UpgradeLevelProp(llP As Word.ListLevel) As Boolean
            'moves level prop for listlevel llp from NameFarEast to NameBi
            'use late bound object for post-97 property
            llP.Parent.Parent.ListLevels(llP.Index).Font.NameBi = llP.Font.NameFarEast
            Return False
        End Function

        Public Shared Function RestorePropFldToTNR(oFont As Word.Font) As Long
            On Error Resume Next
            'use late bound object for post-97 property
            oFont.Parent.Font.NameBi = "Times New Roman"
        End Function

        Public Shared Function bPropFldNeedsReset(oFont As Word.Font) As Boolean
            Dim xValue As String

            On Error Resume Next
            'use late bound object for post-97 property
            xValue = oFont.Parent.Font.NameBi & oFont.NameFarEast

            If InStr(xValue, "|") Or (xValue = "###") Then
                bPropFldNeedsReset = True
            End If
        End Function
    End Class
End Namespace

