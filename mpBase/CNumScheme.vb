Option Explicit On

Imports LMP.Numbering.Base.cSchemeRecords

'**********************************************************
'   CNumScheme Collection Class
'   created 6/3/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define the
'   top level data for a numbering scheme
'**********************************************************

Namespace LMP.Numbering.Base
    Public Class cNumScheme
        Private m_xName As String
        Private m_xDisplayName As String
        Private m_iOrigin As mpSchemeTypes
        Private m_iType As mpSchemeTypes
        Private m_xTOCScheme As String
        Private m_iDynamicFonts As Integer
        Private m_iDynamicSpacing As Integer
        Private m_xDescription As String

        '**********************************************************
        'Properties
        '**********************************************************
        Public Property Name As String
            Get
                Name = m_xName
            End Get
            Set(xNew As String)
                m_xName = xNew
            End Set
        End Property

        Public Property DisplayName As String
            Get
                DisplayName = m_xDisplayName
            End Get
            Set(xNew As String)
                m_xDisplayName = xNew
            End Set
        End Property

        Public Property TOCScheme As String
            Get
                TOCScheme = m_xTOCScheme
            End Get
            Set(xNew As String)
                m_xTOCScheme = xNew
            End Set
        End Property

        Public Property DymanicFonts As Integer
            Get
                DymanicFonts = m_iDynamicFonts
            End Get
            Set(iNew As Integer)
                m_iDynamicFonts = iNew
            End Set
        End Property

        Public Property DynamicSpacing As Integer
            Get
                DynamicSpacing = m_iDynamicSpacing
            End Get
            Set(iNew As Integer)
                m_iDynamicSpacing = iNew
            End Set
        End Property

        Public Property SchemeType As mpSchemeTypes
            Get
                SchemeType = m_iType
            End Get
            Set(iNew As mpSchemeTypes)
                m_iType = iNew
            End Set
        End Property

        Public Property Origin As mpSchemeTypes
            Get
                Origin = m_iOrigin
            End Get
            Set(iNew As mpSchemeTypes)
                m_iOrigin = iNew
            End Set
        End Property

        Public Property Description As String
            Get
                Description = m_xDescription
            End Get
            Set(xNew As String)
                m_xDescription = xNew
            End Set
        End Property
    End Class
End Namespace

