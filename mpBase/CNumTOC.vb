Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.mpNumSchemes
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cStrings
Imports [mpWDXX] = LMP.Numbering.LegacyStructure.cWDXX
Imports System.Math
Imports LMP.Numbering.Base.cConstants
Imports Microsoft.Office.Core
Imports System.IO.Path
Imports System.Reflection.Assembly
Imports System.Drawing.Graphics
Imports System.Xml
Imports System.IO

Namespace LMP.Numbering.Base
    Public Class cNumTOC
        Public Shared g_oWordApp As Word.Application
        Public Shared g_xCodeBasePath As String
        Private Shared xNumbersSty As String
        Private Shared xFirmNumbersSty As String
        Private Shared xMsg As String
        Private Const TWIPS_PER_INCH As Single = 1440
        Private Shared _form = New System.Windows.Forms.Form()
        Private Shared _graphics = _form.CreateGraphics()

        Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (
            ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String,
            ByVal lpFileName As String) As Integer

        'numbered list types
        Public Enum mpListTypes
            mpListTypeNoNumbering = 0
            mpListTypeNative = 1
            mpListTypeListNum = 2
            mpListTypeMixed = 3
        End Enum

        'level properties for numbering schemes;
        'in 9.8, TrailUnderline has been appropriated for
        'dynamic alignment (see below)
        Public Enum mpNumLevelProps
            mpNumLevelProp_TrailChr = 1
            mpNumLevelProp_TrailUnderline = 2
            mpNumLevelProp_HeadingFormat = 3
        End Enum

        'subfield level properties - TrailUnderline;
        'to avoid a retrofit, these fields are
        'based on existing pre-9.8 reality; existing
        'values were 0 or 1; a right-aligned numbered
        'style meant adjust to normal but a right-aligned
        'cont style did not
        Public Enum mpTrailUnderlineFields
            mpTrailUnderlineField_Underline = &H1
            mpTrailUnderlineField_HonorAlignment = &H2
            mpTrailUnderlineField_AdjustToNormal = &H4
            mpTrailUnderlineField_AdjustContToNormal = &H8
        End Enum

        'subfield level properties - heading formats
        Public Enum mpTCFormatFields
            mpTCFormatField_Bold = &H1
            mpTCFormatField_Italic = &H2
            mpTCFormatField_Underline = &H4
            mpTCFormatField_AllCaps = &H8
            mpTCFormatField_SmallCaps = &H10
            mpTCFormatField_Type = &H20
        End Enum

        'level properties for toc schemes
        Public Enum mpTOCLevelProp
            mpTOCLevelProp_SpaceBeforeLevel = 1
            mpTOCLevelProp_IncludePgNum = 2
        End Enum

        'trailing characters
        Public Enum mpTrailingChars
            mpTrailingChar_None = 0
            mpTrailingChar_Tab = 1
            mpTrailingChar_Space = 2
            mpTrailingChar_DoubleSpace = 3
            mpTrailingChar_ShiftReturn = 4
            mpTrailingChar_DoubleShiftReturn = 5
        End Enum

        'TOC/TCEntry
        Private Const mpTOCSchemeGeneric As String = "Generic"

        Public Shared Property CurWordApp As Word.Application
            Get
                CurWordApp = g_oWordApp
            End Get
            Set(value As Word.Application)
                g_oWordApp = value
            End Set
        End Property

        Public Shared ReadOnly Property CodeBasePath As String
            Get
                Dim xPath As String
                If g_xCodeBasePath <> "" Then
                    CodeBasePath = g_xCodeBasePath
                Else
                    xPath = GetDirectoryName(GetExecutingAssembly().GetName().CodeBase)
                    CodeBasePath = xPath.Replace("file:\", "")
                End If
            End Get
        End Property

        Public Shared ReadOnly Property iHeadingColor() As Integer
            Get
                'returns the color for a MacPac TC Heading -
                'offset exists because color wdAuto is 0, but
                'we want to use 0 to test whether to get
                'the value for the ini
                Const iOffset As Integer = 999
                If g_iTCEntryColor = 0 Then
                    g_iTCEntryColor = GetAppSetting("Numbering", "HeadingColor") + iOffset
                End If
                iHeadingColor = g_iTCEntryColor - iOffset
            End Get
        End Property

        Public Shared ReadOnly Property SettingsFileName As String
            Get
                If g_xSettingsFileName = "" Then
                    If g_xFirmXMLConfig = "" Then
                        If g_xAppPath = "" Then _
                            g_xAppPath = mpNumSchemes.GetAppPath()
                        g_xFirmXMLConfig = g_xAppPath & "\tsgNumberingConfig.xml"
                    End If

                    If File.Exists(g_xFirmXMLConfig) Then
                        g_xSettingsFileName = "tsgNumberingConfig.xml"
                    Else
                        g_xSettingsFileName = "tsgNumbering.ini"
                    End If
                End If
                Return g_xSettingsFileName
            End Get
        End Property
        Public Shared Function iGetSchemes(ByRef xArray(,) As String,
                                    iSchemeType As mpSchemeTypes) As Integer
            'cycles through custom document properties in tsgNumbers.sty
            'to fill array with scheme names and aliases
            Dim i As Integer
            Dim j As Integer
            Dim iNumSchemes As Integer
            Dim oSource As Object
            Dim xValue As String
            Dim iPos As Integer
            Dim ltP As Word.ListTemplate
            Dim xScheme As String

            On Error Resume Next

            If CurWordApp.ActiveDocument.Windows.Count = 0 Then
                CurWordApp.Documents.Add()
            End If

            '   point to appropriate source of scheme - based on type
            oSource = oSchemeSource(iSchemeType)
            On Error GoTo 0

            '   convert Word Heading Styles to MacPac scheme -
            '   we should eventually not convert Heading Styles
            '   to a MacPac scheme, but rather be able to make
            '   Heading styles compatible with MacPac numbering.
            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                ConvertWordHeadingStyles()
            End If

            '   get num schemes
            iNumSchemes = SchemeCount(iSchemeType, oSource)
            If iNumSchemes = 0 Then
                ReDim xArray(0, 2)
                iGetSchemes = 0
                Exit Function
            End If

            ReDim xArray(iNumSchemes - 1, 2)
            For Each ltP In oSource.ListTemplates
                xScheme = ltP.Name
                If xScheme <> "" Then
                    '            If IsMacPacScheme(xScheme, , oSource) Then
                    If bIsMPListTemplate(ltP) Then
                        '               store name
                        xArray(j, 0) = GetLTRoot(xScheme)

                        '               store display name (alias)
                        xArray(j, 1) = GetField(xScheme, mpRecordFields.mpRecField_Alias, , oSource)

                        '               store scheme type (personal, document, firm)
                        xArray(j, 2) = iSchemeType

                        j = j + 1
                    End If
                End If
            Next ltP

            '   sort array
            CurWordApp.WordBasic.SortArray(xArray, 0, 0, UBound(xArray), 0, 1)

            iGetSchemes = iNumSchemes
        End Function

        Public Shared Function iGetListType(rngLocation As Word.Range) As Integer
            'returns type of list in listformat object -
            'could use WORD's .listtype, but it's buggy
            Dim iNumParagraphNumbers As Integer
            Dim iNumListNumNumbers As Integer


            With rngLocation.Paragraphs(1).Range.ListFormat
                '       count number of Native numbers
                iNumParagraphNumbers = .CountNumberedItems(WdNumberType.wdNumberParagraph)

                '       count number of ListNum numbers
                iNumListNumNumbers = .CountNumberedItems(WdNumberType.wdNumberListNum)

                '       determine type based on above counts
                If iNumParagraphNumbers > 0 And iNumListNumNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeMixed
                ElseIf iNumParagraphNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeNative
                ElseIf iNumListNumNumbers > 0 Then
                    iGetListType = mpListTypes.mpListTypeListNum
                Else
                    iGetListType = mpListTypes.mpListTypeNoNumbering
                End If
            End With
        End Function

        Public Shared Function bLoadFiles(Optional ByVal bIncludeTOCLink As Boolean = True) As Boolean
            Dim addNumbers As Word.AddIn
            Dim addFirmNumbers As Word.AddIn
            Dim xTOCSty As String
            Dim i As Integer

            If g_xUserPath = "" Then _
                GetNumAppPaths()

            '   ensure that mpNumbersStys are installed as add-ins
            xNumbersSty = g_xUserPath & "\tsgNumbers.sty"
            If Dir(xNumbersSty) = "" Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("Le fichier requis, ""tsgNumbers.sty""" &
                        ", n'a pas �t� trouv� dans les mod�les Word utilisateur ou dans le r�pertoire de l'utilisateur pr�cis� dans " & SettingsFileName & ". " &
                        "Veuillez contacter votre administrateur syst�me.", vbCritical, g_xAppName)
                Else
                    MsgBox("The required file, ""tsgNumbers.sty""" &
                        ", was not found in either your Word User " &
                        "Templates directory or in the User Directory  " &
                        "specifed in " & SettingsFileName & ".  " & vbCr & "Please contact " &
                        "your system administrator.", vbCritical, g_xAppName)
                End If
                Exit Function
            End If
            xFirmNumbersSty = mpNumSchemes.GetAppPath() & "\tsgNumbers.sty"
            If Dir(xFirmNumbersSty) = "" Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    MsgBox("Le fichier requis, ""tsgNumbers.sty""" &
                        ", n'a pas �t� trouv� dans le r�pertoire MacPac. " &
                        "Veuillez contacter votre administrateur syst�me.", vbCritical, g_xAppName)
                Else
                    MsgBox("The required file, ""tsgNumbers.sty""" &
                        ", was not found in your MacPac application " &
                        "directory.  " & vbCr & "Please contact your system " &
                        "administrator.", vbCritical, g_xAppName)
                End If
                Exit Function
            End If

            '   test for existence of tsgTOC.sty if link to TOC
            '   has been specified
            If bIncludeTOCLink Then
                xTOCSty = mpNumSchemes.GetAppPath() & "\tsgTOC.sty"
                If Dir(xTOCSty) = "" Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Le fichier requis, ""xTOC.sty""" &
                            ", n'a pas �t� trouv� dans le r�pertoire MacPac.  Veuillez contacter votre administrateur syst�me.", vbCritical, g_xAppName)
                    Else
                        MsgBox("The required file, ""xTOC.sty""" &
                            ", was not found in your MacPac " &
                            "application directory.  Please contact " &
                            "your system administrator.", vbCritical, g_xAppName)
                    End If
                    Exit Function
                End If
            End If

            On Error Resume Next
            Err.Number = 0

            Dim oAddIns As Word.AddIns
            oAddIns = CurWordApp.AddIns
            addNumbers = oAddIns(xNumbersSty)
            addFirmNumbers = oAddIns(xFirmNumbersSty)

            If Err.Number = 5941 Then
                '    ---remove any wrong project versions of tsgNumbers.sty & add
                '    ---correct version
                For i = oAddIns.Count To 1 Step -1
                    If InStr(UCase(CurWordApp.AddIns.Item(i).Name), "MPNUMBERS.STY") Or
                        InStr(UCase(CurWordApp.AddIns.Item(i).Name), "TSGNUMBERS.STY") Then
                        oAddIns.Item(i).Delete()
                    End If
                Next i
                Err.Number = 0
                oAddIns.Add(xNumbersSty, True)
                oAddIns.Add(xFirmNumbersSty, True)
                addNumbers = oAddIns(xNumbersSty)
                addFirmNumbers = oAddIns(xFirmNumbersSty)
            End If

            If addNumbers Is Nothing Then _
                    addNumbers = oAddIns.Add(xNumbersSty)
            If addFirmNumbers Is Nothing Then _
                    addFirmNumbers = oAddIns.Add(xFirmNumbersSty)
            oAddIns(xNumbersSty).Installed = True
            oAddIns(xFirmNumbersSty).Installed = True

            bLoadFiles = True
        End Function

        Public Shared Function iGetCurDefTOCScheme() As String
            'returns current TOC Scheme if available,
            'else "Generic"
            Dim iType As mpSchemeTypes
            Dim xScheme As String
            Dim xTemp As String

            On Error GoTo ProcError
            xTemp = CurWordApp.ActiveDocument _
                    .Variables("zzmpFixedCurScheme_9.0").Value

            If Len(xTemp) Then
                '       parse type and name from Doc Var
                iType = mpSchemeTypes.mpSchemeType_Document
                xScheme = Mid(xTemp, 2)

                '       get toc scheme from doc var
                iGetCurDefTOCScheme = GetField(xScheme, mpRecordFields.mpRecField_TOCScheme, iType)
            End If
            Exit Function

ProcError:
            iGetCurDefTOCScheme = ""
            Exit Function
        End Function

        Public Shared Function rngFormatTCHeading(rngTCCode As Word.Range,
                                  iLevel As Integer,
                                  bReformat As Boolean) As Word.Range
            'formats heading by applying font object of
            'TCEntry style - this function exists for
            'possible future customization of heading
            'formatting that cannot be accomodated by
            'TCEntry styles

            Dim rngHeading As Word.Range
            Dim styTCHeading As Word.Style
            Dim fldExisting As Word.Field
            Dim bUnderlineTrailChr As Boolean
            Dim xScheme As String
            Dim iTrailChr As mpTrailingChars
            Dim rngTrailChr As Word.Range
            Dim iTrailLen As Integer
            Dim iHeadingFormat As Integer
            Dim bApplyHeadingColor As Boolean
            Dim iUnderline As Integer

            '   get TC heading
            rngHeading = rngGetTCHeading(rngTCCode)

            'GLOG 2893 (11/7/11) - exclude tab/spaces after list num
            If rngHeading.Characters.Count > 2 Then
                If rngHeading.Characters(1).Fields.Count = 1 Then
                    If rngHeading.Characters(1).Fields(1).Type = WdFieldType.wdFieldListNum Then
                        If rngHeading.Characters(2).Fields.Count = 0 Then
                            rngHeading.MoveStart()
                            While (Left$(rngHeading.Text, 1) = Chr(9)) Or
                                    (Left$(rngHeading.Text, 1) = Chr(32))
                                rngHeading.MoveStart()
                            End While
                        End If
                    End If
                End If
            End If

            '   get list template name
            On Error Resume Next
            xScheme = rngHeading.ListFormat.ListTemplate.Name
            On Error GoTo 0

            If bReformat And Len(xScheme) Then
                '       get trailing characters and whether to underline them;
                '       this will only be possible when they're not part of number
                '        If IsMacPacScheme(xScheme, , Word.ActiveDocument) Then
                If bIsMPListTemplate(rngHeading.ListFormat.ListTemplate) Then
                    If CurWordApp.ActiveDocument.ListTemplates(xScheme) _
                        .ListLevels(iLevel).TrailingCharacter =
                                WdTrailingCharacter.wdTrailingNone Then

                        iTrailChr = xGetLevelProp _
                                           (xScheme,
                                            iLevel,
                                            mpNumLevelProps.mpNumLevelProp_TrailChr,
                                            mpSchemeTypes.mpSchemeType_Document)

                        iUnderline = xGetLevelProp _
                                            (xScheme,
                                            iLevel,
                                            mpNumLevelProps.mpNumLevelProp_TrailUnderline,
                                            mpSchemeTypes.mpSchemeType_Document)
                        bUnderlineTrailChr = (iUnderline And
                            mpTrailUnderlineFields.mpTrailUnderlineField_Underline)

                        '               set trailing character range
                        iTrailLen = Len(xGetTrailChrs(iTrailChr))
                        rngTrailChr = rngHeading.Duplicate
                        rngTrailChr.SetRange(rngHeading.Start,
                            rngHeading.Start + iTrailLen)

                        '               move start of heading past trailing characters
                        rngHeading.MoveStart(WdUnits.wdCharacter, iTrailLen)
                    End If

                    '           apply manual formats based on
                    '           appropriate level properties
                    '           for applied scheme

                    '           format info is held as bits
                    '           in an integer - get integer
                    iHeadingFormat = xGetLevelProp(xScheme,
                                              iLevel,
                                              mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                              mpSchemeTypes.mpSchemeType_Document)
                    lApplyFontFormat(rngHeading, iHeadingFormat)
                End If
            End If

            '   color heading
            On Error Resume Next
            bApplyHeadingColor = GetAppSetting("Numbering", "ApplyColorToHeadings")
            On Error GoTo 0
            If bApplyHeadingColor Then _
                rngHeading.Font.ColorIndex = iHeadingColor()

            '   underline trailing character
            If bUnderlineTrailChr Then _
                rngTrailChr.Underline = WdUnderline.wdUnderlineSingle

            '   return heading
            rngFormatTCHeading = rngHeading
        End Function

        Public Shared Function rngGetTCHeading(rngTCCode) As Word.Range
            '   get text from insertion to start of para
            With rngTCCode
                rngGetTCHeading = CurWordApp.ActiveDocument _
                    .Range(.Paragraphs(1).Range.Start, .Start)
                With rngGetTCHeading
                    .MoveStartWhile(CStr(Chr(12)))
                End With
            End With
        End Function

        Public Shared Function bSchemeIsStyleBased(xScheme As String) As Boolean
            'ALWAYS RETURN FALSE IN V9.0 - THERE IS NO SUCH
            'THING AS A STYLE BASED SCHEME IN V9.0
        End Function

        Public Shared Function lSetLevelProps(ByVal xScheme As String,
                                ByVal iLevel As Integer,
                                ByVal iSchemeType As mpSchemeTypes,
                                ByVal xValue As String) As Long
            'returns the delimited property string for specified
            'level and scheme of the listlevel.font object
            Dim oSource As Object
            Dim xLT As String

            Err.Clear()

            '   get source based on scheme type
            On Error Resume Next
            oSource = oSchemeSource(iSchemeType)
            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                xLT = GetFullLTName(xScheme)
            Else
                xLT = GetLTRoot(xScheme)
            End If

            If iSchemeType = mpSchemeTypes.mpSchemeType_TOC Then
                oSource.CustomDocumentProperties(
                    xScheme & "_L" & iLevel).Value = xValue
            Else
                SetPropFld(oSource.ListTemplates(xLT),
                    iLevel, xValue)
            End If

            '   return error value
            lSetLevelProps = Err.Number
        End Function

        Public Shared Function lSetNumLevelProps(ltScheme As Word.ListTemplate,
                                    iLevel As Integer,
                                    ByVal xValue As String,
                                    Optional bToFont As Boolean = False) _
                                    As Long
            'sets the delimited level property string
            'for specified list level of numbering scheme
            Err.Clear()

            On Error Resume Next
            If bToFont Then
                mpWDXX.SetPropFld(ltScheme.ListLevels(iLevel).Font,
                    xValue)
            Else
                SetPropFld(ltScheme, iLevel, xValue)
            End If

            '   return error value
            lSetNumLevelProps = Err.Number
        End Function

        Public Shared Function lSetFontPropToTNR(ByVal rngLN As Word.Range) As Long
            With rngLN
                If bPropFldNeedsReset(.Font) Then
                    If .Characters.Count = 1 Then _
                        .Expand(WdUnits.wdCharacter)
                    RestorePropFldToTNR(.Font)
                    .Font = .Font
                    While bPropFldNeedsReset(.Next(WdUnits.wdCharacter, 1).Font)
                        .MoveEnd(WdUnits.wdCharacter)
                        With .Characters.Last
                            RestorePropFldToTNR(.Font)
                            .Font = .Font
                        End With
                    End While
                    While bPropFldNeedsReset(.Previous(WdUnits.wdCharacter, 1).Font)
                        .MoveStart(WdUnits.wdCharacter, -1)
                        With .Characters.First
                            RestorePropFldToTNR(.Font)
                            .Font = .Font
                        End With
                    End While
                End If
            End With
        End Function

        Public Shared Function xGetNumLevelProps(ltScheme As Word.ListTemplate,
                                    iLevel As Integer,
                                    Optional bFromFont As Boolean = False) _
                                    As String
            On Error Resume Next
            If bFromFont Then
                xGetNumLevelProps =
                    mpWDXX.xGetPropFld(ltScheme.ListLevels(iLevel).Font)
            Else
                xGetNumLevelProps = xGetPropFld(ltScheme, iLevel)
            End If
        End Function

        Public Shared Function xGetLevelProps(ByVal xScheme As String,
                                ByVal iLevel As Integer,
                                ByVal iSchemeType As mpSchemeTypes) As String
            'returns the delimited property string for
            'specified level and scheme
            'of the listlevel.font object
            Dim oSource As Object
            Dim xProps As String
            Dim xLT As String

            '   get source based on scheme type
            On Error Resume Next
            oSource = oSchemeSource(iSchemeType)

            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                xLT = GetFullLTName(xScheme)
            Else
                xLT = GetLTRoot(xScheme)
            End If

            If iSchemeType = mpSchemeTypes.mpSchemeType_TOC Then
                xProps = oSource.CustomDocumentProperties(
                            xScheme & "_L" & iLevel).Value
            Else
                xProps = xGetPropFld(oSource.ListTemplates(xLT),
                    iLevel)
            End If
            xGetLevelProps = xProps
            oSource = Nothing

        End Function

        Public Shared Function bLevelExists(ByVal xScheme As String,
                              ByVal iLevel As Integer,
                              ByVal iSchemeType As mpSchemeTypes) As Boolean
            'returns TRUE if the level property of specified
            'level is not empty and is not tagged 'unavailable'
            Dim xProp As String
            On Error Resume Next
            xProp = xGetLevelProps(xScheme, iLevel, iSchemeType)
            bLevelExists = Len(xProp) And (xProp <> cConstants.mpPropNotAvailable)
        End Function

        Public Shared Function xGetLevelProp(xScheme As String,
                                iLevel As Integer,
                                iColumn As Integer,
                                iSchemeType As mpSchemeTypes) As String
            'returns the specified property for the specified level
            'of the specified list template in the specified source file -
            Dim i As Integer
            Dim iPrevious As Integer
            Dim iNext As Integer
            Dim xValue As String
            Dim oSource As Object
            Dim xLT As String

            On Error Resume Next

            '   column 1 for level 1 actually begins at column 4 -
            '   columns 1-3 are reserved for scheme properties-
            '   thus, increment iColumn by 3
            If iLevel = 1 Then
                iColumn = iColumn + 4
            End If

            '   get source based on scheme type
            oSource = oSchemeSource(iSchemeType)

            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                xLT = GetFullLTName(xScheme)
            Else
                xLT = GetLTRoot(xScheme)
            End If

            If iSchemeType = mpSchemeTypes.mpSchemeType_TOC Then
                '       get properties from custom doc props
                xValue = oSource.CustomDocumentProperties(
                            xScheme & "_L" & iLevel).Value
            Else
                xValue = xGetPropFld(oSource.ListTemplates(xLT),
                    iLevel)
            End If
            oSource = Nothing

            For i = 1 To iColumn
                iPrevious = InStr(iPrevious + 1, xValue, "|")
            Next i

            iNext = InStr(iPrevious + 1, xValue, "|")

            xGetLevelProp = Mid(xValue, iPrevious + 1,
                iNext - iPrevious - 1)
        End Function

        Public Shared Function lSetLevelProp(xScheme As String,
                                iLevel As Integer,
                                iColumn As Integer,
                                xValue As String,
                                iSchemeType As mpSchemeTypes) As Long
            'sets the specified property for the specified level
            'of the specified list template in the specified source file -
            Dim i As Integer
            Dim iPrev As Integer
            Dim iNext As Integer
            Dim xOld As String
            Dim xNew As String
            Dim oSource As Object
            Dim xLT As String

            On Error Resume Next

            '   column 1 for level 1 actually begins at column 4 -
            '   column 1-3 are reserved for scheme properties-
            '   thus, increment iColumn by 3
            If iLevel = 1 Then
                iColumn = iColumn + 4
            End If

            '   get source based on scheme type
            oSource = oSchemeSource(iSchemeType)
            If iSchemeType = mpSchemeTypes.mpSchemeType_Document Then
                xLT = GetFullLTName(xScheme)
            Else
                xLT = GetLTRoot(xScheme)
            End If

            xOld = xGetPropFld(oSource.ListTemplates(xLT), iLevel)

            For i = 1 To iColumn
                iPrev = InStr(iPrev + 1, xOld, "|")
            Next i
            iNext = InStr(iPrev + 1, xOld, "|")

            If iPrev = 0 Or iNext = 0 Then _
                Exit Function

            '   set value
            xNew = Left(xOld, iPrev) & xValue &
                   Right(xOld, Len(xOld) - iNext + 1)
            SetPropFld(oSource.ListTemplates(xLT),
                        iLevel, xNew)

            oSource = Nothing
        End Function

        Public Shared Function iGetLevels(ByVal xScheme As String,
                ByVal iSchemeType As mpSchemeTypes) As Integer
            'returns the number of levels in the requested scheme
            Dim i As Integer
            Dim oSource As Object
            Dim xProp As String
            Dim bLevelExists As Boolean
            Dim bExists As Boolean

            '   get source based on scheme type
            oSource = oSchemeSource(iSchemeType)

            For i = 9 To 1 Step -1
                '       get level property
                xProp = xGetLevelProps(xScheme, i, iSchemeType)
                '       level exists if the prop is not empty
                '       and it is not tagged 'unavailable'
                If Len(xProp) And xProp <> cConstants.mpPropNotAvailable Then
                    iGetLevels = i
                    Exit Function
                End If
            Next i

            If iGetLevels = 0 Then
                '       scheme is not a macpac
                '       scheme so assume 9 levels
                iGetLevels = 9
            End If
        End Function

        Public Shared Function IsMacPacScheme(xScheme As String,
                        Optional iSchemeType As mpSchemeTypes =
                        mpSchemeTypes.mpSchemeType_Private,
                        Optional oSource As Object = Nothing) As Boolean
            'returns true if xScheme is the name of a MacPac
            'numbering scheme in the oSource file -
            'macpac schemes are "zzmp" prefixed
            Dim xLTName As String
            Dim bIsSourceFile As Boolean
            Dim xLTScheme As String
            Dim xSource As String
            Dim ltScheme As Word.ListTemplate

            If xScheme = "" Then _
                Exit Function

            If oSource Is Nothing Then
                oSource = oSchemeSource(iSchemeType)
            End If

            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            If bIsSourceFile Then
                xLTScheme = GetLTRoot(xScheme)
            Else
                If Not oSource Is CurWordApp.ActiveDocument Then _
                    xSource = oSource.Name
                xLTScheme = GetFullLTName(xScheme, xSource)
            End If

            On Error Resume Next
            If Len(xScheme) And (xLTScheme <> "zzmp") Then
                '       this test is safer than the test below, which
                '       will disable mouse pointer in rest of Word session
                '       under certain circumstances
                For Each ltScheme In oSource.ListTemplates
                    If UCase(ltScheme.Name) = UCase(xLTScheme) Then
                        IsMacPacScheme =
                            (Left(xLTScheme, 4) = cConstants.mpPrefix) Or
                            (Left(xLTScheme, Len(cConstants.mpNativeHeadingScheme)) =
                            cConstants.mpNativeHeadingScheme)
                        Exit Function
                    End If
                Next ltScheme
                '        xLTName = oSource.ListTemplates(xLTScheme).Name
                '        If Len(xLTName) And xLTName <> "zzmp" Then
                '            IsMacPacScheme = _
                '                (Left(xLTName, 4) = mpPrefix) Or _
                '                (Left(xLTName, Len(mpNativeHeadingScheme)) = _
                '                mpNativeHeadingScheme)
                '        End If
            End If
        End Function

        Public Shared Function xGetTrailChrs(iTrailing As mpTrailingChars) As String
            'returns actual characters correponding to trailing char constants
            Select Case iTrailing
                Case mpTrailingChars.mpTrailingChar_None
                    xGetTrailChrs = ""
                Case mpTrailingChars.mpTrailingChar_Tab
                    xGetTrailChrs = vbTab
                Case mpTrailingChars.mpTrailingChar_Space
                    xGetTrailChrs = Space(1)
                Case mpTrailingChars.mpTrailingChar_DoubleSpace
                    xGetTrailChrs = Space(2)
                Case mpTrailingChars.mpTrailingChar_ShiftReturn
                    xGetTrailChrs = Chr(11)
                Case mpTrailingChars.mpTrailingChar_DoubleShiftReturn
                    xGetTrailChrs = Chr(11) & Chr(11)
            End Select

        End Function

        Public Shared Function bTOCLevelIsStyleBased(xScheme As String,
                                       iLevel As Integer) As Boolean
            'returns true if level is style-based
            Dim iHeadingFormat As Integer

            If xScheme = "" Then
                bTOCLevelIsStyleBased = False
                Exit Function
            End If
            iHeadingFormat = xGetLevelProp(xScheme,
                                            iLevel,
                                            mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                            mpSchemeTypes.mpSchemeType_Document)

            '   test bit for Heading Type (1 = style based)
            bTOCLevelIsStyleBased =
                (iHeadingFormat And mpTCFormatFields.mpTCFormatField_Type)
        End Function

        Public Shared Function iGetTOCSchemes(ByRef xArray(,) As String) As Integer
            'cycles through custom document properties in tsgNumbers.sty
            'to fill array with scheme names and aliases
            Dim i As Integer
            Dim iNumSchemes As Integer
            Dim oSource As Object
            Dim xValue As String
            Dim iPos As Integer
            On Error Resume Next

            '   point to appropriate source of scheme - based on type
            oSource = oSchemeSource(mpSchemeTypes.mpSchemeType_TOC)

            iNumSchemes = SchemeCount(mpSchemeTypes.mpSchemeType_TOC, oSource)
            If iNumSchemes = 0 Then
                ReDim xArray(0, 2)
                Exit Function
            End If

            ReDim xArray(iNumSchemes - 1, 2)
            For i = 0 To iNumSchemes - 1
                '       store display name (alias)
                xArray(i, 0) = xTranslateTOCSchemeDisplayName(GetTOCField(i + 1,
                                        mpTOCRecordFields.mpTOCRecField_Name,
                                        ,
                                        oSource))

                '       store name
                xArray(i, 1) = GetTOCField(i + 1,
                                        mpTOCRecordFields.mpTOCRecField_Alias,
                                        ,
                                        oSource)

                '       store scheme type (personal, document, firm)
                xArray(i, 2) = mpSchemeTypes.mpSchemeType_TOC
            Next i

            '   sort array
            CurWordApp.WordBasic.SortArray(xArray, 0, 0, UBound(xArray), 0, 1)

            iGetTOCSchemes = iNumSchemes
        End Function

        Public Shared ReadOnly Property TOCSchemeGeneric() As String
            Get
                TOCSchemeGeneric = mpTOCSchemeGeneric
            End Get
        End Property

        Public Shared Function GetTOCField(ByVal iID As Integer,
                         ByVal iField As mpTOCRecordFields,
                         Optional ByVal iType As mpSchemeTypes = mpSchemeTypes.mpSchemeType_Category,
                         Optional ByVal oSource As Object = Nothing) As String
            'returns specified field from doc prop with
            'name xName in template/document oSource
            Dim xTemp As String
            Dim iPos1 As Integer
            Dim iPos2 As Integer
            Dim i As Integer
            Dim oTempSource As Object

            '   get source from type if not supplied
            If (oSource Is Nothing) Then
                oTempSource = oSchemeSource(iType)
            Else
                oTempSource = oSource
            End If

            '   get value of doc prop
            On Error Resume Next
            xTemp = oTempSource.CustomDocumentProperties("Scheme" & iID).Value
            On Error GoTo 0
            oTempSource = Nothing

            If Len(xTemp) Then
                '       get pipe that precedes the requested field
                For i = 1 To iField
                    iPos1 = InStr(iPos1 + 1, xTemp, "|")
                Next i

                '       get pipe that succeeds the requested field
                iPos2 = InStr(iPos1 + 1, xTemp, "|")

                If iPos2 Then
                    '           return the contents in between the 2 pipes
                    GetTOCField = Mid(xTemp, iPos1 + 1, iPos2 - iPos1 - 1)
                Else
                    '           return the contents from pipe to end of string
                    GetTOCField = Mid(xTemp, iPos1 + 1)
                End If
            End If
        End Function

        Public Shared Function bIsMarkedAndFormatted(paraP As Word.Paragraph) As Boolean
            Dim iPos As Integer
            Dim iHFormat As Integer
            Dim ltP As Word.ListTemplate
            Dim rngP As Word.Range
            Dim xScheme As String
            Dim bShowAll As Boolean
            Dim xTCPrefix As String

            On Error Resume Next
            xTCPrefix = GetAppSetting("TOC", "TCPrefix")
            On Error GoTo 0
            If xTCPrefix = "" Then xTCPrefix = "TC"

            rngP = paraP.Range

            '   get if numbered
            If rngP.ListFormat.ListType = WdListType.wdListNoNumbering Then _
                Exit Function

            '   ensure that TC codes are displayed
            With CurWordApp.ActiveWindow.View
                bShowAll = .ShowAll
                .ShowAll = True
            End With

            '   get if marked for TOC
            iPos = InStr(rngP.Text, Chr(19) & " " & xTCPrefix)

            '   if para is marked for TOC...
            If iPos Then
                '       get list template
                ltP = rngP.ListFormat.ListTemplate
                xScheme = ltP.Name

                '       if scheme is a macpac scheme
                If bIsMPListTemplate(ltP) Then
                    '        If IsMacPacScheme(xScheme, mpSchemeType_Document) Then
                    '           get heading
                    rngP.StartOf()
                    rngP.MoveEnd(WdUnits.wdCharacter, iPos - 1)

                    '           get formats for level
                    iHFormat = xGetLevelProp(xScheme,
                                        rngP.ListFormat.ListLevelNumber,
                                        mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                        mpSchemeTypes.mpSchemeType_Document)

                    With rngP.Font
                        '               reformat only if the heading is
                        '               already formatted correctly for
                        '               the level - test what is against
                        '               what should be for the level
                        bIsMarkedAndFormatted =
                            Abs(.Bold) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Bold) And
                            Abs(.Italic) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Italic) And
                            Abs(.AllCaps) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_AllCaps) And
                            Abs(.Underline) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Underline) And
                            Abs(.SmallCaps) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_SmallCaps)
                    End With
                End If
            End If

            '   restore view
            CurWordApp.ActiveWindow.View.ShowAll = bShowAll

        End Function

        Public Shared Function bIsHeadingFormatted(paraP As Word.Paragraph) As Boolean
            'returns TRUE if first sentence of para is
            'formatted correctly for it's level.
            Dim iPos As Integer
            Dim iHFormat As Integer
            Dim ltP As Word.ListTemplate
            Dim rngP As Word.Range
            Dim xScheme As String

            rngP = paraP.Range

            '   get if numbered
            If rngP.ListFormat.ListType = WdListType.wdListNoNumbering Then _
                Exit Function

            '   get list template
            ltP = rngP.ListFormat.ListTemplate
            xScheme = ltP.Name

            '   if scheme is a macpac scheme
            '    If IsMacPacScheme(xScheme, mpSchemeType_Document) Then
            If bIsMPListTemplate(ltP) Then
                '       get first sentence
                With rngP
                    iPos = InStr(.Text, ".  ")
                    If iPos Then
                        .StartOf()
                        .MoveEnd(WdUnits.wdCharacter, iPos)
                    End If
                End With

                '       get formats for level
                iHFormat = xGetLevelProp(xScheme,
                                    rngP.ListFormat.ListLevelNumber,
                                    mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                    mpSchemeTypes.mpSchemeType_Document)
                If iHFormat <> 0 Then
                    With rngP.Font
                        '               reformat only if the heading is
                        '               already formatted correctly for
                        '               the level - test what is against
                        '               what should be for the level
                        bIsHeadingFormatted =
                            Abs(.Bold) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Bold) And
                            Abs(.Italic) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Italic) And
                            Abs(.AllCaps) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_AllCaps) And
                            Abs(.Underline) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_Underline) And
                            Abs(.SmallCaps) = iHasTCFormat(iHFormat, mpTCFormatFields.mpTCFormatField_SmallCaps)
                    End With
                End If
            End If
        End Function

        Public Shared Function iHasTCFormat(ByVal iHFormat As Integer,
                              ByVal iTCFormatField As mpTCFormatFields) As Integer
            'returns 1 if bit for tc format field
            'is turned on in iHFormat, else 0-
            'this does not return booleans because
            'chkboxes need values of 1
            iHasTCFormat = Abs(Int((iHFormat And Int(iTCFormatField)) > 0))
        End Function

        Public Shared Function bBitwisePropIsTrue(iPropertyValue As Integer, iBit As Integer) As Boolean
            bBitwisePropIsTrue = ((iPropertyValue And iBit) > 0)
        End Function

        Public Shared Function BackupProps(Optional ByVal xScheme As String = "") As Object
            'creates/updates doc vars that hold backups
            'of all scheme and level properties
            'for all schemes in active document
            Dim ltP As Word.ListTemplate
            Dim llp As Word.ListLevel
            Dim xDetail As String

            With CurWordApp.ActiveDocument
                If Len(xScheme) Then
                    '           backup specified scheme
                    On Error GoTo ProcError
                    ltP = .ListTemplates(GetFullLTName(xScheme))
                    On Error GoTo 0

                    For Each llp In ltP.ListLevels
                        '               build string of level props
                        xDetail = xDetail &
                                xGetPropFld(ltP, llp.Index)
                    Next llp
                    '           create/update var with scheme name
                    .Variables(GetLTRoot(ltP.Name)).Value = "|" & xDetail & "|"
                Else
                    '           backup all schemes in doc
                    For Each ltP In .ListTemplates
                        For Each llp In ltP.ListLevels
                            '                   build string of level props
                            xDetail = xDetail &
                                xGetPropFld(ltP, llp.Index)
                        Next llp
                        '               create/update var with scheme name
                        .Variables(GetLTRoot(ltP.Name)).Value = "|" & xDetail & "|"
                    Next ltP
                End If
            End With
            Exit Function

ProcError:
            Exit Function
        End Function

        Public Shared Function RestoreLevelProp(ByVal xScheme As String,
                                  ByVal iLevel As Integer) As Long
            'get backup prop from doc var
            Dim xValue As String
            Dim iPrev As Integer
            Dim iNext As Integer
            Dim i As Integer

            On Error GoTo ProcError

            '   get doc var - this carries all
            '   level prop info for scheme
            xValue = CurWordApp.ActiveDocument.Variables(xScheme)

            '   find starting point of level prop
            For i = 1 To iLevel
                iPrev = InStr(iPrev + 1, xValue, "||")
            Next i

            '   find starting point of next level prop
            iNext = InStr(iPrev + 1, xValue, "||")

            '   parse between start and end points -
            '   include starting and end pipe
            xValue = Mid(xValue, iPrev + 1,
                         iNext - iPrev)

            '   restore level prop
            SetPropFld(CurWordApp.ActiveDocument _
                .ListTemplates(GetFullLTName(xScheme)),
                iLevel, xValue)
            Exit Function
ProcError:
            '    MsgBox "Write code to reset entire scheme from a " & _
            '           "Public or Private scheme" & _
            '            vbCr & "(Scheme = " & Chr(34) & xScheme & Chr(34) & ")", , _
            '            "Programmer's Note"
            Exit Function
        End Function

        Public Shared Function RestoreProps(Optional ByVal xScheme As String = "") As Long
            'refill scheme level props from backup doc var
            'for specified scheme (xScheme).  If no scheme
            'is specified, do all schemes in doc
            Dim ltP As Word.ListTemplate
            Dim llp As Word.ListLevel

            With CurWordApp.ActiveDocument
                If Len(xScheme) Then
                    '           restore specified scheme
                    ltP = .ListTemplates(GetFullLTName(xScheme))
                    For Each llp In ltP.ListLevels
                        '               restore level prop
                        RestoreLevelProp(xScheme, llp.Index)
                    Next llp
                Else
                    '           restore all schemes in doc
                    For Each ltP In .ListTemplates
                        If bIsMPListTemplate(ltP) Then
                            For Each llp In ltP.ListLevels
                                '                       restore level prop
                                RestoreLevelProp(ltP.Name, llp.Index)
                            Next llp
                        End If
                    Next ltP
                End If
            End With
        End Function

        Public Shared Function GetAllProps() As Object
            'returns array filled with all scheme
            'properties in active document
            Dim ltP As Word.ListTemplate
            Dim llp As Word.ListLevel
            Dim i As Integer
            i = CurWordApp.ActiveDocument.ListTemplates.Count - 1
            Dim xSchemeDetail(i, 0 To 9)
            i = 0
            For Each ltP In CurWordApp.ActiveDocument.ListTemplates
                xSchemeDetail(i, 0) = ltP.Name
                For Each llp In ltP.ListLevels
                    xSchemeDetail(i, llp.Index) = xGetPropFld(ltP, llp.Index)
                Next llp
                i = i + 1
            Next ltP
            GetAllProps = xSchemeDetail
        End Function

        Public Shared Function xGetStyleRoot(ByVal xScheme As String) As String
            On Error Resume Next
            If Left(xScheme, 4) = cConstants.mpPrefix Then
                xGetStyleRoot = Mid(GetLTRoot(xScheme), 5)
            ElseIf xScheme = cConstants.mpNativeHeadingScheme Then
                xGetStyleRoot = "Heading"
            End If
            Err.Clear()
        End Function

        Public Shared Function xGetStyleName(ByVal xScheme As String,
                              ByVal iLevel As Integer,
                              Optional ByVal xStyleRoot As String = "") As String
            'returns the style name for the given
            'scheme and level - uses xStyleRoot if supplied (for speed)
            If xStyleRoot = "Heading" Then
                If bIsHeadingScheme("HeadingStyles") Then
                    xGetStyleName = xTranslateHeadingStyle(iLevel)
                Else
                    xGetStyleName = "Heading_L" & iLevel
                End If
            ElseIf Len(xStyleRoot) Then
                xGetStyleName = xGetStyleRoot(xScheme) & "_L" & iLevel
            Else
                If bIsHeadingScheme(xScheme) Then
                    xGetStyleName = xTranslateHeadingStyle(iLevel)
                Else
                    xGetStyleName = xGetStyleRoot(xScheme) & "_L" & iLevel
                End If
            End If
        End Function

        Public Shared Function bIsMPListTemplate(ltP As Word.ListTemplate) As Boolean
            'returns TRUE if ltP is a MacPac list template-
            'this will be the case if the name of the
            'lt is prefixed with mpPrefix
            Dim oRecs As cSchemeRecords

            On Error Resume Next
            bIsMPListTemplate = (Left(ltP.Name, 4) = mpPrefix)
            If Not bIsMPListTemplate Then
                bIsMPListTemplate = (Left(ltP.Name, Len(mpNativeHeadingScheme)) =
                    mpNativeHeadingScheme)
            End If
        End Function

        Public Shared Function bIsHeadingScheme(ByVal xScheme As String) As Boolean
            On Error Resume Next
            bIsHeadingScheme = (xHeadingScheme() = xScheme)
        End Function

        Public Shared Function xHeadingScheme() As String
            'returns the name of the MacPac
            'scheme that uses Word Heading styles-
            'else returns EMPTY
            Dim ltP As Word.ListTemplate
            Dim styHeading As Word.Style

            On Error Resume Next
            styHeading = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleHeading1)
            If styHeading Is Nothing Then _
                Return ""

            '   get list template linked to Heading 1 style
            With styHeading
                '       test for InUse so as not to make style InUse
                If .InUse Then
                    '           get attached list template
                    ltP = .ListTemplate
                    Err.Clear()
                    xHeadingScheme = GetLTRoot(ltP.Name)
                Else
                    xHeadingScheme = ""
                End If
            End With
        End Function

        Public Shared Function bHeadingSchemeIsNativeWord() As Boolean
            'returns TRUE if the Heading Scheme of the active
            'document is not a MacPac scheme
            Dim bMacPacScheme As Boolean
            Dim xScheme As String

            xScheme = xHeadingScheme()

            bMacPacScheme = IsMacPacScheme(xScheme,
                                           ,
                                           CurWordApp.ActiveDocument)
            bHeadingSchemeIsNativeWord = (Not bMacPacScheme) Or
                                         xScheme = mpNativeHeadingScheme

        End Function

        Public Shared Function ConvertWordHeadingStyles() As Boolean
            'converts native heading 1-9 styles to a MacPac
            'numbering scheme if heading 1 is attached to a
            'list template - returns TRUE if a conversion
            'executed successfully
            Dim ltP As Word.ListTemplate
            Dim i As Integer
            Dim iFormats As Integer
            Dim bHeadingLinked As Boolean
            Dim bIsSaved As Boolean
            Dim ltDest As Word.ListTemplate
            Dim xLT As String
            Dim styHeading As Word.Style

            On Error Resume Next
            styHeading = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleHeading1)
            If styHeading Is Nothing Then _
                Exit Function
            On Error GoTo 0

            '   store saved property
            bIsSaved = CurWordApp.ActiveDocument.Saved

            '   check for native Word Heading styles
            With styHeading
                '       do if heading 1 is in use and
                '       is linked to a list template
                If .InUse Then
                    If Not (.ListTemplate Is Nothing) Then
                        ltP = .ListTemplate
                        '               do only if not already a MacPac list template w/props in name
                        If ((ltP.ListLevels.Count = 9) And
                                Not bIsMPListTemplate(ltP)) Or
                                (ltP.Name = mpNativeHeadingScheme) Then
                            '                   this to account for possibility that "HeadingStyles"
                            '                   is already in doc, but Heading 1-9 have been manually
                            '                   linked to a different, non-MacPac list template
                            If (ltP.Name <> mpNativeHeadingScheme) And
                                    bListTemplateExists(mpNativeHeadingScheme) Then
                                ltDest = CurWordApp.ActiveDocument _
                                    .ListTemplates(GetFullLTName(mpNativeHeadingScheme))
                                ltDest.Name = ""
                            End If

                            i = 1
                            '                   cycle through list levels until we
                            '                   reach a level that is not linked
                            '                   to the appropriate heading style
                            Do
                                '                       set Heading properties field based
                                '                       on font formats in the style
                                iFormats = iGetFontFormat(CurWordApp.ActiveDocument _
                                    .Styles(xTranslateHeadingStyle(i)).Font)

                                If i = 1 Then
                                    '                       copy props into doc list template
                                    xLT = "|Heading|3|3|0|1|0|" & iFormats & "|"
                                Else
                                    '                           copy props into doc list template
                                    xLT = xLT & "|1|0|" & iFormats & "|"
                                End If

                                '                       check if next level is linked to
                                '                       the appropriate heading style
                                If i < 9 Then
                                    i = i + 1
                                    Dim xLinked As String
                                    xLinked = ltP.ListLevels(i).LinkedStyle
                                    If xLinked <> "" Then
                                        bHeadingLinked =
                                            (CurWordApp.ActiveDocument.Styles(xLinked).NameLocal =
                                            xTranslateHeadingStyle(i))
                                    End If
                                Else
                                    bHeadingLinked = False
                                End If
                            Loop While bHeadingLinked

                            If i < 9 Then
                                For i = i To 9
                                    '                           copy props into doc list template
                                    On Error Resume Next
                                    xLT = xLT & mpPropNotAvailable
                                    On Error GoTo 0
                                Next i
                            End If

                            '                   rename list template
                            ltP.Name = mpNativeHeadingScheme & "|" & xLT & "|"

                            ConvertWordHeadingStyles = True
                        End If
                    End If
                End If
            End With

            '   don't prompt to save unless other
            '   changes were previously made
            If bIsSaved Then _
                CurWordApp.ActiveDocument.Saved = True
        End Function

        Public Shared Function iGetFontFormat(oFont As Word.Font) As Integer
            'returns a bit field integer representing the
            'formats of the specified font
            Dim iHeadingFormat As Integer
            With oFont
                '       set bit for heading type
                iHeadingFormat = iHeadingFormat Or
                                 mpTCFormatFields.mpTCFormatField_Type

                '       set bit for heading bold
                If .Bold Then
                    iHeadingFormat = iHeadingFormat Or
                                     mpTCFormatFields.mpTCFormatField_Bold
                End If

                '       set bit for heading all caps
                If .AllCaps Then
                    iHeadingFormat = iHeadingFormat Or
                                     mpTCFormatFields.mpTCFormatField_AllCaps
                End If

                '       set bit for heading italic
                If .Italic Then
                    iHeadingFormat = iHeadingFormat Or
                                     mpTCFormatFields.mpTCFormatField_Italic
                End If

                '       set bit for heading small caps
                If .SmallCaps Then
                    iHeadingFormat = iHeadingFormat Or
                                     mpTCFormatFields.mpTCFormatField_SmallCaps
                End If

                '       set bit for heading small caps
                If .Underline Then
                    iHeadingFormat = iHeadingFormat Or
                                     mpTCFormatFields.mpTCFormatField_Underline
                End If
            End With

            iGetFontFormat = iHeadingFormat
        End Function

        Public Shared Function iGetHeadingStyleFontFormat(ByVal iLevel As Integer) As Integer
            'just to maintain compatibility
        End Function

        Public Shared Function bDocProtected(docP As Word.Document,
                                      bAlert As Boolean) As Boolean
            If docP.ProtectionType <> WdProtectionType.wdNoProtection Then
                If bAlert Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("La fonction s�lectionn�e n'est pas disponible parce que le document actuel est prot�g�. Pour utiliser cette fonction, vous devez �ter la protection du document.", vbInformation, g_xAppName)
                    Else
                        MsgBox("The function you have selected is not " &
                               "available because the current document " &
                               "is protected from changes." & vbCr &
                               "To use this function, you must first " &
                               "unprotect the document.", vbInformation, g_xAppName)
                    End If
                End If
                bDocProtected = True
            End If
        End Function

        Public Shared Function UpgradeListTemplate(ltP As Word.ListTemplate)
            Dim llp As Word.ListLevel

            '   cycle through list levels
            For Each llp In ltP.ListLevels
                '       move props from NameFarEast to NameBi
                mpWDXX.UpgradeLevelProp(llp)
            Next llp
        End Function

        Public Shared Sub CreateLevelProps(tmpSource As Word.Template, ByVal xScheme As String)
            Dim i As Integer
            For i = 1 To 9
                tmpSource.CustomDocumentProperties.Add(
                    xScheme & i, False, MsoDocProperties.msoPropertyTypeString, "")
            Next i
        End Sub

        Public Shared Sub TransferPropsToDocProps(oSource As Object)
            'moves level props to from NameFarEast to
            'Document Properties in specified template
            Dim ltP As Word.ListTemplate
            Dim i As Integer

            '   cycle through list templates
            For Each ltP In oSource.ListTemplates
                '       do only MacPac ones
                If bIsMPListTemplate(ltP) Then
                    '           cycle through list levels
                    For i = 1 To 9
                        '               move props to doc props
                        MoveLevelProps(ltP.Name)
                    Next i
                End If
            Next ltP

            '   mark as a source file
            oSource.CustomDocumentProperties.Add(
                "MPN90SourceFile", False, MsoDocProperties.msoPropertyTypeBoolean, True)
            oSource.Save()
        End Sub

        Public Shared Sub MoveLevelProps(ByVal xScheme As String)
            'moves the props for a scheme from
            Dim i As Integer
            Dim xValue As String

            '   cycle through list levels
            For i = 1 To 9
                xValue = xGetLevelProps(xScheme,
                                        i,
                                        mpSchemeTypes.mpSchemeType_Document)
                '       create doc prop with value of current level prop
                CurWordApp.ActiveDocument.CustomDocumentProperties.Add(
                    xScheme & i, False, MsoDocProperties.msoPropertyTypeString,
                    xValue)
                '       clear out current level prop
                lSetLevelProps(xScheme, i, mpSchemeTypes.mpSchemeType_Document, "###")
            Next i
        End Sub
        Public Shared Sub MoveOldPropsToDocProps(oSource As Object, ByVal xScheme As String)
            'moves the props for a scheme from
            Dim i As Integer
            Dim xValue As String

            '   cycle through list levels
            For i = 1 To 9
                xValue = xGetLevelProps(xScheme,
                                        i,
                                        mpSchemeTypes.mpSchemeType_Document)
                '       create doc prop with value of current level prop
                CurWordApp.ActiveDocument.CustomDocumentProperties.Add(
                    xScheme & i, False, MsoDocProperties.msoPropertyTypeString,
                    xValue)
                '       clear out current level prop
                lSetLevelProps(xScheme, i, mpSchemeTypes.mpSchemeType_Document, "###")
            Next i
        End Sub

        Sub CopyLevelProps(ByVal xScheme As String,
                           xSource As String,
                           xDest As String)
            'copies all level props of scheme xScheme
            'from oSource to oDest
            Dim i As Integer
            Dim xProp As String
            Dim xValue As String
            Dim oDest As Object
            Dim oSource As Object
            Dim xLT As String

            '   get source file
            If xSource = g_xUserPath & "\tsgNumbers.sty" Then
                'this branch was added in 9.9.3006
                If g_oPNumSty Is Nothing Then _
                        g_oPNumSty = mpNumSchemes.GetTemplate(xSource)
                oSource = g_oPNumSty
            Else
                On Error Resume Next
                oSource = mpNumSchemes.GetTemplate(xSource)
                On Error GoTo 0
                If oSource Is Nothing Then
                    oSource = CurWordApp.Documents(xSource)
                End If
            End If

            '   get destination file
            On Error Resume Next
            oDest = mpNumSchemes.GetTemplate(xDest)
            On Error GoTo 0
            If oDest Is Nothing Then
                oDest = CurWordApp.Documents(xDest)
            End If

            On Error Resume Next
            '   do levels 1 to 9
            For i = 1 To 9
                xValue = ""
                '       get source prop name and value
                xProp = xScheme & i
                xValue = oSource.CustomDocumentProperties(xProp).Value
                If xValue = "" Then
                    xLT = GetFullLTName(xScheme)
                    xValue = xGetPropFld(oSource.ListTemplates(xLT), i)
                End If
                With oDest
                    If (.CustomDocumentProperties(xProp) Is Nothing) Then
                        '               prop does not exist in dest - create
                        .CustomDocumentProperties.Add(
                            xProp, False, MsoDocProperties.msoPropertyTypeString, xValue)
                    Else
                        '               prop exists in dest - edit value
                        .CustomDocumentProperties(xProp).Value = xValue
                    End If
                End With
            Next i
            On Error GoTo 0
        End Sub

        Public Shared Function bListTemplateExists(xScheme As String) As Boolean
            'returns TRUE if the doc contains a list template named xScheme
            Dim ltScheme As Word.ListTemplate
            Dim xLT As String

            For Each ltScheme In CurWordApp.ActiveDocument.ListTemplates
                xLT = ltScheme.Name
                If xLT <> "" Then
                    If GetLTRoot(xLT) = xScheme Then
                        bListTemplateExists = True
                        Exit Function
                    End If
                End If
            Next ltScheme
        End Function

        Public Shared Function lSetAdminFlag(bIsAdmin As Boolean) As Long
            g_bIsAdmin = bIsAdmin
        End Function

        Public Shared Function iGetTOCSchemeIndex(xTOCScheme As String) As Integer
            'returns index of xTOCScheme
            Dim oSource As Object
            Dim i As Integer
            Dim iNumSchemes As Integer

            oSource = oSchemeSource(mpSchemeTypes.mpSchemeType_TOC)

            iNumSchemes = SchemeCount(mpSchemeTypes.mpSchemeType_TOC, oSource)
            For i = 1 To iNumSchemes
                If xTranslateTOCSchemeDisplayName(GetTOCField(i,
                        mpTOCRecordFields.mpTOCRecField_Name, , oSource)) = xTOCScheme Then
                    iGetTOCSchemeIndex = i
                    Exit Function
                End If
            Next i
        End Function

        Public Shared Function xGetLTRoot(xLT As String) As String
            xGetLTRoot = GetLTRoot(xLT)
        End Function

        Public Shared Function xGetFullLTName(xScheme As String,
                                Optional xSource As String = "") As String
            xGetFullLTName = GetFullLTName(xScheme, xSource)
        End Function

        Public Shared Function bPropFldNeedsReset(oFont As Word.Font) As Boolean
            bPropFldNeedsReset = mpWDXX.bPropFldNeedsReset(oFont)
        End Function

        Public Shared Function RestorePropFldToTNR(oFont As Word.Font) As Long
            RestorePropFldToTNR = mpWDXX.RestorePropFldToTNR(oFont)
        End Function

        Public Shared Function lApplyFontFormat(rngLocation As Word.Range,
                                        iFormat As Integer) As Long
            'applies font attributes represented by bit field integer
            With rngLocation.Font
                .Bold = iHasTCFormat(iFormat,
                                     mpTCFormatFields.mpTCFormatField_Bold)
                .Italic = iHasTCFormat(iFormat,
                                     mpTCFormatFields.mpTCFormatField_Italic)
                .AllCaps = iHasTCFormat(iFormat,
                                     mpTCFormatFields.mpTCFormatField_AllCaps)
                .Underline = iHasTCFormat(iFormat,
                                     mpTCFormatFields.mpTCFormatField_Underline)
                .SmallCaps = iHasTCFormat(iFormat,
                                     mpTCFormatFields.mpTCFormatField_SmallCaps)
            End With
        End Function

        Public Shared Function xTranslateHeadingStyle(iLevel As Integer) As String
            'gets local name of iLevel Word Heading style
            With CurWordApp.ActiveDocument
                Select Case iLevel
                    Case 1
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading1).NameLocal
                    Case 2
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading2).NameLocal
                    Case 3
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading3).NameLocal
                    Case 4
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading4).NameLocal
                    Case 5
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading5).NameLocal
                    Case 6
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading6).NameLocal
                    Case 7
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading7).NameLocal
                    Case 8
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading8).NameLocal
                    Case 9
                        xTranslateHeadingStyle =
                            .Styles(WdBuiltinStyle.wdStyleHeading9).NameLocal
                End Select
            End With
        End Function

        Public Shared Function xTranslateBuiltInStyle(xStyle) As String
            'this is useful when style is variable, e.g. next paragraph
            'style list in ini
            With CurWordApp.ActiveDocument
                If xStyle = "Normal" Then
                    xTranslateBuiltInStyle =
                        .Styles(WdBuiltinStyle.wdStyleNormal).NameLocal
                ElseIf xStyle = "Body Text" Then
                    xTranslateBuiltInStyle =
                        .Styles(WdBuiltinStyle.wdStyleBodyText).NameLocal
                ElseIf Left(xStyle, 8) = "Heading " Then
                    xTranslateBuiltInStyle =
                        xTranslateHeadingStyle(Right(xStyle, 1))
                Else
                    xTranslateBuiltInStyle = xStyle
                End If
            End With
        End Function

        Public Shared Function bIsHeadingStyle(xStyle As String) As Boolean
            'returns TRUE if xStyle is a Word Heading style
            With CurWordApp.ActiveDocument
                Select Case xStyle
                    Case .Styles(WdBuiltinStyle.wdStyleHeading1).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading2).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading3).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading4).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading5).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading6).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading7).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading8).NameLocal
                        bIsHeadingStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleHeading9).NameLocal
                        bIsHeadingStyle = True
                    Case Else
                        bIsHeadingStyle = False
                End Select
            End With
        End Function

        Public Shared Function xTranslateTOCStyle(iLevel As Integer) As String
            'gets local name of iLevel TOC style
            With CurWordApp.ActiveDocument
                Select Case iLevel
                    Case 1
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC1).NameLocal
                    Case 2
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC2).NameLocal
                    Case 3
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC3).NameLocal
                    Case 4
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC4).NameLocal
                    Case 5
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC5).NameLocal
                    Case 6
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC6).NameLocal
                    Case 7
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC7).NameLocal
                    Case 8
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC8).NameLocal
                    Case 9
                        xTranslateTOCStyle =
                            .Styles(WdBuiltinStyle.wdStyleTOC9).NameLocal
                End Select
            End With
        End Function

        Public Shared Function bIsTOCStyle(xStyle As String) As Boolean
            'returns TRUE if xStyle is a Word TOC style
            With CurWordApp.ActiveDocument
                Select Case xStyle
                    Case .Styles(WdBuiltinStyle.wdStyleTOC1).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC2).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC3).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC4).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC5).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC6).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC7).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC8).NameLocal
                        bIsTOCStyle = True
                    Case .Styles(WdBuiltinStyle.wdStyleTOC9).NameLocal
                        bIsTOCStyle = True
                    Case Else
                        bIsTOCStyle = False
                End Select
            End With
        End Function

        Public Shared Function CreateSessionVariable() As Long
            Randomize()
            g_sSession = Rnd()
        End Function

        Public Shared Function bIsConverted() As Boolean
            On Error Resume Next
            bIsConverted =
                (CurWordApp.ActiveDocument.Variables("zzmpnSession").Value =
                Trim(g_sSession))
        End Function

        Public Shared Function MarkAsConverted() As Long
            On Error Resume Next
            With CurWordApp.ActiveDocument.Variables
                .Add("zzmpnSession")
                .Item("zzmpnSession").Value = g_sSession
            End With
        End Function

        Public Shared Function xActivateFirstScheme() As String
            '   if there are 9.0 schemes, ensure that one is active
            Dim x80Scheme As String
            Dim x90Scheme As String
            Dim iDocSchemes As Integer
            Dim xDSchemes(,) As String

            With CurWordApp.ActiveDocument
                If .Name = "tsgNumbers.sty" Then Exit Function
                On Error Resume Next
                '       get MacPac Numbering 9.0 scheme
                x90Scheme = .Variables("zzmpFixedCurScheme_9.0").Value
                '       get MacPac Numbering 8.0 scheme
                x80Scheme = .Variables("zzmpFixedCurScheme").Value
                On Error GoTo 0

                '6/30/09 (9.9.2007) - modified the following conditional to activate
                'first 9.x scheme regardless of whether there's an 8.x variable
                '        If (x80Scheme = Empty) And (x90Scheme = Empty) Then
                If x90Scheme = "" Then
                    '           no scheme is active
                    iDocSchemes = iGetSchemes(xDSchemes,
                        mpSchemeTypes.mpSchemeType_Document)

                    If iDocSchemes > 0 Then
                        '               activate first scheme
                        x90Scheme = "1" & xDSchemes(0, 0)
                        .Variables("zzmpFixedCurScheme_9.0").Value = x90Scheme
                        .Variables("zzmpFixedCurScheme").Value = Mid(x90Scheme, 6)
                        '               return name of activated scheme
                        xActivateFirstScheme = xDSchemes(0, 0)
                    End If
                End If
            End With
        End Function

        Public Shared Function iGetWordHeadingLevel(xStyle As String) As Integer
            'returns level of Word Heading style; if xStyle is not a
            'Word heading style, returns 0
            With CurWordApp.ActiveDocument
                Select Case xStyle
                    Case .Styles(WdBuiltinStyle.wdStyleHeading1).NameLocal
                        iGetWordHeadingLevel = 1
                    Case .Styles(WdBuiltinStyle.wdStyleHeading2).NameLocal
                        iGetWordHeadingLevel = 2
                    Case .Styles(WdBuiltinStyle.wdStyleHeading3).NameLocal
                        iGetWordHeadingLevel = 3
                    Case .Styles(WdBuiltinStyle.wdStyleHeading4).NameLocal
                        iGetWordHeadingLevel = 4
                    Case .Styles(WdBuiltinStyle.wdStyleHeading5).NameLocal
                        iGetWordHeadingLevel = 5
                    Case .Styles(WdBuiltinStyle.wdStyleHeading6).NameLocal
                        iGetWordHeadingLevel = 6
                    Case .Styles(WdBuiltinStyle.wdStyleHeading7).NameLocal
                        iGetWordHeadingLevel = 7
                    Case .Styles(WdBuiltinStyle.wdStyleHeading8).NameLocal
                        iGetWordHeadingLevel = 8
                    Case .Styles(WdBuiltinStyle.wdStyleHeading9).NameLocal
                        iGetWordHeadingLevel = 9
                    Case Else
                        iGetWordHeadingLevel = 0
                End Select
            End With
        End Function

        Public Shared Function iGetProprietaryLevel(ByVal xRoot As String,
                                             ByVal xStyle As String) As Integer
            'returns level of proprietary style; if xStyle does not use
            'xRoot, returns 0
            xRoot = xRoot & "_L"
            If InStr(UCase(xStyle), UCase(xRoot)) Then
                On Error Resume Next
                iGetProprietaryLevel = Val(Mid(xStyle, Len(xRoot) + 1, 1))
            End If
        End Function

        Public Shared Function StripStyleAlias(xStyle) As String
            'strips alias from style name
            Dim iPos As Integer
            Dim xDecimal As String

            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
            If xDecimal = "," Then
                iPos = InStr(xStyle, ";")
            Else
                iPos = InStr(xStyle, ",")
            End If

            If iPos <> 0 Then
                StripStyleAlias = Left(xStyle, iPos - 1)
            Else
                StripStyleAlias = xStyle
            End If
        End Function

        Public Shared Function GetStandardIncrement(iUnit As WdMeasurementUnits) As Single
            'returns Word's standard increment for unit of measurement
            Select Case iUnit
                Case WdMeasurementUnits.wdInches, WdMeasurementUnits.wdCentimeters
                    GetStandardIncrement = 0.1
                Case Else
                    GetStandardIncrement = 1
            End Select
        End Function

        Public Shared Function GetResetOnHigherLevel(oLL As Word.ListLevel) As Long
            GetResetOnHigherLevel = oLL.Parent.Parent.ListLevels(oLL.Index).ResetOnHigher
        End Function

        Public Shared Sub SetResetOnHigherLevel(oLL As Word.ListLevel, lLevel As Long)
            oLL.Parent.Parent.ListLevels(oLL.Index).ResetOnHigher = lLevel
        End Sub

        Public Shared Sub DeleteCorruptedVariables()
            'checks format of 9.x active scheme document variable and, if invalid,
            'deletes all of the generic numbering variables - added 6/30/09 (9.9.2007)
            'to deal with corruption caused by Office 2007 security update KB969604
            Dim xValue As String
            Dim bDelete As Boolean

            On Error Resume Next

            xValue = CurWordApp.ActiveDocument.Variables("zzmpFixedCurScheme_9.0").Value
            If xValue <> "" Then
                xValue = UCase(xValue)
                If Len(xValue) < 5 Then
                    bDelete = True
                Else
                    xValue = Mid(xTrimSpaces(xValue), 2)
                    bDelete = ((Left$(xValue, 4) <> "ZZMP") And (xValue <> "HEADINGSTYLES"))
                End If

                If bDelete Then
                    With CurWordApp.ActiveDocument.Variables
                        .Item("zzmpFixedCurScheme_9.0").Delete()
                        .Item("zzmpFixedCurScheme").Delete()
                        .Item("zzmpnSession").Delete()
                        .Item("zzmpLTFontsClean").Delete()
                    End With
                End If
            End If
        End Sub

        Public Shared Function GetLanguageFromIni() As Long
            Dim xValue As String
            xValue = GetAppSetting("General", "UILanguage")
            If xValue <> "" Then
                g_lUILanguage = CLng(xValue)
                If g_lUILanguage = 0 Then _
                    g_lUILanguage = WdLanguageID.wdEnglishUS
            End If
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                g_xAppName = "Num�rotation TSG"
                LMP.Error.Language = [Error].mpErrorLanguages.French
            Else
                g_xAppName = "TSG Numbering"
                LMP.Error.Language = [Error].mpErrorLanguages.English
            End If

            GetLanguageFromIni = g_lUILanguage
        End Function

        Public Shared Function xTranslateTOCSchemeDisplayName(xName As String) As String
            Dim xTranslated As String

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                Select Case xName
                    Case "Capped Level 1"
                        xTranslated = "Niveau 1 sup�rieur"
                    Case "Centered Level 1"
                        xTranslated = "Niveau 1 centr�"
                    Case "Distinct Level 1"
                        xTranslated = "Niveau 1 diff�rent"
                    Case "Generic"
                        xTranslated = "G�n�rique"
                    Case Else
                        xTranslated = xName
                End Select
            Else
                xTranslated = xName
            End If

            xTranslateTOCSchemeDisplayName = xTranslated
        End Function

        Public Shared Function GetSplitHeadingStyle(ByVal xScheme As String, ByVal iLevel As Integer,
                Optional ByVal bCreateIfNecessary As Boolean = True) As Word.Style
            Dim oStyle As Word.Style
            Dim xStyle As String

            xStyle = xGetStyleRoot(xScheme) & " Heading_L" & CStr(iLevel)

            On Error Resume Next
            oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
            On Error GoTo 0

            If bCreateIfNecessary And (oStyle Is Nothing) Then
                oStyle = CurWordApp.ActiveDocument.Styles.Add(xStyle)
                oStyle.BaseStyle = xGetStyleName(xScheme, iLevel)
            End If

            GetSplitHeadingStyle = oStyle
        End Function

        Public Shared Function GetSplitParaStyle(ByVal xScheme As String, ByVal iLevel As Integer,
                Optional ByVal bCreateIfNecessary As Boolean = True) As Word.Style
            Dim oStyle As Word.Style
            Dim xStyle As String
            Dim oBaseStyle As Word.Style

            xStyle = xGetStyleRoot(xScheme) & " Para " & CStr(iLevel)

            On Error Resume Next
            oStyle = CurWordApp.ActiveDocument.Styles(xStyle)
            On Error GoTo 0

            If bCreateIfNecessary And (oStyle Is Nothing) Then
                'add unlinked paragraph style
                CurWordApp.ActiveDocument.Styles.Add(xStyle, WdStyleType.wdStyleTypeParagraphOnly)
                With oStyle
                    'base on numbered style
                    .BaseStyle = xGetStyleName(xScheme, iLevel)

                    '9.9.5011 - prevent para style from picking up the
                    'direct formatting of selected paragraph
                    oBaseStyle = CurWordApp.ActiveDocument.Styles(.BaseStyle)
                    .Font = oBaseStyle.Font
                    .ParagraphFormat = oBaseStyle.ParagraphFormat

                    'remove numbering
                    .LinkToListTemplate(Nothing)

                    'remove outline level
                    .ParagraphFormat.OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText

                    'this will guarantee no frame or borders when cursor
                    'happens to be in a paragraph with one of these
                    On Error Resume Next
                    .Borders.Enable = False
                    .Frame.Delete()
                    On Error GoTo 0

                    'match the way the numbered style wraps - this won't happen automatically
                    'because the numbered style gets its indents from the list template
                    .ParagraphFormat.LeftIndent = oBaseStyle.ParagraphFormat.LeftIndent

                    'next paragraph style
                    .NextParagraphStyle = oBaseStyle.NextParagraphStyle
                End With
            End If

            GetSplitParaStyle = oStyle
        End Function

        'VB6 Compatibility replacement functions
        Public Shared Function TwipsToPixelsY(iTwips As Integer) As Single
            'Replacement for VB6 function
            Dim dpiy As Single = _graphics.DpiY
            Return iTwips * dpiy / TWIPS_PER_INCH
        End Function

        Public Shared Function TwipsToPixelsX(iTwips As Integer) As Single
            'Replacement for VB6 function
            Dim dpix As Single = _graphics.DpiX
            Return iTwips * dpix / TWIPS_PER_INCH
        End Function
        Public Shared Function PixelsToTwipsY(iPixels As Integer) As Single
            'Replacement for VB6 function
            Dim dpiy As Single = _graphics.DpiY
            Return iPixels * TWIPS_PER_INCH / dpiy
        End Function

        Public Shared Function PixelsToTwipsX(iPixels As Integer) As Single
            'Replacement for VB6 function
            Dim dpix As Single = _graphics.DpiX
            Return iPixels * TWIPS_PER_INCH / dpix
        End Function
        Public Shared Function FontChangeBold(oFont As System.Drawing.Font, bBold As Boolean) As System.Drawing.Font
            Dim oStyle As System.Drawing.FontStyle

            If bBold Then
                oStyle = oFont.Style Or System.Drawing.FontStyle.Bold
            Else
                oStyle = oFont.Style And Not System.Drawing.FontStyle.Bold
            End If

            Return New System.Drawing.Font(oFont.FontFamily, oFont.Size, oStyle)
        End Function
        Public Shared Function FontChangeUnderline(oFont As System.Drawing.Font, bUnderline As Boolean) As System.Drawing.Font
            Dim oStyle As System.Drawing.FontStyle

            If bUnderline Then
                oStyle = oFont.Style Or System.Drawing.FontStyle.Underline
            Else
                oStyle = oFont.Style And Not System.Drawing.FontStyle.Underline
            End If

            Return New System.Drawing.Font(oFont.FontFamily, oFont.Size, oStyle)
        End Function

        Public Shared Function GetAppSetting(xSection As String, xKey As String) As String
            If g_xFirmXMLConfig = "" Then
                'GLOG 15876 (DM) - get ini without loading Word add-ins or setting any initialization markers
                If g_xAppPath = "" Then _
                    g_xAppPath = mpNumSchemes.GetAppPath()
                g_xFirmIni = g_xAppPath & "\tsgNumbering.ini"
                g_xFirmXMLConfig = g_xAppPath & "\tsgNumberingConfig.xml"
            End If

            If File.Exists(g_xFirmXMLConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                oDoc = New XmlDocument()
                oDoc.Load(g_xFirmXMLConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            Return oKey.Attributes("value").Value
                        End If
                    Next
                End If

                GetAppSetting = ""
            Else
                GetAppSetting = _
                    CurWordApp.System.PrivateProfileString(g_xFirmIni, xSection, xKey)
            End If
        End Function

        Public Shared Sub SetAppSetting(xSection As String, xKey As String, xValue As String)
            If g_xFirmXMLConfig = "" Then
                'GLOG 15876 (DM) - get ini without loading Word add-ins or setting any initialization markers
                If g_xAppPath = "" Then _
                    g_xAppPath = mpNumSchemes.GetAppPath()
                g_xFirmIni = g_xAppPath & "\tsgNumbering.ini"
                g_xFirmXMLConfig = g_xAppPath & "\tsgNumberingConfig.xml"
            End If

            If File.Exists(g_xFirmXMLConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                oDoc = New XmlDocument()
                oDoc.Load(g_xFirmXMLConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            oKey.Attributes("value").Value = xValue
                            oDoc.Save(g_xFirmXMLConfig)
                            Exit Sub
                        End If
                    Next
                End If
            Else
                CurWordApp.System.PrivateProfileString(g_xFirmIni, xSection, xKey) = xValue
            End If
        End Sub

        Public Shared Function GetUserSetting(xSection As String, xKey As String) As String
            If g_xUserXMLConfig = "" Then
                'GLOG 15876 (DM) - get ini without loading Word add-ins or setting any initialization markers
                If g_xUserPath = "" Then _
                    g_xUserPath = GetUserDir()
                g_xUserIni = g_xUserPath & "\NumTOC.ini"
                g_xUserXMLConfig = g_xUserPath & "\NumTOCConfig.xml"
            End If

            If File.Exists(g_xUserXMLConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                oDoc = New XmlDocument()
                oDoc.Load(g_xUserXMLConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            Return oKey.Attributes("value").Value
                        End If
                    Next
                End If

                GetUserSetting = ""
            Else
                GetUserSetting = _
                    CurWordApp.System.PrivateProfileString(g_xUserIni, xSection, xKey)
            End If
        End Function

        Public Shared Sub SetUserSetting(xSection As String, xKey As String, xValue As String)
            If g_xUserXMLConfig = "" Then
                'GLOG 15876 (DM) - get ini without loading Word add-ins or setting any initialization markers
                If g_xUserPath = "" Then _
                    g_xUserPath = GetUserDir()
                g_xUserIni = g_xUserPath & "\NumTOC.ini"
                g_xUserXMLConfig = g_xUserPath & "\NumTOCConfig.xml"
            End If

            'create file if it doesn't exist
            If InDevelopmentModeState(8) Then
                If Not File.Exists(g_xUserXMLConfig) Then
                    Dim oSettings As Xml.XmlWriterSettings
                    oSettings = New Xml.XmlWriterSettings()
                    oSettings.Indent = True
                    Dim oWriter As XmlWriter = XmlWriter.Create(g_xUserXMLConfig, oSettings)
                    oWriter.WriteStartDocument()
                    oWriter.WriteStartElement("configuration")
                    oWriter.WriteEndElement()
                    oWriter.WriteEndDocument()
                    oWriter.Close()
                End If
            End If

            If File.Exists(g_xUserXMLConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                Dim oName As Xml.XmlAttribute
                Dim oValue As Xml.XmlAttribute
                oDoc = New XmlDocument()
                oDoc.Load(g_xUserXMLConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            oKey.Attributes("value").Value = xValue
                            oDoc.Save(g_xUserXMLConfig)
                            Exit Sub
                        End If
                    Next
                Else
                    'create section node
                    Dim oRoot As Xml.XmlNode
                    oRoot = oDoc.DocumentElement
                    oNode = oDoc.CreateNode("element", "section", "")
                    oName = oDoc.CreateAttribute("name")
                    oName.Value = xSection
                    oNode.Attributes.SetNamedItem(oName)
                    oRoot.AppendChild(oNode)
                End If

                'create key node
                oKey = oDoc.CreateNode("element", "setting", "")
                oName = oDoc.CreateAttribute("name")
                oName.Value = xKey
                oKey.Attributes.SetNamedItem(oName)
                oValue = oDoc.CreateAttribute("value")
                oValue.Value = xValue
                oKey.Attributes.SetNamedItem(oValue)
                oNode.AppendChild(oKey)
                oDoc.Save(g_xUserXMLConfig)
            Else
                CurWordApp.System.PrivateProfileString(g_xUserIni, xSection, xKey) = xValue
            End If
        End Sub

        Public Shared Sub DeleteUserSetting(xSection As String, xKey As String)
            If g_xUserXMLConfig = "" Then
                'GLOG 15876 (DM) - get ini without loading Word add-ins or setting any initialization markers
                If g_xUserPath = "" Then _
                    g_xUserPath = GetUserDir()
                g_xUserIni = g_xUserPath & "\NumTOC.ini"
                g_xUserXMLConfig = g_xUserPath & "\NumTOCConfig.xml"
            End If

            If File.Exists(g_xUserXMLConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                oDoc = New XmlDocument()
                oDoc.Load(g_xUserXMLConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            oNode.RemoveChild(oKey)
                            oDoc.Save(g_xUserXMLConfig)
                            Exit Sub
                        End If
                    Next
                End If
            Else
                WritePrivateProfileString(xSection, xKey, vbNullString, g_xUserIni)
            End If
        End Sub

        Public Shared Function GetUserDir() As String
            ' Checks for existence of path specified for UserDir in tsgNumbering.ini
            ' Creates paths if necessary (for instance in <AppData> for a new user)
            Dim xUserDir As String
            Dim oFSO() As FileInfo
            Dim oFolder As DirectoryInfo
            Dim oFile As FileInfo
            Dim xCacheFolder As String
            Dim lPos As Long
            Dim xAppPath As String

            If g_xUserPath = "" Then
                xUserDir = GetAppSetting("General", "UserDir")
                If xUserDir <> "" Then
                    'trim trailing slash
                    If Right(xUserDir, 1) = "\" Then _
                        xUserDir = Left(xUserDir, Len(xUserDir) - 1)

                    'evaluate variables
                    If (InStr(UCase(xUserDir), "<USERNAME>") > 0) Or _
                            (InStr(UCase(xUserDir), "<USER>") > 0) Then
                        'use API to get Windows user name
                        xUserDir = mpNumSchemes.GetUserVarPath(xUserDir)
                    Else
                        'use environmental variable
                        xUserDir = mpNumSchemes.GetEnvironVarPath(xUserDir)
                    End If

                    'rename "mpNumbers.sty"
                    If Not File.Exists(xUserDir & "\tsgNumbers.sty") Then
                        If File.Exists(xUserDir & "\mpNumbers.sty") Then
                            FileSystem.Rename(xUserDir & "\mpNumbers.sty", xUserDir & "\tsgNumbers.sty")
                        End If
                    End If

                    'GLOG 5479 (9.9.6004) - check for NumTOC.ini as well
                    xAppPath = mpNumSchemes.GetAppPath()
                    If (Not File.Exists(xUserDir & "\tsgNumbers.sty")) Or _
                            ((Not File.Exists(xUserDir & "\NumTOC.ini")) And _
                             (Not File.Exists(xUserDir & "\NumTOCConfig.xml"))) Then
                        'attempt to copy personal files from cache folder
                        'GLOG 5564 (9.9.6007) - first check in new public location
                        Dim bCacheFound As Boolean
                        If UCase$(Right$(xAppPath, 4)) = "\APP" Then
                            xCacheFolder = Left$(xAppPath, Len(xAppPath) - 3) & "UserCache"
                            bCacheFound = Directory.Exists(xCacheFolder)
                        End If

                        'if not in new location, check in old
                        If Not bCacheFound Then
                            lPos = InStrRev(CodeBasePath, "\")
                            xCacheFolder = Left$(CodeBasePath, lPos) & "Tools\UserCache"
                            bCacheFound = Directory.Exists(xCacheFolder)
                        End If

                        If bCacheFound Then
                            'cache folder exists - create specified directory if necessary
                            bCreatePath(xUserDir)

                            'if specified folder exists, copy personal files from cache;
                            'don't overwrite existing
                            If Directory.Exists(xUserDir) Then
                                On Error Resume Next
                                oFolder = New DirectoryInfo(xCacheFolder)
                                oFSO = oFolder.GetFiles()
                                For Each oFile In oFSO
                                    If Not File.Exists(xUserDir & "\" & oFile.Name) Then
                                        oFile.CopyTo(xUserDir & "\" & oFile.Name, False)
                                    End If
                                Next oFile
                                On Error GoTo 0
                            End If
                        End If
                    End If

                    'ensure that sty file now exists
                    If Not File.Exists(xUserDir & "\tsgNumbers.sty") Then _
                        xUserDir = ""
                End If

                If xUserDir = "" Then
                    'use Word's default path
                    xUserDir = CurWordApp.Options.DefaultFilePath(WdDefaultFilePath.wdUserTemplatesPath)
                End If

                g_xUserPath = xUserDir
            End If

            GetUserDir = g_xUserPath
        End Function

        Private Shared Function bCreatePath(xFullPath As String) As Boolean
            Dim oParent As DirectoryInfo

            If Not Directory.Exists(xFullPath) Then
                On Error Resume Next
                Directory.CreateDirectory(xFullPath)
                On Error GoTo 0
            End If

            bCreatePath = Directory.Exists(xFullPath)
        End Function

        Public Shared Function GetSchemeMapping(xSection As String, xKey As String) As String
            Dim xIni As String
            Dim xConfig As String

            If g_xAppPath = "" Then _
                g_xAppPath = mpNumSchemes.GetAppPath()
            xConfig = g_xAppPath & "\tsgSchemeMappingsConfig.xml"

            If File.Exists(xConfig) Then
                Dim oDoc As Xml.XmlDocument
                Dim oNode As Xml.XmlNode
                Dim oKey As Xml.XmlNode
                oDoc = New XmlDocument()
                oDoc.Load(xConfig)

                oNode = oDoc.SelectSingleNode("//section[@name='" + xSection + "']")
                If Not oNode Is Nothing Then
                    'cycle through nodes to work around case sensitivity
                    For Each oKey In oNode.ChildNodes
                        If oKey.Attributes("name").Value.ToUpper() = xKey.ToUpper() Then
                            Return oKey.Attributes("value").Value
                        End If
                    Next
                End If

                GetSchemeMapping = ""
            Else
                xIni = g_xAppPath & "\tsgSchemeMappings.ini"
                GetSchemeMapping = CurWordApp.System.PrivateProfileString(xIni, xSection, xKey)
            End If
        End Function

        Public Shared Function InDevelopmentModeState(ByVal i As Int32) As Boolean
            'added as part of GLOG 8779
            'uses bitwise values
            '1=General
            '8=Convert user ini to xml (value chosen for consistency with Forte)
            Dim xVal As String
            Dim iVal As Integer = 0
            Dim oReg As cRegistry
            oReg = New cRegistry()
            xVal = oReg.GetLocalMachineValue("Software\The Sackett Group\Numbering", "DevelopmentMode")
            If Int32.TryParse(xVal, iVal) Then
                Return ((iVal And i) = i)
            Else
                Return False
            End If
        End Function

        Public Shared Function InDevelopmentMode() As Boolean
            'added as part of GLOG 8779
            Return InDevelopmentModeState(1)
        End Function

        Public Shared Function COMAddInIsLoaded(ByVal xName As String) As Boolean
            'added in 9.9.6006 for GLOG 5551
            Dim oAddIn As Microsoft.Office.Core.COMAddIn

            On Error Resume Next
            oAddIn = CurWordApp.COMAddIns.Item(xName)
            On Error GoTo 0

            If Not oAddIn Is Nothing Then
                COMAddInIsLoaded = oAddIn.Connect
            Else
                COMAddInIsLoaded = False
            End If
        End Function

        Public Shared Function AddUnlinkedParagraphStyle(ByVal oDoc As Word.Document,
                                                  ByVal xName As String) As Word.Style
            'creates style of wdStyleTypeParagraphOnly type, which was introduced in Word 2007
            AddUnlinkedParagraphStyle = oDoc.Styles.Add(xName, WdStyleType.wdStyleTypeParagraphOnly)
        End Function
    End Class
End Namespace
