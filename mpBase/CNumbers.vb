Option Explicit On

Namespace LMP.Numbering.Base
    Public Class cNumbers
        Public Shared Function bGetIntegerSequence(xArray() As String, _
                             iLBound As Integer, _
                             iUBound As Integer, _
                             Optional bIncludeBlankLine As Boolean = False) As Boolean
            Dim i As Integer

            If bIncludeBlankLine Then
                ReDim xArray(0 To (iUBound - iLBound + 1))
                xArray(0) = "____"
                For i = iLBound To iUBound
                    xArray(i - iLBound + 1) = i
                Next i
            Else
                ReDim xArray(0 To (iUBound - iLBound))
                For i = iLBound To iUBound
                    xArray(i - iLBound) = i
                Next i
            End If

        End Function

        Public Shared Function bGetOrdinalSequence(xArray() As String, _
                                     iLBound As Integer, _
                                     iUBound As Integer, _
                                     Optional bIncludeBlankLine As Boolean = False) As Boolean
            Dim i As Integer

            If bIncludeBlankLine Then
                ReDim xArray(0 To (iUBound - iLBound + 1))
                xArray(0) = "____"
                For i = iLBound To iUBound
                    xArray(i - iLBound + 1) = xGetOrdinal(i)
                Next i
            Else
                ReDim xArray(0 To (iUBound - iLBound))
                For i = iLBound To iUBound
                    xArray(i - iLBound) = xGetOrdinal(i)
                Next i
            End If

        End Function

        Public Shared Function mpMax(i As Double, j As Double) As Double
            If i > j Then
                mpMax = i
            Else
                mpMax = j
            End If
        End Function

        Public Shared Function mpMin(i As Double, j As Double) As Double
            If i > j Then
                mpMin = j
            Else
                mpMin = i
            End If
        End Function

        Public Shared Function xGetOrdinal(i As Integer) As String
            If i >= 4 And i <= 20 Then
                xGetOrdinal = i & "th"
            ElseIf Right(i, 1) = 1 Then
                xGetOrdinal = i & "st"
            ElseIf Right(i, 1) = 2 Then
                xGetOrdinal = i & "nd"
            ElseIf Right(i, 1) = 3 Then
                xGetOrdinal = i & "rd"
            Else
                xGetOrdinal = i & "th"
            End If
        End Function

        Public Shared Function bDecimalSeparatorIsComma() As Boolean
            'returns TRUE if stystem is set to use comma as
            'decimal separator
            Dim xDecimal As String
            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
            bDecimalSeparatorIsComma = (xDecimal = ",")
        End Function
    End Class
End Namespace



