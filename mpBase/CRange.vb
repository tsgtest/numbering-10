Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cWP

Namespace LMP.Numbering.Base
    Public Class cRange
        Public Shared Function bIsPageBreak(rngPointer As Word.Range) As Boolean
            'returns true if rngPointer is a page break -
            'necessary because Word Library has no property/method
            'to distinguish among chr(12) types
            Dim iPageNum1 As Integer
            Dim iPageNum2 As Integer
            Dim iSecNum1 As Integer
            Dim iSecNum2 As Integer
            Dim rngTemp As Word.Range

            rngTemp = rngPointer.Duplicate

            With rngTemp
                If .Text <> Chr(12) Then
                    bIsPageBreak = False
                Else
                    .Select()
                    With CurWordApp.Selection
                        'GLOG 5082 - compare character before to character after,
                        'not to character itself, and compare section number only -
                        'the paragraph mark after a page break is still the same page
                        .Move()
                        '                iPageNum1 = .Information(wdActiveEndPageNumber)
                        iSecNum1 = .Information(WdInformation.wdActiveEndSectionNumber)
                        .Move(WdUnits.wdCharacter, -2)
                        '                iPageNum2 = .Information(wdActiveEndPageNumber)
                        iSecNum2 = .Information(WdInformation.wdActiveEndSectionNumber)
                        '                If (iSecNum1 = iSecNum2) And _
                        '                        (iPageNum1 <> iPageNum2) Then
                        If iSecNum1 = iSecNum2 Then
                            bIsPageBreak = True
                        End If
                        .Move(WdUnits.wdCharacter, 1)
                    End With
                End If
            End With
        End Function

        Public Shared Function bUnderlineToLengthOfLongest(rngLocation As Word.Range, _
                                             Optional sDefaultTabStop As Single = 36) As Boolean

            Const mpHPos = WdInformation.wdHorizontalPositionRelativeToTextBoundary
            Const mpVPos = WdInformation.wdVerticalPositionRelativeToPage

            Dim iPosition
            Dim iLongestLine
            Dim iLeftEdge
            Dim iRightEdge
            Dim iTabStops As Integer
            Dim sRightIndentPos As Single
            Dim lTabPos As Long
            Dim iNumTabs As Integer
            Dim bIsHangingIndent As Boolean
            Dim i As Integer
            Dim tsExisting As Word.TabStop
            Dim iListLevel As Integer
            Dim rngLineEnd As Word.Range
            Dim iVPosition

            CurWordApp.ScreenUpdating = False

            CurWordApp.ActiveWindow.View.ShowAll = False

            rngLocation.Select()
            With CurWordApp.Selection
                lTabPos = InStr(.Text, vbTab)
                iNumTabs = lCountChrs(.Text, vbTab)
                .Collapse(WdCollapseDirection.wdCollapseStart)
                .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                If .Text = vbCr Then
                    If lTabPos And .ParagraphFormat.FirstLineIndent < 0 Then
                        rngLocation.MoveStart(WdUnits.wdCharacter, lTabPos)
                    End If
                    rngLocation.Underline = WdUnderline.wdUnderlineSingle
                    Exit Function
                End If

                If .ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter Then
                    iRightEdge = .Information(mpHPos)
                    .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                    iLeftEdge = .Information(mpHPos)
                    .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                    .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    While CurWordApp.Selection.InRange(rngLocation)
                        iPosition = .Information(mpHPos)
                        If iPosition > iRightEdge Then _
                            iRightEdge = iPosition
                        .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                        iPosition = .Information(mpHPos)
                        If iPosition < iLeftEdge Then _
                            iLeftEdge = iPosition
                        .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    End While
                    .HomeKey(WdUnits.wdLine, WdMovementType.wdExtend)
                    rngLineEnd = CurWordApp.ActiveDocument.Range(.End, .End)

                    If .Next(WdUnits.wdCharacter).Information(mpHPos) = iRightEdge Then
                        '           lines are the same length - add no spaces
                        GoTo skipHardSpaces
                    End If

                    iVPosition = .Information(mpVPos)
                    While (.Information(mpHPos) < iRightEdge) And _
                            (.Information(mpVPos) = iVPosition)
                        .InsertAfter(Chr(160))
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        If .Information(mpHPos) < iRightEdge Then
                            .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                            .InsertAfter(Chr(160))
                            .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        End If
                    End While

                    If .Information(mpVPos) > iVPosition Then
                        '               last line wrapped - delete responsible spaces
                        .HomeKey(WdUnits.wdLine, WdMovementType.wdMove)
                        While .Characters(1).Text = Chr(160)
                            .Delete()
                        End While
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        While .Information(mpVPos) > iVPosition
                            .Move(WdUnits.wdCharacter, -1)
                            If .Characters(1).Text = Chr(160) Then _
                                .Delete()
                            .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        End While
                    End If
skipHardSpaces:
                    i = i
                Else
                    iLongestLine = .Information(mpHPos)
                    .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                    .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    While CurWordApp.Selection.InRange(rngLocation)
                        iPosition = .Information(mpHPos)
                        If iPosition > iLongestLine Then _
                            iLongestLine = iPosition
                        .MoveDown(WdUnits.wdLine, 1, WdMovementType.wdMove)
                        .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                    End While
                    iPosition = .Information(mpHPos)

                    If iPosition < iLongestLine Then
                        '               add tab stops every x points if hanging indent
                        '               and >1 tab or not hanging and > 0 tabs.
                        With .ParagraphFormat
                            bIsHangingIndent = (.FirstLineIndent < .LeftIndent)
                            With rngLocation
                                iNumTabs = lCountChrs(.Text, vbTab)
                                With .ListFormat
                                    If .ListType <> WdListType.wdListNoNumbering Then
                                        iListLevel = .ListLevelNumber
                                        If .ListTemplate.ListLevels(iListLevel).TrailingCharacter = WdTrailingCharacter.wdTrailingTab Then
                                            iNumTabs = iNumTabs + 1
                                        End If
                                    End If
                                End With
                            End With
                            If bIsHangingIndent + iNumTabs Then
                                With CurWordApp.ActiveDocument.PageSetup
                                    sRightIndentPos = .PageWidth - _
                                        (.LeftMargin + .RightMargin)
                                End With

                                '                       get position of 1st custom tab stop,
                                '                       then specify next half -inch
                                For Each tsExisting In .TabStops
                                    If tsExisting.CustomTab Then
                                        i = tsExisting.Position
                                        '                               specify next half inch
                                        i = i + (36 - (i Mod 36))
                                        Exit For
                                    End If
                                Next tsExisting

                                If i = "" Then _
                                    i = sDefaultTabStop

                                While i <= iLongestLine
                                    .TabStops.Add(i)
                                    i = i + sDefaultTabStop
                                End While
                            End If
                        End With
                        With .ParagraphFormat.TabStops
                            .Add(iLongestLine, WdTabAlignment.wdAlignTabRight)
                        End With
                        While .Information(mpHPos) < Int(iLongestLine)
                            .InsertAfter(vbTab)
                            .EndKey(WdUnits.wdLine, WdMovementType.wdMove)
                        End While
                    End If
                End If
                .HomeKey(WdUnits.wdLine, WdMovementType.wdExtend)
                .Font.Underline = WdUnderline.wdUnderlineSingle
            End With
        End Function

        Public Shared Function bUnderLineHeadingToggle() As Boolean
            ' converted from WordBasic code in MacPac 6/7.
            ' eventually should be rewritten -
            ' partially rewritten 1/11/98 - df

            Dim ShowAllStart As Boolean
            Dim IsUnderlined As Boolean
            Dim CurPosInInches As Single
            Dim bUnderlined As Boolean
            Dim bSelectEOPara As Boolean
            Dim rngLocation As Word.Range
            Dim bIsOneLineHanging As Boolean
            Dim iTabPosition As Integer
            Dim iFirstTabStop As Integer
            Dim iNumTabStops As Integer
            Dim tabExisting As TabStop
            Dim rngStart As Word.Range
            Dim i As Integer

            CurWordApp.ScreenUpdating = False

            rngStart = CurWordApp.Selection.Range
            With rngStart.Paragraphs(1).Range
                If rngStart.Start = .End - 1 And _
                    rngStart.End = .End - 1 Then
                    bSelectEOPara = True
                End If
            End With

            With CurWordApp.WordBasic
                '   get showall
                ShowAllStart = .ShowAll()

                '   test for current underlining -
                '   remove if it exists
                .WW7_EditGoTo("\Para")

                '   trim trailing para from selection if necessary
                If Right(CurWordApp.Selection.Text, 1) = vbCr Then _
                    .CharLeft(1, 1)

                '   select last line
                .CharRight()
                .StartOfLine(1)

                '   check for one-line hanging indent (e.g. reline)
                With CurWordApp.Selection
                    If .Characters.Last.Text <> vbTab And _
                            .ParagraphFormat.FirstLineIndent < 0 And _
                            iTabPosition > 0 And Left(.Text, 1) = vbTab Then
                        bIsOneLineHanging = True
                        .MoveStart(WdUnits.wdCharacter, iTabPosition)
                    End If
                End With

                '   remove underlining, hard spaces,
                '   trailing tab and last tab marker'  if they exist - then exit
                IsUnderlined = CurWordApp.Selection.Range.Underline
                If IsUnderlined Then
                    bEditFindReset()
                    .EditReplace(Find:="^s", Replace:="", ReplaceAll:=1)
                    .EndOfLine()
                    .CharLeft(1, 1)
                    If CurWordApp.Selection.Text = vbTab Then
                        .WW6_EditClear()
                        CurPosInInches = (.SelInfo(7) / 1440)
                        CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)

                        While Right(CurWordApp.Selection.Text, 1) = vbTab
                            CurWordApp.Selection.Delete()
                            CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)
                        End While

                        '--             clear last tab the VBA way
                        With CurWordApp.Selection.ParagraphFormat
                            For i = .TabStops.Count To 1 Step -1
                                With .TabStops(i)
                                    If .CustomTab And .Alignment = WdTabAlignment.wdAlignTabRight Then
                                        .Clear()
                                        Exit For
                                    End If
                                End With
                            Next i

                            '                   check for numbering - if trailing tab
                            '                   for number, skip first tab stop
                            iFirstTabStop = 1
                            With CurWordApp.Selection.Range.ListFormat
                                If .ListType <> WdListType.wdListNoNumbering Then
                                    If .ListTemplate.ListLevels(.ListLevelNumber) _
                                            .TrailingCharacter = WdTrailingCharacter.wdTrailingTab Then

                                        iFirstTabStop = 2
                                    End If
                                End If
                            End With

                            iNumTabStops = .TabStops.Count
                            On Error Resume Next
                            For i = iNumTabStops To iFirstTabStop Step -1
                                If (.TabStops(i).Position Mod 36 = 0) And _
                                        .TabStops(i).Position <> .LeftIndent Then
                                    .TabStops(i).Clear()
                                End If
                            Next i
                            On Error GoTo 0
                        End With
                    End If
                    CurWordApp.Selection.Paragraphs(1).Range.Underline = WdUnderline.wdUnderlineNone
                Else
                    If bIsOneLineHanging Then
                        rngLocation = CurWordApp.Selection.Range
                    Else
                        rngLocation = CurWordApp.Selection.Paragraphs(1).Range
                        rngLocation.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    bUnderlined = bUnderlineToLengthOfLongest(rngLocation)
                End If

                '   ensure insertion point is at end of paragraph
                .WW7_EditGoTo("\Para")

                '   trim trailing para from selection if necessary
                If Right(CurWordApp.Selection.Text, 1) = vbCr Then _
                    .CharLeft(1, 1)
                .CharRight()
                .Underline(0)

                '   set environment
                .ShowAll(ShowAllStart)
                bEditFindReset()

                '       redefine selection range if eo para
                '       should be selected - see top
                If bSelectEOPara Then
                    rngStart = rngStart.Paragraphs(1).Range
                    If rngStart.Characters.Last.Text = vbCr Then _
                        rngStart.MoveEnd(WdUnits.wdCharacter, -1)
                    rngStart.EndOf()
                End If
                rngStart.Select()
            End With
        End Function

        Public Shared Function bInsertUnderlinedLine(rngRange As Word.Range) As Boolean
            'inserts an underline to the end
            'of the line
            Dim sTabStopPos As Single

            With rngRange
                '       get position of tab stop
                With .PageSetup
                    sTabStopPos = .PageWidth - _
                    (.RightMargin + .LeftMargin + rngRange.ParagraphFormat.RightIndent) - 12
                End With

                '       add tab stop at end of line
                With .ParagraphFormat.TabStops
                    .ClearAll()
                    .Add(sTabStopPos, WdTabAlignment.wdAlignTabRight)
                End With

                '       add tab stop
                .Text = vbTab

                '       underline
                .Font.Underline = WdUnderline.wdUnderlineSingle
            End With
        End Function

        Public Shared Function rngGetField(rngFieldCode As Word.Range) As Word.Range
            'returns the range of the field
            'whose code is rngFieldCode

            Dim lStartPos As Long
            Dim lEndPos As Long

            With rngFieldCode
                lStartPos = .Start
                lEndPos = .End
            End With

            rngFieldCode.SetRange(lStartPos - 1, lEndPos + 1)
            rngGetField = rngFieldCode
        End Function
    End Class
End Namespace





