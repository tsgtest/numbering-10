Option Explicit On

Imports Microsoft.Win32
Imports Microsoft.Win32.Registry

Namespace LMP.Numbering.Base
    Public Class cRegistry
        Public Function GetLocalMachineValue(ByVal xSubKey As String, ByVal xName As String) As String
            Return GetValue(LocalMachine, xSubKey, xName)
        End Function

        Public Function GetCurrentUserValue(ByVal xSubKey As String, ByVal xName As String) As String
            Return GetValue(CurrentUser, xSubKey, xName)
        End Function

        Public Sub SetLocalMachineValue(ByVal xSubKey As String, ByVal xName As String, ByVal xValue As String,
                                        ByVal oKind As RegistryValueKind)
            SetValue(LocalMachine, xSubKey, xName, xValue, oKind)
        End Sub

        Public Sub SetCurrentUserValue(ByVal xSubKey As String, ByVal xName As String, ByVal xValue As String,
                                       ByVal oKind As RegistryValueKind)
            SetValue(CurrentUser, xSubKey, xName, xValue, oKind)
        End Sub

        Private Function GetValue(ByVal oTopLevelKey As RegistryKey, _
                                 ByVal xSubKey As String, ByVal xName As String) As String
            Dim xRetValue As String
            Dim oSubKey As RegistryKey

            xRetValue = Nothing

            On Error Resume Next
            oSubKey = oTopLevelKey.OpenSubKey(xSubKey)
            On Error GoTo 0

            If oSubKey Is Nothing Then
                Return Nothing
            Else
                On Error Resume Next
                xRetValue = oSubKey.GetValue(xName, Nothing).ToString()
                On Error GoTo 0
                If xRetValue Is Nothing Then
                    Return Nothing
                Else
                    Return xRetValue
                End If
            End If

            oSubKey.Close()
        End Function

        Private Sub SetValue(ByVal oTopLevelKey As RegistryKey, ByVal xSubKey As String, ByVal xName As String,
                             ByVal xValue As String, ByVal oKind As RegistryValueKind)
            Dim oSubKey As RegistryKey

            On Error Resume Next
            oSubKey = oTopLevelKey.CreateSubKey(xSubKey)
            On Error GoTo 0

            If Not oSubKey Is Nothing Then
                oSubKey.SetValue(xName, xValue, oKind)
                oSubKey.Close()
            End If
        End Sub

        Public Function GetComponentPath(ByVal xProgID As String) As String
            'returns the location of the dll, ocx, or exe
            'that contains the specified class
            Dim xCLSID As String
            Dim xPath As String
            Dim iPos As Integer
            Dim iLastPos As Integer

            '   get guid from prog id
            xCLSID = GetValue(ClassesRoot, xProgID & "\Clsid", "")

            '   get location from guid
            xPath = GetValue(ClassesRoot, "CLSID\" & xCLSID & "\InprocServer32", "")

            '   trim file name
            iPos = 1
            While InStr(iPos, xPath, "\") > 0
                iLastPos = InStr(iPos, xPath, "\")
                iPos = iLastPos + 1
            End While

            GetComponentPath = Left(xPath, iLastPos)
        End Function
    End Class
End Namespace

