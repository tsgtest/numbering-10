Option Explicit On

Imports LMP.Numbering.Base.cNumTOC

Namespace LMP.Numbering.Base
    Public Class cStrings
        Public Shared Function xLineIfEmpty(ByVal xText As String, iLineLength As Integer) As String
            ' returns a blank line of length iLineLength
            ' if xText is empty, else returns xText

            If xText = "" Then
                xLineIfEmpty = New String("_", iLineLength)
            Else
                xLineIfEmpty = xText
            End If
        End Function

        Public Shared Function xInitialCap(xString As String) As String
            '***Takes any string and returns it Initial Capped

            Dim xTemp As String
            Dim xFinal As String
            Dim iCount As Integer
            Dim i As Integer
            Dim bSpace As Boolean

            xTemp = LCase(xString)
            iCount = Len(xTemp)

            xFinal = UCase(Left(xTemp, 1))

            For i = 1 To iCount - 1

                xTemp = Right(xTemp, iCount - i)
                If Left(xTemp, 1) = Chr(32) Then bSpace = True

                If Left(xTemp, 1) <> Chr(32) And bSpace Then
                    xFinal = xFinal + UCase(Left(xTemp, 1))
                    bSpace = False
                Else
                    xFinal = xFinal + Left(xTemp, 1)
                End If

            Next i

            xInitialCap = xFinal

        End Function

        Public Shared Function xGetNumeric(xString As String) As String

            'Trims all non numeric chars from start of string
            'made for Attorney Bar ID number field that contains
            'abbreviated state info

            Dim iLength As Long
            Dim i As Long
            Dim iPos As Long
            Dim xChr As String

            iLength = Len(xString)
            iPos = 0

            For i = 1 To iLength
                xChr = Mid(xString, i, 1)
                If IsNumeric(xChr) Then
                    iPos = i
                    Exit For
                End If
            Next i

            If iPos <> 0 Then
                xGetNumeric = Right(xString, iLength - (iPos - 1))
            Else
                xGetNumeric = ""
            End If
        End Function


        Public Shared Function xSplit(xText As String, _
                        xSplitAfter As String, _
                        Optional bIncludeComma As Boolean = True) As String
            'inserts new line chr in xText after xSplitAfter-
            'if bIncludeComma then leave trailing char at end
            'of first line

            Dim iSplitPos As Integer
            Dim iSplitLength As Integer
            Dim xFirstLine As String
            Dim xSecondLine As String

            iSplitPos = InStr(xText, xSplitAfter)

            If iSplitPos Then
                iSplitLength = Len(xSplitAfter)
                xFirstLine = Trim(Left(xText, iSplitPos - 1))
                If Not bIncludeComma Then
                    If Right(xFirstLine, 1) = "," Then
                        xFirstLine = Left(xFirstLine, Len(xFirstLine) - 1)
                    End If
                End If
                xSecondLine = Trim(Mid(xText, iSplitPos + iSplitLength))
                xSplit = xFirstLine & Chr(11) & xSecondLine
            Else
                xSplit = xText
            End If
        End Function


        Public Shared Function lCountChrs(xSource As String, xSearch As String) As Long
            'returns the number of instances
            'of xSearch in xSource

            Dim iPos As Integer
            Dim l As Long

            iPos = InStr(xSource, xSearch)
            While iPos
                l = l + 1
                iPos = InStr(iPos + 1, xSource, xSearch)
            End While
            lCountChrs = l
        End Function
        Public Shared Function xNullToString(vValue As Object) As String
            If IsNothing(vValue) Then _
                xNullToString = CStr(vValue)
        End Function
        Public Shared Function xStringToArray(ByVal xString As String, _
                                xArray(,) As String, _
                                Optional iNumCols As Integer = 1, _
                                Optional xSep As String = ",") As String

            Dim iNumSeps As Integer
            Dim INumEntries As Integer
            Dim iSepPos As Integer
            Dim xEntry As String
            Dim i As Integer
            Dim j As Integer

            '   get ubound of xArray - count delimiter
            '   then divide by iNumCols
            iNumSeps = lCountChrs(xString, xSep)
            INumEntries = (iNumSeps + 1) / iNumCols

            ReDim xArray(INumEntries - 1, iNumCols - 1)

            For i = 0 To INumEntries - 1
                For j = 0 To iNumCols - 1
                    '           get next entry & store
                    iSepPos = InStr(xString, xSep)
                    If iSepPos Then
                        xEntry = Left(xString, iSepPos - 1)
                    Else
                        xEntry = xString
                    End If
                    xArray(i, j) = xEntry

                    '           remove entry from xstring
                    If iSepPos Then _
                        xString = Mid(xString, iSepPos + 1)
                Next j
            Next i
        End Function

        Public Shared Function xTrimTrailingChrs(ByVal xText As String, _
                                   Optional ByVal xChr As String = "", _
                                   Optional bTrimStart As Boolean = False, _
                                   Optional bTrimEnd As Boolean = True) As String

            '   removes trailing xChr from xText -
            '   if xchr = "" then trim last char

            If xChr <> "" Then
                If bTrimStart Then
                    While Left(xText, Len(xChr)) = xChr
                        xText = Mid(xText, Len(xChr) + 1)
                    End While
                End If
                If bTrimEnd Then
                    While Right(xText, Len(xChr)) = xChr
                        xText = Left(xText, Len(xText) - Len(xChr))
                    End While
                End If
            Else
                xText = Left(xText, Len(xText) - 1)
            End If

            xTrimTrailingChrs = xText
        End Function

        Public Shared Function bParseAddress(xEntry As String, _
                               xName As String, _
                               xAddress As String) As Boolean

            'fills xName and xAddress
            'after parsing xEntry

            Dim iParaPos As String

            On Error Resume Next

            iParaPos = InStr(xEntry, vbCr)
            xName = Left(xEntry, iParaPos - 1)
            xAddress = Right(xEntry, Len(xEntry) - iParaPos)

            While Right(xAddress, 1) = vbCr
                xAddress = Left(xAddress, Len(xAddress) - 1)
            End While

            bParseAddress = True
        End Function

        Public Shared Function bStringToArray(ByVal xString As String, _
                            ByRef xArray() As String, _
                            Optional ByVal xSep As String = ",") As Boolean
            'fills xArray with items
            'separated by xSep in xString
            Dim iNewUBound As Integer
            Dim xNewItem As String
            Dim iCurSepPos As Integer

            '   get current delimiter position
            iCurSepPos = InStr(xString, xSep)

            While iCurSepPos

                '       dim to one greater if last item
                '       in array is not empty, else use
                '       last item
                If xArray(UBound(xArray)) <> "" Then
                    iNewUBound = UBound(xArray) + 1
                    ReDim Preserve xArray(iNewUBound)
                Else
                    iNewUBound = UBound(xArray)
                End If

                '       isolate item from string
                xNewItem = Left(xString, iCurSepPos - 1)

                '       fill array with new item
                xArray(iNewUBound) = xNewItem

                '       trim item from original string
                xString = Mid(xString, iCurSepPos + Len(xSep))

                '       search remainder of string for delimiter
                iCurSepPos = InStr(xString, xSep)
            End While

            '   dim to one greater if last item
            '   in array is not empty, else use
            '   last item
            If xArray(UBound(xArray)) <> "" Then
                iNewUBound = UBound(xArray) + 1
                ReDim Preserve xArray(iNewUBound)
            Else
                iNewUBound = UBound(xArray)
            End If

            '   fill array with new item
            xArray(iNewUBound) = xString

            bStringToArray = True

        End Function

        Public Shared Function iStringToArray(ByVal xString As String, _
                            xArray() As String, _
                            Optional ByVal xSep As String = ",") As Integer
            'fills xArray with items
            'separated by xSep in xString
            Dim iNewUBound As Integer
            Dim xNewItem As String
            Dim iCurSepPos As Integer


            '   get current delimiter position
            iCurSepPos = InStr(xString, xSep)

            While iCurSepPos

                '       dim to one greater if last item
                '       in array is not empty, else use
                '       last item
                If xArray(UBound(xArray)) <> "" Then
                    iNewUBound = UBound(xArray) + 1
                    ReDim Preserve xArray(iNewUBound)
                Else
                    iNewUBound = UBound(xArray)
                End If

                '       isolate item from string
                xNewItem = Left(xString, iCurSepPos - 1)

                '       fill array with new item
                xArray(iNewUBound) = xNewItem

                '       trim item from original string
                xString = Mid(xString, iCurSepPos + _
                              Len(xSep))

                '       search remainder of string for delimiter
                iCurSepPos = InStr(xString, xSep)
            End While

            '   dim to one greater if last item
            '   in array is not empty, else use
            '   last item
            If xArray(UBound(xArray)) <> "" Then
                iNewUBound = UBound(xArray) + 1
                ReDim Preserve xArray(iNewUBound)
            Else
                iNewUBound = UBound(xArray)
            End If

            '   fill array with new item
            If xString <> "" Then
                xArray(iNewUBound) = xString
                iNewUBound = UBound(xArray) + 1
            End If

            iStringToArray = iNewUBound

        End Function



        Public Shared Function xArrayToString(xArray() As String, _
                                Optional ByVal xSep As String = ",") As String
            Dim i As Integer
            Dim xString As String

            On Error Resume Next

            '   concatenate array items to string
            For i = LBound(xArray) To UBound(xArray)
                xString = xString & xArray(i) & xSep
            Next i

            '   trim final separator
            xString = Left(xString, Len(xString) - _
                                    Len(xSep))

            xArrayToString = xString
        End Function


        Public Shared Function xSubstitute(ByVal xString As String, _
                             xSearch As String, _
                             xReplace As String) As String
            'replaces xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer
            Dim xNewString As String

            '   get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            '   remove switch all chars
            While iSeachPos
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
                iSeachPos = InStr(UCase(xString), _
                                  UCase(xSearch))

            End While

            xNewString = xNewString & xString
            xSubstitute = xNewString
        End Function

        Public Shared Function xReturnsToLFChange(ByVal xParse As String, _
                                    Optional bTrimFinalLF As Boolean = False) As String
            Dim xTemp As String

            xParse = CurWordApp.CleanString(xParse)

            xTemp = xSubstitute(xParse, _
                                Chr(13), _
                                Chr(11))
            If bTrimFinalLF Then
                If Right(xTemp, 1) = Chr(11) Then
                    xTemp = Left(xTemp, Len(xTemp) - 1)
                End If
            End If
            xReturnsToLFChange = xTemp
        End Function
        Public Shared Function xGetMonth(iMonth As Integer, xFormat As String) As String

            Dim i As Integer
            Dim MonthArray(11) As String

            Select Case xFormat
                Case "mmmm"
                    MonthArray(0) = "January"
                    MonthArray(1) = "February"
                    MonthArray(2) = "March"
                    MonthArray(3) = "April"
                    MonthArray(4) = "May"
                    MonthArray(5) = "June"
                    MonthArray(6) = "July"
                    MonthArray(7) = "August"
                    MonthArray(8) = "September"
                    MonthArray(9) = "October"
                    MonthArray(10) = "November"
                    MonthArray(11) = "December"
                Case "mmm"
                    MonthArray(0) = "Jan"
                    MonthArray(1) = "Feb"
                    MonthArray(2) = "Mar"
                    MonthArray(3) = "Apr"
                    MonthArray(4) = "May"
                    MonthArray(5) = "Jun"
                    MonthArray(6) = "Jul"
                    MonthArray(7) = "Aug"
                    MonthArray(8) = "Sept"
                    MonthArray(9) = "Oct"
                    MonthArray(10) = "Nov"
                    MonthArray(11) = "Dec"
                Case Else
                    For i = 0 To 11
                        MonthArray(i) = LTrim(Str(i + 1))
                    Next i
            End Select
            xGetMonth = MonthArray(iMonth - 1)
        End Function

        Public Shared Function xGetMonths(xMonths() As String, xFormat As String, _
                           Optional bIncludeBlankLine As Boolean = False) As String
            'fills xMonths with months whose
            'format is specified by xFormat -
            'returns current month in that format

            Dim i As Integer

            If bIncludeBlankLine Then
                ReDim xMonths(12)
                xMonths(0) = New String("_", 15)
                Select Case xFormat
                    Case "mmmm"
                        xMonths(1) = "January"
                        xMonths(2) = "February"
                        xMonths(3) = "March"
                        xMonths(4) = "April"
                        xMonths(5) = "May"
                        xMonths(6) = "June"
                        xMonths(7) = "July"
                        xMonths(8) = "August"
                        xMonths(9) = "September"
                        xMonths(10) = "October"
                        xMonths(11) = "November"
                        xMonths(12) = "December"
                    Case "mmm"
                        xMonths(1) = "Jan"
                        xMonths(2) = "Feb"
                        xMonths(3) = "Mar"
                        xMonths(4) = "Apr"
                        xMonths(5) = "May"
                        xMonths(6) = "Jun"
                        xMonths(7) = "Jul"
                        xMonths(8) = "Aug"
                        xMonths(9) = "Sept"
                        xMonths(10) = "Oct"
                        xMonths(11) = "Nov"
                        xMonths(12) = "Dec"
                    Case Else
                        For i = 1 To 12
                            xMonths(i) = LTrim(Str(i + 1))
                        Next i
                End Select
            Else
                ReDim xMonths(11)
                Select Case xFormat
                    Case "mmmm"
                        xMonths(0) = "January"
                        xMonths(1) = "February"
                        xMonths(2) = "March"
                        xMonths(3) = "April"
                        xMonths(4) = "May"
                        xMonths(5) = "June"
                        xMonths(6) = "July"
                        xMonths(7) = "August"
                        xMonths(8) = "September"
                        xMonths(9) = "October"
                        xMonths(10) = "November"
                        xMonths(11) = "December"
                    Case "mmm"
                        xMonths(0) = "Jan"
                        xMonths(1) = "Feb"
                        xMonths(2) = "Mar"
                        xMonths(3) = "Apr"
                        xMonths(4) = "May"
                        xMonths(5) = "Jun"
                        xMonths(6) = "Jul"
                        xMonths(7) = "Aug"
                        xMonths(8) = "Sept"
                        xMonths(9) = "Oct"
                        xMonths(10) = "Nov"
                        xMonths(11) = "Dec"
                    Case Else
                        For i = 0 To 11
                            xMonths(i) = LTrim(Str(i + 1))
                        Next i
                End Select
            End If
            xGetMonths = xMonths(Month(Now))
        End Function

        Public Shared Function xTrimSpaces(ByVal xString As String) As String

            xTrimSpaces = xSubstitute(xString, _
                                      Chr(32), _
                                      "")
        End Function

        Public Shared Function xFlipName(xName, xSep) As String
            'returns xName such that characters
            'after last xsep appear before
            'characters before last xsep

            Dim iSepPos As Integer
            Dim iLastSepPos As Integer
            Dim xPart1 As String
            Dim xPart2 As String
            Dim xNameTemp As String

            xNameTemp = xName

            iSepPos = InStr(xName, xSep)
            While iSepPos <> 0
                iLastSepPos = iSepPos
                xNameTemp = Mid(xNameTemp, iSepPos + Len(xSep))
                iSepPos = InStr(xNameTemp, xSep)
            End While

            If iLastSepPos > 0 Then
                iLastSepPos = Len(xName) - (Len(xNameTemp) + Len(xSep) - 1)
                xPart1 = Trim(Left(xName, iLastSepPos - 1))
                xPart2 = Trim(Mid(xName, iLastSepPos + Len(xSep)))
                xFlipName = xPart2 & " " & xPart1
            Else
                xFlipName = xName
            End If

        End Function

        Public Shared Function bIniToBoolean(xTFString As String) As Boolean
            If UCase(xTFString) = "TRUE" Then
                bIniToBoolean = True
            Else
                bIniToBoolean = False
            End If

        End Function
        Public Shared Function UCaseName(xName As String) As String
            Dim i As Integer
            Dim xInputString As String
            xInputString = UCase(xName)
            i = InStr(xInputString, " MC")
            If i > 0 Then
                xInputString = Mid(xInputString, 1, i + 1) & _
                    "c" & Mid(xInputString, i + 3)
            End If
            UCaseName = xInputString
        End Function

        Public Shared Function GetUserVarPath(ByVal xPath As String) As String
            'substitutes user path for <xToken>
            GetUserVarPath = mpNumSchemes.GetUserVarPath(xPath)
        End Function

        Public Shared Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            GetEnvironVarPath = mpNumSchemes.GetEnvironVarPath(xPath)
        End Function

        Public Shared Function xLocalizeNumericString(xString As String) As String
            'replaces decimal separator with the current one set for the OS
            Dim xTemp As String
            Dim xDecimal As String

            xTemp = xString
            If xTemp = "" Then Exit Function
            xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
            If xDecimal = "." Then
                xLocalizeNumericString = xSubstitute(xTemp, ",", ".")
            Else
                xLocalizeNumericString = xSubstitute(xTemp, ".", ",")
            End If
        End Function

        Public Shared Function GetAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            GetAppPath = mpNumSchemes.GetAppPath()
        End Function

        Public Shared Function GetMacPacAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            GetMacPacAppPath = mpNumSchemes.GetMacPacAppPath()
        End Function

        Public Shared Function GetDataTable(ByVal oValues As String(,)) As DataTable
            Dim dtValues As New DataTable()
            Dim oNameCol As DataColumn
            Dim oValueCol As DataColumn
            Dim i As Int32

            oNameCol = New DataColumn("Name", System.Type.GetType("System.String"))
            dtValues.Columns.Add(oNameCol)
            oValueCol = New DataColumn("Value", System.Type.GetType("System.String"))
            dtValues.Columns.Add(oValueCol)

            'populate the table with the values
            For i = 0 To UBound(oValues)
                dtValues.Rows.Add(oValues(i, 0), oValues(i, 1))
            Next

            Return dtValues
        End Function

        Public Shared Function RemoveIllegalIDChars(ByVal xID As String) As String
            Dim xName As String
            xName = xID.Replace(" ", "")
            xName = xName.Replace("'", "")
            xName = xName.Replace("!", "")
            xName = xName.Replace("<", "")
            xName = xName.Replace(">", "")
            xName = xName.Replace("&", "")
            xName = xName.Replace("]", "")
            xName = xName.Replace("[", "")
            xName = xName.Replace("(", "")
            xName = xName.Replace(")", "")
            xName = xName.Replace("@", "")
            xName = xName.Replace("#", "")
            xName = xName.Replace("$", "")
            xName = xName.Replace("*", "")
            xName = xName.Replace("$", "")
            xName = xName.Replace("+", "")
            xName = xName.Replace("=", "")
            xName = xName.Replace("?", "")
            xName = xName.Replace("{", "")
            xName = xName.Replace("}", "")
            xName = xName.Replace(";", "")
            xName = xName.Replace(",", "")
            xName = xName.Replace("%", "")
            xName = xName.Replace("^", "")
            xName = xName.Replace("""", "")
            Return xName
        End Function
    End Class
End Namespace
