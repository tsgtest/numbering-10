Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Numbering.Base
    Public Class cTripleStateChkbox
        Shared Function ConvToTripleState(lValue As Long) As Integer
            'converts a value into a valid
            'value for a VB.chkbox
            Select Case lValue
                Case -1, 1, 2
                    '           value = 2 is considered checked
                    '           because it represents wdUnderlineWords
                    ConvToTripleState = 1
                Case 0
                    ConvToTripleState = 0
                Case Else
                    ConvToTripleState = 2
            End Select
        End Function

        Shared Function ConvFromTripleState(lValue As Long) As Long
            'converts a value into a valid
            'value for a VB.chkbox
            Select Case lValue
                Case 0
                    ConvFromTripleState = 0
                Case 1
                    ConvFromTripleState = -1
                Case Else
                    ConvFromTripleState = WdConstants.wdUndefined
            End Select
        End Function

        Shared Sub CycleTripleState(chkP As Object, ByVal iValue As Integer)
            If iValue = 1 Then
                chkP.Value = 2
            ElseIf iValue = 2 Then
                chkP.Value = 0
            Else
                chkP.Value = 1
            End If
            System.Windows.Forms.Application.DoEvents()
        End Sub
    End Class
End Namespace


