Option Explicit On

Imports LMP.Numbering.Base.cNumTOC

Namespace LMP.Numbering.Base
    Public Class cWP
        Public Shared Function bEditFindReset() As Boolean
            'required becuase EditFind in WordBasic modifies
            'the Edit/Find dialog - trailer and underline to
            'longest both use WordBasic EditFind
            On Error Resume Next
            With CurWordApp.WordBasic
                .EditReplace(Find:="", _
                    Replace:="", _
                    Direction:=0, _
                    WholeWord:=0, _
                    MatchCase:=0, _
                    PatternMatch:=0, _
                    SoundsLike:=0, _
                    Wrap:=0, _
                    Format:=0)
                .EditFindClearFormatting()
                .EditReplaceClearFormatting()
            End With
        End Function
    End Class
End Namespace


