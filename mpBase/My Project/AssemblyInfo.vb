﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("nBase")> 
<Assembly: AssemblyDescription("TSG Numbering Base Component")> 
<Assembly: AssemblyCompany("The Sackett Group, Inc.")> 
<Assembly: AssemblyProduct("TSG Numbering")> 
<Assembly: AssemblyCopyright("©1990 - 2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("8410114f-9437-4054-be4e-5235fcbae67e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("10.0.0.28")> 
<Assembly: AssemblyFileVersion("10.0.0.28")> 
