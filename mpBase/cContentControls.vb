Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.cNumTOC

Namespace LMP.Numbering.Base
    Public Class cContentControls
        Public Shared Function BeforeBlockLevelCC() As Boolean
            'returns true if immediately before a block-level content control
            Dim oRange As Word.Range
            Dim oCC As Word.ContentControl
            Dim lParaEnd As Long

            BeforeBlockLevelCC = False

            On Error Resume Next
            oRange = CurWordApp.Selection.Next(Word.WdUnits.wdCharacter)
            On Error GoTo 0

            If oRange Is Nothing Then Exit Function

            oRange = CurWordApp.Selection.Range
            oRange.Move()
            oCC = oRange.ParentContentControl
            If Not oCC Is Nothing Then
                If oRange.Start = oCC.Range.Start Then
                    lParaEnd = CurWordApp.Selection.Paragraphs(1).Range.End - 1
                    If oCC.Range.End < lParaEnd Then
                        oRange.SetRange(oCC.Range.End, lParaEnd)
                        BeforeBlockLevelCC = (oRange.Text = "")
                    Else
                        BeforeBlockLevelCC = True
                    End If
                End If
            End If
        End Function

        Public Shared Function AfterBlockLevelCC() As Boolean
            'returns true if immediately after a block-level content control
            Dim oRange As Word.Range
            Dim oCC As Word.ContentControl
            Dim lParaStart As Long

            AfterBlockLevelCC = False

            oRange = CurWordApp.Selection.Range
            If oRange.Start = 0 Then Exit Function
            oRange.Move(WdUnits.wdCharacter, -1)
            oCC = oRange.ParentContentControl
            If Not oCC Is Nothing Then
                If oRange.End = oCC.Range.End Then
                    lParaStart = CurWordApp.Selection.Paragraphs(1).Range.Start
                    If oCC.Range.Start > lParaStart Then
                        oRange.SetRange(lParaStart, oCC.Range.Start)
                        AfterBlockLevelCC = (oRange.Text = "")
                    Else
                        AfterBlockLevelCC = True
                    End If
                End If
            End If
        End Function

        Public Shared Function GetCCSafeParagraphStart(ByVal oInsertionRange As Word.Range) As Long
            'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
            'content controls that would otherwise force a new paragraph to be created above the controls()
            Dim i As Integer
            Dim rngPara As Word.Range
            Dim iCount As Integer
            Dim oCC As Word.ContentControl

            rngPara = oInsertionRange.Paragraphs(1).Range

            'count multiparagraph tags at start of paragraph
            For i = 1 To rngPara.ContentControls.Count
                oCC = rngPara.ContentControls(i)
                If (oCC.Range.Start = rngPara.Start + i) And
                        (oCC.Range.Paragraphs.Count > 1) Then
                    iCount = iCount + 1
                Else
                    Exit For
                End If
            Next i

            GetCCSafeParagraphStart = rngPara.Start + iCount
        End Function

        Public Shared Function GetCCSafeParagraphEnd(ByVal oInsertionRange As Word.Range) As Long
            'gets "end of paragraph" insertion location, ensuring that inside any multiparagraph
            'content controls that would otherwise force a new paragraph to be created below the control(s)
            Dim i As Integer
            Dim rngPara As Word.Range
            Dim iCount As Integer
            Dim oCC As Word.ContentControl

            rngPara = oInsertionRange.Paragraphs(1).Range
            With rngPara
                'move to end of paragraph
                .MoveEnd(WdUnits.wdCharacter, -1)

                'count multiparagraph tags at start of paragraph
                'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
                'should be staying inside of and preserving all block tags
                For i = 1 To .ContentControls.Count
                    oCC = .ContentControls(i)
                    If (oCC.Range.End = .End - i) And
                            (oCC.Range.Paragraphs.Count > 1) Then
                        iCount = iCount + 1
                    Else
                        Exit For
                    End If
                Next i
            End With

            GetCCSafeParagraphEnd = rngPara.End - iCount
        End Function

        Public Shared Function CountCCsEndingInRange(ByVal oRange As Word.Range) As Integer
            'returns the number of content controls in the range that end before the end of the range
            Dim i As Integer
            Dim iCount As Integer

            For i = 1 To oRange.ContentControls.Count
                If oRange.ContentControls(i).Range.End < oRange.End Then _
                    iCount = iCount + 1
            Next i

            CountCCsEndingInRange = iCount
        End Function

        Public Shared Function GetPositionAfterInlineCC(ByVal oRange As Word.Range) As Long
            'if specified range is inside an inline content control,
            'returns the position immediately after it
            Dim oCC As Word.ContentControl
            Dim lPos As Integer

            oCC = oRange.ParentContentControl
            If Not oCC Is Nothing Then
                If ContentControlIsInline(oCC) Then
                    lPos = oCC.Range.End + 1
                End If
            End If

            GetPositionAfterInlineCC = lPos
        End Function

        Private Shared Function ContentControlIsInline(ByVal oCC As Word.ContentControl) As Boolean
            'returns TRUE if functionally inline, i.e. contains no text, or other content controls
            'that start and end before or after it in the paragraph
            Dim oParaRange As Word.Range
            Dim oCC2 As Word.ContentControl
            Dim oTagRange As Word.Range
            Dim oPrecedingRange As Word.Range
            Dim oTrailingRange As Word.Range

            oTagRange = oCC.Range

            'check whether oCC contains multiple paragraphs
            If oTagRange.Paragraphs.Count > 1 Then
                ContentControlIsInline = False
                Exit Function
            ElseIf oTagRange.Characters.Last.Text = vbCr Then
                'GLOG 4891 (dm) - the above test will miss a 2-paragraph
                'cc in which the second paragraph is empty
                ContentControlIsInline = False
                Exit Function
            End If

            oParaRange = oTagRange.Paragraphs(1).Range

            'check whether there's preceding text in the paragraph
            oPrecedingRange = oTagRange.Duplicate
            oPrecedingRange.SetRange(oParaRange.Start, oTagRange.Start - 1)
            If oPrecedingRange.Text <> "" Then
                ContentControlIsInline = True
                Exit Function
            End If

            'check whether there's trailing text in the paragraph
            oTrailingRange = oTagRange.Duplicate
            oTrailingRange.SetRange(oTagRange.End + 1, oParaRange.End - 1)
            If oTrailingRange.Text <> "" Then
                ContentControlIsInline = True
                Exit Function
            End If

            'check whether there are preceding tags in the paragraph
            For Each oCC2 In oPrecedingRange.ContentControls
                If oCC2.Range.End < oPrecedingRange.End Then
                    ContentControlIsInline = True
                    Exit Function
                End If
            Next oCC2

            'check whether there are trailing tags in the paragraph
            For Each oCC2 In oTrailingRange.ContentControls
                If oCC2.Range.Start > oTrailingRange.Start Then
                    ContentControlIsInline = True
                    Exit Function
                End If
            Next oCC2

            ContentControlIsInline = False
        End Function

        Public Shared Function AddTempParaToContentControl(ByVal oRange As Word.Range) As Boolean
            'if oRange ends in a paragraph that ends with a content control,
            'adds a temp para and returns TRUE
            Dim oCC As Word.ContentControl
            Dim lEnd As Long
            Dim oParaRange As Word.Range

            oParaRange = oRange.Paragraphs.Last.Range
            lEnd = oParaRange.End - 2
            oParaRange.SetRange(lEnd, lEnd)

            oCC = oParaRange.ParentContentControl
            While Not oCC Is Nothing
                If oCC.Range.End = lEnd Then
                    oCC.Range.EndOf()
                    oCC.Range.InsertAfter(vbCr)
                    AddTempParaToContentControl = True
                    Exit Function
                Else
                    oCC = oCC.ParentContentControl
                End If
            End While

            AddTempParaToContentControl = False
        End Function
    End Class
End Namespace
