Option Explicit On
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.VisualBasic
Imports LMP.Numbering.Base.cNumTOC

Namespace LMP.Numbering.Base
    Public Class cWordXML
        Public Shared Function SetXMLMarkupState(ByVal oDocument As Word.Document,
                                          ByVal bShowXML As Boolean) As Long
            ' There is a bug in Word 2007 that causes the ShowAll state to be changed when
            ' opening a new document if ShowXMLMarkup is changed to not match ShowAll
            ' Need to toggle ShowAll to reset the correct state
            With oDocument.ActiveWindow.View
                If .ShowXMLMarkup <> bShowXML Then
                    .ShowXMLMarkup = bShowXML
                    SetXMLMarkupState = -1
                Else
                    SetXMLMarkupState = 0
                End If
            End With
        End Function

        Public Shared Function GetTagSafeParagraphStart(ByVal oInsertionRange As Word.Range,
                                         Optional ByRef iParagraphLevelTagsAtStart As Integer = 0) As Long
            'gets "start of paragraph" insertion location, ensuring that inside any multiparagraph
            'tags that would otherwise force a new paragraph to be created above the tag(s)
            Dim i As Integer
            Dim rngPara As Word.Range
            Dim iCount As Integer
            Dim oTag As Word.XMLNode

            rngPara = oInsertionRange.Paragraphs(1).Range

            'count multiparagraph tags at start of paragraph -
            'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
            'should be staying inside of and preserving all block tags
            For i = 1 To rngPara.XMLNodes.Count
                oTag = rngPara.XMLNodes(i)
                If (oTag.Range.Start = rngPara.Start + i) And
                        (oTag.Level <> WdXMLNodeLevel.wdXMLNodeLevelInline) Then
                    iCount = iCount + 1
                Else
                    Exit For
                End If
            Next i

            '9.9.4009
            iParagraphLevelTagsAtStart = iCount

            GetTagSafeParagraphStart = rngPara.Start + iCount
        End Function

        Public Shared Function GetTagSafeParagraphEnd(ByVal oInsertionRange As Word.Range,
                                       Optional ByRef iParagraphLevelTagsAtEnd As Integer = 0) As Long
            'gets "end of paragraph" insertion location, ensuring that inside any multiparagraph
            'tags that would otherwise force a new paragraph to be created below the tag(s)
            Dim i As Integer
            Dim rngPara As Word.Range
            Dim iCount As Integer
            Dim oTag As Word.XMLNode

            rngPara = oInsertionRange.Paragraphs(1).Range
            With rngPara
                '        'move to end of paragraph
                .MoveEnd(WdUnits.wdCharacter, -1)

                'count multiparagraph tags at start of paragraph
                'mp10 GLOG 3914 - instead of # of paragraphs, check tag level - we
                'should be staying inside of and preserving all block tags
                For i = 1 To .XMLNodes.Count
                    oTag = .XMLNodes(i)
                    If (oTag.Range.End = .End - i) And
                            (oTag.Level <> WdXMLNodeLevel.wdXMLNodeLevelInline) Then
                        iCount = iCount + 1
                    Else
                        Exit For
                    End If
                Next i
            End With

            '9.9.4009
            iParagraphLevelTagsAtEnd = iCount

            GetTagSafeParagraphEnd = rngPara.End - iCount
        End Function

        Private Shared Function TagIsBlockable(ByVal oTag As Word.XMLNode) As Boolean
            'returns TRUE if oTag can contain multiple paragraphs -
            'converts inline tag if possible
            Dim oRange As Word.Range
            Dim lEnd As Long
            Dim bIsBlockable As Boolean

            oRange = oTag.Range
            With oRange
                If .Paragraphs.Count > 1 Then
                    'tag already contains multiple paragraphs
                    bIsBlockable = True
                Else
                    'test blockability by seeing whether tag expands when a paragraph mark
                    'is inserted at the end of its range - note that this will automatically
                    'convert inline tags if there's nothing else in the paragraph
                    lEnd = .End

                    Try
                        .InsertAfter(vbCr)
                    Catch e As System.Exception
                        If Err.Number = 5904 Then
                            'cannot edit range - tag spans table,
                            'which means it's already a block tag
                            TagIsBlockable = True
                            Err().Number = 0
                            Exit Function
                        ElseIf Err().Number > 0 Then
                            TagIsBlockable = False
                            Throw e
                        End If
                    End Try

                    bIsBlockable = (oTag.Range.End > lEnd)

                    If .Characters.Last.Text = vbCr & Chr(7) Then _
                        .MoveEnd(WdUnits.wdCharacter, -1)
                    .Characters.Last.Delete()
                End If
            End With

            TagIsBlockable = bIsBlockable
        End Function

        Public Shared Function CursorIsAtStartOfBlockTag() As Boolean
            Dim oTag As Word.XMLNode
            Dim bIsAtStart As Boolean

            oTag = CurWordApp.Selection.XMLParentNode
            If Not oTag Is Nothing Then
                If TagIsBlockable(oTag) Then
                    bIsAtStart = (CurWordApp.Selection.Start = oTag.Range.Start)
                End If
            End If

            CursorIsAtStartOfBlockTag = bIsAtStart
        End Function

        Public Shared Function CountTagsEndingInRange(ByVal oRange As Word.Range) As Integer
            'returns the number of XML tags in the specified range that end before the end of the range
            Dim i As Integer
            Dim iCount As Integer

            For i = 1 To oRange.XMLNodes.Count
                If oRange.XMLNodes(i).Range.End < oRange.End Then _
                    iCount = iCount + 1
            Next i

            CountTagsEndingInRange = iCount
        End Function

        Public Shared Function GetPositionAfterInlineTag(ByVal oRange As Word.Range) As Long
            'if specified range is inside an inline tag, returns the position immediately after it
            Dim oTag As Word.XMLNode
            Dim lPos As Integer

            oTag = oRange.XMLParentNode
            If Not oTag Is Nothing Then
                If oTag.Level = WdXMLNodeLevel.wdXMLNodeLevelInline Then
                    lPos = oTag.Range.End + 1
                End If
            End If

            GetPositionAfterInlineTag = lPos
        End Function
    End Class
End Namespace

