Option Explicit On

Imports System.Runtime.InteropServices

Namespace LMP.Numbering.Base
    Friend Class mpDeclares
        Friend Const CSIDL_LOCAL_APPDATA = &H1C&
        Friend Const CSIDL_FLAG_CREATE = &H8000&
        Friend Const CSIDL_COMMON_DOCUMENTS = &H2E
        Friend Const SHGFP_TYPE_CURRENT = 0
        Friend Const SHGFP_TYPE_DEFAULT = 1
        Friend Const MAX_PATH = 260

        <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Unicode)> _
        Public Shared Function GetKeyState(ByVal nVirtKey As Int32) As Short
        End Function

        Declare Function SHGetFolderPath Lib "shfolder" _
            Alias "SHGetFolderPathA" _
            (ByVal hwndOwner As Long, ByVal nFolder As Long, _
            ByVal hToken As Long, ByVal dwFlags As Long, _
            ByVal pszPath As String) As Long

        Friend Const SYNCHRONIZE = &H100000
        Friend Const STANDARD_RIGHTS_READ = &H20000
        Friend Const STANDARD_RIGHTS_WRITE = &H20000
        Friend Const STANDARD_RIGHTS_EXECUTE = &H20000
        Friend Const STANDARD_RIGHTS_REQUIRED = &HF0000
        Friend Const STANDARD_RIGHTS_ALL = &H1F0000
        Friend Const KEY_QUERY_VALUE = &H1
        Friend Const KEY_SET_VALUE = &H2
        Friend Const KEY_CREATE_SUB_KEY = &H4
        Friend Const KEY_ENUMERATE_SUB_KEYS = &H8
        Friend Const KEY_NOTIFY = &H10
        Friend Const KEY_CREATE_LINK = &H20
        Friend Const KEY_READ = ((STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE _
         Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
        Friend Const KEY_WRITE = ((STANDARD_RIGHTS_WRITE Or KEY_SET_VALUE _
         Or KEY_CREATE_SUB_KEY) And (Not SYNCHRONIZE))
        Friend Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL _
         Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY _
         Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) _
         And (Not SYNCHRONIZE))
    End Class
End Namespace



