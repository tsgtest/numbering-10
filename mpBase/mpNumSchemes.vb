Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cRegistry
Imports LMP.Numbering.Base.cStrings
Imports System.IO.Path
Imports System.Reflection.Assembly
Imports LMP.Numbering.Base.mpDeclares
Imports LMP.Numbering.Base.cConstants

Namespace LMP.Numbering.Base
    Friend Class mpNumSchemes
        Public Shared g_iTCEntryColor As Integer
        Public Shared g_bIsAdmin As Boolean
        Public Shared g_xAdminPath As String
        Public Shared g_xUserPath As String
        Public Shared g_sSession As Single
        Public Shared g_xAppPath As String
        Public Shared g_lUILanguage As Long
        Public Shared g_xAppName As String
        Public Shared g_oPNumSty As Word.Template
        Public Shared g_xFirmIni As String
        Public Shared g_xFirmXMLConfig As String
        Public Shared g_xUserIni As String
        Public Shared g_xUserXMLConfig As String
        Public Shared g_xSettingsFileName As String

        Public Shared Function GetNumAppPaths() As Long
            On Error Resume Next

            '   get admin schemes directory
            g_xAdminPath = GetAppSetting("Numbering", "AdminDirectory")
            If g_xAdminPath = "" Then
                g_xAdminPath = GetAppPath() & "\Admin Schemes"
            ElseIf (InStr(UCase(g_xAdminPath), "<USERNAME>") > 0) Or _
                    (InStr(UCase(g_xAdminPath), "<USER>") > 0) Then
                '       use API to get Windows user name
                g_xAdminPath = GetUserVarPath(g_xAdminPath)
            Else
                '       use environmental variable
                g_xAdminPath = GetEnvironVarPath(g_xAdminPath)
            End If

            '   strip trailing slash if necessary
            If Right(g_xAdminPath, 1) = "\" Then
                g_xAdminPath = Left(g_xAdminPath, Len(g_xAdminPath) - 1)
            End If

            '   get personal directory
            g_xUserPath = GetUserDir()
        End Function

        Public Shared Function oSchemeSource(ByVal iType As mpSchemeTypes) As Object
            'returns the source file of scheme type iType
            If g_xAdminPath = "" Or _
                    g_xUserPath = "" Then
                GetNumAppPaths()
            End If

            Select Case iType
                Case mpSchemeTypes.mpSchemeType_Private
                    If g_bIsAdmin Then
                        oSchemeSource = GetTemplate(g_xAdminPath & "\tsgNumbers.sty")
                    Else
                        '9.9.3006 - we're now holding onto the personal tsgNumbers.sty to
                        'avoid having to cycle through the templates collection each time
                        'to get it from a network location in Word 2007
                        If g_oPNumSty Is Nothing Then _
                                g_oPNumSty = GetTemplate(g_xUserPath & "\tsgNumbers.sty")
                        oSchemeSource = g_oPNumSty
                    End If
                Case mpSchemeTypes.mpSchemeType_Document
                    oSchemeSource = CurWordApp.ActiveDocument
                Case mpSchemeTypes.mpSchemeType_Public
                    If g_bIsAdmin Then
                        oSchemeSource = GetTemplate(GetAppPath() & "\tsgNumbers.sty.clean")
                    Else
                        oSchemeSource = GetTemplate(GetAppPath() & "\tsgNumbers.sty")
                    End If
                Case mpSchemeTypes.mpSchemeType_TOC
                    oSchemeSource = GetTemplate(GetAppPath() & "\tsgTOC.sty")
            End Select
        End Function

        Public Shared Function xGetPropFld(ltScheme As Word.ListTemplate, _
                                        iLevel As Integer) As String
            Dim oSource As Object
            Dim bIsSourceFile As Boolean
            Dim iPos As Integer
            Dim iNext As Integer
            Dim xName As String
            Dim i As Integer
            Dim oProp As Word.CustomProperty

            oSource = ltScheme.Parent.Parent

            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            xName = ltScheme.Name

            If bIsSourceFile Then
                iPos = InStr(xName, "|")
                If iPos > 0 Then _
                    xName = Left(xName, iPos - 1)
                xGetPropFld = oSource.CustomDocumentProperties(xName & iLevel).Value
            Else
                '       find starting point of level prop
                For i = 1 To iLevel
                    iPos = InStr(iPos + 1, xName, "||")
                Next i

                '       find starting point of next level prop
                iNext = InStr(iPos + 1, xName, "||")

                '       parse between start and end points -
                '       include starting and end pipe
                xGetPropFld = Mid(xName, iPos + 1, _
                             iNext - iPos)
            End If
        End Function

        Public Shared Function SetPropFld(ltScheme As Word.ListTemplate, _
                                    iLevel As Integer, _
                                    ByVal xValue As String) As Long
            Dim oSource As Object
            Dim bIsSourceFile As Boolean
            Dim iPos As Integer
            Dim iNext As Integer
            Dim xName As String
            Dim i As Integer

            oSource = ltScheme.Parent.Parent
            On Error Resume Next
            bIsSourceFile = Not (oSource _
                .CustomDocumentProperties("MPN90SourceFile") Is Nothing)
            On Error GoTo 0

            xName = ltScheme.Name

            If bIsSourceFile Then
                iPos = InStr(xName, "|")
                If iPos > 0 Then _
                    xName = Left(xName, iPos - 1)
                On Error Resume Next
                oSource.CustomDocumentProperties( _
                    xName & iLevel).Value = xValue
                If Err.Number Then
                    oSource.CustomDocumentProperties.Add( _
                        xName & iLevel, False, 4, xValue)
                End If
            Else
                '       only edit list template name if props are already there
                If InStr(xName, "|") = 0 Then _
                    Exit Function

                '       find starting point of level prop
                For i = 1 To iLevel
                    iPos = InStr(iPos + 1, xName, "||")
                Next i

                '       find starting point of next level prop
                iNext = InStr(iPos + 1, xName, "||")

                '       insert between start and end points -
                '       include starting and end pipe
                ltScheme.Name = Left(xName, iPos) & xValue & _
                    Mid(xName, iNext + 1)
            End If
        End Function
        Public Shared Function GetLTRoot(xLT As String) As String
            Dim iPos As Integer

            iPos = InStr(xLT, "|")
            If iPos = 0 Then
                GetLTRoot = xLT
            Else
                GetLTRoot = Left(xLT, iPos - 1)
            End If
        End Function

        Public Shared Function GetFullLTName(xScheme As String, _
                                Optional xSource As String = "") As String
            Dim ltScheme As Word.ListTemplate
            Dim oSource As Object

            If xSource = "" Then
                oSource = CurWordApp.ActiveDocument
            ElseIf InStr(UCase(xSource), "TSGNUMBERS.STY") Then
                GetFullLTName = xScheme
                Exit Function
            Else
                oSource = CurWordApp.Documents(xSource)
            End If

            For Each ltScheme In oSource.ListTemplates
                On Error Resume Next
                If Left(ltScheme.Name, Len(xScheme) + 1) = _
                        xScheme & "|" Then
                    GetFullLTName = ltScheme.Name
                    Exit Function
                End If
            Next ltScheme

            '   if list template isn't in source, return string unchanged
            GetFullLTName = xScheme
        End Function

        Public Shared Function GetAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            Dim oReg As cRegistry
            Dim xPath As String
            Dim lRet As Long
            Dim xTemp As String

            If g_xAppPath = "" Then
                oReg = New cRegistry

                'look for new registry key first
                xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\Numbering", "DataDirectory")
                xPath = RTrim(xPath)

                If Len(xPath) = 0 Then
                    'look for old registry key value
                    xPath = oReg.GetLocalMachineValue("Software\Legal MacPac\9.x", "NumberingData")
                    xPath = RTrim(xPath)
                End If

                'exists, so set path to value
                If Len(xPath) > 0 Then
                    'substitute real path for user var if it exists in string
                    If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                        'use API to get Windows user name
                        xTemp = GetUserVarPath(CStr(xPath))
                    ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                        xTemp = GetCommonDocumentsPath(CStr(xPath))
                    Else
                        'use environmental variable
                        xTemp = GetEnvironVarPath(CStr(xPath))
                    End If

                    'trim trailing slash
                    If Right$(xTemp, 1) = "\" Then _
                        xTemp = Left$(xTemp, Len(xTemp) - 1)

                    'if ini is not in specified path, use App.Path
                    If (Dir(xTemp & "\tsgNumbering.ini") = "") And _
                        (Dir(xTemp & "\tsgNumberingConfig.xml") = "") Then
                        xTemp = CodeBasePath
                    End If
                Else
                    'registry value empty, so set path to App.Path
                    xTemp = CodeBasePath
                End If

                g_xAppPath = xTemp
            End If

            'get ui language if we don't already have it
            If g_lUILanguage = 0 Then
                On Error Resume Next
                g_lUILanguage = CLng(GetAppSetting("General", "UILanguage"))
                On Error GoTo 0
                If g_lUILanguage = 0 Then _
                    g_lUILanguage = WdLanguageID.wdEnglishUS
            End If

            GetAppPath = g_xAppPath
        End Function

        Public Shared Function GetCommonDocumentsPath(ByVal xPath As String) As String
            'substitutes user path for <UserName>
            Dim xBuf As String

            '   get common documents folder
            xBuf = New String(" ", 255)

            SHGetFolderPath(0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf)

            If Len(xBuf) = 0 Then
                '       alert to no logon name
                Throw New Exception("Common_Documents is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   trim extraneous buffer chars
            xBuf = RTrim(xBuf)
            xBuf = Left(xBuf, Len(xBuf) - 1)

            '   substitute common documents at specified point in path
            GetCommonDocumentsPath = xSubstitute(xPath, "<Common_Documents>", xBuf)
        End Function

        Public Shared Function GetUserVarPath(ByVal xPath As String) As String
            'substitutes user path for <xToken>
            Dim xBuf As String
            Dim xToken As String

            '   get token - we now accept either "USER" or "USERNAME"
            If InStr(UCase(xPath), "<USER>") > 0 Then
                xToken = "<User>"
            Else
                xToken = "<UserName>"
            End If

            '   get logon name
            xBuf = Environment.UserName()

            If Len(xBuf) = 0 Then
                '       alert to no logon name
                Throw New Exception("UserName is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   substitute user name at specified point in path
            GetUserVarPath = xSubstitute(xPath, xToken, xBuf)
        End Function

        Public Shared Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            Dim xToken As String
            Dim iPosStart As Integer
            Dim iPosEnd As Integer
            Dim xValue As String

            iPosStart = InStr(xPath, "<")
            iPosEnd = InStr(xPath, ">")

            If (iPosStart > 0) And (iPosEnd > 0) Then
                xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
                xValue = Environ(xToken)
            End If

            If xValue <> "" Then
                GetEnvironVarPath = xSubstitute(xPath, "<" & xToken & ">", xValue)
            Else
                GetEnvironVarPath = xPath
            End If
        End Function

        Public Shared Function GetMacPacAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            Dim oReg As cRegistry
            Dim xPath As String
            Dim lRet As Long
            Dim xTemp As String

            oReg = New cRegistry

            'GLOG 5056 (2/1/12) - look for new registry key first
            xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\MacPac 9.0", "DataDirectory")
            xPath = RTrim(xPath)

            If Len(xPath) = 0 Then
                'look for old registry key value
                xPath = oReg.GetLocalMachineValue("Software\Legal MacPac\9.x", "MacPacData")
                xPath = RTrim(xPath)
            End If

            'exists, so set path to value
            If Len(xPath) > 0 Then
                'substitute real path for user var if it exists in string
                If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                    'use API to get Windows user name
                    xTemp = GetUserVarPath(CStr(xPath))
                ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                    xTemp = GetCommonDocumentsPath(CStr(xPath))
                Else
                    'use environmental variable
                    xTemp = GetEnvironVarPath(CStr(xPath))
                End If

                If Dir(xTemp & "\MacPac.ini") = "" Then
                    'ini is not in specified location, so set path to App.Path
                    xTemp = oReg.GetComponentPath("MPO.CApplication")
                End If
            Else
                'registry value empty, so set path to App.Path
                xTemp = oReg.GetComponentPath("MPO.CApplication")
            End If

            'GLOG 5056 - we assume a trailing backslash
            If xTemp <> "" Then
                If Right$(xTemp, 1) <> "\" Then _
                    xTemp = xTemp & "\"
            End If

            GetMacPacAppPath = xTemp
        End Function

        Public Shared Function GetTemplate(xFile As String) As Word.Template
            'added in 9.9.3006 to workaround native Word 2007 SP2 bug whereby
            'Word.Templates() no longer takes a network path
            Dim oTemplate As Word.Template
            Dim i As Integer
            Dim xName As String
            Dim lPos As Long

            On Error Resume Next
            oTemplate = CurWordApp.Templates(xFile)
            On Error GoTo 0

            If oTemplate Is Nothing Then
                lPos = InStrRev(xFile, "\")
                xName = Mid$(xFile, lPos + 1)
                For i = 1 To CurWordApp.Templates.Count
                    If CurWordApp.Templates(i).Name = xName Then
                        oTemplate = CurWordApp.Templates(i)
                        Exit For
                    End If
                Next i
            End If

            GetTemplate = oTemplate
        End Function
    End Class
End Namespace