Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("tsgControls3")> 
<Assembly: AssemblyDescription("TSG Spinner Control")> 
<Assembly: AssemblyCompany("The Sackett Group, Inc.")>
<Assembly: AssemblyProduct("TSG")> 
<Assembly: AssemblyCopyright("�1990 - 2016")> 
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("10.0.0.28")> 



<Assembly: AssemblyFileVersionAttribute("10.0.0.28")> 
