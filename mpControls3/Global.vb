Option Strict Off
Option Explicit On
'UPGRADE_NOTE: Global was upgraded to Global_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
Module Global_Renamed
	
	Public Function Max(ByRef i As Decimal, ByRef j As Decimal) As Decimal
		If i > j Then
			Max = i
		Else
			Max = j
		End If
	End Function
	
	Public Function Min(ByRef i As Decimal, ByRef j As Decimal) As Decimal
		If i > j Then
			Min = j
		Else
			Min = i
		End If
	End Function
	
	Public Function xSubstitute(ByVal xString As String, ByVal xSearch As String, ByVal xReplace As String) As String
		'replaces xSearch in
		'xString with xReplace -
		'returns modified xString -
		'NOTE: SEARCH IS NOT CASE SENSITIVE
		Dim lSearchPos As Integer
		Dim xNewString As String
		
		'   if xSearch is empty, InStr() will always return 1,
		'   resulting in an infinite loop; return string as is;
		'   this is a fix for Macpac 9 generic log item #1726
		If xSearch = "" Then
			xSubstitute = xString
			Exit Function
		End If
		
		'   get first char pos
		lSearchPos = InStr(UCase(xString), UCase(xSearch))
		
		'   remove switch all chars
		While lSearchPos
			xNewString = xNewString & Left(xString, lSearchPos - 1) & xReplace
			xString = Mid(xString, lSearchPos + Len(xSearch))
			lSearchPos = InStr(UCase(xString), UCase(xSearch))
		End While
		
		xNewString = xNewString & xString
		xSubstitute = xNewString
	End Function
End Module