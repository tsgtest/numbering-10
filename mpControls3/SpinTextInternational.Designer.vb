<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated(), ToolboxBitmap(GetType(SpinTextInternational), "SpinTextInternational.ToolboxBitmap")> Partial Class SpinTextInternational
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.spnText = New ExtendedNumericUpDown()
        CType(Me.spnText, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtText
        '
        Me.txtText.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtText.BackColor = System.Drawing.Color.White
        Me.txtText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtText.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtText.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtText.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtText.Location = New System.Drawing.Point(0, 0)
        Me.txtText.Margin = New System.Windows.Forms.Padding(1)
        Me.txtText.Name = "txtText"
        Me.txtText.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtText.Size = New System.Drawing.Size(179, 20)
        Me.txtText.TabIndex = 0
        '
        'spnText
        '
        Me.spnText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spnText.AutoSize = True
        Me.spnText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.spnText.DecimalPlaces = 2
        Me.spnText.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnText.Location = New System.Drawing.Point(140, 0)
        Me.spnText.Margin = New System.Windows.Forms.Padding(0)
        Me.spnText.MinimumSize = New System.Drawing.Size(18, 0)
        Me.spnText.Name = "spnText"
        Me.spnText.Size = New System.Drawing.Size(56, 20)
        Me.spnText.TabIndex = 1
        Me.spnText.TabStop = False
        Me.spnText.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'SpinTextInternational
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.spnText)
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SpinTextInternational"
        Me.Size = New System.Drawing.Size(196, 20)
        CType(Me.spnText, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents spnText As ExtendedNumericUpDown
    Private WithEvents txtText As System.Windows.Forms.TextBox
#End Region
#Region "Upgrade Support"
	<System.Runtime.InteropServices.ProgId("MouseDownEventArgs_NET.MouseDownEventArgs")> Public NotInheritable Class MouseDownEventArgs
		Inherits System.EventArgs
		Public Button As Short
		Public Shift As Short
		Public X As Single
		Public Y As Single
		Public Sub New(ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef Y As Single)
			MyBase.New()
			Me.Button = Button
			Me.Shift = Shift
			Me.X = X
			Me.Y = Y
		End Sub
	End Class
	<System.Runtime.InteropServices.ProgId("KeyDownEventArgs_NET.KeyDownEventArgs")> Public NotInheritable Class KeyDownEventArgs
		Inherits System.EventArgs
		Public KeyCode As Short
		Public Shift As Short
		Public Sub New(ByRef KeyCode As Short, ByRef Shift As Short)
			MyBase.New()
			Me.KeyCode = KeyCode
			Me.Shift = Shift
		End Sub
	End Class
#End Region 
End Class