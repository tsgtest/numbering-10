Option Strict Off
Option Explicit On

Imports LMP

<System.Runtime.InteropServices.ProgId("SpinTextInternational_NET.SpinTextInternational")> Public Class SpinTextInternational
	Inherits System.Windows.Forms.UserControl
	Public Event AppendSymbolChange()
	Public Event AllowFractionsChange()
	Public Event ValueUnitChange()
	Public Event MaxValueChange()
	Public Event BackColorChange()
	Public Event EnabledChange()
	Public Event ValueChange()
	Public Event VisibleChange()
	Public Event DisplayUnitChange()
	Public Event MinValueChange()
	Public Event AppearanceChange()
    Public Event IncrementValueChange()
    'VB6 InitProperties replaced with initializers 
    Private m_IncrementValue As Decimal = 1
    Private m_MinValue As Decimal = 0
    Private m_MaxValue As Decimal = 100
    Private m_AppendSymbol As Boolean = False
    Private m_Value As Decimal = 0
    Private m_Symbol As String = " pt" 'JTS: Initialized to match default for DisplayUnit
    Private m_ValueUnit As stbUnits = stbUnits.stbUnit_Points
    Private m_DisplayUnit As stbUnits = stbUnits.stbUnit_Points
    Private m_DisplayValue As Decimal = System.Math.Round(m_Converter * m_Value, m_DecimalPlaces) 'JTS: Initialized based on default properties
    Private m_Converter As Double = UnitsPerBase(m_DisplayUnit) / UnitsPerBase(m_ValueUnit) 'JTS: Initialized based on default DisplayUnit and ValueUnit
    Private m_MaxDisplay As Decimal = System.Math.Round(m_Converter * m_MaxValue, m_DecimalPlaces) 'JTS: Initialized based on default properties
    Private m_MinDisplay As Decimal = System.Math.Round(m_Converter * m_MinValue, m_DecimalPlaces) 'JTS: Initialized based on default properties
    Private m_bNoChange As Boolean = False
    Private m_AllowFractions As Boolean = True
    Private m_DecimalPlaces As Short = 2 'JTS: Initialized to match default for AllowFractions
    Private m_Appearance As stbAppearance = stbAppearance.Flat

	Public Enum stbUnits
		stbUnit_Inches = 0
		stbUnit_Centimeters = 1
		stbUnit_Millimeters = 2
		stbUnit_Points = 3
		stbUnit_Picas = 4
	End Enum
	
	Public Enum stbAppearance
		Flat = 0
		ThreeDimensional = 1
	End Enum
	
	Private Const mpUnitsPerBase_Inches As Decimal = 1
	Private Const mpUnitsPerBase_Centimeters As Decimal = 2.54
	Private Const mpUnitsPerBase_Millimeters As Decimal = 25.4
	Private Const mpUnitsPerBase_Points As Decimal = 72
	Private Const mpUnitsPerBase_Picas As Decimal = 6
	
	Public Shadows Event KeyDown(ByVal Sender As System.Object, ByVal e As KeyDownEventArgs)
	Public Shadows Event MouseDown(ByVal Sender As System.Object, ByVal e As MouseDownEventArgs)
	Public Event Change(ByVal Sender As System.Object, ByVal e As System.EventArgs)
	Public Event SpinUp(ByVal Sender As System.Object, ByVal e As System.EventArgs)
	Public Event SpinDown(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
	'*************************************************************************
	'   Properties
	'*************************************************************************
	Public Overrides Property BackColor() As System.Drawing.Color
		Get
            BackColor = Me.txtText.BackColor
		End Get
		Set(ByVal Value As System.Drawing.Color)
            Me.txtText.BackColor = Value
			RaiseEvent BackColorChange()
		End Set
	End Property
	
	
	Public Shadows Property Enabled() As Boolean
		Get
			Enabled = MyBase.Enabled
		End Get
		Set(ByVal Value As Boolean)
			MyBase.Enabled = Value
            Me.txtText.Enabled = Value
			RaiseEvent EnabledChange()
		End Set
	End Property
	
    Public Shadows Property Visible() As Boolean
        Get
            Visible = MyBase.Visible
        End Get
        Set(ByVal Value As Boolean)
            MyBase.Visible = Value
            Me.txtText.Visible = Value
            RaiseEvent VisibleChange()
        End Set
    End Property

	Public Property Appearance() As stbAppearance
		Get
			Appearance = m_Appearance
		End Get
		Set(ByVal Value As stbAppearance)
            If Value <> m_Appearance Then
                m_Appearance = Value
                'JTS 8/3/16: Not really a need for this property any more,
                'but has been left in place for compatibility.
                'There is only a single textbox control now
                With txtText
                    If m_Appearance = stbAppearance.ThreeDimensional Then
                        .BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
                    Else
                        .BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
                    End If
                End With
                RaiseEvent AppearanceChange()
            End If
		End Set
	End Property
    Public Property AllowFractions() As Boolean
        Get
            AllowFractions = m_AllowFractions
        End Get
        Set(ByVal Value As Boolean)
            If Value <> m_AllowFractions Then
                m_AllowFractions = Value
                If m_AllowFractions = False Then
                    m_DecimalPlaces = 0
                    '           if fractions not allowed, force increment to 1
                    If CInt(m_IncrementValue) <> m_IncrementValue Then m_IncrementValue = 1
                Else
                    m_DecimalPlaces = 2
                End If
                SetDisplayUnit()
                RaiseEvent AllowFractionsChange()
            End If
        End Set
    End Property
    Public Property IncrementValue() As Decimal
        Get
            IncrementValue = m_IncrementValue
        End Get
        Set(ByVal Value As Decimal)
            If Value <> m_IncrementValue Then
                m_IncrementValue = Value
                'if increment isn't whole, allow fractions
                If CInt(m_IncrementValue) <> m_IncrementValue Then
                    m_AllowFractions = True
                    m_DecimalPlaces = 2
                End If
                SetDisplayUnit()
                RaiseEvent IncrementValueChange()
            End If
        End Set
    End Property
    Public Property MinValue() As Decimal
        Get
            MinValue = m_MinValue
        End Get
        Set(ByVal Value As Decimal)
            If Value <> m_MinValue Then
                m_MinValue = Value
                spnText.Minimum = m_MinValue
                SetDisplayUnit()
                RaiseEvent MinValueChange()
            End If
        End Set
    End Property
	
	Public Property MaxValue() As Decimal
		Get
			MaxValue = m_MaxValue
		End Get
		Set(ByVal Value As Decimal)
            If Value <> m_MaxValue Then
                m_MaxValue = Value
                spnText.Maximum = m_MaxValue
                SetDisplayUnit()
                RaiseEvent MaxValueChange()
            End If
        End Set
	End Property
	
	
	Public Property ValueUnit() As stbUnits
		Get
			ValueUnit = m_ValueUnit
		End Get
		Set(ByVal Value As stbUnits)
            If Value <> m_ValueUnit Then
                m_ValueUnit = Value
                SetDisplayUnit()
                RaiseEvent ValueUnitChange()
            End If
        End Set
	End Property
	
	
	Public Property DisplayUnit() As stbUnits
		Get
			DisplayUnit = m_DisplayUnit
		End Get
		Set(ByVal Value As stbUnits)
            If Value <> m_DisplayUnit Then
                m_DisplayUnit = Value
                SetDisplayUnit()
                RaiseEvent DisplayUnitChange()
            End If
        End Set
	End Property
	
	
	Public Property AppendSymbol() As Boolean
		Get
			AppendSymbol = m_AppendSymbol
		End Get
		Set(ByVal Value As Boolean)
            If Value <> m_AppendSymbol Then
                m_AppendSymbol = Value
                m_bNoChange = True
                If Value Then
                    txtText.Text = AddSymbol(txtText.Text)
                Else
                    txtText.Text = Trim(xSubstitute(txtText.Text, m_Symbol, ""))
                End If
                m_bNoChange = False
                RaiseEvent AppendSymbolChange()
            End If
        End Set
    End Property


    Public Property Value() As Decimal
        Get
            Value = m_Value
        End Get
        Set(ByVal Value As Decimal)
            If Value <> m_Value And Value >= m_MinValue And Value <= m_MaxValue Then
                m_Value = Value
                m_bNoChange = True
                m_DisplayValue = System.Math.Round(m_Converter * m_Value, m_DecimalPlaces)
                spnText.Value = m_Value
                txtText.Text = CStr(m_DisplayValue)
                If Me.AppendSymbol Then txtText.Text = AddSymbol(txtText.Text)
                m_bNoChange = False
                RaiseEvent ValueChange()
            End If
        End Set
    End Property

    Public ReadOnly Property DisplayValue() As Decimal
        Get
            DisplayValue = m_DisplayValue
        End Get
    End Property


    Public Property DisplayText() As String
        Get
            DisplayText = Me.txtText.Text
        End Get
        Set(ByVal Value As String)
            m_bNoChange = True
            Me.txtText.Text = Value
            m_bNoChange = False
        End Set
    End Property


    Public Property SelLength() As Integer
        Get
            SelLength = Me.txtText.SelectionLength
        End Get
        Set(ByVal Value As Integer)
            Me.txtText.SelectionLength = Value
        End Set
    End Property


    Public Property SelStart() As Integer
        Get
            SelStart = Me.txtText.SelectionStart
        End Get
        Set(ByVal Value As Integer)
            Me.txtText.SelectionStart = Value
        End Set
    End Property


    Public Property SelText() As String
        Get
            SelText = Me.txtText.SelectedText
        End Get
        Set(ByVal Value As String)
            Me.txtText.SelectedText = Value
        End Set
    End Property


    '******************************************************************
    '---internal functions
    '******************************************************************
    'UPGRADE_WARNING: Event txtText.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub txtText_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtText.TextChanged
        Try
            If Not m_bNoChange Then
                RaiseEvent Change(Me, Nothing)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtText_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtText.Enter
        Try
            txtText.SelectionStart = 0
            txtText.SelectionLength = Len(txtText.Text)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub txtText_KeyDown(sender As Object, e As KeyEventArgs) Handles txtText.KeyDown
        Try
            If e.KeyCode = 40 Then
                spnText.DownButton()
            ElseIf e.KeyCode = 38 Then
                spnText.UpButton()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Function AddSymbol(ByVal xText As String) As String
        Dim xTemp As String
        If xText = "" Then
            AddSymbol = ""
            Exit Function
        End If
        xTemp = Trim(xSubstitute(xText, Trim(m_Symbol), ""))
        xTemp = xTemp & m_Symbol
        AddSymbol = xTemp
    End Function

    Public Function IsValid(Optional ByRef bDisplayMessage As Boolean = True) As Boolean
        Dim xTemp As String
        Dim xSymbol As String
        Dim xSubject As String

        '   remove symbol
        xTemp = Trim(xSubstitute(txtText.Text, m_Symbol, ""))

        If Not IsNumeric(xTemp) Then
            '       check for symbol of different unit
            xTemp = ConvertToDisplayUnit(xTemp)

            '       remove symbol
            xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
        End If

        '   empty will be changed to 0 on lost focus
        If xTemp = "" Then
            IsValid = True
            Exit Function
        End If

        '   if displaying symbol, include in message
        If m_AppendSymbol Then
            xSymbol = m_Symbol
            xSubject = "measurement"
        Else
            xSubject = "value"
        End If

        If (Not IsNumeric(xTemp)) Or SeparatorIsInvalid(xTemp) Then
            '       display message
            If bDisplayMessage Then
                MsgBox("This is not a valid " & xSubject & ".", MsgBoxStyle.Exclamation, "TSG Numbering")
            End If
        ElseIf (CDec(xTemp) > m_MaxDisplay) Or (CDec(xTemp) < m_MinDisplay) Then
            '       display message
            If bDisplayMessage Then
                MsgBox("The " & xSubject & " must be between " & m_MinDisplay & xSymbol & " and " & m_MaxDisplay & xSymbol & ".", MsgBoxStyle.Exclamation, "TSG Numbering")
            End If
        ElseIf (m_AllowFractions = False) And (CInt(xTemp) <> CDec(xTemp)) Then
            '       display message
            If bDisplayMessage Then
                MsgBox("The " & xSubject & " must be a whole number between " & m_MinDisplay & xSymbol & " and " & m_MaxDisplay & xSymbol & ".", MsgBoxStyle.Exclamation, "TSG Numbering")
            End If
        Else
            IsValid = True
            Exit Function
        End If

        '   select entire text
        txtText.SelectionStart = 0
        txtText.SelectionLength = Len(txtText.Text)
    End Function
    Private Function UnitsPerBase(ByVal iUnit As stbUnits) As Decimal
        Select Case iUnit
            Case stbUnits.stbUnit_Inches
                UnitsPerBase = mpUnitsPerBase_Inches
            Case stbUnits.stbUnit_Centimeters
                UnitsPerBase = mpUnitsPerBase_Centimeters
            Case stbUnits.stbUnit_Millimeters
                UnitsPerBase = mpUnitsPerBase_Millimeters
            Case stbUnits.stbUnit_Points
                UnitsPerBase = mpUnitsPerBase_Points
            Case stbUnits.stbUnit_Picas
                UnitsPerBase = mpUnitsPerBase_Picas
            Case Else
                UnitsPerBase = mpUnitsPerBase_Points
        End Select
    End Function

    Private Function GetSymbol(ByVal iUnit As stbUnits) As String
        Select Case iUnit
            Case stbUnits.stbUnit_Inches
                GetSymbol = Chr(34)
            Case stbUnits.stbUnit_Centimeters
                GetSymbol = " cm"
            Case stbUnits.stbUnit_Millimeters
                GetSymbol = " mm"
            Case stbUnits.stbUnit_Points
                GetSymbol = " pt"
            Case stbUnits.stbUnit_Picas
                GetSymbol = " pi"
            Case Else
                GetSymbol = " pt"
        End Select
    End Function

    Private Sub SetDisplayUnit()
        'sets module level variables for display unit
        Dim curDisplay As Decimal
        Dim curValue As Decimal

        '   converter
        curValue = UnitsPerBase(m_ValueUnit)
        Debug.Print("SetDisplayUnit - curValue = " & curValue.ToString())
        curDisplay = UnitsPerBase(m_DisplayUnit)
        Debug.Print("SetDisplayUnit - curDisplay = " & curDisplay.ToString())
        m_Converter = curDisplay / curValue
        spnText.Increment = m_IncrementValue / m_Converter
        Debug.Print("SetDisplayUnit - m_Converter = " & m_Converter.ToString())

        '   symbol
        m_Symbol = GetSymbol(m_DisplayUnit)

        '   max/min
        m_MaxDisplay = System.Math.Round(m_Converter * m_MaxValue, m_DecimalPlaces)
        m_MinDisplay = System.Math.Round(m_Converter * m_MinValue, m_DecimalPlaces)

        ''   increment
        '    If m_ValueUnit < 2 And m_DisplayUnit > 1 Then
        ''       adjust increment for smaller unit
        '        m_IncrementDisplay = m_IncrementValue * 10
        '    Else
        '        m_IncrementDisplay = m_IncrementValue
        '    End If
    End Sub

    Private Function NextIncrement(ByVal curValue As Decimal, ByVal bUp As Boolean) As Decimal
        Dim curTemp As Decimal
        Dim lTemp As Integer
        Dim curTest As Decimal

        Debug.Print("curValue = " & curValue)
        Debug.Print("m-IncrementValue = " & m_IncrementValue)
        '   round to nearest increment
        curTemp = curValue / m_IncrementValue
        Debug.Print("curTemp = " & curTemp)
        lTemp = CInt(curTemp)

        If lTemp = curTemp Then
            curTemp = curValue
        Else
            curTemp = lTemp * m_IncrementValue
        End If

        '   increment/decrement
        If bUp Then
            If curTemp > curValue Then
                NextIncrement = curTemp
            Else
                NextIncrement = curTemp + m_IncrementValue
            End If
        Else
            If curTemp < curValue Then
                NextIncrement = curTemp
            Else
                NextIncrement = curTemp - m_IncrementValue
            End If
        End If
        Debug.Print("NextIncrement = " & NextIncrement)
    End Function

    Private Function ConvertToDisplayUnit(ByVal xText As String) As String
        Dim iUnit As stbUnits
        Dim xTemp As String
        Dim curValue As Decimal
        Dim curDisplay As Decimal
        Dim curUnit As Decimal
        Dim xSymbol As String

        '   identify unit by looking
        '   for one of the standard symbols
        xTemp = xText
        If InStr(xTemp, Chr(34)) Then
            iUnit = stbUnits.stbUnit_Inches
        ElseIf InStr(UCase(xTemp), "CM") Then
            iUnit = stbUnits.stbUnit_Centimeters
        ElseIf InStr(UCase(xTemp), "MM") Then
            iUnit = stbUnits.stbUnit_Millimeters
        ElseIf InStr(UCase(xTemp), "PT") Then
            iUnit = stbUnits.stbUnit_Points
        ElseIf InStr(UCase(xTemp), "PI") Then
            iUnit = stbUnits.stbUnit_Picas
        Else
            '       no symbol found - return unchanged
            ConvertToDisplayUnit = xTemp
            Exit Function
        End If

        '   remove symbol
        xSymbol = GetSymbol(iUnit)
        xTemp = Trim(xSubstitute(xText, Trim(xSymbol), ""))

        If Not IsNumeric(xTemp) Then
            '       return unchanged
            ConvertToDisplayUnit = xTemp
            Exit Function
        End If

        '   convert to display unit
        curDisplay = UnitsPerBase(m_DisplayUnit)
        curUnit = UnitsPerBase(iUnit)
        curValue = System.Math.Round(CDec(xTemp) * (curDisplay / curUnit), m_DecimalPlaces)
        xTemp = Trim(CStr(curValue))
        xTemp = AddSymbol(xTemp)
        ConvertToDisplayUnit = xTemp
    End Function

    '****************************************************************
    '---methods
    '****************************************************************

    Private Sub SpinTextInternational_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            m_bNoChange = True
            SetDisplayUnit()
            m_DisplayValue = System.Math.Round(m_Converter * m_Value, m_DecimalPlaces)
            txtText.Text = CStr(m_DisplayValue)
            If Me.AppendSymbol Then txtText.Text = AddSymbol(txtText.Text)
            spnText.Value = m_Value
            m_bNoChange = False
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub SpinTextInternational_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        Try
            Me.txtText.Width = Me.Width - 18
            Me.spnText.Left = Me.Width - Me.spnText.Width
            Me.spnText.SendToBack()
            Me.txtText.BringToFront()
            Me.Height = Me.txtText.Height
            Me.Refresh()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Public Overrides Sub Refresh()
        'refreshes display
        txtText.Text = CStr(m_DisplayValue)
        If Me.AppendSymbol Then txtText.Text = AddSymbol(txtText.Text)
    End Sub

    Private Function SeparatorIsInvalid(ByRef xText As String) As Boolean
        Dim xDecimal As String

        xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
        If xDecimal = "." Then
            If InStr(xText, ",") Then SeparatorIsInvalid = True
        Else
            If InStr(xText, ".") Then SeparatorIsInvalid = True
        End If
    End Function
    Private Sub txtText_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtText.Validating
        Try
            RaiseEvent Validating(Me, e)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub txtText_Validated(sender As Object, e As EventArgs) Handles txtText.Validated
        Try
            Dim xTemp As String
            '   remove symbol
            xTemp = Trim(xSubstitute(txtText.Text, m_Symbol, ""))
            If xTemp = "" Then
                xTemp = "0"
            End If
            If Not IsNumeric(xTemp) Then
                'check for symbol of different unit
                xTemp = ConvertToDisplayUnit(xTemp)
                'remove symbol
                xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
            End If
            If IsNumeric(xTemp) Then
                m_DisplayValue = CDec(xTemp)
                If Me.AppendSymbol Then
                    txtText.Text = AddSymbol(CStr(m_DisplayValue))
                Else
                    txtText.Text = CStr(m_DisplayValue)
                End If
                m_Value = m_DisplayValue / m_Converter
                spnText.Value = m_Value
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    'Up or Down button was pressed
    Private Sub spnText_UpDown(Sender As Object, bUp As Boolean) Handles spnText.UpDown
        Try
            m_Value = spnText.Value
            m_DisplayValue = System.Math.Round(m_Converter * m_Value, m_DecimalPlaces)
            If Me.AppendSymbol Then
                txtText.Text = AddSymbol(CStr(m_DisplayValue))
            Else
                txtText.Text = CStr(m_DisplayValue)
            End If
            If bUp Then
                RaiseEvent SpinUp(Me.spnText, Nothing)
            Else
                RaiseEvent SpinDown(Me.spnText, Nothing)
            End If
            RaiseEvent Click(Me.spnText, Nothing)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class
'Override UpButton/DownButton handlers for new event
Friend Class ExtendedNumericUpDown
    Inherits NumericUpDown
    Public Event UpDown(ByVal Sender As System.Object, ByVal bUp As Boolean)
    Public Overrides Sub UpButton()
        MyBase.UpButton()
        RaiseEvent UpDown(Me, True)
    End Sub
    Public Overrides Sub DownButton()
        MyBase.DownButton()
        RaiseEvent UpDown(Me, False)
    End Sub
End Class
