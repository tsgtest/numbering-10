using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using Win32 = Microsoft.Win32;

namespace mp10Reg
{
    /// <summary>
    /// this application creates and updates the registry keys
    /// that allows TSGNumbering.dll to be used by COM
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] aArgs)
        {
            try
            {
                RegisterTLB();

                bool bMessage = true;
                string xArgs = string.Join(" ", aArgs).ToUpper();
                foreach (string xArg in aArgs)
                {
                    if (xArg.ToUpper().Substring(0, 2) == "/S")
                    {
                        bMessage = false;
                    }
                }

                //message if necessary
                if (bMessage)
                {
                    MessageBox.Show("All components were registered.", "TSG Numbering");
                }
             }
            catch (System.Exception oE)
            {
                System.Windows.Forms.MessageBox.Show(oE.Message, 
                    "Registration Error", MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// writes all registry keys necessary to
        /// register TSGNumbering.tlb
        /// </summary>
        public static void RegisterTLB()
        {
            Assembly oAsm = Assembly.GetExecutingAssembly();
            string xCodeBase = oAsm.CodeBase;
            xCodeBase = xCodeBase.Replace("tsgNumReg.exe", "tsgNumbering.dll");
            string xDLL = Assembly.GetExecutingAssembly().Location;
            xDLL = xDLL.Replace("tsgNumReg.exe", "tsgNumbering.dll");

            string xVersion = oAsm.GetName().Version.ToString();

            Win32.RegistryKey oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"MacPacNumbering.LMP.Numbering.PublicFunctions");
            oKey.SetValue("", "MacPacNumbering.LMP.Numbering.PublicFunctions");

            oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"MacPacNumbering.LMP.Numbering.PublicFunctions\CLSID");
            oKey.SetValue("", "{54994054-0205-40D7-AFE0-A7FE08593F68}");

            oKey = Win32.Registry.ClassesRoot
                .CreateSubKey(@"CLSID\{54994054-0205-40D7-AFE0-A7FE08593F68}");
            oKey.SetValue("", "MacPacNumbering.LMP.Numbering.PublicFunctions");

            try
            {
                Win32.Registry.ClassesRoot.DeleteSubKeyTree(
                    @"CLSID\{54994054-0205-40D7-AFE0-A7FE08593F68}\InprocServer32");
            }
            catch { }

            oKey = Win32.Registry.ClassesRoot.CreateSubKey(
                    @"CLSID\{54994054-0205-40D7-AFE0-A7FE08593F68}\InprocServer32");
            oKey.SetValue("", "mscoree.dll");
            oKey.SetValue("Assembly", "tsgNumbering, Version=" + xVersion +
                ", Culture=neutral, PublicKeyToken=737a799d780979fe");
            oKey.SetValue("Class", "MacPacNumbering.LMP.Numbering.PublicFunctions");
            oKey.SetValue("CodeBase", xCodeBase);
            oKey.SetValue("RuntimeVersion", "v4.0.30319");
            oKey.SetValue("ThreadingModel", "Both");

            Win32.RegistryKey oSubKey = oKey.CreateSubKey(xVersion);
            oSubKey.SetValue("Assembly", "tsgNumbering, Version=" + xVersion +
                ", Culture=neutral, PublicKeyToken=737a799d780979fe");
            oSubKey.SetValue("Class", "MacPacNumbering.LMP.Numbering.PublicFunctions");
            oSubKey.SetValue("CodeBase", xCodeBase);
            oSubKey.SetValue("RuntimeVersion", "v4.0.30319");

            //register tlb file
            string xRoot = @"TypeLib\{6f928976-47e5-4e47-97d6-320f5323bdac}";
            
            oKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot);

            //split version into major and minor
            string xMajor = String.Format("{0:x}", oAsm.GetName().Version.Major);
            string xMinor = String.Format("{0:x}", oAsm.GetName().Version.Minor);
            string xHexVersion = xMajor + "." + xMinor;

            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + @"\" + xHexVersion);
            oSubKey.SetValue("", "TSG Numbering Assembly");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\0");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\0\\win32");
            string xTlb = xDLL.Replace("tsgNumbering.dll", "tsgNumbering.tlb");
            oSubKey.SetValue("", xTlb);
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\FLAGS");
            oSubKey.SetValue("", "0");
            oSubKey = Win32.Registry.ClassesRoot.CreateSubKey(xRoot + "\\" + xHexVersion + "\\HELPDIR");
            oSubKey.SetValue("", xDLL.Replace("\\tsgNumbering.dll", ""));
        }
    }
}