﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("tsgNumReg")]
[assembly: AssemblyDescription("TSG Numbering Registration Tool")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The Sackett Group, Inc.")]
[assembly: AssemblyProduct("TSG Numbering")]
[assembly: AssemblyCopyright("©1990 - 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2246e106-67bd-448b-aa7b-e6e86a5ea227")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("10.0.0.28")]
[assembly: AssemblyFileVersion("10.0.0.28")]
