Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.Base.cNumTOC
Imports System.Windows.Forms
Imports LMP.Numbering.TOC.mpVariables

Namespace LMP.Numbering.TOC
    Friend Class cError
        Enum mpnErrors
            mpTOCError_NoTOCFound = 523
        End Enum

        Friend Sub Raise(CurError As ErrObject)
            Dim xDescription As String = ""
            Dim iSeverity As Integer

            EchoOn()
            CurWordApp.ScreenUpdating = True
            Cursor.Current = Cursors.Default
            Select Case CurError.Number
                Case mpnErrors.mpTOCError_NoTOCFound
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        Err.Description = "Aucune table des mati�res n'a �t� trouv� dans ce document."
                    Else
                        Err.Description = "Could not find a Table Of Contents in this document."
                    End If
                Case Else
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xDescription = "Une erreur inattendue s'est produite dans la proc�dure " & _
                            Err.Source & "." & vbCr & "Erreur #" & CurError.Number & ": " & _
                            CurError.Description
                    Else
                        xDescription = "An unexpected error occurred in procedure " & _
                            Err.Source & "." & vbCr & "Error #" & CurError.Number & ": " & _
                            CurError.Description
                    End If
                    iSeverity = vbCritical
            End Select
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                MsgBox(xDescription, iSeverity, "Table des mati�res TSG")
            Else
                MsgBox(xDescription, iSeverity, "TSG Table of Contents")
            End If
        End Sub
    End Class
End Namespace
