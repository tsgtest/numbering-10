Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.TOC.cError
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumTOC

Namespace LMP.Numbering.TOC
    Public Class cTOCProps
        '**********************************************************
        '   CTOCProps Class
        '   created 4/23/99 by Daniel Fisherman-
        '   momshead@earthlink.net

        '   Contains properties and methods that define the
        '   CTOCProps Class - the class that contains and implements
        '   scheme-level format properties of a toc
        '**********************************************************
        Public Enum mpTOCLevels
            mpTOCLevels_UseCurrent = -1
            mpTOCLevels_All = 0
            mpTOCLevels_1 = 1
            mpTOCLevels_Subsequent = 2
            mpTOCLevels_None = 3
        End Enum

        Public Enum mpLineSpacing
            mpLineSpacing_UseCurrent = -1
            mpLineSpacing_1 = 0
            mpLineSpacing_15 = 1
            mpLineSpacing_2 = 2
        End Enum

        Private Const mpTOCUseCurrent As Integer = -1

        Private m_iLineSpacing As Word.WdLineSpacing
        Private m_iAllCaps As mpTOCLevels
        Private m_iPageNumbers As mpTOCLevels
        Private m_iDotLeaders As mpTOCLevels
        Private m_bUseStepIndents As Boolean
        Private m_bCenterLevel1 As Boolean
        Private m_sSpaceBefore As Single
        Private m_sSpaceBetween As Single

        '**********************************************************
        'PROPERTIES

        '**********************************************************
        Public Property LineSpacing As mpLineSpacing
            Get
                LineSpacing = m_iLineSpacing
            End Get
            Set(value As mpLineSpacing)
                m_iLineSpacing = value
            End Set
        End Property

        Public Property AllCaps As mpTOCLevels
            Get
                AllCaps = m_iAllCaps
            End Get
            Set(value As mpTOCLevels)
                m_iAllCaps = value
            End Set
        End Property

        Public Property PageNumbers As mpTOCLevels
            Get
                PageNumbers = m_iPageNumbers
            End Get
            Set(value As mpTOCLevels)
                m_iPageNumbers = value
            End Set
        End Property

        Public Property DotLeaders As mpTOCLevels
            Get
                DotLeaders = m_iDotLeaders
            End Get
            Set(value As mpTOCLevels)
                m_iDotLeaders = value
            End Set
        End Property

        Public Property UseStepIndents As Boolean
            Get
                UseStepIndents = m_bUseStepIndents
            End Get
            Set(value As Boolean)
                m_bUseStepIndents = value
            End Set
        End Property

        Public Property CenterLevel1 As Boolean
            Get
                CenterLevel1 = m_bCenterLevel1
            End Get
            Set(value As Boolean)
                m_bCenterLevel1 = value
            End Set
        End Property

        Public Property SpaceBeforeLevel As Single
            Get
                SpaceBeforeLevel = m_sSpaceBefore
            End Get
            Set(value As Single)
                m_sSpaceBefore = value
            End Set
        End Property

        Public Property SpaceBetweenLevel As Single
            Get
                SpaceBetweenLevel = m_sSpaceBetween
            End Get
            Set(value As Single)
                m_sSpaceBetween = value
            End Set
        End Property

        '**********************************************************
        'METHODS
        '**********************************************************
        Public Function TOCRange() As Word.Range
            TOCRange = rngGetTOC()
        End Function


        Sub DoStepIndents()
            'formats current toc range with hanging step indents
            Dim i As Integer
            Dim iLevel As Integer
            Dim paraTOC As Word.Paragraph
            Dim xTabFlag(8) As String
            Dim sHPos As Single
            Dim xStartRange As String

            If (Me.TOCRange Is Nothing) Then
                Err.Number = mpnErrors.mpTOCError_NoTOCFound
                RaiseError("MPTOC90.CTOC.DoStepIndents")
                Exit Sub
            End If

            '---cycle through TOC paragraphs
            For Each paraTOC In Me.TOCRange.Paragraphs
                With paraTOC.Range
                    '           get level
                    iLevel = Right(.Style, 1)

                    '           step indent - adjust hanging indent of style based on
                    '           first representative entry for this level

                    '           determine if it's a numbered entry and
                    '           whether level has already been checked
                    If lCountChrs(.Text, vbTab) > 1 And _
                            xTabFlag(iLevel - 1) = "" Then
                        '               set flag
                        xTabFlag(iLevel - 1) = "X"
                        '               get start position of text after tab
                        .StartOf()
                        .MoveStartUntil(vbTab)
                        .Move(WdUnits.wdCharacter)
                        .Select()
                        sHPos = CurWordApp.Selection.Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)
                        '               if past left indent, needs adjusting
                        If sHPos > .ParagraphFormat.LeftIndent Then
                            .Move(WdUnits.wdCharacter, -1)
                            .Select()
                            '                   locate the .25" sector of document that contains end of number;
                            '                   tab s/b .5" from start of that sector
                            sHPos = CurWordApp.Selection.Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)
                            xStartRange = LTrim(Partition(sHPos, 0, 612, 18))
                            xStartRange = Left(xStartRange, InStr(xStartRange, ":") - 1)
                            '                   redefine style
                            With CurWordApp.ActiveDocument.Styles(.Style).ParagraphFormat
                                .LeftIndent = CSng(xStartRange) + 36
                                .FirstLineIndent = (36 * (iLevel - 1)) - .LeftIndent
                            End With
                        End If
                        .Expand(WdUnits.wdParagraph)
                    End If
                End With
            Next paraTOC
        End Sub
        '**********************************************************
        'EVENT PROCS
        '**********************************************************
        '**********************************************************
        'INTERNAL PROCS
        '**********************************************************
        Private Function rngGetTOC() As Word.Range
            'returns location of existing TOC -

            With CurWordApp.ActiveDocument
                '       first check for existing bookmark, then for
                '       placeholder, then for existing TOC -
                If .Bookmarks.Exists("mpTableOfContents") Then
                    rngGetTOC = .Bookmarks("mpTableOfContents").Range
                ElseIf .Bookmarks.Exists("TableOfContents") Then
                    rngGetTOC = .Bookmarks("TableOfContents").Range
                ElseIf .Bookmarks.Exists("PTOC") Then
                    rngGetTOC = .Bookmarks("PTOC").Range
                ElseIf .Bookmarks.Exists("TOC") Then
                    rngGetTOC = .Bookmarks("TOC").Range
                ElseIf .TablesOfContents.Count Then
                    '           check for existing TOC
                    rngGetTOC = .TablesOfContents(1).Range
                Else
                    rngGetTOC = Nothing
                End If
            End With
        End Function

        Function ShowEditTOCDialog() As Boolean
            'returns true if not cancelled or erred
            Dim oForm As System.Windows.Forms.Form
            Dim oEditForm As frmEditTOC
            Dim oEditFormFrench As frmEditTOCFrench

            ShowEditTOCDialog = False

            CurWordApp.ScreenUpdating = False

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                oForm = New frmEditTOCFrench()
                oEditFormFrench = CType(oForm, frmEditTOCFrench)
                oEditFormFrench.EditCustomScheme = False
            Else
                oForm = New frmEditTOC()
                oEditForm = CType(oForm, frmEditTOC)
                oEditForm.EditCustomScheme = False
            End If

            With oForm
                .ShowDialog()

                If .DialogResult <> System.Windows.Forms.DialogResult.Cancel Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        If Len(oEditFormFrench.cmbAllCaps) Then
                            Me.AllCaps = oEditFormFrench.cmbAllCaps.SelectedIndex
                        Else
                            Me.AllCaps = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditFormFrench.cmbDotLeaders) Then
                            Me.DotLeaders = oEditFormFrench.cmbDotLeaders.SelectedIndex
                        Else
                            Me.DotLeaders = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditFormFrench.cmbLineSpacing) Then
                            Me.LineSpacing = oEditFormFrench.cmbLineSpacing.SelectedIndex
                        Else
                            Me.LineSpacing = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditFormFrench.cmbPageNums) Then
                            Me.PageNumbers = oEditFormFrench.cmbPageNums.SelectedIndex
                        Else
                            Me.PageNumbers = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        Me.SpaceBeforeLevel = oEditFormFrench.spnBefore.Value
                        Me.SpaceBetweenLevel = oEditFormFrench.spnBetween.Value
                        Me.CenterLevel1 = (oEditFormFrench.chkCenterLevel1.CheckState = 1)
                    Else
                        If Len(oEditForm.cmbAllCaps) Then
                            Me.AllCaps = oEditForm.cmbAllCaps.SelectedIndex
                        Else
                            Me.AllCaps = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditForm.cmbDotLeaders) Then
                            Me.DotLeaders = oEditForm.cmbDotLeaders.SelectedIndex
                        Else
                            Me.DotLeaders = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditForm.cmbLineSpacing) Then
                            Me.LineSpacing = oEditForm.cmbLineSpacing.SelectedIndex
                        Else
                            Me.LineSpacing = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        If Len(oEditForm.cmbPageNums) Then
                            Me.PageNumbers = oEditForm.cmbPageNums.SelectedIndex
                        Else
                            Me.PageNumbers = mpTOCLevels.mpTOCLevels_UseCurrent
                        End If

                        Me.SpaceBeforeLevel = oEditForm.spnBefore.Value
                        Me.SpaceBetweenLevel = oEditForm.spnBetween.Value
                        Me.CenterLevel1 = (oEditForm.chkCenterLevel1.CheckState = 1)
                    End If

                    ShowEditTOCDialog = True
                End If
            End With
            oForm.Close()
        End Function

        Sub Modify()
            'displays TOC customization form and
            'modifies the active document TOC scheme.
            Dim i As Integer
            Dim xStyle As String = ""
            Dim styTOC As Word.Style

            If ShowEditTOCDialog() Then
                '       modify TOC styles and doc props
                For i = 1 To 9
                    xStyle = xTranslateTOCStyle(i)
                    styTOC = CurWordApp.ActiveDocument.Styles(xStyle)

                    '           paragraph
                    With styTOC.ParagraphFormat
                        If Me.LineSpacing <> mpLineSpacing.mpLineSpacing_UseCurrent Then _
                            .LineSpacingRule = Me.LineSpacing
                        If Me.SpaceBetweenLevel <> mpTOCUseCurrent Then _
                            .SpaceAfter = Me.SpaceBetweenLevel

                        Select Case Me.DotLeaders
                            Case mpTOCLevels.mpTOCLevels_All
                                .TabStops(1).Leader = 1
                            Case mpTOCLevels.mpTOCLevels_1
                                .TabStops(1).Leader = System.Math.Abs(CInt(i = 1))
                            Case mpTOCLevels.mpTOCLevels_Subsequent
                                .TabStops(1).Leader = System.Math.Abs(CInt(i > 1))
                            Case mpTOCLevels.mpTOCLevels_None
                                .TabStops(1).Leader = 0
                        End Select

                        If Me.CenterLevel1 And (i = 1) Then
                            .Alignment = WdParagraphAlignment.wdAlignParagraphCenter
                        End If
                    End With

                    '           font
                    Select Case Me.AllCaps
                        Case mpTOCLevels.mpTOCLevels_All
                            styTOC.Font.AllCaps = True
                        Case mpTOCLevels.mpTOCLevels_1
                            styTOC.Font.AllCaps = (i = 1)
                        Case mpTOCLevels.mpTOCLevels_Subsequent
                            styTOC.Font.AllCaps = (i > 1)
                        Case mpTOCLevels.mpTOCLevels_None
                            styTOC.Font.AllCaps = False
                    End Select
                Next i

                Dim paraP As Word.Paragraph
                Dim rngP As Word.Range
                For Each paraP In Me.TOCRange.Paragraphs
                    rngP = paraP.Range
                    Me.DoCentering(rngP)
                    Me.DoPageNumbers(rngP)
                    Me.DoSpaceBeforeLevel(rngP)
                Next paraP

                If Me.UseStepIndents Then
                    Me.DoStepIndents()
                End If

            End If
            CurWordApp.ScreenUpdating = True
        End Sub


        Sub DoPageNumbers(rngScope As Word.Range)
            Dim iLevel As Integer
            Dim bIsCentered As Boolean
            Dim paraTOC As Word.Paragraph
            Dim bOmitPageNumber As Boolean

            '---cycle through TOC paragraphs
            For Each paraTOC In rngScope.Paragraphs
                With paraTOC.Range
                    '           get level
                    iLevel = Right(.Style, 1)

                    '           get alignment
                    bIsCentered = (.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter)

                    '           delete/format page numbers
                    Select Case Me.PageNumbers
                        Case mpTOCLevels.mpTOCLevels_All
                            bOmitPageNumber = False
                        Case mpTOCLevels.mpTOCLevels_1
                            bOmitPageNumber = (iLevel > 1)
                        Case mpTOCLevels.mpTOCLevels_Subsequent
                            bOmitPageNumber = (iLevel = 1)
                        Case mpTOCLevels.mpTOCLevels_None
                            bOmitPageNumber = True
                    End Select

                    If bOmitPageNumber Then
                        '               remove page # and tab
                        .EndOf()
                        .Move(WdUnits.wdCharacter, -1)
                        .MoveStartUntil(vbTab, WdConstants.wdBackward)
                        .MoveStart(WdUnits.wdCharacter, -1)
                        .Delete()
                    Else
                        '               if centered with page no, replace last tab with 2 spaces
                        If bIsCentered Then
                            .EndOf()
                            .MoveUntil(vbTab, WdConstants.wdBackward)
                            .MoveStart(WdUnits.wdCharacter, -1)
                            .Text = Space(2)
                        Else
                            '                   if any EOL's are not at the right tab position,
                            '                   then add a tab using tab set as determination
                            With .Characters.Last
                                .Select()
                                If (Int(CurWordApp.Selection.Information(WdInformation.wdHorizontalPositionRelativeToPage)) + 1 < _
                                    Int(CurWordApp.Selection.ParagraphFormat.TabStops(1).Position)) And _
                                    CurWordApp.Selection.Information(WdInformation.wdHorizontalPositionRelativeToPage) > -1 Then
                                    .MoveEndUntil(vbTab, WdConstants.wdBackward)
                                    .InsertAfter(vbTab)
                                End If
                            End With
                        End If
                    End If
                    .Expand(WdUnits.wdParagraph)
                End With
            Next paraTOC
        End Sub

        Sub DoCentering(rngScope As Word.Range)
            Dim iLevel As Integer
            Dim bIsCentered As Boolean
            Dim xTrailingChar As String
            Dim paraTOC As Word.Paragraph

            '---cycle through paragraphs
            For Each paraTOC In rngScope.Paragraphs
                With paraTOC.Range
                    '           get level
                    iLevel = Right(.Style, 1)

                    '           get alignment
                    bIsCentered = .ParagraphFormat.Alignment

                    If iLevel = 1 And bIsCentered Then
                        '               base # of shift-returns on line spacing
                        With .ParagraphFormat
                            If .LineSpacingRule = WdLineSpacing.wdLineSpaceDouble Or _
                                    .LineSpacing > 14 Then
                                xTrailingChar = Chr(11)
                            Else
                                xTrailingChar = New String(Chr(11), 2)
                            End If
                        End With
                        '               replace remaining tab with shift-return(s)
                        .MoveEnd(WdUnits.wdCharacter, -1)
                        .Text = xSubstitute(.Text, vbTab, xTrailingChar)
                    End If
                End With
            Next paraTOC
        End Sub

        Sub DoSpaceBeforeLevel(rngScope As Word.Range)
            Dim iLevel As Integer
            Dim paraTOC As Word.Paragraph
            Dim bAtStartOfTOC As Boolean
            Dim bNewGroup As Boolean

            If Me.SpaceBeforeLevel <> mpTOCUseCurrent Then
                '       cycle through paragraphs
                For Each paraTOC In rngScope.Paragraphs
                    With paraTOC.Range
                        '               get level
                        iLevel = Right(.Style, 1)

                        '               if first para of new level,
                        '               adjust space after previous paragraph
                        bAtStartOfTOC = (.Start = Me.TOCRange.Start)
                        bNewGroup = (.Style.Namelocal <> .Previous(WdUnits.wdParagraph).Style.Namelocal)

                        If (Not bAtStartOfTOC) And bNewGroup Then
                            .Previous(WdUnits.wdParagraph).ParagraphFormat _
                                .SpaceAfter = Me.SpaceBeforeLevel
                        End If
                    End With
                Next paraTOC
            End If
        End Sub
    End Class
End Namespace


