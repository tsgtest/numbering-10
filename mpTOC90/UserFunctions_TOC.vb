Option Explicit On

Imports LMP.Numbering.TOC.mpVariables
Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.TOC.mpTypes
Imports LMP.Numbering.TOC.mpDocument
Imports LMP.Numbering.TOC.mpTOC
Imports LMP.Numbering.TOC.mpApplication

Namespace LMP.Numbering.TOC
    Public Class UserFunctions_TOC
        Public Shared Function zzmpConvertHiddenParas() As Long
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                MsgBox("Cette macro n'est pas disponible.", _
                    vbInformation, "Numérotation TSG")
            Else
                MsgBox("This macro is still under construction.", _
                    vbInformation, "TSG Numbering")
            End If
            '    If bReadyToGo(True, True, True) Then
            '        zzmpConvertHiddenParas = mpTOC.ConvertHiddenParas()
            '    End If
        End Function

        Public Shared Function zzmpFillTCCodes() As Long
            If bReadyToGo(True, True, True) Then
                zzmpFillTCCodes = mpTOC.FillTCCodes()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpFillTCCodesFormatted() As Long
            If bReadyToGo(True, True, True) Then
                zzmpFillTCCodesFormatted = mpTOC.FillTCCodes(True)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpRestoreTCCodes() As Long
            If bReadyToGo(True, True, True) Then
                zzmpRestoreTCCodes = mpTOC.RestoreMPTCCodes()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function zzmpModifyTOC()
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, True, True) Then
                    'force accelerator cues in Word 2013
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    Dim oTOC As cTOCProps
                    oTOC = New cTOCProps
                    oTOC.Modify()
                    oTOC = Nothing
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                End If

            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpEditCustomTOCScheme()
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, True, True) Then
                    'force accelerator cues in Word 2013
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    mpTOC.EditCustomTOCScheme()
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()

                End If

            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpChangeTOCTabs() As Long
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, True, True) Then
                    'force accelerator cues in Word 2013
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    zzmpChangeTOCTabs = mpTOC.bUserChangeTOCTabs()
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                End If

            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function
        Public Shared Function zzmpMarkHeading() As Long
            If bReadyToGo(True, True, True) Then
                CurWordApp.ScreenUpdating = False
                If g_bMarkWithStyleSeparators Then
                    zzmpMarkHeading = mpTOC.InsertStyleSeparator(False)
                Else
                    zzmpMarkHeading = mpTOC.bMarkForTOC(False)
                End If
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpMarkAndFormatHeading() As Long
            If bReadyToGo(True, True, True) Then
                CurWordApp.ScreenUpdating = False
                If g_bMarkWithStyleSeparators Then
                    zzmpMarkAndFormatHeading = mpTOC.InsertStyleSeparator(True)
                Else
                    zzmpMarkAndFormatHeading = mpTOC.bMarkForTOC(True)
                End If
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpTableOfContentsInsert() As Long
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, True, True) Then
                    'force accelerator cues in Word 2013
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    '       disable mp10 event handling
                    If g_bIsMP10 Then _
                        CurWordApp.Run("zzmpSuppressXMLEventHandling")

                    '       get doc/application environment
                    envMpDoc = envGetDocEnvironment()
                    envMpApp = envGetAppEnvironment()

                    '9.9.5001 - new field-only dialog
                    If g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field Then
                        lRet = bInsertTOCAsField()
                    Else
                        lRet = bInsertTOC()
                    End If

                    '       reset doc/application environment
                    '       envMpApp/envMpDoc below are global vars
                    bSetDocEnvironment(envMpDoc)
                    bSetAppEnvironment(envMpApp, Not g_bPreserveUndoListTOC)

                    If lRet = 0 Then _
                        CurWordApp.StatusBar = MsgFinished
                    zzmpTableOfContentsInsert = lRet
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                End If
            Finally
                'restore mp10 event handling
                If g_bIsMP10 Then _
                    CurWordApp.Run("zzmpResumeXMLEventHandling")

                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpUnmarkForTOC() As Long
            Dim bTCCodes As Boolean
            Dim bStyleSeps As Boolean

            If bReadyToGo(True, True, True) Then
                CurWordApp.ScreenUpdating = False
                bStyleSeps = bRemoveStyleSeparators(CurWordApp.Selection.Range)
                bTCCodes = bMarkForTOCRemove(CurWordApp.Selection.Range)
                If Not bTCCodes And Not bStyleSeps Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        If g_bMarkWithStyleSeparators Then
                            MsgBox("Il n'y a pas de séparateurs de style dans la sélection.",
                                vbExclamation, "Numérotation TSG")
                        Else
                            MsgBox("Il n'y a pas d'entrées TM dans la sélection.",
                                vbExclamation, "Numérotation TSG")
                        End If
                    Else
                        If g_bMarkWithStyleSeparators Then
                            MsgBox("There are no style separators in the current selection.",
                                vbExclamation, AppName)
                        Else
                            MsgBox("There are no TC entries in the current selection.",
                                vbExclamation, AppName)
                        End If
                    End If
                End If
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.ScreenUpdating = True
            End If
        End Function

        Public Shared Function zzmpMarkAll() As Long
            Dim bCancel As Boolean
            Dim lKeyboardCues As Long
            Dim bRestoreNoCues As Boolean

            Try
                If bReadyToGo(True, True, True) Then
                    'force accelerator cues in Word 2013
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
                        If lKeyboardCues = 0 Then
                            SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 1, 2)
                            bRestoreNoCues = True
                        End If
                    End If

                    '       get doc/application environment
                    envMpDoc = envGetDocEnvironment()
                    envMpApp = envGetAppEnvironment()

                    bCancel = mpTOC.bMarkAll

                    '       reset doc/application environment
                    '       envMpApp/envMpDoc below are global vars
                    bSetAppEnvironment(envMpApp)
                    If Not bCancel Then _
                        bSetDocEnvironment(envMpDoc)

                    zzmpMarkAll = CLng(bCancel)
                    If Not g_bPreserveUndoList Then _
                        CurWordApp.ActiveDocument.UndoClear()
                End If

            Finally
                'turn off accelerator cues if necessary
                If bRestoreNoCues Then _
                    SetSystemParametersInfo(SPI_SETKEYBOARDCUES, 0, 0, 2)
            End Try
        End Function

        Public Shared Function zzmpIncrTOCTabs() As Long
            If bReadyToGo(True, True, True) Then
                zzmpIncrTOCTabs = IncrementTOCTabs()
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.WordBasic.WaitCursor(False)
            End If
        End Function

        Public Shared Function zzmpDecrTOCTabs() As Long
            If bReadyToGo(True, True, True) Then
                zzmpDecrTOCTabs = IncrementTOCTabs(True)
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
                CurWordApp.WordBasic.WaitCursor(False)
            End If
        End Function

        Public Shared Function zzmpDeleteMPTOC() As Long
            If bReadyToGo(True, True, True) Then
                zzmpDeleteMPTOC = mpTOC.bDeleteMPTOC
                If Not g_bPreserveUndoList Then _
                    CurWordApp.ActiveDocument.UndoClear()
            End If
        End Function

        Public Shared Function EnsureLatestNumbering() As Long
            On Error Resume Next
            If Not bIsConverted() Then
                CurWordApp.Run("zzmpDoConversions")
            End If
        End Function

        Private Shared Function bReadyToGo(ByVal bOnlyIfActiveDocument As Boolean, _
                                    ByVal bOnlyIfDocUnprotected As Boolean, _
                                    ByVal bDoConversions As Boolean) As Boolean
            'returns TRUE iff Word environment
            'is ready to run TOC functions

            '9.9.3 - attempt to get ui language immediately -
            'this will allow us to show pre-initialization messages in the correct language
            If g_lUILanguage = 0 Then
                g_lUILanguage = GetLanguageFromIni()
            End If

            If bOnlyIfActiveDocument Then
                If CurWordApp.Documents.Count = 0 Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Veuillez ouvrir un document avant d'utiliser cette fonction.", _
                            vbInformation, "Numérotation TSG")
                    Else
                        MsgBox("Please open a document before running this function.", _
                            vbInformation, "TSG Numbering")
                    End If
                    Exit Function
                End If
            End If

            If bOnlyIfDocUnprotected Then
                '       false if doc is protected
                If bDocProtected(CurWordApp.ActiveDocument, True) Then _
                    Exit Function
            End If

            '   reinitialize if necessary
            If xBP = "" Then
                bAppInitialize()
            End If

            '   6/30/09 - check for doc var corruption caused by KB969604
            DeleteCorruptedVariables()

            '   run conversions
            If bDoConversions Then
                EnsureLatestNumbering()
            End If

            bReadyToGo = True
        End Function

        Public Shared Function zzmpGoToMPTOC() As Long
            If bReadyToGo(True, True, False) Then
                zzmpGoToMPTOC = mpTOC.bGoToMPTOC
            End If
        End Function

        Public Shared Function zzmpSwitchMarkingMode()
            Dim xMode As String
            If bReadyToGo(True, True, False) Then
                g_bMarkWithStyleSeparators = Not g_bMarkWithStyleSeparators
                SetUserSetting("TOC", "MarkWithStyleSeparators", _
                    CStr(g_bMarkWithStyleSeparators))
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If g_bMarkWithStyleSeparators Then
                        xMode = "séparateurs de style"
                    Else
                        xMode = "codes TM"
                    End If
                    MsgBox("Les titres seront marqués avec des " & xMode & ".", _
                        vbInformation, "Numérotation TSG")
                Else
                    If g_bMarkWithStyleSeparators Then
                        xMode = "style separators"
                    Else
                        xMode = "TC codes"
                    End If
                    MsgBox("Headings will now be marked with " & xMode & ".", _
                        vbInformation, "TSG Numbering")
                End If
            End If
        End Function

        Public Shared Function zzmpTCCodesToStyleSeparators()
            If bReadyToGo(True, True, True) Then
                mpTOC.ConvertToStyleSeparators()
            End If
        End Function

        Public Shared Function zzmpStyleSeparatorsToTCCodes()
            If bReadyToGo(True, True, True) Then
                mpTOC.ConvertToTCCodes()
            End If
        End Function

        Public Shared Function GetMarkMenuXML() As String
            If bReadyToGo(False, False, False) Then
                GetMarkMenuXML = mpApplication.GetMarkMenuXML
            End If
        End Function

        Public Shared Function zzmpSwitchTOCMode()
            Dim xMode As String
            If bReadyToGo(True, True, False) Then
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field Then
                        g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Text
                        xMode = "texte"
                    Else
                        g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field
                        xMode = "codes de champ"
                    End If
                    SetUserSetting("TOC", "InsertAsFieldDefault", _
                        CStr(g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field))
                    MsgBox("Les TMs seront dorénavant insérées comme " & xMode & ".", _
                        vbInformation, "Numérotation TSG")
                Else
                    If g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field Then
                        g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Text
                        xMode = "text"
                    Else
                        g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field
                        xMode = "field codes"
                    End If
                    SetUserSetting("TOC", "InsertAsFieldDefault", _
                        CStr(g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field))
                    MsgBox("TOCs will now be inserted as " & xMode & ".", _
                        vbInformation, "TSG Numbering")
                End If
            End If
        End Function

        Public Shared Function GetTOCMenuXML() As String
            If bReadyToGo(False, False, False) Then
                GetTOCMenuXML = mpApplication.GetTOCMenuXML
            End If
        End Function

        Public Shared Function AllowTOCAsField() As Boolean
            If bReadyToGo(False, False, False) Then
                AllowTOCAsField = g_bAllowTOCAsField
            End If
        End Function

        Public Shared Function InsertTOCsAsField() As Boolean
            If bReadyToGo(False, False, False) Then
                InsertTOCsAsField = (g_iTOCDialogStyle = mpTOCDialogStyles.mpTOCDialogStyle_Field)
            End If
        End Function

        Public Shared Function zzmpReinitialize() As Long
            'added in 9.9.6006 for GLOG 3940
            zzmpReinitialize = CLng(bAppInitialize())
        End Function
    End Class
End Namespace
