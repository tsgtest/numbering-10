<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmChangeTOCTabsFrench
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cbxTabLevels As mpnControls.ComboBox
    Public WithEvents lblTabLevels As System.Windows.Forms.Label
    Public WithEvents lblTabs As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChangeTOCTabsFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblTabLevels = New System.Windows.Forms.Label()
        Me.lblTabs = New System.Windows.Forms.Label()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnReset = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.spnTabs = New SpinTextInternational()
        Me.cbxTabLevels = New mpnControls.ComboBox()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTabLevels
        '
        Me.lblTabLevels.BackColor = System.Drawing.SystemColors.Control
        Me.lblTabLevels.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTabLevels.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTabLevels.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTabLevels.Location = New System.Drawing.Point(20, 64)
        Me.lblTabLevels.Name = "lblTabLevels"
        Me.lblTabLevels.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTabLevels.Size = New System.Drawing.Size(77, 22)
        Me.lblTabLevels.TabIndex = 2
        Me.lblTabLevels.Text = "&Appliquer � :"
        '
        'lblTabs
        '
        Me.lblTabs.BackColor = System.Drawing.SystemColors.Control
        Me.lblTabs.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTabs.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTabs.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTabs.Location = New System.Drawing.Point(20, 17)
        Me.lblTabs.Name = "lblTabs"
        Me.lblTabs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTabs.Size = New System.Drawing.Size(157, 19)
        Me.lblTabs.TabIndex = 0
        Me.lblTabs.Text = "Augmenter &tabulations par:"
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnReset, Me.toolStripSeparator3, Me.btnCancel, Me.toolStripSeparator1, Me.btnOK})
        Me.tsButtons.Location = New System.Drawing.Point(0, 125)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.Size = New System.Drawing.Size(252, 48)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 24
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnReset
        '
        Me.btnReset.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnReset.AutoSize = False
        Me.btnReset.Image = Global.My.Resources.Resources.Refresh
        Me.btnReset.ImageTransparentColor = System.Drawing.Color.White
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(60, 45)
        Me.btnReset.Text = "&R�tablir"
        Me.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 48)
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Annuler"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 48)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'spnTabs
        '
        Me.spnTabs.AllowFractions = True
        Me.spnTabs.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnTabs.AppendSymbol = True
        Me.spnTabs.AutoSize = True
        Me.spnTabs.DisplayText = "0"""
        Me.spnTabs.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Inches
        Me.spnTabs.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnTabs.IncrementValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spnTabs.Location = New System.Drawing.Point(21, 33)
        Me.spnTabs.MaxValue = New Decimal(New Integer() {288, 0, 0, 0})
        Me.spnTabs.MinValue = New Decimal(New Integer() {288, 0, 0, -2147483648})
        Me.spnTabs.Name = "spnTabs"
        Me.spnTabs.SelLength = 0
        Me.spnTabs.SelStart = 0
        Me.spnTabs.SelText = ""
        Me.spnTabs.Size = New System.Drawing.Size(145, 20)
        Me.spnTabs.TabIndex = 1
        Me.spnTabs.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnTabs.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'cbxTabLevels
        '
        Me.cbxTabLevels.AllowEmptyValue = False
        Me.cbxTabLevels.AutoSize = True
        Me.cbxTabLevels.Borderless = False
        Me.cbxTabLevels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxTabLevels.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTabLevels.IsDirty = False
        Me.cbxTabLevels.LimitToList = True
        Me.cbxTabLevels.Location = New System.Drawing.Point(21, 80)
        Me.cbxTabLevels.MaxDropDownItems = 8
        Me.cbxTabLevels.Name = "cbxTabLevels"
        Me.cbxTabLevels.SelectedIndex = -1
        Me.cbxTabLevels.SelectedValue = Nothing
        Me.cbxTabLevels.SelectionLength = 0
        Me.cbxTabLevels.SelectionStart = 0
        Me.cbxTabLevels.Size = New System.Drawing.Size(183, 23)
        Me.cbxTabLevels.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxTabLevels.SupportingValues = ""
        Me.cbxTabLevels.TabIndex = 3
        Me.cbxTabLevels.Tag2 = Nothing
        Me.cbxTabLevels.Value = ""
        '
        'frmChangeTOCTabsFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(252, 173)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.spnTabs)
        Me.Controls.Add(Me.cbxTabLevels)
        Me.Controls.Add(Me.lblTabLevels)
        Me.Controls.Add(Me.lblTabs)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangeTOCTabsFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Augmenter/r�duire tabulations TM "
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents spnTabs As SpinTextInternational
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnReset As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
#End Region
End Class