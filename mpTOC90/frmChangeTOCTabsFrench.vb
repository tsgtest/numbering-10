Option Strict Off
Option Explicit On
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.TOC.mpTOC
Imports LMP.Numbering.Base.cConstants
Imports [Word] = Microsoft.Office.Interop.Word
Imports LMP
Imports System.Windows.Forms

Friend Class frmChangeTOCTabsFrench
    Inherits System.Windows.Forms.Form

    Public m_bFinished As Boolean
    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Dim dValue As Decimal
        Dim iIndex As Integer

        Try
            If Not spnTabs.Validate() Then
                spnTabs.Focus()
                Return
            End If

            dValue = Me.spnTabs.Value
            iIndex = Me.cbxTabLevels.SelectedIndex

            Me.Close()
            SetTOCTabs(dValue, iIndex)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnReset.Click
        Dim rngTOC As Word.Range
        Dim rngStart As Word.Range

        Try
            Me.Close()
            System.Windows.Forms.Application.DoEvents()
            With CurWordApp.ActiveDocument
                If .Bookmarks.Exists("mpTableOfContents") Then
                    rngTOC = .Bookmarks.Item("mpTableOfContents").Range
                Else
                    rngTOC = .Content
                    With rngTOC.Find
                        .ClearFormatting()
                        .Text = ""
                        .Style = Microsoft.Office.Interop.Word.WdBuiltinStyle.wdStyleTOC1
                        .Execute()
                        If Not .Found Then
                            MsgBox("Aucune table des mati�res n'a �t� trouv� dans ce document.", MsgBoxStyle.Exclamation, AppName)
                            Exit Sub
                        Else
                            rngTOC.MoveEnd(Microsoft.Office.Interop.Word.WdUnits.wdSection)
                            If rngTOC.Characters.Last.Text = Chr(12) Then rngTOC.MoveEnd(Microsoft.Office.Interop.Word.WdUnits.wdCharacter, -1)
                        End If
                    End With
                End If
            End With
            CurWordApp.Application.ScreenUpdating = False

            rngStart = CurWordApp.Selection.Range
            bResetTOCTabs(rngTOC)
            rngStart.Select()
        Catch oE As Exception
            [Error].Show(oE)
        Finally
            CurWordApp.Application.ScreenUpdating = True
            CurWordApp.Application.ScreenRefresh()
        End Try
    End Sub

    Private Sub spnTabs_Validated() Handles spnTabs.Validated
        Try
            With Me
                If .spnTabs.Value < 0 Then
                    .lblTabs.Text = "R�duire &Tabulations par:"
                Else
                    .lblTabs.Text = "Augmenter &Tabulations par:"
                End If
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmChangeTOCTabs_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub frmChangeTOCTabs_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim xApplyTo(9) As String
        Dim i As Short
        Dim xarTabLevels As Array

        Try
            Me.m_bFinished = False

            'adjust for scaling
            Dim sFactor As Single = LMP.Numbering.TOC.mpApplication.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnReset.Width = Me.btnReset.Width * sFactor
                Me.btnOK.Height = Me.btnOK.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
                Me.btnReset.Height = Me.btnReset.Height + (sFactor - 1) * 40
            End If

            '   fill Tab Levels array
            xarTabLevels = New String(9, 1) {}
            xarTabLevels.SetValue("Tous niveaux", 0, 0)
            xarTabLevels.SetValue("0", 0, 1)
            For i = 1 To 9
                xarTabLevels.SetValue("Niveau " & i, i, 0)
                xarTabLevels.SetValue(CStr(i), i, 1)
            Next i

            Me.cbxTabLevels.SetList(xarTabLevels)
            Dim lUnits As Integer

            'initialize to first in list
            Me.cbxTabLevels.SelectedIndex = 0

            lUnits = CurWordApp.Options.MeasurementUnit
            With Me.spnTabs
                .DisplayUnit = lUnits
                .IncrementValue = GetStandardIncrement(lUnits)
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTabs_SpinDown() Handles spnTabs.SpinDown
        Try
            If Me.spnTabs.Value < 0 Then Me.lblTabs.Text = "R�duire &Tabulations par:"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTabs_SpinUp() Handles spnTabs.SpinUp
        Try
            If Me.spnTabs.Value >= 0 Then Me.lblTabs.Text = "Augmenter &Tabulations par:"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub spnTabs_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles spnTabs.Validating
        Dim oSpinner As SpinTextInternational = sender
        Try
            If Not oSpinner.IsValid Then e.Cancel = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class