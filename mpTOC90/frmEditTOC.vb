Option Strict Off
Option Explicit On
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumTOC
Imports LMP
Imports System.Windows.Forms

Friend Class frmEditTOC
    Inherits System.Windows.Forms.Form

    Private m_bCancelled As Boolean
    Private m_bEditCustomScheme As Boolean
    Private m_xarLineSpacing As Array
    Private m_xarAllCaps As Array
    Private m_xarBold As Array
    Private m_xarDotLeaders As Array
    Private m_xarPageNums As Array

    Public ReadOnly Property Cancelled() As Boolean
        Get
            Cancelled = m_bCancelled
        End Get
    End Property

    Property EditCustomScheme() As Boolean
        Get
            EditCustomScheme = m_bEditCustomScheme
        End Get
        Set(ByVal Value As Boolean)
            m_bEditCustomScheme = Value
            '   GLOG : 5445 : ceh
            '   moved to Form_Load so controls get properly initialized
            '   SetupDlg bNew
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmEditTOC_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmEditTOC_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Try
            'adjust for scaling
            Dim sFactor As Single = LMP.Numbering.TOC.mpApplication.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnOK.Height = Me.btnOK.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
            End If

            '   GLOG : 5445 : ceh
            '   moved from EditCustomScheme property let
            SetupDlg(EditCustomScheme)
            '   hide border - left on in design to
            '   avoid confusion
            'UPGRADE_ISSUE: Frame property fraMain.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            'Me.fraMain.BorderStyle = 0
            Dim xValue As String

            If EditCustomScheme Then
                '   initialize values
                '   line spacing
                xValue = GetUserSetting("TOC", "LineSpacing")
                If xValue = "" Or xValue = "-1" Then xValue = "0"

                Me.cmbLineSpacing.SelectedIndex = CInt(xValue) ' SelectedValue = m_xarLineSpacing.GetValue(CInt(xValue), 0)
                '   Me.cmbLineSpacing.Text = m_xarLineSpacing(CLng(xValue), 0)

                '   all caps
                xValue = GetUserSetting("TOC", "AllCaps")
                If xValue = "" Or xValue = "-1" Then xValue = "1" 'GLOG 15801
                '   we removed an item from this list
                If CDbl(xValue) > m_xarAllCaps.GetLength(0) - 1 Then xValue = CStr(CDbl(xValue) - 1)

                Me.cmbAllCaps.SelectedIndex = CInt(xValue)

                '   bold
                xValue = GetUserSetting("TOC", "Bold")
                If xValue = "" Or xValue = "-1" Then xValue = "2"

                Me.cmbBold.SelectedIndex = CInt(xValue)

                '   dot leaders
                xValue = GetUserSetting("TOC", "DotLeaders")
                If xValue = "" Or xValue = "-1" Then xValue = "0"

                Me.cmbDotLeaders.SelectedIndex = CInt(xValue)
                '    Me.cmbDotLeaders.Text = m_xarDotLeaders(CLng(xValue), 0)

                '   page numbers
                xValue = GetUserSetting("TOC", "PageNumbers")
                If xValue = "" Or xValue = "-1" Then xValue = "0"

                Me.cmbPageNums.SelectedIndex = CInt(xValue)
                '    Me.cmbPageNums.Text = m_xarPageNums(CLng(xValue), 0)

                '   points before
                xValue = GetUserSetting("TOC", "PointsBefore")
                If xValue = "" Then
                    xValue = "6" 'GLOG 15801
                Else
                    xValue = xLocalizeNumericString(xValue)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBefore. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnBefore.Value = CDbl(xValue)

                '   points between
                xValue = GetUserSetting("TOC", "PointsBetween")
                If xValue = "" Then
                    xValue = "6" 'GLOG 15801
                Else
                    xValue = xLocalizeNumericString(xValue)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.spnBetween. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.spnBetween.Value = CDbl(xValue)

                '   center level 1
                xValue = GetUserSetting("TOC", "CenterLevel1")
                If xValue = "" Then xValue = "0"
                Me.chkCenterLevel1.CheckState = CShort(xValue)

                '   default to custom
                xValue = GetUserSetting("TOC", "DefaultToCustom")
                If xValue = "" Then xValue = "0"
                Me.chkDefaultScheme.CheckState = CShort(xValue)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub SetupDlg(ByVal bForCustomTOCScheme As Boolean)
        Dim xValue As String = ""
        'Dim ctlP As System.Windows.Forms.Control

        If bForCustomTOCScheme Then
            Me.Text = " Edit Custom TOC Scheme"
            Me.btnOK.Text = "Sa&ve"
            '        On Error Resume Next
            'JTS 10/7/16: Unclear about this code - is this still necessary or was it specific to TDBCombo?
            ''cehtdb
            '         For Each ctlP In Me.Controls
            '             'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            '             If TypeOf ctlP Is AxTrueDBList60.AxTDBCombo Then
            '                 If GetTDBListCount(ctlP) = 5 Then
            '                     RemoveItemFromTDBCombo(ctlP, 4)
            '                 End If
            '             End If
            '         Next ctlP
            'Me.fraMain.Top = lblMessage.Top
            'Me.Height = Me.Height - lblMessage.Height
            ''GLOG : 5591 : ceh
            ''fix dialog height for Word version post 2010
            '         If CShort(CurWordApp.Version) >= 15 Then
            '             Me.Height = TwipsToPixelsY(4575)
            '         Else
            '             Me.Height = TwipsToPixelsY(4500)
            '         End If

            '       load line spacing list
            m_xarLineSpacing = New Object(2, 1) {{"Single", 0},
                                                 {"1.5 Lines", 1},
                                                 {"Double", 2}}

            Me.cmbLineSpacing.SetList(m_xarLineSpacing)

            '       load all caps list
            m_xarAllCaps = New Object(2, 1) {{"All Levels", 0},
                                            {"Level 1", 1},
                                            {"Do Not Cap Any Level", 2}}

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbAllCaps.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbAllCaps.SetList(m_xarAllCaps)

            '       load bold list
            m_xarBold = New Object(2, 1) {{"All Levels", 0},
                                         {"Level 1", 1},
                                         {"Do Not Bold Any Level", 2}}

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbBold.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbBold.SetList(m_xarBold)

            '       load DotLeaders list
            m_xarDotLeaders = New Object(3, 1) {{"All Levels", 0},
                                               {"Level 1", 1},
                                               {"Levels 2-9", 2},
                                               {"No Dot Leaders", 3}}

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbDotLeaders.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbDotLeaders.SetList(m_xarDotLeaders)

            '       load PageNumbers list
            m_xarPageNums = New Object(3, 1) {{"All Levels", 0},
                                             {"Level 1", 1},
                                             {"Levels 2-9", 2},
                                             {"No Page Numbers", 3}}

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cmbPageNums.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cmbPageNums.SetList(m_xarPageNums)
        Else
            Me.Text = "Edit TOC"
            Me.btnOK.Text = "O&K"
            '        On Error Resume Next
            'cehtdb
            'JTS 10/7/16: Unclear about this code - is this still necessary or was it specific to TDBCombo?
            'For Each ctlP In Me.Controls
            '    'UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            '    If TypeOf ctlP Is AxTrueDBList60.AxTDBCombo Then
            '        If GetTDBListCount(ctlP) = 4 Then
            '            AddItemToTDBCombo(ctlP, "Use Current Settings")
            '        End If
            '    End If
            'Next ctlP
            '        Me.fraMain.Top = 165
            'Me.btnOK.Top = TwipsToPixelsY(PixelsToTwipsY(Me.btnOK.Top) - 100)
            'Me.btnCancel.Top = TwipsToPixelsY(PixelsToTwipsY(Me.btnCancel.Top) - 100)
            'Me.Height = TwipsToPixelsY(4650)
        End If
        Me.lblMessage.Visible = Not bForCustomTOCScheme
        Me.chkDefaultScheme.Visible = bForCustomTOCScheme
    End Sub
	
    Private Sub spnBefore_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles spnBefore.Validating, spnBetween.Validating
        Try
            If Not sender.IsValid Then e.Cancel = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub	
End Class