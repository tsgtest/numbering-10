<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEditTOCFrench
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents chkCenterLevel1 As System.Windows.Forms.CheckBox
    Public WithEvents chkDefaultScheme As System.Windows.Forms.CheckBox
    Public WithEvents cmbAllCaps As mpnControls.ComboBox
    Public WithEvents cmbBold As mpnControls.ComboBox
    Public WithEvents cmbPageNums As mpnControls.ComboBox
    Public WithEvents cmbDotLeaders As mpnControls.ComboBox
    Public WithEvents cmbLineSpacing As mpnControls.ComboBox
    Public WithEvents lblBold As System.Windows.Forms.Label
    Public WithEvents lblAllCaps As System.Windows.Forms.Label
    Public WithEvents lblPageNums As System.Windows.Forms.Label
    Public WithEvents lblDotLeaders As System.Windows.Forms.Label
    Public WithEvents lblSpacing As System.Windows.Forms.Label
    Public WithEvents lblBetween As System.Windows.Forms.Label
    Public WithEvents lblBefore As System.Windows.Forms.Label
    Public WithEvents fraMain As System.Windows.Forms.GroupBox
    Public WithEvents lblMessage As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditTOCFrench))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.fraMain = New System.Windows.Forms.GroupBox()
        Me.spnBetween = New SpinTextInternational()
        Me.spnBefore = New SpinTextInternational()
        Me.chkCenterLevel1 = New System.Windows.Forms.CheckBox()
        Me.chkDefaultScheme = New System.Windows.Forms.CheckBox()
        Me.cmbAllCaps = New mpnControls.ComboBox()
        Me.cmbBold = New mpnControls.ComboBox()
        Me.cmbPageNums = New mpnControls.ComboBox()
        Me.cmbDotLeaders = New mpnControls.ComboBox()
        Me.cmbLineSpacing = New mpnControls.ComboBox()
        Me.lblBold = New System.Windows.Forms.Label()
        Me.lblAllCaps = New System.Windows.Forms.Label()
        Me.lblPageNums = New System.Windows.Forms.Label()
        Me.lblDotLeaders = New System.Windows.Forms.Label()
        Me.lblSpacing = New System.Windows.Forms.Label()
        Me.lblBetween = New System.Windows.Forms.Label()
        Me.lblBefore = New System.Windows.Forms.Label()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.fraMain.SuspendLayout()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'fraMain
        '
        Me.fraMain.BackColor = System.Drawing.SystemColors.Control
        Me.fraMain.Controls.Add(Me.spnBetween)
        Me.fraMain.Controls.Add(Me.spnBefore)
        Me.fraMain.Controls.Add(Me.chkCenterLevel1)
        Me.fraMain.Controls.Add(Me.chkDefaultScheme)
        Me.fraMain.Controls.Add(Me.cmbAllCaps)
        Me.fraMain.Controls.Add(Me.cmbBold)
        Me.fraMain.Controls.Add(Me.cmbPageNums)
        Me.fraMain.Controls.Add(Me.cmbDotLeaders)
        Me.fraMain.Controls.Add(Me.cmbLineSpacing)
        Me.fraMain.Controls.Add(Me.lblBold)
        Me.fraMain.Controls.Add(Me.lblAllCaps)
        Me.fraMain.Controls.Add(Me.lblPageNums)
        Me.fraMain.Controls.Add(Me.lblDotLeaders)
        Me.fraMain.Controls.Add(Me.lblSpacing)
        Me.fraMain.Controls.Add(Me.lblBetween)
        Me.fraMain.Controls.Add(Me.lblBefore)
        Me.fraMain.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraMain.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraMain.Location = New System.Drawing.Point(3, 5)
        Me.fraMain.Name = "fraMain"
        Me.fraMain.Padding = New System.Windows.Forms.Padding(0)
        Me.fraMain.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMain.Size = New System.Drawing.Size(459, 315)
        Me.fraMain.TabIndex = 1
        Me.fraMain.TabStop = False
        '
        'spnBetween
        '
        Me.spnBetween.AllowFractions = True
        Me.spnBetween.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnBetween.AppendSymbol = True
        Me.spnBetween.AutoSize = True
        Me.spnBetween.DisplayText = "0 pt"
        Me.spnBetween.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnBetween.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnBetween.IncrementValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spnBetween.Location = New System.Drawing.Point(249, 215)
        Me.spnBetween.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.spnBetween.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnBetween.Name = "spnBetween"
        Me.spnBetween.SelLength = 0
        Me.spnBetween.SelStart = 0
        Me.spnBetween.SelText = ""
        Me.spnBetween.Size = New System.Drawing.Size(81, 20)
        Me.spnBetween.TabIndex = 16
        Me.spnBetween.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnBetween.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'spnBefore
        '
        Me.spnBefore.AllowFractions = True
        Me.spnBefore.Appearance = SpinTextInternational.stbAppearance.Flat
        Me.spnBefore.AppendSymbol = True
        Me.spnBefore.AutoSize = True
        Me.spnBefore.DisplayText = "0 pt"
        Me.spnBefore.DisplayUnit = SpinTextInternational.stbUnits.stbUnit_Points
        Me.spnBefore.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spnBefore.IncrementValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.spnBefore.Location = New System.Drawing.Point(249, 179)
        Me.spnBefore.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.spnBefore.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnBefore.Name = "spnBefore"
        Me.spnBefore.SelLength = 0
        Me.spnBefore.SelStart = 0
        Me.spnBefore.SelText = ""
        Me.spnBefore.Size = New System.Drawing.Size(81, 20)
        Me.spnBefore.TabIndex = 14
        Me.spnBefore.Value = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spnBefore.ValueUnit = SpinTextInternational.stbUnits.stbUnit_Points
        '
        'chkCenterLevel1
        '
        Me.chkCenterLevel1.BackColor = System.Drawing.SystemColors.Control
        Me.chkCenterLevel1.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkCenterLevel1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCenterLevel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkCenterLevel1.Location = New System.Drawing.Point(249, 134)
        Me.chkCenterLevel1.Name = "chkCenterLevel1"
        Me.chkCenterLevel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkCenterLevel1.Size = New System.Drawing.Size(140, 20)
        Me.chkCenterLevel1.TabIndex = 12
        Me.chkCenterLevel1.Text = "Niveau 1 &centr�"
        Me.chkCenterLevel1.UseVisualStyleBackColor = False
        '
        'chkDefaultScheme
        '
        Me.chkDefaultScheme.BackColor = System.Drawing.SystemColors.Control
        Me.chkDefaultScheme.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDefaultScheme.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDefaultScheme.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDefaultScheme.Location = New System.Drawing.Point(18, 260)
        Me.chkDefaultScheme.Name = "chkDefaultScheme"
        Me.chkDefaultScheme.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDefaultScheme.Size = New System.Drawing.Size(199, 20)
        Me.chkDefaultScheme.TabIndex = 15
        Me.chkDefaultScheme.Text = "D�finir th�me &TM par d�faut"
        Me.chkDefaultScheme.UseVisualStyleBackColor = False
        '
        'cmbAllCaps
        '
        Me.cmbAllCaps.AllowEmptyValue = False
        Me.cmbAllCaps.AutoSize = True
        Me.cmbAllCaps.Borderless = False
        Me.cmbAllCaps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbAllCaps.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbAllCaps.IsDirty = False
        Me.cmbAllCaps.LimitToList = True
        Me.cmbAllCaps.Location = New System.Drawing.Point(8, 83)
        Me.cmbAllCaps.MaxDropDownItems = 8
        Me.cmbAllCaps.Name = "cmbAllCaps"
        Me.cmbAllCaps.SelectedIndex = -1
        Me.cmbAllCaps.SelectedValue = Nothing
        Me.cmbAllCaps.SelectionLength = 0
        Me.cmbAllCaps.SelectionStart = 0
        Me.cmbAllCaps.Size = New System.Drawing.Size(224, 23)
        Me.cmbAllCaps.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbAllCaps.SupportingValues = ""
        Me.cmbAllCaps.TabIndex = 7
        Me.cmbAllCaps.Tag2 = Nothing
        Me.cmbAllCaps.Value = ""
        '
        'cmbBold
        '
        Me.cmbBold.AllowEmptyValue = False
        Me.cmbBold.AutoSize = True
        Me.cmbBold.Borderless = False
        Me.cmbBold.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbBold.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBold.IsDirty = False
        Me.cmbBold.LimitToList = True
        Me.cmbBold.Location = New System.Drawing.Point(8, 131)
        Me.cmbBold.MaxDropDownItems = 8
        Me.cmbBold.Name = "cmbBold"
        Me.cmbBold.SelectedIndex = -1
        Me.cmbBold.SelectedValue = Nothing
        Me.cmbBold.SelectionLength = 0
        Me.cmbBold.SelectionStart = 0
        Me.cmbBold.Size = New System.Drawing.Size(224, 23)
        Me.cmbBold.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbBold.SupportingValues = ""
        Me.cmbBold.TabIndex = 11
        Me.cmbBold.Tag2 = Nothing
        Me.cmbBold.Value = ""
        '
        'cmbPageNums
        '
        Me.cmbPageNums.AllowEmptyValue = False
        Me.cmbPageNums.AutoSize = True
        Me.cmbPageNums.Borderless = False
        Me.cmbPageNums.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbPageNums.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPageNums.IsDirty = False
        Me.cmbPageNums.LimitToList = True
        Me.cmbPageNums.Location = New System.Drawing.Point(249, 37)
        Me.cmbPageNums.MaxDropDownItems = 8
        Me.cmbPageNums.Name = "cmbPageNums"
        Me.cmbPageNums.SelectedIndex = -1
        Me.cmbPageNums.SelectedValue = Nothing
        Me.cmbPageNums.SelectionLength = 0
        Me.cmbPageNums.SelectionStart = 0
        Me.cmbPageNums.Size = New System.Drawing.Size(199, 23)
        Me.cmbPageNums.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbPageNums.SupportingValues = ""
        Me.cmbPageNums.TabIndex = 5
        Me.cmbPageNums.Tag2 = Nothing
        Me.cmbPageNums.Value = ""
        '
        'cmbDotLeaders
        '
        Me.cmbDotLeaders.AllowEmptyValue = False
        Me.cmbDotLeaders.AutoSize = True
        Me.cmbDotLeaders.Borderless = False
        Me.cmbDotLeaders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbDotLeaders.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDotLeaders.IsDirty = False
        Me.cmbDotLeaders.LimitToList = True
        Me.cmbDotLeaders.Location = New System.Drawing.Point(249, 83)
        Me.cmbDotLeaders.MaxDropDownItems = 8
        Me.cmbDotLeaders.Name = "cmbDotLeaders"
        Me.cmbDotLeaders.SelectedIndex = -1
        Me.cmbDotLeaders.SelectedValue = Nothing
        Me.cmbDotLeaders.SelectionLength = 0
        Me.cmbDotLeaders.SelectionStart = 0
        Me.cmbDotLeaders.Size = New System.Drawing.Size(199, 23)
        Me.cmbDotLeaders.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbDotLeaders.SupportingValues = ""
        Me.cmbDotLeaders.TabIndex = 9
        Me.cmbDotLeaders.Tag2 = Nothing
        Me.cmbDotLeaders.Value = ""
        '
        'cmbLineSpacing
        '
        Me.cmbLineSpacing.AllowEmptyValue = False
        Me.cmbLineSpacing.AutoSize = True
        Me.cmbLineSpacing.Borderless = False
        Me.cmbLineSpacing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbLineSpacing.Font = New System.Drawing.Font("Arial Unicode MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbLineSpacing.IsDirty = False
        Me.cmbLineSpacing.LimitToList = True
        Me.cmbLineSpacing.Location = New System.Drawing.Point(8, 36)
        Me.cmbLineSpacing.MaxDropDownItems = 8
        Me.cmbLineSpacing.Name = "cmbLineSpacing"
        Me.cmbLineSpacing.SelectedIndex = -1
        Me.cmbLineSpacing.SelectedValue = Nothing
        Me.cmbLineSpacing.SelectionLength = 0
        Me.cmbLineSpacing.SelectionStart = 0
        Me.cmbLineSpacing.Size = New System.Drawing.Size(224, 23)
        Me.cmbLineSpacing.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbLineSpacing.SupportingValues = ""
        Me.cmbLineSpacing.TabIndex = 3
        Me.cmbLineSpacing.Tag2 = Nothing
        Me.cmbLineSpacing.Value = ""
        '
        'lblBold
        '
        Me.lblBold.BackColor = System.Drawing.SystemColors.Control
        Me.lblBold.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBold.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBold.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBold.Location = New System.Drawing.Point(9, 116)
        Me.lblBold.Name = "lblBold"
        Me.lblBold.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBold.Size = New System.Drawing.Size(102, 18)
        Me.lblBold.TabIndex = 10
        Me.lblBold.Text = "&Gras:"
        '
        'lblAllCaps
        '
        Me.lblAllCaps.BackColor = System.Drawing.SystemColors.Control
        Me.lblAllCaps.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAllCaps.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllCaps.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAllCaps.Location = New System.Drawing.Point(9, 68)
        Me.lblAllCaps.Name = "lblAllCaps"
        Me.lblAllCaps.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAllCaps.Size = New System.Drawing.Size(102, 16)
        Me.lblAllCaps.TabIndex = 6
        Me.lblAllCaps.Text = "&Majuscules:"
        '
        'lblPageNums
        '
        Me.lblPageNums.BackColor = System.Drawing.SystemColors.Control
        Me.lblPageNums.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPageNums.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPageNums.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPageNums.Location = New System.Drawing.Point(250, 22)
        Me.lblPageNums.Name = "lblPageNums"
        Me.lblPageNums.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPageNums.Size = New System.Drawing.Size(157, 19)
        Me.lblPageNums.TabIndex = 4
        Me.lblPageNums.Text = "Inclure &num�ros de page:"
        '
        'lblDotLeaders
        '
        Me.lblDotLeaders.BackColor = System.Drawing.SystemColors.Control
        Me.lblDotLeaders.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDotLeaders.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDotLeaders.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDotLeaders.Location = New System.Drawing.Point(250, 69)
        Me.lblDotLeaders.Name = "lblDotLeaders"
        Me.lblDotLeaders.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDotLeaders.Size = New System.Drawing.Size(175, 24)
        Me.lblDotLeaders.TabIndex = 8
        Me.lblDotLeaders.Text = "Utiliser &points de suite:"
        '
        'lblSpacing
        '
        Me.lblSpacing.BackColor = System.Drawing.SystemColors.Control
        Me.lblSpacing.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSpacing.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpacing.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSpacing.Location = New System.Drawing.Point(9, 20)
        Me.lblSpacing.Name = "lblSpacing"
        Me.lblSpacing.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSpacing.Size = New System.Drawing.Size(152, 24)
        Me.lblSpacing.TabIndex = 2
        Me.lblSpacing.Text = "Espacement de &ligne:"
        '
        'lblBetween
        '
        Me.lblBetween.BackColor = System.Drawing.SystemColors.Control
        Me.lblBetween.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBetween.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBetween.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBetween.Location = New System.Drawing.Point(15, 219)
        Me.lblBetween.Name = "lblBetween"
        Me.lblBetween.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBetween.Size = New System.Drawing.Size(240, 19)
        Me.lblBetween.TabIndex = 15
        Me.lblBetween.Text = "Espacement &entre �l�ments de m�me niveau:"
        '
        'lblBefore
        '
        Me.lblBefore.BackColor = System.Drawing.SystemColors.Control
        Me.lblBefore.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBefore.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBefore.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBefore.Location = New System.Drawing.Point(15, 184)
        Me.lblBefore.Name = "lblBefore"
        Me.lblBefore.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBefore.Size = New System.Drawing.Size(197, 20)
        Me.lblBefore.TabIndex = 13
        Me.lblBefore.Text = "&Espacement avant nouveau niveau:"
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.SystemColors.Control
        Me.lblMessage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMessage.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMessage.Location = New System.Drawing.Point(8, 10)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMessage.Size = New System.Drawing.Size(438, 35)
        Me.lblMessage.TabIndex = 0
        Me.lblMessage.Text = "Specify values only to those properties that you wish to modify.  Items that do n" & _
    "ot have any values specified will appear unchanged in the TOC."
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 324)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.Size = New System.Drawing.Size(466, 48)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 23
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Annuler"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 48)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 48)
        '
        'frmEditTOCFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(466, 372)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.fraMain)
        Me.Controls.Add(Me.lblMessage)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditTOCFrench"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " �diter le th�me personnalis� TM"
        Me.fraMain.ResumeLayout(False)
        Me.fraMain.PerformLayout()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents spnBefore As SpinTextInternational
    Friend WithEvents spnBetween As SpinTextInternational
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region
End Class