<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmHeaderFooter
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdSetType As System.Windows.Forms.Button
    Public WithEvents cbxType As mpnControls.ComboBox
	Public WithEvents lblType As System.Windows.Forms.Label
	Public WithEvents pnlLanguage As System.Windows.Forms.GroupBox
    Public WithEvents chkContinue As System.Windows.Forms.CheckBox
	Public WithEvents cmdSetPage As System.Windows.Forms.Button
    Public WithEvents cmbNumberStyle As mpnControls.ComboBox
    Public WithEvents cmbPunctuation As mpnControls.ComboBox
	Public WithEvents lblPunctuation As System.Windows.Forms.Label
	Public WithEvents lblNumberStyle As System.Windows.Forms.Label
	Public WithEvents pnlPage As System.Windows.Forms.GroupBox
	Public WithEvents chkTOCUnderline As System.Windows.Forms.CheckBox
	Public WithEvents chkTOCCaps As System.Windows.Forms.CheckBox
	Public WithEvents chkTOCBold As System.Windows.Forms.CheckBox
	Public WithEvents cmdSetHeader As System.Windows.Forms.Button
	Public WithEvents chkPageBold As System.Windows.Forms.CheckBox
	Public WithEvents chkPageCaps As System.Windows.Forms.CheckBox
	Public WithEvents chkPageUnderline As System.Windows.Forms.CheckBox
	Public WithEvents lblTableOfContents As System.Windows.Forms.Label
	Public WithEvents lblPage As System.Windows.Forms.Label
	Public WithEvents pnlHeader As System.Windows.Forms.GroupBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHeaderFooter))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlLanguage = New System.Windows.Forms.GroupBox()
        Me.cmdSetType = New System.Windows.Forms.Button()
        Me.cbxType = New mpnControls.ComboBox()
        Me.lblType = New System.Windows.Forms.Label()
        Me.pnlPage = New System.Windows.Forms.GroupBox()
        Me.chkContinue = New System.Windows.Forms.CheckBox()
        Me.cmdSetPage = New System.Windows.Forms.Button()
        Me.cmbNumberStyle = New mpnControls.ComboBox()
        Me.cmbPunctuation = New mpnControls.ComboBox()
        Me.lblPunctuation = New System.Windows.Forms.Label()
        Me.lblNumberStyle = New System.Windows.Forms.Label()
        Me.pnlHeader = New System.Windows.Forms.GroupBox()
        Me.chkTOCUnderline = New System.Windows.Forms.CheckBox()
        Me.chkTOCCaps = New System.Windows.Forms.CheckBox()
        Me.chkTOCBold = New System.Windows.Forms.CheckBox()
        Me.cmdSetHeader = New System.Windows.Forms.Button()
        Me.chkPageBold = New System.Windows.Forms.CheckBox()
        Me.chkPageCaps = New System.Windows.Forms.CheckBox()
        Me.chkPageUnderline = New System.Windows.Forms.CheckBox()
        Me.lblTableOfContents = New System.Windows.Forms.Label()
        Me.lblPage = New System.Windows.Forms.Label()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.pnlLanguage.SuspendLayout()
        Me.pnlPage.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlLanguage
        '
        Me.pnlLanguage.BackColor = System.Drawing.SystemColors.Control
        Me.pnlLanguage.Controls.Add(Me.cmdSetType)
        Me.pnlLanguage.Controls.Add(Me.cbxType)
        Me.pnlLanguage.Controls.Add(Me.lblType)
        Me.pnlLanguage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLanguage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlLanguage.Location = New System.Drawing.Point(9, 282)
        Me.pnlLanguage.Name = "pnlLanguage"
        Me.pnlLanguage.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlLanguage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlLanguage.Size = New System.Drawing.Size(357, 58)
        Me.pnlLanguage.TabIndex = 19
        Me.pnlLanguage.TabStop = False
        Me.pnlLanguage.Text = "Language"
        '
        'cmdSetType
        '
        Me.cmdSetType.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSetType.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSetType.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSetType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSetType.Location = New System.Drawing.Point(297, 17)
        Me.cmdSetType.Name = "cmdSetType"
        Me.cmdSetType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSetType.Size = New System.Drawing.Size(48, 28)
        Me.cmdSetType.TabIndex = 21
        Me.cmdSetType.Text = "Set"
        Me.cmdSetType.UseVisualStyleBackColor = False
        '
        'cbxType
        '
        Me.cbxType.AllowEmptyValue = False
        Me.cbxType.AutoSize = True
        Me.cbxType.Borderless = False
        Me.cbxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxType.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.IsDirty = False
        Me.cbxType.LimitToList = True
        Me.cbxType.Location = New System.Drawing.Point(70, 20)
        Me.cbxType.MaxDropDownItems = 8
        Me.cbxType.Name = "cbxType"
        Me.cbxType.SelectedIndex = -1
        Me.cbxType.SelectedValue = Nothing
        Me.cbxType.SelectionLength = 0
        Me.cbxType.SelectionStart = 0
        Me.cbxType.Size = New System.Drawing.Size(217, 24)
        Me.cbxType.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxType.SupportingValues = ""
        Me.cbxType.TabIndex = 22
        Me.cbxType.Tag2 = Nothing
        Me.cbxType.Value = ""
        '
        'lblType
        '
        Me.lblType.BackColor = System.Drawing.SystemColors.Control
        Me.lblType.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblType.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblType.Location = New System.Drawing.Point(13, 25)
        Me.lblType.Name = "lblType"
        Me.lblType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblType.Size = New System.Drawing.Size(42, 19)
        Me.lblType.TabIndex = 20
        Me.lblType.Text = "T&ype:"
        '
        'pnlPage
        '
        Me.pnlPage.BackColor = System.Drawing.SystemColors.Control
        Me.pnlPage.Controls.Add(Me.chkContinue)
        Me.pnlPage.Controls.Add(Me.cmdSetPage)
        Me.pnlPage.Controls.Add(Me.cmbNumberStyle)
        Me.pnlPage.Controls.Add(Me.cmbPunctuation)
        Me.pnlPage.Controls.Add(Me.lblPunctuation)
        Me.pnlPage.Controls.Add(Me.lblNumberStyle)
        Me.pnlPage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlPage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlPage.Location = New System.Drawing.Point(9, 155)
        Me.pnlPage.Name = "pnlPage"
        Me.pnlPage.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlPage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlPage.Size = New System.Drawing.Size(357, 121)
        Me.pnlPage.TabIndex = 10
        Me.pnlPage.TabStop = False
        Me.pnlPage.Text = "Footer - Page Numbering"
        '
        'chkContinue
        '
        Me.chkContinue.BackColor = System.Drawing.SystemColors.Control
        Me.chkContinue.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkContinue.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkContinue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkContinue.Location = New System.Drawing.Point(10, 87)
        Me.chkContinue.Name = "chkContinue"
        Me.chkContinue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkContinue.Size = New System.Drawing.Size(250, 19)
        Me.chkContinue.TabIndex = 15
        Me.chkContinue.Text = "Use Continuous &Page Numbering"
        Me.chkContinue.UseVisualStyleBackColor = False
        '
        'cmdSetPage
        '
        Me.cmdSetPage.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSetPage.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSetPage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSetPage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSetPage.Location = New System.Drawing.Point(297, 78)
        Me.cmdSetPage.Name = "cmdSetPage"
        Me.cmdSetPage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSetPage.Size = New System.Drawing.Size(48, 28)
        Me.cmdSetPage.TabIndex = 16
        Me.cmdSetPage.Text = "Set"
        Me.cmdSetPage.UseVisualStyleBackColor = False
        '
        'cmbNumberStyle
        '
        Me.cmbNumberStyle.AllowEmptyValue = False
        Me.cmbNumberStyle.AutoSize = True
        Me.cmbNumberStyle.Borderless = False
        Me.cmbNumberStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbNumberStyle.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbNumberStyle.IsDirty = False
        Me.cmbNumberStyle.LimitToList = True
        Me.cmbNumberStyle.Location = New System.Drawing.Point(10, 44)
        Me.cmbNumberStyle.MaxDropDownItems = 8
        Me.cmbNumberStyle.Name = "cmbNumberStyle"
        Me.cmbNumberStyle.SelectedIndex = -1
        Me.cmbNumberStyle.SelectedValue = Nothing
        Me.cmbNumberStyle.SelectionLength = 0
        Me.cmbNumberStyle.SelectionStart = 0
        Me.cmbNumberStyle.Size = New System.Drawing.Size(133, 24)
        Me.cmbNumberStyle.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbNumberStyle.SupportingValues = ""
        Me.cmbNumberStyle.TabIndex = 12
        Me.cmbNumberStyle.Tag2 = Nothing
        Me.cmbNumberStyle.Value = ""
        '
        'cmbPunctuation
        '
        Me.cmbPunctuation.AllowEmptyValue = False
        Me.cmbPunctuation.AutoSize = True
        Me.cmbPunctuation.Borderless = False
        Me.cmbPunctuation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbPunctuation.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPunctuation.IsDirty = False
        Me.cmbPunctuation.LimitToList = True
        Me.cmbPunctuation.Location = New System.Drawing.Point(156, 44)
        Me.cmbPunctuation.MaxDropDownItems = 8
        Me.cmbPunctuation.Name = "cmbPunctuation"
        Me.cmbPunctuation.SelectedIndex = -1
        Me.cmbPunctuation.SelectedValue = Nothing
        Me.cmbPunctuation.SelectionLength = 0
        Me.cmbPunctuation.SelectionStart = 0
        Me.cmbPunctuation.Size = New System.Drawing.Size(133, 24)
        Me.cmbPunctuation.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbPunctuation.SupportingValues = ""
        Me.cmbPunctuation.TabIndex = 14
        Me.cmbPunctuation.Tag2 = Nothing
        Me.cmbPunctuation.Value = ""
        '
        'lblPunctuation
        '
        Me.lblPunctuation.BackColor = System.Drawing.SystemColors.Control
        Me.lblPunctuation.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPunctuation.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPunctuation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPunctuation.Location = New System.Drawing.Point(157, 27)
        Me.lblPunctuation.Name = "lblPunctuation"
        Me.lblPunctuation.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPunctuation.Size = New System.Drawing.Size(125, 19)
        Me.lblPunctuation.TabIndex = 13
        Me.lblPunctuation.Text = "Punc&tuation:"
        '
        'lblNumberStyle
        '
        Me.lblNumberStyle.BackColor = System.Drawing.SystemColors.Control
        Me.lblNumberStyle.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblNumberStyle.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumberStyle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblNumberStyle.Location = New System.Drawing.Point(11, 27)
        Me.lblNumberStyle.Name = "lblNumberStyle"
        Me.lblNumberStyle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblNumberStyle.Size = New System.Drawing.Size(120, 19)
        Me.lblNumberStyle.TabIndex = 11
        Me.lblNumberStyle.Text = "&Number Style:"
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.SystemColors.Control
        Me.pnlHeader.Controls.Add(Me.chkTOCUnderline)
        Me.pnlHeader.Controls.Add(Me.chkTOCCaps)
        Me.pnlHeader.Controls.Add(Me.chkTOCBold)
        Me.pnlHeader.Controls.Add(Me.cmdSetHeader)
        Me.pnlHeader.Controls.Add(Me.chkPageBold)
        Me.pnlHeader.Controls.Add(Me.chkPageCaps)
        Me.pnlHeader.Controls.Add(Me.chkPageUnderline)
        Me.pnlHeader.Controls.Add(Me.lblTableOfContents)
        Me.pnlHeader.Controls.Add(Me.lblPage)
        Me.pnlHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlHeader.Location = New System.Drawing.Point(9, 12)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlHeader.Size = New System.Drawing.Size(357, 132)
        Me.pnlHeader.TabIndex = 0
        Me.pnlHeader.TabStop = False
        Me.pnlHeader.Text = "Header"
        '
        'chkTOCUnderline
        '
        Me.chkTOCUnderline.BackColor = System.Drawing.SystemColors.Control
        Me.chkTOCUnderline.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkTOCUnderline.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTOCUnderline.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkTOCUnderline.Location = New System.Drawing.Point(14, 70)
        Me.chkTOCUnderline.Name = "chkTOCUnderline"
        Me.chkTOCUnderline.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkTOCUnderline.Size = New System.Drawing.Size(88, 19)
        Me.chkTOCUnderline.TabIndex = 3
        Me.chkTOCUnderline.Text = "&Underline"
        Me.chkTOCUnderline.UseVisualStyleBackColor = False
        '
        'chkTOCCaps
        '
        Me.chkTOCCaps.BackColor = System.Drawing.SystemColors.Control
        Me.chkTOCCaps.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkTOCCaps.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTOCCaps.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkTOCCaps.Location = New System.Drawing.Point(14, 95)
        Me.chkTOCCaps.Name = "chkTOCCaps"
        Me.chkTOCCaps.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkTOCCaps.Size = New System.Drawing.Size(80, 19)
        Me.chkTOCCaps.TabIndex = 4
        Me.chkTOCCaps.Text = "All &Caps"
        Me.chkTOCCaps.UseVisualStyleBackColor = False
        '
        'chkTOCBold
        '
        Me.chkTOCBold.BackColor = System.Drawing.SystemColors.Control
        Me.chkTOCBold.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkTOCBold.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTOCBold.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkTOCBold.Location = New System.Drawing.Point(14, 45)
        Me.chkTOCBold.Name = "chkTOCBold"
        Me.chkTOCBold.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkTOCBold.Size = New System.Drawing.Size(52, 19)
        Me.chkTOCBold.TabIndex = 2
        Me.chkTOCBold.Text = "&Bold"
        Me.chkTOCBold.UseVisualStyleBackColor = False
        '
        'cmdSetHeader
        '
        Me.cmdSetHeader.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSetHeader.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSetHeader.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSetHeader.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSetHeader.Location = New System.Drawing.Point(297, 86)
        Me.cmdSetHeader.Name = "cmdSetHeader"
        Me.cmdSetHeader.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSetHeader.Size = New System.Drawing.Size(48, 28)
        Me.cmdSetHeader.TabIndex = 9
        Me.cmdSetHeader.Text = "Set"
        Me.cmdSetHeader.UseVisualStyleBackColor = False
        '
        'chkPageBold
        '
        Me.chkPageBold.BackColor = System.Drawing.SystemColors.Control
        Me.chkPageBold.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPageBold.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPageBold.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPageBold.Location = New System.Drawing.Point(158, 45)
        Me.chkPageBold.Name = "chkPageBold"
        Me.chkPageBold.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPageBold.Size = New System.Drawing.Size(52, 19)
        Me.chkPageBold.TabIndex = 6
        Me.chkPageBold.Text = "&Bold"
        Me.chkPageBold.UseVisualStyleBackColor = False
        '
        'chkPageCaps
        '
        Me.chkPageCaps.BackColor = System.Drawing.SystemColors.Control
        Me.chkPageCaps.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPageCaps.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPageCaps.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPageCaps.Location = New System.Drawing.Point(158, 95)
        Me.chkPageCaps.Name = "chkPageCaps"
        Me.chkPageCaps.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPageCaps.Size = New System.Drawing.Size(80, 19)
        Me.chkPageCaps.TabIndex = 8
        Me.chkPageCaps.Text = "All &Caps"
        Me.chkPageCaps.UseVisualStyleBackColor = False
        '
        'chkPageUnderline
        '
        Me.chkPageUnderline.BackColor = System.Drawing.SystemColors.Control
        Me.chkPageUnderline.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkPageUnderline.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPageUnderline.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPageUnderline.Location = New System.Drawing.Point(158, 70)
        Me.chkPageUnderline.Name = "chkPageUnderline"
        Me.chkPageUnderline.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkPageUnderline.Size = New System.Drawing.Size(88, 19)
        Me.chkPageUnderline.TabIndex = 7
        Me.chkPageUnderline.Text = "&Underline"
        Me.chkPageUnderline.UseVisualStyleBackColor = False
        '
        'lblTableOfContents
        '
        Me.lblTableOfContents.BackColor = System.Drawing.SystemColors.Control
        Me.lblTableOfContents.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblTableOfContents.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTableOfContents.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTableOfContents.Location = New System.Drawing.Point(9, 23)
        Me.lblTableOfContents.Name = "lblTableOfContents"
        Me.lblTableOfContents.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblTableOfContents.Size = New System.Drawing.Size(133, 19)
        Me.lblTableOfContents.TabIndex = 1
        Me.lblTableOfContents.Text = "Table of Contents"
        '
        'lblPage
        '
        Me.lblPage.BackColor = System.Drawing.SystemColors.Control
        Me.lblPage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPage.Location = New System.Drawing.Point(155, 23)
        Me.lblPage.Name = "lblPage"
        Me.lblPage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPage.Size = New System.Drawing.Size(42, 19)
        Me.lblPage.TabIndex = 5
        Me.lblPage.Text = "Page"
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 342)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.Size = New System.Drawing.Size(374, 48)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 24
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 48)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 48)
        '
        'frmHeaderFooter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(374, 390)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.pnlLanguage)
        Me.Controls.Add(Me.pnlPage)
        Me.Controls.Add(Me.pnlHeader)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHeaderFooter"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " TOC Header/Footer"
        Me.pnlLanguage.ResumeLayout(False)
        Me.pnlLanguage.PerformLayout()
        Me.pnlPage.ResumeLayout(False)
        Me.pnlPage.PerformLayout()
        Me.pnlHeader.ResumeLayout(False)
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
#End Region 
End Class