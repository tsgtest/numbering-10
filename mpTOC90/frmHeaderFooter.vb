Option Strict Off
Option Explicit On
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.Base.cNumTOC
Imports LMP
Imports System.Windows.Forms

Friend Class frmHeaderFooter
    Inherits System.Windows.Forms.Form
    Public Cancelled As Boolean
    Private m_iNumberStyleDefault As Short
    Private m_iNumberPunctuationDefault As Short

    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Cancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            Cancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkTOCBold_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTOCBold.CheckStateChanged
        Try
            Me.lblTableOfContents.Font = FontChangeBold(Me.lblTableOfContents.Font, Me.chkTOCBold.CheckState)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub chkTOCCaps_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTOCCaps.CheckStateChanged
        Try
            If Me.chkTOCCaps.CheckState = 1 Then
                Me.lblTableOfContents.Text = "TABLE OF CONTENTS"
            Else
                Me.lblTableOfContents.Text = "Table of Contents"
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub chkTOCUnderline_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTOCUnderline.CheckStateChanged
        Try
            Me.lblTableOfContents.Font = FontChangeUnderline(Me.lblTableOfContents.Font, Me.chkTOCUnderline.CheckState)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkPageBold_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPageBold.CheckStateChanged
        Try
            Me.lblPage.Font = FontChangeBold(Me.lblPage.Font, Me.chkPageBold.CheckState)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkPageCaps_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPageCaps.CheckStateChanged
        Try
            If Me.chkPageCaps.CheckState = 1 Then
                Me.lblPage.Text = "PAGE"
            Else
                Me.lblPage.Text = "Page"
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkPageUnderline_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPageUnderline.CheckStateChanged
        Try
            Me.lblPage.Font = FontChangeUnderline(Me.lblPage.Font, Me.chkPageUnderline.CheckState)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdSetHeader_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSetHeader.Click
        Try
            SetUserSetting("TOC", "HeaderTOCBold", CStr((Me.chkTOCBold).CheckState))
            SetUserSetting("TOC", "HeaderTOCCaps", CStr((Me.chkTOCCaps).CheckState))
            SetUserSetting("TOC", "HeaderTOCUnderline", CStr((Me.chkTOCUnderline).CheckState))
            SetUserSetting("TOC", "HeaderPageBold", CStr((Me.chkPageBold).CheckState))
            SetUserSetting("TOC", "HeaderPageCaps", CStr((Me.chkPageCaps).CheckState))
            SetUserSetting("TOC", "HeaderPageUnderline", CStr((Me.chkPageUnderline).CheckState))
            g_oWordApp.StatusBar = "Default header format set"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdSetPage_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSetPage.Click
        Try
            SetUserSetting("TOC", "PageNoStyle", (Me.cmbNumberStyle.SelectedIndex))
            SetUserSetting("TOC", "PageNoPunctuation", (Me.cmbPunctuation.SelectedIndex))
            SetUserSetting("TOC", "ContinuePageNoFromPrevious", CStr((Me.chkContinue).CheckState))
            g_oWordApp.StatusBar = "Default page numbering set"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cmdSetType_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSetType.Click
        Try
            SetUserSetting("TOC", "DefaultBoilerplateIndex", (Me.cbxType.SelectedIndex))
            g_oWordApp.StatusBar = "Default language type set"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    'UPGRADE_WARNING: Form event frmHeaderFooter.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
    Private Sub frmHeaderFooter_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
        Try
            Me.cmbPunctuation.SelectedValue = System.Math.Abs(Me.NumberPunctuationDefault)
            If Me.cmbPunctuation.Text = "" Then
                Me.cmbPunctuation.SelectedIndex = 0
            End If
            Me.cmbNumberStyle.SelectedIndex = Me.NumberStyleDefault
            If Me.cmbNumberStyle.Text = "" Then
                Me.cmbNumberStyle.SelectedIndex = 0
            End If

            If g_xBoilerplates(0, 0) <> "" Then
                Try
                    Me.cbxType.SelectedIndex = CInt(GetUserSetting("TOC", "DefaultBoilerplateIndex"))
                Catch
                End Try
                If Me.cbxType.Text = "" Then Me.cbxType.SelectedIndex = 0
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmHeaderFooter_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmHeaderFooter_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim arNumberStyle As Array
        Dim arPunctuation As Array
        Dim arType As Array

        Try
            Cancelled = True

            'adjust for scaling
            Dim sFactor As Single = LMP.Numbering.TOC.mpApplication.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnOK.Height = Me.btnOK.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
            End If

            '   load number style
            arNumberStyle = New String(4, 1) {{"Arabic (1, 2, 3)", "0"},
                                              {"Roman (I, II, III)", "1"},
                                              {"Roman (i, ii, iii) ", "2"},
                                              {"Letter (A, B, C)", "3"},
                                              {"Letter (a, b, c) ", "4"}}


            Me.cmbNumberStyle.SetList(arNumberStyle)

            '   load punctuation
            arPunctuation = New String(2, 1) {{"(None)", "0"},
                                              {"Hyphens (-#-)", "1"},
                                              {"Hyphens (- # -)", "2"}}

            Me.cmbPunctuation.SetList(arPunctuation)

            If g_xBoilerplates(0, 0) <> "" Then
                arType = New String(UBound(g_xBoilerplates), 1) {}

                For i = 0 To UBound(g_xBoilerplates)
                    arType.SetValue(g_xBoilerplates(i, 0), i, 0)
                    arType.SetValue(g_xBoilerplates(i, 1), i, 1)
                Next i

                Me.cbxType.SetList(arType)
            Else
                Me.pnlLanguage.Visible = False
                Me.lblType.Visible = False
                Me.cbxType.Visible = False
                Me.cmdSetType.Visible = False
                Me.Height = TwipsToPixelsY(PixelsToTwipsY(Me.Height) - 800)
                'Me.btnCancel.Top = TwipsToPixelsY(PixelsToTwipsY(Me.btnCancel.Top) - 800)
                'Me.btnOK.Top = TwipsToPixelsY(PixelsToTwipsY(Me.btnOK.Top) - 800)
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub


    Public Property NumberStyleDefault() As Short
        Get
            NumberStyleDefault = m_iNumberStyleDefault
        End Get
        Set(ByVal Value As Short)
            m_iNumberStyleDefault = Value
        End Set
    End Property


    Public Property NumberPunctuationDefault() As Short
        Get
            NumberPunctuationDefault = m_iNumberPunctuationDefault
        End Get
        Set(ByVal Value As Short)
            m_iNumberPunctuationDefault = Value
        End Set
    End Property
End Class