﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInsertTOCFrench
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInsertTOCFrench))
        Me.tsMenu = New System.Windows.Forms.ToolStrip()
        Me.tbtnMain = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbtnOptions = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbtnAdvanced = New System.Windows.Forms.ToolStripButton()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.btnSetDefaults = New System.Windows.Forms.Button()
        Me.fraContent = New System.Windows.Forms.GroupBox()
        Me.chkStyles = New System.Windows.Forms.CheckBox()
        Me.lblInsertAt = New System.Windows.Forms.Label()
        Me.chkTCEntries = New System.Windows.Forms.CheckBox()
        Me.lblCreateFrom = New System.Windows.Forms.Label()
        Me.lblThrough = New System.Windows.Forms.Label()
        Me.lblIncludeLevels = New System.Windows.Forms.Label()
        Me.fraMark = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkMark = New System.Windows.Forms.CheckBox()
        Me.pnlOptions = New System.Windows.Forms.Panel()
        Me.fraSchemes = New System.Windows.Forms.GroupBox()
        Me.lstStyles = New System.Windows.Forms.CheckedListBox()
        Me.optIncludeAll = New System.Windows.Forms.RadioButton()
        Me.optIncludeStyles = New System.Windows.Forms.RadioButton()
        Me.optIncludeSchemes = New System.Windows.Forms.RadioButton()
        Me.fraTOCScheme = New System.Windows.Forms.GroupBox()
        Me.optTOCStylesKeep = New System.Windows.Forms.RadioButton()
        Me.optTOCStylesUpdate = New System.Windows.Forms.RadioButton()
        Me.fraTOCBody = New System.Windows.Forms.GroupBox()
        Me.chkApplyTOC9 = New System.Windows.Forms.CheckBox()
        Me.chkApplyManualFormatsToTOC = New System.Windows.Forms.CheckBox()
        Me.chkTwoColumn = New System.Windows.Forms.CheckBox()
        Me.pnlAdvanced = New System.Windows.Forms.Panel()
        Me.cmdEdit = New System.Windows.Forms.Button()
        Me.btnEditHeaderFooter = New System.Windows.Forms.Button()
        Me.chkHyperlinks = New System.Windows.Forms.CheckBox()
        Me.lstDesignate = New System.Windows.Forms.CheckedListBox()
        Me.fraStyleList = New System.Windows.Forms.Label()
        Me.cbxInsertAt = New mpnControls.ComboBox()
        Me.cbxMaxLevel = New mpnControls.ComboBox()
        Me.cbxMinLevel = New mpnControls.ComboBox()
        Me.cmbTextStyles = New mpnControls.ComboBox()
        Me.cbxHeadingDef = New mpnControls.ComboBox()
        Me.cbxTOCScheme = New mpnControls.ComboBox()
        Me.cbxScheduleStyles = New mpnControls.ComboBox()
        Me.tsMenu.SuspendLayout()
        Me.tsButtons.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.fraContent.SuspendLayout()
        Me.fraMark.SuspendLayout()
        Me.pnlOptions.SuspendLayout()
        Me.fraSchemes.SuspendLayout()
        Me.fraTOCScheme.SuspendLayout()
        Me.fraTOCBody.SuspendLayout()
        Me.pnlAdvanced.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsMenu
        '
        Me.tsMenu.AutoSize = False
        Me.tsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsMenu.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.tsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbtnMain, Me.toolStripSeparator12, Me.tbtnOptions, Me.toolStripSeparator11, Me.tbtnAdvanced})
        Me.tsMenu.Location = New System.Drawing.Point(0, 0)
        Me.tsMenu.Name = "tsMenu"
        Me.tsMenu.Size = New System.Drawing.Size(367, 34)
        Me.tsMenu.Stretch = True
        Me.tsMenu.TabIndex = 20
        Me.tsMenu.Text = "ToolStrip1"
        '
        'tbtnMain
        '
        Me.tbtnMain.CheckOnClick = True
        Me.tbtnMain.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnMain.Name = "tbtnMain"
        Me.tbtnMain.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnMain.Size = New System.Drawing.Size(57, 31)
        Me.tbtnMain.Text = "&Principal"
        Me.tbtnMain.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator12
        '
        Me.toolStripSeparator12.Name = "toolStripSeparator12"
        Me.toolStripSeparator12.Size = New System.Drawing.Size(6, 34)
        '
        'tbtnOptions
        '
        Me.tbtnOptions.CheckOnClick = True
        Me.tbtnOptions.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbtnOptions.Name = "tbtnOptions"
        Me.tbtnOptions.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnOptions.Size = New System.Drawing.Size(53, 31)
        Me.tbtnOptions.Text = "&Options"
        Me.tbtnOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator11
        '
        Me.toolStripSeparator11.Name = "toolStripSeparator11"
        Me.toolStripSeparator11.Size = New System.Drawing.Size(6, 34)
        '
        'tbtnAdvanced
        '
        Me.tbtnAdvanced.CheckOnClick = True
        Me.tbtnAdvanced.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tbtnAdvanced.Image = CType(resources.GetObject("tbtnAdvanced.Image"), System.Drawing.Image)
        Me.tbtnAdvanced.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbtnAdvanced.Name = "tbtnAdvanced"
        Me.tbtnAdvanced.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnAdvanced.Size = New System.Drawing.Size(50, 31)
        Me.tbtnAdvanced.Text = "&Avancé"
        Me.tbtnAdvanced.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 383)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.Size = New System.Drawing.Size(367, 48)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 21
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Fermer"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 48)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 48)
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.btnSetDefaults)
        Me.pnlMain.Controls.Add(Me.fraContent)
        Me.pnlMain.Controls.Add(Me.fraMark)
        Me.pnlMain.Location = New System.Drawing.Point(0, 37)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(369, 342)
        Me.pnlMain.TabIndex = 22
        '
        'btnSetDefaults
        '
        Me.btnSetDefaults.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetDefaults.Location = New System.Drawing.Point(197, 310)
        Me.btnSetDefaults.Name = "btnSetDefaults"
        Me.btnSetDefaults.Size = New System.Drawing.Size(161, 23)
        Me.btnSetDefaults.TabIndex = 3
        Me.btnSetDefaults.Text = "&Définir valeurs par défaut"
        Me.btnSetDefaults.UseVisualStyleBackColor = True
        '
        'fraContent
        '
        Me.fraContent.Controls.Add(Me.chkStyles)
        Me.fraContent.Controls.Add(Me.cbxInsertAt)
        Me.fraContent.Controls.Add(Me.lblInsertAt)
        Me.fraContent.Controls.Add(Me.chkTCEntries)
        Me.fraContent.Controls.Add(Me.lblCreateFrom)
        Me.fraContent.Controls.Add(Me.cbxMaxLevel)
        Me.fraContent.Controls.Add(Me.cbxMinLevel)
        Me.fraContent.Controls.Add(Me.lblThrough)
        Me.fraContent.Controls.Add(Me.lblIncludeLevels)
        Me.fraContent.Controls.Add(Me.cmbTextStyles)
        Me.fraContent.Location = New System.Drawing.Point(14, 5)
        Me.fraContent.Name = "fraContent"
        Me.fraContent.Size = New System.Drawing.Size(344, 175)
        Me.fraContent.TabIndex = 0
        Me.fraContent.TabStop = False
        Me.fraContent.Text = "Contenu"
        '
        'chkStyles
        '
        Me.chkStyles.AutoSize = True
        Me.chkStyles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkStyles.Location = New System.Drawing.Point(108, 69)
        Me.chkStyles.Name = "chkStyles"
        Me.chkStyles.Size = New System.Drawing.Size(56, 19)
        Me.chkStyles.TabIndex = 5
        Me.chkStyles.Text = "St&yles"
        Me.chkStyles.UseVisualStyleBackColor = True
        '
        'lblInsertAt
        '
        Me.lblInsertAt.AutoSize = True
        Me.lblInsertAt.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsertAt.Location = New System.Drawing.Point(10, 138)
        Me.lblInsertAt.Name = "lblInsertAt"
        Me.lblInsertAt.Size = New System.Drawing.Size(45, 15)
        Me.lblInsertAt.TabIndex = 8
        Me.lblInsertAt.Text = "&Insérer:"
        '
        'chkTCEntries
        '
        Me.chkTCEntries.AutoSize = True
        Me.chkTCEntries.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTCEntries.Location = New System.Drawing.Point(108, 97)
        Me.chkTCEntries.Name = "chkTCEntries"
        Me.chkTCEntries.Size = New System.Drawing.Size(85, 19)
        Me.chkTCEntries.TabIndex = 7
        Me.chkTCEntries.Text = "Entrées &TM"
        Me.chkTCEntries.UseVisualStyleBackColor = True
        '
        'lblCreateFrom
        '
        Me.lblCreateFrom.AutoSize = True
        Me.lblCreateFrom.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreateFrom.Location = New System.Drawing.Point(10, 70)
        Me.lblCreateFrom.Name = "lblCreateFrom"
        Me.lblCreateFrom.Size = New System.Drawing.Size(94, 15)
        Me.lblCreateFrom.TabIndex = 4
        Me.lblCreateFrom.Text = "Créer à partir de:"
        '
        'lblThrough
        '
        Me.lblThrough.AutoSize = True
        Me.lblThrough.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblThrough.Location = New System.Drawing.Point(181, 33)
        Me.lblThrough.Name = "lblThrough"
        Me.lblThrough.Size = New System.Drawing.Size(13, 15)
        Me.lblThrough.TabIndex = 2
        Me.lblThrough.Text = "à"
        '
        'lblIncludeLevels
        '
        Me.lblIncludeLevels.AutoSize = True
        Me.lblIncludeLevels.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeLevels.Location = New System.Drawing.Point(10, 33)
        Me.lblIncludeLevels.Name = "lblIncludeLevels"
        Me.lblIncludeLevels.Size = New System.Drawing.Size(110, 15)
        Me.lblIncludeLevels.TabIndex = 0
        Me.lblIncludeLevels.Text = "Co&mprend niveaux:"
        '
        'fraMark
        '
        Me.fraMark.Controls.Add(Me.cbxHeadingDef)
        Me.fraMark.Controls.Add(Me.Label5)
        Me.fraMark.Controls.Add(Me.chkMark)
        Me.fraMark.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraMark.Location = New System.Drawing.Point(14, 192)
        Me.fraMark.Name = "fraMark"
        Me.fraMark.Size = New System.Drawing.Size(344, 111)
        Me.fraMark.TabIndex = 1
        Me.fraMark.TabStop = False
        Me.fraMark.Text = "Marquer"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(15, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Dé&finition des titres:"
        '
        'chkMark
        '
        Me.chkMark.AutoSize = True
        Me.chkMark.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMark.Location = New System.Drawing.Point(18, 23)
        Me.chkMark.Name = "chkMark"
        Me.chkMark.Size = New System.Drawing.Size(184, 19)
        Me.chkMark.TabIndex = 0
        Me.chkMark.Text = "A&ppliquer séparateurs de style"
        Me.chkMark.UseVisualStyleBackColor = True
        '
        'pnlOptions
        '
        Me.pnlOptions.Controls.Add(Me.fraSchemes)
        Me.pnlOptions.Controls.Add(Me.fraTOCScheme)
        Me.pnlOptions.Controls.Add(Me.fraTOCBody)
        Me.pnlOptions.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOptions.Location = New System.Drawing.Point(0, 34)
        Me.pnlOptions.Name = "pnlOptions"
        Me.pnlOptions.Size = New System.Drawing.Size(369, 342)
        Me.pnlOptions.TabIndex = 23
        '
        'fraSchemes
        '
        Me.fraSchemes.Controls.Add(Me.lstStyles)
        Me.fraSchemes.Controls.Add(Me.optIncludeAll)
        Me.fraSchemes.Controls.Add(Me.optIncludeStyles)
        Me.fraSchemes.Controls.Add(Me.optIncludeSchemes)
        Me.fraSchemes.Location = New System.Drawing.Point(14, 192)
        Me.fraSchemes.Name = "fraSchemes"
        Me.fraSchemes.Size = New System.Drawing.Size(344, 143)
        Me.fraSchemes.TabIndex = 2
        Me.fraSchemes.TabStop = False
        Me.fraSchemes.Text = "Comprend"
        '
        'lstStyles
        '
        Me.lstStyles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstStyles.FormattingEnabled = True
        Me.lstStyles.Location = New System.Drawing.Point(32, 41)
        Me.lstStyles.Name = "lstStyles"
        Me.lstStyles.Size = New System.Drawing.Size(245, 40)
        Me.lstStyles.TabIndex = 1
        '
        'optIncludeAll
        '
        Me.optIncludeAll.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optIncludeAll.Location = New System.Drawing.Point(12, 114)
        Me.optIncludeAll.Name = "optIncludeAll"
        Me.optIncludeAll.Size = New System.Drawing.Size(331, 19)
        Me.optIncludeAll.TabIndex = 3
        Me.optIncludeAll.Tag = ""
        Me.optIncludeAll.Text = "Tous les para&graphes des niveaux hiérarchiques spécifiés"
        Me.optIncludeAll.UseVisualStyleBackColor = True
        '
        'optIncludeStyles
        '
        Me.optIncludeStyles.AutoSize = True
        Me.optIncludeStyles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optIncludeStyles.Location = New System.Drawing.Point(12, 92)
        Me.optIncludeStyles.Name = "optIncludeStyles"
        Me.optIncludeStyles.Size = New System.Drawing.Size(288, 19)
        Me.optIncludeStyles.TabIndex = 2
        Me.optIncludeStyles.Tag = ""
        Me.optIncludeStyles.Text = "&Appliquer styles individuellement (Onglet avancé)"
        Me.optIncludeStyles.UseVisualStyleBackColor = True
        '
        'optIncludeSchemes
        '
        Me.optIncludeSchemes.AutoSize = True
        Me.optIncludeSchemes.Checked = True
        Me.optIncludeSchemes.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optIncludeSchemes.Location = New System.Drawing.Point(12, 18)
        Me.optIncludeSchemes.Name = "optIncludeSchemes"
        Me.optIncludeSchemes.Size = New System.Drawing.Size(198, 19)
        Me.optIncludeSchemes.TabIndex = 0
        Me.optIncludeSchemes.TabStop = True
        Me.optIncludeSchemes.Tag = ""
        Me.optIncludeSchemes.Text = "&Entrées à partir du thème suivant"
        Me.optIncludeSchemes.UseVisualStyleBackColor = True
        '
        'fraTOCScheme
        '
        Me.fraTOCScheme.Controls.Add(Me.cbxTOCScheme)
        Me.fraTOCScheme.Controls.Add(Me.optTOCStylesKeep)
        Me.fraTOCScheme.Controls.Add(Me.optTOCStylesUpdate)
        Me.fraTOCScheme.Location = New System.Drawing.Point(14, 5)
        Me.fraTOCScheme.Name = "fraTOCScheme"
        Me.fraTOCScheme.Size = New System.Drawing.Size(344, 76)
        Me.fraTOCScheme.TabIndex = 0
        Me.fraTOCScheme.TabStop = False
        Me.fraTOCScheme.Text = "Format"
        '
        'optTOCStylesKeep
        '
        Me.optTOCStylesKeep.AutoSize = True
        Me.optTOCStylesKeep.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTOCStylesKeep.Location = New System.Drawing.Point(12, 45)
        Me.optTOCStylesKeep.Name = "optTOCStylesKeep"
        Me.optTOCStylesKeep.Size = New System.Drawing.Size(179, 19)
        Me.optTOCStylesKeep.TabIndex = 2
        Me.optTOCStylesKeep.Text = "Garder les styles TM &courants"
        Me.optTOCStylesKeep.UseVisualStyleBackColor = True
        '
        'optTOCStylesUpdate
        '
        Me.optTOCStylesUpdate.AutoSize = True
        Me.optTOCStylesUpdate.Checked = True
        Me.optTOCStylesUpdate.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTOCStylesUpdate.Location = New System.Drawing.Point(12, 19)
        Me.optTOCStylesUpdate.Name = "optTOCStylesUpdate"
        Me.optTOCStylesUpdate.Size = New System.Drawing.Size(149, 19)
        Me.optTOCStylesUpdate.TabIndex = 0
        Me.optTOCStylesUpdate.TabStop = True
        Me.optTOCStylesUpdate.Text = "Mettre à jour &styles TM:"
        Me.optTOCStylesUpdate.UseVisualStyleBackColor = True
        '
        'fraTOCBody
        '
        Me.fraTOCBody.Controls.Add(Me.cbxScheduleStyles)
        Me.fraTOCBody.Controls.Add(Me.chkApplyTOC9)
        Me.fraTOCBody.Controls.Add(Me.chkApplyManualFormatsToTOC)
        Me.fraTOCBody.Controls.Add(Me.chkTwoColumn)
        Me.fraTOCBody.Location = New System.Drawing.Point(14, 83)
        Me.fraTOCBody.Name = "fraTOCBody"
        Me.fraTOCBody.Size = New System.Drawing.Size(344, 103)
        Me.fraTOCBody.TabIndex = 1
        Me.fraTOCBody.TabStop = False
        Me.fraTOCBody.Text = "Autre"
        '
        'chkApplyTOC9
        '
        Me.chkApplyTOC9.AutoSize = True
        Me.chkApplyTOC9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkApplyTOC9.Location = New System.Drawing.Point(12, 69)
        Me.chkApplyTOC9.Name = "chkApplyTOC9"
        Me.chkApplyTOC9.Size = New System.Drawing.Size(147, 19)
        Me.chkApplyTOC9.TabIndex = 3
        Me.chkApplyTOC9.Text = "Appliquer TM style &9 à:"
        Me.chkApplyTOC9.UseVisualStyleBackColor = True
        '
        'chkApplyManualFormatsToTOC
        '
        Me.chkApplyManualFormatsToTOC.AutoSize = True
        Me.chkApplyManualFormatsToTOC.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkApplyManualFormatsToTOC.Location = New System.Drawing.Point(12, 46)
        Me.chkApplyManualFormatsToTOC.Name = "chkApplyManualFormatsToTOC"
        Me.chkApplyManualFormatsToTOC.Size = New System.Drawing.Size(214, 19)
        Me.chkApplyManualFormatsToTOC.TabIndex = 2
        Me.chkApplyManualFormatsToTOC.Text = "Appl&iquer format direct Titre au TM"
        Me.chkApplyManualFormatsToTOC.UseVisualStyleBackColor = True
        '
        'chkTwoColumn
        '
        Me.chkTwoColumn.AutoSize = True
        Me.chkTwoColumn.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTwoColumn.Location = New System.Drawing.Point(12, 24)
        Me.chkTwoColumn.Name = "chkTwoColumn"
        Me.chkTwoColumn.Size = New System.Drawing.Size(161, 19)
        Me.chkTwoColumn.TabIndex = 0
        Me.chkTwoColumn.Text = "&Utiliser format 2 colonnes"
        Me.chkTwoColumn.UseVisualStyleBackColor = True
        '
        'pnlAdvanced
        '
        Me.pnlAdvanced.Controls.Add(Me.cmdEdit)
        Me.pnlAdvanced.Controls.Add(Me.btnEditHeaderFooter)
        Me.pnlAdvanced.Controls.Add(Me.chkHyperlinks)
        Me.pnlAdvanced.Controls.Add(Me.lstDesignate)
        Me.pnlAdvanced.Controls.Add(Me.fraStyleList)
        Me.pnlAdvanced.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAdvanced.Location = New System.Drawing.Point(0, 37)
        Me.pnlAdvanced.Name = "pnlAdvanced"
        Me.pnlAdvanced.Size = New System.Drawing.Size(369, 342)
        Me.pnlAdvanced.TabIndex = 25
        '
        'cmdEdit
        '
        Me.cmdEdit.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEdit.Location = New System.Drawing.Point(21, 295)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(267, 28)
        Me.cmdEdit.TabIndex = 4
        Me.cmdEdit.Text = "&Éditer thème personnalisé TM..."
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'btnEditHeaderFooter
        '
        Me.btnEditHeaderFooter.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditHeaderFooter.Location = New System.Drawing.Point(21, 256)
        Me.btnEditHeaderFooter.Name = "btnEditHeaderFooter"
        Me.btnEditHeaderFooter.Size = New System.Drawing.Size(267, 28)
        Me.btnEditHeaderFooter.TabIndex = 3
        Me.btnEditHeaderFooter.Text = "Format &en-tête/pied de page..."
        Me.btnEditHeaderFooter.UseVisualStyleBackColor = True
        '
        'chkHyperlinks
        '
        Me.chkHyperlinks.AutoSize = True
        Me.chkHyperlinks.Checked = True
        Me.chkHyperlinks.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHyperlinks.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHyperlinks.Location = New System.Drawing.Point(27, 219)
        Me.chkHyperlinks.Name = "chkHyperlinks"
        Me.chkHyperlinks.Size = New System.Drawing.Size(196, 19)
        Me.chkHyperlinks.TabIndex = 2
        Me.chkHyperlinks.Text = "&Insérer entrées TM en hyperliens"
        Me.chkHyperlinks.UseVisualStyleBackColor = True
        '
        'lstDesignate
        '
        Me.lstDesignate.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDesignate.FormattingEnabled = True
        Me.lstDesignate.Location = New System.Drawing.Point(21, 32)
        Me.lstDesignate.Name = "lstDesignate"
        Me.lstDesignate.Size = New System.Drawing.Size(270, 166)
        Me.lstDesignate.TabIndex = 1
        '
        'fraStyleList
        '
        Me.fraStyleList.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraStyleList.Location = New System.Drawing.Point(18, 15)
        Me.fraStyleList.Name = "fraStyleList"
        Me.fraStyleList.Size = New System.Drawing.Size(334, 15)
        Me.fraStyleList.TabIndex = 0
        Me.fraStyleList.Text = "Inclure styles disponibles suivants"
        '
        'cbxInsertAt
        '
        Me.cbxInsertAt.AllowEmptyValue = False
        Me.cbxInsertAt.AutoSize = True
        Me.cbxInsertAt.Borderless = False
        Me.cbxInsertAt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxInsertAt.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInsertAt.IsDirty = False
        Me.cbxInsertAt.LimitToList = True
        Me.cbxInsertAt.Location = New System.Drawing.Point(58, 134)
        Me.cbxInsertAt.MaxDropDownItems = 8
        Me.cbxInsertAt.Name = "cbxInsertAt"
        Me.cbxInsertAt.SelectedIndex = -1
        Me.cbxInsertAt.SelectedValue = Nothing
        Me.cbxInsertAt.SelectionLength = 0
        Me.cbxInsertAt.SelectionStart = 0
        Me.cbxInsertAt.Size = New System.Drawing.Size(196, 23)
        Me.cbxInsertAt.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxInsertAt.SupportingValues = ""
        Me.cbxInsertAt.TabIndex = 9
        Me.cbxInsertAt.Tag2 = Nothing
        Me.cbxInsertAt.Value = ""
        '
        'cbxMaxLevel
        '
        Me.cbxMaxLevel.AllowEmptyValue = False
        Me.cbxMaxLevel.AutoSize = True
        Me.cbxMaxLevel.Borderless = False
        Me.cbxMaxLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxMaxLevel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMaxLevel.IsDirty = False
        Me.cbxMaxLevel.LimitToList = True
        Me.cbxMaxLevel.Location = New System.Drawing.Point(203, 29)
        Me.cbxMaxLevel.MaxDropDownItems = 8
        Me.cbxMaxLevel.Name = "cbxMaxLevel"
        Me.cbxMaxLevel.SelectedIndex = -1
        Me.cbxMaxLevel.SelectedValue = Nothing
        Me.cbxMaxLevel.SelectionLength = 0
        Me.cbxMaxLevel.SelectionStart = 0
        Me.cbxMaxLevel.Size = New System.Drawing.Size(51, 23)
        Me.cbxMaxLevel.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxMaxLevel.SupportingValues = ""
        Me.cbxMaxLevel.TabIndex = 3
        Me.cbxMaxLevel.Tag2 = Nothing
        Me.cbxMaxLevel.Value = ""
        '
        'cbxMinLevel
        '
        Me.cbxMinLevel.AllowEmptyValue = False
        Me.cbxMinLevel.AutoSize = True
        Me.cbxMinLevel.Borderless = False
        Me.cbxMinLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxMinLevel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMinLevel.IsDirty = False
        Me.cbxMinLevel.LimitToList = True
        Me.cbxMinLevel.Location = New System.Drawing.Point(120, 29)
        Me.cbxMinLevel.MaxDropDownItems = 8
        Me.cbxMinLevel.Name = "cbxMinLevel"
        Me.cbxMinLevel.SelectedIndex = -1
        Me.cbxMinLevel.SelectedValue = Nothing
        Me.cbxMinLevel.SelectionLength = 0
        Me.cbxMinLevel.SelectionStart = 0
        Me.cbxMinLevel.Size = New System.Drawing.Size(51, 23)
        Me.cbxMinLevel.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxMinLevel.SupportingValues = ""
        Me.cbxMinLevel.TabIndex = 1
        Me.cbxMinLevel.Tag2 = Nothing
        Me.cbxMinLevel.Value = ""
        '
        'cmbTextStyles
        '
        Me.cmbTextStyles.AllowEmptyValue = False
        Me.cmbTextStyles.AutoSize = True
        Me.cmbTextStyles.Borderless = False
        Me.cmbTextStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cmbTextStyles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTextStyles.IsDirty = False
        Me.cmbTextStyles.LimitToList = True
        Me.cmbTextStyles.Location = New System.Drawing.Point(34, 96)
        Me.cmbTextStyles.MaxDropDownItems = 8
        Me.cmbTextStyles.Name = "cmbTextStyles"
        Me.cmbTextStyles.SelectedIndex = -1
        Me.cmbTextStyles.SelectedValue = Nothing
        Me.cmbTextStyles.SelectionLength = 0
        Me.cmbTextStyles.SelectionStart = 0
        Me.cmbTextStyles.Size = New System.Drawing.Size(267, 23)
        Me.cmbTextStyles.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cmbTextStyles.SupportingValues = ""
        Me.cmbTextStyles.TabIndex = 6
        Me.cmbTextStyles.Tag2 = Nothing
        Me.cmbTextStyles.Value = ""
        Me.cmbTextStyles.Visible = False
        '
        'cbxHeadingDef
        '
        Me.cbxHeadingDef.AllowEmptyValue = False
        Me.cbxHeadingDef.AutoSize = True
        Me.cbxHeadingDef.Borderless = False
        Me.cbxHeadingDef.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxHeadingDef.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxHeadingDef.IsDirty = False
        Me.cbxHeadingDef.LimitToList = True
        Me.cbxHeadingDef.Location = New System.Drawing.Point(16, 74)
        Me.cbxHeadingDef.MaxDropDownItems = 8
        Me.cbxHeadingDef.Name = "cbxHeadingDef"
        Me.cbxHeadingDef.SelectedIndex = -1
        Me.cbxHeadingDef.SelectedValue = Nothing
        Me.cbxHeadingDef.SelectionLength = 0
        Me.cbxHeadingDef.SelectionStart = 0
        Me.cbxHeadingDef.Size = New System.Drawing.Size(267, 23)
        Me.cbxHeadingDef.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxHeadingDef.SupportingValues = ""
        Me.cbxHeadingDef.TabIndex = 2
        Me.cbxHeadingDef.Tag2 = Nothing
        Me.cbxHeadingDef.Value = ""
        '
        'cbxTOCScheme
        '
        Me.cbxTOCScheme.AllowEmptyValue = False
        Me.cbxTOCScheme.AutoSize = True
        Me.cbxTOCScheme.Borderless = False
        Me.cbxTOCScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxTOCScheme.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxTOCScheme.IsDirty = False
        Me.cbxTOCScheme.LimitToList = True
        Me.cbxTOCScheme.Location = New System.Drawing.Point(160, 18)
        Me.cbxTOCScheme.MaxDropDownItems = 8
        Me.cbxTOCScheme.Name = "cbxTOCScheme"
        Me.cbxTOCScheme.SelectedIndex = -1
        Me.cbxTOCScheme.SelectedValue = Nothing
        Me.cbxTOCScheme.SelectionLength = 0
        Me.cbxTOCScheme.SelectionStart = 0
        Me.cbxTOCScheme.Size = New System.Drawing.Size(132, 23)
        Me.cbxTOCScheme.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxTOCScheme.SupportingValues = ""
        Me.cbxTOCScheme.TabIndex = 1
        Me.cbxTOCScheme.Tag2 = Nothing
        Me.cbxTOCScheme.Value = ""
        '
        'cbxScheduleStyles
        '
        Me.cbxScheduleStyles.AllowEmptyValue = False
        Me.cbxScheduleStyles.AutoSize = True
        Me.cbxScheduleStyles.Borderless = False
        Me.cbxScheduleStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxScheduleStyles.Enabled = False
        Me.cbxScheduleStyles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxScheduleStyles.IsDirty = False
        Me.cbxScheduleStyles.LimitToList = True
        Me.cbxScheduleStyles.Location = New System.Drawing.Point(160, 67)
        Me.cbxScheduleStyles.MaxDropDownItems = 8
        Me.cbxScheduleStyles.Name = "cbxScheduleStyles"
        Me.cbxScheduleStyles.SelectedIndex = -1
        Me.cbxScheduleStyles.SelectedValue = Nothing
        Me.cbxScheduleStyles.SelectionLength = 0
        Me.cbxScheduleStyles.SelectionStart = 0
        Me.cbxScheduleStyles.Size = New System.Drawing.Size(132, 23)
        Me.cbxScheduleStyles.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxScheduleStyles.SupportingValues = ""
        Me.cbxScheduleStyles.TabIndex = 4
        Me.cbxScheduleStyles.Tag2 = Nothing
        Me.cbxScheduleStyles.Value = ""
        '
        'frmInsertTOCFrench
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(367, 431)
        Me.Controls.Add(Me.tsButtons)
        Me.Controls.Add(Me.tsMenu)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.pnlAdvanced)
        Me.Controls.Add(Me.pnlOptions)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInsertTOCFrench"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " Insérer Table de Matières"
        Me.tsMenu.ResumeLayout(False)
        Me.tsMenu.PerformLayout()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.pnlMain.ResumeLayout(False)
        Me.fraContent.ResumeLayout(False)
        Me.fraContent.PerformLayout()
        Me.fraMark.ResumeLayout(False)
        Me.fraMark.PerformLayout()
        Me.pnlOptions.ResumeLayout(False)
        Me.fraSchemes.ResumeLayout(False)
        Me.fraSchemes.PerformLayout()
        Me.fraTOCScheme.ResumeLayout(False)
        Me.fraTOCScheme.PerformLayout()
        Me.fraTOCBody.ResumeLayout(False)
        Me.fraTOCBody.PerformLayout()
        Me.pnlAdvanced.ResumeLayout(False)
        Me.pnlAdvanced.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents tbtnMain As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbtnOptions As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbtnAdvanced As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnSetDefaults As System.Windows.Forms.Button
    Friend WithEvents fraContent As System.Windows.Forms.GroupBox
    Friend WithEvents cbxInsertAt As mpnControls.ComboBox
    Friend WithEvents lblInsertAt As System.Windows.Forms.Label
    Friend WithEvents chkTCEntries As System.Windows.Forms.CheckBox
    Friend WithEvents lblCreateFrom As System.Windows.Forms.Label
    Friend WithEvents cbxMaxLevel As mpnControls.ComboBox
    Friend WithEvents cbxMinLevel As mpnControls.ComboBox
    Friend WithEvents lblThrough As System.Windows.Forms.Label
    Friend WithEvents lblIncludeLevels As System.Windows.Forms.Label
    Friend WithEvents fraMark As System.Windows.Forms.GroupBox
    Friend WithEvents cbxHeadingDef As mpnControls.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkMark As System.Windows.Forms.CheckBox
    Friend WithEvents pnlOptions As System.Windows.Forms.Panel
    Friend WithEvents chkStyles As System.Windows.Forms.CheckBox
    Friend WithEvents fraTOCScheme As System.Windows.Forms.GroupBox
    Friend WithEvents cbxTOCScheme As mpnControls.ComboBox
    Friend WithEvents optTOCStylesKeep As System.Windows.Forms.RadioButton
    Friend WithEvents optTOCStylesUpdate As System.Windows.Forms.RadioButton
    Friend WithEvents fraTOCBody As System.Windows.Forms.GroupBox
    Friend WithEvents cbxScheduleStyles As mpnControls.ComboBox
    Friend WithEvents chkApplyTOC9 As System.Windows.Forms.CheckBox
    Friend WithEvents chkApplyManualFormatsToTOC As System.Windows.Forms.CheckBox
    Friend WithEvents chkTwoColumn As System.Windows.Forms.CheckBox
    Friend WithEvents fraSchemes As System.Windows.Forms.GroupBox
    Friend WithEvents optIncludeStyles As System.Windows.Forms.RadioButton
    Friend WithEvents optIncludeSchemes As System.Windows.Forms.RadioButton
    Friend WithEvents lstStyles As System.Windows.Forms.CheckedListBox
    Friend WithEvents optIncludeAll As System.Windows.Forms.RadioButton
    Friend WithEvents pnlAdvanced As System.Windows.Forms.Panel
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents btnEditHeaderFooter As System.Windows.Forms.Button
    Friend WithEvents chkHyperlinks As System.Windows.Forms.CheckBox
    Friend WithEvents lstDesignate As System.Windows.Forms.CheckedListBox
    Friend WithEvents fraStyleList As System.Windows.Forms.Label
    Friend WithEvents cmbTextStyles As mpnControls.ComboBox
End Class
