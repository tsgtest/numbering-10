﻿Option Strict Off
Option Explicit On
Imports LMP.Numbering.TOC.mpTOC
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cStrings
Imports System.Windows.Forms
Imports LMP
Imports [Word] = Microsoft.Office.Interop.Word

Public Class frmInsertTOCFrench
    Inherits System.Windows.Forms.Form

    Public Cancelled As Boolean
    Private m_bStepIndent As Boolean
    Private m_arStyles As Array
    Private m_arDesignate As Array
    Private m_arLevels As Array
    Private m_arInsertAt As Array
    Private m_arTOCScheme(,) As String
    Private m_arScheduleStyles As Array
    Private m_arHeadingDef As Array
    Private m_bTOCBold As Boolean
    Private m_bTOCCap As Boolean
    Private m_bTOCUnderline As Boolean
    Private m_bPageBold As Boolean
    Private m_bPageCap As Boolean
    Private m_bPageUnderline As Boolean
    Private m_iNumberStyle As Short
    Private m_iNumberPunctuation As Short
    Private m_xSchemes(,) As String
    Private m_bRefreshStyleList As Boolean
    Private m_bNoJumpToAdvanced As Boolean
    Private m_iTOCMarker As mpTOCExistsMarker
    Private m_bContinuePageNumbering As Boolean
    Private m_bInsertAsField As Boolean
    Private m_bLoaded As Boolean
    Public Property InsertAsField As Boolean
        Get
            InsertAsField = m_bInsertAsField
        End Get
        Set(value As Boolean)
            m_bInsertAsField = value
        End Set
    End Property
    Public ReadOnly Property MaxLevel As Integer
        Get
            MaxLevel = CInt(Me.cbxMaxLevel.Text)
        End Get
    End Property
    Public ReadOnly Property MinLevel As Integer
        Get
            MinLevel = CInt(Me.cbxMinLevel.Text)
        End Get
    End Property
    Public ReadOnly Property InsertAt As String
        Get
            InsertAt = Me.cbxInsertAt.Text
        End Get
    End Property
    Public ReadOnly Property TOCScheme() As String
        Get
            If Me.cbxTOCScheme.SelectedIndex < 1 Then
                TOCScheme = "Custom"
            Else
                TOCScheme = m_xSchemes(Me.cbxTOCScheme.SelectedIndex - 1, 1)
            End If
        End Get
    End Property
    Public Property BoldHeaderTOC() As Boolean
        Get
            BoldHeaderTOC = m_bTOCBold
        End Get
        Set(ByVal Value As Boolean)
            m_bTOCBold = Value
        End Set
    End Property
    Public Property CapHeaderTOC() As Boolean
        Get
            CapHeaderTOC = m_bTOCCap
        End Get
        Set(ByVal Value As Boolean)
            m_bTOCCap = Value
        End Set
    End Property


    Public Property UnderlineHeaderTOC() As Boolean
        Get
            UnderlineHeaderTOC = m_bTOCUnderline
        End Get
        Set(ByVal Value As Boolean)
            m_bTOCUnderline = Value
        End Set
    End Property
    Public Property BoldHeaderPage() As Boolean
        Get
            BoldHeaderPage = m_bPageBold
        End Get
        Set(ByVal Value As Boolean)
            m_bPageBold = Value
        End Set
    End Property
    Public Property CapHeaderPage() As Boolean
        Get
            CapHeaderPage = m_bPageCap
        End Get
        Set(ByVal Value As Boolean)
            m_bPageCap = Value
        End Set
    End Property
    Public Property UnderlineHeaderPage() As Boolean
        Get
            UnderlineHeaderPage = m_bPageUnderline
        End Get
        Set(ByVal Value As Boolean)
            m_bPageUnderline = Value
        End Set
    End Property
    Public Property NumberStyle() As Short
        Get
            NumberStyle = m_iNumberStyle
        End Get
        Set(ByVal Value As Short)
            m_iNumberStyle = Value
        End Set
    End Property


    Public Property NumberPunctuation() As Short
        Get
            NumberPunctuation = m_iNumberPunctuation
        End Get
        Set(ByVal Value As Short)
            m_iNumberPunctuation = Value
        End Set
    End Property
    Public Property Exclusions() As String
        Get
            'returns a string of schemes that have been unchecked
            Dim i As Short
            Dim xTemp As String = ""

            '   cycle through xarray
            With m_arStyles
                For i = 0 To .GetUpperBound(0)
                    '           get if scheme should be excluded
                    If (.GetValue(i, 0) = 0) Then
                        '               if so, add to comma delimited string
                        xTemp = xTemp & .GetValue(i, 2) & ","
                    End If
                Next i
            End With

            If Len(xTemp) Then
                '       if list is not empty, add a preceding ','
                xTemp = "," & xTemp
            End If

            Exclusions = xTemp
        End Get
        Set(ByVal Value As String)
            'checks off the selected list
            Dim i As Short

            '   cycle through xarray
            With m_arStyles
                For i = 0 To .GetUpperBound(0)
                    '           if scheme is in exclusion string, uncheck
                    If InStr(Value, "," & .GetValue(i, 2) & ",") Then
                        '               if so, add to comma delimited string
                        .SetValue(0, i, 0)
                    End If
                Next i
            End With

        End Set
    End Property

    Public ReadOnly Property Inclusions() As String
        Get
            'returns the list of schemes to
            'use to create toc entries - if
            'user has specified that only marked
            'text should be used, return an empty
            'inclusions list
            Dim i As Short
            Dim xTemp As String = ""

            '   cycle through xarray
            With m_arStyles
                For i = 0 To .GetUpperBound(0)
                    '           get if scheme should be included
                    If .GetValue(i, 0) = 1 Then
                        '               if so, add to comma delimited string
                        xTemp = xTemp & .GetValue(i, 2) & ","
                    End If
                Next i
            End With

            If Len(xTemp) Then
                '       if list is not empty, add a preceding ','
                xTemp = "," & xTemp
            End If

            Inclusions = xTemp
        End Get
    End Property
    Property bStepIndent() As Boolean
        Get
            bStepIndent = m_bStepIndent
        End Get
        Set(ByVal Value As Boolean)
            m_bStepIndent = Value
        End Set
    End Property
    Public Property StyleExclusions() As String
        Get
            'returns a string of styles that have been unchecked
            Dim i As Short
            Dim xTemp As String = ""

            '   cycle through xarray
            With m_arDesignate
                For i = 0 To .GetUpperBound(0)
                    '           get if scheme should be excluded
                    If .GetValue(i, 2) <> "" Then
                        If (.GetValue(i, 0) = 0) Then
                            '                   if so, add to comma delimited string
                            xTemp = xTemp & .GetValue(i, 2) & ","
                        End If
                    Else
                        Exit For
                    End If
                Next i
            End With

            If Len(xTemp) Then
                '       if list is not empty, add a preceding ','
                xTemp = "," & xTemp
            End If

            StyleExclusions = xTemp
        End Get
        Set(ByVal Value As String)
            'checks off the selected list
            Dim i As Short

            '   cycle through xarray
            With m_arDesignate
                For i = 0 To .GetUpperBound(0)
                    '           if scheme is in exclusion string, uncheck
                    If .GetValue(i, 2) <> "" Then
                        If InStr(Value, "," & .GetValue(i, 2) & ",") Then
                            .SetValue(0, i, 0)
                        End If
                    Else
                        Exit For
                    End If
                Next i
            End With

        End Set
    End Property


    Public Property StyleInclusions() As String
        Get
            'returns the list of styles to
            'use to create toc entries
            Dim i As Short
            Dim xTemp As String = ""

            '   cycle through xarray
            With m_arDesignate
                For i = 0 To .GetUpperBound(0)
                    '           get if scheme should be included
                    If .GetValue(i, 2) <> "" Then
                        If .GetValue(i, 0) = 1 Then
                            '                   if so, add to comma delimited string
                            xTemp = xTemp & .GetValue(i, 2) & ","
                        End If
                    Else
                        Exit For
                    End If
                Next i
            End With

            If Len(xTemp) Then
                '       if list is not empty, add a preceding ','
                xTemp = "," & xTemp
            End If

            StyleInclusions = xTemp
        End Get
        Set(ByVal Value As String)
            'checks on the selected list
            Dim i As Short

            '   cycle through xarray
            With m_arDesignate
                For i = 0 To .GetUpperBound(0)
                    '           if scheme is in inclusion string, check
                    If .GetValue(i, 2) <> "" Then
                        If InStr(Value, "," & .GetValue(i, 2) & ",") Then
                            .SetValue(1, i, 0)
                        End If
                    Else
                        Exit For
                    End If
                Next i
            End With

        End Set
    End Property


    Public Property ContinuePageNumbering() As Boolean
        Get
            ContinuePageNumbering = m_bContinuePageNumbering
        End Get
        Set(ByVal Value As Boolean)
            m_bContinuePageNumbering = Value
        End Set
    End Property

    Private Sub btnEditHeaderFooter_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnEditHeaderFooter.Click
        Try
            Dim oForm = New frmHeaderFooterFrench()
            With oForm
                .chkPageBold.CheckState = System.Math.Abs(CInt(Me.BoldHeaderPage))
                .chkTOCBold.CheckState = System.Math.Abs(CInt(Me.BoldHeaderTOC))
                .chkPageCaps.CheckState = System.Math.Abs(CInt(Me.CapHeaderPage))
                .chkTOCCaps.CheckState = System.Math.Abs(CInt(Me.CapHeaderTOC))
                .chkPageUnderline.CheckState = System.Math.Abs(CInt(Me.UnderlineHeaderPage))
                .chkTOCUnderline.CheckState = System.Math.Abs(CInt(Me.UnderlineHeaderTOC))
                .NumberPunctuationDefault = Me.NumberPunctuation
                .NumberStyleDefault = Me.NumberStyle
                .chkContinue.CheckState = System.Math.Abs(CInt(Me.ContinuePageNumbering))

                .ShowDialog()

                If Not .Cancelled Then
                    Me.BoldHeaderPage = .chkPageBold.CheckState
                    Me.BoldHeaderTOC = .chkTOCBold.CheckState
                    Me.CapHeaderPage = .chkPageCaps.CheckState
                    Me.CapHeaderTOC = .chkTOCCaps.CheckState
                    Me.UnderlineHeaderPage = .chkPageUnderline.CheckState
                    Me.UnderlineHeaderTOC = .chkTOCUnderline.CheckState
                    'UPGRADE_WARNING: Couldn't resolve default property of object frmHeaderFooter.cmbPunctuation.SelectedItem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.NumberPunctuation = .cmbPunctuation.SelectedIndex
                    'UPGRADE_WARNING: Couldn't resolve default property of object frmHeaderFooter.cmbNumberStyle.SelectedItem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.NumberStyle = .cmbNumberStyle.SelectedIndex
                    Me.ContinuePageNumbering = .chkContinue.CheckState

                    '           reset current boilerplate
                    If .cbxType.Text <> "" Then
                        SetUserSetting("TOC", "CurrentBoilerplate", g_xBoilerplates(.cbxType.SelectedIndex, 1))
                    End If
                End If
            End With
            oForm.Close()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnSetDefaults_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnSetDefaults.Click
        Dim xValue As String

        Try
            'create from
            SetUserSetting("TOC", "CreateFromStyles", CStr(Me.chkStyles.CheckState))
            SetUserSetting("TOC", "CreateFromTCEntries", CStr(Me.chkTCEntries.CheckState))

            'levels
            SetUserSetting("TOC", "LevelStart", Trim(Me.cbxMinLevel.Text))
            SetUserSetting("TOC", "LevelEnd", Trim(Me.cbxMaxLevel.Text))

            'insertion location
            With Me.cbxInsertAt
                If (.Text <> mpTOCLocationAboveTOA) And (.Text <> mpTOCLocationAtCurrent) Then SetUserSetting("TOC", "DefaultLocation", .SelectedIndex)
            End With

            'other checkboxes
            SetUserSetting("TOC", "TOCDlgMark", CStr(Me.chkMark.CheckState))

            'heading definition
            If Me.cbxHeadingDef.SelectedIndex = 1 Then
                'period and 2 spaces was old default
                xValue = "0"
                '    ElseIf Me.cbxHeadingDef.SelectedItem = 2 Then
                '        'whole paragraph
                '        xValue = "1"
            Else
                'period and 1 space
                xValue = "3"
            End If
            SetUserSetting("TOC", "CreateFrom", xValue)

            CurWordApp.StatusBar = "Valeurs par défaut définies dans l'onglet principal"
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxMaxLevel_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxMaxLevel.ValueChanged
        Try
            If m_bLoaded Then m_bRefreshStyleList = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub cbxMinLevel_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxMinLevel.ValueChanged
        Try
            If m_bLoaded Then m_bRefreshStyleList = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxMinLevel_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxMinLevel.Leave
        Try
            If Me.cbxMinLevel.Text > Me.cbxMaxLevel.Text Then
                Const xMsg As String = "Niveau le plus bas doit être inférieur ou égal au niveau le plus élevé."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
                Me.cbxMinLevel.Text = CStr(1)
                Me.cbxMinLevel.Focus()
            ElseIf m_bRefreshStyleList Then
                LoadStylesArray()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    '    Private Sub cbxMinLevel_Mismatch(ByVal eventSender As System.Object, ByVal eventArgs As AxTrueDBList60.TrueDBComboEvents_MismatchEvent) Handles cbxMinLevel.Mismatch
    '        On Error GoTo ProcError
    '        CorrectTDBComboMismatch((Me.cbxMinLevel), eventArgs.Reposition)
    '        Exit Sub
    'ProcError:
    '        RaiseError("frmInsertTOC.cbxMinLevel_Mismatch")
    '        Exit Sub
    '    End Sub

    Private Sub cbxMaxLevel_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxMaxLevel.Leave
        Try
            If Me.cbxMinLevel.Text > Me.cbxMaxLevel.Text Then
                Const xMsg As String = "Niveau le plus élevé doit être supérieur ou égal au niveau le plus bas."
                MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
                Me.cbxMaxLevel.Text = CStr(9)
                Me.cbxMaxLevel.Focus()
            ElseIf m_bRefreshStyleList Then
                LoadStylesArray()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxScheduleStyles_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxScheduleStyles.ValueChanged
        Try
            If m_bLoaded Then m_bRefreshStyleList = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxScheduleStyles_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxScheduleStyles.Leave
        Try
            If m_bRefreshStyleList Then
                If Me.optIncludeSchemes.Checked Then
                    LoadStylesArray()
                ElseIf Me.optIncludeStyles.Checked And (Me.chkApplyTOC9.CheckState = 1) Then
                    SelectTOC9Style((Me.cbxScheduleStyles).Text)
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxTOCScheme_ValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxTOCScheme.ValueChanged
        Try
            Me.cmdEdit.Enabled = (Me.cbxTOCScheme.Text = "Personnalisé")
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkApplyTOC9_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkApplyTOC9.CheckStateChanged
        Try
            If m_bLoaded Then
                If Not Me.optIncludeStyles.Checked Then
                    '           refresh list
                    LoadStylesArray()

                    '           force designation option
                    If Me.chkApplyTOC9.CheckState = 1 Then
                        m_bNoJumpToAdvanced = True
                        Me.optIncludeStyles.Checked = True
                        m_bNoJumpToAdvanced = False
                    End If
                Else
                    '           designate TOC9 style for inclusion
                    SelectTOC9Style((Me.cbxScheduleStyles).Text)
                End If
            End If
            Me.cbxScheduleStyles.Enabled = (Me.chkApplyTOC9.CheckState = System.Windows.Forms.CheckState.Checked)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkMark_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMark.CheckStateChanged
        Try
            SetMarkControls()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkStyles_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkStyles.CheckStateChanged
        Try
            SetMarkControls()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkTCEntries_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTCEntries.CheckStateChanged
        Try
            SetMarkControls()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub SetMarkControls()
        'enable/disable and set value/caption of Mark checkbox and Heading Definition dropdown
        Dim xValue As String = ""

        If Me.chkStyles.CheckState = System.Windows.Forms.CheckState.Checked Then
            Me.chkMark.Text = "A&ppliquer séparateurs de style"
        Else
            Me.chkMark.Text = "A&ppliquer codes TM"
        End If

        With Me.cbxHeadingDef
            .Enabled = (Me.chkMark.CheckState = System.Windows.Forms.CheckState.Checked)
            '        If Me.chkMark Then
            '            If (Me.chkStyles = 1) And (.ListCount = 3) Then
            '                'whole paragraph option doesn't apply when marking with style separators -
            '                'switch before removing it
            '                If .ListIndex = 2 Then
            '                    xValue = GetUserSetting("TOC", "CreateFrom")
            '                    If xValue = "0" Then
            '                        'period and 2 spaces
            '                        .ListIndex = 1
            '                    Else
            '                        'period and 1 space
            '                        .ListIndex = 0
            '                    End If
            '                End If
            '
            '                .RemoveItem 2
            '            ElseIf (Me.chkStyles = 0) And (.ListCount = 2) Then
            '                'restore whole paragraph option for TC codes if necessary
            '                .AddItem "Whole paragraph"
            '            End If
            '        End If
        End With
    End Sub
    Private Sub cmdEdit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEdit.Click
        Try
            Dim bDefaultCustom As Boolean = False
            Dim bRet As Boolean = False
            bRet = EditCustomTOCScheme()
            If bRet Then
                '       user saved the edits to custom toc scheme

                '       set current toc scheme to
                '       custom if specified by user
                'UPGRADE_WARNING: Couldn't resolve default property of object bDefaultCustom. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                bDefaultCustom = bIniToBoolean(GetUserSetting("TOC", "DefaultToCustom"))
                'UPGRADE_WARNING: Couldn't resolve default property of object bDefaultCustom. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If bDefaultCustom Then
                    Me.cbxTOCScheme.SelectedIndex = 0
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Cancelled = True
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Hide()
            'System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            '   validate
            If bIsValid() Then
                Me.Cancelled = False
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Hide()
                'System.Windows.Forms.Application.DoEvents()
                SaveCurInput()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub lstStyles_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstStyles.Leave
        Try
            If m_bRefreshStyleList Then
                LoadStylesArray()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub optIncludeSchemes_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optIncludeSchemes.CheckedChanged
        Try
            Me.lstStyles.Enabled = eventSender.Checked
            If eventSender.Checked And m_bLoaded Then
                '           uncheck Apply TOC 9 - this will also reset defaults
                Me.chkApplyTOC9.Checked = System.Windows.Forms.CheckState.Unchecked
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub optIncludeStyles_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optIncludeStyles.CheckedChanged
        Try
            'Disable Level controls if checked
            Me.cbxMaxLevel.Enabled = Not eventSender.Checked
            Me.cbxMinLevel.Enabled = Not eventSender.Checked
            Me.lblIncludeLevels.Enabled = Not eventSender.Checked
            Me.lblThrough.Enabled = Not eventSender.Checked
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub frmInsertTOC_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim iNumSchemes As Short
        Dim xStyleSchemes() As String
        Dim xHeadingStyles As String
        Dim xItemLocations() As String
        Dim iNumStyleSchemes As Short
        Dim xUserDef As String
        Dim iBoilerplate As Short
        Dim xUserMin As String
        Dim xUserMax As String
        Dim xValue As String
        Dim iCurDefScheme As Short
        Dim xTOCField As String

        Try
            Me.Cancelled = True

            'adjust for scaling
            Dim sFactor As Single = LMP.Numbering.TOC.mpApplication.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnOK.Height = Me.btnOK.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
            End If

            'Set size at runtime, so all panels can be visible in design
            Me.Width = Me.pnlMain.Width + 14
            Me.CenterToParent()
            Me.tbtnMain.Dock = System.Windows.Forms.DockStyle.Fill
            Me.tbtnAdvanced.Dock = System.Windows.Forms.DockStyle.None
            Me.tbtnOptions.Dock = System.Windows.Forms.DockStyle.None
            Me.tbtnMain.Checked = True
            If Not Me.InsertAsField Then
                Me.fraMark.Visible = False
                Me.cmbTextStyles.Visible = True
                Me.optIncludeAll.Visible = False
                Me.chkStyles.Top = Me.cmbTextStyles.Top
                Me.chkStyles.Left = Me.cmbTextStyles.Left
                Me.chkTCEntries.Left = Me.cmbTextStyles.Left
                Me.cmbTextStyles.Top = Me.cmbTextStyles.Top + Me.cmbTextStyles.Height + 2
                Me.chkTCEntries.Top = Me.cmbTextStyles.Top + Me.cmbTextStyles.Height + 2

                Me.chkTCEntries.Top = Me.cmbTextStyles.Top + Me.cmbTextStyles.Height + 12
                Me.lblInsertAt.Left = Me.lblCreateFrom.Left + 4
                Me.cbxInsertAt.Top = Me.chkTCEntries.Top + Me.chkTCEntries.Height + 30
                Me.cbxInsertAt.Width = Me.cbxMaxLevel.Left + Me.cbxMaxLevel.Width - Me.cbxInsertAt.Left
                Me.lblInsertAt.Top = Me.cbxInsertAt.Top + 4
                Me.fraContent.Height = Me.fraSchemes.Top + Me.fraSchemes.Height - Me.fraContent.Top
                Me.btnSetDefaults.Top = Me.fraContent.Top + Me.fraContent.Height - Me.btnSetDefaults.Height - 10
                Me.btnSetDefaults.Left = Me.fraContent.Left + Me.fraContent.Width - Me.btnSetDefaults.Width - 10

                'GLOG 8728 (dm) - hide hyperlinks checkbox
                Me.chkHyperlinks.Visible = False
                Me.cmdEdit.Top = Me.cmdEdit.Top - (Me.btnEditHeaderFooter.Top - Me.chkHyperlinks.Top)
                Me.btnEditHeaderFooter.Top = Me.chkHyperlinks.Top
            End If
            '   load Min/Max levels
            m_arLevels = New Object(8, 1) {}
            For i = 0 To 8
                m_arLevels.SetValue(i + 1, i, 0)
                m_arLevels.SetValue(i + 1, i, 1)
            Next i
            Me.cbxMinLevel.SetList(m_arLevels)
            Me.cbxMaxLevel.SetList(m_arLevels)

            '   load insert at
            m_iTOCMarker = iTOCExists(CurWordApp.ActiveDocument)
            If m_iTOCMarker <> mpTOCExistsMarker.mpTOCExistsMarker_None Then
                m_arInsertAt = New Object(0, 1) {}
                m_arInsertAt.SetValue(mpTOCLocationAtCurrent, 0, 0)
                m_arInsertAt.SetValue(mpTOCLocationAtCurrent, 0, 1)

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxInsertAt.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cbxInsertAt.SetList(m_arInsertAt)
            Else
                If bTOAExists(CurWordApp.ActiveDocument) Then
                    m_arInsertAt = New Object(UBound(g_xTOCLocations) + 1, 1) {}
                    For i = 0 To UBound(g_xTOCLocations)
                        m_arInsertAt.SetValue(g_xTOCLocations(i), i, 0)
                        m_arInsertAt.SetValue(g_xTOCLocations(i), i, 1)
                    Next i
                    '            add and default to "above TOA"
                    m_arInsertAt.SetValue(mpTOCLocationAboveTOA, i, 0)
                    m_arInsertAt.SetValue(mpTOCLocationAboveTOA, i, 1)

                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxInsertAt.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.cbxInsertAt.SetList(m_arInsertAt)
                Else
                    m_arInsertAt = New Object(UBound(g_xTOCLocations), 1) {}
                    For i = 0 To UBound(g_xTOCLocations)
                        m_arInsertAt.SetValue(g_xTOCLocations(i), i, 0)
                        m_arInsertAt.SetValue(g_xTOCLocations(i), i, 1)
                    Next i

                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxInsertAt.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.cbxInsertAt.SetList(m_arInsertAt)
                End If

                '       sync current boilerplate with default - this should
                '       ultimately be a property of the form
                Try
                    iBoilerplate = CShort(GetUserSetting("TOC", "DefaultBoilerplateIndex"))
                Catch
                End Try
                If g_xBoilerplates(iBoilerplate, 1) <> "" Then
                    Try
                        SetUserSetting("TOC", "CurrentBoilerplate", g_xBoilerplates(iBoilerplate, 1))
                    Catch
                    End Try
                End If
            End If

            iNumSchemes = iGetTOCSchemes(m_xSchemes)

            '   load TOC schemes
            ReDim m_arTOCScheme(UBound(m_xSchemes) + 1, 1)

            m_arTOCScheme(0, 0) = "Personnalisé"
            m_arTOCScheme(0, 1) = "Personnalisé"
            For i = 0 To UBound(m_xSchemes)
                m_arTOCScheme(i + 1, 0) = m_xSchemes(i, 0)
                m_arTOCScheme(i + 1, 1) = m_xSchemes(i, 1)
            Next i

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxTOCScheme.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cbxTOCScheme.SetList(m_arTOCScheme)

            If Me.InsertAsField Then
                '   load Heading Def
                m_arHeadingDef = New Object(,) {{"Première phrase (point et au moins 1 espace)", "0"}, {"Première phrase (point et au moins 2 espaces)", "1"}}

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxHeadingDef.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cbxHeadingDef.SetList(m_arHeadingDef)
            Else
                '   load Heading Def
                m_arHeadingDef = New Object(,) {{"Première phrase (point et au moins 1 espace)", "0"},
                                                {"Première phrase (point et au moins 2 espaces)", "1"},
                                                {"Paragraphe entier", "3"}}

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxHeadingDef.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cmbTextStyles.SetList(m_arHeadingDef)

            End If

            '   load schedule styles
            If g_xScheduleStyles(0) = "" Then
                Me.chkApplyTOC9.Enabled = False
                Me.cbxScheduleStyles.Enabled = False
                'Me.fraTOCBody.Height = TwipsToPixelsY(1000)
                'Me.fraSchemes.Top = TwipsToPixelsY(2185)
            Else
                m_arScheduleStyles = New Object(UBound(g_xScheduleStyles), 1) {}

                For i = 0 To UBound(g_xScheduleStyles)
                    m_arScheduleStyles.SetValue(g_xScheduleStyles(i), i, 0)
                    m_arScheduleStyles.SetValue(g_xScheduleStyles(i), i, 1)
                Next i

                'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxScheduleStyles.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.cbxScheduleStyles.SetList(m_arScheduleStyles)
            End If

            With Me
                '"create from" checkboxes
                xValue = GetUserSetting("TOC", "CreateFromStyles")
                If xValue <> "" Then .chkStyles.CheckState = Val(xValue)
                xValue = GetUserSetting("TOC", "CreateFromTCEntries")
                If xValue <> "" Then .chkTCEntries.CheckState = Val(xValue)
                If .chkStyles.CheckState + .chkTCEntries.CheckState = 0 Then .chkStyles.CheckState = System.Windows.Forms.CheckState.Checked

                '       load schemes in use
                xarGetSchemes()

                'get mark checkbox default
                If m_iTOCMarker = mpTOCExistsMarker.mpTOCExistsMarker_None Then '9.9.5005
                    xValue = GetUserSetting("TOC", "TOCDlgMark")
                    Me.chkMark.CheckState = System.Math.Abs(CInt(xValue = "1"))
                End If

                'get defaults for Header/Footer
                'GLOG 15801 (dm) - set defaults as they were in generic NumTOC.ini
                xValue = GetUserSetting("TOC", "HeaderTOCBold")
                If xValue <> "" Then
                    Try
                        .BoldHeaderTOC = CBool(xValue)
                    Catch
                    End Try
                Else
                    .BoldHeaderTOC = True
                End If

                xValue = GetUserSetting("TOC", "HeaderTOCCaps")
                If xValue <> "" Then
                    Try
                        .CapHeaderTOC = CBool(xValue)
                    Catch
                    End Try
                Else
                    .CapHeaderTOC = True
                End If

                xValue = GetUserSetting("TOC", "HeaderTOCUnderline")
                If xValue <> "" Then
                    Try
                        .UnderlineHeaderTOC = CBool(xValue)
                    Catch
                    End Try
                End If

                xValue = GetUserSetting("TOC", "HeaderPageBold")
                If xValue <> "" Then
                    Try
                        .BoldHeaderPage = CBool(xValue)
                    Catch
                    End Try
                Else
                    .BoldHeaderPage = True
                End If

                xValue = GetUserSetting("TOC", "HeaderPageCaps")
                If xValue <> "" Then
                    Try
                        .CapHeaderPage = CBool(xValue)
                    Catch
                    End Try
                End If

                xValue = GetUserSetting("TOC", "HeaderPageUnderline")
                If xValue <> "" Then
                    Try
                        .UnderlineHeaderPage = CBool(xValue)
                    Catch
                    End Try
                End If

                xValue = GetUserSetting("TOC", "PageNoStyle")
                If xValue <> "" Then
                    Try
                        .NumberStyle = CShort(xValue)
                    Catch
                    End Try
                Else
                    .NumberStyle = 2
                End If

                xValue = GetUserSetting("TOC", "PageNoPunctuation")
                If xValue <> "" Then
                    Try
                        .NumberPunctuation = CShort(xValue)
                    Catch
                    End Try
                Else
                    .NumberPunctuation = 1
                End If

                xValue = GetUserSetting("TOC", "ContinuePageNoFromPrevious")
                If xValue <> "" Then
                    Try
                        .ContinuePageNumbering = CBool(xValue)
                    Catch
                    End Try
                End If

                .btnEditHeaderFooter.Visible = g_bAllowHeaderFooterEdit

                '9.9.5002 - new ini setting for Include default - set only
                'if not already set for TOC9 style functionality
                If Not .optIncludeStyles.Checked Then
                    If g_iIncludeInTOCDefault = 1 Then
                        .optIncludeStyles.Checked = True
                    ElseIf g_iIncludeInTOCDefault = 2 Then
                        .optIncludeAll.Checked = True
                    End If
                End If
            End With
            '   levels - first try user ini, then firm ini
            xUserMin = GetUserSetting("TOC", "LevelStart")
            If xUserMin <> "" Then
                Try
                    Me.cbxMinLevel.Text = xUserMin
                Catch
                End Try
                If Me.cbxMinLevel.Text = "" Then Me.cbxMinLevel.Text = CStr(g_iDefaultLevelStart)
            Else
                Me.cbxMinLevel.Text = CStr(g_iDefaultLevelStart)
            End If

            'CODE MOVED FROM Form_Activate
            xUserMax = GetUserSetting("TOC", "LevelEnd")
            If xUserMax <> "" Then
                Try
                    Me.cbxMaxLevel.SelectedIndex = CInt(xUserMax) - 1
                Catch
                End Try
                If Me.cbxMaxLevel.Text = "" Then Me.cbxMaxLevel.SelectedIndex = g_iDefaultLevelEnd - 1
            Else
                Me.cbxMaxLevel.SelectedIndex = g_iDefaultLevelEnd - 1
            End If

            If m_iTOCMarker <> mpTOCExistsMarker.mpTOCExistsMarker_None Then
                Me.cbxInsertAt.SelectedIndex = 0
                Me.cbxInsertAt.Enabled = False

                Me.lblInsertAt.Enabled = False
                Me.btnEditHeaderFooter.Enabled = False

                If m_iTOCMarker <> mpTOCExistsMarker.mpTOCExistsMarker_Field Then
                    '       there's an existing MacPac TOC;
                    '       default to keep current TOC styles
                    Me.optTOCStylesKeep.Checked = True
                End If
            Else
                xUserDef = GetUserSetting("TOC", "DefaultLocation")
                If bTOAExists(CurWordApp.ActiveDocument) Then
                    Me.cbxInsertAt.SelectedIndex = Me.cbxInsertAt.Items.Count - 1
                Else
                    If (xUserDef = "") Or (Val(xUserDef) > Me.cbxInsertAt.Items.Count - 1) Then
                        'GLOG 15801 (dm) - default to end of doc
                        If g_xTOCLocations.Contains(mpTOCLocationEOFFrench) Then
                            Me.cbxInsertAt.SelectedValue = mpTOCLocationEOFFrench
                        Else
                            Me.cbxInsertAt.SelectedIndex = 0
                        End If
                    Else
                        Me.cbxInsertAt.SelectedIndex = CInt(Val(xUserDef))
                    End If
                End If
            End If

            With Me.cbxTOCScheme
                If GetUserSetting("TOC", "DefaultToCustom") = "1" Then
                    .SelectedIndex = 0
                Else
                    Try
                        iCurDefScheme = CShort(iGetCurDefTOCScheme())
                        xTOCField = GetTOCField(iCurDefScheme, mpTOCRecordFields.mpTOCRecField_Alias, mpSchemeTypes.mpSchemeType_TOC)
                    Catch
                    End Try
                    If xTOCField <> "" Then
                        Try
                            Me.cbxTOCScheme.Value = xTOCField
                        Catch
                        End Try
                    Else
                        .SelectedIndex = 0
                    End If
                End If
            End With

            '   initialize to first in list
            If g_xScheduleStyles(0) <> "" Then
                Me.cbxScheduleStyles.SelectedIndex = 0
            End If

            'set default checkbox value
            If g_bApplyTOC9StyleDefault Then
                Me.chkApplyTOC9.CheckState = System.Windows.Forms.CheckState.Checked
                Me.optIncludeStyles.Checked = True
            End If

            '   get heading definition default
            xValue = GetUserSetting("TOC", "CreateFrom")
            If Me.InsertAsField Then
                If xValue = "0" Then
                    'period and 2 spaces
                    Me.cbxHeadingDef.SelectedIndex = 1
                    '        ElseIf xValue = "1" Then
                    '            'whole paragraph
                    '            .cbxHeadingDef.ListIndex = 2
                Else
                    '10/1/12 - period and 1 space is now the default
                    Me.cbxHeadingDef.SelectedIndex = 0
                End If
            Else
                If xValue = "0" Then
                    'period and 2 spaces
                    Me.cmbTextStyles.SelectedIndex = 1
                ElseIf xValue = "1" Then
                    'whole paragraph
                    Me.cmbTextStyles.SelectedIndex = 2
                Else
                    '10/1/12 - period and 1 space is now the default
                    Me.cmbTextStyles.SelectedIndex = 0
                End If
            End If
            'GLOG 8728: Native TOC does not include direct formatting when inserted as Field in Word 2016
            If InStr(CurWordApp.Version, "16") = 1 Then
                Me.chkApplyManualFormatsToTOC.Visible = False
                Me.chkApplyManualFormatsToTOC.Checked = False
                'Close up gap
                Me.chkApplyTOC9.Top = Me.chkApplyTOC9.Top - (LMP.Numbering.TOC.mpApplication.GetScalingFactor() * 16)
                Me.cbxScheduleStyles.Top = Me.cbxScheduleStyles.Top - (LMP.Numbering.TOC.mpApplication.GetScalingFactor() * 16)
            End If


            GetPrevInput()

            '   load available styles
            'System.Windows.Forms.Application.DoEvents()
            LoadStylesArray()

            Try
                Me.StyleExclusions = CurWordApp.ActiveDocument.Variables.Item("StyleExclusions").Value
            Catch
            End Try
            Try
                Me.StyleInclusions = CurWordApp.ActiveDocument.Variables.Item("StyleInclusions").Value
            Catch
            End Try

            'ensure correct label and that heading def disabled if not marking
            SetMarkControls()

            'this was added because some of the control events are firing before the Load event
            m_bLoaded = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub xarGetSchemes()
        Dim i As Short
        Dim iNumSchemes As Short
        Dim xDocSchemes(,) As String
        Dim bHSchemeExists As Boolean
        Dim iSchedule As Short
        Dim bFirstRun As Boolean
        Dim xExclusions As String = ""

        bFirstRun = (iTOCExists(CurWordApp.ActiveDocument) = mpTOCExistsMarker.mpTOCExistsMarker_None)
        ReDim xDocSchemes(-1, 2)
        '   get numbering schemes in active doc
        iNumSchemes = iGetSchemes(xDocSchemes, mpSchemeTypes.mpSchemeType_Document)

        'GLOG 8599 - get previous exclusions
        If Not bFirstRun Then
            On Error Resume Next
            xExclusions = CurWordApp.ActiveDocument.Variables.Item("Exclusions").Value
            On Error GoTo 0
        End If

        '   check if any schedule styles are listed in ini
        '    If g_xScheduleStyles(0) <> "" Then
        '        iSchedule = 1
        '    End If

        '   get if there is a heading scheme in the doc
        bHSchemeExists = (Len(xHeadingScheme()) > 0)

        '   there should be one less row if there is a MacPac scheme
        '   that uses heading styles
        m_arStyles = New Object(iNumSchemes + iSchedule + IIf(bHSchemeExists, -1, 0), 2) {}
        With m_arStyles

            '       add schedule option only if at least one style is listed in ini
            If iSchedule = 1 Then
                .SetValue("Schedule", iNumSchemes + 1, 2)
                .SetValue("Schedule/Exhibit Styles", iNumSchemes + 1, 1)
                .SetValue(System.Math.Abs(CInt(g_bIncludeSchedule Or Not bFirstRun)), iNumSchemes + 1, 0)
            End If

            ''       add other styles option
            '        .Value(iNumSchemes + 1, 2) = "Other"
            '        .Value(iNumSchemes + 1, 1) = "Other Outline Level Styles"
            '        .Value(iNumSchemes + 1, 0) = Abs(Not bFirstRun)

            '       get all numbering schemes in document
            For i = 1 To iNumSchemes
                If Not bHSchemeExists Then
                    .SetValue(xDocSchemes(i - 1, 0), i, 2)
                    .SetValue(xDocSchemes(i - 1, 1), i, 1)
                    If .GetValue(i, 1) = "" Then
                        '               if scheme props are missing for some reason,
                        '               display style root rather than an empty string
                        .SetValue(xGetStyleRoot(.GetValue(i, 2)), i, 1)
                    End If
                    If InStr(xExclusions, xDocSchemes(i - 1, 0)) Then
                        'GLOG 8599
                        .SetValue(0, i, 0)
                    Else
                        .SetValue(1, i, 0)
                    End If
                Else
                    'GLOG 8598 - added branch
                    .SetValue(xDocSchemes(i - 1, 0), i - 1, 2)
                    .SetValue(xDocSchemes(i - 1, 1), i - 1, 1)
                    If .GetValue(i - 1, 1) = "" Then
                        '               if scheme props are missing for some reason,
                        '               display style root rather than an empty string
                        .SetValue(xGetStyleRoot(.GetValue(i - 1, 2)), i - 1, 1)
                    End If
                    If bIsHeadingScheme(.GetValue(i - 1, 2)) Then
                        .SetValue("Styles Titre Word (" & .GetValue(i - 1, 1) & ")", i - 1, 1)
                    End If
                    If InStr(xExclusions, xDocSchemes(i - 1, 0)) Then
                        'GLOG 8599
                        .SetValue(0, i - 1, 0)
                    Else
                        .SetValue(1, i - 1, 0)
                    End If
                End If
            Next i

            If Not bHSchemeExists Then
                '           if there is no MacPac scheme
                '           that uses heading styles...
                '           add heading styles
                .SetValue("Heading", 0, 2)
                .SetValue("Styles Titre Word", 0, 1)
                'only default on if no schemes or previously selected (GLOG 8599)
                Dim bOn As Boolean
                bOn = ((InStr(xExclusions, ",Heading") = 0) And Not bFirstRun)
                .SetValue(System.Math.Abs(CInt(iNumSchemes = 0 Or bOn)), 0, 0)
            End If
        End With
        '   load list
        lstStyles.Items.Clear()
        For i = 0 To m_arStyles.GetUpperBound(0)
            Me.lstStyles.Items.Add(m_arStyles.GetValue(i, 1), m_arStyles.GetValue(i, 0) = 1)
        Next

    End Sub

    Private Function LoadStylesArray() As Object
        Dim styP As Word.Style
        Dim i As Short
        Dim iCount As Short
        Dim iLevel As Short
        Dim iPos As Short
        Dim xScheme As String = ""
        Dim bOn As Boolean
        Dim bIncludeHScheme As Boolean
        Dim xHScheme As String = ""
        Dim xInclusions As String = ""
        Dim xBase As String = ""
        Dim arTemp As Array
        Dim xStyleInclusions As String = ""

        m_bRefreshStyleList = True
        arTemp = New Object(1000, 2) {}

        'GLOG 8599 (dm) - if styles designated individually, check for previous inclusion list
        If Me.optIncludeStyles.Checked Then
            On Error Resume Next
            xStyleInclusions = CurWordApp.ActiveDocument.Variables.Item("StyleInclusions").Value
            On Error GoTo 0
        End If

        '   get inclusions from Options tab
        xInclusions = UCase(Me.Inclusions)

        '   get scheme using Word Heading styles and whether to include
        xHScheme = UCase(xHeadingScheme())
        If xHScheme <> "" Then
            bIncludeHScheme = InStr(xInclusions, "," & xHScheme & ",")
        End If

        For Each styP In CurWordApp.ActiveDocument.Styles
            bOn = False
            With styP
                If .Type = Microsoft.Office.Interop.Word.WdStyleType.wdStyleTypeParagraph Then
                    '               get outline level;
                    '               retrieving outline level will make style "in use",
                    '               so only do this if style is already in use; use
                    '               different method to check Word Heading styles,
                    '               which should be listed in any case
                    iLevel = iGetWordHeadingLevel(.NameLocal)
                    If iLevel = 0 Then
                        If .InUse Then
                            iLevel = .ParagraphFormat.OutlineLevel
                        Else
                            iLevel = Microsoft.Office.Interop.Word.WdOutlineLevel.wdOutlineLevelBodyText
                        End If
                    End If

                    '               strip alias
                    xBase = StripStyleAlias(.NameLocal)

                    If iLevel <> Microsoft.Office.Interop.Word.WdOutlineLevel.wdOutlineLevelBodyText Then
                        '                   it's an outline level style
                        iPos = InStr(UCase(xBase), "_L")
                        If iPos Then
                            '                       it's a MacPac numbering style
                            xScheme = "ZZMP" & UCase(Mid(xBase, 1, iPos - 1))
                            '                       if scheme uses Word Heading styles, do not list
                            '                       proprietary style
                            If xScheme = xHScheme Then GoTo labNextSty
                        End If

                        '                   add style to list
                        arTemp.SetValue(xBase, iCount, 2)
                        arTemp.SetValue(.NameLocal, iCount, 1)

                        '                   default style to ON if TOC 9 style or if level is in designated range
                        '                   and scheme is on inclusion list
                        If (xBase = Me.cbxScheduleStyles.Text) And (Me.chkApplyTOC9.CheckState = 1) Then
                            '                       TOC9 Style
                            bOn = True
                        ElseIf (iLevel >= Val(Me.cbxMinLevel.Text)) And (iLevel <= Val(Me.cbxMaxLevel.Text)) Then
                            If xStyleInclusions <> "" Then
                                'GLOG 8599 (dm)
                                bOn = InStr(xStyleInclusions, "," & .NameLocal)
                            ElseIf bIsHeadingStyle(.NameLocal) Then
                                '                           Word Heading style
                                bOn = bIncludeHScheme Or InStr(xInclusions, ",HEADING,")
                            ElseIf iPos Then
                                '                           MacPac numbering style
                                bOn = InStr(xInclusions, "," & xScheme & ",")
                                '                        ElseIf bIsScheduleStyle(xBase) Then
                                ''                           Schedule/Exhibit style
                                '                            bOn = InStr(xInclusions, ",SCHEDULE,")
                            End If
                        End If
                        arTemp.SetValue(System.Math.Abs(CInt(bOn)), iCount, 0)

                        '                   increment count
                        iCount = iCount + 1
                    ElseIf bIsScheduleStyle(xBase) Then
                        '                   schedule/exhibit styles may not be defined as outline level
                        arTemp.SetValue(xBase, iCount, 2)
                        arTemp.SetValue(.NameLocal, iCount, 1)

                        ''                   default style to ON if level is in designated range
                        ''                   and scheme is on inclusion list
                        '                    If (.NameLocal = Me.cbxScheduleStyles) And _
                        ''                            (Me.chkApplyTOC9 = 1) Then
                        ''                       designated for inclusion as TOC 9 - always in range
                        '                        iLevel = Val(Me.cbxMinLevel)
                        '                    Else
                        ''                       treat others as level one
                        '                        iLevel = 1
                        '                    End If
                        '
                        '                    If (iLevel >= Val(Me.cbxMinLevel)) And _
                        ''                            (iLevel <= Val(Me.cbxMaxLevel)) Then
                        '                        xarTemp.Value(iCount, 0) = Abs(InStr(xInclusions, _
                        ''                            ",SCHEDULE,") <> 0)
                        '                    End If

                        '                   default TOC9 style ON, others OFF
                        arTemp.SetValue(System.Math.Abs(CInt((xBase = Me.cbxScheduleStyles.Text) And (Me.chkApplyTOC9.CheckState = 1))), iCount, 0)

                        '                   increment count
                        iCount = iCount + 1
                    End If
                End If
            End With
labNextSty:
        Next styP

        '   dim array to actual size
        m_arDesignate = New Object(iCount - 1, 2) {}

        For i = 0 To iCount - 1
            m_arDesignate.SetValue(arTemp.GetValue(i, 0), i, 0)
            m_arDesignate.SetValue(arTemp.GetValue(i, 1), i, 1)
            m_arDesignate.SetValue(arTemp.GetValue(i, 2), i, 2)
        Next i
        lstDesignate.Items.Clear()
        '   load list
        For i = 0 To m_arDesignate.GetUpperBound(0)
            Me.lstDesignate.Items.Add(m_arDesignate.GetValue(i, 1), m_arDesignate.GetValue(i, 0) = 1)
        Next
        m_bRefreshStyleList = False
        Return m_arDesignate
    End Function

    Private Sub lstStyles_ItemCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ItemCheckEventArgs) Handles lstStyles.ItemCheck
        Dim lBkmk As Integer

        Try
            lBkmk = eventArgs.Index

            If eventArgs.NewValue = System.Windows.Forms.CheckState.Checked Then
                m_arStyles.SetValue(1, lBkmk, 0)
            Else
                m_arStyles.SetValue(0, lBkmk, 0)
            End If
            If m_bLoaded Then m_bRefreshStyleList = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub SaveCurInput()
        With CurWordApp.ActiveDocument.Variables
            .Item("cbxMinLevel").Value = Me.cbxMinLevel.Text
            .Item("cbxMaxLevel").Value = Me.cbxMaxLevel.Text

            If Me.InsertAsField Then
                If Me.cbxHeadingDef.SelectedIndex = 1 Then
                    'period and 2 spaces was old default
                    .Item("optCreateFrom").Value = "0"
                    '        ElseIf Me.cbxHeadingDef.ListIndex = 2 Then
                    '            'whole paragraph
                    '            .Item("optCreateFrom") = "1"
                Else
                    'period and 1 space
                    .Item("optCreateFrom").Value = "3"
                End If
            Else
                If Me.cmbTextStyles.SelectedIndex = 1 Then
                    'period and 2 spaces was old default
                    .Item("optCreateFrom").Value = "0"
                ElseIf Me.cmbTextStyles.SelectedIndex = 2 Then
                    'whole paragraph
                    .Item("optCreateFrom").Value = "1"
                Else
                    'period and 1 space
                    .Item("optCreateFrom").Value = "3"
                End If
            End If

            If Me.optIncludeSchemes.Checked Then
                .Item("optInclude").Value = CStr(0)
            ElseIf Me.optIncludeStyles.Checked Then
                .Item("optInclude").Value = CStr(1)
            Else
                .Item("optInclude").Value = CStr(2)
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxTOCScheme.SelectedItem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            .Item("cbxTOCScheme").Value = Me.cbxTOCScheme.SelectedIndex
            .Item("chkApplyManualFormatsToTOC").Value = CStr(Me.chkApplyManualFormatsToTOC.CheckState)
            .Item("chkStyles").Value = CStr(Me.chkStyles.CheckState)
            .Item("chkTCEntries").Value = CStr(Me.chkTCEntries.CheckState)
            .Item("Exclusions").Value = Me.Exclusions
            .Item("StyleExclusions").Value = Me.StyleExclusions
            .Item("StyleInclusions").Value = Me.StyleInclusions
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxScheduleStyles.SelectedItem. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            .Item("cbxScheduleStyles").Value = Me.cbxScheduleStyles.SelectedIndex
            .Item("chkApplyTOC9").Value = CStr(Me.chkApplyTOC9.CheckState)
            .Item("chkHyperlinks").Value = CStr(Me.chkHyperlinks.CheckState)
            .Item("chkTwoColumn").Value = CStr(Me.chkTwoColumn.CheckState)
            .Item("chkMark").Value = CStr(Me.chkMark.CheckState)
        End With
    End Sub

    Private Function GetPrevInput() As Object
        Dim xValue As String

        With CurWordApp.ActiveDocument.Variables
            On Error Resume Next
            Me.cbxMinLevel.SelectedIndex = CInt(.Item("cbxMinLevel").Value) - 1
            Me.cbxMaxLevel.SelectedIndex = CInt(.Item("cbxMaxLevel").Value) - 1

            xValue = .Item("optCreateFrom").Value
            If Me.InsertAsField Then
                If xValue <> "" Then
                    If xValue = "0" Then
                        Me.cbxHeadingDef.SelectedIndex = 1
                    Else
                        Me.cbxHeadingDef.SelectedIndex = 0
                    End If
                End If
            Else
                If xValue <> "" Then
                    If xValue = "0" Then
                        Me.cmbTextStyles.SelectedIndex = 1
                    ElseIf xValue = "1" Then
                        Me.cmbTextStyles.SelectedIndex = 2
                    Else
                        Me.cmbTextStyles.SelectedIndex = 0
                    End If
                End If

            End If

            xValue = ""
            xValue = .Item("optInclude").Value
            If xValue <> "" Then
                If xValue = "0" Then
                    Me.optIncludeSchemes.Checked = True
                ElseIf xValue = "1" Then
                    Me.optIncludeStyles.Checked = True
                Else
                    Me.optIncludeAll.Checked = True
                End If
            End If

            Me.cbxTOCScheme.SelectedIndex = CInt(.Item("cbxTOCScheme").Value)
            Me.chkApplyManualFormatsToTOC.CheckState = CShort(.Item("chkApplyManualFormatsToTOC").Value)
            Me.chkStyles.CheckState = CShort(.Item("chkStyles").Value)
            Me.chkTCEntries.CheckState = CShort(.Item("chkTCEntries").Value)
            Me.chkTwoColumn.CheckState = CShort(.Item("chkTwoColumn").Value)
            Me.Exclusions = .Item("Exclusions").Value
            Me.cbxScheduleStyles.SelectedIndex = CInt(.Item("cbxScheduleStyles").Value)
            Me.chkApplyTOC9.CheckState = CShort(.Item("chkApplyTOC9").Value)
            '        Me.chkMark = .Item("chkMark") '9.9.5005

            'ignore previous hyperlinks value if existing TOC is text
            If CurWordApp.ActiveDocument.TablesOfContents.Count > 0 Then Me.chkHyperlinks.CheckState = CShort(.Item("chkHyperlinks").Value)
        End With
    End Function

    Function bIsValid() As Boolean
        'returns TRUE if user choices in
        'dlg can produce a toc with entries
        Dim xMsg As String

        bIsValid = False
        If Me.chkStyles.CheckState + Me.chkTCEntries.CheckState = 0 Then
            '       at least one must be selected
            xMsg = "La Table des matières ne peut pas être générée selon votre sélection.  " & _
                vbCr & "Veuillez préciser de créer la TM à partir de styles, d'entrées de table ou les deux."
            MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
            On Error Resume Next
            tbtnOptions.Checked = True
            Me.chkStyles.Focus()
            Exit Function
        ElseIf Me.chkStyles.CheckState And Me.optIncludeSchemes.Checked And Me.Inclusions = "" Then
            '       user specified 'styles', but no styles have been included
            xMsg = "Aucun style n'a été sélectionné pour être ajouté dans la TM.  " & _
                   vbCr & "Sélectionnez au moins un thème parmi les styles dans l'onglet 'Options'."
            MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
            On Error Resume Next
            tbtnOptions.Checked = True
            Me.lstStyles.Focus()
            Exit Function
        ElseIf Me.chkStyles.CheckState And Me.optIncludeStyles.Checked And Me.StyleInclusions = "" Then
            '       user specified 'styles', but no styles have been included
            xMsg = "Aucun style n'a été sélectionné pour être ajouté dans la TM.  " & _
                   vbCr & "Sélectionnez au moins un style dans l'onglet 'Avancé'."
            MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
            On Error Resume Next
            tbtnAdvanced.Checked = True
            Me.lstDesignate.Focus()
            Exit Function
        End If
        bIsValid = True
    End Function

    Private Sub lstDesignate_ItemCheck(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ItemCheckEventArgs) Handles lstDesignate.ItemCheck
        Dim lBkmk As Integer

        Try
            If m_bRefreshStyleList Then Exit Sub

            lBkmk = eventArgs.Index

            If eventArgs.NewValue = System.Windows.Forms.CheckState.Checked Then
                m_arDesignate.SetValue(1, lBkmk, 0)
            Else
                m_arDesignate.SetValue(0, lBkmk, 0)
            End If
            Me.optIncludeStyles.Checked = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub SelectTOC9Style(ByRef xTOC9Style As String)
        'ensures that TOC 9 style is selected
        Dim i As Short

        With m_arDesignate
            For i = 0 To .GetUpperBound(0)
                If .GetValue(i, 2) = xTOC9Style Then
                    .SetValue(1, i, 0)
                    Me.lstDesignate.SetItemChecked(i, True)
                End If
            Next i
        End With
        m_bRefreshStyleList = False
    End Sub

    'UPGRADE_WARNING: Event optTOCStyles.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub optTOCStylesUpdate_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optTOCStylesUpdate.CheckedChanged
        Try
            Me.cbxTOCScheme.Enabled = eventSender.Checked
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnMain_CheckedChanged(sender As Object, e As EventArgs) Handles tbtnMain.CheckedChanged
        Try
            If tbtnMain.Checked Then
                Me.tbtnMain.BackColor = Drawing.SystemColors.ControlDark
                Me.pnlOptions.Visible = False
                Me.pnlAdvanced.Visible = False
                'Me.pnlOptions.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlAdvanced.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
                Me.tbtnMain.Checked = True
                Me.tbtnOptions.Checked = False
                Me.tbtnAdvanced.Checked = False
                If Me.cbxMinLevel.Enabled Then
                    Me.cbxMinLevel.Focus()
                Else
                    Me.chkStyles.Focus()
                End If
                Me.pnlMain.Visible = True
                System.Windows.Forms.Application.DoEvents()
            Else
                Me.tbtnMain.BackColor = Drawing.SystemColors.ButtonFace
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnOptions_CheckedChanged(sender As Object, e As EventArgs) Handles tbtnOptions.CheckedChanged
        Try
            If tbtnOptions.Checked Then
                Me.tbtnOptions.BackColor = Drawing.SystemColors.ButtonHighlight
                Me.pnlMain.Visible = False
                Me.pnlAdvanced.Visible = False
                'Me.pnlMain.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlAdvanced.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlOptions.Dock = System.Windows.Forms.DockStyle.Fill
                Me.tbtnMain.Checked = False
                Me.tbtnOptions.Checked = True
                Me.tbtnAdvanced.Checked = False
                If Me.optTOCStylesUpdate.Checked Then
                    Me.optTOCStylesUpdate.Focus()
                Else
                    Me.optTOCStylesKeep.Focus()
                End If
                Me.pnlOptions.Visible = True
                System.Windows.Forms.Application.DoEvents()
            Else
                Me.tbtnOptions.BackColor = Drawing.SystemColors.ButtonFace
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnAdvanced_CheckedChanged(sender As Object, e As EventArgs) Handles tbtnAdvanced.CheckedChanged
        Try
            If tbtnAdvanced.Checked Then
                Me.tbtnAdvanced.BackColor = Drawing.SystemColors.ButtonHighlight
                Me.pnlOptions.Visible = False
                Me.pnlMain.Visible = False
                'Me.pnlMain.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlOptions.Dock = System.Windows.Forms.DockStyle.None
                'Me.pnlAdvanced.Dock = System.Windows.Forms.DockStyle.Fill
                Me.tbtnMain.Checked = False
                Me.tbtnOptions.Checked = False
                Me.tbtnAdvanced.Checked = True
                Me.lstDesignate.Focus()
                Me.pnlAdvanced.Visible = True
                System.Windows.Forms.Application.DoEvents()
            Else
                Me.tbtnAdvanced.BackColor = Drawing.SystemColors.ButtonFace
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkStyles_CheckedChanged(sender As Object, e As EventArgs) Handles chkStyles.CheckedChanged
        Try
            If Not Me.InsertAsField Then
                Me.cmbTextStyles.Enabled = Me.chkStyles.CheckState = System.Windows.Forms.CheckState.Checked
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub frmInsertTOC_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class