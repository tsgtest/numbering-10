<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMarkAll
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public ToolTip1 As System.Windows.Forms.ToolTip
    'Public WithEvents chkLevel As Microsoft.VisualBasic.Compatibility.VB6.CheckBoxArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMarkAll))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkLevel1 = New System.Windows.Forms.CheckBox()
        Me.chkLevel2 = New System.Windows.Forms.CheckBox()
        Me.chkLevel3 = New System.Windows.Forms.CheckBox()
        Me.chkLevel4 = New System.Windows.Forms.CheckBox()
        Me.chkLevel5 = New System.Windows.Forms.CheckBox()
        Me.chkLevel6 = New System.Windows.Forms.CheckBox()
        Me.chkLevel7 = New System.Windows.Forms.CheckBox()
        Me.chkLevel8 = New System.Windows.Forms.CheckBox()
        Me.chkLevel9 = New System.Windows.Forms.CheckBox()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.pnlUndo = New System.Windows.Forms.GroupBox()
        Me.chkUnformat = New System.Windows.Forms.CheckBox()
        Me.chkUnmark = New System.Windows.Forms.CheckBox()
        Me.pnlDo = New System.Windows.Forms.GroupBox()
        Me.chkMarkTC = New System.Windows.Forms.CheckBox()
        Me.chkFormat = New System.Windows.Forms.CheckBox()
        Me.cbxUse = New System.Windows.Forms.ComboBox()
        Me.lblMode = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pnlLevels = New System.Windows.Forms.GroupBox()
        Me.pnlApplyTo = New System.Windows.Forms.GroupBox()
        Me.cbxScheme = New mpnControls.ComboBox()
        Me.optAllSchemes = New System.Windows.Forms.RadioButton()
        Me.optSelectedScheme = New System.Windows.Forms.RadioButton()
        Me.pnlScope = New System.Windows.Forms.GroupBox()
        Me.cbxScope = New mpnControls.ComboBox()
        Me.txtDefinition = New System.Windows.Forms.TextBox()
        Me.cbxDefinition = New System.Windows.Forms.ComboBox()
        Me.lblScope = New System.Windows.Forms.Label()
        Me.lblDefinition = New System.Windows.Forms.Label()
        Me.pnlFormatting = New System.Windows.Forms.Panel()
        Me.cbxFormatting = New mpnControls.ComboBox()
        Me.grdFormatting = New System.Windows.Forms.DataGridView()
        Me.Level = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bold = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Italics = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Underline = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SmallCaps = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AllCaps = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblFormatting = New System.Windows.Forms.Label()
        Me.tsMenu = New System.Windows.Forms.ToolStrip()
        Me.tbtnMain = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbtnFormatting = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsButtons = New System.Windows.Forms.ToolStrip()
        Me.btnCancel = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnOK = New System.Windows.Forms.ToolStripButton()
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.pnlMain.SuspendLayout()
        Me.pnlUndo.SuspendLayout()
        Me.pnlDo.SuspendLayout()
        Me.pnlLevels.SuspendLayout()
        Me.pnlApplyTo.SuspendLayout()
        Me.pnlScope.SuspendLayout()
        Me.pnlFormatting.SuspendLayout()
        CType(Me.grdFormatting, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tsMenu.SuspendLayout()
        Me.tsButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkLevel1
        '
        Me.chkLevel1.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel1.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel1.Image = Global.My.Resources.Resources._1
        Me.chkLevel1.Location = New System.Drawing.Point(17, 24)
        Me.chkLevel1.Name = "chkLevel1"
        Me.chkLevel1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel1.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel1.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.chkLevel1, "Click to check level 1.")
        Me.chkLevel1.UseVisualStyleBackColor = False
        '
        'chkLevel2
        '
        Me.chkLevel2.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel2.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel2.Image = Global.My.Resources.Resources._2
        Me.chkLevel2.Location = New System.Drawing.Point(17, 53)
        Me.chkLevel2.Name = "chkLevel2"
        Me.chkLevel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel2.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel2.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.chkLevel2, "Press the shift key and click to check levels 1-2.")
        Me.chkLevel2.UseVisualStyleBackColor = False
        '
        'chkLevel3
        '
        Me.chkLevel3.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel3.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel3.Image = Global.My.Resources.Resources._3
        Me.chkLevel3.Location = New System.Drawing.Point(17, 82)
        Me.chkLevel3.Name = "chkLevel3"
        Me.chkLevel3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel3.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel3.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.chkLevel3, "Press the shift key and click to check levels 1-3.")
        Me.chkLevel3.UseVisualStyleBackColor = False
        '
        'chkLevel4
        '
        Me.chkLevel4.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel4.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel4.Image = Global.My.Resources.Resources._4
        Me.chkLevel4.Location = New System.Drawing.Point(17, 111)
        Me.chkLevel4.Name = "chkLevel4"
        Me.chkLevel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel4.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel4.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.chkLevel4, "Press the shift key and click to check levels 1-4.")
        Me.chkLevel4.UseVisualStyleBackColor = False
        '
        'chkLevel5
        '
        Me.chkLevel5.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel5.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel5.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel5.Image = Global.My.Resources.Resources._5
        Me.chkLevel5.Location = New System.Drawing.Point(17, 140)
        Me.chkLevel5.Name = "chkLevel5"
        Me.chkLevel5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel5.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel5.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.chkLevel5, "Press the shift key and click to check levels 1-5.")
        Me.chkLevel5.UseVisualStyleBackColor = False
        '
        'chkLevel6
        '
        Me.chkLevel6.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel6.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel6.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel6.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel6.Image = Global.My.Resources.Resources._6
        Me.chkLevel6.Location = New System.Drawing.Point(17, 169)
        Me.chkLevel6.Name = "chkLevel6"
        Me.chkLevel6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel6.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel6.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.chkLevel6, "Press the shift key and click to check levels 1-6.")
        Me.chkLevel6.UseVisualStyleBackColor = False
        '
        'chkLevel7
        '
        Me.chkLevel7.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel7.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel7.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel7.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel7.Image = Global.My.Resources.Resources._7
        Me.chkLevel7.Location = New System.Drawing.Point(17, 198)
        Me.chkLevel7.Name = "chkLevel7"
        Me.chkLevel7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel7.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel7.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.chkLevel7, "Press the shift key and click to check levels 1-7.")
        Me.chkLevel7.UseVisualStyleBackColor = False
        '
        'chkLevel8
        '
        Me.chkLevel8.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel8.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel8.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel8.Image = Global.My.Resources.Resources._8
        Me.chkLevel8.Location = New System.Drawing.Point(17, 227)
        Me.chkLevel8.Name = "chkLevel8"
        Me.chkLevel8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel8.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel8.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.chkLevel8, "Press the shift key and click to check levels 1-8.")
        Me.chkLevel8.UseVisualStyleBackColor = False
        '
        'chkLevel9
        '
        Me.chkLevel9.BackColor = System.Drawing.SystemColors.Control
        Me.chkLevel9.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkLevel9.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkLevel9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLevel9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLevel9.Image = Global.My.Resources.Resources._9
        Me.chkLevel9.Location = New System.Drawing.Point(17, 256)
        Me.chkLevel9.Name = "chkLevel9"
        Me.chkLevel9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkLevel9.Size = New System.Drawing.Size(43, 28)
        Me.chkLevel9.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.chkLevel9, "Press the shift key and click to check levels 1-9.")
        Me.chkLevel9.UseVisualStyleBackColor = False
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlUndo)
        Me.pnlMain.Controls.Add(Me.pnlDo)
        Me.pnlMain.Controls.Add(Me.pnlLevels)
        Me.pnlMain.Controls.Add(Me.pnlApplyTo)
        Me.pnlMain.Controls.Add(Me.pnlScope)
        Me.pnlMain.Location = New System.Drawing.Point(0, 35)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(501, 324)
        Me.pnlMain.TabIndex = 3
        '
        'pnlUndo
        '
        Me.pnlUndo.BackColor = System.Drawing.SystemColors.Control
        Me.pnlUndo.Controls.Add(Me.chkUnformat)
        Me.pnlUndo.Controls.Add(Me.chkUnmark)
        Me.pnlUndo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlUndo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlUndo.Location = New System.Drawing.Point(314, 12)
        Me.pnlUndo.Name = "pnlUndo"
        Me.pnlUndo.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlUndo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlUndo.Size = New System.Drawing.Size(175, 138)
        Me.pnlUndo.TabIndex = 26
        Me.pnlUndo.TabStop = False
        Me.pnlUndo.Text = "Reverse Actions"
        '
        'chkUnformat
        '
        Me.chkUnformat.BackColor = System.Drawing.SystemColors.Control
        Me.chkUnformat.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUnformat.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnformat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUnformat.Location = New System.Drawing.Point(12, 24)
        Me.chkUnformat.Name = "chkUnformat"
        Me.chkUnformat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUnformat.Size = New System.Drawing.Size(142, 25)
        Me.chkUnformat.TabIndex = 17
        Me.chkUnformat.Text = "&Unformat Headings"
        Me.chkUnformat.UseVisualStyleBackColor = False
        '
        'chkUnmark
        '
        Me.chkUnmark.BackColor = System.Drawing.SystemColors.Control
        Me.chkUnmark.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkUnmark.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnmark.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkUnmark.Location = New System.Drawing.Point(12, 74)
        Me.chkUnmark.Name = "chkUnmark"
        Me.chkUnmark.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkUnmark.Size = New System.Drawing.Size(142, 34)
        Me.chkUnmark.TabIndex = 18
        Me.chkUnmark.Text = "U&nmark for TOC"
        Me.chkUnmark.UseVisualStyleBackColor = False
        '
        'pnlDo
        '
        Me.pnlDo.BackColor = System.Drawing.SystemColors.Control
        Me.pnlDo.Controls.Add(Me.chkMarkTC)
        Me.pnlDo.Controls.Add(Me.chkFormat)
        Me.pnlDo.Controls.Add(Me.cbxUse)
        Me.pnlDo.Controls.Add(Me.lblMode)
        Me.pnlDo.Controls.Add(Me.Label3)
        Me.pnlDo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlDo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlDo.Location = New System.Drawing.Point(101, 12)
        Me.pnlDo.Name = "pnlDo"
        Me.pnlDo.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlDo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlDo.Size = New System.Drawing.Size(199, 138)
        Me.pnlDo.TabIndex = 25
        Me.pnlDo.TabStop = False
        Me.pnlDo.Text = "Actions"
        '
        'chkMarkTC
        '
        Me.chkMarkTC.BackColor = System.Drawing.SystemColors.Control
        Me.chkMarkTC.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMarkTC.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMarkTC.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMarkTC.Location = New System.Drawing.Point(12, 76)
        Me.chkMarkTC.Name = "chkMarkTC"
        Me.chkMarkTC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMarkTC.Size = New System.Drawing.Size(150, 25)
        Me.chkMarkTC.TabIndex = 14
        Me.chkMarkTC.Text = "Mark for &TOC"
        Me.chkMarkTC.UseVisualStyleBackColor = False
        '
        'chkFormat
        '
        Me.chkFormat.BackColor = System.Drawing.SystemColors.Control
        Me.chkFormat.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkFormat.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFormat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkFormat.Location = New System.Drawing.Point(12, 24)
        Me.chkFormat.Name = "chkFormat"
        Me.chkFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkFormat.Size = New System.Drawing.Size(153, 25)
        Me.chkFormat.TabIndex = 12
        Me.chkFormat.Text = "Format &Headings"
        Me.chkFormat.UseVisualStyleBackColor = False
        '
        'cbxUse
        '
        Me.cbxUse.BackColor = System.Drawing.SystemColors.Window
        Me.cbxUse.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbxUse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUse.Enabled = False
        Me.cbxUse.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxUse.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbxUse.Items.AddRange(New Object() {"Style Separators", "TC Codes"})
        Me.cbxUse.Location = New System.Drawing.Point(34, 188)
        Me.cbxUse.Name = "cbxUse"
        Me.cbxUse.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbxUse.Size = New System.Drawing.Size(155, 21)
        Me.cbxUse.TabIndex = 15
        Me.cbxUse.Visible = False
        '
        'lblMode
        '
        Me.lblMode.BackColor = System.Drawing.SystemColors.Control
        Me.lblMode.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMode.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMode.Location = New System.Drawing.Point(33, 102)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMode.Size = New System.Drawing.Size(147, 19)
        Me.lblMode.TabIndex = 34
        Me.lblMode.Text = "(Mode)"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(33, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(159, 19)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "(Format is Directly Applied)"
        '
        'pnlLevels
        '
        Me.pnlLevels.BackColor = System.Drawing.SystemColors.Control
        Me.pnlLevels.Controls.Add(Me.chkLevel1)
        Me.pnlLevels.Controls.Add(Me.chkLevel2)
        Me.pnlLevels.Controls.Add(Me.chkLevel3)
        Me.pnlLevels.Controls.Add(Me.chkLevel4)
        Me.pnlLevels.Controls.Add(Me.chkLevel5)
        Me.pnlLevels.Controls.Add(Me.chkLevel6)
        Me.pnlLevels.Controls.Add(Me.chkLevel7)
        Me.pnlLevels.Controls.Add(Me.chkLevel8)
        Me.pnlLevels.Controls.Add(Me.chkLevel9)
        Me.pnlLevels.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLevels.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlLevels.Location = New System.Drawing.Point(9, 12)
        Me.pnlLevels.Name = "pnlLevels"
        Me.pnlLevels.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlLevels.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlLevels.Size = New System.Drawing.Size(82, 307)
        Me.pnlLevels.TabIndex = 24
        Me.pnlLevels.TabStop = False
        Me.pnlLevels.Text = "Levels"
        '
        'pnlApplyTo
        '
        Me.pnlApplyTo.BackColor = System.Drawing.SystemColors.Control
        Me.pnlApplyTo.Controls.Add(Me.cbxScheme)
        Me.pnlApplyTo.Controls.Add(Me.optAllSchemes)
        Me.pnlApplyTo.Controls.Add(Me.optSelectedScheme)
        Me.pnlApplyTo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlApplyTo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlApplyTo.Location = New System.Drawing.Point(101, 160)
        Me.pnlApplyTo.Name = "pnlApplyTo"
        Me.pnlApplyTo.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlApplyTo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlApplyTo.Size = New System.Drawing.Size(199, 159)
        Me.pnlApplyTo.TabIndex = 27
        Me.pnlApplyTo.TabStop = False
        Me.pnlApplyTo.Text = "Apply To"
        '
        'cbxScheme
        '
        Me.cbxScheme.AllowEmptyValue = False
        Me.cbxScheme.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxScheme.AutoSize = True
        Me.cbxScheme.Borderless = False
        Me.cbxScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxScheme.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxScheme.IsDirty = False
        Me.cbxScheme.LimitToList = True
        Me.cbxScheme.Location = New System.Drawing.Point(12, 107)
        Me.cbxScheme.MaxDropDownItems = 8
        Me.cbxScheme.Name = "cbxScheme"
        Me.cbxScheme.SelectedIndex = -1
        Me.cbxScheme.SelectedValue = Nothing
        Me.cbxScheme.SelectionLength = 0
        Me.cbxScheme.SelectionStart = 0
        Me.cbxScheme.Size = New System.Drawing.Size(178, 23)
        Me.cbxScheme.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxScheme.SupportingValues = ""
        Me.cbxScheme.TabIndex = 22
        Me.cbxScheme.Tag2 = Nothing
        Me.cbxScheme.Value = ""
        '
        'optAllSchemes
        '
        Me.optAllSchemes.BackColor = System.Drawing.SystemColors.Control
        Me.optAllSchemes.Checked = True
        Me.optAllSchemes.Cursor = System.Windows.Forms.Cursors.Default
        Me.optAllSchemes.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAllSchemes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optAllSchemes.Location = New System.Drawing.Point(14, 37)
        Me.optAllSchemes.Name = "optAllSchemes"
        Me.optAllSchemes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optAllSchemes.Size = New System.Drawing.Size(127, 24)
        Me.optAllSchemes.TabIndex = 20
        Me.optAllSchemes.TabStop = True
        Me.optAllSchemes.Text = "&All Schemes"
        Me.optAllSchemes.UseVisualStyleBackColor = False
        '
        'optSelectedScheme
        '
        Me.optSelectedScheme.BackColor = System.Drawing.SystemColors.Control
        Me.optSelectedScheme.Cursor = System.Windows.Forms.Cursors.Default
        Me.optSelectedScheme.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optSelectedScheme.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optSelectedScheme.Location = New System.Drawing.Point(14, 77)
        Me.optSelectedScheme.Name = "optSelectedScheme"
        Me.optSelectedScheme.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optSelectedScheme.Size = New System.Drawing.Size(161, 24)
        Me.optSelectedScheme.TabIndex = 21
        Me.optSelectedScheme.TabStop = True
        Me.optSelectedScheme.Text = "S&elect Scheme:"
        Me.optSelectedScheme.UseVisualStyleBackColor = False
        '
        'pnlScope
        '
        Me.pnlScope.BackColor = System.Drawing.SystemColors.Control
        Me.pnlScope.Controls.Add(Me.cbxScope)
        Me.pnlScope.Controls.Add(Me.txtDefinition)
        Me.pnlScope.Controls.Add(Me.cbxDefinition)
        Me.pnlScope.Controls.Add(Me.lblScope)
        Me.pnlScope.Controls.Add(Me.lblDefinition)
        Me.pnlScope.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlScope.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnlScope.Location = New System.Drawing.Point(314, 160)
        Me.pnlScope.Name = "pnlScope"
        Me.pnlScope.Padding = New System.Windows.Forms.Padding(0)
        Me.pnlScope.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlScope.Size = New System.Drawing.Size(175, 159)
        Me.pnlScope.TabIndex = 28
        Me.pnlScope.TabStop = False
        Me.pnlScope.Text = "Other"
        '
        'cbxScope
        '
        Me.cbxScope.AllowEmptyValue = False
        Me.cbxScope.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxScope.AutoSize = True
        Me.cbxScope.Borderless = False
        Me.cbxScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxScope.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxScope.IsDirty = False
        Me.cbxScope.LimitToList = True
        Me.cbxScope.Location = New System.Drawing.Point(12, 108)
        Me.cbxScope.MaxDropDownItems = 8
        Me.cbxScope.Name = "cbxScope"
        Me.cbxScope.SelectedIndex = -1
        Me.cbxScope.SelectedValue = Nothing
        Me.cbxScope.SelectionLength = 0
        Me.cbxScope.SelectionStart = 0
        Me.cbxScope.Size = New System.Drawing.Size(155, 23)
        Me.cbxScope.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxScope.SupportingValues = ""
        Me.cbxScope.TabIndex = 27
        Me.cbxScope.Tag2 = Nothing
        Me.cbxScope.Value = ""
        '
        'txtDefinition
        '
        Me.txtDefinition.AcceptsReturn = True
        Me.txtDefinition.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDefinition.BackColor = System.Drawing.SystemColors.Window
        Me.txtDefinition.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDefinition.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDefinition.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDefinition.Location = New System.Drawing.Point(12, 48)
        Me.txtDefinition.MaxLength = 0
        Me.txtDefinition.Name = "txtDefinition"
        Me.txtDefinition.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtDefinition.Size = New System.Drawing.Size(155, 23)
        Me.txtDefinition.TabIndex = 25
        '
        'cbxDefinition
        '
        Me.cbxDefinition.BackColor = System.Drawing.SystemColors.Window
        Me.cbxDefinition.Cursor = System.Windows.Forms.Cursors.Default
        Me.cbxDefinition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDefinition.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDefinition.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cbxDefinition.Location = New System.Drawing.Point(12, 167)
        Me.cbxDefinition.Name = "cbxDefinition"
        Me.cbxDefinition.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cbxDefinition.Size = New System.Drawing.Size(155, 21)
        Me.cbxDefinition.TabIndex = 33
        Me.cbxDefinition.Visible = False
        '
        'lblScope
        '
        Me.lblScope.BackColor = System.Drawing.SystemColors.Control
        Me.lblScope.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblScope.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScope.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblScope.Location = New System.Drawing.Point(14, 92)
        Me.lblScope.Name = "lblScope"
        Me.lblScope.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblScope.Size = New System.Drawing.Size(142, 18)
        Me.lblScope.TabIndex = 26
        Me.lblScope.Text = "&Scope:"
        '
        'lblDefinition
        '
        Me.lblDefinition.BackColor = System.Drawing.SystemColors.Control
        Me.lblDefinition.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDefinition.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefinition.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDefinition.Location = New System.Drawing.Point(14, 32)
        Me.lblDefinition.Name = "lblDefinition"
        Me.lblDefinition.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDefinition.Size = New System.Drawing.Size(142, 18)
        Me.lblDefinition.TabIndex = 24
        Me.lblDefinition.Text = "Heading &Definition:"
        '
        'pnlFormatting
        '
        Me.pnlFormatting.Controls.Add(Me.cbxFormatting)
        Me.pnlFormatting.Controls.Add(Me.grdFormatting)
        Me.pnlFormatting.Controls.Add(Me.lblFormatting)
        Me.pnlFormatting.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlFormatting.Location = New System.Drawing.Point(3, 0)
        Me.pnlFormatting.Name = "pnlFormatting"
        Me.pnlFormatting.Size = New System.Drawing.Size(493, 324)
        Me.pnlFormatting.TabIndex = 4
        '
        'cbxFormatting
        '
        Me.cbxFormatting.AllowEmptyValue = False
        Me.cbxFormatting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxFormatting.AutoSize = True
        Me.cbxFormatting.Borderless = False
        Me.cbxFormatting.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.cbxFormatting.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFormatting.IsDirty = False
        Me.cbxFormatting.LimitToList = True
        Me.cbxFormatting.Location = New System.Drawing.Point(7, 48)
        Me.cbxFormatting.MaxDropDownItems = 8
        Me.cbxFormatting.Name = "cbxFormatting"
        Me.cbxFormatting.SelectedIndex = -1
        Me.cbxFormatting.SelectedValue = Nothing
        Me.cbxFormatting.SelectionLength = 0
        Me.cbxFormatting.SelectionStart = 0
        Me.cbxFormatting.Size = New System.Drawing.Size(238, 23)
        Me.cbxFormatting.SortOrder = System.Windows.Forms.SortOrder.None
        Me.cbxFormatting.SupportingValues = ""
        Me.cbxFormatting.TabIndex = 38
        Me.cbxFormatting.Tag2 = Nothing
        Me.cbxFormatting.Value = ""
        '
        'grdFormatting
        '
        Me.grdFormatting.AllowUserToAddRows = False
        Me.grdFormatting.AllowUserToDeleteRows = False
        Me.grdFormatting.AllowUserToResizeColumns = False
        Me.grdFormatting.AllowUserToResizeRows = False
        Me.grdFormatting.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdFormatting.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Me.grdFormatting.BackgroundColor = System.Drawing.SystemColors.Control
        Me.grdFormatting.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdFormatting.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText
        Me.grdFormatting.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdFormatting.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdFormatting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFormatting.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Level, Me.Bold, Me.Italics, Me.Underline, Me.SmallCaps, Me.AllCaps})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdFormatting.DefaultCellStyle = DataGridViewCellStyle8
        Me.grdFormatting.EnableHeadersVisualStyles = False
        Me.grdFormatting.Location = New System.Drawing.Point(4, 88)
        Me.grdFormatting.MultiSelect = False
        Me.grdFormatting.Name = "grdFormatting"
        Me.grdFormatting.RowHeadersVisible = False
        Me.grdFormatting.RowTemplate.Height = 35
        Me.grdFormatting.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdFormatting.ShowEditingIcon = False
        Me.grdFormatting.Size = New System.Drawing.Size(481, 229)
        Me.grdFormatting.TabIndex = 42
        '
        'Level
        '
        Me.Level.DataPropertyName = "Level"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Level.DefaultCellStyle = DataGridViewCellStyle2
        Me.Level.HeaderText = "Level"
        Me.Level.Name = "Level"
        Me.Level.ReadOnly = True
        Me.Level.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Level.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Bold
        '
        Me.Bold.DataPropertyName = "Bold"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Bold.DefaultCellStyle = DataGridViewCellStyle3
        Me.Bold.HeaderText = "Bold"
        Me.Bold.Name = "Bold"
        Me.Bold.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Bold.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Italics
        '
        Me.Italics.DataPropertyName = "Italics"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Italics.DefaultCellStyle = DataGridViewCellStyle4
        Me.Italics.HeaderText = "Italics"
        Me.Italics.Name = "Italics"
        Me.Italics.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Italics.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Underline
        '
        Me.Underline.DataPropertyName = "Underline"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Underline.DefaultCellStyle = DataGridViewCellStyle5
        Me.Underline.HeaderText = "Underline"
        Me.Underline.Name = "Underline"
        Me.Underline.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Underline.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'SmallCaps
        '
        Me.SmallCaps.DataPropertyName = "SmallCaps"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.SmallCaps.DefaultCellStyle = DataGridViewCellStyle6
        Me.SmallCaps.HeaderText = "Small Caps"
        Me.SmallCaps.Name = "SmallCaps"
        Me.SmallCaps.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SmallCaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AllCaps
        '
        Me.AllCaps.DataPropertyName = "AllCaps"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.AllCaps.DefaultCellStyle = DataGridViewCellStyle7
        Me.AllCaps.HeaderText = "All Caps"
        Me.AllCaps.Name = "AllCaps"
        Me.AllCaps.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.AllCaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lblFormatting
        '
        Me.lblFormatting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFormatting.BackColor = System.Drawing.SystemColors.Control
        Me.lblFormatting.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblFormatting.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormatting.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblFormatting.Location = New System.Drawing.Point(4, 12)
        Me.lblFormatting.Name = "lblFormatting"
        Me.lblFormatting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFormatting.Size = New System.Drawing.Size(257, 32)
        Me.lblFormatting.TabIndex = 37
        Me.lblFormatting.Text = "Show direct formatting for run-in levels selected on the Main tab for the followi" & _
    "ng scheme:"
        '
        'tsMenu
        '
        Me.tsMenu.AutoSize = False
        Me.tsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsMenu.ImageScalingSize = New System.Drawing.Size(28, 28)
        Me.tsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tbtnMain, Me.toolStripSeparator12, Me.tbtnFormatting, Me.toolStripSeparator11})
        Me.tsMenu.Location = New System.Drawing.Point(0, 0)
        Me.tsMenu.Name = "tsMenu"
        Me.tsMenu.Size = New System.Drawing.Size(501, 32)
        Me.tsMenu.Stretch = True
        Me.tsMenu.TabIndex = 0
        Me.tsMenu.Text = "ToolStrip1"
        '
        'tbtnMain
        '
        Me.tbtnMain.CheckOnClick = True
        Me.tbtnMain.ImageTransparentColor = System.Drawing.Color.White
        Me.tbtnMain.Name = "tbtnMain"
        Me.tbtnMain.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnMain.Size = New System.Drawing.Size(38, 29)
        Me.tbtnMain.Text = "Main"
        Me.tbtnMain.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator12
        '
        Me.toolStripSeparator12.Name = "toolStripSeparator12"
        Me.toolStripSeparator12.Size = New System.Drawing.Size(6, 32)
        '
        'tbtnFormatting
        '
        Me.tbtnFormatting.CheckOnClick = True
        Me.tbtnFormatting.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbtnFormatting.Name = "tbtnFormatting"
        Me.tbtnFormatting.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.tbtnFormatting.Size = New System.Drawing.Size(70, 29)
        Me.tbtnFormatting.Text = "Formatting"
        Me.tbtnFormatting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator11
        '
        Me.toolStripSeparator11.Name = "toolStripSeparator11"
        Me.toolStripSeparator11.Size = New System.Drawing.Size(6, 32)
        '
        'tsButtons
        '
        Me.tsButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tsButtons.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tsButtons.ImageScalingSize = New System.Drawing.Size(26, 26)
        Me.tsButtons.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnCancel, Me.toolStripSeparator1, Me.btnOK, Me.toolStripSeparator3})
        Me.tsButtons.Location = New System.Drawing.Point(0, 357)
        Me.tsButtons.Name = "tsButtons"
        Me.tsButtons.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.tsButtons.Size = New System.Drawing.Size(501, 49)
        Me.tsButtons.Stretch = True
        Me.tsButtons.TabIndex = 2
        Me.tsButtons.Text = "ToolStrip1"
        '
        'btnCancel
        '
        Me.btnCancel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnCancel.AutoSize = False
        Me.btnCancel.Image = Global.My.Resources.Resources.close
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.White
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(0, 2, 0, 2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnCancel.Size = New System.Drawing.Size(60, 45)
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(6, 49)
        '
        'btnOK
        '
        Me.btnOK.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.btnOK.AutoSize = False
        Me.btnOK.Image = Global.My.Resources.Resources.OK
        Me.btnOK.ImageTransparentColor = System.Drawing.Color.White
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me.btnOK.Size = New System.Drawing.Size(60, 45)
        Me.btnOK.Text = "O&K"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(6, 49)
        '
        'frmMarkAll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(501, 406)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.pnlFormatting)
        Me.Controls.Add(Me.tsMenu)
        Me.Controls.Add(Me.tsButtons)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMarkAll"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = " Mark/Format All Headings"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlUndo.ResumeLayout(False)
        Me.pnlDo.ResumeLayout(False)
        Me.pnlLevels.ResumeLayout(False)
        Me.pnlApplyTo.ResumeLayout(False)
        Me.pnlApplyTo.PerformLayout()
        Me.pnlScope.ResumeLayout(False)
        Me.pnlScope.PerformLayout()
        Me.pnlFormatting.ResumeLayout(False)
        Me.pnlFormatting.PerformLayout()
        CType(Me.grdFormatting, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tsMenu.ResumeLayout(False)
        Me.tsMenu.PerformLayout()
        Me.tsButtons.ResumeLayout(False)
        Me.tsButtons.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Public WithEvents pnlUndo As System.Windows.Forms.GroupBox
    Public WithEvents chkUnformat As System.Windows.Forms.CheckBox
    Public WithEvents chkUnmark As System.Windows.Forms.CheckBox
    Public WithEvents pnlDo As System.Windows.Forms.GroupBox
    Public WithEvents chkMarkTC As System.Windows.Forms.CheckBox
    Public WithEvents chkFormat As System.Windows.Forms.CheckBox
    Public WithEvents cbxUse As System.Windows.Forms.ComboBox
    Public WithEvents lblMode As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents pnlLevels As System.Windows.Forms.GroupBox
    Public WithEvents chkLevel1 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel2 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel3 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel4 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel5 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel6 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel7 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel8 As System.Windows.Forms.CheckBox
    Public WithEvents chkLevel9 As System.Windows.Forms.CheckBox
    Public WithEvents pnlApplyTo As System.Windows.Forms.GroupBox
    Public WithEvents optAllSchemes As System.Windows.Forms.RadioButton
    Public WithEvents optSelectedScheme As System.Windows.Forms.RadioButton
    Public WithEvents pnlScope As System.Windows.Forms.GroupBox
    Public WithEvents txtDefinition As System.Windows.Forms.TextBox
    Public WithEvents cbxDefinition As System.Windows.Forms.ComboBox
    Public WithEvents lblScope As System.Windows.Forms.Label
    Public WithEvents lblDefinition As System.Windows.Forms.Label
    Friend WithEvents pnlFormatting As System.Windows.Forms.Panel
    Public WithEvents lblFormatting As System.Windows.Forms.Label
    Friend WithEvents tsMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents tbtnMain As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbtnFormatting As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsButtons As System.Windows.Forms.ToolStrip
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOK As System.Windows.Forms.ToolStripButton
    Private WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents grdFormatting As System.Windows.Forms.DataGridView
    Friend WithEvents cbxFormatting As mpnControls.ComboBox
    Friend WithEvents cbxScheme As mpnControls.ComboBox
    Friend WithEvents cbxScope As mpnControls.ComboBox
    Friend WithEvents Level As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Bold As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Italics As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Underline As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SmallCaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllCaps As System.Windows.Forms.DataGridViewTextBoxColumn
#End Region
End Class