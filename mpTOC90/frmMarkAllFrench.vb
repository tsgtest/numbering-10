Option Strict Off
Option Explicit On
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.Base.cIO
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cSchemeRecords
Imports System.ComponentModel
Imports System.Windows.Forms
Imports LMP

Friend Class frmMarkAllFrench
    Inherits System.Windows.Forms.Form
    Public m_bFinished As Boolean
    Private m_bClickingLevels As Boolean
    Private m_bInitializing As Boolean
    Private m_oFormattingChanges As Hashtable
    Private m_bLoadingDefaults As Boolean
    Private oGridList As BindingList(Of GridRecord)
    Private m_iActive As Short
    Private m_xarScheme As Array
    Private arCheckLevels(8) As System.Windows.Forms.CheckBox
    Private m_oBindingSource As BindingSource
    Private m_bLoaded As Boolean

    Private Enum mpFormattingColumns
        Level = 0
        Bold = 1
        Italic = 2
        Underline = 3
        SmallCaps = 4
        AllCaps = 5
    End Enum
    Private Structure GridRecord
        Public Property Level() As Short
        Public Property Bold() As Object
        Public Property Italics() As Object
        Public Property Underline() As Object
        Public Property SmallCaps() As Object
        Public Property AllCaps() As Object
    End Structure
    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Dim i As Short

        Try
            If bIsValid() Then
                grdFormatting.EndEdit()
                '       sticky fields
                For i = 0 To 8
                    SetUserSetting("MarkAll", Str(i + 1), CStr(Me.arCheckLevels(i).CheckState))
                Next i
                SetUserSetting("MarkAll", "ApplyToSelected", CStr((Me.optSelectedScheme).Checked))
                SetUserSetting("MarkAll", "SentenceDefinition", (Me.txtDefinition).Text)
                SetUserSetting("MarkAll", "Format", CStr((Me.chkFormat).CheckState))
                SetUserSetting("MarkAll", "MarkTC", CStr((Me.chkMarkTC).CheckState))
                SetUserSetting("MarkAll", "Unformat", CStr((Me.chkUnformat).CheckState))
                SetUserSetting("MarkAll", "Unmark", CStr((Me.chkUnmark).CheckState))
                SetUserSetting("MarkAll", "Scheme", (Me.cbxScheme.Text))
                SetUserSetting("MarkAll", "Scope", (Me.cbxScope.Text))
                m_bFinished = True
                Me.DialogResult = System.Windows.Forms.DialogResult.OK
                Me.Hide()
                System.Windows.Forms.Application.DoEvents()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxFormatting_ItemChange(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxFormatting.ValueChanged
        Try
            If m_bLoaded Then
                LoadFormattingDefaults()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub cbxScheme_ItemChange(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cbxScheme.ValueChanged
        If m_bLoaded Then
            Me.cbxFormatting.SelectedIndex = Me.cbxScheme.SelectedIndex
            LoadFormattingDefaults()
        End If
    End Sub
    Private Sub chkFormat_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkFormat.CheckStateChanged
        Dim bEnabled As Boolean
        Dim i As Short

        Try
            If chkFormat.CheckState = 1 Then
                chkUnformat.CheckState = System.Windows.Forms.CheckState.Unchecked

                'enable formatting tab only if formatting is checked and
                'at least one level is selected
                For i = 0 To 8
                    If Me.arCheckLevels(i).CheckState = System.Windows.Forms.CheckState.Checked Then
                        bEnabled = True
                        Exit For
                    End If
                Next i
            End If

            tbtnFormatting.Enabled = bEnabled
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub chkLevel_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLevel1.CheckStateChanged, chkLevel2.CheckStateChanged,
                chkLevel3.CheckStateChanged, chkLevel4.CheckStateChanged, chkLevel5.CheckStateChanged, chkLevel6.CheckStateChanged, chkLevel7.CheckStateChanged, chkLevel8.CheckStateChanged, chkLevel9.CheckStateChanged
        Try
            Dim i As Short
            Dim bEnabled As Boolean
            Dim Index As Short
            Dim oCheck As System.Windows.Forms.CheckBox = eventSender
            Index = CShort(Mid(oCheck.Name, Len(oCheck.Name))) - 1

            '   if shift is pressed, make all previous
            '   levels the same value as the clicked level
            If Not m_bClickingLevels Then
                m_bClickingLevels = True
                If IsPressed(KEY_SHIFT) Then
                    For i = 0 To Index - 1
                        Me.arCheckLevels(i).CheckState = Me.arCheckLevels(Index).CheckState
                    Next i
                End If
                m_bClickingLevels = False
            End If

            'load formatting defaults
            If Not m_bInitializing Then LoadFormattingDefaults()

            'enable formatting tab only if formatting is checked and
            'at least one level is selected
            If chkFormat.CheckState = 1 Then
                For i = 0 To 8
                    If Me.arCheckLevels(i).CheckState = 1 Then
                        bEnabled = True
                        Exit For
                    End If
                Next i
            End If
            tbtnFormatting.Enabled = bEnabled
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkMarkTC_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMarkTC.CheckStateChanged
        Try
            If chkMarkTC.CheckState = 1 Then chkUnmark.CheckState = System.Windows.Forms.CheckState.Unchecked
            Me.cbxUse.Enabled = CBool(Me.chkMarkTC.CheckState)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkUnformat_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkUnformat.CheckStateChanged
        Try
            If chkUnformat.CheckState = 1 Then chkFormat.CheckState = System.Windows.Forms.CheckState.Unchecked
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub chkUnmark_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkUnmark.CheckStateChanged
        Try
            If chkUnmark.CheckState = 1 Then chkMarkTC.CheckState = System.Windows.Forms.CheckState.Unchecked
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmMarkAll_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim i As Short
        Dim iChecked As Short
        Dim xActive As String
        Dim xDef As String
        Dim xScope As String
        Dim xScheme As String
        Dim xarScope As Array
        Dim oRecord As GridRecord

        Try
            m_bInitializing = True
            m_bFinished = False

            Me.Width = Me.pnlMain.Width + 14
            Me.CenterToParent()
            Me.tbtnMain.Dock = System.Windows.Forms.DockStyle.Fill
            Me.tbtnFormatting.Dock = System.Windows.Forms.DockStyle.None
            Me.tbtnMain.Checked = True
            m_oFormattingChanges = New Hashtable

            'adjust for scaling
            Dim sFactor As Single = LMP.Numbering.TOC.mpApplication.GetScalingFactor()

            If sFactor > 1 Then
                Me.btnOK.Width = Me.btnOK.Width * sFactor
                Me.btnCancel.Width = Me.btnCancel.Width * sFactor
                Me.btnOK.Height = Me.btnOK.Height + (sFactor - 1) * 40
                Me.btnCancel.Height = Me.btnCancel.Height + (sFactor - 1) * 40
            End If

            '   formatting tab should start disabled
            tbtnFormatting.Enabled = False

            '   load scopes
            xarScope = New String(2, 1) {{"Document en cours", "0"},
                                         {"Section en cours", "1"},
                                         {"S�lection en cours", "2"}}

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxScope.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cbxScope.SetList(xarScope)

            arCheckLevels(0) = Me.chkLevel1
            arCheckLevels(1) = Me.chkLevel2
            arCheckLevels(2) = Me.chkLevel3
            arCheckLevels(3) = Me.chkLevel4
            arCheckLevels(4) = Me.chkLevel5
            arCheckLevels(5) = Me.chkLevel6
            arCheckLevels(6) = Me.chkLevel7
            arCheckLevels(7) = Me.chkLevel8
            arCheckLevels(8) = Me.chkLevel9

            '   sticky checkboxes
            For i = 0 To 8
                Try
                    iChecked = Val(GetUserSetting("MarkAll", Str(i + 1)))
                Catch
                End Try
                Me.arCheckLevels(i).CheckState = iChecked
            Next i
            Try
                Me.chkFormat.CheckState = Val(GetUserSetting("MarkAll", "Format"))
            Catch
            End Try
            Try
                Me.chkMarkTC.CheckState = Val(GetUserSetting("MarkAll", "MarkTC"))
            Catch
            End Try
            Try
                Me.chkUnformat.CheckState = Val(GetUserSetting("MarkAll", "Unformat"))
            Catch
            End Try
            Try
                Me.chkUnmark.CheckState = Val(GetUserSetting("MarkAll", "Unmark"))
            Catch
            End Try

            '   ensure that at least one action is checked
            If Me.chkFormat.CheckState + Me.chkMarkTC.CheckState + Me.chkUnformat.CheckState + Me.chkUnmark.CheckState = 0 Then
                Me.chkFormat.CheckState = System.Windows.Forms.CheckState.Checked
            End If

            Me.optSelectedScheme.Checked = (GetUserSetting("MarkAll", "ApplyToSelected") = "True")


            '   load schemes
            Try
                xActive = CurWordApp.ActiveDocument.Variables.Item("zzmpFixedCurScheme_9.0").Value
            Catch
            End Try

            If xActive <> "" Then xActive = Mid(xTrimSpaces(xActive), 2)

            m_xarScheme = New String(UBound(g_xDSchemes), 1) {}

            m_iActive = 0

            For i = 0 To UBound(g_xDSchemes)
                If g_xDSchemes(i, 1) = "" Then
                    m_xarScheme.SetValue(xGetStyleRoot(g_xDSchemes(i, 0)), i, 0)
                    m_xarScheme.SetValue(CStr(i), i, 1)
                Else
                    m_xarScheme.SetValue(g_xDSchemes(i, 1), i, 0)
                    m_xarScheme.SetValue(CStr(i), i, 1)
                End If

                If g_xDSchemes(i, 0) = xActive Then m_iActive = i
            Next i

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxScheme.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cbxScheme.SetList(m_xarScheme)

            '   sentence definition
            xDef = GetUserSetting("MarkAll", "SentenceDefinition")
            If xDef = "" Then xDef = "." & g_xBullet & g_xBullet
            With Me.txtDefinition
                .Text = xDef
                .SelectionStart = Len(xDef)
            End With

            'marking mode
            If g_bMarkWithStyleSeparators Then
                Me.lblMode.Text = "(Utiliser s�parateurs de style)"
            Else
                Me.lblMode.Text = "(Utiliser codes TM)"
            End If

            'set formatting list and defaults
            Me.cbxFormatting.SetList(m_xarScheme)

            '   initialize scope
            xScope = GetUserSetting("MarkAll", "Scope")
            If CurWordApp.Selection.Type = Microsoft.Office.Interop.Word.WdSelectionType.wdSelectionNormal Then
                Me.cbxScope.SelectedIndex = 2
            Else
                If xScope = "Section en cours" Then
                    Me.cbxScope.SelectedIndex = 1
                Else
                    Me.cbxScope.SelectedIndex = 0
                End If
            End If

            '   if last selected scheme is not in doc, default to active scheme
            xScheme = GetUserSetting("MarkAll", "Scheme")

            For i = 0 To m_xarScheme.GetUpperBound(0)
                If m_xarScheme.GetValue(i, 0) = xScheme Then
                    Me.cbxScheme.SelectedIndex = m_xarScheme.GetValue(i, 1)
                    Exit For
                End If
            Next i

            If Me.cbxScheme.Text = "" Then Me.cbxScheme.SelectedIndex = m_iActive

            Me.cbxFormatting.SelectedIndex = Me.cbxScheme.SelectedIndex

            oGridList = New BindingList(Of GridRecord)
            For i = 0 To 8
                oRecord = New GridRecord
                oRecord.Level = i + 1
                oRecord.Bold = False
                oRecord.Italics = False
                oRecord.Underline = False
                oRecord.SmallCaps = False
                oRecord.AllCaps = False
                oGridList.Add(oRecord)
            Next
            m_oBindingSource = New BindingSource()
            m_oBindingSource.DataSource = oGridList
            grdFormatting.DataSource = m_oBindingSource
            LoadFormattingDefaults()

            'grdFormatting.DataSource = oGridArray
            'For i = 0 To 8
            '    grdFormatting.Rows.Add(i + 1, 0, 0, 0, 0, 0)
            'Next i
            m_bInitializing = False

            'this was added because some of the control events are firing before the Load event
            m_bLoaded = True
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Function bIsValid() As Boolean
        'returns TRUE only if user has
        'selected valid choices to run the
        '"Mark all" function
        Dim bLevelsOK As Boolean
        Dim bActionsOK As Boolean
        bIsValid = False
        With Me.arCheckLevels
            bLevelsOK = (.ElementAt(1).CheckState + .ElementAt(2).CheckState + .ElementAt(3).CheckState + .ElementAt(4).CheckState + .ElementAt(5).CheckState + .ElementAt(6).CheckState + .ElementAt(7).CheckState + .ElementAt(8).CheckState + .ElementAt(0).CheckState) > 0
        End With

        bActionsOK = (Me.chkFormat.CheckState + Me.chkMarkTC.CheckState + Me.chkUnformat.CheckState + Me.chkUnmark.CheckState) > 0

        If Not bDefinitionIsValid() Then
            Me.txtDefinition.Focus()
            If Mid(Me.txtDefinition.Text, 0, 1) = g_xBullet Then
                '           move cursor to start
                Me.txtDefinition.SelectionStart = 0
            End If
        ElseIf bLevelsOK And bActionsOK Then
            bIsValid = True
        ElseIf Not bLevelsOK Then
            xMsg = "S�lectionnez au moins un niveau pour marquer et/ou formater."
            MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
            Me.arCheckLevels(0).Focus()
        Else
            xMsg = "Vous devez faire une action.  Aucun �l�ment n'est s�lectionn�."
            MsgBox(xMsg, MsgBoxStyle.Exclamation, AppName)
            Me.chkFormat.Focus()
        End If
    End Function
    Private Sub grdFormatting_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdFormatting.CellContentClick
        'GLOG 8764: Make sure SmallCaps and AllCaps checkboxes are mutually exclusive
        Try
            Dim oCell As DataGridViewCell = grdFormatting.Rows(e.RowIndex).Cells(e.ColumnIndex)
            If Not oCell.ReadOnly And TypeOf oCell Is DataGridViewCheckBoxCell Then
                Dim oRecord As GridRecord
                oRecord = m_oBindingSource.List(e.RowIndex)
                If oCell.ColumnIndex = mpFormattingColumns.AllCaps Then
                    If oCell.Value = False Then
                        'Checkbox will toggle to True automatically
                        oRecord.AllCaps = True
                        UpdateFormattingChanges(e.RowIndex, mpFormattingColumns.AllCaps)
                        If grdFormatting.Rows(e.RowIndex).Cells(mpFormattingColumns.SmallCaps).Value = True Then
                            oRecord.SmallCaps = False
                            grdFormatting.UpdateCellValue(mpFormattingColumns.SmallCaps, e.RowIndex)
                            UpdateFormattingChanges(e.RowIndex, mpFormattingColumns.SmallCaps)
                        End If
                        m_oBindingSource.List(e.RowIndex) = oRecord
                    End If
                ElseIf oCell.ColumnIndex = mpFormattingColumns.SmallCaps Then
                    If oCell.Value = False Then
                        'Checkbox will toggle to True automatically
                        oRecord.SmallCaps = True
                        UpdateFormattingChanges(e.RowIndex, mpFormattingColumns.SmallCaps)
                        If grdFormatting.Rows(e.RowIndex).Cells(mpFormattingColumns.AllCaps).Value = True Then
                            oRecord.AllCaps = False
                            grdFormatting.UpdateCellValue(mpFormattingColumns.AllCaps, e.RowIndex)
                            UpdateFormattingChanges(e.RowIndex, mpFormattingColumns.AllCaps)
                        End If
                        m_oBindingSource.List(e.RowIndex) = oRecord
                    End If
                End If
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub grdFormatting_CellValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFormatting.CellValueChanged
        Try
            If Not m_bLoaded Then Exit Sub
            If m_bLoadingDefaults Then Exit Sub
            UpdateFormattingChanges(eventArgs.RowIndex, eventArgs.ColumnIndex)
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub UpdateFormattingChanges(ByVal iRow As Integer, ByVal iCol As Integer)
        Dim iOldFormat As Short
        Dim iNewFormat As Short
        Dim xScheme As String
        Dim iField As mpTCFormatFields
        Dim xKey As String

        'get current heading format
        xScheme = g_xDSchemes(Me.cbxFormatting.SelectedIndex, 0)
        xKey = xScheme & CStr(iRow + 1)
        If m_oFormattingChanges.Contains(xKey) Then
            'from dictionary key
            iOldFormat = CShort(m_oFormattingChanges(xKey))
        Else
            'from list template
            iOldFormat = CShort(xGetLevelProp(xScheme, iRow + 1, mpNumLevelProps.mpNumLevelProp_HeadingFormat, mpSchemeTypes.mpSchemeType_Document))
        End If

        'get target field
        Select Case iCol
            Case mpFormattingColumns.Bold
                iField = mpTCFormatFields.mpTCFormatField_Bold
            Case mpFormattingColumns.Italic
                iField = mpTCFormatFields.mpTCFormatField_Italic
            Case mpFormattingColumns.Underline
                iField = mpTCFormatFields.mpTCFormatField_Underline
            Case mpFormattingColumns.SmallCaps
                iField = mpTCFormatFields.mpTCFormatField_SmallCaps
            Case mpFormattingColumns.AllCaps
                iField = mpTCFormatFields.mpTCFormatField_AllCaps
        End Select

        'toggle target bit
        If iHasTCFormat(iOldFormat, iField) = 0 Then iNewFormat = iField

        'restore other bits
        If (iHasTCFormat(iOldFormat, mpTCFormatFields.mpTCFormatField_Bold) = 1) And (iField <> mpTCFormatFields.mpTCFormatField_Bold) Then
            iNewFormat = iNewFormat Or mpTCFormatFields.mpTCFormatField_Bold
        End If
        If (iHasTCFormat(iOldFormat, mpTCFormatFields.mpTCFormatField_Italic) = 1) And (iField <> mpTCFormatFields.mpTCFormatField_Italic) Then
            iNewFormat = iNewFormat Or mpTCFormatFields.mpTCFormatField_Italic
        End If
        If (iHasTCFormat(iOldFormat, mpTCFormatFields.mpTCFormatField_Underline) = 1) And (iField <> mpTCFormatFields.mpTCFormatField_Underline) Then
            iNewFormat = iNewFormat Or mpTCFormatFields.mpTCFormatField_Underline
        End If
        If (iHasTCFormat(iOldFormat, mpTCFormatFields.mpTCFormatField_SmallCaps) = 1) And (iField <> mpTCFormatFields.mpTCFormatField_SmallCaps) Then
            iNewFormat = iNewFormat Or mpTCFormatFields.mpTCFormatField_SmallCaps
        End If
        If (iHasTCFormat(iOldFormat, mpTCFormatFields.mpTCFormatField_AllCaps) = 1) And (iField <> mpTCFormatFields.mpTCFormatField_AllCaps) Then
            iNewFormat = iNewFormat Or mpTCFormatFields.mpTCFormatField_AllCaps
        End If

        m_oFormattingChanges(xScheme & CStr(iRow + 1)) = CStr(iNewFormat)

    End Sub

    Private Sub optSelectedScheme_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSelectedScheme.CheckedChanged
        Try
            If eventSender.Checked Then
                Try
                    Me.cbxScheme.Focus()
                Catch
                End Try
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Function bDefinitionIsValid() As Boolean
        Dim i As Short
        Dim iLen As Short
        bDefinitionIsValid = False
        '   ensure that sentence is defined by more than just spaces
        iLen = Len(Me.txtDefinition.Text)
        For i = 1 To iLen
            If Mid(Me.txtDefinition.Text, i, 1) <> g_xBullet Then
                Exit For
            ElseIf i = iLen Then
                MsgBox("Une phrase ne peut pas �tre d�finie par un espace. Veuillez entrer au moins un caract�re.", MsgBoxStyle.Exclamation, AppName)
                Exit Function
            End If
        Next i

        '   check for other invalid definitions
        Select Case Me.txtDefinition.Text
            Case ""
                '           no definition
                MsgBox("Veuillez entrer une d�finition pour la phrase.", MsgBoxStyle.Exclamation, AppName)
                Exit Function
            Case "."
                '           period only
                MsgBox("Ceci n'est pas une d�finition valide. Veuillez entrer au moins un espace apr�s le point.", MsgBoxStyle.Exclamation, AppName)
                Exit Function
        End Select

        bDefinitionIsValid = True
    End Function

    Private Sub txtDefinition_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDefinition.KeyUp
        Try
            Dim KeyCode As Short = eventArgs.KeyCode
            Dim Shift As Short = eventArgs.KeyData \ &H10000
            'display space as bullet
            If KeyCode = 32 Then
                With txtDefinition
                    If .SelectionStart > 0 Then
                        'conditional accounts for spacebar being pressed before IME input has been accepted
                        .SelectionStart = .SelectionStart - 1
                        .SelectionLength = 1
                        .SelectedText = g_xBullet
                    End If
                End With
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub LoadFormattingDefaults()
        'sets formatting defaults based on selected scheme
        Dim xScheme As String
        Dim iFormat As Short
        Dim iLevel As Short
        Dim i As Short
        Dim bSetDefaults As Boolean
        Dim xKey As String
        Dim oRow As DataGridViewRow
        Dim oRecord As GridRecord
        Dim iFirstRow As Integer = -1

        m_bLoadingDefaults = True

        xScheme = g_xDSchemes(Me.cbxFormatting.SelectedIndex, 0)
        For iLevel = 0 To 8
            '1/29/13 - set defaults only for enabled rows
            bSetDefaults = False
            If (Me.arCheckLevels(iLevel).CheckState = 1) Then
                If bLevelExists(xScheme, iLevel + 1, mpSchemeTypes.mpSchemeType_Document) Then
                    bSetDefaults = Not bTOCLevelIsStyleBased(xScheme, iLevel + 1)
                End If
            End If
            oRow = grdFormatting.Rows(iLevel)
            oRow.Cells(0).Style.BackColor = IIf(bSetDefaults, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ButtonFace)
            For i = 1 To 5
                oRow.Cells(i).ReadOnly = Not bSetDefaults
                If Not bSetDefaults Then
                    oRow.Cells(i) = New DataGridViewTextBoxCell()
                Else
                    oRow.Cells(i) = New DataGridViewCheckBoxCell()
                End If
                oRow.Cells(i).Style.BackColor = IIf(bSetDefaults, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ButtonFace)
            Next i
            oRecord = m_oBindingSource.List(iLevel)
            If bSetDefaults Then
                If iFirstRow = -1 Then
                    iFirstRow = iLevel
                End If
                oRow.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.WindowText
                'set defaults
                xKey = xScheme & CStr(iLevel + 1)
                If m_oFormattingChanges.Contains(xKey) Then
                    'use updated values
                    iFormat = CShort(m_oFormattingChanges(xKey))
                Else
                    iFormat = CShort(xGetLevelProp(xScheme, iLevel + 1, mpNumLevelProps.mpNumLevelProp_HeadingFormat, mpSchemeTypes.mpSchemeType_Document))
                End If
                oRecord.Bold = iHasTCFormat(iFormat, mpTCFormatFields.mpTCFormatField_Bold) = 1
                oRecord.Italics = iHasTCFormat(iFormat, mpTCFormatFields.mpTCFormatField_Italic) = 1
                oRecord.Underline = iHasTCFormat(iFormat, mpTCFormatFields.mpTCFormatField_Underline) = 1
                oRecord.SmallCaps = iHasTCFormat(iFormat, mpTCFormatFields.mpTCFormatField_SmallCaps) = 1
                oRecord.AllCaps = iHasTCFormat(iFormat, mpTCFormatFields.mpTCFormatField_AllCaps) = 1
            Else
                oRow.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.GrayText
                'level doesn't exist
                oRecord.Bold = Nothing 'False
                oRecord.Italics = Nothing 'False
                oRecord.Underline = Nothing 'False
                oRecord.SmallCaps = Nothing 'False
                oRecord.AllCaps = Nothing 'False
            End If
            m_oBindingSource.List(iLevel) = oRecord
        Next iLevel
        If iFirstRow > -1 Then
            grdFormatting.CurrentCell = grdFormatting.Rows(iFirstRow).Cells(1)
        End If
        grdFormatting.ClearSelection()
        m_bLoadingDefaults = False
    End Sub

    Public ReadOnly Property FormattingChanges() As Hashtable
        Get
            FormattingChanges = m_oFormattingChanges
        End Get
    End Property
    Public ReadOnly Property Levels(Index As Integer) As System.Windows.Forms.CheckBox
        Get
            Levels = arCheckLevels(Index)
        End Get
    End Property

    Private Sub grdFormatting_CellValidated(sender As Object, e As DataGridViewCellEventArgs) Handles grdFormatting.CellValidated
        Dim xProp As String
        Dim iRow As Integer
        Dim iCol As Integer
        Dim oRecord As GridRecord
        Dim bValue As Boolean

        Try
            If (e.RowIndex < 0 Or e.RowIndex > 8 Or e.ColumnIndex = 0 Or e.ColumnIndex > 5) Then
                Exit Sub
            End If
            iRow = e.RowIndex
            iCol = e.ColumnIndex
            If (TypeOf grdFormatting.Rows(iRow).Cells(iCol) Is DataGridViewCheckBoxCell) Then
                bValue = grdFormatting.Rows(iRow).Cells(iCol).EditedFormattedValue
                xProp = grdFormatting.Columns(iCol).DataPropertyName
                oRecord = m_oBindingSource.List(iRow)
                Select Case xProp
                    Case "Bold"
                        oRecord.Bold = bValue
                    Case "Italics"
                        oRecord.Italics = bValue
                    Case "Underline"
                        oRecord.Underline = bValue
                    Case "SmallCaps"
                        oRecord.SmallCaps = bValue
                        If bValue Then
                            oRecord.AllCaps = False
                        End If
                    Case "AllCaps"
                        oRecord.AllCaps = bValue
                        If bValue Then
                            oRecord.SmallCaps = False
                        End If
                    Case Else
                        Exit Sub
                End Select
                'Update binding source
                m_oBindingSource.List(iRow) = oRecord
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnMain_CheckedChanged(sender As Object, e As EventArgs) Handles tbtnMain.CheckedChanged
        Try
            If tbtnMain.Checked Then
                Me.tbtnMain.BackColor = Drawing.SystemColors.ControlDark
                Me.pnlFormatting.Visible = False
                Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
                Me.tbtnMain.Checked = True
                Me.tbtnFormatting.Checked = False
                Me.pnlMain.Visible = True
                System.Windows.Forms.Application.DoEvents()
            Else
                Me.tbtnMain.BackColor = Drawing.SystemColors.ButtonFace
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub tbtnFormatting_CheckedChanged(sender As Object, e As EventArgs) Handles tbtnFormatting.CheckedChanged
        Try
            If tbtnFormatting.Checked Then
                Me.tbtnFormatting.BackColor = Drawing.SystemColors.ButtonHighlight
                Me.pnlMain.Visible = False
                Me.pnlFormatting.Dock = System.Windows.Forms.DockStyle.Fill
                Me.tbtnMain.Checked = False
                Me.tbtnFormatting.Checked = True
                Me.pnlFormatting.Visible = True
                System.Windows.Forms.Application.DoEvents()
            Else
                Me.tbtnFormatting.BackColor = Drawing.SystemColors.ButtonFace
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub frmMarkAll_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Try
            'Close form when Esc key pressed
            If e.KeyCode = Keys.Escape Then
                btnCancel.PerformClick()
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub grdFormatting_CellStateChanged(sender As Object, e As DataGridViewCellStateChangedEventArgs) Handles grdFormatting.CellStateChanged
        Static iLastSelectedRow As Integer = -1
        Static iLastSelectedColumn As Integer = -1
        If m_bLoadingDefaults Then
            Exit Sub
        End If
        Try
            Dim oCell As DataGridViewCell = e.Cell
            If oCell Is Nothing Or e.StateChanged <> DataGridViewElementStates.Selected Then
                Exit Sub
            End If
            If oCell.RowIndex = iLastSelectedRow And oCell.ColumnIndex = iLastSelectedColumn Then
                Exit Sub
            End If
            If oCell.ReadOnly And oCell.Selected Then
                grdFormatting.ClearSelection()
                If iLastSelectedRow > -1 And iLastSelectedColumn > -1 Then
                    grdFormatting.CurrentCell = grdFormatting.Rows(iLastSelectedRow).Cells(iLastSelectedColumn)
                    grdFormatting.CurrentCell.Selected = True
                End If
            ElseIf Not oCell.ReadOnly Then
                iLastSelectedRow = oCell.RowIndex
                iLastSelectedColumn = oCell.ColumnIndex
            End If
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

End Class