<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMarkPrompt
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdYesAll As System.Windows.Forms.Button
	Public WithEvents cmdNoAll As System.Windows.Forms.Button
	Public WithEvents cmdYes As System.Windows.Forms.Button
	Public WithEvents cmdNo As System.Windows.Forms.Button
	Public WithEvents cmdSkip As System.Windows.Forms.Button
	Public WithEvents Picture1 As System.Windows.Forms.PictureBox
	Public WithEvents lblMessage As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMarkPrompt))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdYesAll = New System.Windows.Forms.Button()
        Me.cmdNoAll = New System.Windows.Forms.Button()
        Me.cmdYes = New System.Windows.Forms.Button()
        Me.cmdNo = New System.Windows.Forms.Button()
        Me.cmdSkip = New System.Windows.Forms.Button()
        Me.Picture1 = New System.Windows.Forms.PictureBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.Location = New System.Drawing.Point(305, 171)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdCancel.Size = New System.Drawing.Size(95, 29)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'cmdYesAll
        '
        Me.cmdYesAll.BackColor = System.Drawing.SystemColors.Control
        Me.cmdYesAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdYesAll.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdYesAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdYesAll.Location = New System.Drawing.Point(91, 172)
        Me.cmdYesAll.Name = "cmdYesAll"
        Me.cmdYesAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdYesAll.Size = New System.Drawing.Size(95, 29)
        Me.cmdYesAll.TabIndex = 3
        Me.cmdYesAll.Text = "Yes to &All"
        Me.cmdYesAll.UseVisualStyleBackColor = False
        '
        'cmdNoAll
        '
        Me.cmdNoAll.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNoAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNoAll.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdNoAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNoAll.Location = New System.Drawing.Point(198, 171)
        Me.cmdNoAll.Name = "cmdNoAll"
        Me.cmdNoAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNoAll.Size = New System.Drawing.Size(95, 29)
        Me.cmdNoAll.TabIndex = 4
        Me.cmdNoAll.Text = "N&o to All"
        Me.cmdNoAll.UseVisualStyleBackColor = False
        '
        'cmdYes
        '
        Me.cmdYes.BackColor = System.Drawing.SystemColors.Control
        Me.cmdYes.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdYes.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdYes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdYes.Location = New System.Drawing.Point(91, 137)
        Me.cmdYes.Name = "cmdYes"
        Me.cmdYes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdYes.Size = New System.Drawing.Size(95, 29)
        Me.cmdYes.TabIndex = 0
        Me.cmdYes.Text = "&Yes"
        Me.cmdYes.UseVisualStyleBackColor = False
        '
        'cmdNo
        '
        Me.cmdNo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdNo.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdNo.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdNo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNo.Location = New System.Drawing.Point(198, 136)
        Me.cmdNo.Name = "cmdNo"
        Me.cmdNo.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdNo.Size = New System.Drawing.Size(95, 29)
        Me.cmdNo.TabIndex = 1
        Me.cmdNo.Text = "&No"
        Me.cmdNo.UseVisualStyleBackColor = False
        '
        'cmdSkip
        '
        Me.cmdSkip.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSkip.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdSkip.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSkip.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdSkip.Location = New System.Drawing.Point(305, 136)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdSkip.Size = New System.Drawing.Size(95, 29)
        Me.cmdSkip.TabIndex = 2
        Me.cmdSkip.Text = "&Skip Paragraph"
        Me.cmdSkip.UseVisualStyleBackColor = False
        '
        'Picture1
        '
        Me.Picture1.BackColor = System.Drawing.SystemColors.Control
        Me.Picture1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Picture1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Picture1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Picture1.Image = CType(resources.GetObject("Picture1.Image"), System.Drawing.Image)
        Me.Picture1.Location = New System.Drawing.Point(14, 17)
        Me.Picture1.Name = "Picture1"
        Me.Picture1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Picture1.Size = New System.Drawing.Size(39, 45)
        Me.Picture1.TabIndex = 6
        Me.Picture1.TabStop = False
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.SystemColors.Control
        Me.lblMessage.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblMessage.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblMessage.Location = New System.Drawing.Point(70, 14)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblMessage.Size = New System.Drawing.Size(347, 115)
        Me.lblMessage.TabIndex = 7
        Me.lblMessage.Text = "Label1"
        '
        'frmMarkPrompt
        '
        Me.AcceptButton = Me.cmdYes
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(427, 212)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdYesAll)
        Me.Controls.Add(Me.cmdNoAll)
        Me.Controls.Add(Me.cmdYes)
        Me.Controls.Add(Me.cmdNo)
        Me.Controls.Add(Me.cmdSkip)
        Me.Controls.Add(Me.Picture1)
        Me.Controls.Add(Me.lblMessage)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(4, 28)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMarkPrompt"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Prompt for Mark"
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 
End Class