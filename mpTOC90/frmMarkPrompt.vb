Option Strict Off
Option Explicit On
Imports LMP.Numbering.TOC.mpTOC
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cNumTOC
Imports LMP

Friend Class frmMarkPrompt
	Inherits System.Windows.Forms.Form
	Public m_iRetval As Short
    Private m_iMode As mpMarkingModes
	Private m_bIsAmbiguousHeading As Boolean
	
    Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.Cancel
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdNo_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNo.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.Retry
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdNoAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNoAll.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.No
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdSkip_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSkip.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.Ignore
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdYes_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdYes.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.Abort
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Private Sub cmdYesAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdYesAll.Click
        Try
            Me.Hide()
            m_iRetval = MsgBoxResult.Yes
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
	Private Sub frmMarkPrompt_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim xMsg As String

        Try
            With Me
                .Text = AppName
                If m_bIsAmbiguousHeading Then
                    xMsg = "Treat the selected sentence as a heading?"
                    If m_iMode = mpMarkingModes.mpMarkingMode_TCCodes Then
                        'this text isn't appropriate for style separators
                        xMsg = xMsg & vbCrLf & vbCrLf & "If you choose No, another attempt " & "will be made to find an appropriate heading in this paragraph.  " & "If none can be found, the whole paragraph will be " & "marked/formatted as a heading."
                    End If
                ElseIf m_iMode = mpMarkingModes.mpMarkingMode_StyleSeparators Then
                    xMsg = "Replace TC code in TSG numbered paragraph with style separator?"
                Else
                    xMsg = "Replace style separator in TSG numbered paragraph with TC code?"
                End If

                .lblMessage.Text = xMsg

                'shrink dialog to account for shorter message
                If (m_iMode = mpMarkingModes.mpMarkingMode_StyleSeparators) Or Not m_bIsAmbiguousHeading Then
                    .lblMessage.Top = TwipsToPixelsY(250)
                    .cmdSkip.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdSkip.Top) - 750)
                    .cmdCancel.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdCancel.Top) - 750)
                    .cmdNo.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdNo.Top) - 750)
                    .cmdNoAll.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdNoAll.Top) - 750)
                    .cmdYes.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdYes.Top) - 750)
                    .cmdYesAll.Top = TwipsToPixelsY(PixelsToTwipsY(.cmdYesAll.Top) - 750)
                    .cmdSkip.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdSkip.Left) + 225)
                    .cmdCancel.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdCancel.Left) + 225)
                    .cmdNo.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdNo.Left) + 225)
                    .cmdNoAll.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdNoAll.Left) + 225)
                    .cmdYes.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdYes.Left) + 225)
                    .cmdYesAll.Left = TwipsToPixelsX(PixelsToTwipsX(.cmdYesAll.Left) + 225)
                    .Height = TwipsToPixelsY(PixelsToTwipsY(.Height) - 750)
                End If
            End With
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
	
    Public Property MarkingMode() As mpMarkingModes
        Get
            MarkingMode = m_iMode
        End Get
        Set(ByVal Value As mpMarkingModes)
            m_iMode = Value
        End Set
    End Property
	
	
	Public Property IsAmbiguousHeading() As Boolean
		Get
			IsAmbiguousHeading = m_bIsAmbiguousHeading
		End Get
		Set(ByVal Value As Boolean)
			m_bIsAmbiguousHeading = Value
		End Set
	End Property
End Class