Option Strict Off
Option Explicit On

Imports LMP

Friend Class frmTOCEntryLevelFrench
    Inherits System.Windows.Forms.Form
    Public Cancelled As Boolean

    Private Sub btnCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Cancelled = True
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles btnOK.Click
        Try
            Me.Cancelled = False
            Me.Hide()
            System.Windows.Forms.Application.DoEvents()
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
    Private Sub frmTOCEntryLevel_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim xarTOCLevels As Array
        Dim i As Short

        Try
            Me.Cancelled = True

            '   fill Tab Levels array
            xarTOCLevels = New String(8, 1) {}
            For i = 0 To 8
                xarTOCLevels.SetValue(CStr(i + 1), i, 0)
                xarTOCLevels.SetValue(CStr(i + 1), i, 1)
            Next i

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.cbxTOCLevels.Array. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.cbxTOCLevels.SetList(xarTOCLevels)
            Me.cbxTOCLevels.SelectedIndex = 0
        Catch oE As Exception
            [Error].Show(oE)
        End Try
    End Sub
End Class