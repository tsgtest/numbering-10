Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.TOC.mpTypes
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cRange
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.TOC.mpApplication

Namespace LMP.Numbering.TOC
    Friend Class mpDocument
        Friend Shared Function secCreateSection(rngLocation As Word.Range, _
                                  Optional bLinkHeadersFooters As Boolean = False, _
                                  Optional bAllowTrailer As Boolean = True, _
                                  Optional bRestartPageNumbers As Boolean = True, _
                                  Optional bRetagMP10HeadersFooters As Boolean = False) As Word.Section
            'create section at start of range rngLocation
            'bRetagMP10HeadersFooters parameter added in 9.9.5008
            Dim secNew As Word.Section
            Dim iNewSec As Integer
            Dim rngNextChr As Word.Range
            Dim rngPrevChr As Word.Range
            Dim ftrExisting As HeaderFooter
            Dim iPageStart As Integer
            Dim secNext As Word.Section
            Dim bParaAdded As Boolean

            With rngLocation
                '           create different section breaks based on location
                If .Start = curwordapp.ActiveDocument.Content.Start Then
                    iNewSec = .Sections.First.Index ' .Information(wdActiveEndSectionNumber)
                    '               get current starting page number of section one
                    With CurWordApp.ActiveDocument.Sections(1) _
                            .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers
                        If .RestartNumberingAtSection = True Then
                            iPageStart = .StartingNumber
                        Else
                            iPageStart = 1
                        End If
                    End With

                    'if there's a table at start of doc, add a paragraph before to prevent
                    'issue with xml tags
                    If .Tables.Count > 0 Then
                        rngAddParaBeforeTable(.Tables(1))
                        bParaAdded = True
                    End If

                    .InsertBreak(WdBreakType.wdSectionBreakNextPage)

                    'delete added para
                    If bParaAdded Then _
                        CurWordApp.ActiveDocument.Sections(2).Range.Characters.First.Delete()

                    CurWordApp.ActiveDocument.Sections(1).Range.Style = WdBuiltinStyle.wdStyleNormal
                    bRet = bUnlinkHeadersFooters(CurWordApp.ActiveDocument.Sections(2))

                    'GLOG 5264 - restore mp10 bookmarks
                    If g_bIsMP10 Then _
                        RestoreMP10BookmarksAfterUnlinking(CurWordApp.ActiveDocument.Sections(2), False)

                    '               restart page numbering in what is now section two
                    With CurWordApp.ActiveDocument.Sections(2) _
                            .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers
                        .RestartNumberingAtSection = bRestartPageNumbers 'GLOG 5609
                        .StartingNumber = iPageStart
                    End With
                ElseIf .Start = CurWordApp.ActiveDocument.Content.End - 1 Then
                    iNewSec = .Sections.First.Index + 1 ' .Information(wdActiveEndSectionNumber) + 1
                    .InsertBreak(WdBreakType.wdSectionBreakNextPage)
                    .Previous(WdUnits.wdParagraph).Style = WdBuiltinStyle.wdStyleNormal
                Else
                    rngPrevChr = CurWordApp.ActiveDocument.Range(.End - 1, .End)
                    rngNextChr = CurWordApp.ActiveDocument.Range(.End, .End + 1)

                    iNewSec = .Sections.First.Index ' .Information(wdActiveEndSectionNumber)

                    '               delete succeeding manual page breaks
                    'GLOG 5082 - moved this code up to before insertion of section break
                    If bIsPageBreak(rngNextChr) Then
                        If rngNextChr.Paragraphs(1).Range.Text = Chr(12) & vbCr Then
                            'GLOG 5082 - delete the entire paragraph if all it contains
                            'is the page break
                            rngNextChr.Paragraphs(1).Range.Delete()
                        Else
                            rngNextChr.Delete()
                        End If
                    End If

                    '               delete preceding manual page breaks
                    If bIsPageBreak(rngPrevChr) Then
                        rngPrevChr.Delete()
                    End If

                    If rngPrevChr.Text <> Chr(12) Then
                        iNewSec = iNewSec + 1
                        .InsertBreak(WdBreakType.wdSectionBreakNextPage)
                        '---ensure normal format
                        .MoveEnd(WdUnits.wdCharacter, -1)
                        .Style = WdBuiltinStyle.wdStyleNormal
                        .MoveEnd(WdUnits.wdCharacter, 1)
                    End If

                    rngNextChr = CurWordApp.ActiveDocument.Range(.End, .End + 1)

                    ''               delete succeeding manual page breaks
                    '                If bIsPageBreak(rngNextChr) Then
                    '                    rngNextChr.Delete
                    '                    Set rngNextChr = ActiveDocument.Range(.End, .End + 1)
                    '                End If

                    If rngNextChr.Text <> Chr(12) Then
                        .InsertBreak(WdBreakType.wdSectionBreakNextPage)
                    End If

                    '               ensure that heads/foots are unlinked
                    '               in subsequent section
                    secNext = CurWordApp.ActiveDocument.Sections(iNewSec + 1)
                    bUnlinkHeadersFooters(secNext)

                    'GLOG 5264 - restore mp10 bookmarks
                    'remmed in 9.9.5007 - we should be calling this only
                    'when inserting the TOC at start of document
                    '9.9.5008 - we do need to do this when inserting at
                    'insertion point, in case we're splitting a segment
                    If g_bIsMP10 And bRetagMP10HeadersFooters Then _
                        RestoreMP10BookmarksAfterUnlinking(secNext, True)

                    '               restart page numbering in subsequent section
                    If bRestartPageNumbers Then 'GLOG 5609
                        For Each ftrExisting In secNext.Footers
                            With ftrExisting.PageNumbers
                                .RestartNumberingAtSection = True
                                .StartingNumber = 1
                            End With
                        Next ftrExisting
                    End If
                End If
                secNew = CurWordApp.ActiveDocument.Sections(iNewSec)
            End With

            If Not bLinkHeadersFooters Then _
                bRet = bUnlinkHeadersFooters(secNew)

            ClearContent(bAllSections:=False, _
                          secCurSection:=secNew, _
                          bHeaders:=True, _
                          bFooters:=True)

            '       set restart of page numbers
            For Each ftrExisting In secNew.Footers
                ftrExisting.PageNumbers _
                    .RestartNumberingAtSection = bRestartPageNumbers
            Next ftrExisting

            '        If Not bAllowTrailer Then _
            '            mpTrailer.bMarkForNoTrailer secNew

            secCreateSection = secNew

        End Function

        Friend Shared Function iInsertAtMultiple(ByVal xLabel As String, _
                     Optional ByVal xText As String = "", _
                     Optional ByVal xBPRange As String = "", _
                     Optional iLineLength As Integer = 0, _
                     Optional bDeletePara As Boolean = False, _
                     Optional ByVal vSearch As Object = Nothing, _
                     Optional xDefaultText As String = "") As Integer

            '   runs rngInsertAt for each
            '   occurrence of xLabel
            Dim rngFound As Word.Range
            Dim i As Integer

            '   dummy range to get started in loop
            rngFound = CurWordApp.ActiveDocument.Content

            While Not (rngFound Is Nothing)
                rngFound = rngInsertAt(xLabel, _
                                           xText, _
                                           xBPRange, _
                                           iLineLength, _
                                           bDeletePara, _
                                           vSearch, _
                                           xDefaultText)
                i = i + 1
            End While

            '   return number of finds
            iInsertAtMultiple = i - 1
        End Function

        Friend Shared Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
            With docDoc
                If InStr(UCase(.AttachedTemplate), "PLEAD") Or _
                        InStr(UCase(.AttachedTemplate), TemplatePOS) Or _
                        InStr(UCase(.AttachedTemplate), "VER") Then
                    bIsPleadingDocument = True
                Else
                    bIsPleadingDocument = False
                End If
            End With
        End Function


        Friend Shared Sub ClearContent(Optional bAllSections As Boolean = True, _
                        Optional secCurSection As Word.Section = Nothing, _
                        Optional bMainStory As Boolean = False, _
                        Optional bHeaders As Boolean = False, _
                        Optional bFooters As Boolean = False)

            'clears contents of specified stories
            'of specified sections

            '/////////MODIFIED COMPLETELY
            Dim hdrHeader As HeaderFooter
            Dim ftrFooter As HeaderFooter
            Dim secSection As Word.Section
            Dim fldLoc As Word.Field
            Dim bClearShapes As Boolean

            If bAllSections Then
                'delete shapes first
                DeleteHeaderFooterShapes(, bHeaders, bFooters)

                For Each secSection In CurWordApp.ActiveDocument.Sections
                    With secSection
                        If bMainStory Then
                            With .Range
                                .Delete()
                                For Each fldLoc In .Fields
                                    fldLoc.Delete()
                                Next fldLoc
                            End With
                        End If

                        If bHeaders Then
                            For Each hdrHeader In .Headers
                                With hdrHeader.Range
                                    .Delete()
                                    For Each fldLoc In .Fields
                                        fldLoc.Delete()
                                    Next fldLoc
                                End With
                            Next hdrHeader
                        End If

                        If bFooters Then
                            For Each ftrFooter In .Footers
                                With ftrFooter.Range
                                    .Delete()
                                    For Each fldLoc In .Fields
                                        fldLoc.Delete()
                                    Next fldLoc
                                End With
                            Next ftrFooter
                        End If
                    End With
                Next secSection
            Else
                With secCurSection
                    If bMainStory Then
                        With .Range
                            .Delete()
                            For Each fldLoc In .Fields
                                fldLoc.Delete()
                            Next fldLoc
                        End With
                    End If

                    'delete shapes first
                    '2/19/13 - deleting shapes here is causing Word to crash in documents
                    'with 2013 compatibility mode and doesn't appear to be necessary
                    '(at least for pleading paper)
                    'GLOG 5413/5414 - always skip this code in Word 2013 - it was also
                    'crashing in mp9 pleadings in earlier Word compatibility modes
                    If (InStr(CurWordApp.Version, "15.") <> 0) Or _
                            (InStr(CurWordApp.Version, "16.") <> 0) Then
                        bClearShapes = False
                    Else
                        bClearShapes = True
                    End If
                    If bClearShapes Then _
                        DeleteHeaderFooterShapes(.Index, bHeaders, bFooters)

                    If bHeaders Then
                        For Each hdrHeader In .Headers
                            With hdrHeader.Range
                                .Delete()
                                For Each fldLoc In .Fields
                                    fldLoc.Delete()
                                Next fldLoc

                                'GLOG 5083 - table may not get deleted automatically when
                                'there's no trailing paragraph due to corruption
                                If .Tables.Count Then
                                    .Tables(1).Delete()
                                    .StartOf()
                                    .InsertParagraphAfter()
                                End If
                            End With
                        Next hdrHeader
                    End If

                    If bFooters Then
                        For Each ftrFooter In .Footers
                            With ftrFooter.Range
                                .Delete()
                                For Each fldLoc In .Fields
                                    fldLoc.Delete()
                                Next fldLoc

                                'GLOG 5083 - table may not get deleted automatically when
                                'there's no trailing paragraph due to corruption
                                If .Tables.Count Then
                                    .Tables(1).Delete()
                                    .StartOf()
                                    .InsertParagraphAfter()
                                End If
                            End With
                        Next ftrFooter
                    End If
                End With
            End If
        End Sub

        Friend Shared Sub DeleteAllBookmarks(Optional bMacPacOnly _
                            As Boolean = False)

            '-7/29 this is a slightly different version, which
            '-leaves zzmpFixed - prefixed bookmarks alone, as w/ docvars

            '   deletes all bookmarks or all
            '   MacPac (zzmp) bookmarks

            '---safety if boilerplate doc

            If InStr(UCase(CurWordApp.ActiveDocument.Name), ".ATE") Then
                Exit Sub
            End If

            Dim bmkBookmark As Bookmark

            If bMacPacOnly Then
                For Each bmkBookmark In CurWordApp.ActiveDocument.Bookmarks
                    If Left(bmkBookmark.Name, 4) = "zzmp" Then _
                        If InStr(bmkBookmark.Name, "zzmpFixed") = 0 Then _
                            bmkBookmark.Delete()
                Next bmkBookmark
            Else
                For Each bmkBookmark In CurWordApp.ActiveDocument.Bookmarks
                    bmkBookmark.Delete()
                Next bmkBookmark
            End If
        End Sub

        Friend Shared Function envGetDocEnvironment() As mpDocEnvironment
            'fills type mpDocEnvironment with
            'current doc environment settings -
            'envMpDoc below is a global var

            '   some properties are not available
            '   in preview mode - this will cause
            '   error - hence turn off trapping
            On Error Resume Next

            envMpDoc.iProtectionType = CurWordApp.ActiveDocument.ProtectionType
            envMpDoc.bTrackChanges = CurWordApp.ActiveDocument.TrackRevisions

            '   get selection parameters
            With CurWordApp.Selection
                envMpDoc.iSelectionStory = .StoryType
                envMpDoc.lSelectionStartPos = .Start
                envMpDoc.lSelectionEndPos = .End
            End With

            With CurWordApp.ActiveDocument.ActiveWindow.ActivePane
                envMpDoc.sVScroll = .VerticalPercentScrolled
                envMpDoc.sHScroll = .HorizontalPercentScrolled
                With .View
                    envMpDoc.bShowAll = .ShowAll
                    envMpDoc.bFieldCodes = .ShowFieldCodes
                    envMpDoc.bBookmarks = .ShowBookmarks
                    envMpDoc.bTabs = .ShowTabs
                    envMpDoc.bSpaces = .ShowSpaces
                    envMpDoc.bParagraphs = .ShowParagraphs
                    envMpDoc.bHyphens = .ShowHyphens
                    envMpDoc.bHiddenText = .ShowHiddenText
                    envMpDoc.bTextBoundaries = .ShowTextBoundaries
                    envMpDoc.iView = .Type
                End With

            End With

            envGetDocEnvironment = envMpDoc

        End Function

        Friend Shared Function rngInsertAt(ByVal xLabel As String, _
                     Optional ByVal xText As String = "", _
                     Optional ByVal xBPRange As String = "", _
                     Optional iLineLength As Integer = 0, _
                     Optional bDeletePara As Boolean = False, _
                     Optional ByVal vSearch As Object = Nothing, _
                     Optional xDefaultText As String = "") As Word.Range

            'replaces first instance of label xLabel
            'with xBPRange entry or xText -
            'if xText and xBPRange is empty, replaces with line
            'of iLineLength characters.  Deletes entire para
            'if specified and xText is empty and no line length = 0.
            'Returns range of inserted text. Use return value to format.

            Dim rngFind As Word.Range

            If vSearch Is Nothing Then
                rngFind = CurWordApp.ActiveDocument.Content
            Else
                rngFind = vSearch
            End If

            With rngFind.Find
                .Text = xLabel
                .Execute()
                If .Found Then
                    If xBPRange = "" Then
                        '               use default text, if no xText
                        If xText = "" Then _
                            xText = xDefaultText

                        '               insert text or underline at
                        '               label if still no text
                        If xText = "" Then
                            xText = New String("_", iLineLength)
                            rngFind.Text = xText

                            '                   delete para if specified
                            '                   and no line specified
                            If iLineLength = 0 And bDeletePara Then _
                                rngFind.Paragraphs(1).Range.Text = ""

                        Else
                            rngFind.Text = xText
                        End If
                    Else
                        '               insert boilerplate entry at label
                        rngFind.InsertFile(xBP, xBPRange, , , False)
                    End If

                    rngInsertAt = rngFind
                Else
                    rngInsertAt = Nothing
                End If
            End With
        End Function
        Friend Shared Function bSetDocEnvironment(envCur As mpDocEnvironment, _
                                    Optional bScroll As Boolean = False, _
                                    Optional bSelect As Boolean = False) As Boolean
            '   sets the doc/window related vars
            '   based on type mpDocEnvironment var (global)

            Dim rngStory As Word.Range

            '   some properties are not available
            '   in preview mode - this will cause
            '   error - hence turn off trapping
            On Error Resume Next
            With CurWordApp.ActiveWindow.ActivePane
                With .View
                    .ShowAll = envCur.bShowAll
                    .ShowFieldCodes = envCur.bFieldCodes
                    .ShowBookmarks = envCur.bBookmarks
                    .ShowTabs = envCur.bTabs
                    .ShowSpaces = envCur.bSpaces
                    .ShowParagraphs = envCur.bParagraphs
                    .ShowHyphens = envCur.bHyphens
                    .ShowHiddenText = envCur.bHiddenText
                    .ShowTextBoundaries = envCur.bTextBoundaries
                    .Type = envCur.iView
                End With

                If bSelect Then
                    With envCur
                        rngStory = CurWordApp.ActiveDocument.StoryRanges(.iSelectionStory)
                        rngStory.SetRange(.lSelectionStartPos, .lSelectionEndPos)
                        rngStory.Select()
                        '               this needs to happen again here
                        '               because selecting the headers/footers
                        '               automatically puts you in normal view.
                        CurWordApp.ActiveWindow.View.Type = .iView
                    End With
                End If

                If bScroll Then
                    .VerticalPercentScrolled = envCur.sVScroll
                    .HorizontalPercentScrolled = envCur.sHScroll
                End If

            End With

            CurWordApp.ActiveDocument.TrackRevisions = envCur.bTrackChanges

            bSetDocEnvironment = True

        End Function
        Friend Shared Function bUnlinkHeadersFooters(xSection As Word.Section) As Boolean

            On Error Resume Next
            With xSection
                With .Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                    .LinkToPrevious = False
                End With
                With .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                    .LinkToPrevious = False
                End With
                With .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                    .LinkToPrevious = False
                End With
                With .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                    .LinkToPrevious = False
                End With
            End With

        End Function

        Friend Shared Function bQueryHFLink(xHeader As Boolean, _
                            secSection As Word.Section, _
                            vHF As Object) As Boolean

            Select Case xHeader
                Case True
                    With secSection.Headers(vHF)
                        bQueryHFLink = .LinkToPrevious
                    End With
                Case Else
                    With secSection.Footers(vHF)
                        bQueryHFLink = .LinkToPrevious
                    End With
            End Select


        End Function
        Friend Shared Function bHFIsLinked(hfExisting As HeaderFooter) As Boolean
            bHFIsLinked = hfExisting.LinkToPrevious
        End Function

        Friend Shared Function bUnlinkAllHeaderFooters(secSection As Word.Section) As Boolean

            On Error Resume Next
            With secSection
                .Headers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).LinkToPrevious = False
                .Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).LinkToPrevious = False

                .Footers(WdHeaderFooterIndex.wdHeaderFooterFirstPage).LinkToPrevious = False
                .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).LinkToPrevious = False
            End With

        End Function

        Friend Shared Function rngAdjustHeaderTabs(rngHeader As Word.Range) As Word.Range
            'resets default header tabs
            'call after implementing user's margins
            Dim sRight As Single

            With rngHeader
                With .Sections(1).PageSetup
                    'GLOG 8814 (dm) - match right tab in body
                    sRight = .PageWidth - .LeftMargin - _
                        .RightMargin - .Gutter - CurWordApp.InchesToPoints(0.05)
                End With

                With .ParagraphFormat.TabStops
                    .ClearAll()
                    .Add(sRight, WdTabAlignment.wdAlignTabRight)
                    .Add(sRight / 2, WdTabAlignment.wdAlignTabCenter)
                End With
            End With

            rngAdjustHeaderTabs = rngHeader

        End Function

        Friend Shared Sub AdjustCenteredFooter(rngFooter As Range)
            'in a 3-column table, splits the available space
            'equally between columns 1 and 3
            Dim sWidth As Single

            With rngFooter
                If .Tables.Count <> 1 Then Exit Sub

                With .Sections(1).PageSetup
                    sWidth = .PageWidth - (.LeftMargin + .RightMargin + .Gutter) - 3
                End With

                With .Tables(1)
                    If (.Columns.Count <> 3) Or _
                            (.Rows(1).Alignment <> WdRowAlignment.wdAlignRowCenter) Then
                        Exit Sub
                    End If

                    sWidth = sWidth - .Columns(2).Width
                    .Columns(1).Width = sWidth / 2
                    .Columns(3).Width = sWidth / 2
                End With
            End With
        End Sub

        Private Shared Sub DeleteHeaderFooterShapes(Optional ByVal lSection As Long = 0, _
                                             Optional ByVal bDoHeaders As Boolean = True, _
                                             Optional ByVal bDoFooters As Boolean = True)
            Dim i As Integer
            Dim oShapes As Word.Shapes
            Dim bDo As Boolean
            Dim iStoryType As WdStoryType

            'there's a single shapes collection for all headers and footers
            oShapes = CurWordApp.ActiveDocument.Sections(1).Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary).Shapes

            For i = oShapes.Count To 1 Step -1
                'validate section
                bDo = ((lSection = 0) Or (oShapes(i).Anchor.Sections.First.Index = lSection))

                'validate story type
                If bDo Then
                    iStoryType = oShapes(i).Anchor.StoryType
                    Select Case iStoryType
                        Case WdStoryType.wdEvenPagesHeaderStory, WdStoryType.wdFirstPageHeaderStory, WdStoryType.wdPrimaryHeaderStory
                            bDo = bDoHeaders
                        Case WdStoryType.wdEvenPagesFooterStory, WdStoryType.wdFirstPageFooterStory, WdStoryType.wdPrimaryFooterStory
                            bDo = bDoFooters
                        Case Else
                            bDo = False
                    End Select
                End If

                'delete if appropriate
                If bDo Then
                    oShapes(i).Delete()
                End If
            Next i
        End Sub

        Friend Shared Function rngAddParaBeforeTable(tblFirst As Word.Table) As Word.Range
            'creates a paragraph immediately
            'before table tblFirst - use with
            'tables that are at bof.
            Dim rngStart As Word.Range

            With tblFirst
                .Rows.Add(.Rows(1))

                rngStart = .Range
                rngStart.StartOf()
                'Make sure any automatic XML Tags are cleared
                .Rows(1).Range.Delete()
                .Rows(1).ConvertToText(vbTab)
            End With
            rngStart.MoveEndUntil(vbCr)
            If rngStart.Text <> "" Then     '#3648
                rngStart.Delete()
            End If
            rngAddParaBeforeTable = rngStart
        End Function

        Private Shared Sub RestoreMP10BookmarksAfterUnlinking(ByVal oSection As Word.Section, _
                                                       ByVal bRetagSource As Boolean)
            'GLOG 5264 - after unlinking, bookmarks will be in the preceding section -
            'this method can be used to restore them to oSection after inserting a new section before
            'bRetagSource parameter added in 9.9.5008
            Dim oHeadFoot As Word.HeaderFooter
            Dim oRange As Word.Range
            Dim oSourceSection As Word.Section
            Dim oBmk As Word.Bookmark
            Dim oBmkRange As Word.Range
            Dim oShape As Word.Shape
            Dim oTextRange As Word.Range
            Dim i As Integer
            Dim oBmkTextRange As Word.Range

            If oSection.Index = 1 Then _
                Exit Sub

            oSourceSection = CurWordApp.ActiveDocument.Sections(oSection.Index - 1)

            'headers
            For Each oHeadFoot In oSourceSection.Headers
                oRange = oHeadFoot.Range
                TransferSegmentBookmarks(oRange, oSection.Headers(oHeadFoot.Index).Range, _
                    bRetagSource)

                'shapes
                For i = 1 To oRange.ShapeRange.Count
                    oShape = oRange.ShapeRange(i)
                    If oShape.Type = MsoShapeType.msoTextBox Then
                        oTextRange = oShape.TextFrame.TextRange
                        'GLOG 5301 (dm) - target shape may not exist in oSection
                        oBmkTextRange = Nothing
                        On Error Resume Next
                        oBmkTextRange = oSection.Headers(oHeadFoot.Index) _
                            .Range.ShapeRange(i).TextFrame.TextRange
                        On Error GoTo 0
                        If Not oBmkTextRange Is Nothing Then
                            TransferSegmentBookmarks(oTextRange, oBmkTextRange, _
                                bRetagSource)
                        End If
                    End If
                Next i
            Next oHeadFoot

            'footers
            For Each oHeadFoot In oSourceSection.Footers
                oRange = oHeadFoot.Range
                TransferSegmentBookmarks(oRange, oSection.Footers(oHeadFoot.Index).Range, _
                    bRetagSource)

                'shapes
                For i = 1 To oRange.ShapeRange.Count
                    oShape = oRange.ShapeRange(i)
                    If oShape.Type = MsoShapeType.msoTextBox Then
                        oTextRange = oShape.TextFrame.TextRange
                        'GLOG 5301 (dm) - target shape may not exist in oSection
                        oBmkTextRange = Nothing
                        On Error Resume Next
                        oBmkTextRange = oSection.Footers(oHeadFoot.Index) _
                            .Range.ShapeRange(i).TextFrame.TextRange
                        On Error GoTo 0
                        If Not oBmkTextRange Is Nothing Then
                            TransferSegmentBookmarks(oTextRange, oBmkTextRange, _
                                bRetagSource)
                        End If
                    End If
                Next i
            Next oHeadFoot
        End Sub

        Private Shared Sub RetagmSEG(ByVal oRange As Word.Range, ByVal xOldTag As String)
            'added in 9.9.5008 (GLOG 5264)
            Dim oDoc As Word.Document
            Dim xOldID As String
            Dim xNewID As String
            Dim xValue As String
            Dim iIndex As Integer
            Dim xVar As String
            Dim bShowHidden As Boolean
            Dim xNewTag As String

            oDoc = CurWordApp.ActiveDocument

            'retag
            xOldID = Mid$(xOldTag, 4, 8)
            xNewID = GenerateDocVarID(oDoc)
            xNewTag = Left$(xOldTag, 3) & xNewID & Mid$(xOldTag, 12)

            'add new doc var with same value as original one
            xValue = oDoc.Variables("mpo" & xOldID).Value
            oDoc.Variables.Add("mpo" & xNewID, xValue)

            'add new mpd doc vars as necessary
            If xOldTag Like "mps*" Then
                xValue = "x"
                While xValue <> ""
                    xValue = ""
                    iIndex = iIndex + 1
                    xVar = "mpd" & xOldID & New String("0", 2 - Len(CStr(iIndex))) & CStr(iIndex)
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo 0
                    If xValue <> "" Then
                        oDoc.Variables.Add("mpd" & xNewID & _
                            New String("0", 2 - Len(CStr(iIndex))) & CStr(iIndex), xValue)
                    End If
                End While
            End If

            'replace bookmark
            With oDoc.Bookmarks
                bShowHidden = .ShowHidden
                .ShowHidden = True
                .Add("_" & xNewTag, oRange)
                .ShowHidden = bShowHidden
            End With
        End Sub

        Private Shared Function GenerateDocVarID(Optional ByVal oDoc As Word.Document = Nothing) As String
            'added in 9.9.5008 (GLOG 5264)
            'generates the 8-digit id used to link a content control
            'with the doc vars that hold its attributes
            Dim xDocVarID As String = ""
            Dim xValue As String = ""

            If oDoc Is Nothing Then _
                    oDoc = CurWordApp.ActiveDocument

            xValue = "x"
            While xValue <> ""
                Randomize()
                xDocVarID = CStr(CLng(100000000 * Rnd()))
                xDocVarID = New String("0", 8 - Len(xDocVarID)) & xDocVarID

                'test for conflict
                xValue = ""
                On Error Resume Next
                xValue = oDoc.Variables("mpo" & xDocVarID).Value
                On Error GoTo 0
            End While

            GenerateDocVarID = xDocVarID
        End Function

        Private Shared Sub TransferSegmentBookmarks(ByVal oSourceRange As Word.Range, _
            ByVal oTargetRange As Word.Range, ByVal bRetagSource As Boolean)
            'added in 9.9.5008 (GLOG 5264)
            Dim xBmks As String = ""
            Dim vBmks As Object
            Dim i As Integer
            Dim oBmk As Word.Bookmark
            Dim oBmkRange As Word.Range

            'get segment bookmarks in source - get in advance because
            'collection will change in process
            For Each oBmk In oSourceRange.Bookmarks
                If oBmk.Name Like "_mps*" Then _
                    xBmks = xBmks & oBmk.Name & "|"
            Next oBmk

            If xBmks <> "" Then
                vBmks = Split(xBmks, "|")
                For i = 0 To UBound(vBmks) - 1
                    'move bookmark to target range
                    oBmk = oSourceRange.Bookmarks(vBmks(i))
                    oBmkRange = oBmk.Range
                    With oTargetRange
                        .SetRange(oBmk.Start, oBmk.End)
                        .Bookmarks.Add(vBmks(i))
                    End With

                    'retag original bookmark range
                    If bRetagSource Then _
                        RetagmSEG(oBmkRange, Mid$(vBmks(i), 2))
                Next i
            End If
        End Sub
    End Class
End Namespace


