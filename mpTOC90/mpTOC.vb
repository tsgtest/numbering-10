Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.TOC.mpVariables
Imports LMP.Numbering.TOC.mpApplication
Imports LMP.Numbering.TOC.mpDocument
Imports LMP.Numbering.TOC.cTOCProps
Imports LMP.MacPac.ProgressForm
Imports LMP.Numbering.Base.cNumTOC
Imports LMP.Numbering.Base.cRange
Imports LMP.Numbering.Base.cConstants
Imports LMP.Numbering.Base.cStrings
Imports LMP.Numbering.Base.cNumbers
Imports LMP.Numbering.Base.cSchemeRecords
Imports LMP.Numbering.Base.cWordXML
Imports LMP.Numbering.Base.cContentControls
Imports System.Windows.Forms
Imports System.IO
Imports System.Reflection
Imports Microsoft.VisualBasic
Imports Microsoft.VisualBasic.Strings
Imports LMP.Numbering.CustomTOC
Imports System.Math

Namespace LMP.Numbering.TOC
    Public Class mpTOC
        Private Const mpCGSSkipItem As Integer = -1
        Private Const mpTCCodeStatic As String = " \z"
        Private Const mpEndOfTOCWarning = "End of TOC - Do not delete this paragraph!"
        Private Const mpNoEntriesWarning As String = _
            "THIS IS THE TABLE OF CONTENTS SECTION. " & vbCr & _
            "NO TABLE OF CONTENTS ENTRIES WERE FOUND!" & vbCr
        Private Const mpNoEntriesWarningFrench As String = _
            "CECI EST LA SECTION DE LA TABLE DES MATI�RES. " & vbCr & _
            "AUCUNE ENTR�E TM N'A �T� TROUV�E!" & vbCr
        Private Const mpDummyStyle As String = "zzmpTOCEntry"

        Private Enum mpTOCContent
            mpTOCContent_FirstSentence = 0
            mpTOCContent_Paragraph = 1
            mpTOCContent_TCEntries = 2
        End Enum

        Friend Structure TOCFormat
            Dim IncludePageNumber() As Boolean
            Dim SpaceBefore() As String
            Dim IncludeDotLeaders() As Boolean
            Dim CenterLevel1 As Boolean
            Dim AllCaps() As Boolean
            Dim Bold() As Boolean
            Dim LineSpacing As Integer
            Dim SpaceBetween As String
        End Structure

        Public Enum mpTOCExistsMarker
            mpTOCExistsMarker_None = 0
            mpTOCExistsMarker_Bookmark = 1
            mpTOCExistsMarker_Placeholder = 2
            mpTOCExistsMarker_Field = 3
        End Enum

        Public Enum mpMarkActions
            mpMarkAction_None = 0
            mpMarkAction_Mark = 1
            mpMarkAction_Unmark = 2
        End Enum

        Public Enum mpFormatActions
            mpFormatAction_None = 0
            mpFormatAction_Format = 1
            mpFormatAction_Unformat = 2
        End Enum

        Public Enum mpMarkingModes
            mpMarkingMode_TCCodes = 0
            mpMarkingMode_StyleSeparators = 1
        End Enum

        Public Enum mpMarkFormatScopes
            mpMarkFormatScope_Document = 0
            mpMarkFormatScope_Section = 1
            mpMarkFormatScope_Selection = 2
        End Enum

        Public Enum mpTOCDialogStyles
            mpTOCDialogStyle_Text = 0
            mpTOCDialogStyle_Field = 1
            mpTOCDialogStyle_Legacy = 2
        End Enum

        Public Enum mpMarkingReplacementModes
            mpMarkingReplacementMode_None = 0
            mpMarkingReplacementMode_Prompt = 1
            mpMarkingReplacementMode_Automatic = 2
        End Enum

        Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

        Friend Shared Function bMarkForTOCRemove(rngSelection As Word.Range) As Boolean
            'removes TC code and TCEntryLX
            'style from TC Heading

            Dim rngTCField As Word.Range
            Dim fldP As Word.Field
            Dim rngPara As Word.Range
            Dim paraP As Paragraph
            Dim bTCFound As Boolean
            bMarkForTOCRemove = False
            '   cycle through all paras in selection range
            For Each paraP In rngSelection.Paragraphs

                rngPara = paraP.Range

                '       get range of TC Field -
                '       this is faster than FIND
                rngTCField = Nothing
                For Each fldP In rngPara.Fields
                    If fldP.Type = WdFieldType.wdFieldTOCEntry Then
                        rngTCField = rngGetField(fldP.Code)
                        bTCFound = True
                        Exit For
                    End If
                Next fldP

                If Not (rngTCField Is Nothing) Then
                    ''           prompt to unformat only if there are directly applied
                    ''           ttributes somewhere in the selection;
                    ''           test isn't perfect, but it will eliminate some
                    ''           embarrassing prompts
                    '            If Not bDirectFound Then
                    '                With rngPara.Font
                    '                    If .Bold = wdUndefined Or _
                    '                            .AllCaps = wdUndefined Or _
                    '                            .SmallCaps = wdUndefined Or _
                    '                            .Underline = wdUndefined Or _
                    '                            .Italic = wdUndefined Then
                    '                        bDirectFound = True
                    '                    Else
                    '                        Set styFont = rngPara.Style.Font
                    '                        If .Bold <> styFont.Bold Or _
                    '                                .AllCaps <> styFont.AllCaps Or _
                    '                                .SmallCaps <> styFont.SmallCaps Or _
                    '                                .Underline <> styFont.Underline Or _
                    '                                .Italic <> styFont.Italic Then
                    '                            bDirectFound = True
                    '                        End If
                    '                    End If
                    '                End With
                    '                If bDirectFound Then
                    '                    iUnformat = MsgBox("Would you like to remove " & _
                    '                        "directly applied attributes from " & _
                    '                        "formerly marked headings?", _
                    '                        vbQuestion + vbYesNo + vbDefaultButton2, AppName)
                    '                End If
                    '            End If

                    '           delete field
                    rngTCField.Delete()

                    ''           unformat TCEntry Style
                    '            If iUnformat = vbYes Then
                    '                On Error Resume Next
                    '                xScheme = xGetLTRoot(rngPara.ListFormat.ListTemplate.Name)
                    '                On Error GoTo 0
                    '
                    '                If IsMacPacScheme(xScheme, mpSchemeTypes.mpSchemeType_Document) Then
                    '                    iLevel = rngPara.ListFormat.ListLevelNumber
                    '                    bIsStyleBased = bTOCLevelIsStyleBased(xScheme, iLevel)
                    '                End If
                    '
                    '                Set rngTCHeading = CurWordApp.ActiveDocument.Range(rngPara.Start, _
                    '                    rngTCField.Start)
                    '
                    '                With rngTCHeading
                    ''                   remove all applied manual formats
                    '                    If Not bIsStyleBased Then
                    '                        .Font.Reset
                    '                    Else
                    '                        If g_bApplyHeadingColor Then _
                    '                            .Font.ColorIndex = wdAuto
                    '                    End If
                    '                End With
                    '            End If
                End If
            Next paraP

            '   selection prevents text typed immediately
            '   after rngTCHeading from being formatted
            '   with previous format
            CurWordApp.Selection.Range.Select()

            bMarkForTOCRemove = bTCFound
        End Function

        Friend Shared Function bUserChangeTOCTabs() As Long
            Dim oForm As Form
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                oForm = New frmChangeTOCTabsFrench
            Else
                oForm = New frmChangeTOCTabs
            End If
            bUserChangeTOCTabs = oForm.ShowDialog()
            CurWordApp.Activate()
            CurWordApp.ActiveWindow.SetFocus()
        End Function

        Friend Shared Function rngFormatTCHeading_Styles(rngTCCode As Word.Range, _
                                  iListLevel As Integer, _
                                  bReformat As Boolean) As Word.Range
            'formats heading by applying TCEntry
            'style - this function exists for
            'possible future customization of heading
            'formatting that cannot be accomodated by
            'TCEntry styles

            Dim rngHeading As Word.Range


            '   get TC heading
            rngHeading = rngGetTCHeading(rngTCCode)

            '   apply style if specified
            If bReformat Then _
                rngHeading.Style = "TCEntryL" & iListLevel

            '   return heading
            rngFormatTCHeading_Styles = rngHeading

        End Function

        Friend Shared Sub SetTOCTabs(Optional ByVal sIncrement As Single = 3.6, _
                             Optional ByVal iLevel As Integer = 0)

            'expands or contracts space between
            'tab settings for TOC 1-9 styles
            Dim i As Integer
            Dim j As Integer
            Dim styTOC As Word.Style
            Dim xTOCStyle As String
            Dim sglModifyBy As Single
            Dim iMinLevel As Integer
            Dim iMaxLevel As Integer

            CurWordApp.ScreenUpdating = False

            '   set scope of changes - if iLevel
            '   is 0 do all levels, else do
            '   level iLevel only
            If iLevel = 0 Then
                iMinLevel = 1
                iMaxLevel = 9
            Else
                iMinLevel = iLevel
                iMaxLevel = iLevel
            End If

            '   cycle through specified TOC styles
            For i = iMinLevel To iMaxLevel
                xTOCStyle = xTranslateTOCStyle(i)
                styTOC = CurWordApp.ActiveDocument.Styles(xTOCStyle)
                With styTOC.ParagraphFormat
                    '           modify second line indent
                    .FirstLineIndent = .FirstLineIndent - sIncrement
                    .LeftIndent = .LeftIndent + sIncrement
                    With .TabStops
                        '               cycle through all tab stops, excluding
                        '               last one (that's the page number tab)
                        For j = 1 To .Count - 1
                            sglModifyBy = sIncrement * j
                            .Item(j).Position = .Item(j).Position + _
                                                        sglModifyBy
                        Next j
                    End With
                End With
            Next i

            CurWordApp.ScreenUpdating = True

        End Sub

        Friend Shared Sub ReApplyTOCStyles()
            Dim i As Integer
            Dim xTOCStyle As String

            With CurWordApp
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    .StatusBar = "Mise � jour des Styles TM..."
                Else
                    .StatusBar = "Updating TOC Styles..."
                End If
                .ScreenUpdating = False
                '       cycle through TOC 1-9
                For i = 1 To 9
                    xTOCStyle = xTranslateTOCStyle(i)
                    With CurWordApp.ActiveDocument.Bookmarks("mpTableOfContents").Range.Find
                        .ClearFormatting()
                        .Style = xTOCStyle
                        .Replacement.Style = xTOCStyle
                        .Execute(Replace:=WdReplace.wdReplaceAll)
                    End With
                Next i

                .StatusBar = ""
            End With
        End Sub

        Friend Shared Function xResetTOCStyles(xScheme As String, _
                               Optional secTOC As Word.Section = Nothing, _
                               Optional bCenterLevel1 As Boolean = False, _
                               Optional bIncludeSchedule As Boolean = False, _
                               Optional bDoScheduleOnly As Boolean = False) As String
            'redefines TOC 1-9 styles based
            'on scheme xScheme - TOC styles are
            'now stored in document itself.
            'returns scheme applied- this
            'is not necessarily xScheme. if xScheme
            'is not valid, return value will be "Generic"

            Dim xStyleSourceFile As String = ""
            Dim xStyleDestFile As String = ""
            Dim xStyle As String
            Dim iRightTabPos As Integer
            Dim xTOCStyle As String
            Dim stySourceStyle As Word.Style
            Dim styTOCStyle As Word.Style
            Dim styNormal As Word.Style
            Dim bDeleteStyle As Boolean
            Dim sSpacing As Single
            Dim i As Integer
            Dim udtFormat As TOCFormat
            Dim iStart As Integer

            On Error GoTo xResetTOCStyles_Error
            xResetTOCStyles = ""
            'exit if nothing to do
            If (Not bIncludeSchedule) And bDoScheduleOnly Then Exit Function

            With CurWordApp
                .ScreenUpdating = False
                xStyleSourceFile = xTOCSTY
            End With

            '   use WordBasic function to bypass ODMA based return
            xStyleDestFile = CurWordApp.WordBasic.FileName$()

            '   get normal style for future use
            styNormal = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal)

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                CurWordApp.StatusBar = "Applique th�me " & xScheme & " TM"
            Else
                CurWordApp.StatusBar = "Applying " & xScheme & " TOC Scheme"
            End If

            g_oStatus.UpdateProgress(4)

            '*****revised for 2000*****
            'style used to be always copied from tsgNumbers.sty
            'and then deleted at the end of the routine
            'now only do this if style isn't in doc, e.g. "Generic"
            If bDoScheduleOnly Then
                iStart = 9
            Else
                iStart = 1
            End If

            For i = iStart To 9
                If i = 9 And bIncludeSchedule Then
                    '           copy schedule prototype in place of level 9 of scheme
                    xStyle = "SpecialTOC9"
                Else
                    If xScheme = "Custom" Then
                        If bCenterLevel1 Then
                            xStyle = "CenteredL1TOC" & i
                        Else
                            xStyle = "StandardTOC" & i
                        End If
                    Else
                        xStyle = xScheme & "TOC" & i
                    End If
                End If

                xTOCStyle = xTranslateTOCStyle(i)

                On Error Resume Next
                stySourceStyle = Nothing
                stySourceStyle = CurWordApp.ActiveDocument.Styles(xStyle)
                On Error GoTo xResetTOCStyles_Error

                If stySourceStyle Is Nothing Then
                    '           copy style into active document
                    If xStyle = "SpecialTOC9" Then
                        On Error Resume Next
                    End If
                    CurWordApp.OrganizerCopy(xStyleSourceFile, _
                                              xStyleDestFile, _
                                              xStyle, _
                                              WdOrganizerObject.wdOrganizerObjectStyles)
                    If xStyle = "SpecialTOC9" Then
                        If Err.Number = mpErrors.mpStyleDoesNotExist Then
                            '                   if new TOC 9 style isn't in tsgTOC.sty, exit w/o modifying level
                            On Error GoTo xResetTOCStyles_Error
                            Exit For
                        Else
                            On Error GoTo xResetTOCStyles_Error
                        End If
                    End If
                    bDeleteStyle = True
                End If

                With CurWordApp.ActiveDocument.Styles
                    styTOCStyle = .Item(xTOCStyle)
                    stySourceStyle = .Item(xStyle)
                End With

                '       set TOC style to "look like" copied style
                With styTOCStyle
                    .BaseStyle = stySourceStyle.BaseStyle
                    .Font = stySourceStyle.Font
                    With .Font
                        .Name = styNormal.Font.Name
                        .Size = styNormal.Font.Size
                    End With
                    .NextParagraphStyle = stySourceStyle.NextParagraphStyle
                    .ParagraphFormat = stySourceStyle.ParagraphFormat
                    .LanguageID = styNormal.LanguageID

                    '           adjust for exact line spacing
                    If styNormal.ParagraphFormat.LineSpacingRule = WdLineSpacing.wdLineSpaceExactly Then
                        sSpacing = styNormal.ParagraphFormat.LineSpacing
                        With .ParagraphFormat
                            .LineSpacingRule = WdLineSpacing.wdLineSpaceExactly
                            Select Case stySourceStyle.ParagraphFormat.LineSpacingRule
                                Case WdLineSpacing.wdLineSpaceSingle
                                    .LineSpacing = sSpacing
                                Case WdLineSpacing.wdLineSpace1pt5
                                    .LineSpacing = 1.5 * sSpacing
                                Case WdLineSpacing.wdLineSpaceDouble
                                    .LineSpacing = 2 * sSpacing
                            End Select
                        End With
                    End If
                End With

                '       delete copied style
                If bDeleteStyle Then
                    CurWordApp.OrganizerDelete(xStyleDestFile, _
                                                xStyle, _
                                                WdOrganizerObject.wdOrganizerObjectStyles)
                End If
            Next i

            '   redefine generic TOC styles based on values in user.ini
            If xScheme = "Custom" Then
                udtFormat = udtGetTOCFormat("Custom", bIncludeSchedule)
                bRet = bCustomizeGenericStyles(udtFormat, bIncludeSchedule)
            End If

            '   clean up
            CurWordApp.StatusBar = ""

            '   return
            xResetTOCStyles = xScheme

            Exit Function

xResetTOCStyles_Error:
            Select Case Err.Number
                Case mpErrors.mpStyleDoesNotExist
                    '           prompt user that Generic
                    '           TOC Scheme will be applied
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Th�me TM " & xScheme & _
                                " non d�fini. Le th�me TM de base sera appliqu�.", _
                                vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox("The TOC scheme " & xScheme & _
                                " is not fully defined.  " & _
                                "The Generic TOC Scheme will be applied.", _
                                vbExclamation, AppName)
                    End If

                    '           redefine from level 1 onward
                    i = 1

                    xScheme = TOCSchemeGeneric
                    xStyle = xScheme & "TOC" & i
                    xTOCStyle = xTranslateTOCStyle(i)
                    Resume
                Case Else
                    MsgBox(ErrorToString(Err.Number))
            End Select

            Exit Function

        End Function

        Friend Shared Sub GetParaNumberInfo(rngCurPara As Word.Range, _
                                    ltCurrent As ListTemplate, _
                                    iListLevel As Integer, _
                                    iDefaultListLevel As Integer)

            '   gets list template and list level
            '   of range rngCurPara.  Different techniques
            '   for differently numbered paras.

            With rngCurPara.ListFormat
                If .ListType = WdListType.wdListOutlineNumbering Or _
                        .ListType = WdListType.wdListSimpleNumbering Then

                    iListLevel = .ListLevelNumber
                    ltCurrent = .ListTemplate

                ElseIf iDefaultListLevel > 0 Then
                    iListLevel = iDefaultListLevel
                    ltCurrent = Nothing
                Else
                    iListLevel = iGetTOCLevel(rngCurPara)
                    ltCurrent = Nothing
                End If
            End With
        End Sub

        Friend Shared Function bMarkForTOC(Optional ByVal bFormat As Boolean = False) As Boolean
            '   inserts empty TC field at insertion and
            '   applies appropriate TCEntryLX style -

            Dim fldCurField As Word.Field
            Dim fldExisting As Word.Field
            Dim iLevel As Integer
            Dim rngTCCodeStart As Word.Range
            Dim bAutoFormat As Boolean
            Dim bShowAllStart As Boolean
            Dim ltCurrent As Word.ListTemplate
            Dim xStyle As String = ""
            Dim xScheme As String = ""
            Dim bIsOutlineLevel As Boolean
            Dim bIsMPLT As Boolean
            bMarkForTOC = False
            '******VALIDATE ENVIRONMENT******
            With CurWordApp.Selection
                '       ensure that selection is insertion
                If .Type <> WdSelectionType.wdSelectionIP Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Ne pas s�lectionner le texte avant d'effectuer cette proc�dure. Placer le curseur � la fin du texte qui doit �tre marqu� comme TM et r�essayer.", _
                            vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox("Do not select text before running this procedure.  " & _
                               "Position cursor after the text to be marked " & _
                               "for TOC and try again.", vbExclamation, AppName)
                    End If
                    Exit Function
                End If
            End With
            '******END VALIDATE ENVIRONMENT******

            'remove existing style separator
            bRemoveStyleSeparators(CurWordApp.Selection.Range)

            '   test for existence of tc code-
            '   remove if there is one
            For Each fldExisting In CurWordApp.Selection.Paragraphs(1).Range.Fields
                If fldExisting.Type = WdFieldType.wdFieldTOCEntry Then
                    '           unmark for TOC
                    bMarkForTOCRemove(CurWordApp.Selection.Range)
                    '           xMsg = "Paragraph is already marked for table of contents."
                    '           MsgBox xMsg, vbExclamation, AppName
                    '           Exit Function
                End If
            Next fldExisting

            With CurWordApp.ActiveDocument
                With CurWordApp.Selection
                    rngTCCodeStart = CurWordApp.ActiveDocument.Range(.Start, .End)
                End With

                '       force show all - this mimics Word's
                '       behavior with TC marking
                With CurWordApp.ActiveWindow.View
                    bShowAllStart = .ShowAll
                    .ShowAll = True
                End With

                '       get paragraph level
                With rngTCCodeStart
                    iLevel = .ParagraphFormat.OutlineLevel
                    bIsOutlineLevel = iLevel <> WdOutlineLevel.wdOutlineLevelBodyText
                    If bIsOutlineLevel Then
                        On Error Resume Next 'GLOG 2847 - moved up a line
                        xStyle = .Style
                        xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
                        On Error GoTo 0

                        If xScheme <> "" Then
                            bIsMPLT = bIsMPListTemplate(.ListFormat _
                                .ListTemplate)
                        End If
                    End If
                End With

                If bIsMPLT Or _
                        bIsHeadingStyle(xStyle) Or _
                        (xConditionalScheme(rngTCCodeStart) <> "") Then
                    CurWordApp.ScreenUpdating = False
                    '           add tc field  - do not insert any text in code
                    fldCurField = .Fields.Add(rngTCCodeStart, _
                                                      WdFieldType.wdFieldTOCEntry, _
                                                      , _
                                                      False)
                Else
                    If Not bIsOutlineLevel Then
                        '               prompt for level
                        iLevel = iGetParagraphLevel(rngTCCodeStart, 0)
                    End If

                    If iLevel > 0 Then
                        CurWordApp.ScreenUpdating = False
                        '               insert tc field with specified level as text
                        fldCurField = .Fields.Add(rngTCCodeStart, _
                            WdFieldType.wdFieldTOCEntry, _
                            "\l " & Chr(34) & iLevel & Chr(34), _
                            False)
                    Else
                        CurWordApp.ActiveWindow.View.ShowAll = bShowAllStart
                        CurWordApp.ScreenUpdating = True
                        Exit Function
                    End If

                End If

                '       remove formatting from TC field
                With rngTCCodeStart
                    .MoveEnd(WdUnits.wdParagraph)
                    .Fields(1).Code.Font.Reset()
                End With

                If iLevel And bFormat Then
                    '           format heading and number to appropriate level
                    rngFormatTCHeading(rngTCCodeStart, _
                                       iLevel, _
                                       bFormat)
                End If

                CurWordApp.ScreenUpdating = True
            End With
            Exit Function
bMarkForTOC_Error:
            CurWordApp.ScreenUpdating = True
            bMarkForTOC = Err.Number
        End Function

        Friend Shared Sub InsertTCEntry()
            Dim rngLocation As Word.Range
            Dim rngHeading As Word.Range
            Dim xCurNumber As String = ""
            Dim lSelStartPos As Long
            Dim lParaStartPos As Long
            Dim xTCEntry As String = ""
            Dim iOutlineLevel As Integer

            rngLocation = CurWordApp.Selection.Range

            With rngLocation

                '       get number and number format if any
                With .ListFormat
                    If .ListType = WdListType.wdListOutlineNumbering Or _
                        .ListType = WdListType.wdListMixedNumbering Or _
                        .ListType = 1 Then
                        xCurNumber = .ListString
                        iOutlineLevel = .ListLevelNumber

                    End If
                End With

                '       get text from insertion to start of para
                lSelStartPos = .Start
                lParaStartPos = .Paragraphs(1).Range.Start
                rngHeading = CurWordApp.ActiveDocument.Range(lParaStartPos, lSelStartPos)

                '       concatenate
                xTCEntry = xCurNumber & vbTab & rngHeading.Text

                '       insert TC field
                CurWordApp.ActiveDocument.Fields.Add(rngLocation, _
                                          WdFieldType.wdFieldTOCEntry, _
                                          xTCEntry, _
                                          True)


            End With

        End Sub

        Friend Shared Function bResetTCEntries() As Boolean
            Dim fldP As Word.Field
            Dim l As Long
            Dim lFields As Long
            Dim iPos As Integer
            Dim iPosStatic As Integer
            Dim rngC As Word.Range

            bResetTCEntries = False
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                CurWordApp.StatusBar = "R�initialisation Entr�es de TM: 0%"
            Else
                CurWordApp.StatusBar = "Resetting TC Entries: 0%"
            End If

            g_oStatus.UpdateProgress(98)

            lFields = CurWordApp.ActiveDocument.Fields.Count

            For Each fldP In CurWordApp.ActiveDocument.Fields
                If fldP.Type = WdFieldType.wdFieldTOCEntry Then

                    '           clear out only mpTCCodeStatic
                    '           mark from static tc codes
                    rngC = fldP.Code
                    iPosStatic = InStr(UCase(rngC.Text), UCase(mpTCCodeStatic))
                    If iPosStatic Then
                        '               delete static marking
                        rngC.MoveStart(WdUnits.wdCharacter, iPosStatic - 1)
                        rngC.Text = ""
                    Else
                        '               get level
                        iPos = InStr(UCase(rngC.Text), "\L """)
                        If iPos Then
                            '                   is filled
                            rngC.Text = _
                                " " & g_xTCPrefix & " \l """ & Mid(rngC.Text, iPos + 4, 1) & """"
                        Else
                            rngC.Text = " " & g_xTCPrefix & " "
                        End If
                    End If
                End If
                l = l + 1
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "R�initialisation Entr�es de TM: " & _
                                            Format(l / lFields, "0%")
                Else
                    CurWordApp.StatusBar = "Resetting TC Entries: " & _
                                            Format(l / lFields, "0%")
                End If
                g_oStatus.UpdateProgress(98)
            Next fldP

            CurWordApp.StatusBar = ""
        End Function

        Friend Shared Function bInsertTOCAsField() As Boolean
            'inserts a MacPac TOC
            Dim iMaxLevel As Integer
            Dim iMinLevel As Integer
            Dim rngLocation As Word.Range
            Dim fldTOC As Word.Field
            Dim xTOCParams As String = ""
            Dim rngTOC As Word.Range
            Dim xLevRange As String = ""
            Dim iNumStyles As Integer
            Dim xStyleList As String = ""
            Dim bShowAll As Boolean
            Dim bShowHidden As Boolean
            Dim iView As Integer
            Dim ds As Date
            Dim xMsg As String = ""
            Dim iEntryType As mpTOCEntryTypes
            Dim udtTOCFormat As TOCFormat
            Dim i As Integer
            Dim xTestContent As String = ""
            Dim xTimeMsg As String = ""
            Dim lNumParas As Long
            Dim lWarningThreshold As Long
            Dim iLoc As Integer
            Dim bkmkTOCEntry As Word.Bookmark
            Dim bShowHiddenBkmks As Boolean
            Dim bSmartQuotes As Boolean
            Dim lZoom As Long
            Dim bIncludeSchedule As Boolean
            Dim bIncludeOther As Boolean
            Dim xInclusions As String = ""
            Dim xExclusions As String = ""
            Dim xTOC9Style As String = ""
            Dim oDummyStyle As Word.Style
            Dim rngDummyPara As Word.Range
            Dim bIsCentered As Boolean
            Dim iDummySec As Integer
            Dim bColumnsReformatted As Boolean
            Dim lStartTick As Integer
            Dim sElapsedTime As Single
            Dim lShowTags As Long
            Dim dlgTOC As Form
            Dim xScheme As String = ""
            Dim bPrintHiddenText As Boolean
            Dim iHeadingDef As Integer
            Dim oDoc As Word.Document
            Dim xLevel9Style As String = "" '9.9.5001
            Dim iMarkReplacementMode As mpMarkingReplacementModes
            Dim rngShiftReturn As Word.Range
            Dim lPos As Long
            Dim lSearchPos As Long
            Dim lParaStart As Long 'GLOG 5495
            Dim lXMLSafeParaStart As Long 'GLOG 5495
            Dim iInlineTags As Integer 'GLOG 5495
            Dim rngTest As Word.Range 'GLOG 5495
            Dim oDlg As frmInsertTOC
            Dim oDlgFrench As frmInsertTOCFrench
            Dim xInsertAt As String
            Dim bIncludeStyles As Boolean
            Dim bIncludeAll As Boolean
            Dim xScheduleStyle As String
            Dim bApplyTOC9 As Boolean
            Dim bTwoColumn As Boolean
            Dim bApplyManualFormats As Boolean
            Dim bUpdateTOCStyles As Boolean
            Dim bStyles As Boolean
            Dim bTCEntries As Boolean
            Dim bBoldHeaderTOC As Boolean
            Dim bBoldHeaderPage As Boolean
            Dim bCapHeaderTOC As Boolean
            Dim bCapHeaderPage As Boolean
            Dim bUnderlineHeaderTOC As Boolean
            Dim bUnderlineHeaderPage As Boolean
            Dim bPageNoContinue As Boolean
            Dim iPageNoStyle As Integer
            Dim iPageNoPunctuation As Short
            Dim xTOCScheme As String
            Dim xTOCDisplayName As String
            Dim bMark As Boolean
            Dim bHyperlinks As Boolean

            Try
                'OutputDebugString "Start of TOC macro"
                bInsertTOCAsField = False
                '   validate environment
                If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow$() = "") Then
                    'prompt to save document
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Cette macro n'est pas disponible dans un document non sauf-gard�.  Veuillez sauf-garder votre document avant d�ex�cuter cette macro." & _
                            vbCr & vbCr & "Nous vous prions de nous excuser pour tout inconv�nient mais c'est un workaround provisoire pour un bogue de Microsoft.  Le bogue a �t� introduit r�cemment dans Word 2007 SP2 et affecte la copie des styles par l'interm�diaire du code utilisant la fonction de l�Organisateur de Word.  Le correctif de Microsoft devrait �tre disponible dans les deux prochains mois.", _
                            vbInformation, "Num�rotation TSG")
                    Else
                        MsgBox("This macro is not available in an unsaved document.  " & _
                            "Please save your document and run this macro again." & vbCr & vbCr & _
                            "We apologize for the inconvenience but this is a temporary " & _
                            "workaround for a Microsoft bug that affects copying styles via code using Word's Organizer " & _
                            "feature.  Microsoft's hotfix should be available within a couple months.", _
                            vbInformation, AppName)
                    End If
                    Exit Function
                ElseIf g_xTOCLocations(0) = "" Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Ne peut cr�er TM, il n'y a pas d'emplacement d�sign� dans " & SettingsFileName & ".  Contactez votre administrateur."
                    Else
                        xMsg = "Cannot create TOC because there are no TOC locations specified in " & _
                            SettingsFileName & ".  Please see your system administrator."
                    End If
                Else
                    Try
                        xTestContent = CurWordApp.ActiveDocument.Characters(2).Text
                    Catch
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xMsg = "Ne peut cr�er TM.  Le pr�sent document est vide."
                        Else
                            xMsg = "Cannot create TOC.  The " & _
                                   "active document is empty."
                        End If
                    End Try
                End If

                If Len(xMsg) Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox(xMsg, vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox(xMsg, vbExclamation, AppName)
                    End If
                    Exit Function
                End If

                '   warn about large document
                lWarningThreshold = Val(xAppGetFirmSetting("TOC", _
                    "NumberOfParasWarningThreshold"))
                lNumParas = CurWordApp.ActiveDocument.Paragraphs.Count

                If lWarningThreshold > 0 And _
                        lNumParas > lWarningThreshold Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        lRet = MsgBox("Ce document contient " & lNumParas & " paragraphes.  TSG TM macro fait beaucoup plus que les fonctions natives TM Word.  La mise en forme du document prendra quelques temps.  D�sirez-vous continuer?", _
                            vbYesNo + vbQuestion, "Num�rotation TSG")
                    Else
                        lRet = MsgBox("This document contains " & lNumParas & " paragraphs.  " & _
                            "The TSG TOC macro does a lot more work than " & _
                            "the native Word TOC function.  Formatting a document " & _
                            "of this size will take some time.  Do you wish to continue?", _
                            vbYesNo + vbQuestion, AppName)
                    End If
                    If lRet = vbNo Then Exit Function
                End If

                oCustTOC = CreateCTOCObject()

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    dlgTOC = New frmInsertTOCFrench
                    oDlgFrench = CType(dlgTOC, frmInsertTOCFrench)
                    oDlgFrench.InsertAsField = True
                Else
                    dlgTOC = New frmInsertTOC
                    oDlg = CType(dlgTOC, frmInsertTOC)
                    oDlg.InsertAsField = True
                End If

                '   tsgTOC.sty is now only loaded as needed (12/10/01)
                LoadTOCSty()

                '   call any custom code
                lRet = oCustTOC.lBeforeDialogShow(dlgTOC)

                'OutputDebugString "Before form display"

                dlgTOC.ShowDialog()

                'OutputDebugString "After form display"
                lStartTick = CurrentTick()

                If dlgTOC.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                    UnloadTOCSty()
                    dlgTOC.Close()
                    CurWordApp.ActiveWindow.SetFocus()
                    dlgTOC = Nothing
                    oCustTOC = Nothing
                    Exit Function
                End If

                '   call any custom code
                lRet = oCustTOC.lAfterDialogShow()

                'GLOG 5147
                oDoc = CurWordApp.ActiveDocument

                'display status message
                g_oStatus = New MacPac.ProgressForm(100, 2, g_xTOCStatusMsg, "", False)
                g_oStatus.Show()
                System.Windows.Forms.Application.DoEvents()

                'GLOG 5057 (2/1/12) - disable Print Hidden Text option in order
                'to prevent inaccurate page numbers
                bPrintHiddenText = CurWordApp.Options.PrintHiddenText
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = False

                '   delete hidden TOC bookmarks - prevents potential
                '   "insufficient memory" error when generating TOC
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "G�n�re TM.  Veuillez patienter..."
                Else
                    CurWordApp.StatusBar = "Generating TOC.  Please wait..."
                End If

                With CurWordApp.ActiveDocument.Bookmarks
                    bShowHiddenBkmks = .ShowHidden
                    .ShowHidden = True
                End With
                For Each bkmkTOCEntry In CurWordApp.ActiveDocument.Bookmarks
                    With bkmkTOCEntry
                        If Strings.Left(.Name, 4) = "_Toc" Then
                            .Delete()
                            If Not g_bPreserveUndoListTOC Then _
                                CurWordApp.ActiveDocument.UndoClear()
                        End If
                    End With
                Next bkmkTOCEntry
                CurWordApp.ActiveDocument.Bookmarks.ShowHidden = bShowHiddenBkmks

                '   necessary for speed - this function executes
                '   a large number of Word actions.  the undo buffer
                '   fills up after 2 iterations, so to avoid a
                '   msg to the user, clear out first.
                If Not g_bPreserveUndoListTOC Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '   get/set environment
                With CurWordApp
                    .ScreenRefresh()
                    .ScreenUpdating = False
                End With

                EchoOff()
                With CurWordApp.ActiveWindow.View
                    '       get original settings
                    bShowAll = .ShowAll
                    bShowHidden = .ShowHiddenText
                    iView = .Type
                    lZoom = .Zoom.Percentage

                    '       modify environment
                    .ShowAll = True
                    .ShowHiddenText = True
                    .Type = WdViewType.wdNormalView
                    .Zoom.Percentage = 100
                End With

                'hide xml tags
                If g_bXMLSupport Then _
                    lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, False)

                '   turn off track changes
                CurWordApp.ActiveDocument.TrackRevisions = False

                '   turn off smart quotes
                With CurWordApp.Options
                    bSmartQuotes = .AutoFormatAsYouTypeReplaceQuotes
                    .AutoFormatAsYouTypeReplaceQuotes = False
                End With

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    With oDlgFrench
                        'store user choices
                        iMaxLevel = .cbxMaxLevel.Text
                        iMinLevel = .cbxMinLevel.Text

                        '       get inclusions/exclusions
                        If .optIncludeSchemes.Checked Then
                            '           schemes
                            xInclusions = .Inclusions
                            xExclusions = .Exclusions
                        ElseIf .optIncludeStyles.Checked Then
                            '           individual styles
                            xInclusions = .StyleInclusions
                            xExclusions = .StyleExclusions
                        End If

                        xInsertAt = .cbxInsertAt.Text
                        bIncludeStyles = .optIncludeStyles.Checked
                        bIncludeAll = .optIncludeAll.Checked
                        xScheduleStyle = .cbxScheduleStyles.Text
                        bApplyTOC9 = .chkApplyTOC9.Checked
                        bTwoColumn = .chkTwoColumn.Checked
                        bApplyManualFormats = .chkApplyManualFormatsToTOC.Checked
                        bUpdateTOCStyles = .optTOCStylesUpdate.Checked
                        bStyles = .chkStyles.Checked
                        bTCEntries = .chkTCEntries.Checked
                        bMark = .chkMark.Checked
                        bHyperlinks = .chkHyperlinks.Checked
                        xTOCDisplayName = .cbxTOCScheme.Text
                        xTOCScheme = .TOCScheme
                        iHeadingDef = .cbxHeadingDef.SelectedIndex
                        bBoldHeaderTOC = .BoldHeaderTOC
                        bBoldHeaderPage = .BoldHeaderPage
                        bCapHeaderTOC = .CapHeaderTOC
                        bCapHeaderPage = .CapHeaderPage
                        bUnderlineHeaderTOC = .UnderlineHeaderTOC
                        bUnderlineHeaderPage = .UnderlineHeaderPage
                        bPageNoContinue = .ContinuePageNumbering
                        iPageNoStyle = .NumberStyle
                        iPageNoPunctuation = .NumberPunctuation
                    End With
                Else
                    With oDlg
                        'store user choices
                        iMaxLevel = .cbxMaxLevel.Text
                        iMinLevel = .cbxMinLevel.Text

                        '       get inclusions/exclusions
                        If .optIncludeSchemes.Checked Then
                            '           schemes
                            xInclusions = .Inclusions
                            xExclusions = .Exclusions
                        ElseIf .optIncludeStyles.Checked Then
                            '           individual styles
                            xInclusions = .StyleInclusions
                            xExclusions = .StyleExclusions
                        End If

                        xInsertAt = .cbxInsertAt.Text
                        bIncludeStyles = .optIncludeStyles.Checked
                        bIncludeAll = .optIncludeAll.Checked
                        xScheduleStyle = .cbxScheduleStyles.Text
                        bApplyTOC9 = .chkApplyTOC9.Checked
                        bTwoColumn = .chkTwoColumn.Checked
                        bApplyManualFormats = .chkApplyManualFormatsToTOC.Checked
                        bUpdateTOCStyles = .optTOCStylesUpdate.Checked
                        bStyles = .chkStyles.Checked
                        bTCEntries = .chkTCEntries.Checked
                        bMark = .chkMark.Checked
                        bHyperlinks = .chkHyperlinks.Checked
                        xTOCDisplayName = .cbxTOCScheme.Text
                        xTOCScheme = .TOCScheme
                        iHeadingDef = .cbxHeadingDef.SelectedIndex
                        bBoldHeaderTOC = .BoldHeaderTOC
                        bBoldHeaderPage = .BoldHeaderPage
                        bCapHeaderTOC = .CapHeaderTOC
                        bCapHeaderPage = .CapHeaderPage
                        bUnderlineHeaderTOC = .UnderlineHeaderTOC
                        bUnderlineHeaderPage = .UnderlineHeaderPage
                        bPageNoContinue = .ContinuePageNumbering
                        iPageNoStyle = .NumberStyle
                        iPageNoPunctuation = .NumberPunctuation
                    End With
                End If

                '       convert TOC location from string to integer
                Select Case xInsertAt
                    Case mpTOCLocationEOF, mpTOCLocationEOFFrench
                        iLoc = DocPosition.mpAtEOF
                    Case mpTOCLocationBOF, mpTOCLocationBOFFrench
                        iLoc = DocPosition.mpAtBOF
                    Case mpTOCLocationAboveTOA, mpTOCLocationAboveTOAFrench
                        iLoc = DocPosition.mpAboveTOA
                    Case Else
                        iLoc = DocPosition.mpAtInsertion
                End Select

                bIncludeSchedule = InStr(UCase(xInclusions), ",SCHEDULE,")
                bIncludeOther = InStr(UCase(xInclusions), ",OTHER,")
                If bApplyTOC9 Then
                    xTOC9Style = xScheduleStyle
                End If

                '       if including Schedule styles, ensure that outline level is in range
                If bIncludeStyles Or bIncludeSchedule Then
                    SetScheduleLevels(iMinLevel, xTOC9Style)
                End If

                '       get TOC format
                udtTOCFormat = udtGetTOCFormat(xTOCScheme, bApplyTOC9)

                '       Word only allows 17 styles to be
                '       explicitly included in toc  - if there
                '       are more than 17, tag all outline styles
                '       between min and max levels, else tag
                '       only those outline level styles in the
                '       inclusion list that are between the min
                '       and max levels
                '       GLOG 5113 (9/24/12) - Word long ago eliminated this limit - we now only need to
                '       worry about this when it's specified in the ini
                '10/29/12 (dm) - I was wrong about this native limit having been eliminated -
                'g_bUseTOCStyleLimitWorkaround has been hardcoded to TRUE
                If g_bUseTOCStyleLimitWorkaround Or bIncludeOther Then
                    If bIncludeStyles Then
                        iNumStyles = lCountChrs(xInclusions, ",") - 1
                    ElseIf bIncludeAll Then
                        iNumStyles = 18
                    Else
                        iNumStyles = iMaxLevel * (lCountChrs(xInclusions, ",") - 1)
                        If bIncludeOther Then
                            '               we need to bring in all styles
                            iNumStyles = 18
                        ElseIf bIncludeSchedule Then
                            '               schedule styles are not one per level - adjust total
                            iNumStyles = iNumStyles - (iMaxLevel - iMinLevel) + UBound(g_xScheduleStyles)
                        End If
                    End If
                End If

                If iNumStyles <= 17 Then
                    If bIncludeStyles Then
                        '9.9.5001 - Apply TOC 9 option needs to be implemented
                        'via field switches - this option was previously disabled
                        'when inserting as field
                        xLevel9Style = xTOC9Style
                        xStyleList = xGetDesignatedStyles(xInclusions, xLevel9Style)
                    Else
                        xStyleList = xGetStyleList(xInclusions, _
                                                   iMinLevel, _
                                                   iMaxLevel, _
                                                   bApplyTOC9, _
                                                   xScheduleStyle)
                    End If
                End If

                'GLOG 5174 - replace shift-returns that aren't at start of paragraph -
                'note positioning of this code to avoid shift-returns in TC codes
                '9.9.5009 - moved this block up to before section insertion to prevent
                'crashing after inserting pleading paper in Forte 11.0.0.9
                If bStyles Then
                    lPos = -1
                    With CurWordApp.ActiveDocument.Content.Find
                        .ClearFormatting()
                        .Text = Chr(11)
                        .Wrap = WdFindWrap.wdFindContinue
                        .Execute()
                        Do While .Found
                            rngShiftReturn = .Parent

                            'GLOG 5495 - account for block-level bounding objects
                            lParaStart = GetCCSafeParagraphStart(rngShiftReturn)
                            lXMLSafeParaStart = GetTagSafeParagraphStart(rngShiftReturn)
                            If lXMLSafeParaStart > lParaStart Then _
                                lParaStart = lXMLSafeParaStart

                            'GLOG 5495 - account for inline bounding objects
                            'between start of para and shift-return
                            rngTest = rngShiftReturn.Duplicate
                            rngTest.SetRange(lParaStart, rngTest.Start)
                            iInlineTags = CountCCsEndingInRange(rngTest)
                            iInlineTags = (iInlineTags +
                                CountTagsEndingInRange(rngTest)) * 2

                            With rngShiftReturn
                                If .Start <= lSearchPos Then
                                    'we're back at the start
                                    'GLOG 5263 - changed from < to <=
                                    Exit Do
                                ElseIf (.Start = lParaStart + iInlineTags) Or
                                        (.Start = lPos + 1) Then
                                    'skip trailing character
                                    lPos = .Start
                                Else
                                    'replace
                                    .Text = "zzmpShiftReturn"
                                    lPos = -1
                                End If
                                lSearchPos = .Start
                            End With
                            .Execute()
                        Loop
                    End With
                End If

                '       call any custom code
                lRet = oCustTOC.lBeforeTOCSectionInsert()

                '       set location for insertion of TOC -
                '       MacPac TOC is bookmarked as mpTableOfContents
                'OutputDebugString "Before section insert"
                rngLocation = rngGetTOCTarget(CurWordApp.ActiveDocument,
                                                  iLoc,
                                                  bBoldHeaderTOC,
                                                  bBoldHeaderPage,
                                                  bCapHeaderTOC,
                                                  bCapHeaderPage,
                                                  bUnderlineHeaderTOC,
                                                  bUnderlineHeaderPage,
                                                  iPageNoStyle,
                                                  iPageNoPunctuation,
                                                  Not bPageNoContinue)

                '       format columns
                With rngLocation.Sections(1).PageSetup.TextColumns
                    If bTwoColumn And (.Count = 1) Then
                        .SetCount(2)
                        .Spacing = CurWordApp.InchesToPoints(0.5)
                        .EvenlySpaced = True
                        bColumnsReformatted = True
                    ElseIf (Not bTwoColumn) And (.Count = 2) Then
                        .SetCount(1)
                        bColumnsReformatted = True
                    End If
                End With

                'OutputDebugString "After section insert"

                '       call any custom code
                lRet = oCustTOC.lAfterTOCSectionInsert(rngLocation,
                        bColumnsReformatted)

                'new mark all option
                If g_iTOCDialogStyle <> mpTOCDialogStyles.mpTOCDialogStyle_Text Then
                    'add TC codes or style seps if specified
                    If bMark Then
                        Dim xTargetStyles As String = ""
                        Dim iMarkingType As mpMarkingModes
                        Dim xDelimiter As String = ""

                        If bIncludeAll Then
                            'GLOG 5298 - if "All Paragraphs of Specified Outline Levels" option
                            'is selected, use wildcard-type format - we were previously failing to
                            'mark at all with this option
                            For i = iMinLevel To iMaxLevel
                                xTargetStyles = xTargetStyles & "*_L" & CStr(i) & "|"
                            Next i
                        Else
                            'replace commas with pipes
                            xTargetStyles = Replace(xStyleList, ",", "|")
                        End If

                        'get marking type
                        If bStyles Then
                            iMarkingType = mpMarkingModes.mpMarkingMode_StyleSeparators
                        Else
                            iMarkingType = mpMarkingModes.mpMarkingMode_TCCodes
                        End If

                        'get heading delimiter
                        If iHeadingDef = 0 Then
                            xDelimiter = mpSentencePeriodOne
                        Else
                            xDelimiter = mpSentencePeriodTwo
                        End If

                        'get replacement mode - convert automatically unless creating from
                        'both style separators and TC entries
                        If (Not bStyles) Or (Not bTCEntries) Then
                            iMarkReplacementMode = mpMarkingReplacementModes.mpMarkingReplacementMode_Automatic
                        Else
                            iMarkReplacementMode = mpMarkingReplacementModes.mpMarkingReplacementMode_None
                        End If

                        'mark all specified styles
                        bMarkAndFormatHeadings(xTargetStyles, mpMarkActions.mpMarkAction_Mark,
                            mpFormatActions.mpFormatAction_None, iMarkingType, xDelimiter,
                            mpMarkFormatScopes.mpMarkFormatScope_Document, False, iMarkReplacementMode)
                    End If
                End If

                '       Reset TOC scheme based on user choice if specified
                If bUpdateTOCStyles Or bApplyTOC9 Then
                    xResetTOCStyles(xTOCScheme,
                                    rngLocation.Sections(1),
                                    udtTOCFormat.CenterLevel1,
                                    bApplyTOC9,
                                    Not bUpdateTOCStyles)
                End If

                '       reset right tab stop if switching column format or if
                '       style update was requested by user
                If bUpdateTOCStyles Or bColumnsReformatted Then
                    ResetRightTabStop(rngLocation.Sections(1))
                End If
                'OutputDebugString "After resetting styles"

                '9/7/12 - this is used for FillTCCodes and CleanUpTOCField
                bIsCentered = (CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleTOC1) _
                    .ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter)

                'if generating from TC fields and leaving TOC field linked,
                'we need to fill tc codes; since we're also advertising this
                'as a shortcut to avoid having to run the "Convert To Word TC Codes"
                'after running mark all, we need to fill dynamic codes even when
                'these entries were actually generated from styles; but since
                'CleanUpTOCField can't delete the style list after generation from
                'both TC fields and styles, because it can't assume that all
                'numbered paras have been marked, it becomes a training issue
                'to generate from TC fields ONLY in this situation, to avoid double
                'entries after a native update; the real answer is to offer filled TC
                'codes as an option in Mark All and to only fill partially dynamic codes here
                '9/27/12 (dm) - don't replace shift-returns in centered level one -
                'for this reason, moved this block to after xResetTOCStyles
                If bTCEntries Then _
                    RestoreMPTCCodes()

                'fill tc codes
                If bTCEntries Then
                    FillTCCodes(bApplyManualFormats, False, Not bIsCentered)

                    'fill partially dynamic codes with text
                    'from left of code to beginning of para
                    '9.9.5005 - made conditional on .chkTCEntries
                    iRefreshPDynCodes(0, "", , bApplyManualFormats, Not bIsCentered)
                End If

                'OutputDebugString "After updating TC entries"

                '   count number of styles to be included for TOC Entries
                If bIncludeStyles Then
                    xLevRange = "1-9"
                Else
                    xLevRange = iMinLevel & "-" & iMaxLevel
                End If

                '   Word only allows 17 styles to be
                '   explicitly included in toc - go figure
                If bStyles Then
                    If iNumStyles > 17 Then
                        '           create TOC based on TC Entries
                        '           and outline level paragraphs
                        xTOCParams = "\o """ & xLevRange & """" & "\w"

                        '9.9.5002 - if generating from outline levels, add paragraphs
                        If bIncludeAll Then _
                            xTOCParams = xTOCParams & " \u"
                    Else
                        'if system decimal separator is comma, use semi-colons in field
                        '9.9.3004 - get system list separator and adjust string if necessary -
                        'we were previously inferring the list separator from the decimal separator
                        If CurWordApp.International(WdInternationalIndex.wdListSeparator) <> "," Then
                            xStyleList = xSubstitute(xStyleList, ",",
                                CurWordApp.International(WdInternationalIndex.wdListSeparator))
                        End If

                        '           create TOC from explicit
                        '           style list and tc entries
                        xTOCParams = "\t """ & xStyleList & """" & "\w"
                    End If
                End If

                '   preserve line breaks
                xTOCParams = xTOCParams & " \x"

                '   bring in static tc codes if specified
                If bTCEntries Then
                    xTOCParams = xTOCParams & " \l """ &
                                 xLevRange & """"
                End If

                '   use hyperlinks if specified
                If bHyperlinks Then
                    xTOCParams = xTOCParams & " \h"
                End If

                '   exclude page numbers if specified - do only for field, so as not to throw off
                '   the deeply integrated expections in bReworkTOC by altering the number of tabs
                Dim xSwitch As String = ""
                For i = 1 To 9
                    If Not udtTOCFormat.IncludePageNumber(i - 1) Then
                        If xSwitch = "" Then
                            xSwitch = " \n " & CStr(i) & "-" & CStr(i)
                        Else
                            xSwitch = Mid(xSwitch, 1, 6) & CStr(i)
                        End If
                    End If
                Next i
                xTOCParams = xTOCParams & xSwitch

                '   prevent hidden text from
                '   affecting pagination of TOC
                With CurWordApp.ActiveWindow.View
                    .ShowAll = False
                    .ShowHiddenText = False
                    .ShowFieldCodes = False
                End With
                'OutputDebugString "Before inserting field"

                '   add toc
                g_oStatus.UpdateProgress(4)
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "Word ins�re un champ Table des mati�res..."
                Else
                    CurWordApp.StatusBar = "Word is inserting Table of Contents field..."
                End If

                fldTOC = CurWordApp.ActiveDocument.Fields.Add(
                    rngLocation, WdFieldType.wdFieldTOC, xTOCParams)
                'OutputDebugString "After inserting field"

                '   call any custom code
                lRet = oCustTOC.lAfterTOCFieldInsert(fldTOC)

                '   bookmark toc
                rngGetField(fldTOC.Code).Bookmarks.Add("mpTableOfContents")
                rngTOC = CurWordApp.ActiveDocument.Bookmarks("mpTableOfContents").Range

                With rngTOC
                    If Asc(.Characters.Last.Text) = 12 Then
                        rngTOC.MoveEnd(WdUnits.wdCharacter, -1)
                    End If

                    '       update page numbers
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Mise � jour les num�ros de page..."
                    Else
                        CurWordApp.StatusBar = "Word is updating page numbers in Table of Contents..."
                    End If
                    g_oStatus.UpdateProgress(42)
                    CurWordApp.ActiveDocument.TablesOfContents(1) _
                        .UpdatePageNumbers()
                    'OutputDebugString "After updating page numbers"

                    '       show hidden text prior to
                    '       cleaning up doc and toc - this will
                    '       guarantee that codes that are hidden
                    '       are deleted if specified
                    With CurWordApp.ActiveWindow.View
                        .ShowHiddenText = True
                        .ShowAll = True
                    End With

                    '       do only if TOC has entries
                    If Not bIsTOCStyle(.Paragraphs.First.Style.NameLocal) Then
                        '           return doc to original state
                        CleanUpDoc()
                        With rngTOC
                            If .Characters.Last.Text = vbCr Then _
                                .MoveEnd(WdUnits.wdCharacter - 1)
                            .Delete()
                        End With
                    Else
                        '           change content of toc and format
                        '           based on user dlg choices
                        '           remove manual font formats if specified
                        If Not bApplyManualFormats Then
                            If rngTOC.Font.Name <> "" Then
                                rngTOC.Font.Reset()
                            Else
                                'GLOG 2793 - inconsistent font - do targeted
                                'reset to preserve bullets
                                ResetTOCFont(rngTOC)
                            End If
                        End If

                        '           call any custom code
                        lRet = oCustTOC.lBeforeTOCRework(rngTOC)

                        'OutputDebugString "Before reworking"
                        ReworkTOCField(rngTOC,
                                       xTOCDisplayName,
                                       xExclusions,
                                       bStyles,
                                       bTCEntries,
                                       udtTOCFormat,
                                       xTOC9Style,
                                       bIncludeStyles,
                                       bApplyManualFormats)
                        'OutputDebugString "After reworking"

                        '           call any custom code
                        lRet = oCustTOC.lAfterTOCRework()

                        'prepare TOC field for potential native update
                        'OutputDebugString "Before cleaning up TOC field"
                        CleanUpTOCField(rngTOC, Not bIsCentered)
                        'OutputDebugString "After cleaning up TOC field"

                        '           ensure that toc is not colored
                        .Font.ColorIndex = WdColorIndex.wdAuto

                        '           add warning not to delete trailing para
                        If .Sections.Last.Index = CurWordApp.ActiveDocument.Sections.Count Then
                            .EndOf()
                        Else
                            'GLOG 5393 - this code appears to be obsolete and
                            'is problematic when the document starts with an
                            'intentional empty paragraph between the existing TOC
                            'and subsequent text in the same section - I haven't
                            'been able to recreate any scenario in which we currently
                            'insert an extraneous paragraph
                            ''               delete extraneous paragraph
                            '                On Error Resume Next
                            '                If .Characters.Count > 1 Then
                            '                    If .Next(wdParagraph).Text = vbCr Then _
                            '                        .Next(wdParagraph).Delete
                            '                End If
                            '                On Error GoTo ProcError
                        End If

                        '           return doc to original state
                        If Not g_bPreserveUndoListTOC Then _
                            CurWordApp.ActiveDocument.UndoClear()
                    End If
                End With

                'GLOG 5174 - restore replaced shift-returns -
                '4/15/13 - moved this line out of the above block, so that the cleanup
                'will occur regardless of whether entries are found
                ReplaceText(CurWordApp.ActiveDocument.Content, "zzmpShiftReturn", Chr(11))

                '   in Word 97, all caps and bold need to be done at end
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xScheme = "Personnalis�"
                Else
                    xScheme = "Custom"
                End If
                If (xTOCDisplayName = xScheme) And bUpdateTOCStyles Then
                    CurWordApp.ScreenRefresh()
                    CurWordApp.ScreenUpdating = False
                    For i = 1 To 9
                        With CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(i)).Font
                            .AllCaps = udtTOCFormat.AllCaps(i - 1)
                            .Bold = udtTOCFormat.Bold(i - 1)
                        End With
                    Next i
                End If

                '   clear tc codes if specified
                'OutputDebugString "Before resetting TC entries"
                '9.9.4019 - this control defaulted checked and was hidden -
                'got rid of it with the switchover to the native tab control
                '    If dlgTOC.chkClearTCCodes Then
                '9.9.5005 - made conditional on .chkTCEntries
                If bTCEntries Then _
                    bRet = bResetTCEntries()
                'OutputDebugString "After resetting TC entries"

                '   All TOC entries may have been marked for deletion
                If Not CurWordApp.ActiveDocument.Bookmarks.Exists("mpTableOfContents") Then _
                    CurWordApp.ActiveDocument.Bookmarks.Add("mpTableOfContents", rngTOC)

                '   hide status display
                g_oStatus.Close()
                g_oStatus.Dispose()
                'g_oStatus.Hide()
                g_oStatus = Nothing
                CurWordApp.ScreenUpdating = True
                CurWordApp.ScreenRefresh()

                'GLOG 5147 - ensure that focus remains on active document
                CurWordApp.Activate()
                oDoc.Activate()

                '   alert if toc is empty
                If Len(CurWordApp.ActiveDocument.Bookmarks("mpTableOfContents").Range.Text) < 2 Then
                    With rngTOC
                        '           move before "Do Not Delete" warning
                        Try
                            If .Previous(WdUnits.wdParagraph).Text =
                                    mpEndOfTOCWarning & vbCr Then
                                .Move(WdUnits.wdParagraph, -1)
                            End If
                        Catch
                        End Try
                        '           insert warning in document
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            .InsertBefore(mpNoEntriesWarningFrench)
                        Else
                            .InsertBefore(mpNoEntriesWarning)
                        End If
                        .Style = WdBuiltinStyle.wdStyleNormal
                        .HighlightColorIndex = WdColorIndex.wdNoHighlight
                        .Bold = True
                    End With
                    '       rebookmark
                    CurWordApp.ActiveDocument.Bookmarks.Add("mpTableOfContents", rngTOC)
                    '       present dlg alert
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("La Table des mati�res est vide.  " & vbCr &
                               "Aucune entr�e TM n'a �t� trouv� dans ce document.",
                                vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox("The Table Of Contents is empty.  " & vbCr &
                               "No valid Table Of Contents entries " &
                               "could be found in the document.",
                               vbExclamation, AppName)
                    End If
                End If

                oCustTOC = Nothing
                '   select start of TOC - force
                '   vertical scroll to selection
                With CurWordApp.ActiveDocument
                    .Range(0, 0).Select()
                    .Bookmarks("mpTableOfContents").Select()
                End With
                CurWordApp.Selection.StartOf()

                '   restore user's smart quotes setting
                CurWordApp.Options _
                    .AutoFormatAsYouTypeReplaceQuotes = bSmartQuotes

                '   restore outline levels of schedule styles
                If bIncludeStyles Or bIncludeSchedule Then
                    RestoreScheduleLevels()
                End If

                '   tsgTOC.sty is now only loaded as needed (12/10/01);
                '   9.8.1007 - tsgTOC.sty is now left loaded
                '    UnloadTOCSty

                '   reset environment
                'OutputDebugString "Before resetting environment"
                With CurWordApp.ActiveWindow.View
                    .ShowAll = bShowAll
                    .ShowHiddenText = bShowHidden
                    .Type = iView
                    .Zoom.Percentage = lZoom
                End With

                'restore xml tags
                If g_bXMLSupport Then _
                    lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, lShowTags)

                'GLOG 5057 - restore setting
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = True

                EchoOn()
                bInsertTOCAsField = True

                'OutputDebugString "End of TOC macro"

                '9.9.5005
                If g_bDisplayBenchmarks Then
                    sElapsedTime = ElapsedTime(lStartTick)
                    MsgBox(CStr(sElapsedTime))
                End If
            Finally
                If Not g_oStatus Is Nothing Then
                    g_oStatus.Close()
                    g_oStatus.Dispose()
                    g_oStatus = Nothing
                End If

                CurWordApp.ScreenUpdating = True
                CurWordApp.ScreenRefresh()

                'GLOG 5057 - restore setting
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = True

                If Not dlgTOC Is Nothing Then
                    dlgTOC.Close()
                    dlgTOC = Nothing
                End If
                EchoOn()
            End Try
        End Function
#If False Then
        Function iUpdateTCCodes(ByVal iDefaultTOCLevel As Integer, _
                                ByVal xNumberTrailChar As String, _
                                Optional ByVal bReApplyTCEntryStyles As Boolean = False, _
                                Optional ByVal bApplyHeadingFormats As Boolean = False) As Integer

        '   cycles through all tc codes,
        '   filling each appropriately -
        '   returns number of codes found

            Dim rngTCCode As Word.Range
            Dim rngTCCodeStart As Word.Range
            Dim rngTCFieldTextStart As Word.Range
            Dim lTCCodeStartPos As Long
            Dim i As Integer
            Dim iListLevel As Integer
            Dim fldField As Word.Field
            Dim ltCurrent As ListTemplate
            Dim rngHeading As Word.Range
            Dim iNumHeadingNumbers As Integer
            Dim iUserChoice As Integer
            Dim xPrompt As String

            If g_lUILanguage = wdFrenchCanadian Then
                xPrompt = "Mise � jour des entr�s de TM:"
            Else
                xPrompt = "Updating TOC Entries:"
            End If
            CurWordApp.StatusBar = xPrompt

        '   find first tc code
            For Each fldField In CurWordApp.ActiveDocument.Fields

        '       cycle through all tc codes
                If fldField.Type = wdFieldTOCEntry Then

        '           set status
                    i = i + 1
                    CurWordApp.StatusBar = xPrompt & " " & i

        '           get range of field
                    With fldField
                        Set rngTCCode = .Code

                        lTCCodeStartPos = .Code.Start - 1
                        Set rngTCCodeStart = CurWordApp.ActiveDocument.Range(lTCCodeStartPos, _
                                                                  lTCCodeStartPos)
                    End With

        '           get list level, if any
                    iListLevel = iGetParagraphLevel(rngTCCodeStart, _
                                                    iDefaultTOCLevel)

        '           if user cancelled out of TCEntry
        '           level dlg, either remove code
        '           or attempt to get level again
                    While iListLevel = 0
                        If g_lUILanguage = wdFrenchCanadian Then
                            iUserChoice = MsgBox("Enlever marquages pour TM ?", _
                                vbExclamation + vbYesNoCancel, "Num�rotation TSG")
                        Else
                            iUserChoice = MsgBox("Remove Mark For TOC?", _
                                vbExclamation + vbYesNoCancel, AppName)
                        End If
                        If iUserChoice = vbYes Then
                            iListLevel = mpRemoveTOCMarking
                        ElseIf iUserChoice = vbNo Then
                            iListLevel = mpSkipTOCMarking
                        Else
                            iUpdateTCCodes = -1
                            Exit Function
                        End If
                    Wend

                    If iListLevel = mpRemoveTOCMarking Then
        '               remove TOC Marking
                        bRet = bMarkForTOCRemove(rngTCCodeStart)
                    ElseIf iListLevel > mpSkipTOCMarking Then
        '               fill TC Code -
        '               set starting point for TC field content
                        Set rngTCFieldTextStart = rngTCCode.Characters.Last
                        rngTCFieldTextStart.EndOf

        '               format heading and number
                        Set rngHeading = rngFormatTCHeading(rngTCCodeStart, _
                                                            iListLevel, _
                                                            bReApplyTCEntryStyles)

        '               get numbered items
                        iNumHeadingNumbers = rngHeading.ListFormat _
                                            .CountNumberedItems

        '               fill tc code -
        '               MacPac uses two different techniques for filling
        '               TC codes (see below).  The different techniques are
        '               necessary for two reasons: 1)we need a technique to
        '               get formats into TC Entries, 2) we need a technique
        '               to get multiple numbers into TC Entries (WORD can't
        '               do this on its own).  The function that does these
        '               things is signifcantly slower than the other routine,
        '               so we use it only when necessary, as specified by
        '               the following conditional:
                        If (bApplyHeadingFormats Or _
                            iNumHeadingNumbers > 1) Then

                            bRet = bInsertTCEntryText(rngHeading, _
                                                      rngTCFieldTextStart, _
                                                      iListLevel, _
                                                      xNumberTrailChar)

                                If Not bApplyHeadingFormats Then
                                    fldField.Code.Font.Reset
                                End If

                        Else

                            rngTCFieldTextStart.InsertAfter xGetTCEntryText _
                                                            (rngHeading, _
                                                            iListLevel, _
                                                            xNumberTrailChar)

                            If Not bApplyHeadingFormats Then
                                fldField.Code.Font.Reset
                            End If

                        End If
                    End If
                End If
            Next fldField

        '   clean up
            bRet = bDeleteAllBookmarks(bMacPacOnly:=True)

        '   return
            iUpdateTCCodes = i
        End Function
#End If

        Friend Shared Function iRefreshPDynCodes(ByVal iDefaultTOCLevel As Integer,
                                ByVal xNumberTrailChar As String,
                                Optional ByVal bReApplyTCEntryStyles As Boolean = False,
                                Optional ByVal bApplyHeadingFormats As Boolean = False,
                                Optional bReplaceShiftReturns As Boolean = True) As Integer
            '   cycles through all tc codes,
            '   filling each appropriately -
            '   returns number of codes found
            '9/27/12 (dm) - added bReplaceShiftReturns argument
            Dim rngTCCode As Word.Range
            Dim rngTCCodeStart As Word.Range
            Dim rngTCFieldTextStart As Word.Range
            Dim lTCCodeStartPos As Long
            Dim i As Integer
            Dim iLevel As Integer
            Dim fldField As Word.Field
            Dim ltCurrent As ListTemplate
            Dim rngHeading As Word.Range
            Dim iNumHeadingNumbers As Integer
            Dim iUserChoice As Integer
            Dim l As Long
            Dim lFields As Long
            Dim iPos As Integer
            Dim bLevelChanged As Boolean
            Dim bIsMarkedFilled As Boolean
            Dim bIsMarkedDyn As Boolean
            Dim bIsTCPDyn As Boolean

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                CurWordApp.StatusBar = "Mise � jour des entr�s de TM:"
            Else
                CurWordApp.StatusBar = "Updating TOC Entries:"
            End If
            g_oStatus.UpdateProgress(4)

            lFields = CurWordApp.ActiveDocument.Fields.Count

            For Each fldField In CurWordApp.ActiveDocument.Fields
                '       cycle through all tc codes
                If fldField.Type = WdFieldType.wdFieldTOCEntry Then

                    '           get range of field
                    With fldField
                        rngTCCode = .Code
                        lTCCodeStartPos = .Code.Start - 1
                        rngTCCodeStart = CurWordApp.ActiveDocument.Range(
                                lTCCodeStartPos, lTCCodeStartPos)
                    End With

                    bIsMarkedFilled = InStr(UCase(rngTCCode.Text),
                        UCase(g_xTCPrefix) & " " & Chr(34))

                    '           may be using smart quotes
                    If Not bIsMarkedFilled Then
                        bIsMarkedFilled = InStr(UCase(rngTCCode.Text),
                            UCase(g_xTCPrefix) & " " & ChrW(&H201C))
                    End If

                    bIsMarkedDyn = (rngTCCode.Text = " " & g_xTCPrefix & " ")

                    '           update code only if there is no outline level
                    '           for para or if tc code is already filled
                    '            If (rngTCCode.ParagraphFormat.OutlineLevel = _
                    '                wdOutlineLevelBodyText) Or bIsMarkedFilled Then
                    If Not bIsMarkedDyn Then
                        '               attempt to get level from code
                        iPos = InStr(LCase(rngTCCode.Text), "\l """)

                        '               may be using smart quotes
                        If iPos = 0 Then
                            iPos = InStr(LCase(rngTCCode.Text), "\l " & ChrW(&H201C))
                        End If

                        If iPos Then
                            '                   level param exists in tc code
                            On Error Resume Next
                            '                   extract number - iLevel will be 0 if
                            '                   the character succeeding "\l """ is not
                            '                   an integer
                            iLevel = Mid(rngTCCode.Text, iPos + 4, 1)
                            If iLevel < 1 Or iLevel > 9 Then
                                '                       invalid level - alert and prompt for level
                                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                    MsgBox("Niveau TM invalide. S�lectionnez un niveau dans la bo�te de dialogue.",
                                        vbExclamation, "Num�rotation TSG")
                                Else
                                    MsgBox("Invalid TOC level.  Please choose " &
                                           "a level from the following dialog.", vbExclamation, AppName)
                                End If

                                '                       get list level, if any
                                iLevel = iGetParagraphLevel(rngTCCodeStart,
                                                                iDefaultTOCLevel)
                                bLevelChanged = (iLevel <> 0)
                            End If
                        End If

                        If iLevel = 0 Then
                            '                   invalid level - alert and prompt for level
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                MsgBox("Niveau TM invalide ou manquant.  S�lectionnez un niveau dans la bo�te de dialogue.",
                                    vbExclamation, "Num�rotation TSG")
                            Else
                                MsgBox("Invalid or missing TOC level.  Please choose " &
                                    "a level from the following dialog.", vbExclamation, AppName)
                            End If

                            '                   get list level, if any
                            iLevel = iGetParagraphLevel(rngTCCodeStart,
                                                            iDefaultTOCLevel)
                            bLevelChanged = (iLevel <> 0)
                        End If

                        '               if user cancelled out of TCEntry
                        '               level dlg, either remove code
                        '               or attempt to get level again
                        While iLevel = 0
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                iUserChoice = MsgBox("Enlever marquages pour TM?",
                                                     vbExclamation + vbYesNoCancel,
                                                     "Num�rotation TSG")
                            Else
                                iUserChoice = MsgBox("Remove Mark For TOC?",
                                                     vbExclamation + vbYesNoCancel,
                                                     AppName)
                            End If
                            If iUserChoice = vbYes Then
                                iLevel = mpRemoveTOCMarking
                            ElseIf iUserChoice = vbNo Then
                                iLevel = mpSkipTOCMarking
                            Else
                                iRefreshPDynCodes = -1
                                Exit Function
                            End If
                        End While

                        If iLevel = mpRemoveTOCMarking Then
                            '                   remove TOC Marking
                            bRet = bMarkForTOCRemove(rngTCCodeStart)
                        ElseIf bIsMarkedFilled Then
                            '                   replace shift-returns with a space in pre-9.7.8 partially dynamic codes;
                            '                   this is no longer handled in bReworkTOC (7/1/03)
                            bIsTCPDyn = (InStr(UCase(rngTCCode.Text), UCase(mpTCCode_PDyn)) > 0)
                            If bIsTCPDyn Then
                                If bReplaceShiftReturns Or (iLevel <> 1) Then
                                    rngTCCode.Text = xSubstitute(rngTCCode.Text,
                                        New String(Chr(11), 2), Chr(32))
                                    rngTCCode.Text = xSubstitute(rngTCCode.Text,
                                        Chr(11), Chr(32))
                                Else
                                    'replace shift-returns with single shift return
                                    rngTCCode.Text = xSubstitute(rngTCCode.Text,
                                        New String(Chr(11), 2), Chr(11))
                                End If
                            End If

                            'append MacPac tag
                            rngTCCode.InsertAfter(mpTCCodeStatic)
                        ElseIf iLevel > mpSkipTOCMarking Then
                            '                   fill TC Code -
                            '                   set starting point for TC field content
                            rngTCCode.Text = " " & g_xTCPrefix & " "
                            rngTCFieldTextStart = rngTCCode.Characters(4)
                            rngTCFieldTextStart.EndOf()

                            If bLevelChanged Then
                                '                       prompt to format
                                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                    iUserChoice = MsgBox("D�sirez-vous d�finir ce titre comme niveau " & iLevel & "?",
                                        vbQuestion + vbYesNo, "Num�rotation TSG")
                                Else
                                    iUserChoice = MsgBox("Do you want to format this heading " &
                                           "as a level " & iLevel & " heading?", vbQuestion + vbYesNo, AppName)
                                End If

                                If iUserChoice = vbYes Then
                                    '                           level of heading has changed - format heading and number
                                    rngHeading = rngFormatTCHeading(rngTCCodeStart,
                                                                        iLevel,
                                                                        True)
                                Else
                                    '                           just get the range of the heading
                                    rngHeading = CurWordApp.ActiveDocument.Range(
                                        rngTCCode.Paragraphs(1).Range.Start,
                                        rngTCCode.Start - 1)
                                End If
                            Else
                                '                       just get the range of the heading
                                rngHeading = CurWordApp.ActiveDocument.Range(
                                    rngTCCode.Paragraphs(1).Range.Start,
                                    rngTCCode.Start - 1)
                            End If

                            '                   get numbered items
                            iNumHeadingNumbers = rngHeading.ListFormat _
                                                .CountNumberedItems

                            '                   fill tc code -
                            '                   MacPac uses two different techniques for filling
                            '                   TC codes (see below).  The different techniques are
                            '                   necessary for two reasons: 1)we need a technique to
                            '                   get formats into TC Entries, 2) we need a technique
                            '                   to get multiple numbers into TC Entries (WORD can't
                            '                   do this on its own).  The function that does these
                            '                   things is signifcantly slower than the other routine,
                            '                   so we use it only when necessary, as specified by
                            '                   the following conditional:
                            If (bApplyHeadingFormats Or
                                iNumHeadingNumbers > 1) Then

                                bRet = bInsertTCEntryText(rngHeading,
                                                          rngTCFieldTextStart,
                                                          iLevel,
                                                          xNumberTrailChar,
                                                          True)

                                If Not bApplyHeadingFormats Then
                                    fldField.Code.Font.Reset()
                                End If

                            Else

                                rngTCFieldTextStart.InsertAfter(xGetTCEntryText _
                                                                (rngHeading,
                                                                iLevel,
                                                                xNumberTrailChar,
                                                                True))

                                If Not bApplyHeadingFormats Then
                                    fldField.Code.Font.Reset()
                                End If

                            End If
                        End If
                    End If
                End If
                iLevel = 0
                bIsMarkedFilled = False
                iPos = 0
                iNumHeadingNumbers = 0
                bRet = 0
                bLevelChanged = False

                '       set status
                l = l + 1
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "Mise � jour des entr�s de TM: " & Format(l / lFields, "0%")
                Else
                    CurWordApp.StatusBar = "Updating TOC Entries: " & Format(l / lFields, "0%")
                End If
                g_oStatus.UpdateProgress(4)

            Next fldField

            '   clean up
            '9.9.5005 - no need to cycle through all bookmarks when we're after one in particular
            '    bRet = bDeleteAllBookmarks(bMacPacOnly:=True)
            If CurWordApp.ActiveDocument.Bookmarks.Exists("zzmpTCEntryTemp") Then _
                CurWordApp.ActiveDocument.Bookmarks("zzmpTCEntryTemp").Delete()

            '   return
            iRefreshPDynCodes = i
        End Function

        Friend Shared Function iGetTOCLevel(rngTCLocation As Word.Range) As Integer
            Dim dlgLevel As Form
            Dim dlgLevelBase As frmTOCEntryLevel
            Dim dlgLevelFrench As frmTOCEntryLevelFrench
            Dim rngDupe As Word.Range
            Dim bShowAll As Boolean
            Dim bShowHidden As Boolean
            Dim bManageStatus As Boolean

            Try

                iGetTOCLevel = 0
                'hide status screen while prompting (9.9.6014)
                bManageStatus = Not g_oStatus Is Nothing
                If bManageStatus Then
                    g_oStatus.Close()
                    g_oStatus.Dispose()
                    g_oStatus = Nothing
                End If

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    dlgLevel = New frmTOCEntryLevelFrench
                    dlgLevelFrench = CType(dlgLevel, frmTOCEntryLevelFrench)
                Else
                    dlgLevel = New frmTOCEntryLevel
                    dlgLevelBase = CType(dlgLevel, frmTOCEntryLevel)
                End If

                'hide all visible MacPac codes
                HideTags()

                With CurWordApp.ActiveWindow.View
                    bShowAll = .ShowAll
                    bShowHidden = .ShowHiddenText
                    .ShowAll = False
                    .ShowHiddenText = False
                End With

                rngDupe = rngTCLocation.Duplicate

                With rngDupe
                    .SetRange(.Paragraphs(1).Range.Start, .End)
                    .MoveStartWhile(CStr(Chr(12)))
                    CurWordApp.ScreenUpdating = True
                    .Select()
                End With

                With dlgLevel
                    .ShowDialog()
                    If Not .DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            iGetTOCLevel = dlgLevelFrench.cbxTOCLevels.SelectedIndex + 1
                        Else
                            iGetTOCLevel = dlgLevelBase.cbxTOCLevels.SelectedIndex + 1
                        End If
                    End If
                End With

                CurWordApp.ScreenUpdating = False

                With CurWordApp.ActiveWindow.View
                    .ShowAll = bShowAll
                    .ShowHiddenText = bShowHidden
                End With

                'restore status screen (9.9.6014)
                If bManageStatus Then
                    g_oStatus = New MacPac.ProgressForm(100, 3, g_xTOCStatusMsg, "", False)
                    g_oStatus.Show()
                    System.Windows.Forms.Application.DoEvents()
                End If
            Finally
                If Not dlgLevel Is Nothing Then
                    dlgLevel.Close()
                End If
                rngTCLocation.Select()
            End Try

        End Function

        Friend Shared Function rngInsertTOCTOASection(Optional vLocation As Object = Nothing,
                                        Optional bInsertTOA As Boolean = False,
                                        Optional bRestartPageNumbering As Boolean = True,
                                        Optional bBoldTOC As Boolean = True,
                                        Optional bBoldPage As Boolean = True,
                                        Optional bCapTOC As Boolean = True,
                                        Optional bCapPage As Boolean = False,
                                        Optional bUnderlineTOC As Boolean = False,
                                        Optional bUnderlinePage As Boolean = False,
                                        Optional iPageNoStyle As Integer = WdPageNumberStyle.wdPageNumberStyleLowercaseRoman,
                                        Optional iPunctuation As mpTOCPagePunctuation = mpTOCPagePunctuation.mpTOCPagePunctuationHyphens,
                                        Optional bRetagMP10HeadersFooters As Boolean = False) _
                                        As Word.Range
            'bRetagMP10HeadersFooters parameter added in 9.9.5008
            Dim rngLocation As Word.Range
            Dim secTOC As Word.Section
            Dim iRightTabPos As Integer
            Dim rngTOCStart As Word.Range
            Dim bSecIsAtEOF As Boolean
            Dim rngPage As Word.Range
            Dim styNormal As Word.Style
            Dim sWidth As Single
            Dim rngFNTarget As Word.Range
            Dim rngHF As Word.Range
            Dim xWarning As String
            Dim xPreceding As String = ""
            Dim xTrailing As String = ""
            Dim rFirmName As Word.Range
            Dim bExtraSection As Boolean
            Dim rngLabel As Word.Range
            Dim styTOCHeader As Word.Style

            Try
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If bInsertTOA Then
                        CurWordApp.StatusBar = "Insertion Section TR..."
                    Else
                        CurWordApp.StatusBar = "Insertion Section TM..."
                        g_oStatus.UpdateProgress(2)
                    End If
                Else
                    If bInsertTOA Then
                        CurWordApp.StatusBar = "Inserting TOA Section..."
                    Else
                        CurWordApp.StatusBar = "Inserting TOC Section..."
                        g_oStatus.UpdateProgress(2)
                    End If
                End If

                EchoOff()

                '   if location is not specified,
                '   create section at end of doc,
                '   else at location vLocation
                If vLocation Is Nothing Then
                    With CurWordApp.ActiveDocument
                        rngLocation = .Content
                        rngLocation.EndOf()
                    End With
                Else
                    rngLocation = vLocation
                End If

                '   Is TOC going to be inserted at end of doc?
                bSecIsAtEOF = (rngLocation.End >= CurWordApp.ActiveDocument.Content.End - 1)

                '   If EOF, insert additional section?
                bExtraSection = xAppGetFirmSetting("TOC", "InsertExtraSectionAfterTOC")

                With rngLocation
                    If bSecIsAtEOF And bExtraSection Then
                        '           insert blank paragraph at end of document to prevent
                        '           section break from appearing on same line as text
                        .InsertParagraphAfter()
                        .Style = WdBuiltinStyle.wdStyleNormal
                        .EndOf()

                        .InsertBreak(WdBreakType.wdSectionBreakNextPage)

                        xWarning = "WARNING:  This section retains the original formatting, " &
                                "including headers and footers, of the main document.  " &
                                "If you delete the section break above this message " &
                                "(which is visible ONLY in Normal View), any special " &
                                "formatting, including headers and footers for the " &
                                "Table of Contents/Authorities section will be lost." & vbCrLf &
                                "If you delete the section break above the Table of " &
                                "Contents/Authorities, you will overwrite the headers and " &
                                "footers of the main document with Table of Contents/Authorities " &
                                "headers and footers." & vbCrLf &
                                "To delete the Table of Contents/Authorities, begin your " &
                                "selection at the section break above the TOC/TOA section and " &
                                "continue through the end of this message."

                        .InsertAfter(xWarning)
                        .ListFormat.RemoveNumbers()
                        .Style = WdBuiltinStyle.wdStyleBodyText

                        '           unlink headers/footers
                        bUnlinkHeadersFooters(.Sections(1))
                        With .Sections(1).Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers
                            'GLOG 5609 - new option to continue numbering
                            .RestartNumberingAtSection = bRestartPageNumbering
                            If bRestartPageNumbering Then _
                                .StartingNumber = 1
                        End With

                        '           redefine range to be before last section break
                        .StartOf()
                        .Move(WdUnits.wdCharacter, -1)
                    End If

                    '       create section
                    'GLOG 5609 - pass argument whether to continue numbering
                    secTOC = secCreateSection(rngLocation, , ,
                        bRestartPageNumbering, bRetagMP10HeadersFooters)

                End With

                With CurWordApp.ActiveDocument
                    '       set ranges for TOC section
                    rngTOCStart = .Range(secTOC.Range.Start,
                                        secTOC.Range.Start)
                End With

                '   edit section Headers/Footers
                With secTOC
                    With .PageSetup
                        .DifferentFirstPageHeaderFooter = True
                        Try
                            .TopMargin = CurWordApp.InchesToPoints(oCustTOC.TopMargin)
                        Catch
                        End Try
                        Try
                            .BottomMargin = CurWordApp.InchesToPoints(oCustTOC.BottomMargin)
                        Catch
                        End Try
                        Try
                            .HeaderDistance = CurWordApp.InchesToPoints(oCustTOC.HeaderDistance)
                        Catch
                        End Try
                        Try
                            .FooterDistance = CurWordApp.InchesToPoints(oCustTOC.FooterDistance)
                        Catch
                        End Try
                        .VerticalAlignment = WdVerticalAlignment.wdAlignVerticalTop
                        .Orientation = WdOrientation.wdOrientPortrait
                        If oCustTOC.PleadingPaperType = mpPleadingPaperTypes.mpPleadingPaperNone Then
                            '               get position of the tab stop for the "Page" heading
                            iRightTabPos = .PageWidth -
                                (.LeftMargin + .RightMargin + .Gutter)
                        Else
                            '               get appropriate width of second
                            '               col of TOC Header from margins
                            sWidth = .PageWidth -
                                (.LeftMargin + .RightMargin + .Gutter) + 12
                        End If
                    End With

                    With .Headers
                        With .Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                            .LinkToPrevious = False
                            .Range.Delete()

                            '---            insert boilerplate
                            With .Range
                                .InsertFile(oCustTOC.BPFile, oCustTOC.HeaderBookmark, , , False)
                                .WholeStory()
                                If oCustTOC.PleadingPaperType <> mpPleadingPaperTypes.mpPleadingPaperNone Then _
                                        .Tables(1).Columns.Last.Width = sWidth
                                '---                apply attributes; relabel TOC
                                rngLabel = rngHeaderBookmark(.Duplicate,
                                    "zzmpHeaderText_TOC")
                                If rngLabel Is Nothing Then
                                    With .Find
                                        .ClearFormatting()
                                        .Text = "Table of Contents"
                                        .Execute()
                                        If .Found Then
                                            With .Parent
                                                If bInsertTOA Then _
                                                    .Text = "Table of Authorities"
                                                If g_bAllowHeaderFooterEdit Then
                                                    If bBoldTOC Then _
                                                        .Bold = True
                                                    If bCapTOC Then _
                                                        .Font.AllCaps = True
                                                    If bUnderlineTOC Then _
                                                        .Font.Underline = WdUnderline.wdUnderlineSingle
                                                End If
                                            End With
                                        End If
                                    End With
                                Else
                                    With rngLabel
                                        If bInsertTOA Then _
                                            .Text = "Table of Authorities"
                                        If g_bAllowHeaderFooterEdit Then
                                            If bBoldTOC Then _
                                                .Bold = True
                                            If bCapTOC Then _
                                                .Font.AllCaps = True
                                            If bUnderlineTOC Then _
                                                .Font.Underline = WdUnderline.wdUnderlineSingle
                                        End If
                                    End With
                                End If
                                .WholeStory()

                                rngLabel = rngHeaderBookmark(.Duplicate,
                                    "zzmpHeaderText_Page")
                                If rngLabel Is Nothing Then
                                    With .Find
                                        .ClearFormatting()
                                        .Text = "Page"
                                        .Execute()
                                        If .Found Then
                                            With .Parent
                                                If bInsertTOA Then _
                                                    .Text = "Page(s)"
                                                If g_bAllowHeaderFooterEdit Then
                                                    If bBoldPage Then _
                                                        .Bold = True
                                                    If bCapPage Then _
                                                        .Font.AllCaps = True
                                                    If bUnderlinePage Then _
                                                        .Font.Underline = WdUnderline.wdUnderlineSingle
                                                End If
                                            End With
                                        End If
                                    End With
                                Else
                                    With rngLabel
                                        If bInsertTOA Then _
                                            .Text = "Page(s)"
                                        If g_bAllowHeaderFooterEdit Then
                                            If bBoldPage Then _
                                                .Bold = True
                                            If bCapPage Then _
                                                .Font.AllCaps = True
                                            If bUnderlinePage Then _
                                                .Font.Underline = WdUnderline.wdUnderlineSingle
                                        End If
                                    End With
                                End If
                                .WholeStory()
                            End With

                            '               look for "(continued)" bookmark or text
                            '               and delete from first page
                            rngLabel = rngHeaderBookmark(.Range.Duplicate,
                                "zzmpHeaderText_Continued")
                            If rngLabel Is Nothing Then
                                rngInsertAt(xLabel:="(continued)",
                                            xText:="",
                                            vSearch:= .Range)
                            Else
                                rngLabel.Delete()
                            End If
                            If oCustTOC.PleadingPaperType = mpPleadingPaperTypes.mpPleadingPaperNone Then
                                '                   adjust right tab
                                '                    Set rngPage = rngInsertAt(xLabel:="Page", _
                                '                                              xText:="Page", _
                                '                                              vSearch:=.Range)
                                '
                                '                    If Not (rngPage Is Nothing) Then
                                '                        With rngPage.ParagraphFormat.TabStops
                                '                            .Item(.Count).Position = iRightTabPos
                                '                        End With
                                '                    End If

                                '                   replace above with full tab adjustment
                                .Range.WholeStory()
                                bRet = rngAdjustHeaderTabs(.Range)
                            End If
                        End With

                        With .Item(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                            .LinkToPrevious = False
                            .Range.Delete()

                            '---            insert boilerplate
                            With .Range
                                .InsertFile(oCustTOC.BPFile, oCustTOC.HeaderBookmark, , , False)
                                .WholeStory()
                                If oCustTOC.PleadingPaperType <> mpPleadingPaperTypes.mpPleadingPaperNone Then _
                                        .Tables(1).Columns.Last.Width = sWidth

                                '---                apply attributes; relabel TOC
                                rngLabel = rngHeaderBookmark(.Duplicate,
                                    "zzmpHeaderText_TOC")
                                If rngLabel Is Nothing Then
                                    With .Find
                                        .ClearFormatting()
                                        .Text = "Table of Contents"
                                        .Execute()
                                        If .Found Then
                                            With .Parent
                                                If bInsertTOA Then _
                                                    .Text = "Table of Authorities"
                                                If g_bAllowHeaderFooterEdit Then
                                                    If bBoldTOC Then _
                                                        .Bold = True
                                                    If bCapTOC Then _
                                                        .Font.AllCaps = True
                                                    If bUnderlineTOC Then _
                                                        .Font.Underline = WdUnderline.wdUnderlineSingle
                                                End If
                                            End With
                                        End If
                                    End With
                                Else
                                    With rngLabel
                                        If bInsertTOA Then _
                                            .Text = "Table of Authorities"
                                        If g_bAllowHeaderFooterEdit Then
                                            If bBoldTOC Then _
                                                .Bold = True
                                            If bCapTOC Then _
                                                .Font.AllCaps = True
                                            If bUnderlineTOC Then _
                                                .Font.Underline = WdUnderline.wdUnderlineSingle
                                        End If
                                    End With
                                End If
                                .WholeStory()

                                rngLabel = rngHeaderBookmark(.Duplicate,
                                    "zzmpHeaderText_Page")
                                If rngLabel Is Nothing Then
                                    With .Find
                                        .ClearFormatting()
                                        .Text = "Page"
                                        .Execute()
                                        If .Found Then
                                            With .Parent
                                                If bInsertTOA Then _
                                                    .Text = "Page(s)"
                                                If g_bAllowHeaderFooterEdit Then
                                                    If bBoldPage Then _
                                                        .Bold = True
                                                    If bCapPage Then _
                                                        .Font.AllCaps = True
                                                    If bUnderlinePage Then _
                                                        .Font.Underline = WdUnderline.wdUnderlineSingle
                                                End If
                                            End With
                                        End If
                                    End With
                                Else
                                    With rngLabel
                                        If bInsertTOA Then _
                                            .Text = "Page(s)"
                                        If g_bAllowHeaderFooterEdit Then
                                            If bBoldPage Then _
                                                .Bold = True
                                            If bCapPage Then _
                                                .Font.AllCaps = True
                                            If bUnderlinePage Then _
                                                .Font.Underline = WdUnderline.wdUnderlineSingle
                                        End If
                                    End With
                                End If
                                .WholeStory()
                            End With

                            '                If bInsertTOA Then
                            '                    rngInsertAt xLabel:="Table of Contents", _
                            '                                xText:="TABLE OF AUTHORITIES", _
                            '                                vSearch:=.Range
                            '                    .Range.WholeStory
                            '                    rngInsertAt xLabel:="Page", _
                            '                                xText:="Page(s)", _
                            '                                vSearch:=.Range
                            '                End If
                            If oCustTOC.PleadingPaperType = mpPleadingPaperTypes.mpPleadingPaperNone Then
                                '                   adjust right tab
                                '                    Set rngPage = rngInsertAt(xLabel:="Page", _
                                '                                              xText:="Page", _
                                '                                              vSearch:=.Range)
                                '
                                '                    If Not (rngPage Is Nothing) Then
                                '                        With rngPage.ParagraphFormat.TabStops
                                '                            .Item(.Count).Position = iRightTabPos
                                '                        End With
                                '                    End If

                                '                   replace above with full tab adjustment
                                .Range.WholeStory()
                                bRet = rngAdjustHeaderTabs(.Range)
                            End If
                        End With
                    End With


                    'modify footers
                    '9.9.5005 - skip footers entirely if mp9 or mp10 pleading paper
                    If oCustTOC.FooterBookmark <> "zzmpNothing" Then
                        If g_bAllowHeaderFooterEdit Then
                            Select Case iPunctuation
                                Case mpTOCPagePunctuation.mpTOCPagePunctuationNone
                                    xPreceding = ""
                                    xTrailing = ""
                                Case mpTOCPagePunctuation.mpTOCPagePunctuationHyphens
                                    xPreceding = "-"
                                    xTrailing = "-"
                                Case mpTOCPagePunctuation.mpTOCPagePunctuationHyphensWithSpaces
                                    xPreceding = "- "
                                    xTrailing = " -"
                            End Select
                        Else
                            xPreceding = g_xPageNoIntroChar
                            xTrailing = g_xPageNoTrailingChar
                        End If

                        With .Footers
                            With .Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                                .LinkToPrevious = False
                                With .Range
                                    .Delete()
                                    .InsertFile(oCustTOC.BPFile, oCustTOC.FooterBookmark, , , False)
                                    .WholeStory()
                                End With

                                rngLocation = rngInsertAt(xLabel:="(page)",
                                                              xText:="",
                                                              vSearch:= .Range)
                                If Not rngLocation Is Nothing Then
                                    With rngLocation
                                        .Style = WdBuiltinStyle.wdStylePageNumber
                                        .InsertAfter(xPreceding & xTrailing)
                                        .EndOf()
                                        .Move(WdUnits.wdCharacter, 0 - Len(xTrailing))
                                        .Fields.Add(rngLocation, WdFieldType.wdFieldPage)
                                    End With
                                End If

                                If g_bVariablesInBoilerplate Then '9.9.5005
                                    rngLocation = .Range
                                    With rngLocation
                                        .WholeStory()
                                        rngInsertAt("(Doc Title)", oCustTOC.FooterTitle, , , , rngLocation)
                                    End With
                                End If

                                '           Note: this will not work w/current MacPac TOC footer,
                                '           which isn't centered but probably should be
                                If oCustTOC.bIsCenteredFooter Then
                                    AdjustCenteredFooter(.Range())
                                End If
                            End With

                            With .Item(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                                .LinkToPrevious = False
                                With .Range
                                    .Delete()
                                    .InsertFile(oCustTOC.BPFile, oCustTOC.FooterBookmark, , , False)
                                    .WholeStory()
                                End With

                                rngLocation = rngInsertAt(xLabel:="(page)",
                                                              xText:="",
                                                              vSearch:= .Range)
                                If Not rngLocation Is Nothing Then
                                    With rngLocation
                                        .Style = WdBuiltinStyle.wdStylePageNumber
                                        .InsertAfter(xPreceding & xTrailing)
                                        .EndOf()
                                        .Move(WdUnits.wdCharacter, 0 - Len(xTrailing))
                                        .Fields.Add(rngLocation, WdFieldType.wdFieldPage)
                                    End With
                                End If

                                If g_bVariablesInBoilerplate Then '9.9.5005
                                    rngLocation = .Range
                                    With rngLocation
                                        .WholeStory()
                                        rngInsertAt("(Doc Title)", oCustTOC.FooterTitle, , , , rngLocation)
                                    End With
                                End If

                                '           Note: this will not work w/current MacPac TOC footer,
                                '           which isn't centered but probably should be
                                If oCustTOC.bIsCenteredFooter Then
                                    AdjustCenteredFooter(.Range())
                                End If
                            End With
                        End With

                        '---add firm name detail to pleading paper
                        If g_bVariablesInBoilerplate Then '9.9.5005
                            Select Case oCustTOC.PleadingPaperType
                                Case mpPleadingPaperTypes.mpPleadingPaper26LineFNLand,
                                        mpPleadingPaperTypes.mpPleadingPaper28LineFNLand
                                    '---delete FN detail from footer if LS
                                    rngHF = .Footers.Item(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                                    rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                        (rngHF,
                                                        secTOC)
                                    If Not rngFNTarget Is Nothing Then _
                                        rngFNTarget.Delete()
                                    rngHF = .Footers.Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                                    rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                        (rngHF,
                                                        secTOC)
                                    If Not rngFNTarget Is Nothing Then _
                                        rngFNTarget.Delete()

                                    With .Headers
                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                                            rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:=oCustTOC.FirmName,
                                                        vSearch:= .Range)
                                            .Range.WholeStory()
                                            rngInsertAt(xLabel:="(Office)",
                                                        xText:=oCustTOC.Office,
                                                        vSearch:= .Range)
                                        End With

                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                                            rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:=oCustTOC.FirmName,
                                                        vSearch:= .Range)

                                            .Range.WholeStory()
                                            rngInsertAt(xLabel:="(Office)",
                                                        xText:=oCustTOC.Office,
                                                        vSearch:= .Range)
                                        End With

                                    End With

                                    With .Footers
                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                                            rFirmName = rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:="",
                                                        vSearch:= .Range)
                                            If Not rFirmName Is Nothing Then _
                                                rFirmName.Cells(1).Range.Delete()
                                        End With

                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                                            rFirmName = rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:="",
                                                        vSearch:= .Range)
                                            If Not rFirmName Is Nothing Then _
                                                rFirmName.Cells(1).Range.Delete()
                                        End With
                                    End With

                                Case mpPleadingPaperTypes.mpPleadingPaper26LineFNPort,
                                        mpPleadingPaperTypes.mpPleadingPaper28LineFNPort,
                                        mpPleadingPaperTypes.mpPleadingPaper26Line,
                                        mpPleadingPaperTypes.mpPleadingPaper28Line,
                                        mpPleadingPaperTypes.mpPleadingPaperNone

                                    '---add firm name detail if indicated
                                    If oCustTOC.PleadingPaperType < mpPleadingPaperTypes.mpPleadingPaper26LineFNPort Then

                                        rngHF = .Footers.Item(WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                                        rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                            (rngHF,
                                                            secTOC)
                                        If Not rngFNTarget Is Nothing Then _
                                            rngFNTarget.Delete()
                                        rngHF = .Footers.Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage).Range
                                        rngFNTarget = oCustTOC.rngGetFirmNameTarget _
                                                            (rngHF,
                                                            secTOC)
                                        If Not rngFNTarget Is Nothing Then _
                                            rngFNTarget.Delete()
                                    End If

                                    With .Footers
                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterFirstPage)
                                            rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:=oCustTOC.FirmName,
                                                        vSearch:= .Range)
                                            .Range.WholeStory()
                                            rngInsertAt(xLabel:="(Office)",
                                                        xText:=oCustTOC.Office,
                                                        vSearch:= .Range)
                                        End With

                                        With .Item(WdHeaderFooterIndex.wdHeaderFooterPrimary)
                                            rngInsertAt(xLabel:="(Firm Name)",
                                                        xText:=oCustTOC.FirmName,
                                                        vSearch:= .Range)

                                            .Range.WholeStory()
                                            rngInsertAt(xLabel:="(Office)",
                                                        xText:=oCustTOC.Office,
                                                        vSearch:= .Range)
                                        End With
                                    End With

                                Case Else
                            End Select
                        End If
                    End If

                    '9.9.5005
                    With .Footers.Item(WdHeaderFooterIndex.wdHeaderFooterPrimary).PageNumbers
                        'GLOG 5609 - new option to continue numbering
                        .RestartNumberingAtSection = bRestartPageNumbering
                        If bRestartPageNumbering Then _
                            .StartingNumber = 1
                        If g_bAllowHeaderFooterEdit Then
                            .NumberStyle = iPageNoStyle
                        Else
                            .NumberStyle = g_iPageNoStyle
                        End If
                    End With
                End With

                '   this is a patch for MP2K;
                '   force TOC header font to match normal style;
                '   if this isn't desired, use different style
                With CurWordApp.ActiveDocument
                    styNormal = .Styles(WdBuiltinStyle.wdStyleNormal)
                    Try
                        styTOCHeader = .Styles("TOC Header")
                    Catch
                    End Try
                    If Not styTOCHeader Is Nothing Then
                        With styTOCHeader.Font
                            .Name = styNormal.Font.Name
                            .Size = styNormal.Font.Size
                        End With
                    End If
                End With

                rngInsertTOCTOASection = rngTOCStart
            Finally
                EchoOn()
            End Try
        End Function

        Friend Shared Function rngGetTOCTarget(docCurrent As Word.Document,
                                 Optional iLoc As Integer = DocPosition.mpAtEOF,
                                 Optional bBoldTOC As Boolean = True,
                                 Optional bBoldPage As Boolean = True,
                                 Optional bCapTOC As Boolean = True,
                                 Optional bCapPage As Boolean = False,
                                 Optional bUnderlineTOC As Boolean = False,
                                 Optional bUnderlinePage As Boolean = False,
                                 Optional iPageNoStyle As Integer = WdPageNumberStyle.wdPageNumberStyleLowercaseRoman,
                                 Optional iPunctuation As mpTOCPagePunctuation = mpTOCPagePunctuation.mpTOCPagePunctuationHyphens,
                                 Optional bRestartPageNumbering As Boolean = True) _
                                 As Word.Range
            'returns proposed location for TOC -
            'first checks for existing TOC bookmark,
            'then for TOC placeholder (earlier MP versions),
            'then actual TOC.  if not found, checks for
            'existing TOA bookmark, then actual TOA. if
            'none found, then inserts at EOF.
            'GLOG 5609 - added argument for new option to continue page numbering
            Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"

            Dim rngTOAStart As Word.Range
            Dim rngTOCStart As Word.Range
            Dim rngLOEStart As Word.Range
            Dim rngEOF As Word.Range
            Dim rngContent As Word.Range
            Dim toaExisting As Word.TableOfAuthorities
            Dim ftrExisting As HeaderFooter
            Dim rngLoc As Word.Range
            Dim rngMarker As Word.Range
            Dim xTOA As String = ""
            Dim lStart As Long
            Dim lShowTags As Long
            Dim bAtStartOfBlockTag As Boolean
            Dim bParaAdded As Boolean

            With docCurrent

                rngContent = .Content

                '       first check for existing bookmark, then for
                '       placeholder, then for existing TOC -
                If .Bookmarks.Exists("mpTableOfContents") Then
                    rngGetTOCTarget = .Bookmarks("mpTableOfContents").Range

                    '           ensure that bookmark doesn't include trailing section break
                    If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                        rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                    End If

                    '           ensure that bookmark doesn't include preceding section break
                    If Asc(rngGetTOCTarget.Characters.First.Text) = 12 Then
                        rngGetTOCTarget.MoveStart(WdUnits.wdCharacter, 1)
                    End If

                    '           TOC at start of doc
                    If rngGetTOCTarget.Start = CurWordApp.ActiveDocument.Range.Start Then
                        'GLOG 5149 - modified conditional to fix problem whereby warning was
                        'getting only partially deleted with TOC at start of document
                        While (rngGetTOCTarget.Paragraphs.Count > 1) And
                                (Not bIsTOCStyle(rngGetTOCTarget.Paragraphs(1).Style.NameLocal)) And
                                (InStr(rngGetTOCTarget.Text, mpNoEntriesWarning) = 0) And
                                (InStr(rngGetTOCTarget.Text, mpNoEntriesWarningFrench) = 0) 'GLOG 8785 (dm)
                            rngGetTOCTarget.MoveStart(WdUnits.wdParagraph, 1)
                            If rngGetTOCTarget.End > .Bookmarks("mpTableOfContents").Range.End Then
                                rngGetTOCTarget = .Bookmarks("mpTableOfContents").Range
                                If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                                    rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                                End If
                                rngGetTOCTarget.Collapse(WdCollapseDirection.wdCollapseEnd)
                            End If
                        End While
                        If Asc(rngGetTOCTarget.Characters.First.Text) = 12 Then
                            rngGetTOCTarget.MoveStart(WdUnits.wdCharacter, 1)
                        End If
                    End If

                    With rngGetTOCTarget
                        '               if end of TOC marker exists, expand range to include it
                        On Error Resume Next
                        If .Fields.Count = 0 Then
                            rngMarker = .Next(WdUnits.wdParagraph)
                        Else
                            '                   incredibly, if TOC is a field, .Next skips a paragraph
                            rngMarker = .Next(WdUnits.wdParagraph).Previous(WdUnits.wdParagraph)
                        End If
                        On Error GoTo 0

                        If Not rngMarker Is Nothing Then
                            If rngMarker.Text = mpEndOfTOCWarning & vbCr Then _
                                .MoveEnd(WdUnits.wdParagraph)
                        End If

                        '               delete existing TOC
                        If Len(rngGetTOCTarget.Text) > 1 Then
                            '                   attempting to delete a native TOC field when
                            '                   when there's preceding text in the range
                            '                   can cause an error - unlink as a precaution
                            On Error Resume Next
                            .Fields.Unlink()
                            On Error GoTo 0

                            .Delete()
                        End If
                    End With
                ElseIf .Bookmarks.Exists("TableOfContents") Then
                    rngGetTOCTarget = .Bookmarks("TableOfContents").Range
                    If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                        rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    If rngGetTOCTarget.Start = CurWordApp.ActiveDocument.Range.Start Then
                        While Not bIsTOCStyle(rngGetTOCTarget.Paragraphs(1).Style.NameLocal) 'GLOG 8785 (dm)
                            rngGetTOCTarget.MoveStart(WdUnits.wdParagraph, 1)
                            If rngGetTOCTarget.End > .Bookmarks("TableOfContents").Range.End Then
                                rngGetTOCTarget = .Bookmarks("TableOfContents").Range
                                If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                                    rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                                End If
                                rngGetTOCTarget.Collapse(WdCollapseDirection.wdCollapseEnd)
                            End If
                        End While
                        If Asc(rngGetTOCTarget.Characters.First.Text) = 12 Then
                            rngGetTOCTarget.MoveStart(WdUnits.wdCharacter, 1)
                        End If
                    End If
                    If Len(rngGetTOCTarget) > 1 Then
                        rngGetTOCTarget.Delete()
                    End If
                ElseIf .Bookmarks.Exists("PTOC") Then
                    rngGetTOCTarget = .Bookmarks("PTOC").Range
                    If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                        rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    If Len(rngGetTOCTarget) > 1 Then
                        rngGetTOCTarget.Delete()
                    End If
                ElseIf .Bookmarks.Exists("TOC") Then
                    rngGetTOCTarget = .Bookmarks("TOC").Range
                    If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                        rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    If Len(rngGetTOCTarget) > 1 Then
                        rngGetTOCTarget.Delete()
                    End If
                ElseIf rngContent.Find.Execute(mpTOCPlaceholder) Then
                    '           check for TOC placeholder
                    rngGetTOCTarget = rngContent.Paragraphs(1).Range
                    If Asc(rngGetTOCTarget.Characters.Last.Text) = 12 Then
                        rngGetTOCTarget.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    If Len(rngGetTOCTarget) > 1 Then
                        rngGetTOCTarget.Delete()
                    End If
                ElseIf .TablesOfContents.Count Then
                    '           check for existing TOC, if it exists, insert after
                    rngGetTOCTarget = .TablesOfContents(1).Range
                    rngGetTOCTarget.Delete()

                    '           check for end of TOC marker - user may have replaced
                    '           MacPac TOC field with native one
                    With rngGetTOCTarget.Paragraphs(1).Range
                        If .Text = mpEndOfTOCWarning & vbCr Then _
                            .Delete()
                    End With
                ElseIf iLoc = DocPosition.mpAboveTOA Then
                    '           insert above TOA
                    xTOA = xGetTOABookmark()
                    If xTOA <> "" Then
                        rngTOAStart = .Bookmarks(xTOA).Range
                    ElseIf .TablesOfAuthorities.Count Then
                        rngTOAStart = .TablesOfAuthorities(1).Range
                    End If
                    With rngTOAStart
                        .StartOf()
                        '                For Each ftrExisting In .Sections(1).Footers
                        '                    ftrExisting.PageNumbers.RestartNumberingAtSection = False
                        '                Next ftrExisting
                        If .Start = 0 Then
                            lStart = 0
                        Else
                            lStart = .Start - 1
                        End If
                        rngTOCStart = docCurrent.Range(lStart, lStart)
                    End With

                    '9.9.4004 - prevent "outside of block-level tag" error
                    rngTOCStart.Select()
                    If BeforeBlockLevelCC() Then
                        bParaAdded = True
                        CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                    ElseIf AfterBlockLevelCC() Then
                        bParaAdded = True
                        CurWordApp.Selection.EndOf(WdUnits.wdParagraph)
                    ElseIf CursorIsAtStartOfBlockTag() Then 'added for GLOG 15871 (dm)
                        bParaAdded = True
                        lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, True)
                        CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                    End If
                    If bParaAdded Then
                        CurWordApp.Selection.InsertParagraph()
                        CurWordApp.Selection.StartOf()
                        CurWordApp.Selection.Style = WdBuiltinStyle.wdStyleNormal
                        If lShowTags Then  'added for GLOG 15871 (dm)
                            lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument,
                                False)
                        End If
                    End If
                    rngTOCStart = CurWordApp.Selection.Range

                    'GLOG 8863 - get page numbering style from TOA
                    iPageNoStyle = CInt(rngTOAStart.Sections(1).Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary) _
                        .PageNumbers.NumberStyle)

                    rngGetTOCTarget = rngInsertTOCTOASection(rngTOCStart, False,
                        bRestartPageNumbering, , , , , , , iPageNoStyle)

                    'GLOG 5355 (9.9.5007) - continue page numbering in TOA section
                    'after inserting TOC section, not before (as we were doing previously)
                    With rngTOAStart
                        .StartOf()
                        For Each ftrExisting In .Sections(1).Footers
                            ftrExisting.PageNumbers.RestartNumberingAtSection = False
                        Next ftrExisting
                    End With

                    '        ElseIf .Bookmarks.Exists("mpListOfExhibits") Then
                    ''           toc not found - check for MP List of Exhibits,
                    ''           if it exists, insert before
                    '            Set rngLOEStart = .Bookmarks("mpListOfExhibits").Range
                    '
                    '            With rngLOEStart
                    '                .StartOf
                    '                For Each ftrExisting In .Sections(1).Footers
                    '                    ftrExisting.PageNumbers.RestartNumberingAtSection = False
                    '                Next ftrExisting
                    '                Set rngTOCStart = docCurrent.Range(.Start - 1, .Start - 1)
                    '            End With
                    '
                    '            Set rngGetTOCTarget = rngInsertTOCTOASection(rngTOCStart)
                Else
                    '           force eof or bof is CurWordApp.Selection is at end
                    '           or start of doc and user says at insertion point
                    If iLoc = DocPosition.mpAtInsertion Then
                        CurWordApp.Selection.Collapse(WdCollapseDirection.wdCollapseEnd)

                        'move to start of row
                        If CurWordApp.Selection.Information(WdInformation.wdWithInTable) Then _
                            CurWordApp.Selection.StartOf(WdUnits.wdRow)

                        'determine whether at start of block tag
                        If g_bXMLSupport Then _
                            bAtStartOfBlockTag = CursorIsAtStartOfBlockTag()

                        '               if cursor is in middle of paragraph, move to end -
                        '9.9.4003 - modified conditional to check for text at start of paragraph,
                        'rather than looking at the range - this now seems simpler than worrying
                        'about content controls and xml tags
                        '                If (CurWordApp.Selection.Start <> CurWordApp.Selection.Paragraphs(1).Range.Start) And _
                        '                        (Not bAtStartOfBlockTag) Then
                        If CurWordApp.ActiveDocument.Range(CurWordApp.Selection.Paragraphs(1).Range.Start,
                                CurWordApp.Selection.Start).Text <> "" Then
                            CurWordApp.Selection.SetRange(Start:=CurWordApp.Selection.Paragraphs(1).Range.End - 1,
                                End:=CurWordApp.Selection.Paragraphs(1).Range.End - 1)
                        Else
                            '9.9.4003 - move to start of paragraph if necessary
                            CurWordApp.Selection.SetRange(Start:=CurWordApp.Selection.Paragraphs(1).Range.Start,
                                End:=CurWordApp.Selection.Paragraphs(1).Range.Start)
                        End If

                        If (CurWordApp.Selection.Start = 0) Or InRowAtStartOfDoc() Then
                            iLoc = DocPosition.mpAtBOF
                        ElseIf (CurWordApp.Selection.End >= CurWordApp.ActiveDocument.Content.End - 1) Then
                            iLoc = DocPosition.mpAtEOF
                        ElseIf CurWordApp.Selection.Paragraphs(1).Range.Text = vbCr And
                                CurWordApp.Selection.Start = CurWordApp.Selection.Sections(1).Range.Start Then
                            '                   in this case, i.e. empty para at start of section, moving a
                            '                   character will result in an extra section break
                        ElseIf (CurWordApp.Selection.Start = CurWordApp.Selection.Paragraphs(1).Range.Characters.Last.Start) And
                                (Not bAtStartOfBlockTag) Then
                            CurWordApp.Selection.MoveStart(WdUnits.wdCharacter, 1)
                        End If
                    End If

                    '           neither TOA nor TOC found - insert at eof
                    Select Case iLoc
                        Case DocPosition.mpAtBOF
                            rngLoc = docCurrent.Content
                            rngLoc.StartOf()

                            'ensure that not at start of block tag
                            If g_bXMLSupport Then
                                rngLoc.Select()
                                If Not InRowAtStartOfDoc() Then
                                    If CursorIsAtStartOfBlockTag() Then
                                        bParaAdded = True
                                        lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, True)
                                        CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                                        CurWordApp.Selection.InsertParagraph()
                                        CurWordApp.Selection.StartOf()
                                        CurWordApp.Selection.Style = WdBuiltinStyle.wdStyleNormal
                                        If lShowTags Then
                                            lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument,
                                                False)
                                        End If
                                    Else
                                        '9.9.4003 - prevent "outside of block-level tag" error
                                        If BeforeBlockLevelCC() Then
                                            bParaAdded = True
                                            CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                                            CurWordApp.Selection.InsertParagraph()
                                            CurWordApp.Selection.StartOf()
                                            CurWordApp.Selection.Style = WdBuiltinStyle.wdStyleNormal
                                        End If
                                    End If
                                End If
                            End If
                        Case DocPosition.mpAtEOF
                            rngLoc = docCurrent.Content
                            rngLoc.EndOf()
                            '                   insert paragraph if necessary
                            If rngLoc.Paragraphs.Last.Range.Characters.Count > 1 Then
                                rngLoc.InsertParagraphAfter()
                                rngLoc.EndOf()
                            End If
                        Case DocPosition.mpAtInsertion
                            'ensure that not at start of block tag
                            If g_bXMLSupport And (Not InRowAtStartOfDoc()) Then
                                If CursorIsAtStartOfBlockTag() Then
                                    bParaAdded = True
                                    lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, True)
                                    CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                                    On Error Resume Next
                                    CurWordApp.Selection.InsertParagraph()
                                    If Err.Number <> 0 Then
                                        'an error will occur if block tag spans more than row
                                        If CurWordApp.Selection.Information(WdInformation.wdWithInTable) Then
                                            Dim rngBeforeTable As Word.Range
                                            rngBeforeTable = rngAddParaBeforeTable(CurWordApp.Selection.Tables(1))
                                            rngBeforeTable.Select()
                                        Else
                                            Throw New Exception()
                                        End If
                                        On Error GoTo 0
                                    End If
                                    CurWordApp.Selection.StartOf()
                                    CurWordApp.Selection.Style = WdBuiltinStyle.wdStyleNormal
                                    If lShowTags Then
                                        lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument,
                                            False)
                                    End If
                                Else
                                    '9.9.4003 - prevent "outside of block-level tag" error
                                    If BeforeBlockLevelCC() Then
                                        bParaAdded = True
                                        CurWordApp.Selection.StartOf(WdUnits.wdParagraph)
                                    ElseIf AfterBlockLevelCC() Then
                                        bParaAdded = True
                                        CurWordApp.Selection.EndOf(WdUnits.wdParagraph)
                                    End If
                                    If bParaAdded Then
                                        CurWordApp.Selection.InsertParagraph()
                                        CurWordApp.Selection.StartOf()
                                        CurWordApp.Selection.Style = WdBuiltinStyle.wdStyleNormal
                                    End If
                                End If
                            End If

                            rngLoc = CurWordApp.Selection.Range
                    End Select

                    '9.9.5008 - restore mp10 bookmarks when inserting at cursor
                    rngGetTOCTarget = rngInsertTOCTOASection(rngLoc,
                                                                 False,
                                                                 bRestartPageNumbering,
                                                                 bBoldTOC,
                                                                 bBoldPage,
                                                                 bCapTOC,
                                                                 bCapPage,
                                                                 bUnderlineTOC,
                                                                 bUnderlinePage,
                                                                 iPageNoStyle,
                                                                 iPunctuation,
                                                                 (iLoc = DocPosition.mpAtInsertion))

                    '            If iLoc = mpAtBOF Then rngGetTOCTarget.Style = wdBuiltInStyle.wdStyleNormal
                End If

                With rngGetTOCTarget
                    If UCase(.Style.namelocal) <> UCase(CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal).NameLocal) Then _
                        .Style = WdBuiltinStyle.wdStyleNormal

                    'delete added paragraph
                    If bParaAdded Then
                        With .Next(WdUnits.wdSection).Characters(1)
                            If .Text = vbCr Then _
                                .Delete()
                        End With
                    End If
                End With
            End With
        End Function
        Friend Shared Function bTOAExists(docCurrent As Word.Document) As Boolean
            'returns true if TOC exists -
            'GLOG 5022 (10/31/11) - added "zzmpFIXED_TableOfAuthorities" and "zzmpTEMP_TOA"
            With docCurrent
                bTOAExists = .Bookmarks.Exists("mpTableOfAuthorities") Or
                             .Bookmarks.Exists("_zzmpFIXED_TableOfAuthorities") Or
                             .Bookmarks.Exists("zzmpFIXED_TableOfAuthorities") Or
                             .Bookmarks.Exists("TableOfAuthorities") Or
                             .Bookmarks.Exists("PTOA") Or
                             .Bookmarks.Exists("TOA") Or
                             .Bookmarks.Exists("zzmpTEMP_TOA") Or
                             .TablesOfAuthorities.Count
            End With
        End Function

        Friend Shared Function xGetTOABookmark() As String
            'returns name of TOA bookmark
            xGetTOABookmark = ""
            With CurWordApp.ActiveDocument.Bookmarks
                If .Exists("_zzmpFIXED_TableOfAuthorities") Then
                    xGetTOABookmark = "_zzmpFIXED_TableOfAuthorities"
                ElseIf .Exists("zzmpFIXED_TableOfAuthorities") Then
                    'GLOG 5022 (10/31/11)
                    xGetTOABookmark = "zzmpFIXED_TableOfAuthorities"
                ElseIf .Exists("mpTableOfAuthorities") Then
                    xGetTOABookmark = "mpTableOfAuthorities"
                ElseIf .Exists("TableOfAuthorities") Then
                    xGetTOABookmark = "TableOfAuthorities"
                ElseIf .Exists("PTOA") Then
                    xGetTOABookmark = "PTOA"
                ElseIf .Exists("TOA") Then
                    xGetTOABookmark = "TOA"
                ElseIf .Exists("zzmpTEMP_TOA") Then
                    'GLOG 5022 (10/31/11)
                    xGetTOABookmark = "zzmpTEMP_TOA"
                End If
            End With
        End Function

        Friend Shared Function iTOCExists(docCurrent As Word.Document) As mpTOCExistsMarker
            'returns marker type for existing TOC

            Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"
            Dim rngContent As Word.Range
            iTOCExists = mpTOCExistsMarker.mpTOCExistsMarker_None
            With docCurrent
                rngContent = docCurrent.Content
                If .Bookmarks.Exists("mpTableOfContents") Or
                        .Bookmarks.Exists("TableOfContents") Or
                        .Bookmarks.Exists("PTOC") Or
                        .Bookmarks.Exists("TOC") Then
                    iTOCExists = mpTOCExistsMarker.mpTOCExistsMarker_Bookmark
                ElseIf rngContent.Find.Execute(mpTOCPlaceholder) Then
                    iTOCExists = mpTOCExistsMarker.mpTOCExistsMarker_Placeholder
                ElseIf .TablesOfContents.Count Then
                    iTOCExists = mpTOCExistsMarker.mpTOCExistsMarker_Field
                End If
            End With
        End Function

        Friend Shared Function xGetTCEntryText(rngHeading As Word.Range,
                                 iListLevel As Integer,
                                 xNumberTrailChar As String,
                                 Optional bUseQuotePlaceholders As Boolean = False) As String
            '   returns unformatted text for non-numbered
            '   or single numbered headings. use
            '   bInsertTCEntryText for other situations
            Dim mpQ As String
            Dim xLevelSwitch As String = ""
            Dim xCurNumber As String = ""
            Dim xTemp As String = ""
            Dim xHeading As String = ""

            xGetTCEntryText = ""
            mpQ = Chr(34)

            xLevelSwitch = " \l " & mpQ &
                        iListLevel & mpQ

            '   word automatically converts allcaps to ucase,
            '   so ensure that allcaps is removed before retrieval
            '   then reapplied
            With rngHeading
                If (Not CurWordApp.ActiveDocument.Styles(.Style).Font.AllCaps) _
                    And .Font.AllCaps Then
                    .Font.AllCaps = False
                    xHeading = .Text
                    .Font.AllCaps = True
                Else
                    xHeading = .Text
                End If
            End With

            If bUseQuotePlaceholders Then
                '       due to native Word bug that removes preceding quotes from TOC entries,
                '       TOC macro now looks for proprietary placeholders
                xTemp = xSubstitute(xHeading, mpQ, "zzmp34h")
                xTemp = xSubstitute(xTemp, ChrW(&H201C), "zzmp147h")
                xTemp = xSubstitute(xTemp, ChrW(&H201D), "zzmp148h")
            Else
                '       in filled TC codes, special chrs need to be preceded by "\"
                xTemp = xSubstitute(xHeading, mpQ, "\" & mpQ)
                xTemp = xSubstitute(xTemp, ChrW(&H201C), "\" & ChrW(&H201C))
                xTemp = xSubstitute(xTemp, ChrW(&H201D), "\" & ChrW(&H201D))
            End If

            '   get number
            xCurNumber = rngHeading.ListFormat.ListString

            '   concatenate
            If Len(xCurNumber) Then
                xTemp = mpQ & xCurNumber &
                        xNumberTrailChar & xTemp &
                        mpQ & xLevelSwitch
            Else
                xTemp = mpQ & xTemp & mpQ &
                        xLevelSwitch
            End If

            '   return
            xGetTCEntryText = xTemp
        End Function

        Friend Shared Function bInsertTCEntryText(rngHeading As Word.Range,
                                    rngTCEntryTextStart As Word.Range,
                                    iListLevel As Integer,
                                    xNumberTrailChar As String,
                                    Optional bUseQuotePlaceholders As Boolean = False) As Boolean
            '   inserts rngHeading as TCEntry
            '   text. Use when heading formatting is
            '   transferred to TOC, or when heading
            '   contains multiple numbers. Use
            '   xGetTCEntryText otherwise (it's faster)

            Dim rngRef As Word.Range
            Dim fldRef As Word.Field
            Dim xListLevelSwitch As String = ""
            Dim xCurNumber As String = ""
            Dim lXRefPos As Long
            Dim bmkHeading As Bookmark
            Dim xHeadingText As String = ""
            Dim bReset34 As Boolean
            Dim bReset147 As Boolean
            Dim bReset148 As Boolean

            bInsertTCEntryText = False
            xListLevelSwitch = " \l " & Chr(34) &
                        iListLevel & Chr(34)

            '   mark heading for later reference
            With rngHeading
                '   if last character in heading is a straight quote,
                '   use placeholder (fixes a native Word bug);
                '   otherwise, quotes need to be preceded by "\"
                '        If Right(.Text, 1) = Chr(34) Then
                '            .Characters.Last.Text = "zzmp34h"
                '            .MoveEnd wdUnits.wdCharacter, 7
                '        End If

                'GLOG 5620 - if range ends with a quote and space, shrink it
                'to exclude these characters - they will result in a backslash
                'in the TOC entry and the trailing quote won't get included in
                'the TOC in any case (see GLOG 5576)
                xHeadingText = .Text
                If (Strings.Right$(xHeadingText, 2) = Chr(34) & " ") Or
                        (Strings.Right$(xHeadingText, 2) = ChrW(&H201D) & " ") Then
                    .MoveEnd(WdUnits.wdCharacter, -2)
                End If

                .Bookmarks.Add("zzmpTCEntryTemp")
                bmkHeading = CurWordApp.ActiveDocument.Bookmarks("zzmpTCEntryTemp")
                xHeadingText = .Text
            End With

            '   place slash in front of special chrs -
            '   these operations are time hogs,
            '   so do only when necessary
            With bmkHeading.Range.Find
                .ClearFormatting()
                .Wrap = WdFindWrap.wdFindStop

                If InStr(xHeadingText, Chr(34)) Then
                    .Text = "^0034"
                    If bUseQuotePlaceholders Then
                        .Replacement.Text = "zzmp34h"
                    Else
                        .Replacement.Text = "\" & Chr(34)
                    End If
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                    bReset34 = True
                End If

                If InStr(xHeadingText, ChrW(&H201C)) Then
                    .Text = ChrW(&H201C)
                    If bUseQuotePlaceholders Then
                        .Replacement.Text = "zzmp147h"
                    Else
                        .Replacement.Text = "\" & ChrW(&H201C)
                    End If
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                    bReset147 = True
                End If

                If InStr(xHeadingText, ChrW(&H201D)) Then
                    .Text = ChrW(&H201D)
                    If bUseQuotePlaceholders Then
                        .Replacement.Text = "zzmp148h"
                    Else
                        .Replacement.Text = "\" & ChrW(&H201D)
                    End If
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                    bReset148 = True
                End If
            End With

            '   insert formatted text
            With rngTCEntryTextStart
                .InsertAfter(Chr(34))

                '       insert number if list template is applied
                With rngHeading.ListFormat
                    If .ListType <> WdListType.wdListListNumOnly And
                                .ListType <> WdListType.wdListNoNumbering Then
                        '               get number
                        xCurNumber = rngHeading.ListFormat.ListString

                        '               insert number and trailing char
                        rngTCEntryTextStart.InsertAfter(xCurNumber & xNumberTrailChar)
                    End If
                End With

                '       mark position for ref field insert below
                lXRefPos = rngTCEntryTextStart.End

                .InsertAfter(Chr(34) & xListLevelSwitch)

                rngRef = CurWordApp.ActiveDocument.Range(lXRefPos, lXRefPos)
                fldRef = .Fields.Add(rngRef,
                                         WdFieldType.wdFieldRef,
                                         "zzmpTCEntryTemp",
                                         True)

            End With

            '   remove slashes if necessary
            With bmkHeading.Range
                With .Find
                    .ClearFormatting()
                    .Wrap = WdFindWrap.wdFindStop

                    If bReset34 Then
                        If bUseQuotePlaceholders Then
                            .Text = "zzmp34h"
                        Else
                            .Text = "\^0034"
                        End If
                        .Replacement.Text = Chr(34)
                        .Execute(Replace:=WdReplace.wdReplaceAll)
                    End If

                    If bReset147 Then
                        If bUseQuotePlaceholders Then
                            .Text = "zzmp147h"
                        Else
                            .Text = "\" & ChrW(&H201C)
                        End If
                        .Replacement.Text = ChrW(&H201C)
                        .Execute(Replace:=WdReplace.wdReplaceAll)
                    End If

                    If bReset148 Then
                        If bUseQuotePlaceholders Then
                            .Text = "zzmp148h"
                        Else
                            'GLOG 5619 - search text previously left out slash
                            .Text = "\" & ChrW(&H201D)
                        End If
                        .Replacement.Text = ChrW(&H201D)
                        .Execute(Replace:=WdReplace.wdReplaceAll)
                    End If
                End With
            End With

            '   delete bookmark
            On Error Resume Next
            bmkHeading.Delete()

        End Function

        Friend Shared Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
            With docDoc
                If InStr(UCase(.AttachedTemplate), "PLEAD") Or
                        InStr(UCase(.AttachedTemplate), TemplatePOS) Or
                        InStr(UCase(.AttachedTemplate), "VER") Then
                    bIsPleadingDocument = True
                Else
                    bIsPleadingDocument = False
                End If
            End With
        End Function

        Friend Shared Function iGetParagraphLevel(rngLocation As Word.Range,
                                    iDefaultLevel As Integer) As Integer
            'returns the paragraph outline level
            'of the first paragraph of rnglocation

            Dim iLevel As Integer

            '   get list type
            Select Case iGetListType(rngLocation)
                Case mpListTypes.mpListTypeNative
                    iLevel = rngLocation.ListFormat.ListLevelNumber
                Case mpListTypes.mpListTypeMixed
                    iLevel = rngLocation.ListFormat.ListLevelNumber
                Case mpListTypes.mpListTypeNoNumbering
                    If iDefaultLevel > 0 Then
                        '               set level to the default
                        iLevel = iDefaultLevel
                    Else
                        '               prompt user for level
                        iLevel = iGetTOCLevel(rngLocation)
                    End If
            End Select

            iGetParagraphLevel = iLevel

        End Function

        Friend Shared Function xNoPageNumberRange(xTOCScheme As String,
                                    iMinLevel As Integer,
                                    iMaxLevel As Integer) As String
            'returns string for use with page number switch of TOC field
            'if not contiguous, returns "NC", indicating that
            'page numbers have to be removed manually where appropriate

            Dim i As Integer
            Dim iNumBreaks As Integer
            Dim xPageNo As String = ""
            Dim xRange As String = ""
            Dim iStartRange As Integer
            Dim iEndRange As Integer

            For i = iMinLevel To iMaxLevel
                xPageNo = xGetLevelProp(xTOCScheme & "_L",
                                        i,
                                        mpTOCLevelProp.mpTOCLevelProp_IncludePgNum,
                                        mpSchemeTypes.mpSchemeType_Document)
                '       a break occurs when going from level with page no
                '       to level w/o page no or vice versa; if more than
                '       one break, range isn't contiguous
                If xPageNo = "0" Then
                    If iEndRange > 0 And iEndRange < (i - 1) Then
                        '               there's a previous range w/o page numbers
                        iNumBreaks = iNumBreaks + 1
                    End If
                    If iStartRange = 0 Then _
                        iStartRange = i
                    iEndRange = i
                Else
                    If iEndRange > 0 And iEndRange = (i - 1) Then
                        '               the previous level had no page number
                        iNumBreaks = iNumBreaks + 1
                    End If
                End If
            Next i

            If iNumBreaks > 1 Then
                xRange = "NC"
            ElseIf iStartRange > 0 Then
                xRange = Trim(Str(iStartRange))
                If iEndRange > iStartRange Then _
                    xRange = xRange & "-" & Trim(Str(iEndRange))
            End If

            xNoPageNumberRange = xRange
        End Function

        Friend Shared Function bMarkAll() As Boolean
            'marks selected paragraphs as specified
            'by user - ignores currently marked paras
            Dim i As Integer
            Dim iLevel As Integer
            Dim bShowAllStart As Boolean
            Dim rngP As Word.Range
            Dim rngStart As Word.Range
            Dim xScheme As String = ""
            Dim iNumSchemes As Integer
            Dim xDelimiter As String
            Dim oForm As Form
            Dim iMarkAction As mpMarkActions
            Dim iFormatAction As mpFormatActions
            Dim iType As mpMarkingModes
            Dim iScope As mpMarkFormatScopes
            Dim xTargetStyles As String = ""
            Dim xKey As String = ""
            Dim xStyle As String = ""
            Dim iHeadingFormat As Integer
            Dim oDlg As frmMarkAll
            Dim oDlgFrench As frmMarkAllFrench
            Dim bMark As Boolean
            Dim bUnmark As Boolean
            Dim bFormat As Boolean
            Dim bUnformat As Boolean
            Dim xScope As String
            Dim xDefinition As String
            Dim iScheme As Integer
            Dim bAllSchemes As Boolean
            Dim bLevelChecked As Boolean

            Try
                bMarkAll = False
                'get document schemes
                iNumSchemes = iGetSchemes(g_xDSchemes,
                                          mpSchemeTypes.mpSchemeType_Document)

                '   if no schemes, don't display dialog
                If iNumSchemes = 0 Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucun th�me de num�rotation TSG dans ce document.", vbInformation, "Num�rotation TSG")
                    Else
                        MsgBox("There are no TSG numbering schemes " &
                            "in this document.", vbInformation, "TSG Numbering")
                    End If
                    Exit Function
                End If

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    oForm = New frmMarkAllFrench
                    oDlgFrench = CType(oForm, frmMarkAllFrench)
                Else
                    oForm = New frmMarkAll
                    oDlg = CType(oForm, frmMarkAll)
                End If

                With oForm
                    .ShowDialog()

                    'exit if cancelled
                    If .DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                        oForm.Close()
                        CurWordApp.Activate()
                        CurWordApp.ActiveWindow.SetFocus()
                        Exit Function
                    End If

                    'get values
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        With oDlgFrench
                            bMark = .chkMarkTC.Checked
                            bUnmark = .chkUnmark.Checked
                            bFormat = .chkFormat.Checked
                            bUnformat = .chkUnformat.Checked
                            xScope = .cbxScope.Text
                            xDefinition = .txtDefinition.Text
                            iScheme = .cbxScheme.SelectedIndex
                            bAllSchemes = .optAllSchemes.Checked
                        End With
                    Else
                        With oDlg
                            bMark = .chkMarkTC.Checked
                            bUnmark = .chkUnmark.Checked
                            bFormat = .chkFormat.Checked
                            bUnformat = .chkUnformat.Checked
                            xScope = .cbxScope.Text
                            xDefinition = .txtDefinition.Text
                            iScheme = .cbxScheme.SelectedIndex
                            bAllSchemes = .optAllSchemes.Checked
                        End With
                    End If

                    'get current selection for later selection
                    rngStart = CurWordApp.Selection.Range
                    rngP = CurWordApp.Selection.Range

                    CurWordApp.ScreenUpdating = False

                    With CurWordApp.ActiveDocument.ActiveWindow.View
                        bShowAllStart = .ShowAll
                        .ShowAll = True
                    End With

                    'set actions
                    If bMark Then
                        iMarkAction = mpMarkActions.mpMarkAction_Mark
                    ElseIf bUnmark Then
                        iMarkAction = mpMarkActions.mpMarkAction_Unmark
                    Else
                        iMarkAction = mpMarkActions.mpMarkAction_None
                    End If
                    If bFormat Then
                        iFormatAction = mpFormatActions.mpFormatAction_Format
                    ElseIf bUnformat Then
                        iFormatAction = mpFormatActions.mpFormatAction_Unformat
                    Else
                        iFormatAction = mpFormatActions.mpFormatAction_None
                    End If

                    'set type
                    If g_bMarkWithStyleSeparators Then
                        iType = mpMarkingModes.mpMarkingMode_StyleSeparators
                    Else
                        iType = mpMarkingModes.mpMarkingMode_TCCodes
                    End If

                    'set scope
                    Select Case xScope
                        Case "Current Document", "Document en cours"
                            iScope = mpMarkFormatScopes.mpMarkFormatScope_Document
                        Case "Current Section", "Section en cours"
                            iScope = mpMarkFormatScopes.mpMarkFormatScope_Section
                        Case Else
                            iScope = mpMarkFormatScopes.mpMarkFormatScope_Selection
                    End Select

                    'set heading definition - spaces are displayed in dlg as bullets
                    xDelimiter = xSubstitute(xDefinition, ChrW(&HB7), Chr(32))
                    xDelimiter = xSubstitute(xDelimiter, Chr(183), Chr(32))

                    'set styles and formatting options for selected levels
                    For iLevel = 1 To 9
                        'styles
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            bLevelChecked = (oDlgFrench.Levels(iLevel - 1).CheckState = CheckState.Checked)
                        Else
                            bLevelChecked = (oDlg.Levels(iLevel - 1).CheckState = CheckState.Checked)
                        End If
                        If bLevelChecked Then
                            xScheme = g_xDSchemes(iScheme, 0)
                            If bLevelExists(xScheme, iLevel, mpSchemeTypes.mpSchemeType_Document) Then _
                                xTargetStyles = xTargetStyles & xGetStyleName(xScheme, iLevel) & "|"

                            If bAllSchemes Then
                                For i = 0 To UBound(g_xDSchemes)
                                    If i <> iScheme Then
                                        xScheme = g_xDSchemes(i, 0)
                                        If bLevelExists(xScheme, iLevel, mpSchemeTypes.mpSchemeType_Document) Then _
                                            xTargetStyles = xTargetStyles & xGetStyleName(xScheme, iLevel) & "|"
                                    End If
                                Next i
                            End If
                        End If
                    Next iLevel

                    'apply formatting changes
                    If iFormatAction = mpFormatActions.mpFormatAction_Format Then
                        Dim oFormattingChanges As Hashtable
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            oFormattingChanges = oDlgFrench.FormattingChanges
                        Else
                            oFormattingChanges = oDlg.FormattingChanges
                        End If
                        For i = 1 To oFormattingChanges.Count
                            xKey = oFormattingChanges.Keys(i - 1)
                            xScheme = Strings.Left$(xKey, Len(xKey) - 1)
                            iLevel = CInt(Strings.Right(xKey, 1))
                            iHeadingFormat = CInt(oFormattingChanges(xKey))
                            lSetLevelProp(xScheme, iLevel, mpNumLevelProps.mpNumLevelProp_HeadingFormat,
                                CStr(iHeadingFormat), mpSchemeTypes.mpSchemeType_Document)
                        Next i
                    End If

                    'mark/format headings
                    bMarkAndFormatHeadings(xTargetStyles, iMarkAction, iFormatAction, iType,
                        xDelimiter, iScope, True, mpMarkingReplacementModes.mpMarkingReplacementMode_Prompt)
                End With

                bMarkAll = True

            Finally
                '   clean up
                If Not oForm Is Nothing Then
                    oForm.Close()
                End If
                If Not rngStart Is Nothing Then
                    rngStart.Select()
                End If
                CurWordApp.ActiveDocument.ActiveWindow.View.ShowAll = bShowAllStart
                EchoOn()
                CurWordApp.ScreenUpdating = True
                CurWordApp.StatusBar = ""
                CurWordApp.Activate()
                CurWordApp.ActiveWindow.SetFocus()
            End Try
        End Function

        Friend Shared Function EditCustomTOCScheme() As Boolean
            'displays TOC customization form.
            'if xScheme is "custom", sets defaults in ini;
            'otherwise, modifies document scheme.
            Dim oForm As Form
            Dim oDlg As frmEditTOC
            Dim oDlgFrench As frmEditTOCFrench
            EditCustomTOCScheme = False
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                oForm = New frmEditTOCFrench
                oDlgFrench = CType(oForm, frmEditTOCFrench)
                oDlgFrench.EditCustomScheme = True
            Else
                oForm = New frmEditTOC
                oDlg = CType(oForm, frmEditTOC)
                oDlg.EditCustomScheme = True
            End If

            oForm.ShowDialog()

            If oForm.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                oForm.Close()
                CurWordApp.Activate()
                CurWordApp.ActiveWindow.SetFocus()
                Exit Function
            End If

            'set defaults
            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                With oDlgFrench
                    SetUserSetting("TOC", "LineSpacing",
                        .cmbLineSpacing.SelectedIndex)
                    SetUserSetting("TOC", "PointsBefore",
                        .spnBefore.Value)
                    SetUserSetting("TOC", "PointsBetween",
                        .spnBetween.Value)
                    SetUserSetting("TOC", "PageNumbers",
                        .cmbPageNums.SelectedIndex)
                    SetUserSetting("TOC", "DotLeaders",
                        .cmbDotLeaders.SelectedIndex)
                    SetUserSetting("TOC", "AllCaps",
                        .cmbAllCaps.SelectedIndex)
                    SetUserSetting("TOC", "Bold",
                        .cmbBold.SelectedIndex)
                    SetUserSetting("TOC", "CenterLevel1",
                        .chkCenterLevel1.CheckState)
                    SetUserSetting("TOC", "DefaultToCustom",
                        .chkDefaultScheme.CheckState)
                End With
            Else
                With oDlg
                    SetUserSetting("TOC", "LineSpacing",
                        .cmbLineSpacing.SelectedIndex)
                    SetUserSetting("TOC", "PointsBefore",
                        .spnBefore.Value)
                    SetUserSetting("TOC", "PointsBetween",
                        .spnBetween.Value)
                    SetUserSetting("TOC", "PageNumbers",
                        .cmbPageNums.SelectedIndex)
                    SetUserSetting("TOC", "DotLeaders",
                        .cmbDotLeaders.SelectedIndex)
                    SetUserSetting("TOC", "AllCaps",
                        .cmbAllCaps.SelectedIndex)
                    SetUserSetting("TOC", "Bold",
                        .cmbBold.SelectedIndex)
                    SetUserSetting("TOC", "CenterLevel1",
                        .chkCenterLevel1.CheckState)
                    SetUserSetting("TOC", "DefaultToCustom",
                        .chkDefaultScheme.CheckState)
                End With
            End If

            EditCustomTOCScheme = True
            oForm.Close()
            CurWordApp.ScreenUpdating = True
            CurWordApp.Activate()
            CurWordApp.ActiveWindow.SetFocus()
        End Function

        Friend Shared Function bCustomizeGenericStyles(udtFormat As TOCFormat,
                                         bIncludeSchedule As Boolean) As Boolean
            'redefines TOC 1-9 based on values in user ini

            Dim i As Integer
            bCustomizeGenericStyles = False
            '   redefine styles
            For i = 1 To 9
                If (i = 9) And bIncludeSchedule Then Exit Function
                With CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(i))
                    With .ParagraphFormat
                        If udtFormat.LineSpacing <> mpCGSSkipItem Then _
                            .LineSpacingRule = udtFormat.LineSpacing
                        If udtFormat.SpaceBetween <> mpCGSSkipItem Then _
                            .SpaceAfter = CSng(udtFormat.SpaceBetween)
                        .TabStops(1).Leader = Abs(CInt(udtFormat.IncludeDotLeaders(i - 1)))
                    End With

                    '            If iAllCaps <> mpCGSSkipItem Then
                    '                .Font.AllCaps = udtFormat.AllCaps(i)
                    '            End If
                End With
            Next i
            bCustomizeGenericStyles = True
        End Function

        Friend Shared Sub TagTOCEntries(iStartLevel As Integer,
                          iEndLevel As Integer,
                          iEntryType As mpTOCEntryTypes,
                          bTagAllOutlineParas As Boolean,
                          xStyleList As String,
                          bApplyTOC9 As Boolean,
                          xTOC9Style As String,
                          bDesignate As Boolean)
            'tags document para entries in prep for TOC - will tag
            'all outline level paras if specified, else will tag
            'those outline level paras in Style List.
            Dim paraP As Word.Paragraph
            Dim rngPara As Word.Range
            Dim bIsMarked As Boolean
            Dim bIsMarkedDyn As Boolean
            Dim bIsMarkedPDyn As Boolean
            Dim bIsMarkedStatic As Boolean
            Dim lPos As Long
            Dim l As Long
            Dim lParas As Long
            Dim iLevel As Integer
            Dim rngCode As Word.Range
            Dim bIncludeTCEntries As Boolean
            Dim xStyle As String = ""
            Dim xScheme As String = ""
            Dim bKeepDynamic As Boolean
            Dim bIsMPLT As Boolean
            Dim xStyTest As String = ""
            Dim iDisplay As Integer
            Dim lShowTags As Long

            '   set progress indicator
            g_oStatus.UpdateProgress(3)

            'show xml tags if necessary
            If g_bXMLSupport Then
                lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, True)
            End If

            lParas = CurWordApp.ActiveDocument.Paragraphs.Count

            '   cycle through all paras
            For Each paraP In CurWordApp.ActiveDocument.Paragraphs
                bIsMarked = False
                bIsMarkedStatic = False
                bIsMarkedDyn = False
                bIsMarkedPDyn = False
                bKeepDynamic = False
                xStyle = ""
                xScheme = ""

                rngPara = paraP.Range
                If ((rngPara.ListFormat.ListType = WdListType.wdListNoNumbering) And
                        (rngPara.Text = vbCr)) Or (rngPara.Text = Chr(12)) Then
                    GoTo NextPara
                End If

                '       check for existing tc code
                lPos = InStr(rngPara.Text, Chr(19) & " " & g_xTCPrefix & " ")
                If lPos Then
                    '           it's marked - get type of mark
                    rngCode = rngPara.Duplicate
                    rngCode.MoveStart(WdUnits.wdCharacter, lPos)
                    rngCode = rngCode.Fields(1).Code
                    bIsMarked = True

                    '           static mark will have 'tc "'
                    bIsMarkedStatic = InStr(UCase(rngCode.Text), UCase(" " & g_xTCPrefix & " " & Chr(34)))

                    If Not bIsMarkedStatic Then
                        '               partial dynamic markings have level but no text
                        bIsMarkedPDyn = InStr(UCase(rngCode.Text), UCase(" " & g_xTCPrefix & " \l """))
                        If Not bIsMarkedPDyn Then
                            '                   fully dynamic markings don't have either level or text;
                            '                   determine whether TC code should actually be fully
                            '                   dynamic, i.e. it may be from Numbering 8.0
                            With rngCode
                                If .ParagraphFormat.OutlineLevel <> WdOutlineLevel.wdOutlineLevelBodyText Then
                                    On Error Resume Next 'GLOG 2847 - moved up a line
                                    xStyle = .Style.NameLocal 'GLOG 8715 (dm)
                                    xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
                                    On Error GoTo 0
                                    If xScheme <> "" Then
                                        bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
                                    End If
                                End If
                            End With
                            If xStyle = "" Then
                                bKeepDynamic = False
                            ElseIf bIsMPLT Then
                                bKeepDynamic = True
                            ElseIf bIsHeadingStyle(xStyle) Then
                                bKeepDynamic = True
                            ElseIf xConditionalScheme(rngCode) <> "" Then
                                bKeepDynamic = True
                            End If

                            If bKeepDynamic Then
                                '                       include only specified levels
                                bIsMarkedDyn = (paraP.OutlineLevel >= iStartLevel) And
                                    (paraP.OutlineLevel <= iEndLevel)
                            Else
                                '                       this should be partially dynamic - assume level one
                                rngCode.InsertAfter("\l " & Chr(34) & "1" & Chr(34) & " ")
                            End If
                        End If
                    End If
                Else
                    bIsMarked = False
                End If

                '       marked schedule styles should be tagged as TC entries;
                '       bReworkTOC will delete duplicates
                If bIsMarked And bApplyTOC9 And (xTOC9Style <> "") Then
                    '           note - rngPara.Style can be Nothing in Word XP; in this case,
                    '           rngPara.Paragraphs(1).Style will usually yield a value; this
                    '           alternative needs to be error trapped as well, however, because
                    '           a single paragraph may contain multiple styles in some field results,
                    '           e.g. native TOA in Word XP (log item 3185)
                    On Error Resume Next
                    xStyTest = rngPara.Style
                    If xStyTest = "" Then _
                        xStyTest = rngPara.Paragraphs(1).Style
                    On Error GoTo 0
                    If xStyTest = xTOC9Style Then
                        bIsMarkedDyn = (paraP.OutlineLevel >= iStartLevel) And
                            (paraP.OutlineLevel <= iEndLevel)
                    End If
                    xStyTest = ""
                End If

                '       odd entries mean include tc entries
                bIncludeTCEntries = (iEntryType Mod 2 = 1)

                If (iEntryType = mpTOCEntryTypes.mpTOCEntryType_TCEntries) Then
                    If bIsMarkedDyn Then
                        TagAsTCEntry(rngPara, lPos, xTOC9Style, bDesignate)
                    ElseIf paraP.OutlineLevel <> WdOutlineLevel.wdOutlineLevelBodyText Then
                        TagToDelete(paraP)
                    End If
                Else
                    '           tag for sentence or para if para is:
                    '           1)not marked, 2) marked static
                    '           3)marked dynamic and tc entries are
                    '           not being included the toc
                    If bIsMarkedDyn And bIncludeTCEntries Then
                        TagAsTCEntry(rngPara, lPos, xTOC9Style, bDesignate)
                    ElseIf paraP.OutlineLevel <> WdOutlineLevel.wdOutlineLevelBodyText Then
                        If (bIsMarkedStatic Or bIsMarkedPDyn) And bIncludeTCEntries Then
                            '                   this will prevent duplicates
                            TagToDelete(paraP)
                        Else
                            '                   tag for either first sentence
                            '                   or whole para - ignore any tc codes
                            TagAsStyle(paraP,
                                       iEntryType,
                                       bTagAllOutlineParas,
                                       xStyleList,
                                       iStartLevel,
                                       iEndLevel,
                                       bApplyTOC9,
                                       xTOC9Style,
                                       bDesignate)
                        End If
                    End If
                End If

                '       tag hard page break immediately preceding paragraph mark;
                '       this leads to extra TOC entries which need to be removed
                With rngPara
                    lPos = InStr(.Text, Chr(12) & mpEndHeading)
                    If lPos = 0 Then _
                        lPos = InStr(.Text, Chr(12) & mpEndPara)
                    If lPos = 0 Then _
                        lPos = InStr(.Text, mpStartPara & Chr(12))
                    If lPos Then
                        .MoveEndUntil(Chr(12), WdConstants.wdBackward)
                        .InsertAfter(mpPageBreakMarker)
                    End If
                End With

                '       free up resources
                If Not g_bPreserveUndoListTOC Then _
                    CurWordApp.ActiveDocument.UndoClear()
NextPara:
                '       update status - % complete
                l = l + 1
                If ((l / lParas) * 100) > (iDisplay + 10) Then
                    iDisplay = iDisplay + 10
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Pr�paration du document pour TM: " & iDisplay & "%"
                    Else
                        CurWordApp.StatusBar = "Preparing document for TOC: " & iDisplay & "%"
                    End If
                End If
                paraP = Nothing
            Next paraP

            'rehide xml tags if necessary
            If lShowTags Then
                lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, False)
            End If

            CurWordApp.StatusBar = ""
        End Sub

        Private Shared Sub TagAsTCEntry(rngPara As Word.Range,
                                      lPos As Long,
                                      xTOC9Style As String,
                                      bDesignate As Boolean)
            Dim xScheme As String = ""
            Dim rngP As Word.Range
            Dim lPos2 As Long
            Dim bIsMPLT As Boolean
            Dim xStyle As String = ""
            Dim oStyle As Word.Style
            Dim lStart As Long
            Dim lEnd As Long
            Dim iTags As Integer
            Dim lCCSafeStart As Long

            With rngPara
                '       para is marked with tc code-
                xScheme = ""

                '       strip alias from style name
                oStyle = .Style

                'GLOG 2847
                If oStyle Is Nothing Then _
                    Exit Sub

                xStyle = StripStyleAlias(oStyle.NameLocal)

                '       scheme is either "Heading" or name of list template
                If bDesignate Then
                    '           if designating individually, use style name sans alias
                    xScheme = xStyle
                ElseIf bIsHeadingStyle(xStyle) Then
                    xScheme = "Heading"
                ElseIf xStyle = xTOC9Style Then
                    xScheme = xTOC9Style
                Else
                    On Error Resume Next
                    xScheme = xGetLTRoot(.ListFormat.ListTemplate.Name)
                    On Error GoTo 0

                    If xScheme <> "" Then
                        bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
                    End If

                    If xScheme = "" Then
                        '               check if it's a conditional paragraph
                        xScheme = xConditionalScheme(rngPara)
                    ElseIf Not bIsMPLT Then
                        '               don't label non-MacPac scheme
                        xScheme = ""
                    End If
                End If

                'hide tags if necessary
                If g_bXMLSupport Then
                    SetXMLMarkupState(CurWordApp.ActiveDocument, False)
                End If

                'GLOG 5075 - content control at start of paragraph was
                'causing entry to get mistagged - not an issue with tags
                lCCSafeStart = GetCCSafeParagraphStart(rngPara)

                '       add 'tc marked' tag directly before the tc code
                rngP = .Duplicate
                With rngP
                    If lCCSafeStart > 0 Then 'GLOG 5075
                        .SetRange(lCCSafeStart, lCCSafeStart)
                    Else
                        .StartOf()
                    End If
                    .Move(WdUnits.wdCharacter, lPos - 1)
                    .InsertAfter(mpTCMarked)
                    .StartOf(WdUnits.wdParagraph)
                End With

                '       lPos2 is closing brace of tc code (chr 21)
                lPos2 = InStr(lPos + Len(mpStartPara), .Text, Chr(21))

                '       add EndHeading tag after tc code -
                '       include 'level' tag if level was
                '       present in TOC code - if not present
                '       we'll get the outline level later.
                rngP = .Duplicate
                With rngP
                    If lCCSafeStart > 0 Then 'GLOG 5075
                        .SetRange(lCCSafeStart, lCCSafeStart)
                    Else
                        .StartOf()
                    End If
                    .Move(WdUnits.wdCharacter, lPos2)
                    .InsertAfter(mpEndHeading &
                            mpSchemeNameStart &
                            xScheme &
                            mpSchemeNameEnd)
                End With

                '       add EndPara tag before para mark
                rngP = .Duplicate
                With rngP
                    'get xml tag/cc adjusted paragraph end
                    If g_bXMLSupport Then
                        SetXMLMarkupState(CurWordApp.ActiveDocument, True)
                        lEnd = GetTagSafeParagraphEnd(rngPara, iTags)
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        If iTags = 0 Then _
                            lEnd = GetCCSafeParagraphEnd(rngPara)
                        .SetRange(.Start, lEnd)
                    End If
                    .EndOf()
                    If Asc(.Previous(WdUnits.wdCharacter).Text) = 13 Then _
                        .Move(WdUnits.wdCharacter, -1)
                    .InsertAfter(mpEndPara)
                End With

                rngP = .Duplicate
                With rngP
                    'get xml tag/cc adjusted paragraph start
                    If g_bXMLSupport Then
                        lStart = GetTagSafeParagraphStart(rngPara, iTags)
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        If iTags = 0 Then _
                            lStart = lCCSafeStart
                        .SetRange(lStart, .End)
                    End If
                    If .Characters(1).Text = Chr(12) Then _
                        .MoveStart()
                    .InsertBefore(mpStartPara)
                End With
            End With
        End Sub

        Private Shared Sub TagToDelete(ByVal paraP As Word.Paragraph)
            'inserts 'deletion' tag at end of para
            Dim rngP As Word.Range
            Dim lStart As Long
            Dim lEnd As Long
            Dim iTagsAtStart As Integer
            Dim iTagsAtEnd As Integer

            rngP = paraP.Range

            With rngP
                'get xml tag/cc adjusted paragraph start
                If g_bXMLSupport Then
                    lStart = GetTagSafeParagraphStart(rngP, iTagsAtStart)
                    lEnd = GetTagSafeParagraphEnd(rngP, iTagsAtEnd)
                    If iTagsAtStart + iTagsAtEnd = 0 Then
                        '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                        lStart = GetCCSafeParagraphStart(rngP)
                        lEnd = GetCCSafeParagraphEnd(rngP)
                    End If
                    .SetRange(lStart, lEnd)
                End If

                '       marking start of para is necessary to distinguish second part of
                '       entry split by Word 97 page break bug - see bReworkTOC
                If .Characters(1).Text = Chr(12) Then _
                    .MoveStart()
                .InsertBefore(mpStartPara)

                .EndOf()
                If Asc(.Previous(WdUnits.wdCharacter).Text) = 13 Then _
                    .Move(WdUnits.wdCharacter, -1)
                .InsertAfter(mpEndHeading &
                        mpSchemeNameStart &
                        "zzmpDel" &
                        mpSchemeNameEnd &
                        mpEndPara)
            End With
        End Sub

        Friend Shared Sub TagAsStyle(paraP As Word.Paragraph,
                              ByVal iEntryType As mpTOCEntryTypes,
                              ByVal bTagAllOutlineParas As Boolean,
                              ByVal xStyleList As String,
                              ByVal iStartLevel As Integer,
                              ByVal iEndLevel As Integer,
                              ByVal bApplyTOC9 As Boolean,
                              ByVal xTOC9Style As String,
                              ByVal bDesignate As Boolean)
            'tags paraP for either a sentence or paragraph
            Dim rngPara As Word.Range
            Dim rngP As Word.Range
            Dim bIsIncluded As Boolean
            Dim xScheme As String = ""
            Dim rngSentence As Word.Range
            Dim iStyleType As mpTOCEntryTypes
            Dim lPos As Long
            Dim lPos2 As Long
            Dim lPos3 As Long
            Dim iPos As Integer
            Dim xStyle As String = ""
            Dim i As Integer
            Dim xTest As String = ""
            Dim bIsMPLT As Boolean
            Dim bIsQuote As Boolean
            Dim oStyle As Word.Style
            Dim lStart As Long
            Dim lEnd As Long
            Dim iCount As Integer
            Dim iTagsAtStart As Integer
            Dim iTagsAtEnd As Integer

            rngPara = paraP.Range

            With rngPara
                '       first strip alias from heading style name
                oStyle = paraP.Style

                'GLOG 2847
                If oStyle Is Nothing Then _
                    Exit Sub

                xStyle = StripStyleAlias(oStyle.NameLocal)

                If bTagAllOutlineParas Then
                    '           tag if the outline level of the para
                    '           is in the level range of the TOC
                    bIsIncluded = bDesignate Or
                                  ((paraP.OutlineLevel <= iEndLevel) And
                                  (paraP.OutlineLevel >= iStartLevel))
                Else
                    '           tag para if para style is in style list
                    bIsIncluded = InStr(xStyleList, xStyle)
                End If

                If bIsIncluded Then
                    '           get style type by subtracting value for tc entries
                    '           if they are being included in toc
                    If iEntryType Mod 2 Then
                        iStyleType = iEntryType - mpTOCEntryTypes.mpTOCEntryType_TCEntries
                    Else
                        iStyleType = iEntryType
                    End If

                    '           scheme is either "Heading" or name of list template
                    If bDesignate Then
                        '               if designating individually, use style name sans alias
                        xScheme = xStyle
                    ElseIf bIsHeadingStyle(xStyle) Then
                        xScheme = "Heading"
                    ElseIf bApplyTOC9 And (xStyle = xTOC9Style) Then
                        xScheme = xTOC9Style
                        '            ElseIf bIsScheduleStyle(xStyle) Then
                        '                xScheme = "Schedule"
                    Else
                        On Error Resume Next
                        xScheme = xGetLTRoot(rngPara.ListFormat.ListTemplate.Name)
                        On Error GoTo 0

                        If xScheme <> "" Then
                            bIsMPLT = bIsMPListTemplate(.ListFormat.ListTemplate)
                        End If

                        If xScheme = "" Then
                            '                   check if it's a conditional paragraph
                            xScheme = xConditionalScheme(rngPara)

                            'GLOG 4922 - if unlinked MacPac style, infer scheme name from style
                            If xScheme = "" Then
                                lPos = InStr(xStyle, "_L")
                                If (lPos > 0) And (lPos = Len(xStyle) - 2) Then
                                    xScheme = "zzmp" & Strings.Left$(xStyle, lPos - 1)
                                End If
                            End If
                        ElseIf Not bIsMPLT Then
                            '                   don't label non-MacPac scheme
                            xScheme = ""
                        End If

                        '               "other" outline levels option
                        '                If xScheme = "" Then _
                        '                    xScheme = "Other"
                    End If

                    'get xml tag/CC adjusted paragraph start and end
                    If g_bXMLSupport Then
                        lStart = GetTagSafeParagraphStart(rngPara, iTagsAtStart)
                        lEnd = GetTagSafeParagraphEnd(rngPara, iTagsAtEnd)
                        If iTagsAtStart + iTagsAtEnd = 0 Then
                            '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                            lStart = GetCCSafeParagraphStart(rngPara)
                            lEnd = GetCCSafeParagraphEnd(rngPara)
                        End If
                    Else
                        lStart = .Start
                        lEnd = .End
                    End If

                    '           if type is Sentence,
                    If iStyleType = mpTOCEntryTypes.mpTOCEntryType_Sentence Or
                            iStyleType = mpTOCEntryTypes.mpTOCEntryType_PeriodSpace Then
                        '               add EndHeading tag after first sentence -
                        '               follow this immediately with the name
                        '               of the applied scheme
                        bIsQuote = False
                        rngSentence = rngPara.Duplicate
                        With rngSentence
                            .SetRange(lStart, lEnd)
                            If .Characters(1).Text = Chr(12) Then _
                                .MoveStart()
                            .InsertBefore(mpStartPara)
                        End With

                        '               look for Chinese full stop
                        lPos = InStr(.Text, ChrW(&H3002))

                        If lPos Then
                            '                   there's a Chinese full stop
                            With rngSentence
                                .StartOf()
                                .MoveEnd(WdUnits.wdCharacter, lPos)
                                If g_bXMLSupport Then
                                    'account for empty xml tags (12/17/09)
                                    '9.9.4009 - account for all tags ending in range
                                    iCount = CountTagsEndingInRange(rngSentence)
                                    If iCount > 0 Then
                                        .MoveEnd(WdUnits.wdCharacter, iCount)
                                    Else
                                        .MoveEnd(WdUnits.wdCharacter,
                                            CountCCsEndingInRange(rngSentence))
                                    End If
                                End If
                            End With
                        ElseIf iStyleType = mpTOCEntryTypes.mpTOCEntryType_Sentence Then
                            '                   period and 2 spaces
                            lPos = InStr(.Text, mpSentencePeriodTwo)

                            '                   try with end quote
                            lPos2 = InStr(.Text, mpSentenceQuoteTwo)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsQuote = True
                            End If

                            '                   try with smart quote
                            lPos2 = InStr(.Text, g_xSmartTwo)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsQuote = True
                            End If

                            If lPos Then
                                With rngSentence
                                    .StartOf()
                                    .MoveEnd(WdUnits.wdCharacter, lPos)
                                    If g_bXMLSupport Then
                                        'account for empty xml tags (12/17/09)
                                        '9.9.4009 - account for all tags ending in range
                                        iCount = CountTagsEndingInRange(rngSentence)
                                        If iCount > 0 Then
                                            .MoveEnd(WdUnits.wdCharacter, iCount)
                                        Else
                                            .MoveEnd(WdUnits.wdCharacter,
                                                CountCCsEndingInRange(rngSentence))
                                        End If
                                    End If
                                End With
                            End If
                        Else
                            '                   period and 1 space
                            lPos = InStr(.Text, mpSentencePeriodOne)

                            '                   try with end quote
                            lPos2 = InStr(.Text, mpSentenceQuoteOne)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsQuote = True
                            End If

                            '                   try with smart quote
                            lPos2 = InStr(.Text, g_xSmartOne)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsQuote = True
                            End If

                            While lPos
                                With rngSentence
                                    .StartOf()
                                    .MoveEnd(WdUnits.wdCharacter, lPos)
                                    If g_bXMLSupport Then
                                        'account for empty xml tags (12/17/09)
                                        '9.9.4009 - account for all tags ending in range
                                        iCount = CountTagsEndingInRange(rngSentence)
                                        If iCount > 0 Then
                                            .MoveEnd(WdUnits.wdCharacter, iCount)
                                        Else
                                            .MoveEnd(WdUnits.wdCharacter,
                                                CountCCsEndingInRange(rngSentence))
                                        End If
                                    End If
                                    xTest = .Text
                                End With

                                '                       skip common abbreviations
                                If bIsAbbreviation(xTest) Then
                                    bIsQuote = False
                                    rngSentence.Expand(WdUnits.wdParagraph)
                                    lPos2 = InStr(CInt(lPos + 1), .Text, mpSentencePeriodOne)

                                    '                           try with end quote
                                    lPos3 = InStr(CInt(lPos + 1), .Text, mpSentenceQuoteOne)
                                    If (lPos3 <> 0) And
                                            ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                        lPos2 = lPos3
                                        bIsQuote = True
                                    End If

                                    '                           try with smart quote
                                    lPos3 = InStr(CInt(lPos + 1), .Text, g_xSmartOne)
                                    If (lPos3 <> 0) And
                                            ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                        lPos2 = lPos3
                                        bIsQuote = True
                                    End If

                                    lPos = lPos2
                                Else
                                    lPos = 0
                                End If
                            End While
                        End If

                        With rngSentence
                            If Strings.Right(.Text, 1) = " " Then
                                '                       trim trailing spaces off range
                                .MoveEndWhile(" ", -10000)
                            ElseIf Strings.Right(.Text, 1) = vbCr Then
                                '                       trim trailing para off range
                                .MoveEnd(WdUnits.wdCharacter, -1)
                            End If

                            If Strings.Right(.Text, 1) = "." Or
                                    Strings.Right(.Text, 1) = ChrW(&H3002) Then
                                If bIsQuote Then
                                    '                           include quote
                                    .MoveEnd(WdUnits.wdCharacter, 1)
                                Else
                                    '                           trim period or full stop
                                    .MoveEnd(WdUnits.wdCharacter, -1)
                                End If
                            End If

                            .InsertAfter(mpEndHeading &
                                        mpSchemeNameStart &
                                        xScheme &
                                        mpSchemeNameEnd)
                        End With

                        '               add end para tag before para mark
                        rngP = rngPara.Duplicate
                        With rngP
                            If g_bXMLSupport Then
                                lEnd = GetTagSafeParagraphEnd(rngP, iTagsAtEnd)
                                '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                                If iTagsAtEnd = 0 Then _
                                    lEnd = GetCCSafeParagraphEnd(rngP)
                                .SetRange(rngP.Start, lEnd)
                            Else
                                If Strings.Right(.Text, 1) = Chr(7) Then
                                    '                           para is in table
                                    .EndOf(WdUnits.wdCell)
                                Else
                                    .EndOf()
                                End If
                                If (.Previous(WdUnits.wdCharacter).Text = vbCr) Or
                                        (.Previous(WdUnits.wdCharacter).Text = Chr(12)) Then
                                    .Move(WdUnits.wdCharacter, -1)
                                End If
                            End If
                            .InsertAfter(mpEndPara)
                        End With
                    Else
                        '               add EndHeading tag before para mark -
                        '               follow this immediately with the
                        '               name of the applied list template
                        rngP = rngPara.Duplicate
                        rngP.SetRange(lStart, lEnd)
                        rngP.InsertBefore(mpStartPara)
                        rngP.EndOf()
                        If (rngP.Previous(WdUnits.wdCharacter).Text = vbCr) Or
                                (rngP.Previous(WdUnits.wdCharacter).Text = Chr(12)) Then
                            rngP.Move(WdUnits.wdCharacter, -1)
                        End If

                        If Strings.Right(.Text, 1) = "." Then
                            '                   trim period
                            .MoveEnd(WdUnits.wdCharacter, -1)
                        End If

                        rngP.InsertAfter(mpEndHeading &
                            mpSchemeNameStart &
                            xScheme &
                            mpSchemeNameEnd &
                            mpEndPara)
                    End If
                End If
            End With
        End Sub

        Friend Shared Sub ReplaceText(ByVal rngP As Word.Range,
                               ByVal xSearch As String,
                               Optional ByVal xReplace As String = "",
                               Optional ByVal bMatchWildcards As Boolean = False,
                               Optional ByVal iWrap As Integer = WdFindWrap.wdFindStop)
            'replaces xSearch with xReplace
            With rngP.Find
                .ClearFormatting()
                .Replacement.ClearFormatting()
                .Text = xSearch
                .MatchWildcards = bMatchWildcards
                .Wrap = iWrap
                .Replacement.Text = xReplace
                .Execute(Replace:=WdReplace.wdReplaceAll)
                .Replacement.Text = ""
            End With

        End Sub

        Friend Shared Sub HideUnhideText(ByVal rngP As Word.Range,
                                  ByVal xSearch As String,
                                  ByVal bHide As Boolean,
                                  Optional ByVal bMatchWildcards As Boolean = False,
                                  Optional ByVal iWrap As Integer = WdFindWrap.wdFindStop)
            'replaces xSearch with xReplace

            'in Word 2007, a toggle is necessary to hide/unhide
            With rngP.Find
                .ClearFormatting()
                .Text = xSearch
                .MatchWildcards = bMatchWildcards
                .Format = True
                .Wrap = iWrap
                .Replacement.Font.Hidden = Not bHide
                .Execute(Replace:=WdReplace.wdReplaceAll)
            End With

            With rngP.Find
                .ClearFormatting()
                .Text = xSearch
                .MatchWildcards = bMatchWildcards
                .Format = True
                .Wrap = iWrap
                .Replacement.Font.Hidden = bHide
                .Execute(Replace:=WdReplace.wdReplaceAll)
            End With

        End Sub

        Private Shared Sub CleanUpDoc()
            '   remove all tags in doc and
            '   reset tc codes if specified
            Dim bShowAll As Boolean
            Dim bShowHidden As Boolean
            Dim rngContent As Word.Range
            Dim xPrompt As String

            Try
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xPrompt = "Nettoyage du document"
                Else
                    xPrompt = "Cleaning up document"
                End If
                CurWordApp.StatusBar = xPrompt
                g_oStatus.UpdateProgress(67)

                EchoOff()

                rngContent = CurWordApp.ActiveDocument.Content

                Try
                    ReplaceText(rngContent,
                                mpSchemeNameStart & "*" & mpSchemeNameEnd,
                                , True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 33%"
                g_oStatus.UpdateProgress(77)
                Try
                    ReplaceText(rngContent,
                                "|MP??|", , True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 67%"
                g_oStatus.UpdateProgress(87)
                Try
                    ReplaceText(rngContent,
                                "z11H^011", Chr(11), True)
                Catch
                End Try
            Finally
                CurWordApp.StatusBar = ""
                EchoOn()
            End Try
        End Sub

        Private Shared Sub HideTags(Optional rngP As Word.Range = Nothing,
                            Optional bShowStatus As Boolean = False)
            'set all tags in doc to hidden text-
            'this prevents any pagination issues when
            'creating the TOC
            Dim xPrompt As String

            Try
                EchoOff()

                If rngP Is Nothing Then
                    rngP = CurWordApp.ActiveDocument.Content
                End If

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xPrompt = "Activer Rechercher et Remplacer..."
                Else
                    xPrompt = "Running search and replace..."
                End If

                If bShowStatus Then
                    CurWordApp.StatusBar = xPrompt
                    g_oStatus.UpdateProgress(9)
                End If
                Try
                    HideUnhideText(rngP,
                                   mpSchemeNameStart & "*" & mpSchemeNameEnd,
                                   True, True)
                Catch
                End Try
                If bShowStatus Then
                    CurWordApp.StatusBar = xPrompt
                    g_oStatus.UpdateProgress(25)
                End If
                Try
                    HideUnhideText(rngP, "|MP??|", True, True)
                Catch
                End Try
            Finally
                EchoOn()
            End Try
        End Sub

        Private Shared Sub CleanUpTOC(ByVal rngTOC As Word.Range)
            Dim xWild As String
            Dim xPrompt As String

            Try
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xPrompt = "Nettoyage de TM"
                Else
                    xPrompt = "Cleaning up TOC"
                End If
                CurWordApp.StatusBar = xPrompt
                g_oStatus.UpdateProgress(60)
                EchoOff()

                '   localize wildcard used below -
                '   modified in 9.9.3004 - we were previously inferring the list
                '   separator from the decimal separator
                xWild = " {1" & CurWordApp.International(WdInternationalIndex.wdListSeparator) & "}"

                '   remove stray spaces after tabs that
                '   result from chr(11)s in doc
                Try
                    ReplaceText(rngTOC,
                        xWild & mpStartPara, vbTab, True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 10%"
                g_oStatus.UpdateProgress(60)
                Try
                    ReplaceText(rngTOC,
                        "^009" & xWild, vbTab, True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 20%"
                g_oStatus.UpdateProgress(60)
                Try
                    ReplaceText(rngTOC,
                        mpEndHeading & "*" & mpEndPara, , True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 30%"
                g_oStatus.UpdateProgress(62)

                '   remove any stray spaces after text in TOC Entry
                Try
                    ReplaceText(rngTOC,
                        xWild & "^009", vbTab, True)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 40%"
                g_oStatus.UpdateProgress(62)

                '   convert zzmp11Holders to tabs
                Try
                    ReplaceText(rngTOC, " z11H ", vbTab)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 50%"
                g_oStatus.UpdateProgress(64)
                Try
                    ReplaceText(rngTOC, "z11H ", vbTab)
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 60%"
                g_oStatus.UpdateProgress(64)

                '   remove chr(160) - inserted by
                '   underline to longest line function
                Try
                    ReplaceText(rngTOC, "^32" & ChrW(&HA0))
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 70%"
                g_oStatus.UpdateProgress(66)

                '   remove any possible multiple tabs
                Try
                    ReplaceText(rngTOC, "^009{2,}", vbTab, True)
                Catch
                End Try
                Try
                    ReplaceText(rngTOC, "zzmpMultiTabs")
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 80%"
                g_oStatus.UpdateProgress(66)

                '   unhide quote placeholders before removing
                Try
                    HideUnhideText(rngTOC, "zzmp34h", False)
                Catch
                End Try
                Try
                    HideUnhideText(rngTOC, "zzmp147h", False)
                Catch
                End Try
                Try
                    HideUnhideText(rngTOC, "zzmp148h", False)
                Catch
                End Try

                '   remove quote placeholders
                Try
                    ReplaceText(rngTOC, "zzmp34h", Chr(34))
                Catch
                End Try
                Try
                    ReplaceText(rngTOC, "zzmp147h", ChrW(&H201C))
                Catch
                End Try
                Try
                    ReplaceText(rngTOC, "zzmp148h", ChrW(&H201D))
                Catch
                End Try
                CurWordApp.StatusBar = xPrompt & ": 90%"
                g_oStatus.UpdateProgress(70)

                '   force chr 183 to be symbol
                With rngTOC.Find
                    .ClearFormatting()
                    .Text = g_xBullet
                    .MatchWildcards = False
                    .Replacement.Text = g_xBullet
                    .Replacement.Font.Name = "Symbol"
                    .Wrap = WdFindWrap.wdFindStop
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                End With
            Finally
                CurWordApp.StatusBar = ""
                EchoOn()
            End Try
        End Sub

        Private Sub AddDummyFirstPara(rngTOC As Word.Range)
            'adds a first para in toc so that we
            'can delete the 'real' first para if
            'necessary (can't delete 1st para in TOC field
            '-  this para is deleted later
            'in bInsertTOC routine

            Dim rngTOCStart As Word.Range
            Dim xChar1 As String

            rngTOCStart = rngTOC.Paragraphs(1).Range
            With rngTOCStart
                xChar1 = .Characters.First.Text
                .StartOf()
                .Move(WdUnits.wdCharacter)
                .InsertAfter(xChar1)
                .InsertBefore(vbCr)
            End With
        End Sub

        Private Sub DeleteDummyFirstPara(rngTOC As Word.Range)
            'deletes the first para of TOC -
            'this should be a copy of the actual
            'first para - see AddDummyFirstPara, above

            rngTOC.Paragraphs(1).Range.Delete()
        End Sub

        Private Shared Function xGetStyleList(ByVal xInclusions As String,
                                       ByVal iMinLevel As Integer,
                                       ByVal iMaxLevel As Integer,
                                       ByVal bApplyTOC9 As Boolean,
                                       ByVal xTOC9Style As String) As String
            Dim xTemp As String
            Dim iPos As Integer
            Dim xStyleList As String = ""
            Dim xScheme As String = ""
            Dim i As Integer
            Dim xStyleRoot As String = ""
            Dim xHeading As String = ""
            Dim iLevel As Integer

            xTemp = xInclusions

            If Len(xTemp) Then
                '       trim trailing and preceding commas
                xTemp = xTrimTrailingChrs(xTemp, ",", True, False)
                iPos = InStr(xTemp, ",")
                While iPos
                    '           add Heading 1-9 styles
                    xScheme = Strings.Left(xTemp, iPos - 1)
                    If xScheme = "Heading" Or bIsHeadingScheme(xScheme) Then
                        For i = iMinLevel To iMaxLevel
                            '                   strip alias from style name
                            xHeading = StripStyleAlias(xTranslateHeadingStyle(i))
                            xStyleList = xStyleList & xHeading &
                                        "," & i & ","
                        Next i
                    ElseIf xScheme = "Schedule" Then
                        For i = 0 To UBound(g_xScheduleStyles)
                            If bApplyTOC9 And (g_xScheduleStyles(i) = xTOC9Style) Then
                                '                       ensure inclusion regardless of range
                                iLevel = iMinLevel
                            ElseIf Val(g_xScheduleLevels(i, 1)) = WdOutlineLevel.wdOutlineLevelBodyText Then
                                'we're treating non-outline level schedule style as level 1
                                iLevel = WdOutlineLevel.wdOutlineLevel1
                            Else
                                '                       use actual level
                                iLevel = Val(g_xScheduleLevels(i, 1))
                            End If

                            'include if in range
                            If (iLevel >= iMinLevel) And (iLevel <= iMaxLevel) Then
                                xStyleList = xStyleList & g_xScheduleStyles(i) &
                                    "," & iLevel & ","
                            End If
                        Next i
                    Else
                        xStyleRoot = xGetStyleRoot(xScheme)
                        For i = iMinLevel To iMaxLevel
                            xStyleList = xStyleList & xStyleRoot &
                                        "_L" & i & "," & i & ","
                        Next i
                    End If
                    xTemp = Mid(xTemp, iPos + 1)
                    iPos = InStr(xTemp, ",")
                End While
            End If

            xGetStyleList = xStyleList

        End Function

        Friend Shared Function bReworkTOC(rngTOC As Word.Range,
                            xScheme As String,
                            xExclusions As String,
                            bIncludeStyles As Boolean,
                            bIncludeTCEntries As Boolean,
                            udtTOCFormat As TOCFormat,
                            xTOC9Style As String,
                            bDesignate As Boolean,
                            bApplyManualFormats As Boolean) As Boolean
            'cycles through TOC paras, cleaning
            'up in any number of ways
            Const mpErrorMargin As Integer = 5

            Dim i As Integer
            Dim rngP As Word.Range
            Dim iLevel As Integer
            Dim bIsCentered As Boolean
            Dim paraTOC As Word.Paragraph
            Dim xTabFlag(8) As String
            Dim iNumTabs As Integer
            Dim paraP As Word.Paragraph
            Dim l As Long
            Dim lNumParas As Long
            Dim pfPrev As Word.ParagraphFormat
            Dim pfCur As Word.ParagraphFormat
            Dim xPara As String = ""
            Dim bIsMarked As Boolean
            Dim bExclude As Boolean
            Dim bIsMarkedDel As Boolean
            Dim xPageNo As String = ""
            Dim sProgress As Single
            Dim sTab As Single
            Dim xNextPara As String = ""
            Dim iPos As Integer
            Dim xParaNum As String = ""
            Dim xHeading As String = ""
            Dim rngHeading As Word.Range
            Dim bIsSchedule As Boolean
            Dim bIsField As Boolean
            Dim bIsFirstPara As Boolean
            Dim xStyle As String = ""
            Dim xStylePrev As String = ""
            Dim iDisplay As Integer
            Dim sHPos As Single

            lNumParas = rngTOC.Paragraphs.Count
            bIsField = (rngTOC.Fields.Count > 0)

            '   substitute tabs for soft returns
            ReplaceText(rngTOC, New String(Chr(11), 2), vbTab)
            ReplaceText(rngTOC, Chr(11), vbTab)

            '   native TOC automatically 1) inserts space when there's
            '   no trailing character and 2) removes trailing space in
            '   number format (e.g. with our old method of doing two spaces);
            '   substitute tab, leaving mpStartPara for use in Word 97
            '   split para test below
            ReplaceText(rngTOC, " " & mpStartPara, mpStartPara & vbTab)

            '   hide tags in TOC - this will permit
            '   accurate measurements for tabs below
            HideTags(rngTOC)

            '   hide quote placeholders
            HideUnhideText(rngTOC, "zzmp34h", True)
            HideUnhideText(rngTOC, "zzmp147h", True)
            HideUnhideText(rngTOC, "zzmp148h", True)

            '   turn off hidden text, allowing ShowAll to control
            CurWordApp.ActiveWindow.View.ShowHiddenText = False

            '---cycle through TOC paragraphs
            For Each paraTOC In rngTOC.Paragraphs
                'OutputDebugString "bReworkTOC - Start new paragraph"
                rngP = paraTOC.Range

                '       first paragraph of field result will be deleted at end
                '       of this function - it's just a dummy entry necessitated
                '       by inability to freely edit first paragraph of TOC field
                bIsFirstPara = ((rngP.Start = rngTOC.Paragraphs.First.Range.Start) And
                        (rngP.End = rngTOC.Paragraphs.First.Range.End))
                'OutputDebugString "bReworkTOC - After first para comparison"
                If bIsField And bIsFirstPara Then
                    GoTo NextPara
                End If

                With rngP
                    pfCur = .ParagraphFormat
                    'OutputDebugString "bReworkTOC - After set pfCur"
                    xPara = .Text
                    'OutputDebugString xPara
                    '           skip empty paras
                    If Len(xPara) < 2 Then
                        If (paraTOC.Range.Start = rngTOC.Paragraphs.Last.Range.Start) And
                                (paraTOC.Range.End = rngTOC.Paragraphs.Last.Range.End) Then
                            Exit For
                        Else
                            GoTo NextPara
                        End If
                    End If

                    '           this will avoid a problematic second trip through a split
                    '           Word 97 para set to exclude page number
                    If Mid(xPara, Len(xPara) - 1, 1) = "|" Then
                        GoTo NextPara
                    End If

                    '           delete entry under certain conditions -
                    '           this depends on the type of entry that it is,
                    '           and what's been requested by the user for the TOC
                    xPara = .Text
                    If xTOC9Style <> "" Then
                        bIsSchedule = (InStr(UCase(xPara),
                            mpSchemeNameStart & UCase(xTOC9Style) & mpSchemeNameEnd) <> 0)
                    End If
                    bIsMarked = (InStr(UCase(xPara), UCase(mpTCMarked)) > 0)
                    bIsMarkedDel = (InStr(UCase(xPara), UCase(mpMarkedDel)) > 0)

                    If bIsSchedule And bIsMarked Then
                        '               next entry should be a duplicate
                        With CurWordApp.ActiveWindow.View
                            .ShowAll = False
                            With rngP
                                If UCase(.Text) = UCase(.Next(WdUnits.wdParagraph).Text) Then
                                    .Next(WdUnits.wdParagraph).Style = "TOC 9"
                                    .Text = ""
                                    GoTo NextPara
                                End If
                            End With
                            .ShowAll = True
                        End With
                    End If

                    If bIsMarkedDel Then
                        .Text = ""
                        GoTo NextPara
                    ElseIf bIncludeStyles And bIncludeTCEntries Then
                        '               delete if base scheme is in exclusion list
                        If ExcludeTOCEntry(xPara, xExclusions, xTOC9Style, bDesignate) Then
                            .Text = ""
                            GoTo NextPara
                        End If
                    ElseIf bIncludeStyles Then
                        '               delete if base scheme is in exclusion list
                        If ExcludeTOCEntry(xPara, xExclusions, xTOC9Style, bDesignate) Then
                            .Text = ""
                            GoTo NextPara
                        End If
                    ElseIf bIncludeTCEntries Then
                        '               delete if begins with page break
                        If InStr(.Text, vbTab & mpPageBreakMarker) Then
                            .Text = ""
                            GoTo NextPara
                        End If
                    Else
                        .Text = ""
                        GoTo NextPara
                    End If
                    'OutputDebugString "bReworkTOC - After inclusion/exclusion"
                    '           apply TOC 9 to Schedule entry
                    If bIsSchedule Then
                        .Style = g_oWordApp.ActiveDocument.Styles("TOC 9")
                    End If

                    '           get style
                    xStyle = .Style.NameLocal

                    '           remove extra tabs - do if more than two or if it's
                    '           a non-numbered entry - added last conditional on 11/30/01;
                    '           removed last conditional on 7/1/03 to fix log item #3038 -
                    '           shift-returns in non-numbered entries are now stripped from TC
                    '           code, so these no longer come into TOC as tabs that need to be removed
                    iNumTabs = lCountChrs(xPara, vbTab)
                    If (iNumTabs > 2) Or
                            (Strings.Left(xPara, Len(mpStartPara)) = mpStartPara) Then
                        '                    Or (InStr(xPara, "|MP") = 0) Then
                        RemoveExtraTabs(rngP)
                    End If
                    'OutputDebugString "bReworkTOC - After extra tabs"
                    '           get level
                    iLevel = iGetTOCStyleLevel(xStyle)
                    'OutputDebugString "bReworkTOC - After get level"
                    '           adjust hanging indent of style based on
                    '           first representative entry for this level
                    '           determine if it's a numbered entry and
                    '           whether level has already been checked;
                    '           hide tags for accurate measurement
                    With CurWordApp.ActiveWindow.View
                        .ShowAll = False
                    End With
                    xPara = .Text
                    iNumTabs = lCountChrs(xPara, vbTab)
                    If iNumTabs > 1 Then
                        If g_bUseNewTabAdjustmentRules Then
                            'new option checks every entry for tightness and adjusts only as
                            'much as needed, rather than taking first entry as representative
                            'and leaving a cushion for potentially longer entries
                            rngHeading = rngP.Duplicate
                            sHPos = MeasureHeading(rngHeading, pfCur, xPara)

                            'adjust flag if longest entry found for level
                            If xTabFlag(iLevel - 1) = "" Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            End If
                        ElseIf xTabFlag(iLevel - 1) = "" Then
                            'set flag
                            xTabFlag(iLevel - 1) = "X"
                            AdjustStyleIndents(rngP, pfCur, iLevel, xPara)
                        End If
                    ElseIf iNumTabs = 1 Then
                        'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                        'repeated selections from producing a visual "creeping" effect, and
                        'Range.Information has not been problematic since Word 2000
                        Dim oRange As Word.Range
                        oRange = .Characters.First
                        With oRange
                            .EndOf(WdUnits.wdParagraph)
                            .Move(WdUnits.wdCharacter, -1)
                            'style is used, since tabstops method of selection
                            'will fail when document's default tab stop is 0"
                            With CurWordApp.ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                                If .Count > 0 Then _
                                    sTab = .Item(.Count).Position
                            End With
                            If .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary) <
                                    sTab - CurWordApp.InchesToPoints(0.25) Then
                                'measurement is not accurate to the point, so allow 1/4"
                                'leeway - this won't be a problem because hanging indent
                                'or preceding tab stop will never be set this close to right tab
                                .MoveUntil(vbTab, WdConstants.wdBackward)
                                'include placemarker to prevent extra tab
                                'from being deleted in CleanUpTOC
                                .InsertAfter("zzmpMultiTabs" & vbTab)
                            End If
                        End With
                    End If
                    'OutputDebugString "bReworkTOC - After tab set"
                    '           show all again
                    With CurWordApp.ActiveWindow.View
                        .ShowAll = True
                    End With

                    '           delete leading quote if it's the only quote in entry
                    rngHeading = rngP.Duplicate
                    With rngHeading
                        .Expand(WdUnits.wdParagraph)
                        iPos = InStr(.Text, mpEndHeading)
                        If iPos Then
                            '                   MacPac numbered entry - get portion of entry that will be retained
                            xHeading = Strings.Left(.Text, iPos - 1)
                            If lCountChrs(xHeading, Chr(34)) +
                                    lCountChrs(xHeading, ChrW(&H201C)) +
                                    lCountChrs(xHeading, ChrW(&H201D)) = 1 Then
                                '                       there's only one double quote in the entry
                                iPos = InStr(.Text, mpStartPara)
                                If iPos Then
                                    .MoveStart(WdUnits.wdCharacter, iPos + Len(mpStartPara) - 1)
                                    '                           if the lone double quote is at the start of the heading, delete it
                                    With .Characters(1)
                                        If (.Text = Chr(34)) Or (.Text = ChrW(&H201C)) Then
                                            .Delete()
                                        End If
                                    End With
                                End If
                            End If
                        Else
                            '                   non-numbered entry
                            xHeading = .Text
                            If lCountChrs(xHeading, "zzmp34h") +
                                    lCountChrs(xHeading, "zzmp147h") +
                                    lCountChrs(xHeading, "zzmp148h") = 1 Then
                                '                       if the lone double quote is at the start of the heading, delete it
                                .StartOf()
                                If Strings.Left(xHeading, 7) = "zzmp34h" Then
                                    .MoveEnd(WdUnits.wdCharacter, 7)
                                    .Delete()
                                ElseIf Strings.Left(xHeading, 8) = "zzmp147h" Then
                                    .MoveEnd(WdUnits.wdCharacter, 8)
                                    .Delete()
                                End If
                            End If
                        End If
                    End With
                    'OutputDebugString "bReworkTOC - After quotes"
                    '           get alignment
                    bIsCentered = pfCur.Alignment

                    '           format centered paras
                    If bIsCentered Then
                        FormatCenteredLevel(rngP, pfCur)
                    End If

                    '           format para to have or not have page number
                    FormatPageNumber(rngP,
                                     bIsCentered,
                                     pfCur,
                                     udtTOCFormat.IncludePageNumber(iLevel - 1))
                    'OutputDebugString "bReworkTOC - After page number"
                    '           if first para of new level,
                    '           adjust space after previous paragraph -
                    '           pfPrev is set below, so it will be
                    '           nothing the first time around
                    If Not (pfPrev Is Nothing) Then
                        If (xStyle <> xStylePrev) Then
                            If (udtTOCFormat.SpaceBefore(iLevel - 1) <> "") And
                                    (udtTOCFormat.SpaceBefore(iLevel - 1) <> "mpNA") Then
                                pfPrev.SpaceAfter = udtTOCFormat.SpaceBefore(iLevel - 1)
                            End If
                        End If
                    End If
                    'OutputDebugString "bReworkTOC - After space adjustment"
                    '           call any custom code
                    lRet = oCustTOC.lInTOCRework(rngP,
                                                 xScheme,
                                                 xExclusions,
                                                 bIncludeStyles,
                                                 bIncludeTCEntries)
                End With
NextPara:
                '       free up resources
                If Not g_bPreserveUndoListTOC Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       store current para format for
                '       next iteration through loop
                xStylePrev = xStyle
                pfPrev = pfCur
                rngP = Nothing
                paraTOC = Nothing
                l = l + 1
                If ((l / lNumParas) * 100) > (iDisplay + 10) Then
                    iDisplay = iDisplay + 10
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Refaire TM: " & iDisplay & "%"
                    Else
                        CurWordApp.StatusBar = "Reworking TOC: " & iDisplay & "%"
                    End If
                    sProgress = 48 + (12 * (l / lNumParas))
                    g_oStatus.UpdateProgress(sProgress)
                End If
                'OutputDebugString "bReworkTOC - Paragraph l=" & l
            Next paraTOC

            'adjust indents as necessary
            If g_bUseNewTabAdjustmentRules Then
                For i = 0 To 8
                    If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                        AdjustStyleIndentsNew(i + 1, CSng(xTabFlag(i)), True)
                    End If
                Next i
            End If

            '   first paragraph of TOC field can't be deleted in normal manner
            If bIsField Then
                If rngTOC.Paragraphs.Count = 2 Then
                    '           delete field - there were no bona fide TOC entries
                    rngTOC.Fields(1).Delete()
                Else
                    rngP = rngTOC.Paragraphs(1).Range
                    With rngP
                        .MoveEnd(WdUnits.wdCharacter, -1)
                        .MoveStart()
                        .Select()
                        With CurWordApp.Selection
                            CurWordApp.WordBasic.EditClear()
                            .Move()
                            ''                   preserve style before deleting paragraph mark
                            '                    xStyle = .Style
                            '                   1st character and para mark should be all that's left;
                            '                   delete them now
                            CurWordApp.WordBasic.DeleteBackWord()
                            '                   reapply style
                            .Style = xStyle
                            '                   first character doesn't always get deleted
                            On Error Resume Next
                            If .Previous(WdUnits.wdCharacter, 1).Style = xStyle Then _
                                CurWordApp.WordBasic.DeleteBackWord()
                            On Error GoTo 0
                        End With
                    End With
                End If
            End If

            If bApplyManualFormats Then
                '       remove underlining of any underlined tabs
                With rngTOC.Find
                    .ClearFormatting()
                    .Format = True
                    .Font.Underline = WdUnderline.wdUnderlineSingle
                    .Text = vbTab
                    With .Replacement.Font
                        .Underline = WdUnderline.wdUnderlineNone
                        .Hidden = False
                    End With
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                End With
            End If

            '   ensure no space before first entry
            rngTOC.Paragraphs(1).SpaceBefore = 0

            '   turn hidden text back on - this will ensure that the pipes get cleaned up
            CurWordApp.ActiveWindow.View.ShowHiddenText = True
        End Function

        Friend Shared Function udtGetTOCFormat(xScheme As String,
                                 bIncludeSchedule As Boolean) As TOCFormat
            Dim udtFormat As TOCFormat
            Dim xSpaceBefore As String = ""
            Dim xSpaceBetween As String = ""
            Dim xLineSpacing As String = ""
            Dim iInclPgNum As Integer
            Dim iAllCaps As Integer
            Dim iDotLeaders As Integer
            Dim i As Integer
            Dim xBold As String = ""
            Dim xValue As String = ""
            Dim iPos As Integer

            With udtFormat
                'Initial size cannot be set in Type declaration
                'Arrays must have lower bound of 0
                ReDim .AllCaps(0 To 8)
                ReDim .Bold(0 To 8)
                ReDim .SpaceBefore(0 To 8)
                ReDim .IncludeDotLeaders(0 To 8)
                ReDim .IncludePageNumber(0 To 8)
                '       get points before values for all levels
                If xScheme = "Custom" Then
                    '           get user defaults or, if missing
                    '           from user ini, set to skip
                    On Error Resume Next
                    xSpaceBefore = GetUserSetting("TOC", "PointsBefore")
                    iInclPgNum = GetUserSetting("TOC", "PageNumbers")
                    iDotLeaders = GetUserSetting("TOC", "DotLeaders")
                    iAllCaps = GetUserSetting("TOC", "AllCaps")
                    xBold = GetUserSetting("TOC", "Bold")
                    .CenterLevel1 = GetUserSetting("TOC", "CenterLevel1") = "1"
                    xLineSpacing = GetUserSetting("TOC", "LineSpacing")
                    xSpaceBetween = GetUserSetting("TOC", "PointsBetween")

                    On Error GoTo 0

                    If xLineSpacing = "" Then
                        .LineSpacing = mpCGSSkipItem
                    Else
                        .LineSpacing = xLineSpacing
                    End If

                    If xSpaceBetween = "" Then
                        .SpaceBetween = mpCGSSkipItem
                    Else
                        .SpaceBetween = xLocalizeNumericString(xSpaceBetween)
                    End If

                    If xSpaceBefore = "" Then
                        xSpaceBefore = "6"
                    Else
                        xSpaceBefore = xLocalizeNumericString(xSpaceBefore)
                    End If

                    For i = 0 To 8
                        .SpaceBefore(i) = xSpaceBefore

                        '               get whether to include page numbers
                        Select Case iInclPgNum
                            Case mpTOCLevels.mpTOCLevels_All
                                .IncludePageNumber(i) = True
                            Case mpTOCLevels.mpTOCLevels_1
                                .IncludePageNumber(i) = (i = 0)
                            Case mpTOCLevels.mpTOCLevels_Subsequent
                                .IncludePageNumber(i) = (i > 0)
                            Case mpTOCLevels.mpTOCLevels_None
                                .IncludePageNumber(i) = False
                            Case Else
                                .IncludePageNumber(i) = True
                        End Select

                        '               get whether to include all caps;
                        '               subsequent option was removed from list
                        If iAllCaps = mpTOCLevels.mpTOCLevels_Subsequent Then _
                            iAllCaps = mpTOCLevels.mpTOCLevels_None
                        Select Case iAllCaps
                            Case mpTOCLevels.mpTOCLevels_All
                                .AllCaps(i) = True
                            Case mpTOCLevels.mpTOCLevels_1
                                .AllCaps(i) = (i = 0)
                            Case mpTOCLevels.mpTOCLevels_Subsequent
                                .AllCaps(i) = (i > 0)
                            Case mpTOCLevels.mpTOCLevels_None
                                .AllCaps(i) = False
                            Case Else
                                .AllCaps(i) = True
                        End Select

                        '               get whether to include bold
                        Select Case xBold
                            Case Trim(Str(mpTOCLevels.mpTOCLevels_All))
                                .Bold(i) = True
                            Case Trim(Str(mpTOCLevels.mpTOCLevels_1))
                                .Bold(i) = (i = 0)
                            Case Trim(Str(mpTOCLevels.mpTOCLevels_Subsequent))
                                '                       subsequent is not an option for bold
                                .Bold(i) = False
                            Case Else
                                '                       custom scheme hasn't been edited since Bold
                                '                       property added - default to false
                                .Bold(i) = False
                        End Select

                        '               get whether to include dot leaders
                        Select Case iDotLeaders
                            Case mpTOCLevels.mpTOCLevels_All
                                .IncludeDotLeaders(i) = True
                            Case mpTOCLevels.mpTOCLevels_1
                                .IncludeDotLeaders(i) = (i = 0)
                            Case mpTOCLevels.mpTOCLevels_Subsequent
                                .IncludeDotLeaders(i) = (i > 0)
                            Case mpTOCLevels.mpTOCLevels_None
                                .IncludeDotLeaders(i) = False
                            Case Else
                                .IncludeDotLeaders(i) = True
                        End Select
                    Next i
                Else
                    For i = 0 To 8
                        '               get from Numbering Scheme TOC properties
                        .SpaceBefore(i) = xGetLevelProp(xScheme,
                                                i + 1,
                                                mpTOCLevelProp.mpTOCLevelProp_SpaceBeforeLevel,
                                                mpSchemeTypes.mpSchemeType_TOC)

                        .IncludePageNumber(i) = xGetLevelProp(xScheme,
                                                    i + 1,
                                                    mpTOCLevelProp.mpTOCLevelProp_IncludePgNum,
                                                    mpSchemeTypes.mpSchemeType_TOC)

                        '               skip all these formats - when using a predefined scheme
                        '               these values are taken directly from the style definitions
                        .IncludeDotLeaders(i) = mpCGSSkipItem
                        .AllCaps(i) = mpCGSSkipItem
                    Next i
                    .LineSpacing = mpCGSSkipItem
                    .SpaceBetween = mpCGSSkipItem
                End If

                '       get Schedule props
                If bIncludeSchedule Then
                    On Error Resume Next
                    xValue = CurWordApp.Templates(xTOCSTY) _
                        .CustomDocumentProperties("SpecialTOC9").Value
                    On Error GoTo 0
                    If xValue <> "" Then
                        iPos = InStr(2, xValue, "|")
                        .SpaceBefore(8) = Mid(xValue, 2, iPos - 2)
                        .IncludePageNumber(8) = Mid(xValue, Len(xValue) - 1, 1)
                        .IncludeDotLeaders(8) = mpCGSSkipItem
                        .AllCaps(8) = mpCGSSkipItem
                    End If
                End If

            End With
            udtGetTOCFormat = udtFormat
        End Function

        Private Shared Function FormatPageNumber(rngP As Word.Range,
                                          bIsCentered As Boolean,
                                          pfCur As Word.ParagraphFormat,
                                          bInclude As Boolean,
                                          Optional ByVal bIsField As Boolean = False)
            Dim sHPos As Single

            With rngP
                If bInclude Then
                    '           if centered with page no, replace last tab with 2 spaces
                    If bIsCentered Then
                        '9/28/12 - do only if paragraph contains a tab
                        If InStr(rngP.Paragraphs(1).Range.Text, vbTab) > 0 Then
                            .EndOf(WdUnits.wdParagraph)
                            .MoveUntil(vbTab, WdConstants.wdBackward)
                            .MoveStart(WdUnits.wdCharacter, -1)
                            .MoveStartWhile(CStr(Chr(11)), WdConstants.wdBackward)
                            .Text = Chr(11)
                        End If
                    ElseIf Not bIsField Then
                        '               if any EOL's are not at the right tab position,
                        '               then add a tab using tab set as determination
                        '                .EndOf wdUnits.wdParagraph
                        With .Characters.Last
                            'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                            'repeated selections from producing a visual "creeping" effect, and
                            'Range.Information has not been problematic since Word 2000
                            sHPos = .Information(WdInformation.wdHorizontalPositionRelativeToPage)
                            If (Int(sHPos) + 1 <
                                Int(pfCur.TabStops(1).Position)) And
                                (sHPos > -1) Then
                                .MoveEndUntil(vbTab, WdConstants.wdBackward)
                                .InsertAfter(vbTab)
                            End If
                        End With
                    End If
                ElseIf Not bIsField Then
                    '           remove page #, tab, and any extraneous shift-returns
                    '           7/6/12 (dm) - field option uses switch to accomplish this
                    .EndOf(WdUnits.wdParagraph)
                    .Move(WdUnits.wdCharacter, -1)
                    .MoveStartUntil(vbTab, WdConstants.wdBackward)
                    .MoveStart(WdUnits.wdCharacter, -1)
                    .MoveStartWhile(CStr(Chr(11)), WdConstants.wdBackward)
                    .Delete()
                End If

            End With
        End Function

        Friend Shared Function ExcludeTOCEntry(ByVal xPara As String,
                                 ByVal xExclusions As String,
                                 ByVal xTOC9Style As String,
                                 ByVal bDesignate As Boolean) As Boolean
            '   returns TRUE if scheme name is in
            '   exclusion list or heading is empty
            Dim lPos1 As Long
            Dim lPos2 As Long
            Dim xBaseScheme As String = ""

            '   marked for deletion or scheme name is empty
            If InStr(xPara, "zzmpDel") Or
                    InStr(xPara, mpSchemeNameStart & mpSchemeNameEnd) Then
                ExcludeTOCEntry = True
                Exit Function
            End If

            '   return TRUE if the heading is empty - in
            '   this case the '|MPEH|' tag will start the para
            If Strings.Left(xPara, 6) = "|MPEH|" Then
                ExcludeTOCEntry = True
                Exit Function
            End If

            '   return TRUE if page break marker immediately follows tab
            If InStr(xPara, vbTab & mpPageBreakMarker) Then
                ExcludeTOCEntry = True
                Exit Function
            End If

            '   parse out scheme name - held between
            '   'name start' and 'name end' tags
            lPos1 = InStr(UCase(xPara), UCase(mpSchemeNameStart))
            If lPos1 Then
                '       get base scheme from codes
                lPos2 = InStr(CInt(lPos1 + Len(lPos1) + 1),
                              UCase(xPara),
                              UCase(mpSchemeNameEnd))
                xBaseScheme = Mid(xPara, lPos1 + Len(mpSchemeNameStart),
                                lPos2 - lPos1 - Len(mpSchemeNameStart))
            Else
                xBaseScheme = ""
            End If

            '   TOC9 Style is subset of "Schedule"
            If Not bDesignate And
                    (UCase(xBaseScheme) = UCase(xTOC9Style)) And
                    (InStr(UCase(xExclusions), ",SCHEDULE,") <> 0) Then
                ExcludeTOCEntry = True
                Exit Function
            End If

            ExcludeTOCEntry = InStr(UCase(xExclusions),
                    "," & UCase(xBaseScheme) & ",")
        End Function

        Friend Shared Sub RemoveExtraTabs(rngP As Word.Range)
            With rngP
                If (Strings.Left(.Text, Len(mpStartPara)) = mpStartPara) Or
                        (InStr(.Text, "|MP") = 0) Then
                    '           non-numbered paragraph - move before last tab
                    .MoveEndUntil(vbTab, WdConstants.wdBackward)
                    .MoveEnd(WdUnits.wdCharacter, -1)

                    '           replace all extra tabs with a space
                    While lCountChrs(.Text, vbTab) > 0
                        .MoveStartUntil(vbTab)
                        .Characters(1).Text = " "
                    End While
                Else
                    '           move after first tab pos
                    .MoveStartUntil(vbTab)
                    .MoveStart(WdUnits.wdCharacter)

                    '           replace all extra tabs with a space
                    While lCountChrs(.Text, vbTab) > 1
                        .MoveStartUntil(vbTab)
                        .Characters(1).Text = " "
                    End While
                End If

                '       reset range to whole para again
                .Expand(WdUnits.wdParagraph)
            End With
        End Sub

        Friend Shared Sub AdjustStyleIndents(rngP As Word.Range,
                                    pfCur As Word.ParagraphFormat,
                                    ByVal iLevel As Integer,
                                    ByVal xPara As String)
            Dim sHPos As Single
            Dim xStart As String = ""
            Dim lPos As Long
            Dim sLeftIndent As Single
            Dim sChange As Single

            If pfCur.Alignment <> WdParagraphAlignment.wdAlignParagraphCenter Then
                With rngP
                    sLeftIndent = pfCur.LeftIndent
                    '           get start position of text after tab;
                    '           changed to before tab by DM on 12/14/99
                    lPos = InStr(xPara, vbTab)
                    .StartOf()
                    .Move(WdUnits.wdCharacter, lPos - 1)

                    'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                    'repeated selections from producing a visual "creeping" effect, and
                    'Range.Information has not been problematic since Word 2000
                    sHPos = .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)

                    '           if past left indent, needs adjusting
                    If sHPos > pfCur.LeftIndent Then
                        '                Selection.Move wdUnits.wdCharacter, -1
                        '               locate the .25" sector of document that contains end of number;
                        '               tab s/b .5" from start of that sector
                        sHPos = .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)
                        xStart = LTrim(Partition(sHPos, 0, 612, 18))
                        xStart = Strings.Left(xStart, InStr(xStart, ":") - 1)
                        '               redefine style
                        'GLOG 5615 - ensure that not referencing character style
                        With CurWordApp.ActiveDocument.Styles(.Paragraphs(1).Style).ParagraphFormat
                            .LeftIndent = CSng(xStart) + 36
                            sChange = pfCur.LeftIndent - sLeftIndent
                            .FirstLineIndent = .FirstLineIndent - sChange
                        End With
                    ElseIf sHPos = pfCur.LeftIndent And lCountChrs(Strings.Left(xPara, lPos - 1), ".") > 2 Then
                        ' Make sure there's some extra space for multi-level numbers
                        '                CurWordApp.Selection.Move wdUnits.wdCharacter, -1
                        sHPos = .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)
                        If pfCur.LeftIndent - sHPos < 10 Then
                            xStart = LTrim(Partition(sHPos, 0, 612, 18))
                            xStart = Strings.Left(xStart, InStr(xStart, ":") - 1)
                            '               redefine style
                            'GLOG 5615 - ensure that not referencing character style
                            With CurWordApp.ActiveDocument.Styles(.Paragraphs(1).Style).ParagraphFormat
                                .LeftIndent = CSng(xStart) + 36
                                sChange = pfCur.LeftIndent - sLeftIndent
                                .FirstLineIndent = .FirstLineIndent - sChange
                            End With
                        End If
                    End If
                    .Expand(WdUnits.wdParagraph)
                End With
            End If

        End Sub

        Friend Shared Sub AdjustStyleIndentsNew(ByVal iLevel As Integer, ByVal sHeadingPos As Single,
                ByVal bKeepNextLevelAligned As Boolean)
            Const mpErrorMargin As Integer = 5
            Dim sChange As Single
            Dim oStyle As Word.Style
            Dim bUseNextLevel As Boolean
            Dim sStartIndent As Single
            Dim sMinIndent As Single
            Dim i As Integer
            Dim oStyleNext As Word.Style

            On Error Resume Next
            oStyle = CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(iLevel))
            On Error GoTo 0

            If Not oStyle Is Nothing Then
                sStartIndent = oStyle.ParagraphFormat.LeftIndent
                sMinIndent = sHeadingPos + 3.6

                'if past left indent, needs adjusting
                If sStartIndent < sMinIndent Then
                    'use next level if it satisfies minimum indent and is not more
                    'than 1/4" greater than in it
                    If iLevel < 9 Then
                        On Error Resume Next
                        oStyleNext = CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(iLevel + 1))
                        On Error GoTo 0
                        If Not oStyleNext Is Nothing Then
                            bUseNextLevel = ((oStyleNext.ParagraphFormat.LeftIndent > sMinIndent) And
                                (oStyleNext.ParagraphFormat.LeftIndent < sMinIndent + 18))
                        End If
                    End If

                    'adjust
                    With oStyle.ParagraphFormat
                        If bUseNextLevel Then
                            'redefine style to align with next level
                            .LeftIndent = oStyleNext.ParagraphFormat.LeftIndent
                        Else
                            'expand by 0.1 until minimum is reached
                            While .LeftIndent < sMinIndent
                                .LeftIndent = .LeftIndent + 7.2
                            End While
                        End If
                        sChange = .LeftIndent - sStartIndent
                        .FirstLineIndent = .FirstLineIndent - sChange

                        'GLOG 5146 (2/26/13) - decided to no longer adjust subsequent levels at all -
                        'the rules were arbitary and we're guessing at user intent
                        '                If (Not bUseNextLevel) And bKeepNextLevelAligned And (sChange <= 36) Then
                        '                    'if this level previously aligned with subsequent levels,
                        '                    'adjust them as well
                        '                    'GLOG 5146 (2/21/13) - limited to change of 0.5" or less and
                        '                    'removed unwarranted inferences about the first line indent
                        '                    For i = iLevel + 1 To 9
                        '                        Set oStyleNext = CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(i))
                        '                        On Error GoTo ProcError
                        '                        If Not oStyleNext Is Nothing Then
                        '                            With oStyleNext.ParagraphFormat
                        '                                If (.LeftIndent + .FirstLineIndent = sStartIndent) Or _
                        '                                        (.LeftIndent = sStartIndent) Then
                        '                                    'keep headings aligned
                        '                                    sStartIndent = .LeftIndent
                        '                                    .LeftIndent = sStartIndent + sChange
                        '                                Else
                        '                                    Exit For
                        '                                End If
                        '                            End With
                        '                        End If
                        '                    Next i
                        '                End If
                    End With
                End If
            End If
        End Sub

        Friend Shared Function MeasureHeading(ByVal rngP As Word.Range, ByVal pfCur As Word.ParagraphFormat,
                ByVal xPara As String) As Single
            Dim sHPos As Single
            Dim lPos As Long

            If pfCur.Alignment <> WdParagraphAlignment.wdAlignParagraphCenter Then
                With rngP
                    '           get start position of text after tab;
                    '           changed to before tab by DM on 12/14/99
                    lPos = InStr(xPara, vbTab)
                    .StartOf()
                    .Move(WdUnits.wdCharacter, lPos - 1)

                    'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                    'repeated selections from producing a visual "creeping" effect, and
                    'Range.Information has not been problematic since Word 2000
                    sHPos = .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary)
                End With
            End If

            MeasureHeading = sHPos
        End Function

        Friend Shared Sub FormatCenteredLevel(rngP As Word.Range, pfCur As Word.ParagraphFormat,
               Optional ByVal bIsField As Boolean = False)
            Dim xTrailChr As String = ""
            Dim iNumTabs As Integer
            Dim bDo As Boolean

            iNumTabs = lCountChrs(rngP.Text, vbTab)
            If bIsField Then
                bDo = (iNumTabs > 0)
            Else
                bDo = (iNumTabs > 1)
            End If
            If bDo Then
                With rngP
                    '           if centered, replace remaining tab with shift-return(s)
                    '           base # of shift-returns on line spacing
                    '            With pfCur
                    '                If .LineSpacingRule = wdLineSpaceDouble Or _
                    '                        .LineSpacing > 14 Then
                    xTrailChr = Chr(11)
                    '                Else
                    '                    xTrailChr = String(2, 11)
                    '                End If
                    '            End With

                    '           replace first tab with trailing chr
                    .StartOf(WdUnits.wdParagraph)
                    .MoveUntil(vbTab)
                    .MoveEnd(WdUnits.wdCharacter)
                    If .Next(WdUnits.wdCharacter).Text = Chr(32) Then _
                        .MoveEnd(WdUnits.wdCharacter)
                    .Text = xTrailChr
                End With
            End If
        End Sub

        Friend Shared Function IncrementTOCTabs(Optional ByVal bDecrement As Boolean = False) As Long
            'increases/decreases hanging indent of style of
            'first para in selection
            Dim rngSel As Word.Range
            Dim iLevel As Integer
            Dim xStyle As String = ""
            Dim sIncrement As Single

            IncrementTOCTabs = 0
            '   set appropriate increment for Word unit
            Select Case CurWordApp.Options.MeasurementUnit
                Case WdMeasurementUnits.wdInches
                    sIncrement = 3.6
                Case WdMeasurementUnits.wdCentimeters, WdMeasurementUnits.wdMillimeters
                    sIncrement = 1.4
                Case Else
                    sIncrement = 3
            End Select

            '   get style to modify
            xStyle = CurWordApp.Selection.Range.Paragraphs(1).Style.NameLocal

            '   do only for TOC styles
            If bIsTOCStyle(xStyle) Then
                If bDecrement Then
                    SetTOCTabs(-sIncrement, Strings.Right(xStyle, 1))
                Else
                    SetTOCTabs(sIncrement, Strings.Right(xStyle, 1))
                End If
            End If
        End Function

        Friend Shared Function FillTCCodes(Optional bFillFormatted As Boolean = False,
                             Optional bScreenUpdating As Boolean = True,
                             Optional bReplaceShiftReturns As Boolean = True) As Long
            'fills tc codes in doc with formatted
            'text to beginning of para- adds the
            'appropriate codes for restoring them
            'in the future
            '9/27/12 (dm) - added bReplaceShiftReturns argument
            Dim fldP As Word.Field
            Dim rngCode As Word.Range
            Dim bIsTC As Boolean
            Dim bIsTCStatic As Boolean
            Dim bIsTCPDyn As Boolean
            Dim bIsTCDyn As Boolean
            Dim rngHeading As Word.Range
            Dim iLevel As Integer
            Dim lCodes As Long
            Dim l As Long
            Dim iView As WdViewType
            Dim xText As String = ""

            Try
                FillTCCodes = 0

                CurWordApp.ScreenUpdating = False
                iView = CurWordApp.ActiveWindow.View.Type
                CurWordApp.ActiveWindow.View.Type = WdViewType.wdNormalView

                '   cycle through tc codes
                lCodes = CurWordApp.ActiveDocument.Fields.Count
                For Each fldP In CurWordApp.ActiveDocument.Fields
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar =
                            "Classement Codes TM: " &
                                Format(l / lCodes, "0%")
                    Else
                        CurWordApp.StatusBar =
                            "Filling TC Codes: " &
                                Format(l / lCodes, "0%")
                    End If
                    l = l + 1
                    bIsTCDyn = False
                    bIsTCPDyn = False
                    bIsTCStatic = False
                    iLevel = 0

                    rngCode = fldP.Code
                    bIsTC = UCase(Strings.Left(rngCode.Text, 4)) = " " & g_xTCPrefix & " "
                    If bIsTC Then
                        '           get type of tc code
                        bIsTCDyn = (UCase(rngCode.Text) = " " & g_xTCPrefix & " ")
                        If Not bIsTCDyn Then
                            bIsTCStatic =
                                UCase(Strings.Left(rngCode.Text, 5) =
                                    " " & g_xTCPrefix & " " & Chr(34))
                        End If
                        If Not (bIsTCStatic Or bIsTCDyn) Then
                            bIsTCPDyn = True
                        End If

                        '           if dynamic code...
                        If Not bIsTCStatic Then
                            '               get level
                            If bIsTCDyn Then
                                iLevel = iGetDynLevel(rngCode)
                            Else
                                '                   code is partial dynamic - get level from code
                                iLevel = iGetPDynLevel(rngCode)
                            End If

                            '               bInsertTCEntryText, which is used below for formatted
                            '               codes, adds level switch; strip from partially dynamic code (7/1/03)
                            If bIsTCPDyn And bFillFormatted Then _
                                rngCode.Text = " " & g_xTCPrefix & " "

                            '               get heading
                            rngHeading = rngCode.Duplicate

                            With rngHeading
                                '                   collapse range to start
                                .StartOf()

                                '                   if there's no heading, don't attempt to fill code
                                If .Start = .Paragraphs(1).Range.Start Then _
                                    GoTo labNextfield

                                '                   extend to start of para
                                .StartOf(WdUnits.wdParagraph, WdMovementType.wdExtend)
                                .MoveStartWhile(CStr(Chr(12)))

                                '                   this will exclude list num field code
                                '                    .TextRetrievalMode.IncludeFieldCodes = False
                            End With

                            '               insert field code text
                            If bFillFormatted Then
                                bRet = bInsertTCEntryText(rngHeading,
                                                          rngCode,
                                                          iLevel,
                                                          vbTab)
                            Else
                                rngCode.Text = " " & g_xTCPrefix & " " & xGetTCEntryText _
                                                            (rngHeading,
                                                            iLevel,
                                                            vbTab)
                            End If

                            '               function above inserts as ref field
                            fldP.Unlink()

                            If bIsTCDyn Then
                                '                   remove shift-returns and spaces after tabs
                                If bReplaceShiftReturns Or (iLevel <> 1) Then
                                    'replace shift-returns with single tab
                                    xText = fldP.Code.Text
                                    xText = xSubstitute(xText, vbTab & New String(Chr(11), 2), vbTab)
                                    xText = xSubstitute(xText, vbTab & vbVerticalTab, vbTab)
                                Else
                                    'replace shift-returns with single shift-return
                                    xText = fldP.Code.Text
                                    xText = xSubstitute(xText, vbTab & New String(Chr(11), 2), vbVerticalTab)
                                    xText = xSubstitute(xText, vbTab & vbVerticalTab, vbVerticalTab)
                                End If
                                xText = xSubstitute(xText, vbTab & New String(Chr(32), 2), vbTab)
                                xText = xSubstitute(xText, vbTab & Chr(32), vbTab)
                                fldP.Code.Text = xText

                                '                   tag as dynamic
                                fldP.Code.InsertAfter(" " & mpTCCode_Dyn)
                            Else
                                '                   replace shift-returns with a space;
                                '                   this is no longer handled in bReworkTOC (7/1/03)
                                If bReplaceShiftReturns Or (iLevel <> 1) Then
                                    fldP.Code.Text = xSubstitute(fldP.Code.Text, New String(Chr(11), 2), Chr(32))
                                    fldP.Code.Text = xSubstitute(fldP.Code.Text, Chr(11), Chr(32))
                                Else
                                    'replace shift-returns with single shift-return
                                    fldP.Code.Text = xSubstitute(fldP.Code.Text, New String(Chr(11), 2), Chr(11))
                                End If

                                '                   tag as partially dynamic
                                fldP.Code.InsertAfter(" " & mpTCCode_PDyn)
                            End If
                        End If
                    End If
labNextfield:
                Next fldP

                '   restore original view
                CurWordApp.ActiveWindow.View.Type = iView

            Finally
                If bScreenUpdating Then _
                    CurWordApp.ScreenUpdating = True
                CurWordApp.StatusBar = ""
            End Try
        End Function

        Friend Shared Function RestoreMPTCCodes() As Long
            'fills tc codes in doc with formatted
            'text to beginning of para- adds the
            'appropriate codes for restoring them
            'in the future
            Const mpTCPrefix As String = " TC "

            Dim fldP As Word.Field
            Dim rngCode As Word.Range
            Dim bIsTC As Boolean
            Dim bIsTCStatic As Boolean
            Dim bIsTCPDyn As Boolean
            Dim bIsTCDyn As Boolean
            Dim rngHeading As Word.Range
            Dim iLevel As Integer
            Dim lCodes As Long
            Dim l As Long

            Try
                RestoreMPTCCodes = 0
                CurWordApp.ScreenUpdating = False

                '   cycle through tc codes
                lCodes = CurWordApp.ActiveDocument.Fields.Count
                For Each fldP In CurWordApp.ActiveDocument.Fields
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar =
                            "R�initialisation Codes TM: " &
                                Format(l / lCodes, "0%")
                    Else
                        CurWordApp.StatusBar =
                            "Restoring TC Codes: " &
                                Format(l / lCodes, "0%")
                    End If
                    l = l + 1
                    bIsTCDyn = False
                    bIsTCPDyn = False
                    bIsTCStatic = False
                    iLevel = 0

                    rngCode = fldP.Code
                    bIsTC = UCase(Strings.Left(rngCode.Text, 4)) = " " & g_xTCPrefix & " "
                    If bIsTC Then
                        '           get type of tc code
                        bIsTCDyn = (InStr(UCase(fldP.Code.Text),
                            UCase(mpTCCode_Dyn)) > 0)
                        If Not bIsTCDyn Then
                            bIsTCPDyn = (InStr(UCase(fldP.Code.Text),
                                UCase(mpTCCode_PDyn)) > 0)
                        End If
                    End If

                    If bIsTCPDyn Then
                        '           partial dynamic code
                        iLevel = iGetPDynLevel(rngCode)
                        '           insert code with level only
                        fldP.Code.Text = " " & g_xTCPrefix & " \l """ & iLevel & Chr(34)
                    ElseIf bIsTCDyn Then
                        '           insert dynamic (empty) tc code
                        fldP.Code.Text = " " & g_xTCPrefix & " "
                    End If
                Next fldP
            Finally
                CurWordApp.ScreenUpdating = True
                CurWordApp.StatusBar = ""
            End Try
        End Function

        Friend Shared Function iGetPDynLevel(rngCode As Word.Range) As Integer
            'returns level of a partially dynamic code -
            'prompts if none can be found
            Dim lPos As Long
            Dim iLevel As Integer

            '   search for level switch
            'GLOG 5453 - convert to lower case
            lPos = InStr(LCase(rngCode.Text), "\l """)
            If lPos Then
                '       level integer should be 5 chars
                '       to right of start of switch
                On Error Resume Next
                iLevel = Mid(rngCode.Text, lPos + 4, 1)
                On Error GoTo 0
                If (iLevel < 1) Or (iLevel > 9) Then
                    While (iLevel < 1) Or (iLevel > 9)
                        iLevel = iGetParagraphLevel(rngCode, 0)
                    End While
                End If
            Else
                While (iLevel < 1) Or (iLevel > 9)
                    iLevel = iGetParagraphLevel(rngCode, 0)
                End While
            End If
            iGetPDynLevel = iLevel
        End Function

        Friend Shared Function iGetDynLevel(rngCode As Word.Range) As Integer
            Dim iLevel As Integer

            '   dynamic levels should have an
            '   outline level- prompt if none
            iLevel = rngCode.ParagraphFormat.OutlineLevel
            While (iLevel < 1) Or (iLevel > 9)
                iLevel = iGetParagraphLevel(rngCode, 0)
            End While
            iGetDynLevel = iLevel
        End Function

        Friend Shared Function ConvertHiddenParas() As Long
            Dim paraP As Word.Paragraph
            Dim fldP As Word.Field
            Dim rngParaMark As Word.Range
            Dim lNumParas As Long
            Dim l As Long
            Dim styHidden As Word.Style
            Dim styVisible As Word.Style
            Dim rngHeading As Word.Range
            Dim bShowHidden As Boolean
            Dim bBold As Boolean
            Dim bCaps As Boolean
            Dim bSmallCaps As Boolean
            Dim bItalic As Boolean
            Dim bUnderline As Boolean
            Dim xStyleMarker As String = ""
            Dim bStyIsConverted As Boolean

            ConvertHiddenParas = 0
            On Error GoTo ProcError
            CurWordApp.ScreenUpdating = False

            '   turn on hidden text
            With CurWordApp.ActiveWindow.View
                bShowHidden = .ShowHiddenText
                .ShowHiddenText = True
            End With

            lNumParas = CurWordApp.ActiveDocument.Paragraphs.Count

            '   set default toc scheme
            CurWordApp.ActiveDocument.Variables("cbxTOCScheme").Value = 2

            '   cycle through paras
            For Each paraP In CurWordApp.ActiveDocument.Paragraphs
                rngParaMark = paraP.Range.Characters.Last
                With rngParaMark
                    If .Font.Hidden Then
                        '               store styles and font attributes
                        styHidden = paraP.Style
                        If InStr(xStyleMarker, "*" & paraP.Style & "*") Then
                            bStyIsConverted = True
                        Else
                            bStyIsConverted = False
                            xStyleMarker = xStyleMarker & "*" & paraP.Style & "*"
                        End If

                        styVisible = rngParaMark.Next(WdUnits.wdParagraph).Style

                        With styHidden
                            With .Font
                                bBold = .Bold
                                bCaps = .AllCaps
                                bSmallCaps = .SmallCaps
                                bItalic = .Italic
                                bUnderline = (.Underline = WdUnderline.wdUnderlineSingle)
                            End With
                            .Font = styVisible.Font
                        End With

                        '               delete para
                        '                .Delete
                        .Text = ""
                        .Style = styHidden

                        '               insert tc code to the left of any trailing spaces
                        .MoveWhile(" ", WdConstants.wdBackward)

                        '               insert tc code
                        fldP = CurWordApp.ActiveDocument.Fields.Add(
                            rngParaMark, WdFieldType.wdFieldTOCEntry)
                        fldP.Code.Text = " " & g_xTCPrefix & " "

                        '               ensure that tc code is unformatted
                        rngGetField(fldP.Code).Font.Reset()

                        '               apply direct attributes to heading
                        .MoveStart(WdUnits.wdParagraph, -1)
                        With .Font
                            .Bold = bBold
                            .AllCaps = bCaps
                            .SmallCaps = bSmallCaps
                            .Italic = bItalic
                            .Underline = Abs(CInt(bUnderline))
                        End With

                        If .ListFormat.ListType <> WdListType.wdListNoNumbering Then
                            With .ListFormat
                                With .ListTemplate.ListLevels(.ListLevelNumber).Font
                                    .Bold = bBold
                                    .AllCaps = bCaps
                                    .SmallCaps = bSmallCaps
                                    .Italic = bItalic
                                    .Underline = Abs(CInt(bUnderline))
                                End With
                            End With
                        End If
                    End If
                End With
                l = l + 1
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar =
                        "Les marques de paragraphes ont �t� masqu�es et remplac�es par les codes TM TSG:  " &
                        Format(l / lNumParas, "0%")
                Else
                    CurWordApp.StatusBar =
                        "Converting hidden paragraph marks to TSG TC codes:  " &
                        Format(l / lNumParas, "0%")
                End If
            Next paraP

            CurWordApp.ActiveWindow.View.ShowHiddenText = bShowHidden

            With CurWordApp
                .ScreenUpdating = True
                .StatusBar = ""
            End With

            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                MsgBox("Les marques de paragraphes ont �t� masqu�es et remplac�es par les codes TM TSG. " &
                      vbCr & vbCr & "Nous recommandons de supprimer toutes Tables des mati�res avant d'ins�rer une TM TSG. Si vous cr�ez une TM TSG sans supprimer une TM existante, TSG remplacera la TM. Il n'y aura pas de nouvelle section TM avec TSG en-t�te et pied de page.",
                      vbInformation, "Num�rotation TSG")
            Else
                MsgBox("Hidden paragraph marks have been replaced with TSG TC codes. " &
                      vbCr & vbCr & "We recommend that you delete any " &
                      "existing Tables Of Contents before " &
                      vbCr & "inserting a TSG TOC.  Should " &
                      "you choose to create a TSG TOC " &
                      vbCr & "without first deleting an existing TOC, " &
                      "TSG will simply replace the TOC." &
                      vbCr & "It will not create a new TOC section with " &
                      "TSG headers and footers.", vbInformation, AppName)
            End If
            Exit Function

ProcError:
            ConvertHiddenParas = Err.Number
            Exit Function
        End Function

        Friend Shared Sub bResetTOCTabs(rngTOC As Word.Range)
            Dim paraTOC As Word.Paragraph
            Dim pfPrev As Word.ParagraphFormat
            Dim pfCur As Word.ParagraphFormat
            Dim rngP As Word.Range
            Dim xPara As String = ""
            Dim xTabFlag(8) As String
            Dim iNumTabs As Integer
            Dim iLevel As Integer
            Dim bShowAll As Boolean
            Dim bShowHidden As Boolean
            Dim iView As Integer
            Dim i As Integer
            Dim s As Single
            Dim sHPos As Single
            Dim xStyle As String
            Dim oStyle As Word.Style

            EchoOff()
            With CurWordApp.ActiveWindow.ActivePane.View
                bShowAll = .ShowAll
                bShowHidden = .ShowHiddenText
                iView = .Type
                .ShowAll = False
                .ShowHiddenText = False
                .Type = WdViewType.wdNormalView
            End With

            s = 36
            On Error Resume Next
            For i = 1 To 9
                With CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(i))
                    .ParagraphFormat.LeftIndent = s
                    .ParagraphFormat.FirstLineIndent = -36
                End With
                s = s + 36
            Next i
            On Error GoTo EndOfTOC
            For Each paraTOC In rngTOC.Paragraphs
                rngP = paraTOC.Range
                With rngP
                    pfCur = .ParagraphFormat

                    xPara = .Text

                    '           skip empty paras
                    If Len(xPara) < 2 Then
                        If (paraTOC.Range.Start = rngTOC.Paragraphs.Last.Range.Start) And
                                (paraTOC.Range.End = rngTOC.Paragraphs.Last.Range.End) Then
                            Exit For
                        Else
                            GoTo NextPara
                        End If
                    End If

                    '           get level
                    On Error GoTo EndOfTOC
                    oStyle = pfCur.Style
                    xStyle = oStyle.NameLocal
                    iLevel = iGetTOCStyleLevel(xStyle)
                    On Error GoTo 0

                    iNumTabs = lCountChrs(xPara, vbTab)
                    '           adjust hanging indent of style based on
                    '           first representative entry for this level
                    '           determine if it's a numbered entry and
                    '           whether level has already been checked
                    If iNumTabs > 1 Then
                        If g_bUseNewTabAdjustmentRules Then
                            'new option checks every entry for tightness and adjusts only as
                            'much as needed, rather than taking first entry as representative
                            'and leaving a cushion for potentially longer entries
                            sHPos = MeasureHeading(rngP, pfCur, xPara)

                            'adjust flag if longest entry found for level
                            If xTabFlag(iLevel - 1) = "" Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            End If
                        ElseIf xTabFlag(iLevel - 1) = "" Then
                            'set flag
                            xTabFlag(iLevel - 1) = "X"
                            AdjustStyleIndents(rngP, pfCur, iLevel, xPara)
                        End If
                    End If

                End With
NextPara:
                '       store current para format for
                '       next iteration through loop
                pfPrev = pfCur
                rngP = Nothing
                paraTOC = Nothing
            Next paraTOC

EndOfTOC:
            'adjust indents as necessary
            If g_bUseNewTabAdjustmentRules Then
                For i = 0 To 8
                    If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                        AdjustStyleIndentsNew(i + 1, CSng(xTabFlag(i)), False)
                    End If
                Next i
            End If

            '   reset environment
            With CurWordApp.ActiveWindow.ActivePane.View
                .ShowAll = bShowAll
                .ShowHiddenText = bShowHidden
                .Type = iView
            End With
            EchoOn()

        End Sub

        Private Sub TagAsTCEntry_NonDyn(rngPara As Word.Range,
                                            lPos As Long)
            Dim rngP As Word.Range
            Dim lStart As Long
            Dim iTags As Integer

            With rngPara
                'get Word XML adjusted paragraph start
                If g_bXMLSupport Then
                    lStart = GetTagSafeParagraphStart(rngPara, iTags)
                    '9.9.4008/9.9.4009 - only check for ccs if not already adjusted for tags
                    If iTags = 0 Then _
                        lStart = GetCCSafeParagraphStart(rngPara)
                    .SetRange(lStart, .End)
                End If

                '       add 'tc marked' tag directly before the tc code
                rngP = .Duplicate
                With rngP
                    .StartOf()
                    .Move(WdUnits.wdCharacter, lPos - 1)
                    .InsertAfter(mpTCMarked)
                End With
            End With
        End Sub

        Private Shared Function xConditionalScheme(rngPara As Word.Range) As String
            'returns name of MP scheme of which this is a bracketed variant
            Dim xStyle As String = ""
            Dim iPos As Integer
            Dim xLT As String = ""

            xConditionalScheme = ""
            With rngPara.Paragraphs(1).Range
                With .ListFormat
                    If .ListType <> WdListType.wdListOutlineNumbering And
                            .ListType <> WdListType.wdListSimpleNumbering Then
                        Exit Function
                    End If

                    If Strings.Left(.ListString, 1) <> "[" Then
                        Exit Function
                    End If
                End With

                'GLOG 2847
                On Error Resume Next
                xStyle = .Style
                On Error GoTo 0

                If Strings.Left(xStyle, 9) = "Heading_L" Then
                    xLT = "HeadingStyles"
                ElseIf bIsHeadingStyle(xStyle) Then
                    xLT = xHeadingScheme()
                Else
                    iPos = InStr(xStyle, "_L")
                    If iPos Then _
                        xLT = "zzmp" & Strings.Left(xStyle, iPos - 1)
                End If

                If bListTemplateExists(xLT) Then
                    If xLT = "HeadingStyles" Then
                        xConditionalScheme = "Heading"
                    Else
                        xConditionalScheme = xLT
                    End If
                End If
            End With
        End Function

        Friend Shared Function bDeleteMPTOC() As Boolean
            Dim rngTOC As Word.Range

            bDeleteMPTOC = False

            With CurWordApp.ActiveDocument
                If .Bookmarks.Exists("mpTableOfContents") Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        lRet = MsgBox("�tes-vous s�re de vouloir supprimer la table des mati�res?", vbQuestion + vbYesNo, "Num�rotation TSG")
                    Else
                        lRet = MsgBox("Are you sure you want to delete the existing " &
                            "Table of Contents?", vbQuestion + vbYesNo, "TSG Numbering")
                    End If
                    If lRet = vbNo Then _
                        Exit Function

                    rngTOC = .Bookmarks("mpTableOfContents").Range
                    If Asc(rngTOC.Characters.Last.Text) = 12 Then
                        rngTOC.MoveEnd(WdUnits.wdCharacter, -1)
                    End If
                    If (rngTOC.Start = .Range.Start) And (rngTOC.Text <> mpNoEntriesWarning) And
                            (rngTOC.Text <> mpNoEntriesWarningFrench) Then 'GLOG 8785 (dm)
                        While (rngTOC.Paragraphs.Count > 1) And
                                (Not bIsTOCStyle(rngTOC.Paragraphs(1).Style.NameLocal)) 'GLOG 8785 (dm)
                            rngTOC.MoveStart(WdUnits.wdParagraph, 1)
                            If rngTOC.End > .Bookmarks("mpTableOfContents").Range.End Then
                                rngTOC = .Bookmarks("mpTableOfContents").Range
                                If Asc(rngTOC.Characters.Last.Text) = 12 Then
                                    rngTOC.MoveEnd(WdUnits.wdCharacter, -1)
                                End If
                                rngTOC.Collapse(WdCollapseDirection.wdCollapseEnd)
                            End If
                        End While
                        If Asc(rngTOC.Characters.First.Text) = 12 Then
                            rngTOC.MoveStart(WdUnits.wdCharacter, 1)
                        End If
                    End If

                    If Len(rngTOC.Text) > 1 Then
                        With rngTOC
                            On Error Resume Next
                            '                    If .Next(wdUnits.wdParagraph).Text = _
                            '                            mpEndOfTOCWarning & vbCr Then
                            '                        .MoveEnd wdUnits.wdParagraph
                            '                    End If

                            '                   attempting to delete a native TOC field when
                            '                   when there's preceding text in the range
                            '                   can cause an error - unlink as a precaution
                            .Fields.Unlink()
                            On Error GoTo 0

                            '                   leave final paragraph mark
                            If .Characters.Last.Text = vbCr Then _
                                .MoveEnd(WdUnits.wdCharacter, -1)

                            '                   delete TOC
                            .Delete()

                            '                   format paragraph as normal
                            If .Style.NameLocal <> CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleNormal).NameLocal Then _
                                .Style = WdBuiltinStyle.wdStyleNormal
                        End With
                    End If

                    bDeleteMPTOC = True
                Else
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucune table des mati�res TSG dans ce document ou signet TM manquant.", vbInformation,
                            "Num�rotation TSG")
                    Else
                        MsgBox("There is no TSG Table of Contents in this " &
                            "document or the TOC bookmark is missing.", vbInformation,
                            "TSG Numbering")
                    End If
                End If
            End With
        End Function

        Friend Shared Function rngHeaderBookmark(rngHeader As Word.Range,
                                    xPrefix As String) As Word.Range
            'returns "Table of Contents" or "Page" range for formatting
            Dim bkmk As Word.Bookmark
            Dim xName As String

            '   at Murtaugh only, after saving in ProLaw, the main header bookmark
            '   can be part of the collection, but not accessible, even in the
            '   Word interface.  This causes bkmk.name below to generate error 5825,
            '   "object has been deleted".  Strangely, this issue doesn't appear to cause
            '   problems elsewhere in TSG -
            '   9.9.3004 - replaced this workaround with error handling below - I found
            '   that when rngHeader is the primary header story, the main header bookmark
            '   (e.g. zzmpTOCHeader_Primary) is seen by Word as in the first page header story -
            '   this resulted in rngHeader.Bookmarks.Count = 1 and the primary header not getting
            '   formatted correctly
            '    If rngHeader.Bookmarks.Count < 2 Then Exit Function

            For Each bkmk In rngHeader.Bookmarks
                On Error Resume Next
                xName = bkmk.Name
                On Error GoTo 0
                If InStr(xName, xPrefix) Then
                    rngHeaderBookmark = bkmk.Range
                    bkmk.Delete()
                    Exit Function
                End If
            Next bkmk
        End Function

        Friend Shared Function GetCommonAbbrevs() As String()
            Dim xDatFile As String
            Dim TextLine As String
            Dim xList As String()
            Dim oReader As StreamReader

            On Error GoTo Title_ERROR

            ReDim xList(0)
            xDatFile = GetAppPath() & "\tsgAbbreviations.dat"
            oReader = New StreamReader(xDatFile)
            Do While Not oReader.EndOfStream
                TextLine = oReader.ReadLine()
                If TextLine <> "" Then
                    ''           trim periods
                    '            If Right(TextLine, 1) = "." Then _
                    '                TextLine = Left(TextLine, Len(TextLine) - 1)
                    xList(UBound(xList)) = TextLine
                End If
                ReDim Preserve xList(UBound(xList) + 1)
            Loop
            If xList(UBound(xList)) = "" Then ReDim Preserve xList(UBound(xList) - 1)
Title_ERROR:
            oReader.Close()    ' Close file.
            GetCommonAbbrevs = xList
        End Function

        Friend Shared Function bIsAbbreviation(xTest As String) As Boolean
            'Returns TRUE if xTest ends with a common abbreviation
            'which is not to be confused with a sentence delimiter
            Dim i As Integer
            Dim iLen As Integer
            Dim xPrev As String
            For i = 0 To UBound(g_xAbbreviations)
                iLen = Len(g_xAbbreviations(i))
                If Len(xTest) > iLen Then
                    '           compare last characters of segment if previous character is
                    '           a space or pipe, i.e. if they're not part of a longer word
                    xPrev = Mid(xTest, (Len(xTest) - iLen), 1)
                    If xPrev = " " Or xPrev = "|" Then
                        If UCase(Strings.Right(xTest, iLen)) = UCase(g_xAbbreviations(i)) Then
                            '                   this is a listed abbreviation - look
                            '                   for next potential sentence
                            bIsAbbreviation = True
                            Exit Function
                        End If
                    End If
                ElseIf Len(xTest) = iLen Then
                    '           compare entire segment
                    If UCase(xTest) = UCase(g_xAbbreviations(i)) Then
                        '               this is a listed abbreviation - look
                        '               for next potential sentence
                        bIsAbbreviation = True
                        Exit Function
                    End If
                End If
            Next i
            bIsAbbreviation = False

        End Function

        Friend Shared Sub SetScheduleLevels(iMin As Integer, xTOC9Style As String)
            'ensure that ouline level is in selected range;
            'store existing settings in an array
            Dim i As Integer
            Dim iStyles As Integer
            Dim stySchedule As Word.Style

            iStyles = UBound(g_xScheduleStyles)
            ReDim g_xScheduleLevels(0 To iStyles, 0 To 1)

            For i = 0 To iStyles
                On Error Resume Next
                stySchedule = CurWordApp.ActiveDocument.Styles(g_xScheduleStyles(i))
                On Error GoTo 0

                If Not stySchedule Is Nothing Then
                    g_xScheduleLevels(i, 0) = g_xScheduleStyles(i)
                    With stySchedule.ParagraphFormat
                        g_xScheduleLevels(i, 1) = .OutlineLevel
                        If (g_xScheduleStyles(i) = xTOC9Style) Then
                            '                   ensure inclusion regardless of range
                            .OutlineLevel = iMin
                        ElseIf (.OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText) Then
                            '                   treat as level one
                            .OutlineLevel = WdOutlineLevel.wdOutlineLevel1
                        End If
                    End With
                End If
            Next i
        End Sub

        Friend Shared Sub RestoreScheduleLevels()
            Dim i As Integer
            Dim stySchedule As Word.Style

            For i = 0 To UBound(g_xScheduleLevels)
                If g_xScheduleLevels(i, 1) <> "" Then
                    On Error Resume Next
                    stySchedule = CurWordApp.ActiveDocument.Styles(g_xScheduleLevels(i, 0))
                    On Error GoTo 0

                    If Not stySchedule Is Nothing Then
                        stySchedule.ParagraphFormat.OutlineLevel = Val(g_xScheduleLevels(i, 1))
                    End If
                End If
            Next i
        End Sub

        Friend Shared Function bIsScheduleStyle(xStyle As String) As Boolean
            Dim i As Integer

            If xStyle <> "" Then
                For i = 0 To UBound(g_xScheduleStyles)
                    If g_xScheduleStyles(i) = xStyle Then
                        bIsScheduleStyle = True
                        Exit Function
                    End If
                Next i
            End If
            bIsScheduleStyle = False
        End Function

        Friend Shared Function bGoToMPTOC() As Boolean

            With CurWordApp.ActiveDocument
                If .Bookmarks.Exists("mpTableOfContents") Then
                    CurWordApp.Selection.GoTo(WdGoToItem.wdGoToBookmark, Name:="mpTableOfContents")
                    CurWordApp.Selection.Collapse(WdCollapseDirection.wdCollapseStart)
                    bGoToMPTOC = True
                Else
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucune table des mati�res TSG dans ce document ou signet TM manquant.", vbInformation,
                            "Num�rotation TSG")
                    Else
                        MsgBox("There is no TSG Table of Contents in this " &
                            "document or the TOC bookmark is missing.", vbInformation,
                            "TSG Numbering")
                    End If
                    bGoToMPTOC = False
                End If
            End With
        End Function

        Private Shared Function xGetDesignatedStyles(ByVal xInclusions As String,
                                              ByVal xLevel9Style As String) As String
            Dim xTemp As String
            Dim iPos As Integer
            Dim xStyleList As String = ""
            Dim xStyle As String = ""
            Dim iLevel As Integer

            xTemp = xInclusions
            If Len(xTemp) Then
                '       trim trailing and preceding commas
                xTemp = xTrimTrailingChrs(xTemp, ",", True, False)
                iPos = InStr(xTemp, ",")
                While iPos
                    xStyle = Strings.Left(xTemp, iPos - 1)
                    If xStyle <> xLevel9Style Then
                        iLevel = CurWordApp.ActiveDocument.Styles(xStyle).ParagraphFormat.OutlineLevel
                    Else
                        '9.9.5001 - for field TOC, this option is implemented in field itself
                        iLevel = 9
                    End If
                    xStyleList = xStyleList & xStyle & "," & iLevel & ","
                    xTemp = Mid(xTemp, iPos + 1)
                    iPos = InStr(xTemp, ",")
                End While
            End If
            xGetDesignatedStyles = xStyleList
        End Function

        Private Shared Sub CleanUpTOCField(ByVal rngTOC As Word.Range,
                                    ByVal bReplaceShiftReturns As Boolean)
            'prepare TOC field for potential native update
            Dim iPos As Integer
            Dim rngCode As Word.Range

            '   if no real entries, field will have been deleted in bReworkTOC
            If rngTOC.Fields.Count = 0 Then _
                Exit Sub

            'GLOG 5456 - remove chr(160) inserted by
            'underline to longest line function
            If InStr(rngTOC.Text, Chr(32) & Chr(160)) > 0 Then
                ReplaceText(rngTOC, "^32" & ChrW(&HA0) & "{1,}", " ", True)
            End If

            CurWordApp.ActiveWindow.View.ShowFieldCodes = True

            rngCode = rngTOC.Fields(1).Code.Duplicate

            If bReplaceShiftReturns Then
                'remove \x switch
                With rngCode
                    iPos = InStr(UCase(.Text), "\X")
                    If iPos Then
                        .MoveStart(WdUnits.wdCharacter, iPos - 1)
                        .Collapse(WdCollapseDirection.wdCollapseStart)
                        .Delete(WdUnits.wdCharacter, 3)
                    End If
                    iPos = 0
                End With
            Else
                'add \n switch to exclude page # - style needs to remain centered and
                'there's no way to make centered w/page # look right after native update
                With rngCode
                    If InStr(UCase(.Text), "\N") = 0 Then
                        iPos = InStrRev(.Text, "\")
                        .MoveStart(WdUnits.wdCharacter, iPos)
                        .Collapse(WdCollapseDirection.wdCollapseStart)
                        .InsertAfter("n1-1 \")
                    End If
                End With
            End If

            CurWordApp.ActiveWindow.View.ShowFieldCodes = False
        End Sub

        Friend Shared Sub ResetRightTabStop(secTOC As Word.Section)
            Dim sRightTabPos As Single
            Dim xTOCStyle As String = ""
            Dim i As Integer
            Dim styTOC As Word.Style
            Dim styTOA As Word.Style
            Dim lColWidth As Single

            '   get proper position of page number tab stop
            With secTOC.PageSetup
                If .TextColumns.Count = 1 Then
                    sRightTabPos = .PageWidth - .LeftMargin -
                        .RightMargin - .Gutter - CurWordApp.InchesToPoints(0.05)
                ElseIf .TextColumns.Count = 2 Then
                    lColWidth = mpMin(.TextColumns(1).Width,
                        .TextColumns(2).Width)
                    sRightTabPos = lColWidth - CurWordApp.InchesToPoints(0.01)
                Else
                    sRightTabPos = .TextColumns(1).Width - CurWordApp.InchesToPoints(0.01)
                End If
            End With

            For i = 1 To 9
                '       modify the last tab position to proper position
                xTOCStyle = xTranslateTOCStyle(i)
                styTOC = CurWordApp.ActiveDocument.Styles(xTOCStyle)
                With styTOC.ParagraphFormat.TabStops
                    .Item(.Count).Position = sRightTabPos
                End With
            Next i

            '   Also set TOA Page Number tab to match TOC
            styTOA = CurWordApp.ActiveDocument.Styles(WdBuiltinStyle.wdStyleTableOfAuthorities)
            With styTOA.ParagraphFormat.TabStops
                If .Count Then
                    .Item(.Count).Position = sRightTabPos
                End If
            End With
        End Sub

        Friend Shared Function iGetTOCStyleLevel(xStyle As String) As Integer
            'returns the level of the specified TOC style
            With CurWordApp.ActiveDocument
                Select Case xStyle
                    Case .Styles(WdBuiltinStyle.wdStyleTOC1).NameLocal
                        iGetTOCStyleLevel = 1
                    Case .Styles(WdBuiltinStyle.wdStyleTOC2).NameLocal
                        iGetTOCStyleLevel = 2
                    Case .Styles(WdBuiltinStyle.wdStyleTOC3).NameLocal
                        iGetTOCStyleLevel = 3
                    Case .Styles(WdBuiltinStyle.wdStyleTOC4).NameLocal
                        iGetTOCStyleLevel = 4
                    Case .Styles(WdBuiltinStyle.wdStyleTOC5).NameLocal
                        iGetTOCStyleLevel = 5
                    Case .Styles(WdBuiltinStyle.wdStyleTOC6).NameLocal
                        iGetTOCStyleLevel = 6
                    Case .Styles(WdBuiltinStyle.wdStyleTOC7).NameLocal
                        iGetTOCStyleLevel = 7
                    Case .Styles(WdBuiltinStyle.wdStyleTOC8).NameLocal
                        iGetTOCStyleLevel = 8
                    Case .Styles(WdBuiltinStyle.wdStyleTOC9).NameLocal
                        iGetTOCStyleLevel = 9
                    Case Else
                        iGetTOCStyleLevel = 0
                End Select
            End With
        End Function

        Private Shared Function InRowAtStartOfDoc() As Boolean
            If CurWordApp.Selection.Information(WdInformation.wdWithInTable) Then
                InRowAtStartOfDoc = (CurWordApp.Selection.Rows(1).Range.Start = 0)
            Else
                InRowAtStartOfDoc = False
            End If
        End Function

        Friend Shared Sub ReworkTOCField(rngTOC As Word.Range,
                            xScheme As String,
                            xExclusions As String,
                            bIncludeStyles As Boolean,
                            bIncludeTCEntries As Boolean,
                            udtTOCFormat As TOCFormat,
                            xTOC9Style As String,
                            bDesignate As Boolean,
                            bApplyManualFormats As Boolean)
            'cycles through TOC paras, cleaning
            'up in any number of ways

            Dim i As Integer
            Dim rngP As Word.Range
            Dim iLevel As Integer
            Dim bIsCentered As Boolean
            Dim paraTOC As Word.Paragraph
            Dim xTabFlag(8) As String
            Dim iNumTabs As Integer
            Dim l As Long
            Dim lNumParas As Long
            Dim pfPrev As Word.ParagraphFormat
            Dim pfCur As Word.ParagraphFormat
            Dim xPara As String = ""
            Dim sProgress As Single
            Dim sTab As Single
            Dim rngHeading As Word.Range
            Dim xStyle As String = ""
            Dim xStylePrev As String = ""
            Dim iDisplay As Integer
            Dim bPreserveLineBreaks As Boolean
            Dim sHPos As Single
            Dim bIncludePageNumber As Boolean

            lNumParas = rngTOC.Paragraphs.Count

            '   substitute tabs for soft returns unless ini says otherwise for this TOC scheme -
            '   before we added bReworkTOCField in 9.9.5, and switched from never preserving shift-
            '   returns for "TOC as Field" to always preserving them (as we always have for the text
            '   TOC), this ini key was implemented via the native switch - it was a workaround added
            '   solely for Torys in 2008 to account for even earlier custom code to accomplish a
            '   non-centered level with shift-returns in one of their TOC schemes
            For i = 0 To UBound(g_vPreserveLineBreaks)
                If xTranslateTOCSchemeDisplayName(CStr(g_vPreserveLineBreaks(i))) = xScheme Then
                    bPreserveLineBreaks = True
                    Exit For
                End If
            Next i
            If Not bPreserveLineBreaks Then
                ReplaceText(rngTOC, New String(Chr(11), 2), vbTab)
                ReplaceText(rngTOC, Chr(11), vbTab)
            End If

            'GLOG 5495 - remove spaces before tabs - this can be an issue when
            'there's a bounding object before the number's trailing character(s)
            ReplaceText(rngTOC, " " & vbTab, vbTab)

            'GLOG 5174 - replace shift-return substitutes with spaces
            ReplaceText(rngTOC, "zzmpShiftReturn", " ")

            '---cycle through TOC paragraphs
            For Each paraTOC In rngTOC.Paragraphs
                rngP = paraTOC.Range
                With rngP
                    'exit if we're no longer in the body of the TOC -
                    'this line is necessary because the range will include the paragraph
                    'following the field, which is problematic when there's subsequent text
                    'in the section - this does not occur with the text TOC
                    If .End > rngTOC.End Then
                        Exit For
                    End If

                    pfCur = .ParagraphFormat
                    xPara = .Text

                    '           skip empty paras
                    If Len(xPara) < 2 Then
                        If (paraTOC.Range.Start = rngTOC.Paragraphs.Last.Range.Start) And
                                (paraTOC.Range.End = rngTOC.Paragraphs.Last.Range.End) Then
                            Exit For
                        Else
                            GoTo NextPara
                        End If
                    End If

                    '           get level
                    xStyle = pfCur.Style.NameLocal
                    iLevel = iGetTOCStyleLevel(xStyle)

                    '           get alignment
                    bIsCentered = pfCur.Alignment

                    '           get include page number setting
                    bIncludePageNumber = udtTOCFormat.IncludePageNumber(iLevel - 1)

                    '           adjust hanging indent of style based on
                    '           first representative entry for this level
                    '           determine if it's a numbered entry and
                    '           whether level has already been checked;
                    iNumTabs = lCountChrs(xPara, vbTab)
                    If iNumTabs > 1 Then
                        'remove extra tabs
                        If iNumTabs > Abs(CInt(bIncludePageNumber)) + 1 Then
                            With rngP
                                'move after first tab pos
                                .MoveStartUntil(vbTab)
                                .MoveStart(WdUnits.wdCharacter)

                                'replace all extra tabs with a space
                                While lCountChrs(.Text, vbTab) > Abs(CInt(bIncludePageNumber))
                                    .MoveStartUntil(vbTab)
                                    .Characters(1).Text = " "
                                End While
                                .Expand(WdUnits.wdParagraph)
                            End With
                        End If

                        If g_bUseNewTabAdjustmentRules Then
                            'new option checks every entry for tightness and adjusts only as
                            'much as needed, rather than taking first entry as representative
                            'and leaving a cushion for potentially longer entries
                            rngHeading = rngP.Duplicate
                            sHPos = MeasureHeading(rngHeading, pfCur, xPara)

                            'adjust flag if longest entry found for level
                            If xTabFlag(iLevel - 1) = "" Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            ElseIf sHPos > CSng(xTabFlag(iLevel - 1)) Then
                                xTabFlag(iLevel - 1) = CStr(sHPos)
                            End If
                        ElseIf xTabFlag(iLevel - 1) = "" Then
                            'set flag
                            xTabFlag(iLevel - 1) = "X"
                            AdjustStyleIndents(rngP, pfCur, iLevel, xPara)
                        End If
                    ElseIf (iNumTabs = 1) And Not bIsCentered Then
                        'in Word 2007, EchoOff/ScreenUpdating=false does not prevent
                        'repeated selections from producing a visual "creeping" effect, and
                        'Range.Information has not been problematic since Word 2000
                        Dim oRange As Word.Range
                        oRange = .Characters.First
                        With oRange
                            .EndOf(WdUnits.wdParagraph)
                            .Move(WdUnits.wdCharacter, -1)
                            'style is used, since tabstops method of selection
                            'will fail when document's default tab stop is 0"
                            With CurWordApp.ActiveDocument.Styles(xStyle).ParagraphFormat.TabStops
                                If .Count > 0 Then _
                                        sTab = .Item(.Count).Position
                            End With
                            If .Information(WdInformation.wdHorizontalPositionRelativeToTextBoundary) <
                                        sTab - CurWordApp.InchesToPoints(0.25) Then
                                'measurement is not accurate to the point, so allow 1/4"
                                'leeway - this won't be a problem because hanging indent
                                'or preceding tab stop will never be set this close to right tab
                                .MoveUntil(vbTab, WdConstants.wdBackward)
                                .InsertAfter(vbTab)
                            End If
                        End With
                    End If

                    '           format centered paras
                    If bIsCentered Then
                        FormatCenteredLevel(rngP, pfCur, True)
                    End If

                    '           format para to have or not have page number
                    FormatPageNumber(rngP, bIsCentered, pfCur, bIncludePageNumber, True)

                    '           if first para of new level,
                    '           adjust space after previous paragraph -
                    '           pfPrev is set below, so it will be
                    '           nothing the first time around
                    If Not (pfPrev Is Nothing) Then
                        If (xStyle <> xStylePrev) Then
                            If (udtTOCFormat.SpaceBefore(iLevel - 1) <> "") And
                                    (udtTOCFormat.SpaceBefore(iLevel - 1) <> "mpNA") Then
                                pfPrev.SpaceAfter = udtTOCFormat.SpaceBefore(iLevel - 1)
                            End If
                        End If
                    End If

                    '           call any custom code
                    lRet = oCustTOC.lInTOCRework(rngP,
                                                 xScheme,
                                                 xExclusions,
                                                 bIncludeStyles,
                                                 bIncludeTCEntries)
                End With
NextPara:
                '       free up resources
                If Not g_bPreserveUndoListTOC Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '       store current para format for
                '       next iteration through loop
                xStylePrev = xStyle
                pfPrev = pfCur
                rngP = Nothing
                paraTOC = Nothing
                l = l + 1
                If ((l / lNumParas) * 100) > (iDisplay + 10) Then
                    iDisplay = iDisplay + 10
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Refaire TM: " & iDisplay & "%"
                    Else
                        CurWordApp.StatusBar = "Reworking TOC: " & iDisplay & "%"
                    End If
                    sProgress = 48 + (12 * (l / lNumParas))
                    g_oStatus.UpdateProgress(sProgress)
                End If
                'OutputDebugString "bReworkTOC - Paragraph l=" & l
            Next paraTOC

            'adjust indents as necessary
            If g_bUseNewTabAdjustmentRules Then
                For i = 0 To 8
                    If xTabFlag(i) <> "" And xTabFlag(i) <> "0" Then
                        AdjustStyleIndentsNew(i + 1, CSng(xTabFlag(i)), True)
                    End If
                Next i
            End If

            If bApplyManualFormats Then
                '       remove underlining of any underlined tabs
                With rngTOC.Find
                    .ClearFormatting()
                    .Format = True
                    .Font.Underline = WdUnderline.wdUnderlineSingle
                    .Text = vbTab
                    With .Replacement.Font
                        .Underline = WdUnderline.wdUnderlineNone
                        .Hidden = False
                    End With
                    .Execute(Replace:=WdReplace.wdReplaceAll)
                End With
            End If

            '   ensure no space before first entry
            rngTOC.Paragraphs(1).SpaceBefore = 0
        End Sub

        Friend Shared Function InsertStyleSeparator(Optional ByVal bFormat As Boolean = False) As Long
            'inserts style separator at cursor
            Dim bShowAll As Boolean
            Dim xStyle As String = ""
            Dim fldExisting As Word.Field
            Dim oStyle As Word.Style
            Dim xNewStyle As String = ""
            Dim oRange As Word.Range
            Dim xScheme As String = ""
            Dim iLevel As Integer
            Dim oLT As Word.ListTemplate
            Dim xStyleRoot As String = ""
            Dim bWarn As Boolean
            Dim oPara As Word.Paragraph
            Dim lPos As Long
            Dim bIsMPLT As Boolean

            Try
                InsertStyleSeparator = 0

                'ensure that selection is insertion
                If CurWordApp.Selection.Type <> WdSelectionType.wdSelectionIP Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Ne pas s�lectionner le texte avant d'effectuer cette proc�dure. Placer le curseur � la fin du texte qui doit �tre marqu� comme TM et r�essayer.",
                            vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox("Do not select text before running this procedure.  " &
                               "Position cursor after the text to be marked " &
                               "for TOC and try again.", vbExclamation, AppName)
                    End If
                    Exit Function
                End If

                'warn if no outline level
                Try
                    oStyle = CurWordApp.Selection.Paragraphs(1).Style
                Catch
                End Try
                If Not oStyle Is Nothing Then
                    '4/24/13 - don't warn about non-outline level mp numbering styles
                    oLT = CurWordApp.Selection.Paragraphs(1).Range.ListFormat.ListTemplate
                    If Not oLT Is Nothing Then
                        bIsMPLT = bIsMPListTemplate(oLT)
                        oLT = Nothing
                    End If
                    bWarn = ((oStyle.ParagraphFormat.OutlineLevel = WdOutlineLevel.wdOutlineLevelBodyText) And
                        Not bIsScheduleStyle(oStyle.NameLocal) And Not bIsMPLT)
                End If

                'don't warn if paragraph already contains a style separator -
                'user should be able to freely move it
                If bWarn Then
                    'check selected paragraph
                    bWarn = Not CurWordApp.Selection.Paragraphs(1).IsStyleSeparator
                    If bWarn Then
                        'check trailing paragraph
                        Try
                            oRange = CurWordApp.Selection.Previous(WdUnits.wdParagraph)
                        Catch
                        End Try
                        If Not oRange Is Nothing Then
                            bWarn = Not oRange.Paragraphs(1).IsStyleSeparator
                        End If
                    End If
                End If

                If bWarn Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        lRet = MsgBox("L'insertion des s�parateurs de style ne marquera pas le texte comme TM car le style appliqu� n'a pas de niveau hi�rarchique. Utiliser l'une de ces m�thodes pour permettre d'ajouter le texte dans la TM:" &
                            vbCr & vbCr & "1. Marquer avec des codes TM au lieu des s�parateurs de style." & vbCr &
                            "2. Continuer avec l'insertion de s�parateur de style et changer ou modifier le style qui pr�c�de les s�parateurs afin d'obtenir un niveau hi�rarchique." &
                            vbCr & vbCr & "Continuer avec l'insertion de s�parateurs de style?", vbOKCancel, "Num�rotation TSG")
                    Else
                        lRet = MsgBox("Inserting a style separator will not mark this text for TOC because " &
                            "the style applied to it does not have an outline level.  " &
                            "There are two ways to prepare this text for inclusion in the TOC:" &
                            vbCr & vbCr & "1. Mark with a TC code instead of a style separator." & vbCr &
                            "2. Continue with style separator insertion and change or edit the style " &
                            "preceding the separator to have an outline level." &
                            vbCr & vbCr & "Continue with style separator insertion?", vbOKCancel, AppName)
                    End If

                    If lRet = vbCancel Then _
                        Exit Function
                End If

                'move to end of inline tag or content control
                lPos = GetPositionAfterInlineTag(CurWordApp.Selection.Range)
                If lPos = 0 Then _
                    lPos = GetPositionAfterInlineCC(CurWordApp.Selection.Range)
                If lPos > 0 Then _
                    CurWordApp.Selection.SetRange(lPos, lPos)

                'remove existing style separator
                bRemoveStyleSeparators(CurWordApp.Selection.Range)

                'remove existing TC code
                bMarkForTOCRemove(CurWordApp.Selection.Range)

                'force show all
                With CurWordApp.ActiveWindow.View
                    bShowAll = .ShowAll
                    .ShowAll = True
                End With

                'insert style separator
                CurWordApp.Selection.InsertParagraphAfter()
                CurWordApp.Selection.InsertStyleSeparator()
                With CurWordApp.Selection.Previous(WdUnits.wdCharacter)
                    If .Text = " " Then .Delete()
                End With

                'restyle if mp numbering
                oRange = CurWordApp.Selection.Previous(WdUnits.wdParagraph)
                oLT = oRange.ListFormat.ListTemplate
                If Not oLT Is Nothing Then
                    If bIsMPListTemplate(oLT) Then
                        xScheme = xGetLTRoot(oLT.Name)
                        iLevel = oRange.ListFormat.ListLevelNumber

                        '            'apply heading style before separator
                        '            oRange.Style = GetSplitHeadingStyle(xScheme, iLevel)

                        'apply para style after separator
                        CurWordApp.Selection.Style = GetSplitParaStyle(xScheme, iLevel)

                        'add direct formatting if specified
                        If bFormat Then
                            oRange.EndOf()
                            oRange.Move(WdUnits.wdCharacter, -1)
                            rngFormatTCHeading(oRange, iLevel, True)
                        End If
                    End If
                End If

                'apply blue font to style separator
                '10/29/12 - can't do this because numbers turn blue as well
                'after saving, closing, and reopening
                '    oRange.Characters.Last.Font.ColorIndex = wdBlue
            Finally
                CurWordApp.ActiveWindow.View.ShowAll = bShowAll
            End Try
        End Function

        Friend Shared Function bRemoveStyleSeparators(ByVal oRange As Word.Range) As Boolean
            Dim oPara As Paragraph
            Dim rngPara As Word.Range
            Dim bFound As Boolean
            Dim bShowAll As Boolean
            Dim rngDuplicate As Word.Range

            Try
                bRemoveStyleSeparators = False

                'force show all
                With CurWordApp.ActiveWindow.View
                    bShowAll = .ShowAll
                    .ShowAll = True
                End With

                'expand to include previous paragraph - range may start with the
                'paragraph trailing a style separator
                rngDuplicate = oRange.Duplicate
                rngDuplicate.Expand(WdUnits.wdParagraph)
                rngDuplicate.MoveStart(WdUnits.wdParagraph, -1)

                'cycle through all paragraphs in range
                For Each oPara In rngDuplicate.Paragraphs
                    If oPara.IsStyleSeparator Then
                        bFound = True
                        rngPara = oPara.Range
                        rngPara.Characters.Last.Delete(WdUnits.wdCharacter, 1) 'GLOG 8733 (dm)
                        '            rngPara.Expand wdUnits.wdParagraph
                        '            If InStr(rngPara.Style, "Heading_L") > 0 Then
                        '                'remove heading style
                        '                rngPara.Style = CurWordApp.ActiveDocument.Styles(rngPara.Style).BaseStyle
                        '            End If
                    End If
                Next oPara

                bRemoveStyleSeparators = bFound

            Finally
                CurWordApp.ActiveWindow.View.ShowAll = bShowAll
            End Try
        End Function

        Friend Shared Function bMarkAndFormatHeadings(ByVal xTargetStyles As String,
                ByVal iMarkAction As mpMarkActions, ByVal iFormatAction As mpFormatActions,
                ByVal iType As mpMarkingModes, ByVal xHeadingDelimiter As String,
                ByVal iScope As mpMarkFormatScopes, ByVal bPromptIfNoResults As Boolean,
                ByVal iReplacementMode As mpMarkingReplacementModes) As Boolean
            Dim i As Integer
            Dim iNumParas As Integer
            Dim paraP As Word.Paragraph
            Dim iLevel As Integer
            Dim rngPara As Word.Range
            Dim bHeadingsMarked As Boolean
            Dim rngScope As Word.Range
            Dim bFormat As Boolean
            Dim bMark As Boolean
            Dim bIsOutlineLevel As Boolean
            Dim bIsMarked As Boolean
            Dim iMarkPos As Integer
            Dim lPos As Long
            Dim bUnmark As Boolean
            Dim bUnformat As Boolean
            Dim rngTCField As Word.Range
            Dim fldP As Word.Field
            Dim bIsStyleBased As Boolean
            Dim xLT As String = ""
            Dim rngHeading As Word.Range
            Dim rngLocation As Word.Range
            Dim xTest As String = ""
            Dim iUserChoice As Integer
            Dim bDelimiterFound As Boolean
            Dim xEndQuote As String = ""
            Dim xSmartQuote As String = ""
            Dim bIsStandard As Boolean
            Dim bIsEndQuote As Boolean
            Dim bIsSmartQuote As Boolean
            Dim lPos2 As Long
            Dim lPos3 As Long
            Dim oPrompt As Form
            Dim xStyle As String = ""
            Dim oLT As Word.ListTemplate
            Dim bIsMPLT As Boolean
            Dim bIsStyleSep As Boolean
            Dim iUserReplaceChoice As Integer
            Dim bReplace As Boolean
            Dim bManageStatus As Boolean
            Dim oDlg As frmMarkPrompt
            Dim oDlgFrench As frmMarkPromptFrench

            'determine whether to manage progess bar (9.9.6006)
            bManageStatus = Not g_oStatus Is Nothing

            'get actions to execute
            bMark = (iMarkAction = mpMarkActions.mpMarkAction_Mark)
            bUnmark = (iMarkAction = mpMarkActions.mpMarkAction_Unmark)
            bFormat = (iFormatAction = mpFormatActions.mpFormatAction_Format)
            bUnformat = (iFormatAction = mpFormatActions.mpFormatAction_Unformat)

            'set scope
            Select Case iScope
                Case mpMarkFormatScopes.mpMarkFormatScope_Document
                    rngScope = CurWordApp.ActiveDocument.Content
                Case mpMarkFormatScopes.mpMarkFormatScope_Section
                    rngScope = CurWordApp.ActiveDocument.Sections(CurWordApp.Selection _
                        .Information(WdInformation.wdActiveEndSectionNumber)).Range
                Case Else
                    rngScope = CurWordApp.Selection.Range
            End Select

            'get number of numbers in rngScope
            iNumParas = rngScope.ListParagraphs.Count

            'period - double quote - space(s) should be recognized as standard;
            'we're also accepting smart end quotes as a match for straight quotes
            bIsStandard = (xHeadingDelimiter = mpSentencePeriodTwo) Or
                (xHeadingDelimiter = mpSentencePeriodOne)
            If xHeadingDelimiter = mpSentencePeriodTwo Then
                xEndQuote = mpSentenceQuoteTwo
                xSmartQuote = g_xSmartTwo
            ElseIf xHeadingDelimiter = mpSentencePeriodOne Then
                xEndQuote = mpSentenceQuoteOne
                xSmartQuote = g_xSmartOne
            ElseIf InStr(xHeadingDelimiter, Chr(34)) <> 0 Then
                xSmartQuote = xSubstitute(xHeadingDelimiter, Chr(34), ChrW(&H201D))
            End If

            'ensure leading and trailing pipes
            xTargetStyles = "|" & xTargetStyles & "|"

            'cycle through numbered paragraphs
            For Each paraP In rngScope.ListParagraphs
                bReplace = False
                bIsStyleSep = False
                bIsEndQuote = False
                bIsSmartQuote = False
                rngTCField = Nothing
                bIsMPLT = False
                rngPara = paraP.Range

                'get paragraph level
                iLevel = rngPara.ParagraphFormat.OutlineLevel

                'get style
                xStyle = ""
                On Error Resume Next
                xStyle = rngPara.Style.NameLocal
                On Error GoTo 0

                'do only specified styles -
                'GLOG 5298 - added a way to target all mp styles of a given outline level ("*_L#")
                If (xStyle <> "") And ((InStr(UCase$(xTargetStyles),
                        "|" & UCase$(xStyle) & "|") > 0) Or (InStr(UCase$(xTargetStyles),
                        "|*_L" & CStr(iLevel) & "|") > 0)) Then
                    'skip if not part of a MacPac scheme
                    oLT = rngPara.ListFormat.ListTemplate
                    If Not oLT Is Nothing Then _
                        bIsMPLT = bIsMPListTemplate(oLT)
                    If Not bIsMPLT Then _
                        GoTo NextPara

                    'if we're here, then doc contains at least one target heading
                    bHeadingsMarked = True

                    'determine if para is already marked
                    iMarkPos = InStr(rngPara.Text, Chr(19) & " " & g_xTCPrefix & " ")
                    If iMarkPos = 0 Then
                        'check for style separator
                        If paraP.IsStyleSeparator Then
                            iMarkPos = Len(rngPara.Text)
                            bIsStyleSep = True
                        End If
                    End If
                    bIsMarked = iMarkPos

                    'prompt to replace mark of other type
                    If bMark And bIsMarked And (((iType = mpMarkingModes.mpMarkingMode_StyleSeparators) And
                            Not bIsStyleSep) Or ((iType = mpMarkingModes.mpMarkingMode_TCCodes) And
                            bIsStyleSep)) Then
                        If (iUserReplaceChoice = vbYes) Or
                                (iReplacementMode = mpMarkingReplacementModes.mpMarkingReplacementMode_Automatic) Then
                            bReplace = True
                        ElseIf (iUserReplaceChoice = vbNo) Or
                                (iReplacementMode = mpMarkingReplacementModes.mpMarkingReplacementMode_None) Then
                            bReplace = False
                        Else
                            'hide status screen while prompting (9.9.6006)
                            If bManageStatus Then
                                g_oStatus.Close()
                                g_oStatus.Dispose()
                                'g_oStatus.Hide()
                                g_oStatus = Nothing
                                CurWordApp.ScreenUpdating = True
                                CurWordApp.ScreenRefresh()
                            End If

                            'propose replacement to user
                            CurWordApp.ScreenUpdating = True
                            CurWordApp.ScreenRefresh()
                            System.Windows.Forms.Application.DoEvents()

                            'select proposed heading
                            rngPara.Select()

                            'prompt user
                            If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                oPrompt = New frmMarkPromptFrench
                                oDlgFrench = CType(oPrompt, frmMarkPromptFrench)
                                oDlgFrench.MarkingMode = iType
                                oDlgFrench.IsAmbiguousHeading = False
                                oPrompt.ShowDialog()
                                iUserReplaceChoice = oDlgFrench.m_iRetval
                            Else
                                oPrompt = New frmMarkPrompt
                                oDlg = CType(oPrompt, frmMarkPrompt)
                                oDlg.MarkingMode = iType
                                oDlg.IsAmbiguousHeading = False
                                oPrompt.ShowDialog()
                                iUserReplaceChoice = oDlg.m_iRetval
                            End If

                            oPrompt.Close()
                            oPrompt = Nothing

                            'restore status screen (9.9.6006)
                            If bManageStatus Then
                                g_oStatus = New MacPac.ProgressForm(100, 3, g_xTOCStatusMsg, "", False)
                                g_oStatus.Show()
                                System.Windows.Forms.Application.DoEvents()
                            End If

                            CurWordApp.ScreenUpdating = False

                            If iUserReplaceChoice = vbCancel Then
                                'clean up
                                CurWordApp.StatusBar = ""
                                bMarkAndFormatHeadings = False
                                Exit Function
                            Else
                                bReplace = ((iUserReplaceChoice = vbYes) Or
                                    (iUserReplaceChoice = vbAbort))
                            End If
                        End If

                        If bReplace Then
                            bIsMarked = False

                            If iType = mpMarkingModes.mpMarkingMode_TCCodes Then
                                'TC code removal is built in to style separator
                                'insertion, but not vice versa
                                bRemoveStyleSeparators(rngPara)
                                rngPara.Expand(WdUnits.wdParagraph)
                            Else
                                'if existing TC code is at end of para, just remove it
                                For Each fldP In rngPara.Fields
                                    If fldP.Type = WdFieldType.wdFieldTOCEntry Then
                                        rngTCField = rngGetField(fldP.Code)
                                        If rngPara.End = rngTCField.End + 1 Then
                                            fldP.Delete()
                                            bIsMarked = True
                                        End If
                                        Exit For
                                    End If
                                Next fldP
                            End If
                        End If
                    End If

                    'set heading range
                    With rngPara
                        'search for delimiter
                        lPos = InStr(.Text, xHeadingDelimiter)

                        If bIsStandard Then
                            'try with end quote
                            lPos2 = InStr(.Text, xEndQuote)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsEndQuote = True
                                bIsSmartQuote = False
                            End If
                        End If

                        If xSmartQuote <> "" Then
                            'try with smart quote
                            lPos2 = InStr(.Text, xSmartQuote)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsEndQuote = False
                                bIsSmartQuote = True
                            End If
                        End If

                        If bIsMarked Or bReplace Then
                            'expand to start of mark
                            .StartOf()
                            .MoveEnd(WdUnits.wdCharacter, iMarkPos - 1)

                            'GLOG 5202 - account for inline content controls
                            .MoveEnd(WdUnits.wdCharacter,
                                (CountCCsEndingInRange(rngPara) * 2))
                        ElseIf lPos Then
                            'expand to end of first sentence def mark
                            If xHeadingDelimiter = mpSentencePeriodOne Then
                                'if delimiter is period and 1 space,
                                'skip common abbreviations
                                While lPos
                                    .StartOf()
                                    .MoveEnd(WdUnits.wdCharacter, lPos)

                                    'GLOG 5202 - account for inline content controls
                                    .MoveEnd(WdUnits.wdCharacter,
                                        (CountCCsEndingInRange(rngPara) * 2))

                                    If bIsAbbreviation(.Text) Then
                                        bIsEndQuote = False
                                        bIsSmartQuote = False
                                        .Expand(WdUnits.wdParagraph)

                                        'search for delimiter
                                        lPos2 = InStr(CInt(lPos + 1), .Text, xHeadingDelimiter)

                                        'try with end quote
                                        lPos3 = InStr(CInt(lPos + 1), .Text, xEndQuote)
                                        If (lPos3 <> 0) And
                                                ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                            lPos2 = lPos3
                                            bIsEndQuote = True
                                            bIsSmartQuote = False
                                        End If

                                        'try with smart quote
                                        lPos3 = InStr(CInt(lPos + 1), .Text, xSmartQuote)
                                        If (lPos3 <> 0) And
                                                ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                            lPos2 = lPos3
                                            bIsEndQuote = False
                                            bIsSmartQuote = True
                                        End If

                                        lPos = lPos2
                                    Else
                                        lPos = 0
                                    End If
                                End While
                            Else
                                'all other delimiters are final
                                .StartOf()
                                .MoveEnd(WdUnits.wdCharacter, lPos)

                                'GLOG 5202 - account for inline content controls
                                .MoveEnd(WdUnits.wdCharacter,
                                    (CountCCsEndingInRange(rngPara) * 2))
                            End If
                        ElseIf (xHeadingDelimiter = mpSentencePeriodTwo) And
                                (iUserChoice <> vbNo) And
                                (bMark Or bFormat Or bUnformat) Then
                            'period and 2 spaces not found; check
                            'for instances of period and 1 space
                            lPos = InStr(.Text, mpSentencePeriodOne)

                            'try with end quote
                            lPos2 = InStr(.Text, mpSentenceQuoteOne)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsEndQuote = True
                                bIsSmartQuote = False
                            End If

                            'try with smart quote
                            lPos2 = InStr(.Text, g_xSmartOne)
                            If (lPos2 <> 0) And ((lPos = 0) Or (lPos2 < lPos)) Then
                                lPos = lPos2
                                bIsEndQuote = False
                                bIsSmartQuote = True
                            End If

                            'when using style separators, a standalone heading is as good as marked
                            If (lPos = 0) And (iType = mpMarkingModes.mpMarkingMode_StyleSeparators) Then _
                                bIsMarked = True

                            While lPos
                                .StartOf()
                                .MoveEnd(WdUnits.wdCharacter, lPos)

                                'GLOG 5202 - account for inline content controls
                                .MoveEnd(WdUnits.wdCharacter,
                                    (CountCCsEndingInRange(rngPara) * 2))

                                If bIsAbbreviation(.Text) Then
                                    'skip common abbreviations
                                    bIsEndQuote = False
                                    bIsSmartQuote = False
                                    .Expand(WdUnits.wdParagraph)

                                    'search for delimiter
                                    lPos2 = InStr(CInt(lPos + 1), .Text, mpSentencePeriodOne)

                                    'try with end quote
                                    lPos3 = InStr(CInt(lPos + 1), .Text, mpSentenceQuoteOne)
                                    If (lPos3 <> 0) And
                                            ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                        lPos2 = lPos3
                                        bIsEndQuote = True
                                        bIsSmartQuote = False
                                    End If

                                    'try with smart quote
                                    lPos3 = InStr(CInt(lPos + 1), .Text, g_xSmartOne)
                                    If (lPos3 <> 0) And
                                            ((lPos2 = 0) Or (lPos3 < lPos2)) Then
                                        lPos2 = lPos3
                                        bIsEndQuote = False
                                        bIsSmartQuote = True
                                    End If

                                    lPos = lPos2
                                ElseIf iUserChoice = vbYes Then
                                    '"Yes to all" - mark sentence
                                    lPos = 0
                                Else
                                    'hide status screen while prompting (9.9.6006)
                                    If bManageStatus Then
                                        g_oStatus.Close()
                                        g_oStatus.Dispose()
                                        'g_oStatus.Hide()
                                        g_oStatus = Nothing
                                        CurWordApp.ScreenUpdating = True
                                        CurWordApp.ScreenRefresh()
                                    End If

                                    'propose potential heading to user
                                    CurWordApp.ScreenUpdating = True
                                    CurWordApp.ScreenRefresh()
                                    System.Windows.Forms.Application.DoEvents()

                                    'select proposed heading
                                    .Select()

                                    'prompt user
                                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                                        oPrompt = New frmMarkPromptFrench
                                        oDlgFrench = CType(oPrompt, frmMarkPromptFrench)
                                        oDlgFrench.MarkingMode = iType
                                        oDlgFrench.IsAmbiguousHeading = True
                                        oPrompt.ShowDialog()
                                        iUserChoice = oDlgFrench.m_iRetval
                                    Else
                                        oPrompt = New frmMarkPrompt
                                        oDlg = CType(oPrompt, frmMarkPrompt)
                                        oDlg.MarkingMode = iType
                                        oDlg.IsAmbiguousHeading = True
                                        oPrompt.ShowDialog()
                                        iUserChoice = oDlg.m_iRetval
                                    End If

                                    oPrompt.Close()
                                    oPrompt = Nothing

                                    'restore status screen (9.9.6006)
                                    If bManageStatus Then
                                        g_oStatus = New MacPac.ProgressForm(100, 3, g_xTOCStatusMsg, "", False)
                                        g_oStatus.Show()
                                        System.Windows.Forms.Application.DoEvents()
                                    End If

                                    CurWordApp.ScreenUpdating = False

                                    If iUserChoice = vbCancel Then
                                        'clean up
                                        CurWordApp.StatusBar = ""
                                        bMarkAndFormatHeadings = False
                                        Exit Function
                                    ElseIf iUserChoice = vbRetry Then
                                        '"No" - look for next possibility
                                        bIsEndQuote = False
                                        bIsSmartQuote = False
                                        .Expand(WdUnits.wdParagraph)
                                        lPos = InStr(CInt(lPos + 1), .Text, mpSentencePeriodOne)
                                        If (lPos = 0) And (iType = mpMarkingModes.mpMarkingMode_StyleSeparators) Then _
                                            bIsMarked = True
                                    ElseIf iUserChoice = vbNo Then
                                        '"No to all" - mark whole paragraph
                                        .Expand(WdUnits.wdParagraph)
                                        lPos = 0
                                        If iType = mpMarkingModes.mpMarkingMode_StyleSeparators Then _
                                            bIsMarked = True
                                    ElseIf iUserChoice = vbAbort Then
                                        '"Yes" - mark sentence
                                        lPos = 0
                                    ElseIf iUserChoice = vbIgnore Then
                                        'skip paragraph
                                        GoTo NextPara
                                    End If
                                End If
                            End While
                        ElseIf Not bIsStandard Then
                            'if we're not looking for periods, don't mark
                            'entire paragraph when delimiter isn't found
                            GoTo NextPara
                        ElseIf iType = mpMarkingModes.mpMarkingMode_StyleSeparators Then
                            'when using style separators, a standalone heading is as good as marked
                            bIsMarked = True
                        End If

                        'if we're here, then at least one heading
                        'matches sentence definition
                        bDelimiterFound = True

                        'exclude trailing para, end of cell
                        If .Characters.Last.Text = vbCr Then
                            .MoveEnd(WdUnits.wdCharacter, -1)
                        ElseIf Len(.Text) Then
                            If Asc(Strings.Right(.Text, 1)) = 7 Then
                                .MoveEnd(WdUnits.wdCharacter, -1)
                            End If
                        End If

                        'exclude trailing character
                        If .Characters.Last.Text = Strings.Left(xHeadingDelimiter, 1) Then
                            If bIsEndQuote Or bIsSmartQuote Then
                                'include quote
                                .MoveEnd(WdUnits.wdCharacter, 1)
                            Else
                                .MoveEnd(WdUnits.wdCharacter, -1)
                            End If
                        ElseIf bIsSmartQuote And
                                (.Characters.Last.Text = ChrW(&H201D)) Then
                            'non-standard delimiter beginning with quote
                            .MoveEnd(WdUnits.wdCharacter, -1)
                        End If

                        .Collapse(WdCollapseDirection.wdCollapseEnd)

                    End With

                    CurWordApp.ScreenUpdating = False

                    'get TC field
                    If bIsMarked Then
                        For Each fldP In rngPara.Paragraphs(1).Range.Fields
                            If fldP.Type = WdFieldType.wdFieldTOCEntry Then
                                rngTCField = rngGetField(fldP.Code)
                                Exit For
                            End If
                        Next fldP
                    End If

                    If bMark And Not bIsMarked Then
                        If iType = mpMarkingModes.mpMarkingMode_TCCodes Then
                            'GLOG 5404 - prevent error when after block level cc
                            rngPara.Select()
                            While AfterBlockLevelCC()
                                rngPara.Move(WdUnits.wdCharacter, -1)
                                CurWordApp.Selection.Move(WdUnits.wdCharacter, -1)
                            End While

                            'add tc field  - do not insert any text in code
                            rngPara.Fields.Add(rngPara, _
                                               WdFieldType.wdFieldTOCEntry, _
                                               , _
                                               False)

                            'remove formatting from TC field
                            rngLocation = rngPara.Duplicate
                            With rngLocation
                                .MoveEnd(WdUnits.wdParagraph)
                                .Fields(1).Code.Font.Reset()
                            End With
                        Else
                            'insert style separator
                            rngPara.Select()
                            InsertStyleSeparator()
                        End If
                    ElseIf bUnmark And bIsMarked Then
                        'unmark - added 8/10/00
                        If bIsStyleSep Then
                            bRemoveStyleSeparators(rngPara)
                        ElseIf Not rngTCField Is Nothing Then
                            rngTCField.Delete()
                        End If
                    End If

                    '4/24/13 - account for non-outline level mp numbering styles
                    'GLOG 5294 - we accounted for non-outline level styles as far as not erring,
                    'but they still weren't getting formatted/unformatted
                    If iLevel = 10 Then _
                        iLevel = iGetParagraphLevel(rngPara, 0)

                    If (iLevel > 0) And (iLevel < 10) Then
                        If bFormat Then
                            'format heading and number to appropriate level
                            rngFormatTCHeading(rngPara, _
                                               iLevel, _
                                               bFormat)
                        ElseIf bUnformat Then
                            'unformat - added 8/10/00
                            If Not rngTCField Is Nothing Then
                                'move to end of TC field
                                rngPara.SetRange(rngTCField.End, _
                                    rngTCField.End)
                            End If
                            rngHeading = rngGetTCHeading(rngPara)
                            rngHeading.Font.Reset()
                        End If
                    End If

                End If

NextPara:
                'update status
                i = i + 1
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    If bMark And bFormat Then
                        xMsg = "Marquer et Formater: "
                    ElseIf bMark Then
                        xMsg = "Marquer: "
                    ElseIf bFormat Then
                        xMsg = "Formater: "
                    ElseIf bUnmark And bUnformat Then
                        xMsg = "Non marquer et non formater: "
                    ElseIf bUnmark Then
                        xMsg = "Non marquer: "
                    ElseIf bUnformat Then
                        xMsg = "Non formater: "
                    End If
                Else
                    If bMark And bFormat Then
                        xMsg = "Marking and Formatting: "
                    ElseIf bMark Then
                        xMsg = "Marking: "
                    ElseIf bFormat Then
                        xMsg = "Formatting: "
                    ElseIf bUnmark And bUnformat Then
                        xMsg = "Unmarking and Unformatting: "
                    ElseIf bUnmark Then
                        xMsg = "Unmarking: "
                    ElseIf bUnformat Then
                        xMsg = "Unformatting: "
                    End If
                End If

                CurWordApp.StatusBar = _
                    xMsg & Format(i / iNumParas, "0%")
            Next paraP

            'display message if no headings marked
            If bPromptIfNoResults Then
                If Not bHeadingsMarked Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucune action n'a �t� effectu�e selon les options s�lectionn�es. V�rifiez vos s�lections et apportez les changements n�cessaires.", _
                            vbExclamation + vbOKOnly, "Num�rotation TSG")
                    Else
                        MsgBox("No action could be taken based on the " & _
                               "selections made in the dialog box.  Please " & _
                               "check selections and make the necessary changes.", _
                               vbExclamation + vbOKOnly, AppName)
                    End If
                ElseIf Not bDelimiterFound Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Aucun �l�ment de cette d�finition n'a �t� trouv� dans le document.", _
                            vbExclamation + vbOKOnly, "Num�rotation TSG")
                    Else
                        MsgBox("No instances of the specified sentence definition " & _
                            "were found in this document.", vbExclamation + vbOKOnly, AppName)
                    End If
                End If
            End If

            'clean up
            CurWordApp.StatusBar = ""
            bMarkAndFormatHeadings = True
        End Function

        Friend Shared Sub ConvertToStyleSeparators()
            'replaces TC codes in MacPac numbered paragraphs with style separators
            Dim fldP As Word.Field
            Dim rngCode As Word.Range
            Dim lCodes As Long
            Dim l As Long
            Dim oLT As Word.ListTemplate
            Dim rngStart As Word.Range

            Try
                CurWordApp.ScreenUpdating = False

                'get current selection for later reselection
                rngStart = CurWordApp.Selection.Range

                '   cycle through tc codes
                lCodes = CurWordApp.ActiveDocument.Fields.Count
                For Each fldP In CurWordApp.ActiveDocument.Fields
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = _
                            "Convertir codes TM: " & _
                                Format(l / lCodes, "0%")
                    Else
                        CurWordApp.StatusBar = _
                            "Converting TC Codes: " & _
                                Format(l / lCodes, "0%")
                    End If
                    l = l + 1

                    If fldP.Type = WdFieldType.wdFieldTOCEntry Then
                        rngCode = fldP.Code
                        oLT = rngCode.ListFormat.ListTemplate
                        If Not oLT Is Nothing Then
                            If bIsMPListTemplate(oLT) Then
                                fldP.Delete()
                                If rngCode.End < rngCode.Paragraphs(1).Range.End - 1 Then
                                    'run-in heading - add style separator
                                    rngCode.Select()
                                    InsertStyleSeparator()
                                End If
                            End If
                        End If
                    End If
                Next fldP

            Finally
                If Not rngStart Is Nothing Then
                    rngStart.Select()
                End If
                CurWordApp.ScreenUpdating = True
                CurWordApp.StatusBar = ""
            End Try
        End Sub

        Friend Shared Sub ConvertToTCCodes()
            'replaces style separators in MacPac numbered paragraphs with TC codes
            Dim lNumParas As Long
            Dim l As Long
            Dim oLT As Word.ListTemplate
            Dim rngStart As Word.Range
            Dim oPara As Word.Paragraph
            Dim rngPara As Word.Range
            Dim oFld As Word.Field

            Try
                CurWordApp.ScreenUpdating = False

                'get current selection for later reselection
                rngStart = CurWordApp.Selection.Range

                lNumParas = CurWordApp.ActiveDocument.ListParagraphs.Count

                '   cycle through tc codes
                For Each oPara In CurWordApp.ActiveDocument.ListParagraphs
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = _
                            "Convertir s�parateurs de style: " & _
                                Format(l / lNumParas, "0%")
                    Else
                        CurWordApp.StatusBar = _
                            "Converting Style Separators: " & _
                                Format(l / lNumParas, "0%")
                    End If
                    l = l + 1

                    If oPara.IsStyleSeparator Then
                        rngPara = oPara.Range
                        oLT = rngPara.ListFormat.ListTemplate
                        If Not oLT Is Nothing Then
                            If bIsMPListTemplate(oLT) Then
                                'move to end of paragraph
                                rngPara.EndOf()
                                rngPara.MoveEnd(WdUnits.wdCharacter, -1)

                                'delete style separator
                                bRemoveStyleSeparators(rngPara)

                                'insert TC code
                                oFld = rngPara.Fields.Add(rngPara, WdFieldType.wdFieldTOCEntry, , False)

                                'remove formatting from TC code
                                oFld.Code.Font.Reset()
                            End If
                        End If
                    End If
                Next oPara

            Finally
                If Not rngStart Is Nothing Then
                    rngStart.Select()
                End If
                CurWordApp.ScreenUpdating = True
                CurWordApp.StatusBar = ""
            End Try
        End Sub

        Friend Shared Function bInsertTOC() As Long
            'inserts a MacPac TOC
            Dim iMaxLevel As Integer
            Dim iMinLevel As Integer
            Dim iDefaultTCLevel As Integer
            Dim rngLocation As Word.Range
            Dim fldTOC As Word.Field
            Dim xTOCParams As String = ""
            Dim rngTOC As Word.Range
            Dim xLevRange As String = ""
            Dim iNumStyles As Integer
            Dim xStyleList As String = ""
            Dim bShowAll As Boolean
            Dim bShowHidden As Boolean
            Dim iView As Integer
            Dim ds As Date
            Dim xMsg As String = ""
            Dim iEntryType As mpTOCEntryTypes
            Dim udtTOCFormat As TOCFormat
            Dim i As Integer
            Dim xTestContent As String = ""
            Dim xTimeMsg As String = ""
            Dim lNumParas As Long
            Dim lWarningThreshold As Long
            Dim iLoc As Integer
            Dim bkmkTOCEntry As Word.Bookmark
            Dim bShowHiddenBkmks As Boolean
            Dim bSmartQuotes As Boolean
            Dim lZoom As Long
            Dim bIncludeSchedule As Boolean
            Dim bIncludeOther As Boolean
            Dim xInclusions As String = ""
            Dim xExclusions As String = ""
            Dim xTOC9Style As String = ""
            Dim oDummyStyle As Word.Style
            Dim rngDummyPara As Word.Range
            Dim bIsCentered As Boolean
            Dim iDummySec As Integer
            Dim bColumnsReformatted As Boolean
            Dim lStartTick As Integer
            Dim sElapsedTime As Single
            Dim bPreserveLineBreaks As Boolean
            Dim lShowTags As Long
            Dim dlgTOC As Form
            Dim xScheme As String
            Dim bPrintHiddenText As Boolean
            Dim bInsertAsField As Boolean
            Dim iHeadingDef As Integer
            Dim oDoc As Word.Document
            Dim oDlg As frmInsertTOC
            Dim oDlgFrench As frmInsertTOCFrench
            Dim xInsertAt As String
            Dim bIncludeStyles As Boolean
            Dim xScheduleStyle As String
            Dim bApplyTOC9 As Boolean
            Dim bTwoColumn As Boolean
            Dim bApplyManualFormats As Boolean
            Dim bUpdateTOCStyles As Boolean
            Dim bStyles As Boolean
            Dim bTCEntries As Boolean
            Dim bBoldHeaderTOC As Boolean
            Dim bBoldHeaderPage As Boolean
            Dim bCapHeaderTOC As Boolean
            Dim bCapHeaderPage As Boolean
            Dim bUnderlineHeaderTOC As Boolean
            Dim bUnderlineHeaderPage As Boolean
            Dim bPageNoContinue As Boolean
            Dim iPageNoStyle As Integer
            Dim iPageNoPunctuation As Short
            Dim xTOCScheme As String
            Dim xTOCDisplayName As String

            Try
                'OutputDebugString "Start of TOC macro"

                bInsertTOC = False
                '   validate environment
                If g_bOrganizerSavePrompt And (CurWordApp.WordBasic.FileNameFromWindow$() = "") Then
                    'prompt to save document
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("Cette macro n'est pas disponible dans un document non sauf-gard�.  Veuillez sauf-garder votre document avant d�ex�cuter cette macro." & _
                            vbCr & vbCr & "Nous vous prions de nous excuser pour tout inconv�nient mais c'est un workaround provisoire pour un bogue de Microsoft.  Le bogue a �t� introduit r�cemment dans Word 2007 SP2 et affecte la copie des styles par l'interm�diaire du code utilisant la fonction de l�Organisateur de Word.  Le correctif de Microsoft devrait �tre disponible dans les deux prochains mois.", _
                            vbInformation, "Num�rotation TSG")
                    Else
                        MsgBox("This macro is not available in an unsaved document.  " & _
                            "Please save your document and run this macro again." & vbCr & vbCr & _
                            "We apologize for the inconvenience but this is a temporary " & _
                            "workaround for a Microsoft bug that affects copying styles via code using Word's Organizer " & _
                            "feature.  Microsoft's hotfix should be available within a couple months.", _
                            vbInformation, AppName)
                    End If
                    Exit Function
                ElseIf g_xTOCLocations(0) = "" Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        xMsg = "Ne peut cr�er TM, il n'y a pas d'emplacement d�sign� dans " & SettingsFileName & ".  Contactez votre administrateur."
                    Else
                        xMsg = "Cannot create TOC because there are no TOC locations specified in " & _
                            SettingsFileName & ".  Please see your system administrator."
                    End If
                Else
                    Try
                        xTestContent = CurWordApp.ActiveDocument.Characters(2).Text
                    Catch
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            xMsg = "Ne peut cr�er TM.  Le pr�sent document est vide."
                        Else
                            xMsg = "Cannot create TOC.  The " & _
                                   "active document is empty."
                        End If
                    End Try
                End If

                If Len(xMsg) Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox(xMsg, vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox(xMsg, vbExclamation, AppName)
                    End If
                    Exit Function
                End If

                '   warn about large document
                lWarningThreshold = Val(xAppGetFirmSetting("TOC", _
                    "NumberOfParasWarningThreshold"))
                lNumParas = CurWordApp.ActiveDocument.Paragraphs.Count

                If lWarningThreshold > 0 And _
                        lNumParas > lWarningThreshold Then
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        lRet = MsgBox("Ce document contient " & lNumParas & " paragraphes.  TSG TM macro fait beaucoup plus que les fonctions natives TM Word.  La mise en forme du document prendra quelques temps.  D�sirez-vous continuer?", _
                            vbYesNo + vbQuestion, "Num�rotation TSG")
                    Else
                        lRet = MsgBox("This document contains " & lNumParas & " paragraphs.  " & _
                            "The TSG TOC macro does a lot more work than " & _
                            "the native Word TOC function.  Formatting a document " & _
                            "of this size will take some time.  Do you wish to continue?", _
                            vbYesNo + vbQuestion, AppName)
                    End If
                    If lRet = vbNo Then Exit Function
                End If

                oCustTOC = CreateCTOCObject()

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    dlgTOC = New frmInsertTOCFrench
                    oDlgFrench = CType(dlgTOC, frmInsertTOCFrench)
                    oDlgFrench.InsertAsField = False
                Else
                    dlgTOC = New frmInsertTOC
                    oDlg = CType(dlgTOC, frmInsertTOC)
                    oDlg.InsertAsField = False
                End If

                '   tsgTOC.sty is now only loaded as needed (12/10/01)
                LoadTOCSty()

                '   call any custom code
                lRet = oCustTOC.lBeforeDialogShow(dlgTOC)

                'OutputDebugString "Before form display"

                dlgTOC.ShowDialog()

                'OutputDebugString "After form display"
                lStartTick = CurrentTick()

                If dlgTOC.DialogResult = System.Windows.Forms.DialogResult.Cancel Then
                    UnloadTOCSty()
                    dlgTOC.Close()
                    CurWordApp.ActiveWindow.SetFocus()
                    dlgTOC = Nothing
                    oCustTOC = Nothing
                    Exit Function
                End If

                '   call any custom code
                lRet = oCustTOC.lAfterDialogShow()

                'GLOG 5147
                oDoc = CurWordApp.ActiveDocument

                '   display status message
                g_oStatus = New MacPac.ProgressForm(100, 2, g_xTOCStatusMsg, "", False)
                g_oStatus.Show()
                System.Windows.Forms.Application.DoEvents()

                'GLOG 5057 (2/1/12) - disable Print Hidden Text option in order
                'to prevent inaccurate page numbers
                bPrintHiddenText = CurWordApp.Options.PrintHiddenText
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = False

                '   delete hidden TOC bookmarks - prevents potential
                '   "insufficient memory" error when generating TOC
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "G�n�re TM.  Veuillez patienter..."
                Else
                    CurWordApp.StatusBar = "Generating TOC.  Please wait..."
                End If

                With CurWordApp.ActiveDocument.Bookmarks
                    bShowHiddenBkmks = .ShowHidden
                    .ShowHidden = True
                End With
                For Each bkmkTOCEntry In CurWordApp.ActiveDocument.Bookmarks
                    With bkmkTOCEntry
                        If Strings.Left(.Name, 4) = "_Toc" Then
                            .Delete()
                            If Not g_bPreserveUndoListTOC Then _
                                CurWordApp.ActiveDocument.UndoClear()
                        End If
                    End With
                Next bkmkTOCEntry
                CurWordApp.ActiveDocument.Bookmarks.ShowHidden = bShowHiddenBkmks

                '   necessary for speed - this function executes
                '   a large number of Word actions.  the undo buffer
                '   fills up after 2 iterations, so to avoid a
                '   msg to the user, clear out first.
                If Not g_bPreserveUndoListTOC Then _
                    CurWordApp.ActiveDocument.UndoClear()

                '   get/set environment
                With CurWordApp
                    .ScreenRefresh()
                    .ScreenUpdating = False
                End With

                EchoOff()
                With CurWordApp.ActiveWindow.View
                    '       get original settings
                    bShowAll = .ShowAll
                    bShowHidden = .ShowHiddenText
                    iView = .Type
                    lZoom = .Zoom.Percentage

                    '       modify environment
                    .ShowAll = True
                    .ShowHiddenText = True
                    .Type = WdViewType.wdNormalView
                    .Zoom.Percentage = 100
                End With

                'hide xml tags
                If g_bXMLSupport Then _
                    lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, False)

                '   turn off track changes
                CurWordApp.ActiveDocument.TrackRevisions = False

                '   turn off smart quotes
                With CurWordApp.Options
                    bSmartQuotes = .AutoFormatAsYouTypeReplaceQuotes
                    .AutoFormatAsYouTypeReplaceQuotes = False
                End With

                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    With oDlgFrench
                        'store user choices
                        iMaxLevel = .cbxMaxLevel.Value
                        iMinLevel = .cbxMinLevel.Value

                        '       get inclusions/exclusions
                        If .optIncludeSchemes.Checked Then
                            '           schemes
                            xInclusions = .Inclusions
                            xExclusions = .Exclusions
                        Else
                            '           individual styles
                            xInclusions = .StyleInclusions
                            xExclusions = .StyleExclusions
                        End If

                        xInsertAt = .cbxInsertAt.Text
                        bIncludeStyles = .optIncludeStyles.Checked
                        xScheduleStyle = .cbxScheduleStyles.Text
                        bApplyTOC9 = .chkApplyTOC9.Checked
                        bTwoColumn = .chkTwoColumn.Checked
                        bApplyManualFormats = .chkApplyManualFormatsToTOC.Checked
                        bUpdateTOCStyles = .optTOCStylesUpdate.Checked
                        bStyles = .chkStyles.Checked
                        bTCEntries = .chkTCEntries.Checked
                        iHeadingDef = .cmbTextStyles.SelectedIndex
                        xTOCDisplayName = .cbxTOCScheme.Text
                        xTOCScheme = .TOCScheme
                        bBoldHeaderTOC = .BoldHeaderTOC
                        bBoldHeaderPage = .BoldHeaderPage
                        bCapHeaderTOC = .CapHeaderTOC
                        bCapHeaderPage = .CapHeaderPage
                        bUnderlineHeaderTOC = .UnderlineHeaderTOC
                        bUnderlineHeaderPage = .UnderlineHeaderPage
                        bPageNoContinue = .ContinuePageNumbering
                        iPageNoStyle = .NumberStyle
                        iPageNoPunctuation = .NumberPunctuation
                    End With
                Else
                    With oDlg
                        'store user choices
                        iMaxLevel = .cbxMaxLevel.Value
                        iMinLevel = .cbxMinLevel.Value

                        '       get inclusions/exclusions
                        If .optIncludeSchemes.Checked Then
                            '           schemes
                            xInclusions = .Inclusions
                            xExclusions = .Exclusions
                        Else
                            '           individual styles
                            xInclusions = .StyleInclusions
                            xExclusions = .StyleExclusions
                        End If

                        xInsertAt = .cbxInsertAt.Text
                        bIncludeStyles = .optIncludeStyles.Checked
                        xScheduleStyle = .cbxScheduleStyles.Text
                        bApplyTOC9 = .chkApplyTOC9.Checked
                        bTwoColumn = .chkTwoColumn.Checked
                        bApplyManualFormats = .chkApplyManualFormatsToTOC.Checked
                        bUpdateTOCStyles = .optTOCStylesUpdate.Checked
                        bStyles = .chkStyles.Checked
                        bTCEntries = .chkTCEntries.Checked
                        iHeadingDef = .cmbTextStyles.SelectedIndex
                        xTOCDisplayName = .cbxTOCScheme.Text
                        xTOCScheme = .TOCScheme
                        bBoldHeaderTOC = .BoldHeaderTOC
                        bBoldHeaderPage = .BoldHeaderPage
                        bCapHeaderTOC = .CapHeaderTOC
                        bCapHeaderPage = .CapHeaderPage
                        bUnderlineHeaderTOC = .UnderlineHeaderTOC
                        bUnderlineHeaderPage = .UnderlineHeaderPage
                        bPageNoContinue = .ContinuePageNumbering
                        iPageNoStyle = .NumberStyle
                        iPageNoPunctuation = .NumberPunctuation
                    End With
                End If

                '       convert TOC location from string to integer
                Select Case xInsertAt
                    Case mpTOCLocationEOF, mpTOCLocationEOFFrench
                        iLoc = DocPosition.mpAtEOF
                    Case mpTOCLocationBOF, mpTOCLocationBOFFrench
                        iLoc = DocPosition.mpAtBOF
                    Case mpTOCLocationAboveTOA, mpTOCLocationAboveTOAFrench
                        iLoc = DocPosition.mpAboveTOA
                    Case Else
                        iLoc = DocPosition.mpAtInsertion
                End Select

                bIncludeSchedule = InStr(UCase(xInclusions), ",SCHEDULE,")
                bIncludeOther = InStr(UCase(xInclusions), ",OTHER,")
                If bApplyTOC9 Then
                    xTOC9Style = xScheduleStyle
                End If

                '       if including Schedule styles, ensure that outline level is in range
                If bIncludeStyles Or bIncludeSchedule Then
                    SetScheduleLevels(iMinLevel, xTOC9Style)
                End If

                '       get TOC format
                udtTOCFormat = udtGetTOCFormat(xTOCScheme, bApplyTOC9)

                '       get content type from option btns & chk boxes
                If bStyles Then
                    If iHeadingDef = 1 Then
                        iEntryType = mpTOCEntryTypes.mpTOCEntryType_Sentence
                    ElseIf iHeadingDef = 0 Then
                        iEntryType = mpTOCEntryTypes.mpTOCEntryType_PeriodSpace
                    Else
                        iEntryType = mpTOCEntryTypes.mpTOCEntryType_Para
                    End If
                End If

                If bTCEntries Then
                    iEntryType = iEntryType + mpTOCEntryTypes.mpTOCEntryType_TCEntries
                End If

                iDefaultTCLevel = 0

                '       Word only allows 17 styles to be
                '       explicitly included in toc  - if there
                '       are more than 17, tag all outline styles
                '       between min and max levels, else tag
                '       only those outline level styles in the
                '       inclusion list that are between the min
                '       and max levels
                If bIncludeStyles Then
                    iNumStyles = lCountChrs(xInclusions, ",") - 1
                Else
                    iNumStyles = iMaxLevel * (lCountChrs(xInclusions, ",") - 1)
                    If bIncludeOther Then
                        '               we need to bring in all styles
                        iNumStyles = 18
                    ElseIf bIncludeSchedule Then
                        '               schedule styles are not one per level - adjust total
                        iNumStyles = iNumStyles - (iMaxLevel - iMinLevel) + UBound(g_xScheduleStyles)
                    End If
                End If

                If iNumStyles <= 17 Then
                    If bIncludeStyles Then
                        xStyleList = xGetDesignatedStyles(xInclusions, "")
                    Else
                        xStyleList = xGetStyleList(xInclusions, _
                                                   iMinLevel, _
                                                   iMaxLevel, _
                                                   bApplyTOC9, _
                                                   xScheduleStyle)
                    End If
                End If

                '       call any custom code
                lRet = oCustTOC.lBeforeTOCSectionInsert()

                '       set location for insertion of TOC -
                '       MacPac TOC is bookmarked as mpTableOfContents
                'OutputDebugString "Before section insert"
                rngLocation = rngGetTOCTarget(CurWordApp.ActiveDocument, _
                                                  iLoc, _
                                                  bBoldHeaderTOC, _
                                                  bBoldHeaderPage, _
                                                  bCapHeaderTOC, _
                                                  bCapHeaderPage, _
                                                  bUnderlineHeaderTOC, _
                                                  bUnderlineHeaderPage, _
                                                  iPageNoStyle, _
                                                  iPageNoPunctuation, _
                                                  Not bPageNoContinue)

                '       format columns
                With rngLocation.Sections(1).PageSetup.TextColumns
                    If bTwoColumn And (.Count = 1) Then
                        .SetCount(2)
                        .Spacing = CurWordApp.InchesToPoints(0.5)
                        .EvenlySpaced = True
                        bColumnsReformatted = True
                    ElseIf (Not bTwoColumn) And (.Count = 2) Then
                        .SetCount(1)
                        bColumnsReformatted = True
                    End If
                End With

                'OutputDebugString "After section insert"

                '       call any custom code
                lRet = oCustTOC.lAfterTOCSectionInsert(rngLocation, _
                    bColumnsReformatted)

                If bInsertAsField Then
                    'if generating from TC fields and leaving TOC field linked,
                    'we need to fill tc codes; since we're also advertising this
                    'as a shortcut to avoid having to run the "Convert To Word TC Codes"
                    'after running mark all, we need to fill dynamic codes even when
                    'these entries were actually generated from styles; but since
                    'CleanUpTOCField can't delete the style list after generation from
                    'both TC fields and styles, because it can't assume that all
                    'numbered paras have been marked, it becomes a training issue
                    'to generate from TC fields ONLY in this situation, to avoid double
                    'entries after a native update; the real answer is to offer filled TC
                    'codes as an option in Mark All and to only fill partially dynamic codes here
                    If bTCEntries Then
                        RestoreMPTCCodes()
                        FillTCCodes(bApplyManualFormats, False)
                    End If
                Else
                    '           tag document paras with macpac codes
                    'OutputDebugString "Before tagging TOC entries"
                    TagTOCEntries(iMinLevel, _
                                  iMaxLevel, _
                                  iEntryType, _
                                  (iNumStyles > 17), _
                                  xStyleList, _
                                  bApplyTOC9, _
                                  xScheduleStyle, _
                                  bIncludeStyles)
                    'OutputDebugString "After tagging TOC entries"
                End If

                '       fill partially dynamic codes with text
                '       from left of code to beginning of para
                '9.9.5005 - made conditional on .chkTCEntries
                If bTCEntries Then _
                    iRefreshPDynCodes(0, "", , bApplyManualFormats)
                'OutputDebugString "After updating TC entries"

                '       Reset TOC scheme based on user choice if specified
                If bUpdateTOCStyles Or bApplyTOC9 Then
                    xResetTOCStyles(xTOCScheme, _
                                    rngLocation.Sections(1), _
                                    udtTOCFormat.CenterLevel1, _
                                    bApplyTOC9, _
                                    Not bUpdateTOCStyles)
                End If

                '       reset right tab stop if switching column format or if
                '       style update was requested by user
                If bUpdateTOCStyles Or bColumnsReformatted Then
                    ResetRightTabStop(rngLocation.Sections(1))
                End If

                'OutputDebugString "After resetting styles"

                ''   if leaving TOC as a field, add dummy entry at top of doc;
                ''   this will make it easier to work around issues related
                ''   to editing first paragraph of TOC field result
                '    If dlgTOC.chkInsertAsField Then
                ''       create style
                '        On Error Resume Next
                '        Set oDummyStyle = CurWordApp.ActiveDocument.Styles(mpDummyStyle)
                '        On Error GoTo ProcError
                '        If oDummyStyle Is Nothing Then
                '            Set oDummyStyle = CurWordApp.ActiveDocument.Styles.Add(mpDummyStyle)
                '        End If
                '        With oDummyStyle
                '            .BaseStyle = wdBuiltInStyle.wdStyleNormal
                '            .ParagraphFormat.OutlineLevel = iMinLevel
                '            .Font.Hidden = False
                '        End With
                '
                ''       insert para
                '        If rngLocation.Sections(1).Index = 1 Then
                '            iDummySec = 2
                '        Else
                '            iDummySec = 1
                '        End If
                '        Set rngDummyPara = CurWordApp.ActiveDocument.Sections(iDummySec).Range
                '        With rngDummyPara
                '            .StartOf
                '            .InsertParagraphAfter
                '            .StartOf
                '            .Style = mpDummyStyle
                '            .Text = "x"
                '            .Expand wdUnits.wdParagraph
                '        End With
                '
                ''       add to front of style list
                '        xStyleList = mpDummyStyle & "," & iMinLevel & "," & xStyleList
                '    End If

                '   count number of styles to be included for TOC Entries
                If bIncludeStyles Then
                    xLevRange = "1-9"
                Else
                    xLevRange = iMinLevel & "-" & iMaxLevel
                End If

                '   Word only allows 17 styles to be
                '   explicitly included in toc - go figure
                If bStyles Or Not bInsertAsField Then
                    If iNumStyles > 17 Then
                        '           create TOC based on TC Entries
                        '           and outline level paragraphs
                        xTOCParams = "\o """ & xLevRange & """" & "\w"
                    Else
                        '           if system decimal separator is comma, use semi-colons in field
                        '9.9.3004 - get system list separator and adjust string if necessary -
                        'we were previously inferring the list separator from the decimal separator
                        If CurWordApp.International(WdInternationalIndex.wdListSeparator) <> "," Then
                            xStyleList = xSubstitute(xStyleList, ",", _
                                CurWordApp.International(WdInternationalIndex.wdListSeparator))
                        End If

                        '           create TOC from explicit
                        '           style list and tc entries
                        xTOCParams = "\t """ & xStyleList & """" & "\w"
                    End If
                End If

                '   preserve line breaks
                If bInsertAsField Then
                    'TOC field - preserve only for schemes specified in ini
                    For i = 0 To UBound(g_vPreserveLineBreaks)
                        If xTranslateTOCSchemeDisplayName(CStr(g_vPreserveLineBreaks(i))) = _
                                xTOCDisplayName Then
                            bPreserveLineBreaks = True
                            Exit For
                        End If
                    Next i
                Else
                    'text TOC - always preserve
                    bPreserveLineBreaks = True
                End If
                If bPreserveLineBreaks Then _
                    xTOCParams = xTOCParams & " \x"

                '   bring in static tc codes if specified
                If bTCEntries Then
                    xTOCParams = xTOCParams & " \l """ & _
                                 xLevRange & """"
                End If

                '   prevent hidden text from
                '   affecting pagination of TOC
                With CurWordApp.ActiveWindow.View
                    .ShowAll = False
                    .ShowHiddenText = False
                    .ShowFieldCodes = False
                End With
                'OutputDebugString "Before inserting field"

                '   add toc
                g_oStatus.UpdateProgress(4)
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    CurWordApp.StatusBar = "Word ins�re un champ Table des mati�res..."
                Else
                    CurWordApp.StatusBar = "Word is inserting Table of Contents field..."
                End If

                fldTOC = CurWordApp.ActiveDocument.Fields.Add( _
                    rngLocation, WdFieldType.wdFieldTOC, xTOCParams)
                'OutputDebugString "After inserting field"

                '   call any custom code
                lRet = oCustTOC.lAfterTOCFieldInsert(fldTOC)

                '   bookmark toc
                rngGetField(fldTOC.Code).Bookmarks.Add("mpTableOfContents")
                rngTOC = CurWordApp.ActiveDocument.Bookmarks("mpTableOfContents").Range

                With rngTOC
                    If Asc(.Characters.Last.Text) = 12 Then
                        rngTOC.MoveEnd(WdUnits.wdCharacter, -1)
                    End If

                    '       set all tags as hidden
                    '       text to prevent them from
                    '       affecting pagination
                    'OutputDebugString "Before hiding tags"
                    If Not bInsertAsField Then
                        If Not g_bPreserveUndoListTOC Then _
                            CurWordApp.ActiveDocument.UndoClear()
                        HideTags(bShowStatus:=True)
                        If Not g_bPreserveUndoListTOC Then _
                            CurWordApp.ActiveDocument.UndoClear()
                    End If
                    'OutputDebugString "After hiding tags"

                    ''       hide dummy paragraph
                    '        If dlgTOC.chkInsertAsField Then _
                    '            rngDummyPara.Font.Hidden = True

                    '       update page numbers
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        CurWordApp.StatusBar = "Mise � jour les num�ros de page..."
                    Else
                        CurWordApp.StatusBar = "Word is updating page numbers in Table of Contents..."
                    End If
                    g_oStatus.UpdateProgress(42)
                    CurWordApp.ActiveDocument.TablesOfContents(1) _
                        .UpdatePageNumbers()
                    'OutputDebugString "After updating page numbers"

                    ''       show dummy paragraph
                    '        If dlgTOC.chkInsertAsField Then _
                    '            rngDummyPara.Font.Hidden = False

                    '       make toc plain text
                    If Not bInsertAsField Then
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            CurWordApp.StatusBar = "Conversion de la Table des mati�res en texte..."
                        Else
                            CurWordApp.StatusBar = "Converting Table of Contents to text..."
                        End If
                        g_oStatus.UpdateProgress(47)
                        .Fields.Unlink()
                        'OutputDebugString "After unlinking"
                    End If

                    '       show hidden text prior to
                    '       cleaning up doc and toc - this will
                    '       guarantee that codes that are hidden
                    '       are deleted if specified
                    With CurWordApp.ActiveWindow.View
                        .ShowHiddenText = True
                        .ShowAll = True
                    End With

                    '       do only if TOC has entries
                    If Not bIsTOCStyle(.Paragraphs.First.Style.NameLocal) Then
                        '           return doc to original state
                        CleanUpDoc()
                        With rngTOC
                            If .Characters.Last.Text = vbCr Then _
                                .MoveEnd(WdUnits.wdCharacter, -1)
                            .Delete()
                        End With
                        '        ElseIf dlgTOC.chkInsertAsField And (.Paragraphs.Count = 2) Then
                        ''           field contains only dummy entry - return doc to original state
                        '            CleanUpDoc
                        '            rngTOC.Fields(1).Delete
                    Else
                        '           change content of toc and format
                        '           based on user dlg choices
                        '           remove manual font formats if specified
                        If Not bApplyManualFormats Then
                            If rngTOC.Font.Name <> "" Then
                                rngTOC.Font.Reset()
                            Else
                                'GLOG 2793 - inconsistent font - do targeted
                                'reset to preserve bullets
                                ResetTOCFont(rngTOC)
                            End If
                        End If

                        '           call any custom code
                        lRet = oCustTOC.lBeforeTOCRework(rngTOC)

                        'OutputDebugString "Before reworking"


                        If Not bInsertAsField Then
                            bReworkTOC(rngTOC, _
                                       xTOCDisplayName, _
                                       xExclusions, _
                                       bStyles, _
                                       bTCEntries, _
                                       udtTOCFormat, _
                                       xTOC9Style, _
                                       bIncludeStyles, _
                                       bApplyManualFormats)
                        End If

                        'OutputDebugString "After reworking"

                        '           call any custom code
                        lRet = oCustTOC.lAfterTOCRework()

                        '           clean up placeholder text in TOC
                        If Not bInsertAsField Then _
                            CleanUpTOC(rngTOC)
                        'OutputDebugString "After cleaning up TOC"

                        ''           prepare TOC field for potential native update
                        '            If dlgTOC.chkInsertAsField Then
                        '                bIsCentered = (CurWordApp.ActiveDocument.Styles(wdBuiltInStyle.wdStyleTOC1) _
                        '                    .ParagraphFormat.Alignment = wdAlignParagraphCenter)
                        '                CleanUpTOCField rngTOC, Not bIsCentered, _
                        '                    (iEntryType = mpTOCEntryType_TCEntries)
                        '                'OutputDebugString "After cleaning up TOC field"
                        '            End If

                        '           ensure that toc is not colored
                        .Font.ColorIndex = WdColorIndex.wdAuto

                        '           add warning not to delete trailing para
                        If .Sections.Last.Index = CurWordApp.ActiveDocument.Sections.Count Then
                            .EndOf()
                        Else
                            'GLOG 5393 - this code appears to be obsolete and
                            'is problematic when the document starts with an
                            'intentional empty paragraph between the existing TOC
                            'and subsequent text in the same section - I haven't
                            'been able to recreate any scenario in which we currently
                            'insert an extraneous paragraph
                            ''               delete extraneous paragraph
                            '                On Error Resume Next
                            '                If .Characters.Count > 1 Then
                            '                    If .Next(wdUnits.wdParagraph).Text = vbCr Then _
                            '                        .Next(wdUnits.wdParagraph).Delete
                            '                End If
                            '                On Error GoTo ProcError
                        End If

                        '           return doc to original state
                        If Not g_bPreserveUndoListTOC Then _
                            CurWordApp.ActiveDocument.UndoClear()
                        If Not bInsertAsField Then _
                            CleanUpDoc()
                        'OutputDebugString "After cleaning up document"

                    End If
                End With

                ''   delete dummy para and style
                '    If dlgTOC.chkInsertAsField Then
                '        With rngDummyPara
                '            If .Style = oDummyStyle Then
                '                .Delete
                '                oDummyStyle.Delete
                '            End If
                '        End With
                '    End If

                '   in Word 97, all caps and bold need to be done at end
                If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                    xScheme = "Personnalis�"
                Else
                    xScheme = "Custom"
                End If
                If (xTOCDisplayName = xScheme) And bUpdateTOCStyles Then
                    CurWordApp.ScreenRefresh()
                    CurWordApp.ScreenUpdating = False
                    For i = 0 To 8
                        With CurWordApp.ActiveDocument.Styles(xTranslateTOCStyle(i + 1)).Font
                            .AllCaps = udtTOCFormat.AllCaps(i)
                            .Bold = udtTOCFormat.Bold(i)
                        End With
                    Next i
                End If

                '   clear tc codes if specified
                'OutputDebugString "Before resetting TC entries"
                '9.9.4019 - this control defaulted checked and was hidden -
                'got rid of it with the switchover to the native tab control
                'If dlgTOC.chkClearTCCodes Then
                '9.9.5005 - made conditional on .chkTCEntries
                If bTCEntries Then _
                    bRet = bResetTCEntries()
                'OutputDebugString "After resetting TC entries"

                '   All TOC entries may have been marked for deletion
                If Not CurWordApp.ActiveDocument.Bookmarks.Exists("mpTableOfContents") Then _
                    CurWordApp.ActiveDocument.Bookmarks.Add("mpTableOfContents", rngTOC)

                'hide status display
                g_oStatus.Close()
                g_oStatus.Dispose()
                'g_oStatus.Hide()
                g_oStatus = Nothing
                CurWordApp.ScreenUpdating = True
                CurWordApp.ScreenRefresh()

                'GLOG 5147 - ensure that focus remains on active document
                CurWordApp.Activate()
                oDoc.Activate()

                '   alert if toc is empty
                If Len(CurWordApp.ActiveDocument.Bookmarks("mpTableOfContents").Range.Text) < 2 Then
                    With rngTOC
                        '           move before "Do Not Delete" warning
                        Try
                            If .Previous(WdUnits.wdParagraph).Text = _
                                    mpEndOfTOCWarning & vbCr Then
                                .Move(WdUnits.wdParagraph, -1)
                            End If
                        Catch
                        End Try
                        '           insert warning in document
                        If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                            .InsertBefore(mpNoEntriesWarningFrench)
                        Else
                            .InsertBefore(mpNoEntriesWarning)
                        End If
                        .Style = WdBuiltinStyle.wdStyleNormal
                        .HighlightColorIndex = WdColorIndex.wdNoHighlight
                        .Bold = True
                    End With
                    '       rebookmark
                    CurWordApp.ActiveDocument.Bookmarks.Add("mpTableOfContents", rngTOC)
                    '       present dlg alert
                    If g_lUILanguage = WdLanguageID.wdFrenchCanadian Then
                        MsgBox("La Table des mati�res est vide.  " & vbCr & _
                               "Aucune entr�e TM n'a �t� trouv� dans ce document.", _
                               vbExclamation, "Num�rotation TSG")
                    Else
                        MsgBox("The Table Of Contents is empty.  " & vbCr & _
                               "No valid Table Of Contents entries " & _
                               "could be found in the document.", vbExclamation, AppName)
                    End If
                End If

                oCustTOC = Nothing
                '   select start of TOC - force
                '   vertical scroll to selection
                With CurWordApp.ActiveDocument
                    .Range(0, 0).Select()
                    .Bookmarks("mpTableOfContents").Select()
                End With
                CurWordApp.Selection.StartOf()

                '   restore user's smart quotes setting
                CurWordApp.Options _
                    .AutoFormatAsYouTypeReplaceQuotes = bSmartQuotes

                '   restore outline levels of schedule styles
                If bIncludeStyles Or bIncludeSchedule Then
                    RestoreScheduleLevels()
                End If

                '   tsgTOC.sty is now only loaded as needed (12/10/01);
                '   9.8.1007 - tsgTOC.sty is now left loaded
                '    UnloadTOCSty

                '   reset environment
                'OutputDebugString "Before resetting environment"
                With CurWordApp.ActiveWindow.View
                    .ShowAll = bShowAll
                    .ShowHiddenText = bShowHidden
                    .Type = iView
                    .Zoom.Percentage = lZoom
                End With

                'restore xml tags
                If g_bXMLSupport Then _
                    lShowTags = SetXMLMarkupState(CurWordApp.ActiveDocument, lShowTags)

                'GLOG 5057 - restore setting
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = True

                EchoOn()
                bInsertTOC = True

                'OutputDebugString "End of TOC macro"

                '9.9.5005
                If g_bDisplayBenchmarks Then
                    sElapsedTime = ElapsedTime(lStartTick)
                    MsgBox(CStr(sElapsedTime))
                End If
            Finally
                If Not g_oStatus Is Nothing Then
                    g_oStatus.Close()
                    g_oStatus.Dispose()
                    g_oStatus = Nothing
                End If

                CurWordApp.ScreenUpdating = True
                CurWordApp.ScreenRefresh()

                'GLOG 5057 - restore setting
                If bPrintHiddenText Then _
                    CurWordApp.Options.PrintHiddenText = True

                If Not dlgTOC Is Nothing Then
                    dlgTOC.Close()
                    dlgTOC = Nothing
                End If
                EchoOn()
            End Try

        End Function

        Private Shared Sub ResetTOCFont(ByVal rngTOC As Word.Range)
            'added for GLOG 2793 - resets font paragraph by paragraph
            'in order to avoid changing font of bullets
            Dim oPara As Word.Paragraph
            Dim rngTarget As Word.Range
            Dim oStyle As Word.Style
            Dim xAppliedFont As String
            Dim rngCharacter As Word.Range

            For Each oPara In rngTOC.Paragraphs
                rngTarget = oPara.Range
                rngCharacter = rngTarget.Characters(1)
                xAppliedFont = rngCharacter.Font.Name
                If (xAppliedFont = "") Or (xAppliedFont = "Symbol") Or _
                        (xAppliedFont = "Webdings") Or _
                        (Strings.Left$(xAppliedFont, 9) = "Wingdings") Then
                    oStyle = oPara.Style
                    rngCharacter.Font.Bold = oStyle.Font.Bold
                    rngCharacter.Font.Underline = oStyle.Font.Underline
                    rngTarget.MoveStart()
                End If
                rngTarget.Font.Reset()
            Next oPara
        End Sub

        Private Shared Function CreateCTOCObject() As iCTOC
            Dim xCTOCAssembly As String
            Dim oFile As FileInfo
            Dim oAsm As Assembly

            xCTOCAssembly = Assembly.GetExecutingAssembly().CodeBase
            xCTOCAssembly = xCTOCAssembly.Replace("file:///", "")
            'JTS 10/6/16: Directory is part of CodeBase
            oFile = New FileInfo(xCTOCAssembly)
            xCTOCAssembly = oFile.Directory.FullName & "\" & "CTOC.dll"
            oAsm = Assembly.LoadFrom(xCTOCAssembly)
            Dim oObject As Object = oAsm.CreateInstance("LMP.Numbering.CustomTOC.cCustomTOC", True)
            oObject.CurWordApp = g_oWordApp
            CreateCTOCObject = oObject
        End Function
    End Class
End Namespace

