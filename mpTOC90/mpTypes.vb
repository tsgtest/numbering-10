Option Explicit On

Namespace LMP.Numbering.TOC
    Friend Class mpTypes
        Structure mpDocEnvironment
            Public bShowAll As Boolean
            Public sVScroll As Single
            Public sHScroll As Single
            Public iView As Integer
            Public bFieldCodes As Boolean
            Public bBookmarks As Boolean
            Public bTabs As Boolean
            Public bSpaces As Boolean
            Public bHyphens As Boolean
            Public bHiddenText As Boolean
            Public bParagraphs As Boolean
            Public bTextBoundaries As Boolean
            Public iProtectionType As Integer
            Public iSelectionStory As Integer
            Public lSelectionStartPos As Long
            Public lSelectionEndPos As Long
            Public bTrackChanges As Boolean
        End Structure

        Structure mpAppEnvironment
            Public lBrowserTarget As Long
        End Structure
    End Class
End Namespace