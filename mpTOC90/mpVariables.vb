Option Explicit On

Imports LMP.Numbering.TOC.mpTypes
Imports LMP.Numbering.CustomTOC
Imports LMP.Numbering.TOC.mpApplication

Namespace LMP.Numbering.TOC
    Friend Class mpVariables
        'global environment vars
        Public Shared envMpApp As mpAppEnvironment
        Public Shared envMpDoc As mpDocEnvironment

        'paths
        Public Shared xStartPath As String
        Public Shared xWorkgroupPath As String
        Public Shared xUserPath As String
        Public Shared xLitigationPath As String

        'files
        Public Shared xBP As String
        Public Shared xTOCSTY As String
        Public Shared xFirmXMLConfig As String

        'misc
        Public Shared bRet As Object
        Public Shared lRet As Long
        Public Shared xMsg As String
        Public Shared iUserChoice As Integer
        Public Shared xFirmName As String
        Public Shared g_oStatus As MacPac.ProgressForm
        Public Shared g_bIsXP As Boolean
        Public Shared g_xSmartOne As String
        Public Shared g_xSmartTwo As String
        Public Shared g_xBullet As String
        Public Shared g_bIsMP10 As Boolean
        Public Shared g_bXMLSupport As Boolean
        Public Shared g_bOrganizerSavePrompt As Boolean

        'options
        Public Shared g_xTOCLocations() As String
        Public Shared g_xBoilerplates(,) As String
        Public Shared g_bAllowHeaderFooterEdit As Boolean
        Public Shared g_xPageNoIntroChar As String
        Public Shared g_xPageNoTrailingChar As String
        Public Shared g_iPageNoStyle As Integer
        Public Shared g_bApplyHeadingColor As Boolean
        Public Shared g_xDSchemes(,) As String
        Public Shared g_xAbbreviations() As String
        Public Shared g_xSentenceDefs(,) As String
        Public Shared g_bIncludeSchedule As Boolean
        Public Shared g_xScheduleStyles() As String
        Public Shared g_xScheduleLevels(,) As String
        Public Shared g_bAllowTOCAsField As Boolean
        Public Shared g_bInsertTOCAsField As Boolean
        Public Shared g_bForceTCEntries As Boolean
        Public Shared g_bForceLevels As Boolean
        Public Shared g_iDefaultLevelStart As Integer
        Public Shared g_iDefaultLevelEnd As Integer
        Public Shared g_bApplyTOC9StyleDefault As Boolean
        Public Shared g_vPreserveLineBreaks As Object
        Public Shared g_bPreserveUndoList As Boolean
        Public Shared g_bPreserveUndoListTOC As Boolean
        Public Shared g_bUseTOCStyleLimitWorkaround As Boolean
        Public Shared g_bMarkWithStyleSeparators As Boolean
        Public Shared g_bUseNewTabAdjustmentRules As Boolean
        Public Shared g_iTOCDialogStyle As LMP.Numbering.TOC.mpTOC.mpTOCDialogStyles
        Public Shared g_iIncludeInTOCDefault As Integer
        Public Shared g_bVariablesInBoilerplate As Boolean
        Public Shared g_bDisplayBenchmarks As Boolean

        'forms
        Public Shared oCustTOC As iCTOC

        'language
        Public Shared g_xTCPrefix As String
        Public Shared g_lUILanguage As Long
        Public Shared g_xTOCStatusMsg As String
    End Class
End Namespace

