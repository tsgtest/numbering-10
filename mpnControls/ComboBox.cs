using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace mpnControls
{
	/// <summary>
	/// ComboBox class. Defines a MacPac combobox. 
	/// Created by Dan Fisherman 1/05.
	/// </summary>
    public class ComboBox : System.Windows.Forms.UserControl, mpnControls.IControl
    {
        #region *********************fields*********************
        private string m_xList = "";
        private bool m_bBorderless = false;
        private bool m_bLimitToList = true;
        private bool m_bNavigating = false;
        private bool m_bMatching = false;
        private bool m_bIsDirty = false;
        private object m_oTag2;
        private bool m_bProgrammaticallySettingValue;
        private bool m_bPopulatingList;
        private bool m_bAllowEmptyValue;
        private int m_iSelectedIndex = -1;
        private string m_xSupportingValues = "";
        private bool m_bControlIsLoaded = false;
        private bool m_bListLoaded = false;
        private SortOrder m_iSortOrder = SortOrder.None;
        private TextBox textBox1;
        private bool m_bIsValid = false;
        private Timer m_oKeyTimer = null; //GLOG 4548
        private bool m_bUseTimer = false;
        //GLOG : 6444 : CEH
        protected System.Windows.Forms.ComboBox comboBox1;
        protected ContextMenu mnuContext;
        protected MenuItem mnuContext_Clear;
        protected MenuItem menuItem1;
        protected MenuItem mnuContext_Cut;
        protected MenuItem mnuContext_Copy;
        protected MenuItem mnuContext_Paste;
        private bool m_bInPaintEvent = false;  //GLOG 8214
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        #endregion
        #region *********************constructors*********************
        public ComboBox()
        {
            InitializeComponent();
            //GLOG 4548
            m_oKeyTimer = new Timer();
            m_oKeyTimer.Stop();
            m_oKeyTimer.Interval = 300;
            m_oKeyTimer.Tick += new EventHandler(m_oKeyTimer_Tick);
        }
        #endregion
        #region *********************properties*********************
        public System.Windows.Forms.ComboBox.ObjectCollection Items
        {
            get { return this.comboBox1.Items; }
        }
        [DescriptionAttribute("Gets/Sets the text of the control.")]
        public override string Text
        {
            get { return this.comboBox1.Text; }
            set
            {
                string xCurVal = this.comboBox1.Text;
                //set text property only if limit to list is false
                if (!(this.LimitToList))
                {
                    this.comboBox1.SelectedIndex = -1;
                    //GLOG 3368: Setting SelectedValue here was also changing SelectedIndex,
                    //causing Manually-entered text to get replaced
                    //this.comboBox1.SelectedValue = "";
                    this.comboBox1.Text = value;
                }

                if ((this.LimitToList) && (this.comboBox1.SelectedIndex == -1))
                {
                    //set text back to initial value
                    this.comboBox1.Text = xCurVal;

                    //specified text not in the list - alert
                    //throw new LMP.Exceptions.NotInCollectionException(
                    //    LMP.Resources.GetLangString("Error_CouldNotSetComboText") + value);
                }
            }
        }
        /// <summary>
        /// gets the list items of the control as a string
        /// </summary>
        [BrowsableAttribute(false)]
        public string ListString
        {
            get
            {
                string xList = "";
                DataTable oDT = (DataTable)this.comboBox1.DataSource;

                if (oDT == null)
                    return "";

                //populate by cycling through list items
                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    for (int j = 0; j < oDT.Columns.Count; j++)
                        xList = xList + oDT.Rows[i][j].ToString() + "|";
                }

                return xList;
            }
        }

        [DescriptionAttribute("Gets/sets whether empty value is allowed in Limit-to-list combo.")]
        public bool AllowEmptyValue
        {
            get { return m_bAllowEmptyValue; }
            set { m_bAllowEmptyValue = value; }
        }

        /// <summary>
        /// gets the list items of the control as an array
        /// </summary>
        //[BrowsableAttribute(false)]
        public string[,] ListArray
        {
            get
            {
                DataTable oDT = (DataTable)this.comboBox1.DataSource;

                if (oDT == null)
                    return null;

                //create a new string array
                string[,] aList = new string[oDT.Rows.Count, oDT.Columns.Count];

                //populate by cycling through list items
                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    for (int j = 0; j < oDT.Columns.Count; j++)
                        aList[i, j] = oDT.Rows[i][j].ToString();
                }

                return aList;
            }
        }

        /// <summary>
        /// gets the list items of the control as DataTable
        /// </summary>
        //[BrowsableAttribute(false)]
        public DataTable ListDataTable
        {
            get
            {
                DataTable oDT = (DataTable)this.comboBox1.DataSource;

                if (oDT == null)
                    return null;

                return oDT;
            }
        }


        [DescriptionAttribute("Gets/Sets sort order for Displayed list items.")]
        public SortOrder SortOrder
        {
            get { return m_iSortOrder; }
            set { m_iSortOrder = value; }
        }
        [DescriptionAttribute("Gets/Sets whether user must select an item from the list.")]
        public bool LimitToList
        {
            get { return m_bLimitToList; }
            set { m_bLimitToList = value; }
        }
        [DescriptionAttribute("Gets/Sets the backcolor of the control.")]
        public override System.Drawing.Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
                if (this.comboBox1 != null)
                    this.comboBox1.BackColor = value;
            }
        }

        [DescriptionAttribute("Gets/Sets the font of the control.")]
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                if (this.comboBox1 != null)
                    this.comboBox1.Font = value;
            }
        }
        /// <summary>
        /// returns true iff either the combox is not set to
        /// limit to list or the value is an item in the
        /// dropdown list
        /// </summary>
        [Browsable(false)]
        public bool IsValid
        {
            get { return m_bIsValid; }
        }
        [DescriptionAttribute("Gets/Sets whether or not the control displays borders.")]
        public bool Borderless
        {
            get { return m_bBorderless; }
            set
            {
                if (m_bBorderless != value)
                {
                    m_bBorderless = value;
                    this.Invalidate();
                }
            }
        }
        [DescriptionAttribute("Gets/Sets the number of dropdown rows displayed.")]
        public int MaxDropDownItems
        {
            get { return this.comboBox1.MaxDropDownItems; }
            set { this.comboBox1.MaxDropDownItems = value; }
        }
        [DescriptionAttribute("Gets/Sets the index of the selected value.")]
        public int SelectedIndex
        {
            get { return this.comboBox1.SelectedIndex; }
            set
            {
                m_iSelectedIndex = Math.Max(value, -1);
                this.comboBox1.SelectedIndex = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public Object SelectedValue
        {
            get { return this.comboBox1.SelectedValue; }
            set
            {
                this.comboBox1.SelectedValue = value;
            }
        }
        [DescriptionAttribute("Gets/Sets the length of the selected text.")]
        public int SelectionLength
        {
            get { return this.comboBox1.SelectionLength; }
            set
            {
                if (this.DropDownStyle != ComboBoxStyle.DropDownList)
                    this.comboBox1.SelectionLength = Math.Max(value, 0);
            }
        }
        [DescriptionAttribute("Gets/Sets the first selected character.")]
        public int SelectionStart
        {
            get { return this.comboBox1.SelectionStart; }
            set
            {
                if (this.DropDownStyle != ComboBoxStyle.DropDownList)
                    this.comboBox1.SelectionStart = Math.Max(value, 0);
            }
        }
        [BrowsableAttribute(false)]
        protected bool ListLoaded
        {
            //GLOG 3668: Property to indicate that List has been set
            get { return m_bListLoaded; }
            set { m_bListLoaded = value; }
        }

        [DescriptionAttribute("Gets/Sets whether Timer is used to prevent immediate execution of ValueChanged after keystrokes.")]
        [BrowsableAttribute(false)]
        protected bool UseTimer //GLOG 4548
        {
            get { return m_bUseTimer; }
            set { m_bUseTimer = value; }
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// overloaded method takes ArrayList for listitems
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(ArrayList aList)
        {
            string[,] aTemp = new string[aList.Count, 2];
            for (int i = 0; i < aList.Count; i++)
            {
                //create string array and call set list to load
                if (aList[i] is string)
                {
                    aTemp[i, 0] = aList[i].ToString();
                    aTemp[i, 1] = aList[i].ToString();
                }
                else
                {
                    aTemp[i, 0] = ((Object[])aList[i])[1].ToString();
                    aTemp[i, 1] = ((Object[])aList[i])[0].ToString();
                }
            }
            SetList(aTemp);
        }

        /// <summary>
        /// populates the combo list with items in the specified System.Array
        /// </summary>
        /// <param name="aList"></param>
        public void SetList(Array aList)
        {
            int iNumCols = aList.GetUpperBound(1);
            string[,] aTemp = new string[aList.GetUpperBound(0) + 1, 2];

            if (iNumCols == 0)
            {
                for (int i = 0; i <= aList.GetUpperBound(0); i++)
                {
                    //create string array and call set list to load
                    aTemp[i, 0] = aList.GetValue(i).ToString();
                    aTemp[i, 1] = aTemp[i, 0];
                }
            }
            else if (iNumCols == 1)
            {
                for (int i = 0; i <= aList.GetUpperBound(0); i++)
                {
                    //populate temp array row
                    aTemp[i, 0] = aList.GetValue(i, 0).ToString();
                    aTemp[i, 1] = aList.GetValue(i, 1).ToString();
                }
            }

            SetList(aTemp);
        }

        /// <summary>
        /// populates the combo list with items in the specified System.Data.DataTable
        /// </summary>
        /// <param name="oDT"></param>
        public void SetList(DataTable oDT)
        {
            m_bPopulatingList = true;

            if (oDT.Columns.Count == 1)
            {
                //set column as both display and value
                this.comboBox1.DisplayMember = oDT.Columns[0].Caption;
                this.comboBox1.ValueMember = oDT.Columns[0].Caption;
            }
            else if (oDT.Columns.Count == 2)
            {
                //set first column for display, second column for value
                this.comboBox1.DisplayMember = oDT.Columns[0].Caption;
                this.comboBox1.ValueMember = oDT.Columns[1].Caption;
            }
            else
            {
                //invalid list
                //throw new LMP.Exceptions.ListException(
                //    LMP.Resources.GetLangString("Error_InvalidComboboxListArray"));
            }

            //GLOG 2915: Sort list items based on SortOrder property
            if (m_iSortOrder != SortOrder.None)
            {
                DataView oDV = oDT.DefaultView;
                
                if (m_iSortOrder == SortOrder.Descending)
                    oDV.Sort = this.comboBox1.DisplayMember + " DESC";
                else
                    oDV.Sort = this.comboBox1.DisplayMember + " ASC";

                //get sorted table
                oDT = oDV.ToTable();
            }

            //bind datatable to combobox
            this.comboBox1.DataSource = oDT;
            this.Invalidate();

            m_bPopulatingList = false;
            m_bListLoaded = true;
        }

        /// <summary>
        /// displays the items in the specified array
        /// </summary>
        /// <param name="aListItems"></param>
        public void SetList(string[,] aListItems)
        {
            DataTable oDT = new DataTable();

            m_bPopulatingList = true;

            //add columns to data table
            for (int i = 0; i <= aListItems.GetUpperBound(1); i++)
                oDT.Columns.Add("Column" + i.ToString());

            if (oDT.Columns.Count == 1)
            {
                //set column as both display and value
                this.comboBox1.DisplayMember = oDT.Columns[0].Caption;
                this.comboBox1.ValueMember = oDT.Columns[0].Caption;
            }
            else if (oDT.Columns.Count == 2)
            {
                //set first column for display, second column for value
                this.comboBox1.DisplayMember = oDT.Columns[0].Caption;
                this.comboBox1.ValueMember = oDT.Columns[1].Caption;
            }
            else
            {
                //invalid list
                //throw new LMP.Exceptions.ListException(
                //    LMP.Resources.GetLangString("Error_InvalidComboboxListArray"));
            }

            //Can't sort multi-dimensional array, so convert to ArrayList
            ArrayList aList = new ArrayList();
            for (int i = 0; i <= aListItems.GetUpperBound(0); i++)
            {
                string[] aTemp = new string[aListItems.GetUpperBound(1) + 1];
                for (int j = 0; j <= aListItems.GetUpperBound(1); j++)
                {
                    aTemp[j] = aListItems[i, j];
                }
                aList.Add(aTemp);
            }
            //GLOG 2915: Sort list items based on SortOrder property
            if (m_iSortOrder != SortOrder.None)
            {
                //Sort using IComparer
                aList.Sort(new ComboBoxSorter(m_iSortOrder == SortOrder.Descending));
            }
            //cycle through list item array, adding rows to data table
            for (int i = 0; i < aList.Count; i++)
            {
                DataRow oRow = oDT.Rows.Add();
                string[] aTemp = (string[])aList[i];
                for (int j = 0; j <= aTemp.GetUpperBound(0); j++)
                    oRow[j] = aTemp[j];
            }
            //bind list to combobox
            this.comboBox1.DataSource = oDT;
            this.Invalidate();

            m_bPopulatingList = false;
            m_bListLoaded = true;
        }
        /// <summary>
        /// clears data bound list
        /// </summary>
        public void ClearList()
        {
            this.comboBox1.DataSource = null;
            this.comboBox1.Items.Clear();
            this.Invalidate();
            m_bListLoaded = false;
        }
        #endregion
        #region *********************event handlers*********************
        private void ComboBox_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            try
            {
                //GLOG 8214: Avoid endless loop of Paint events
                if (m_bInPaintEvent)
                    return;
                m_bInPaintEvent = true;
                if (this.Borderless)
                    HideBorders();
                else
                {
                    //show borders by clearing white rectangle/lines
                    this.Refresh();
                    e.Graphics.Clear(this.BackColor);
                }
                m_bInPaintEvent = false;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                m_bInPaintEvent = false;
            }
        }
       private void comboBox1_Enter(object sender, System.EventArgs e)
        {
            try
            {
                if (!this.DesignMode && this.Visible)
                    mpnControls.Dialog.EnsureSelectedContent(this, false, false);
                this.Invalidate();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.DesignMode)
                    return;

                PerformValidation();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (!this.DesignMode)
                this.IsDirty = true;
        }
        private void comboBox1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                if (this.DesignMode)
                    return;
                m_oKeyTimer.Stop(); //GLOG 4548

                if ((e.KeyCode == Keys.Back ||
                            e.KeyCode == Keys.Left ||
                            e.KeyCode == Keys.Right ||
                            e.KeyCode == Keys.Up ||
                            e.KeyCode == Keys.Delete ||
                            e.KeyCode == Keys.Down ||
                            e.KeyCode == Keys.PageUp ||
                            e.KeyCode == Keys.PageDown ||
                            e.KeyCode == Keys.Home ||
                            e.KeyCode == Keys.End ||
                            e.KeyCode == Keys.Shift ||
                            e.KeyCode == Keys.Control ||
                            e.KeyCode == Keys.Alt ||
                            e.KeyCode == Keys.Tab ||  //GLOG 7981:  Don't start timer for tab or shift
                            e.KeyCode == Keys.ShiftKey ||
                            e.KeyCode == Keys.LShiftKey ||
                            e.KeyCode == Keys.RShiftKey))
                {
                    m_bNavigating = true;
                }
                else
                {

                    m_bNavigating = false;
                    //GLOG 4548
                    if (this.UseTimer)
                        m_oKeyTimer.Start();
                }

                if (e.KeyCode == Keys.Tab)
                {
                    m_oKeyTimer.Stop(); //GLOG 4548
                    //Container is handling tab - don't process further
                    //TabPressed event is raise by control only in ProcessKeyMessage
                    e.SuppressKeyPress = true;
                    e.Handled = true;
                    return;
                }

                if (this.KeyPressed != null)
                {
                    //raise keypressed event
                    this.KeyPressed(this, e);
                }

                base.OnKeyDown(e);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_TextChanged(object sender, System.EventArgs e)
        {
            try
            {
                //remove MacPac reserved characters from control -
                //we do this here, as opposed to a KeyDown-type
                //event so that we can capture pasting of reserved characters

                bool bContainsReservedChars = mpnControls.Dialog.ContainsMacPacReservedChars(this.Text);

                if (bContainsReservedChars)
                {
                    //get current insertion point position
                    int iSelStart = this.SelectionStart;

                    //remove reserved chars
                    this.Text = mpnControls.Dialog.RemoveMacPacReservedChars(this.Text);

                    //return insertion point
                    this.SelectionStart = Math.Max(iSelStart - 1, 0);
                }

                //don't execute handler code if this handler
                //was triggered by programmatically 
                //setting the value of the handler or loading the list - it
                //should only run when text is changed by user
                if (m_bProgrammaticallySettingValue || m_bPopulatingList)
                    return;

                //need to reference the accessibility object instead of the text
                //property because of bug in System.Windows.Forms.ComboBox -
                //see KB article #814346
                if (!string.IsNullOrEmpty(this.Text) && !m_bMatching && !m_bNavigating && this.comboBox1.AccessibilityObject.Value != null)
                    this.MatchEntry(this.comboBox1.AccessibilityObject.Value);

                this.IsDirty = true;
                //GLOG - 3951 - ceh
                if (!m_oKeyTimer.Enabled && this.ValueChanged != null && !m_bMatching) //GLOG 4548
                    this.ValueChanged(this, new System.EventArgs());
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                //raise keyreleased event
                this.KeyReleased(this, e);
        }
        private void comboBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (this.DesignMode)
                    return;

                //select next item in the list
                int iNext = this.comboBox1.SelectedIndex + 1;

                if (iNext > this.comboBox1.Items.Count - 1)
                    this.comboBox1.SelectedIndex = 0;
                else
                    this.comboBox1.SelectedIndex = iNext;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        /// <summary>
        /// raises selected value changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.DesignMode)
                    return;

                this.IsDirty = true;

                //GLOG item #6353 - dcf
                PerformValidation();

                //GLOG - 3951 - ceh
                if (!m_oKeyTimer.Enabled && this.ValueChanged != null && !m_bMatching) //GLOG 4548
                {
                    this.ValueChanged(this, new System.EventArgs());
                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void ComboBox_Enter(object sender, EventArgs e)
        {
            base.OnGotFocus(e);
        }
        private void comboBox1_MouseHover(object sender, EventArgs e)
        {
            if (this.Borderless)
                this.Invalidate();
        }

        private void comboBox1_MouseEnter(object sender, EventArgs e)
        {
            if (this.Borderless)
                this.Invalidate();
        }

        private void comboBox1_MouseLeave(object sender, EventArgs e)
        {
            if (this.Borderless)
                this.Invalidate();
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            if (this.Borderless)
                this.Invalidate();
        }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {
            if (this.Borderless)
                this.Invalidate();
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\t')
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// Raise LostFocus event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Leave(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }
        /// <summary>
        /// Timer raises ValueChanged event after extended match from keyboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void m_oKeyTimer_Tick(object sender, EventArgs e) //GLOG 4548
        {
            try
            {
                if (this.ValueChanged != null && !m_bMatching)
                    this.ValueChanged(this, new System.EventArgs());
                m_oKeyTimer.Stop();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            if (!this.IsValid)
            {
                //this.comboBox1.Select(0, this.comboBox1.Text.Length);

                //alert
                //GLOG 3926
                //MessageBox.Show(this.TopLevelControl, LMP.Resources.GetLangString("Msg_LimitedToList"),
                //    LMP.ComponentProperties.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //GLOG 8211
                SendKeys.SendWait("+{tab}");
            }
            else
            {
                //GLOG 8211
                SendKeys.SendWait("{tab}");
            }
        }

        //GLOG : 6444 : CEH
        private void mnuContext_Clear_Click(object sender, EventArgs e)
        {
            this.comboBox1.Text = "";
        }

        private void mnuContext_Copy_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.comboBox1.SelectedText, true);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void mnuContext_Cut_Click(object sender, EventArgs e)
        {
            try
            {
                //copy to clipboard
                Clipboard.SetDataObject(this.comboBox1.SelectedText, true);
                //cut
                this.comboBox1.SelectedText = "";
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }

        private void mnuContext_Paste_Click(object sender, EventArgs e)
        {
            try
            {
                this.comboBox1.SelectedText = Clipboard.GetText();
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }


        private void mnuContext_Popup(object sender, EventArgs e)
        {
            try
            {
                //enable/disable context menu items based on selection
                this.mnuContext_Copy.Enabled = (this.comboBox1.SelectedText != "");
                this.mnuContext_Cut.Enabled = (this.comboBox1.SelectedText != "");
                this.mnuContext_Clear.Enabled = (this.comboBox1.Text != "");

                //show/hide context menu items
                this.mnuContext_Cut.Visible = (!this.LimitToList);
                this.mnuContext_Paste.Visible = (!this.LimitToList);
                this.mnuContext_Clear.Visible = (!this.LimitToList);
                this.menuItem1.Visible = (!this.LimitToList);
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        private void comboBox1_Resize(object sender, EventArgs e)
        {
            try
            {
                //GLOG 8492: Ensure combobox text isn't highlighted when resized by autoscaling
                if (this.DesignMode || !comboBox1.IsHandleCreated)
                    return;  // avoid possible exception
                //match parent height to child control to avoid bottom shadow
                this.Height = comboBox1.Height;
                comboBox1.BeginInvoke(new Action(() =>
                {
                    var parent = this.FindForm();
                    if (parent == null)
                        return;

                    if (parent.ActiveControl == null)
                        return;
                    //Don't change selection if this is active control
                    if (parent.ActiveControl == this)
                        return;

                    comboBox1.SelectionLength = 0;
                }));
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************internal procedures*********************
        private void HideBorders()
        {
            //necessary for drawn elements to show
            this.comboBox1.Refresh();

            //get rectangular border to hide
            Rectangle oRect = this.comboBox1.ClientRectangle;

            //shift slightly
            oRect.Offset(1, 1);

            //draw a rectangle in color of combo background
            Graphics oG = this.comboBox1.CreateGraphics();
            oG.DrawRectangle(new Pen(this.comboBox1.BackColor, 2), oRect);

            //draw a bottom line in color of combo background-  
            //the rectangle alone leaves a thin bottom line
            oG.DrawLine(new Pen(Brushes.White, 4),
                oRect.X, oRect.Y + oRect.Height - 1,
                oRect.X + oRect.Width, oRect.Y + oRect.Height - 1);
        }

        private void PerformValidation()
        {
            m_bIsValid = true;

            if (this.LimitToList && this.comboBox1.Items.Count > 0)
            {
                //ensure that there is a match with a list item
                int iMatchIndex = this.comboBox1.FindStringExact(this.comboBox1.Text);

                if (iMatchIndex == -1 && (this.comboBox1.Text != "" || !this.AllowEmptyValue))
                {
                    ////there is no match
                    //e.Cancel = true;

                    m_bIsValid = false;

                    //select text
                    this.comboBox1.SelectionStart = 0;
                    this.comboBox1.SelectionLength = this.comboBox1.Text.Length;
                }
                else
                {
                    //ensure that match is selected
                    this.SelectedIndex = iMatchIndex;
                }
            }
        }

        /// <summary>
        /// autocompletes the combo entry
        /// </summary>
        private bool MatchEntry(string xEntry)
        {
            try
            {
                if (this.comboBox1.Items.Count == 0)
                    return false;

                m_bMatching = true;

                //get the number of characters in the text portion
                int iCharCount = xEntry.Length;

                //find a match in the list
                int iMatchIndex = this.comboBox1.FindString(xEntry);

                if (iMatchIndex > -1)
                {
                    //GLOG 3662: Force update of Selected Index if necessary
                    //Otherwise text won't get updated if matching entry is first in list
                    m_bProgrammaticallySettingValue = true;
                    if (this.SelectedIndex == iMatchIndex)
                        this.SelectedIndex = -1;
                    //there is a match - select the list item
                    this.SelectedIndex = iMatchIndex;

                    //select untyped characters
                    this.comboBox1.SelectionStart = iCharCount;
                    this.comboBox1.SelectionLength = this.comboBox1.Text.Length - iCharCount;
                }
                else if (this.LimitToList)
                {
                    //remove last char - attempt to match
                    this.comboBox1.Text = this.comboBox1.Text.Substring(
                        0, this.comboBox1.Text.Length - 1);
                    MatchEntry(this.comboBox1.Text);
                }
            }
            finally
            {
                m_bMatching = false;
                m_bProgrammaticallySettingValue = false;
            }

            return false;
        }
        private bool MatchEntry()
        {
            return MatchEntry(this.comboBox1.Text);
        }
        public ComboBoxStyle DropDownStyle
        {
            get { return this.comboBox1.DropDownStyle; }
            set { this.comboBox1.DropDownStyle = value; }
        }
        #endregion
        #region *********************protected members*********************
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        /// <summary>
        /// processes tab and shift-tab key messages -
        /// this method will be executed only when the
        /// message pump is broken, ie when the hosting
        /// form's ProcessDialogKey method is not run.
        /// this seems to happen when when tabbing from
        /// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyMessage(ref Message m)
        {
            bool bShiftPressed;
            bool bTabPressed = mpnControls.Dialog.TabKeyPressed(m, out bShiftPressed);

            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
        /// <summary>
        /// ensures that a processed tab (processed in ProcessKeyMessage)
        /// won't continue to be processed
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyEventArgs(ref Message m)
        {
            if (m.WParam.ToInt32() == (int)Keys.Tab)
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
        }
        protected override void OnGotFocus(EventArgs e)
        {
            //line below causes last character of selected text to be hidden
            //this.comboBox1.SelectAll();
            base.OnGotFocus(e);
        }
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public virtual void ExecuteFinalSetup()
        {
            m_bIsDirty = false;
            m_bControlIsLoaded = true;
        }
        [DescriptionAttribute("Gets/Sets the selected value.")]
        public virtual string Value
        {
            get
            {
                if (this.LimitToList)
                {
                    //Make sure Value reflects latest changes
                    this.Validate();
                    //refresh selectedindex property with stored value
                    //this is a bug in windows.system.forms.combobox
                    if (this.comboBox1.SelectedIndex == -1)
                        this.comboBox1.SelectedIndex = this.SelectedIndex;

                    if (this.comboBox1.SelectedValue != null)
                        return this.comboBox1.SelectedValue.ToString();
                    else
                        return "";
                }
                else
                    return this.comboBox1.Text;
            }
            set
            {
                if (this.LimitToList)
                {
                    if (System.String.IsNullOrEmpty(value))
                    {
                        //clear selected text
                        this.comboBox1.SelectedText = "";
                        this.comboBox1.Text = "";
                    }
                    else
                    {
                        m_bProgrammaticallySettingValue = true;
                        bool bIsDirty = this.m_bIsDirty;
                        this.comboBox1.SelectedValue = value;
                        this.m_bIsDirty = bIsDirty;
                        m_bProgrammaticallySettingValue = false;
                    }
                }
                else
                    this.Text = value;
            }
        }
        public bool IsDirty
        {
            set
            {
                if (m_bControlIsLoaded)
                    m_bIsDirty = value;
            }

            //TODO: this needs to be implemented
            get { return m_bIsDirty; }
        }
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
        #endregion
        #region *********************helper classes*********************
        /// <summary>
        /// IComparer to sort List based on first dimension
        /// </summary>
        public class ComboBoxSorter : IComparer
        {
            private bool m_bDescending = false;
            public ComboBoxSorter(bool bDescending)
            {
                m_bDescending = bDescending;
            }
            public int Compare(object x, object y)
            {
                string[] aPref1 = (string[])x;
                string[] aPref2 = (string[])y;

                CaseInsensitiveComparer o = new CaseInsensitiveComparer(System.Threading.Thread.CurrentThread.CurrentCulture);
                if (m_bDescending)
                    return o.Compare(aPref2[0], aPref1[0]);
                else
                    return o.Compare(aPref1[0], aPref2[0]);
            }
        }
        /// <summary>
        /// IComparer for ArrayList of Segment and Section Index items
        /// </summary>
        public class ArraySorterDescending : IComparer
        {
            public int Compare(object x, object y)
            {
                string[] aPref1 = (string[])x;
                string[] aPref2 = (string[])y;

                CaseInsensitiveComparer o = new CaseInsensitiveComparer(System.Threading.Thread.CurrentThread.CurrentCulture);
                return o.Compare(aPref2[0], aPref1[0]);
            }
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.mnuContext_Clear = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuContext_Cut = new System.Windows.Forms.MenuItem();
            this.mnuContext_Copy = new System.Windows.Forms.MenuItem();
            this.mnuContext_Paste = new System.Windows.Forms.MenuItem();
            this.textBox1 = new mpnControls.TextBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.ContextMenu = this.mnuContext;
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.Location = new System.Drawing.Point(0, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(288, 23);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.DropDownClosed += new System.EventHandler(this.comboBox1_DropDownClosed);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            this.comboBox1.Enter += new System.EventHandler(this.comboBox1_Enter);
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            this.comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox1_KeyPress);
            this.comboBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyUp);
            this.comboBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseDoubleClick);
            this.comboBox1.MouseEnter += new System.EventHandler(this.comboBox1_MouseEnter);
            this.comboBox1.MouseLeave += new System.EventHandler(this.comboBox1_MouseLeave);
            this.comboBox1.MouseHover += new System.EventHandler(this.comboBox1_MouseHover);
            this.comboBox1.Resize += new System.EventHandler(this.comboBox1_Resize);
            this.comboBox1.Validating += new System.ComponentModel.CancelEventHandler(this.comboBox1_Validating);
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuContext_Clear,
            this.menuItem1,
            this.mnuContext_Cut,
            this.mnuContext_Copy,
            this.mnuContext_Paste});
            this.mnuContext.Popup += new System.EventHandler(this.mnuContext_Popup);
            // 
            // mnuContext_Clear
            // 
            this.mnuContext_Clear.Index = 0;
            this.mnuContext_Clear.Text = "&Clear";
            this.mnuContext_Clear.Click += new System.EventHandler(this.mnuContext_Clear_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "-";
            // 
            // mnuContext_Cut
            // 
            this.mnuContext_Cut.Index = 2;
            this.mnuContext_Cut.Text = "Cut";
            this.mnuContext_Cut.Click += new System.EventHandler(this.mnuContext_Cut_Click);
            // 
            // mnuContext_Copy
            // 
            this.mnuContext_Copy.Index = 3;
            this.mnuContext_Copy.Text = "Copy";
            this.mnuContext_Copy.Click += new System.EventHandler(this.mnuContext_Copy_Click);
            // 
            // mnuContext_Paste
            // 
            this.mnuContext_Paste.Index = 4;
            this.mnuContext_Paste.Text = "&Paste";
            this.mnuContext_Paste.Click += new System.EventHandler(this.mnuContext_Paste_Click);
            // 
            // textBox1
            // 
            this.textBox1.IsDirty = true;
            this.textBox1.Location = new System.Drawing.Point(-10, -4);
            this.textBox1.Name = "textBox1";
            this.textBox1.SelectionMode = mpnControls.TextBox.SelectionTypes.SelectAll;
            this.textBox1.Size = new System.Drawing.Size(10, 22);
            this.textBox1.SupportingValues = "";
            this.textBox1.TabIndex = 1;
            this.textBox1.TabStop = false;
            this.textBox1.Tag2 = null;
            this.textBox1.Value = "";
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
            // 
            // ComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox1);
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ComboBox";
            this.Size = new System.Drawing.Size(288, 23);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ComboBox_Paint);
            this.Enter += new System.EventHandler(this.ComboBox_Enter);
            this.Leave += new System.EventHandler(this.ComboBox_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}