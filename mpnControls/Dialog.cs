using System;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace mpnControls
{
	/// <summary>
	/// Contains low level Dialog functions
	/// </summary>
	public sealed class Dialog
	{
        [DllImport("User32.dll")]
        public static extern Int32 GetKeyState(int nVirtKey);
        
        /// <summary>
		/// private constructor - prevent instance creation
		/// </summary>
		private Dialog(){}
		public static void EnsureSelectedContent(Control oCtl, 
			bool bLastCharOnly, bool bForceSelection)
		{
			//TODO: implement conditional to select only when 
			//tabbing in or if bForceSelect == true -
			//implement IsPressed(9) as disjunct
			switch(oCtl.GetType().Name.ToUpper())
			{
                //case "LISTBOX":
                //{
                //    //select first list item
                //    LMP.Controls.ListBox oLst = (LMP.Controls.ListBox)oCtl;
                //    if(oLst.SelectedIndex < 0)
                //        oLst.SelectedIndex = 0;
                //    break;
                //}
				case "ComboBox":
				{
					mpnControls.ComboBox oCombo = (mpnControls.ComboBox) oCtl;
					if(bLastCharOnly)
					{
						//select last char
						oCombo.SelectionStart = oCombo.Text.Length;
						oCombo.SelectionLength = 1;
					}
					else
					{
						oCombo.SelectionStart = 0;
						oCombo.SelectionLength = oCombo.Text.Length;
					}
					break;
				}
				case "TEXTBOX":
				{
					mpnControls.TextBox oTB = (mpnControls.TextBox) oCtl;
					if(bLastCharOnly)
					{
						//select last char
						oTB.SelectionStart = oTB.Text.Length;
						oTB.SelectionLength = 1;
					}
					else
					{
						oTB.SelectionStart = 0;
						oTB.SelectionLength = oTB.Text.Length;
					}
					break;
				}
				case "MPMULTILINECOMBO":
				{
					//TODO: implement this block
					break;
				}
				case "SALUTATIONCOMBO":
				{
					//TODO: implement this block
					break;
				}
				case "CLIENTMATTERCOMBO":
				{
					//TODO: implement this block
					break;
				}
				default:
				{
					break;
				}
			}
		}
        /// <summary>
        /// returns True if the key is defined
        /// by MacPac to be an input key - this
        /// is necessary because the base.InputKey()
        /// method is problematic, possible due to IBF
        /// </summary>
        /// <param name="KeyData"></param>
        /// <returns></returns>
        public static bool IsMacPacDefinedInputKey(Keys KeyData)
      {
            const int KEY_EQUALSIGN = 187;
            const int KEY_OPENBRACKET = 219;
            const int KEY_CLOSEBRACKET = 221;

            int iAsc = (int)KeyData;
            return (iAsc >= 65 && iAsc <= 90) || (iAsc >= 65601 && iAsc <= 65626) || (iAsc == 65725) ||
                (iAsc == KEY_EQUALSIGN) || (iAsc == KEY_OPENBRACKET) || (iAsc == KEY_CLOSEBRACKET);
        }

        /// <summary>
        /// returns true iff the specified text contains
        /// a MacPac reserved character - i.e. those characters
        /// that the application uses, like delimiters
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        internal static bool ContainsMacPacReservedChars(string xText)
        {
            return Regex.IsMatch(xText, @"[\|ޤ���]");
        }
        /// <summary>
        /// removes MacPac reserved characters from the specified text
        /// </summary>
        /// <param name="xText"></param>
        /// <returns></returns>
        internal static string RemoveMacPacReservedChars(string xText)
        {
            return Regex.Replace(xText, @"[\|ޤ���]", "");
        }

        /// <summary>
        /// returns True iff the specified message indicates
        /// that the tab key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="ShiftPressed">returns True iff the shift key was pressed along with the tab key</param>
        /// <returns></returns>
        public static bool TabKeyPressed(Message oMessage, out bool ShiftPressed)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_TAB = 9;
            const int KEY_SHIFT = 16;

            if (oMessage.Msg == WM_KEYDOWN && oMessage.WParam.ToInt32() == KEY_TAB)
            {
                //tab key was pressed - get state of shift key
                int iKeyState = GetKeyState(KEY_SHIFT);
                ShiftPressed = !(iKeyState == 1 || iKeyState == 0);

                return true;
            }
            else
            {
                //tab key was not pressed
                ShiftPressed = false;
                return false;
            }
        }

        /// <summary>
        /// returns True if the specified message indicates
        /// that the Enter key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="bNoKeyQualifiers"</param>
        /// <returns></returns>
        public static bool EnterKeyPressed(Message oMessage, out int iQualifierKeys)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_ENTER = 13;
            const int KEY_SHIFT = 16;
            const int KEY_CTRL = 17;
            const int KEY_ALTL = 164;
            const int KEY_ALTR = 165;

            iQualifierKeys = 0;
            if (oMessage.Msg == WM_KEYDOWN && oMessage.WParam.ToInt32() == KEY_ENTER)
            {
                int iKeyState = GetKeyState(KEY_CTRL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_CTRL;

                iKeyState = GetKeyState(KEY_ALTL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_ALTL;

                iKeyState = GetKeyState(KEY_ALTR);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_ALTR;

                iKeyState = GetKeyState(KEY_SHIFT);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_SHIFT;


                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// returns True if the specified message indicates
        /// that the Enter key was pressed
        /// </summary>
        /// <param name="oMessage"></param>
        /// <param name="bNoKeyQualifiers"</param>
        /// <returns></returns>
        public static bool AltKeyPressed(Message oMessage, out int iQualifierKeys)
        {
            const int WM_KEYDOWN = 256;
            const int KEY_ALT = 18;
            const int KEY_SHIFT = 16;
            const int KEY_CTRL = 17;

            iQualifierKeys = 0;
            if ((oMessage.Msg == 260 || oMessage.Msg == WM_KEYDOWN) && oMessage.WParam.ToInt32() == KEY_ALT)
            {
                int iKeyState = GetKeyState(KEY_CTRL);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_CTRL;

                iKeyState = GetKeyState(KEY_SHIFT);
                if (!(iKeyState == 1 || iKeyState == 0))
                    iQualifierKeys = iQualifierKeys + KEY_SHIFT;

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}