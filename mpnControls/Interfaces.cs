using System;
using System.Windows.Forms;

namespace mpnControls
{
	/// <summary>
	/// contains the members that must be implemented
	/// for a class to be an LMP control
	/// </summary>
	public interface IControl
	{
		event TabPressedHandler TabPressed;
        event ValueChangedHandler ValueChanged;
        event KeyEventHandler KeyPressed;
        event KeyEventHandler KeyReleased;

        /// <summary>
        /// executes any setup that the control may need
        /// </summary>
		void ExecuteFinalSetup();
		/// <summary>
		/// gets/sets the value of the control
		/// </summary>
		string Value{get;set;}
        /// <summary>
        /// gets sets whether the control has been edited
        /// </summary>
		bool IsDirty{get;set;}
        /// <summary>
        /// get/sets a second tag value
        /// </summary>
        object Tag2 { get;set;}
        /// <summary>
        /// gets/sets Supporting Values of control
        /// </summary>
        string SupportingValues 
        { 
            //GLOG 2645: Used to hold control-specific values between sessions
            //that can be used to restore control's previous state
            get; 
            set; 
        }
	}

    public interface ICIable
    {
        event CIRequestedHandler CIRequested;

        bool IsSubscribedToCI { get; }

        long ContactCount { get; }
    }

    public delegate void CIRequestedHandler(object sender, EventArgs e);

	public class TabPressedEventArgs: System.EventArgs
	{
		public bool ShiftPressed;

		public TabPressedEventArgs(bool Shift)
		{
			this.ShiftPressed = Shift;
		}
	}

	public class AltPressedEventArgs: System.EventArgs
	{
		public char KeyPressed;

		public AltPressedEventArgs(char cKeyPressed)
		{
            this.KeyPressed = cKeyPressed;
		}
	}

    public class EnterKeyPressedEventArgs : System.EventArgs
    {
        public int QualifierKeys;

        public EnterKeyPressedEventArgs(int iQualifierKeys)
        {
            this.QualifierKeys = iQualifierKeys;
        }
    }

    public delegate void TabPressedHandler(object sender, TabPressedEventArgs e);
	public delegate void AltPressedHandler(object sender, AltPressedEventArgs e);
    public delegate void EnterKeyPressedHandler(object sender, EnterKeyPressedEventArgs e);
    public delegate void ValueChangedHandler(object sender, EventArgs e);
}
