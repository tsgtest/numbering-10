﻿using System;
using System.Runtime.InteropServices;

namespace mpnControls
{
    public class PreviewCapture
    {
        public static int GetPreviewWindowTop(IntPtr hWnd)
        {
            //returns the top of the window with the specified handle-
            //this needs to be in a C# method, as the VB.NET analog
            //executes inconsistently
            Rect oRect = new Rect();
            bool bRet = false;

            bRet = WinAPI.GetWindowRect(hWnd, ref oRect);
            return oRect.Top;
        }
        private class WinAPI
        {
            [DllImport("user32.dll")]
            public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);
        }
        private struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

    }
}