﻿
namespace mpnControls
{
    class Strings
    {
        /// <summary>
        /// returns the number of instances of a substring
        /// </summary>
        /// <param name="xSource"></param>
        /// <param name="xSearch"></param>
        /// <returns></returns>
        public static int CountChrs(string xSource, string xSearch)
        {
            if ((xSource == "") || (xSearch == ""))
                return 0;

            int i = 0;
            int iPos = xSource.IndexOf(xSearch);
            while (iPos != -1)
            {
                i++;
                iPos = xSource.IndexOf(xSearch, iPos + 1);
            }
            return i;
        }
    }
}
