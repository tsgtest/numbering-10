using System;
using System.Windows.Forms;

namespace mpnControls
{
	/// <summary>
	/// contains the member of the LMP textbox -e
	/// derives from Windows Forms textbox
	/// </summary>
	public class TextBox: System.Windows.Forms.TextBox, IControl, ICIable
    {
        #region *********************enumerations*********************
        public enum SelectionTypes  //GLOG 6235
        {
            SelectAll=0,
            SelectEnd=1,
            SelectStart=2
        }
        #endregion
        #region *********************fields*********************
        private bool m_bIsDirty;
        private object m_oTag2;
        public bool m_bAllowReservedCharacterInput;
        private string m_xSupportingValues = "";
        private SelectionTypes m_iSelectionMode = SelectionTypes.SelectAll; //GLOG 6235
        #endregion
		#region *********************constructors*********************
		public TextBox()
		{
			this.AcceptsTab = false;
            this.AllowReservedCharacterInput = false;
            this.EnterKeyPressed += new EnterKeyPressedHandler(HandleEnterKeyPressed);
        }
        public TextBox(bool AllowReservedCharacterInput)
        {
            this.AllowReservedCharacterInput = AllowReservedCharacterInput;
            this.EnterKeyPressed += new EnterKeyPressedHandler(HandleEnterKeyPressed);
        }
        #endregion
		#region *********************properties*********************
        internal bool AllowReservedCharacterInput
        {
            get { return m_bAllowReservedCharacterInput; }
            set { m_bAllowReservedCharacterInput = value; }
        }
        public SelectionTypes SelectionMode //GLOG 6235
        {
            get { return m_iSelectionMode; }
            set { m_iSelectionMode = value; }
        }
        #endregion
        #region *********************IControl members*********************
        public event TabPressedHandler TabPressed;
        public event EnterKeyPressedHandler EnterKeyPressed;
        public event ValueChangedHandler ValueChanged;
        public event KeyEventHandler KeyPressed;
        public event KeyEventHandler KeyReleased;

        public void ExecuteFinalSetup()
		{
		}
		public string Value
		{
            get
            {
                //replace reserved chars with escape sequences
                return this.Text;
            }
			set
			{
				string xValue = null;

				if(value != null)
				{
					//replace returns and shift returns with appropriate chars
					xValue = value;

                    // GLOG : 2929 : JAB
                    // Replace \n with \r\n to display proper carriage return. 
                    //GLOG: 3207 - first replace existing \r\n to prevent extra line breaks from getting added
                    xValue = xValue.Replace("\r\n", "\n");
                    xValue = xValue.Replace("\r", "\n"); //GLOG 4511
                    xValue = xValue.Replace("\n", "\r\n");
                    xValue = xValue.Replace("\v", "\r\n");
				}

                this.Text = xValue;
			}
		}
		public bool IsDirty
		{
			set{m_bIsDirty = value;}
			get{return m_bIsDirty;}
		}
        public object Tag2
        {
            get { return m_oTag2; }
            set { m_oTag2 = value; }
        }
        public string SupportingValues
        {
            get { return m_xSupportingValues; }
            set { m_xSupportingValues = value; }
        }
		#endregion
        #region *********************ICIable members*********************
        public event CIRequestedHandler CIRequested;
        public bool IsSubscribedToCI
        {
            get {
                if ((System.Delegate)this.CIRequested != null)
                {
                    return this.CIRequested.GetInvocationList().Length > 0;
                }
                else
                {
                    return false;
                }
            }
        }
        public long  ContactCount
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Text))
                    return Strings.CountChrs(this.Text, "\r\n\r\n") + 1;
                else
                    return 0;
            }
        }
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Return)
                // Need this so that Enter will generate WM_KEYDOWN for EnterKeyPressed function
                return true;
            else if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
                if (this.KeyPressed != null)
                    this.KeyPressed(this, e);

                base.OnKeyDown(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (this.KeyReleased != null)
                this.KeyReleased(this, e);
        }
		/// <summary>
		/// ensures that a processed tab (processed in ProcessKeyMessage)
		/// won't continue to be processed
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		protected override bool ProcessKeyEventArgs(ref Message m)
		{
            if ((m.WParam.ToInt32() == (int)Keys.Tab || m.WParam.ToInt32() == (int)Keys.Return))
                return true;
            else
                return base.ProcessKeyEventArgs(ref m);
		}
		/// <summary>
		/// processes tab and shift-tab key messages -
		/// this method will be executed only when the
		/// message pump is broken, ie when the hosing
		/// form's ProcessDialogKey method is not run.
		/// this seems to happen when when tabbing from
		/// a usercontrol(e.g. LMP.Controls.Combobox) to a textbox
		/// </summary>
		/// <param name="m"></param>
		/// <returns></returns>
		protected override bool ProcessKeyMessage(ref Message m)
		{
            int iQualifierKeys;
            int iAltQualifierKeys;
            bool bShiftPressed;
            bool bEnterKeyPressed = mpnControls.Dialog.EnterKeyPressed(m, out iQualifierKeys);
            bool bTabPressed = mpnControls.Dialog.TabKeyPressed(m, out bShiftPressed);
            bool bAltPressed = mpnControls.Dialog.AltKeyPressed(m, out iAltQualifierKeys);
            if (bTabPressed)
            {
                //tab key was pressed - raise TabPressed event
                //if there are subscribers
                if (this.TabPressed != null)
                    this.TabPressed(this, new TabPressedEventArgs(bShiftPressed));

                //return that key was processed
                return true;
            }
            else if (bEnterKeyPressed)
            {
                if (this.KeyPressed != null)
                    this.KeyPressed(this, new KeyEventArgs(Keys.Enter));

                if (this.EnterKeyPressed != null)
                    this.EnterKeyPressed(this, new EnterKeyPressedEventArgs(iQualifierKeys));
                //return that key was processed
                return true;
            }
            else
                //process key normally
                return base.ProcessKeyMessage(ref m);
        }
		protected override void OnGotFocus(EventArgs e)
		{
            switch (m_iSelectionMode)
            {
                case SelectionTypes.SelectEnd:
                    this.Select(this.Text.Length, 0);
                    break;
                case SelectionTypes.SelectStart:
                    this.Select(0, 0);
                    break;
                default:
                    this.SelectAll();
                    break;
            }
			base.OnGotFocus (e);
		}
		protected override void OnTextChanged(EventArgs e)
		{
			//mark as edited
			m_bIsDirty = true;

            //remove MacPac reserved characters from control -
            //we do this here, as opposed to a KeyDown-type
            //event so that we can capture pasting of reserved characters
            if (!this.AllowReservedCharacterInput && Dialog.ContainsMacPacReservedChars(this.Text))
            {
                //get current insertion point position
                int iSelStart = this.SelectionStart;

                //remove reserved chars
                this.Text = Dialog.RemoveMacPacReservedChars(this.Text);

                //return insertion point
                this.SelectionStart = Math.Max(iSelStart - 1, 0);
            }

			base.OnTextChanged (e);

            //raise event if subscribers exist
            if (ValueChanged != null)
                ValueChanged(this, new EventArgs());
		}
        #endregion
        #region *********************event handlers*********************
        /// <summary>
        /// Handle Enter Key if multiline configured
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleEnterKeyPressed(object sender, EnterKeyPressedEventArgs e)
        {
            //When located on the Taskpane, enter key does not work insert line feed; insert directly

            const int KEY_SHIFT = 16;
            const int KEY_CTRL = 17;
            const int KEY_ALTL = 164;
            const int KEY_ALTR = 165;
            try
            {
                //Code not necessary if Ctrl is pressed
                if (((e.QualifierKeys & KEY_CTRL) == KEY_CTRL))
                    return;

                if (this.AcceptsReturn && this.Multiline)
                {
                    int iSelStart = this.SelectionStart;
                    this.SelectedText = "\r\n";
                    this.SelectionStart = iSelStart + 2;
                    this.SelectionLength = 0;
                }
            }
            catch (Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
        #endregion
        #region *********************private members*********************
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.ResumeLayout(false);

        }
        #endregion
    }

	/// <summary>
	/// contains the members of the LMP mulitline textbox -
	/// derives from LMP textbox
	/// </summary>
	public class MultilineTextBox: mpnControls.TextBox
	{
        #region *********************fields*********************
        private int m_iNumLines = 2;
        #endregion
        #region *********************protected members*********************
        protected override bool IsInputKey(Keys keyData)
		{
            if (keyData == Keys.Return)
                // Need this so that Enter will generate WM_KEYDOWN for EnterKeyPressed function
                return true;
            else if (keyData == Keys.Tab)
                return false;
            else if (Dialog.IsMacPacDefinedInputKey(keyData))
                //fixes a bug, whereby certain alphanumeric
                //characters cause this method to return false
                return true;
            else
                return base.IsInputKey(keyData);
        }
        protected override void OnTextChanged(EventArgs e)
        {
            try
            {
                base.OnTextChanged(e);

                //add vertical scroll bar if there are more
                //lines than can be seen without scrolling
                bool bScrollBarsChanged = false;

                //GLOG : 6424 : CEH
                //get selection start before scrollbars reset
                int iSelectionStart = this.SelectionStart;

                //GLOG 3099: value returned is zero-based
                if (this.GetLineFromCharIndex(this.Text.Length - 1) >= this.NumberOfLines)
                {
                    bScrollBarsChanged = this.ScrollBars == ScrollBars.None;
                    this.ScrollBars = ScrollBars.Vertical;
                }
                else
                {
                    bScrollBarsChanged = this.ScrollBars == ScrollBars.Vertical;
                    this.ScrollBars = ScrollBars.None;
                }

                if (bScrollBarsChanged)
                {
                    //GLOG : 6424 : CEH
                    //select end of previous selection -
                    //this is necessary because all text in the
                    //control is selected when the scroll bars are set

                    //this.SelectionStart = this.SelectionStart + this.SelectionLength;
                    this.SelectionStart = iSelectionStart;
                    this.SelectionLength = 0;

                    //make sure caret is visible
                    if (this.GetLineFromCharIndex(this.SelectionStart) >= this.NumberOfLines)
                        this.ScrollToCaret();

                }
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
            }
        }
		#endregion
        #region *********************constructors*********************
        public MultilineTextBox()
		{
            ConfigureTextBox();
        }
        public MultilineTextBox(bool bAllowReservedCharacterInput)
            : base(bAllowReservedCharacterInput)
        {
            ConfigureTextBox(); 
        }

        #endregion
        #region ********************private methods*********************
        private void ConfigureTextBox()
        {
			this.Multiline = true;
			this.AcceptsReturn = true;
			this.AcceptsTab = false;
			this.ScrollBars = ScrollBars.None;
            //GLOG : 6424 : CEH
            //set default
            this.Width = 285;
        }
        #endregion *****************************************************
        #region *********************properties*************************
        public int NumberOfLines
		{
			get{return m_iNumLines;}
			set
			{
				if(value < 1)
				{
                    //throw new LMP.Exceptions.ControlValueException(
                    //    LMP.Resources.GetLangString("Error_InvalidPropertySetting") + 
                    //    "NumberOfLines=" + value.ToString());
				}
				m_iNumLines = value;

                //GLOG : 6424 : CEH
                //set textbox height
				this.Height = 25 + ((m_iNumLines - 1) * 15);
			}
        }
        #endregion
    }
}
