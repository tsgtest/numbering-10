﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace LMP
{
    public struct MailServerParameters
    {
        public string Host;
        public bool UseSsl;
        public string UserID;
        public string Password;
        public bool UseNetworkCredentialsToLogin;
        public int Port;
    }

    public class Mail
    {
        private MailServerParameters m_oParams;

        public Mail(MailServerParameters oParams)
        {
            this.m_oParams = oParams;
        }

        /// <summary>
        /// sends the specified message to the specified address
        /// </summary>
        /// <param name="xEmailAddress"></param>
        /// <param name="xSubject"></param>
        /// <param name="xBody"></param>
        public static void SendMessage(string xFrom, string xSubject,
            string xBody, MailServerParameters oClientParams, Attachment[] aAttachments, params string[] aRecipients)
        {
            MailMessage oMsg = new MailMessage();
            oMsg.From = new MailAddress(xFrom);

            foreach (string xRecipient in aRecipients)
            {
                oMsg.To.Add(new MailAddress(xRecipient));
            }

            oMsg.Subject = xSubject;
            oMsg.Body = xBody;
            oMsg.IsBodyHtml = true;

            if (aAttachments != null)
            {
                foreach (Attachment oAttachment in aAttachments)
                {
                    oMsg.Attachments.Add(oAttachment);
                }
            }

            SmtpClient oClient = new SmtpClient();

            if (oClientParams.UseNetworkCredentialsToLogin)
                oClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            else
            {
                System.Net.NetworkCredential oCredential = new System.Net.NetworkCredential(
                    oClientParams.UserID, oClientParams.Password);
                oClient.Credentials = oCredential;
            }

            oClient.Host = oClientParams.Host;
            oClient.EnableSsl = oClientParams.UseSsl;
            oClient.Port = oClientParams.Port;
            oClient.Send(oMsg);
        }
    }
}
